import sys
import PyQt5
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout, QMenu, QProgressBar, QInputDialog, QMessageBox
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtCore import pyqtSlot
from TableWidget import Ui_MainWindow
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Input
import numpy as np
from numpy import genfromtxt, array2string
import time
import os
import h5py
import math
import tkinter as tk
from tkinter import filedialog
import csv

# qtCreatorFile = "TableWidget.py"
#
# Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class MainWindow(QMainWindow, Ui_MainWindow):


    # Initializes entire app
    def __init__(self):
        loaded = False
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.string_array = []
        self.prediction = []
        self.actionLoad = self.actionLoad_Data
        self.actionLoad.triggered.connect(self.load_Data)

        self.actionSearch = self.actionSearch_Time
        self.actionSearch.triggered.connect(self.search_data)

        self.progressBar.setProperty("value", 0)


    # Creates Table Widget where predict data will go
    def tableSetUp(self):
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("Time"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Prediction"))
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

    def search_data(self):
        searchInput, okPressed = QInputDialog.getText(self, "Search Box", "Input time you wish to search for: ")

        if okPressed:
            length = len(self.string_array)
            index = 0
            found = False
            while index < length and found == False:
                currStr = self.string_array[index][0]
                if currStr == searchInput:
                    found = True
                    displayText = "Time = " + self.string_array[index][0] + ", AGC = " + \
                                  self.prediction[index][0].astype('str')
                    dialog = QMessageBox()
                    dialog.about(self, "Search Result", displayText)
                else:
                    index += 1




    def load_Data(self, data):
        super()


        # Get file from user
        root = tk.Tk()
        root.withdraw()
        rawFile = filedialog.askopenfilename()
        print(rawFile)

        # Open file into string array
        file = r'{}'.format(rawFile)
        fileOpen = open(file, "r")
        string_file = csv.reader(fileOpen, delimiter = ",")
        rownum = 0
        self.string_array = []

        for row in string_file:
            self.string_array.append(row)
            rownum += 1
        currStep = 10
        self.progressBar.setProperty("value", currStep)
        # Generate nparray from data
        # user_data = np.zeros((1, 22))
        user_data = (genfromtxt(rawFile, delimiter=',', usecols=np.arange(0, 22), skip_header=6))
        # user_data = np.append(user_data, new_data, axis=0)

        # Format user data
        print("Formatting Data...")
        columns_to_delete = [21, 20, 19, 3, 0]
        length = len(self.string_array) - 6
        progressIndex = 0

        #Delete first column of each row in dataset (i.e. delete timestamp)
        for column in columns_to_delete:
            self.progressBar.setProperty("value", 10 + progressIndex/length * 40)
            user_data = np.delete(user_data, column, 1)
            progressIndex += 1

        # Delete the row of zeros at the beginning
        # user_data = np.delete(user_data, 0, 0)

        # Filling table with time stamps
        print("Filling table...")

        self.tableWidget.setRowCount(length)
        self.string_array = self.string_array[6:]

        print("Running Predict")
        predict_Data(self, user_data, length, self.progressBar)
        print("Table filled")
        loaded = True




def predict_Data(app, data, length, progressBar):
    dir = "/home/hal/Documents/H-TownHussies/PyQt/"
    # Load neural net
    model = load_model("output2.h5")

    app.prediction = model.predict(data)

    datacount = 0

    while datacount < length:

        time = QTableWidgetItem()
        time.setText(app.string_array[datacount][0])
        app.tableWidget.setItem(datacount, 0, time)

        predict = QTableWidgetItem()
        predict.setText(app.prediction[datacount][0].astype('str'))
        app.tableWidget.setItem(datacount, 1, predict)

        if app.prediction[datacount][0].astype('float') < 0.5:
            predict.setBackground(QColor(198,239,206))
        else:
            predict.setBackground(QColor(255,199,206))

        progressBar.setProperty("value", 50 + datacount/length*50)
        datacount += 1



def main():
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.tableSetUp()
    main_window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

