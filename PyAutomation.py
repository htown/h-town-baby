
import goodDataBadData, skynetGPUv2, shutil, os



def main():

    # loop through files here instead of in gooddatabaddata
    dir = '/home/hal/Documents/H-TownHussies/binaryFolder'
    folder = 'SBandData_raw'
    for file in os.listdir(dir):
        print(file)
        if file.endswith(".csv"):

            shutil.move(dir + '/' + file, dir + '/' + folder)
            goodDataBadData.main()

    skynetGPUv2.main()



if __name__ == "__main__":
    main()
    exit()