IS_AntmanAuto                   IAM_AUTO_UPDATE # Auto update, manager only

IS_Gmt                  M41F9001D       # GMT in milliseconds from start of year
IS_GmtYear              M41F9003F       # GMT Year

# TDRS
IS_SunCoordsX           LADP06MD2425U   # Sun LOS Vector X
IS_SunCoordsY           LADP06MD2426U   # Sun LOS Vector Y
IS_SunCoordsZ           LADP06MD2427U   # Sun LOS Vector Z

IS_TdreSatId            LADP06MD2607U   # TDR1 Satellite ID
IS_TdreCoordsX          LADP06MD2601U   # TDR1 LOS Vector X
IS_TdreCoordsY          LADP06MD2602U   # TDR1 LOS Vector Y
IS_TdreCoordsZ          LADP06MD2603U   # TDR1 LOS Vector Z

IS_TdrwSatId            LADP06MD2615U   # TDR2 Satellite ID
IS_TdrwCoordsX          LADP06MD2609U   # TDR2 LOS Vector X
IS_TdrwCoordsY          LADP06MD2610U   # TDR2 LOS Vector Y
IS_TdrwCoordsZ          LADP06MD2611U   # TDR2 LOS Vector Z

IS_TdrsSelectFlag       LADP01MDAPT7J   # selected E=[1,2,3]  W=[4,5,6]
IS_RiseSetQuality       LADP06MD2594U   # TDRS Rise Set Quality
IS_RiseSetTarget1       LADP06MD2546U   # CDT PS Rise/Set Target 1
IS_RiseSetTarget2       LADP06MD2547U   # CDT PS Rise/Set Target 2
IS_RiseSetTarget3       LADP06MD2548U   # CDT PS Rise/Set Target 3
IS_RiseSetTarget4       LADP06MD2549U   # CDT PS Rise/Set Target 4
IS_RiseSetFlag1         LADP06MD2554U   # CDT PS Rise(1) or Set(0) Flag 1
IS_RiseSetFlag2         LADP06MD2555U   # CDT PS Rise(1) or Set(0) Flag 2 TDRW inview
IS_RiseSetFlag3         LADP06MD2556U   # CDT PS Rise(1) or Set(0) Flag 3 TDRE inview
IS_RiseSetFlag4         LADP06MD2557U   # CDT PS Rise(1) or Set(0) Flag 4

IS_P_TRRJ_DSM           S0DP04MD30D6J   # DSM configuration status logical thread enabled TRRJ M PORT
IS_P_TRRJ_Gamma         S0DP04MD601SJ   # TRRJ M bad data int port gamma joint status
IS_P_TRRJ_Readout       S0DP04MD604BH   # TRRJ M bad data float port joint angle readout
IS_S_TRRJ_DSM           S0DP04MD30D5J   # DSM configuration status logical thread enabled TRRJ M STBD
IS_S_TRRJ_Gamma         S0DP04MD601PJ   # TRRJ M bad data int stbd gamma joint status
IS_S_TRRJ_Readout       S0DP04MD604AH   # TRRJ M bad data float stbd joint angle readout

# JOINT ANGLES
IS_2BJointAngle         P6DP34MD5632H   # 2B Joint Angle
IS_4BJointAngle         P6DP34MD5631H   # 4B Joint Angle
IS_2AJointAngle         P4DP33MD5631H   # 2A Joint Angle
IS_4AJointAngle         P4DP33MD5632H   # 4A Joint Angle
IS_PAJointAngle         S0DP04MD604JH   # Port SARJ Joint Angle
IS_SAJointAngle         S0DP04MD604IH   # Stbd SARJ Joint Angle
IS_1AJointAngle         S4DP31MD5631H   # 1A Joint Angle
IS_3AJointAngle         S4DP31MD5632H   # 3A Joint Angle
IS_1BJointAngle         S6DP32MD5632H   # 1B Joint Angle
IS_3BJointAngle         S6DP32MD5631H   # 3B Joint Angle
IS_PTCSJointAngle       P1TRRJPos       # Port Thermal Radiator Angle
IS_STCSJointAngle       S1TRRJPos       # Stbd Thermal Radiator Angle
#IS_PTCSJointAngle       RESERVED        # if (S0DP04MD30D6J == 0 || S0DP04MD601SJ == 3, 0,S0DP04MD604BH)
#IS_STCSJointAngle       RESERVED        # if (S0DP04MD30D5J == 0 || S0DP04MD601PJ == 3, 0,S0DP04MD604AH)

# PATHGO, SG1, SG2 ISPATOM PathGo.atm symbols output
IS_PathGO               PathGo          #
IS_SG1                  SG1             #
IS_SG2                  SG2             #

# KUBAND
IS_KuPwrAmpAct          Z1CK04FC0010J   # Actual Power Amp Setting
IS_KuPwrAmpMisCmpr      RESERVED        # Actual/Pending Miscompare Ind.
IS_KuPwrAmpPend         LADP01MDARP1J   # Pending Power Amp Setting
IS_KuPLCAct             Z1CK04FC0009J   # Actual PLC State
IS_KuPLCMisCmpr         RESERVED        # Actual/Pending Miscompare Ind.
IS_KuPLCPend            LADP01MDARP3J   # Pending PLC State
IS_KuPtgAct             Z1CK04FC0005J   # Actual Pointing Mode
IS_KuPtgMisCmpr         RESERVED        # Actual/Pending Miscompare Ind.
IS_KuPtgPend            LADP01MDARPBJ   # Pending Pointing Mode
IS_KuTDRSSelect         LADP01MDARMHJ   # Selected TDRS
IS_KuAutotrack          Z1CK04FC0029J   # autotrack status
IS_KuHandover           LADP01MDARMJJ   # handover
IS_KuRetry              LADP01MDARM9J   # retry
IS_KuGAO                LADP01MDCPVVU   # Gimbal Angle Offset
IS_KuActualXEL          Z1CK04FC0012H   # actual Cross-Elevation
IS_KuActualEL           Z1CK04FC0013H   # actual Elevation
IS_KuPWRL               Z1CK04FC0019E   # Received Signal Strength
IS_KuFwdPwrVolts        Z1CK04FC0116V   # Fwd Pwr Voltage
IS_KuReflPwrVolts       Z1CK04FC0118V   # Reflected Pwr Voltage
IS_KuIFOutVolts         Z1CK04FC0016V   # IF Output Voltage
IS_KuIFInVolts          Z1CK04FC0115V   # IF Input Voltage
IS_KuSG1Temp            Z1CK04FC0100T   # SGANT#1 Temp - XEL Encoder
IS_KuSG2Temp            Z1CK04FC0101T   # SGANT#2 Temp - EL Encoder
IS_KuSG3Temp            Z1CK04FC0102T   # SGANT#3 Temp - Tracking Mod Driver
IS_KuSG4Temp            Z1CK04FC0103T   # SGANT#4 Temp - Motor Drive Amp
IS_KuSG5Temp            Z1CK04FC0104T   # SGANT#5 Temp - XEL Motor
IS_KuSG6Temp            Z1CK04FC0105T   # SGANT#6 Temp - EL Motor
IS_KuSG7Temp            Z1CK04FC0106T   # SGANT#7 Temp - Tracking Mod Driver
IS_KuSG8Temp            Z1CK04FC0107T   # SGANT#8 Temp - Motor Drive Amp
IS_KuTRCS1Temp          Z1CK04FC0111T   # TRC S-1 Temp
IS_KuTRCS2Temp          Z1CK04FC0112T   # TRC S-2 Temp
IS_KuTRCS3Temp          Z1CK04FC0113T   # TRC S-3 Temp
IS_KuTRCPwrAmpTemp      Z1CK04FC0109T   # TRC Power Amp Temp
IS_KuTRCPwrSupTemp      Z1CK04FC0108T   # TRC Power Supply Temp
IS_KuHRMTemp            LACK02FC0032T   # High Rate Modem Baseplate Temp
IS_KuHRFMTemp           LACK01FC0186T   # High Rate Frame Mux Baseplate Temp
IS_KuVBSPTemp           LACK06FC0056T   # VBSP Baseplate Temp
IS_KuTRC1               LADP01MDAVQBJ   # TRC 1
IS_KuTRC2               LADP01MDAVZTJ   # TRC 2

# SBAND 1
IS_Sb1FrameLk           S1CS04FC0036J   # BSP Frame Lock Indicator
IS_Sb1CarrLk            S1CS23FC0014J   # Xpndr Carrier Lock Indicator
IS_Sb1BitDet            S1CS23FC0018J   # Xpndr Bit Detection Indicator
IS_Sb1Handover          LADP01MDCYHRU   # handover mode
IS_Sb1Tracking          LADP01MDCYHTU   # tracking mode
IS_Sb1SatSel            LADP01MDCYHQU   # satellite select mode
IS_Sb1CmdLnk            RESERVED        # command link status - PathGo
IS_Sb1RELPCh1           RESERVED        # Ch. 1 RELP Sync     - SG1
IS_Sb1RELPCh2           RESERVED        # Ch. 2 RELP Sync     - SG2
IS_Sb1MUXCh1            S1CS04FC0027J   # Ch. 1 MUX Activity
IS_Sb1MUXCh2            S1CS04FC0028J   # Ch. 2 MUX Activity
IS_Sb1CmdAz             S1CS14FC0012H   # est Azimuth
IS_Sb1CmdEl             S1CS14FC0013H   # est Elevation
IS_Sb1AGC               S1CS23FC0010J   # Digital AGC
IS_Sb1AZTemp            S1CS14FC0074T   # Azimuth Gimbal Temperature
IS_Sb1ELTemp            S1CS14FC0073T   # Elevation Gimbal Temperature
IS_Sb1RFGTemp           S1CS14FC0077T   # RFG Baseplate Temperature
IS_Sb1TransTemp         S1CS23FC0043T   # Transponder Baseplate Temperature
IS_Sb1BSPTemp           S1CS04FC0073T   # BSP Baseplate Temperature

# SBAND 2
IS_Sb2FrameLk           P1CS03FC0036J   # BSP Frame Lock Indicator
IS_Sb2CarrLk            P1CS22FC0014J   # Xpndr Carrier Lock Indicator
IS_Sb2BitDet            P1CS22FC0018J   # Xpndr Bit Detection Indicator
IS_Sb2Handover          LADP01MDAPT6J   # handover mode
IS_Sb2Tracking          LADP01MDAPT8J   # tracking mode
IS_Sb2SatSel            LADP01MDAPT5J   # satellite select mode
IS_Sb2CmdLnk            RESERVED        # command link status - PathGo
IS_Sb2RELPCh1           RESERVED        # Ch. 1 RELP Sync     - SG1
IS_Sb2RELPCh2           RESERVED        # Ch. 2 RELP Sync     - SG2
IS_Sb2MUXCh1            P1CS03FC0027J   # Ch. 1 MUX Activity
IS_Sb2MUXCh2            P1CS03FC0028J   # Ch. 2 MUX Activity
IS_Sb2CmdAz             P1CS13FC0012H   # est Azimuth
IS_Sb2CmdEl             P1CS13FC0013H   # est Elevation
IS_Sb2AGC               P1CS22FC0010J   # Digital AGC
IS_Sb2AZTemp            P1CS13FC0074T   # Azimuth Gimbal Temperature
IS_Sb2ELTemp            P1CS13FC0073T   # Elevation Gimbal Temperature
IS_Sb2RFGTemp           P1CS13FC0077T   # RFG Baseplate Temperature
IS_Sb2TransTemp         P1CS22FC0043T   # Transponder Baseplate Temperature
IS_Sb2BSPTemp           P1CS03FC0073T   # BSP Baseplate Temperature

# KUBAND MASKS
IS_KuPtgMask1           LADP01MDDAO9J   # Pointing mask #1
IS_KuPtgMask2           LADP01MDDAOAJ   # Pointing mask #2
IS_KuPtgMask3           LADP01MDDAOBJ   # Pointing mask #3
IS_KuPtgMask4           LADP01MDDAOCJ   # Pointing mask #4
IS_KuPtgMask5           LADP01MDDAODJ   # Pointing mask #5
IS_KuPtgMask6           LADP01MDDAOEJ   # Pointing mask #6
IS_KuPtgMask7           LADP01MDDAOFJ   # Pointing mask #7
IS_KuPtgMask8           LADP01MDDAOGJ   # Pointing mask #8
IS_KuPtgMask9           LADP01MDDAOHJ   # Pointing mask #9
IS_KuPtgMask10          LADP01MDDAOIJ   # Pointing mask #10
IS_KuPtgMask11          RESERVED        # Pointing mask #11
IS_KuPtgMask12          RESERVED        # Pointing mask #12
IS_KuPtgMask13          RESERVED        # Pointing mask #13
IS_KuPtgMask14          RESERVED        # Pointing mask #14
IS_KuPtgMask15          RESERVED        # Pointing mask #15
IS_KuPtgMask16          RESERVED        # Pointing mask #16
IS_KuPtgMask17          RESERVED        # Pointing mask #17
IS_KuPtgMask18          RESERVED        # Pointing mask #18
IS_KuPtgMask19          RESERVED        # Pointing mask #19
IS_KuPtgMask20          RESERVED        # Pointing mask #20

IS_KuLowerELMask1       LADP01MDCWXCU   # Lower El Mask Limit 1
IS_KuUpperELMask1       LADP01MDCWXDU   # Upper El Mask Limit 1
IS_KuLowerXELMask1      LADP01MDCWXEU   # Lower XEl Mask Limit 1
IS_KuUpperXELMask1      LADP01MDCWXFU   # Upper XEl Mask Limit 1

IS_KuLowerELMask2       LADP01MDCWXGU   # Lower El Mask Limit 2
IS_KuUpperELMask2       LADP01MDCWXHU   # Upper El Mask Limit 2
IS_KuLowerXELMask2      LADP01MDCWXIU   # Lower XEl Mask Limit 2
IS_KuUpperXELMask2      LADP01MDCWXJU   # Upper XEl Mask Limit 2

IS_KuLowerELMask3       LADP01MDCWXKU   # Lower El Mask Limit 3
IS_KuUpperELMask3       LADP01MDCWXLU   # Upper El Mask Limit 3
IS_KuLowerXELMask3      LADP01MDCWXMU   # Lower XEl Mask Limit 3
IS_KuUpperXELMask3      LADP01MDCWXNU   # Upper XEl Mask Limit 3

IS_KuLowerELMask4       LADP01MDCWXOU   # Lower El Mask Limit 4
IS_KuUpperELMask4       LADP01MDCWXPU   # Upper El Mask Limit 4
IS_KuLowerXELMask4      LADP01MDCWXQU   # Lower XEl Mask Limit 4
IS_KuUpperXELMask4      LADP01MDCWXRU   # Upper XEl Mask Limit 4

IS_KuLowerELMask5       LADP01MDCWXSU   # Lower El Mask Limit 5
IS_KuUpperELMask5       LADP01MDCWXTU   # Upper El Mask Limit 5
IS_KuLowerXELMask5      LADP01MDCWXUU   # Lower XEl Mask Limit 5
IS_KuUpperXELMask5      LADP01MDCWXVU   # Upper XEl Mask Limit 5

IS_KuLowerELMask6       LADP01MDCWXWU   # Lower El Mask Limit 6
IS_KuUpperELMask6       LADP01MDCWXXU   # Upper El Mask Limit 6
IS_KuLowerXELMask6      LADP01MDCWXYU   # Lower XEl Mask Limit 6
IS_KuUpperXELMask6      LADP01MDCWXZU   # Upper XEl Mask Limit 6

IS_KuLowerELMask7       LADP01MDCWY0U   # Lower El Mask Limit 7
IS_KuUpperELMask7       LADP01MDCWY1U   # Upper El Mask Limit 7
IS_KuLowerXELMask7      LADP01MDCWY2U   # Lower XEl Mask Limit 7
IS_KuUpperXELMask7      LADP01MDCWY3U   # Upper XEl Mask Limit 7

IS_KuLowerELMask8       LADP01MDDAP7U        # Lower El Mask Limit 8
IS_KuUpperELMask8       LADP01MDDAP8U        # Upper El Mask Limit 8
IS_KuLowerXELMask8      LADP01MDDAP9U        # Lower XEl Mask Limit 8
IS_KuUpperXELMask8      LADP01MDDAPAU        # Upper XEl Mask Limit 8

IS_KuLowerELMask9       LADP01MDDAPBU        # Lower El Mask Limit 9
IS_KuUpperELMask9       LADP01MDDAPCU        # Upper El Mask Limit 9
IS_KuLowerXELMask9      LADP01MDDAPDU        # Lower XEl Mask Limit 9
IS_KuUpperXELMask9      LADP01MDDAPEU        # Upper XEl Mask Limit 9

IS_KuLowerELMask10      LADP01MDDAPFU        # Lower El Mask Limit 10
IS_KuUpperELMask10      LADP01MDDAPGU        # Upper El Mask Limit 10
IS_KuLowerXELMask10     LADP01MDDAPHU        # Lower XEl Mask Limit 10
IS_KuUpperXELMask10     LADP01MDDAPIU        # Upper XEl Mask Limit 10

IS_KuLowerELMask11      RESERVED        # Lower El Mask Limit 11
IS_KuUpperELMask11      RESERVED        # Upper El Mask Limit 11
IS_KuLowerXELMask11     RESERVED        # Lower XEl Mask Limit 11
IS_KuUpperXELMask11     RESERVED        # Upper XEl Mask Limit 11

IS_KuLowerELMask12      RESERVED        # Lower El Mask Limit 12
IS_KuUpperELMask12      RESERVED        # Upper El Mask Limit 12
IS_KuLowerXELMask12     RESERVED        # Lower XEl Mask Limit 12
IS_KuUpperXELMask12     RESERVED        # Upper XEl Mask Limit 12

IS_KuLowerELMask13      RESERVED        # Lower El Mask Limit 13
IS_KuUpperELMask13      RESERVED        # Upper El Mask Limit 13
IS_KuLowerXELMask13     RESERVED        # Lower XEl Mask Limit 13
IS_KuUpperXELMask13     RESERVED        # Upper XEl Mask Limit 13

IS_KuLowerELMask14      RESERVED        # Lower El Mask Limit 14
IS_KuUpperELMask14      RESERVED        # Upper El Mask Limit 14
IS_KuLowerXELMask14     RESERVED        # Lower XEl Mask Limit 14
IS_KuUpperXELMask14     RESERVED        # Upper XEl Mask Limit 14

IS_KuLowerELMask15      RESERVED        # Lower El Mask Limit 15
IS_KuUpperELMask15      RESERVED        # Upper El Mask Limit 15
IS_KuLowerXELMask15     RESERVED        # Lower XEl Mask Limit 15
IS_KuUpperXELMask15     RESERVED        # Upper XEl Mask Limit 15

IS_KuLowerELMask16      RESERVED        # Lower El Mask Limit 16
IS_KuUpperELMask16      RESERVED        # Upper El Mask Limit 16
IS_KuLowerXELMask16     RESERVED        # Lower XEl Mask Limit 16
IS_KuUpperXELMask16     RESERVED        # Upper XEl Mask Limit 16

IS_KuLowerELMask17      RESERVED        # Lower El Mask Limit 17
IS_KuUpperELMask17      RESERVED        # Upper El Mask Limit 17
IS_KuLowerXELMask17     RESERVED        # Lower XEl Mask Limit 17
IS_KuUpperXELMask17     RESERVED        # Upper XEl Mask Limit 17

IS_KuLowerELMask18      RESERVED        # Lower El Mask Limit 18
IS_KuUpperELMask18      RESERVED        # Upper El Mask Limit 18
IS_KuLowerXELMask18     RESERVED        # Lower XEl Mask Limit 18
IS_KuUpperXELMask18     RESERVED        # Upper XEl Mask Limit 18

IS_KuLowerELMask19      RESERVED        # Lower El Mask Limit 19
IS_KuUpperELMask19      RESERVED        # Upper El Mask Limit 19
IS_KuLowerXELMask19     RESERVED        # Lower XEl Mask Limit 19
IS_KuUpperXELMask19     RESERVED        # Upper XEl Mask Limit 19

IS_KuLowerELMask20      RESERVED        # Lower El Mask Limit 20
IS_KuUpperELMask20      RESERVED        # Upper El Mask Limit 20
IS_KuLowerXELMask20     RESERVED        # Lower XEl Mask Limit 20
IS_KuUpperXELMask20     RESERVED        # Upper XEl Mask Limit 20
