/* <TITLE> TEXT2PS Convert plain text to postscript</TITLE> */
/*
From: wade@dax.asub.arknet.edu
Date: 21 May 96 20:56:32 GMT
*/
/*
 *  text2ps
 *
 *  Convert plain text to postscript
 *  - Stephen Frede, UNSW, Australia
 */
#define VERSION     "V1.1"

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <locale.h>
#include <fcntl.h>
#include <ctype.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/text2ps/text2ps.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


#ifdef G_OS_WIN32
    #include <windows.h>
    #include <io.h>
#else
    #include <unistd.h>
    #include <sys/param.h>
#endif

#define NEW_PAGE    014         /* ^L forces a new page */
#define TABSIZE 8
#define CHARS_PER_LINE 77

#define LINE_X_POSITION 5
#define LINE_HEIGHT 13

#define PAGE_BEGIN 720
#define PAGE_END 23

#define PAGE_WIDTH 612
#define PAGE_HEIGHT 792

static void usage(void);
static void Text2PS(FILE *ifp, FILE *ofp, char *filename, char *title);
static int get_page_count(FILE *ofp);
static void write_wrapped_indicator(int line_position, FILE *ofp);
static void write_header(FILE *ofp);
static void write_trailer(int pagenum, FILE *ofp);
static void write_line_start(int *line_position, int *pagenum,
                             int total_pages, char *header, char *modstr, FILE *ofp);
static void write_line_end(FILE *ofp);
static void write_page_start(int pagenum, int total_pages, char *header, char *modstr, FILE *ofp);
static void write_char(int ch, int *ch_count, int *line_position, int pagenum,
                       int total_pages, char *header, char *modstr, FILE *ofp);
static void write_page_end(FILE *ofp);

/**
 * Does the conversion of text file to postscript file.
 * @param ifp input file
 * @param ofp output file
 * @param filename the input filename
 * @param title optional title, if null, the filename is used
 */
static void Text2PS(FILE *ifp, FILE *ofp, char *filename, char *title)
{
    char ch;
    int ch_count = 0;
    int line_position = PAGE_BEGIN;
    int pagenum = 1;
    int total_pages = 0;
    char *modstr;

    char *header = NULL;

    time_t t;
    time(&t);
    modstr = ctime(&t);

    if (title != NULL)
    {
        header = title;
    }
    else
    {
        header = g_path_get_basename(filename);
    }

    /* preprocess file to get total page count */
    total_pages = get_page_count(ifp);

    /* print a page and start a new one */
    write_header(ofp);
    write_page_start(pagenum, total_pages, header, modstr, ofp);
    pagenum++;

    write_line_start(&line_position, &pagenum,
                     total_pages, header, modstr, ofp);
    while ((ch=getc(ifp)) != EOF)
    {
        ch_count++;

        /* validate the character */
        if (ch < ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != NEW_PAGE)
        {
            ch = '?';
        }

        /* tab */
        if (ch == '\t')
        {
            int i;
            for (i=0; i<TABSIZE; i++)
            {
                write_char(' ', &ch_count, &line_position, pagenum,
                           total_pages, header, modstr, ofp);
                ch_count++;
            }
        }
        /* newline */
        else if (ch == '\n' || ch == '\r')
        {
            write_line_end(ofp);
            ch_count = 0;
            write_line_start(&line_position, &pagenum,
                             total_pages, header, modstr, ofp);
        }
        /* ctrl L */
        else if (ch == NEW_PAGE)
        {
            write_line_end(ofp);
            ch_count = 0;

            write_page_end(ofp);
            write_page_start(pagenum, total_pages, header, modstr, ofp);
            write_line_start(&line_position, &pagenum,
                             total_pages, header, modstr, ofp);

            line_position = PAGE_BEGIN;
            pagenum++;
        }
        /* process words */
        else
        {
            write_char(ch, &ch_count, &line_position, pagenum,
                       total_pages, header, modstr, ofp);
        }
    }
    write_line_end(ofp);
    write_page_end(ofp);
    write_trailer(pagenum-1, ofp);
}

/**
 * Outputs the character to the output file.
 * If the character exceeds the right margin a wrap character is
 * printed indicating that the text continues on the next line.
 * @param ch print this character
 * @param ch_count the current character count for this line
 * @param line_position y position on the page
 * @param pagenum the current page we're on
 * @param total_pages total page count
 * @param header header used at top of each new page
 * @param modstr time of this print
 * @param ofp output file pointer
 */
static void write_char(int ch, int *ch_count, int *line_position, int pagenum,
                       int total_pages, char *header, char *modstr, FILE *ofp)
{
    /* try and output punctuation chars if enough room exists */
    if (((ispunct(ch)) && (*ch_count) > CHARS_PER_LINE+4) ||
        (*ch_count) > CHARS_PER_LINE)
    {
        /* need to write out the wrapped line indicator */
        /* start new line of text */
        write_line_end(ofp);
        write_wrapped_indicator((*line_position)+LINE_HEIGHT-5, ofp);

        *ch_count = 1;

        write_line_start(line_position, &pagenum,
                         total_pages, header, modstr, ofp);
    }

    if (ch < ' ' || ch > '~')
    {
        fprintf(ofp, "\\%3.3o", ch);
    }
    else
    {
        if (ch == '(' || ch == ')' || ch == '\\')
        {
            putc('\\', ofp);
        }
        putc(ch, ofp);
    }
}

/**
 * Writes a wrap character at the specified y position.
 * First parameter is the x position, the end of 552 seems to
 * work. 15 15 is the width and height of the graphic. And 3 is
 * the graphic style, it seems that this works for the arrow.
 * @param y_position y position to write the wrap character at.
 * @param ofp output file pointer
 */
static void write_wrapped_indicator(int y_position, FILE *ofp)
{
    /* 552 y 15 15 3 wrapped_line_mark */
    fprintf(ofp, "552 %d 15 15 3 wrapped_line_mark\n", y_position);
}

/**
 * Writes the opening parenthesis for a line of text. Prior to
 * writing the paren, the line position is checked to see if the
 * line can be written to the page or not. If the line won't
 * fit, then a new page is started.
 * @param line_position
 * @param pagenum
 * @param total_pages
 * @param header
 * @param modstr
 * @param ofp
 */
static void write_line_start(int *line_position, int *pagenum,
                             int total_pages, char *header, char *modstr, FILE *ofp)
{
    if ((*line_position) < PAGE_END)
    {
        write_page_end(ofp);
        write_page_start(*pagenum, total_pages, header, modstr, ofp);
        *line_position = PAGE_BEGIN;
        (*pagenum)++;
    }

    fprintf(ofp, "%d %d M\n", LINE_X_POSITION, *line_position);
    putc('(', ofp);
    *line_position -= LINE_HEIGHT;
}

/**
 * Closes out the current line with the closing parenthesis.
 * @param ofp
 */
static void write_line_end(FILE *ofp)
{
    fprintf(ofp, ") s\n");
}

/**
 * Writes a new page.
 * @param pagenum the current page number
 * @param total_pages total pages for this document
 * @param fname the filename being processed
 * @param modstr time this document was created
 * @param ofp output file pointer
 */
static void write_page_start(int pagenum, int total_pages, char *fname, char *modstr, FILE *ofp)
{
    char *basename = g_path_get_basename(fname);

    fprintf(ofp, "%%%%Page: (%d) %d\n", pagenum, pagenum);
    fprintf(ofp, "%%%%BeginPageSetup\n");
    fprintf(ofp, "_S\n");
    fprintf(ofp, "24 24 translate\n");
    fprintf(ofp, "/pagenum %d def\n", pagenum);
    fprintf(ofp, "%% User defined strings:\n");
    fprintf(ofp, "/fmodstr (%s) def\n", modstr);
    fprintf(ofp, "/pagenumstr (%d) def\n", pagenum);
    fprintf(ofp, "/user_header_p true def\n");
    fprintf(ofp, "/user_header_left_str (%s) def\n", modstr);
    fprintf(ofp, "/user_header_center_str (%s) def\n", basename);
    fprintf(ofp, "/user_header_right_str (Page %d of %d) def\n", pagenum, total_pages);
    fprintf(ofp, "/user_footer_p false def\n");
    fprintf(ofp, "%%%%EndPageSetup\n");
    fprintf(ofp, "do_header\n");
}

/**
 * Writes the closing postscript macros to close out a page.
 * @param ofp output file pointer
 */
static void write_page_end(FILE *ofp)
{
    fprintf(ofp, "_R\n");
    fprintf(ofp, "S\n");
}

/**
 * Writes the postscript trailer which includes the number of
 * pages this document contains.
 * @param pagenum page number or total pages
 * @param ofp output file pointer
 */
static void write_trailer(int pagenum, FILE *ofp)
{
    fprintf(ofp, "%%%%Trailer\n");
    fprintf(ofp, "%%%%Pages: %d\n", pagenum);
    fprintf(ofp, "%%%%DocumentNeededResources: font Courier Times-Italic\n");
    fprintf(ofp, "%%%%EOF\n");
}

/**
 * Reads the input file and determines how many pages will be
 * created. After processing the input file, it is rewinded.
 * @param ifp input file pointer
 *
 * @return int total page count
 */
static int get_page_count(FILE *ifp)
{
    char ch;
    int ch_count = 0;
    int line_position = PAGE_BEGIN;
    int pagenum = 1;


    while ((ch=getc(ifp)) != EOF)
    {
        ch_count++;

        /* validate the character */
        if (ch < ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != NEW_PAGE)
        {
            ch = '?';
        }

        /* tab */
        if (ch == '\t')
        {
            int i;

            for (i=0; i<TABSIZE; i++)
            {
                /* will the char fit the page ? */
                if (ch_count > CHARS_PER_LINE)
                {
                    ch_count = 1;

                    if (line_position < PAGE_END)
                    {
                        line_position = PAGE_BEGIN;
                        pagenum++;
                    }
                }
                line_position -= LINE_HEIGHT;
                ch_count++;
            }
        }
        /* newline */
        else if (ch == '\n' || ch == '\r')
        {
            ch_count = 0;
            if (line_position < PAGE_END)
            {
                line_position = PAGE_BEGIN;
                pagenum++;
            }
            line_position -= LINE_HEIGHT;
        }
        /* ctrl L */
        else if (ch == NEW_PAGE)
        {
            ch_count = 0;
            line_position = PAGE_BEGIN;
            pagenum++;
        }
        /* process words */
        else
        {
            if (ch_count > CHARS_PER_LINE)
            {
                ch_count = 1;
                if (line_position < PAGE_END)
                {
                    line_position = PAGE_BEGIN;
                    pagenum++;
                }
                line_position -= LINE_HEIGHT;
            }
        }
    }

    rewind(ifp);

    return pagenum;
}

/**
 * Writes the postscript definition and macros.
 * @param ofp output file pointer
 */
static void write_header(FILE *ofp)
{
    fprintf(ofp, "%%!PS-Adobe-3.0\n");
    fprintf(ofp, "%%%%BoundingBox: 0 0 %d %d\n", PAGE_WIDTH, PAGE_HEIGHT);
    fprintf(ofp, "%%%%Title: Enscript Output\n");
    fprintf(ofp, "%%%%For: Apache user\n");
    fprintf(ofp, "%%%%Creator: GNU enscript 1.6.3\n");
    fprintf(ofp, "%%%%CreationDate: Wed Nov 29 00:56:55 2006\n");
    fprintf(ofp, "%%%%Orientation: Portrait\n");
    fprintf(ofp, "%%%%Pages: (atend)\n");
    fprintf(ofp, "%%%%DocumentMedia: Letter %d %d 0 () ()\n", PAGE_WIDTH, PAGE_HEIGHT);
    fprintf(ofp, "%%%%DocumentNeededResources: (atend)\n");
    fprintf(ofp, "%%%%EndComments\n");
    fprintf(ofp, "%%%%BeginProlog\n");
    fprintf(ofp, "%%%%BeginResource: procset Enscript-Prolog 1.6 3\n");
    fprintf(ofp, "%%\n");
    fprintf(ofp, "%% Procedures.\n");
    fprintf(ofp, "%%\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/_S {   %% save current state\n");
    fprintf(ofp, "  /_s save def\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "/_R {   %% restore from saved state\n");
    fprintf(ofp, "  _s restore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/S {    %% showpage protecting gstate\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "  showpage\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} bind def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/MF {   %% fontname newfontname -> -     make a new encoded font\n");
    fprintf(ofp, "  /newfontname exch def\n");
    fprintf(ofp, "  /fontname exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  /fontdict fontname findfont def\n");
    fprintf(ofp, "  /newfont fontdict maxlength dict def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  fontdict {\n");
    fprintf(ofp, "    exch\n");
    fprintf(ofp, "    dup /FID eq {\n");
    fprintf(ofp, "      %% skip FID pair\n");
    fprintf(ofp, "      pop pop\n");
    fprintf(ofp, "    } {\n");
    fprintf(ofp, "      %% copy to the new font dictionary\n");
    fprintf(ofp, "      exch newfont 3 1 roll put\n");
    fprintf(ofp, "    } ifelse\n");
    fprintf(ofp, "  } forall\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  newfont /FontName newfontname put\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  %% insert only valid encoding vectors\n");
    fprintf(ofp, "  encoding_vector length 256 eq {\n");
    fprintf(ofp, "    newfont /Encoding encoding_vector put\n");
    fprintf(ofp, "  } if\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  newfontname newfont definefont pop\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/MF_PS { %% fontname newfontname -> -    make a new font preserving its enc\n");
    fprintf(ofp, "  /newfontname exch def\n");
    fprintf(ofp, "  /fontname exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  /fontdict fontname findfont def\n");
    fprintf(ofp, "  /newfont fontdict maxlength dict def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  fontdict {\n");
    fprintf(ofp, "    exch\n");
    fprintf(ofp, "    dup /FID eq {\n");
    fprintf(ofp, "      %% skip FID pair\n");
    fprintf(ofp, "      pop pop\n");
    fprintf(ofp, "    } {\n");
    fprintf(ofp, "      %% copy to the new font dictionary\n");
    fprintf(ofp, "      exch newfont 3 1 roll put\n");
    fprintf(ofp, "    } ifelse\n");
    fprintf(ofp, "  } forall\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  newfont /FontName newfontname put\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  newfontname newfont definefont pop\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/SF { %% fontname width height -> -      set a new font\n");
    fprintf(ofp, "  /height exch def\n");
    fprintf(ofp, "  /width exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  findfont\n");
    fprintf(ofp, "  [width 0 0 height 0 0] makefont setfont\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/SUF { %% fontname width height -> -     set a new user font\n");
    fprintf(ofp, "  /height exch def\n");
    fprintf(ofp, "  /width exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  /F-gs-user-font MF\n");
    fprintf(ofp, "  /F-gs-user-font width height SF\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/SUF_PS { %% fontname width height -> -  set a new user font preserving its enc\n");
    fprintf(ofp, "  /height exch def\n");
    fprintf(ofp, "  /width exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  /F-gs-user-font MF_PS\n");
    fprintf(ofp, "  /F-gs-user-font width height SF\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/M {moveto} bind def\n");
    fprintf(ofp, "/s {show} bind def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/Box {  %% x y w h -> -                  define box path\n");
    fprintf(ofp, "  /d_h exch def /d_w exch def /d_y exch def /d_x exch def\n");
    fprintf(ofp, "  d_x d_y  moveto\n");
    fprintf(ofp, "  d_w 0 rlineto\n");
    fprintf(ofp, "  0 d_h rlineto\n");
    fprintf(ofp, "  d_w neg 0 rlineto\n");
    fprintf(ofp, "  closepath\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/bgs {  %% x y height blskip gray str -> -       show string with bg color\n");
    fprintf(ofp, "  /str exch def\n");
    fprintf(ofp, "  /gray exch def\n");
    fprintf(ofp, "  /blskip exch def\n");
    fprintf(ofp, "  /height exch def\n");
    fprintf(ofp, "  /y exch def\n");
    fprintf(ofp, "  /x exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    x y blskip sub str stringwidth pop height Box\n");
    fprintf(ofp, "    gray setgray\n");
    fprintf(ofp, "    fill\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "  x y M str s\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/bgcs { %% x y height blskip red green blue str -> -  show string with bg color\n");
    fprintf(ofp, "  /str exch def\n");
    fprintf(ofp, "  /blue exch def\n");
    fprintf(ofp, "  /green exch def\n");
    fprintf(ofp, "  /red exch def\n");
    fprintf(ofp, "  /blskip exch def\n");
    fprintf(ofp, "  /height exch def\n");
    fprintf(ofp, "  /y exch def\n");
    fprintf(ofp, "  /x exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    x y blskip sub str stringwidth pop height Box\n");
    fprintf(ofp, "    red green blue setrgbcolor\n");
    fprintf(ofp, "    fill\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "  x y M str s\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Highlight bars.\n");
    fprintf(ofp, "/highlight_bars {       %% nlines lineheight output_y_margin gray -> -\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    setgray\n");
    fprintf(ofp, "    /ymarg exch def\n");
    fprintf(ofp, "    /lineheight exch def\n");
    fprintf(ofp, "    /nlines exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "    %% This 2 is just a magic number to sync highlight lines to text.\n");
    fprintf(ofp, "    0 d_header_y ymarg sub 2 sub translate\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "    /cw d_output_w cols div def\n");
    fprintf(ofp, "    /nrows d_output_h ymarg 2 mul sub lineheight div cvi def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "    %% for each column\n");
    fprintf(ofp, "    0 1 cols 1 sub {\n");
    fprintf(ofp, "      cw mul /xp exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "      %% for each rows\n");
    fprintf(ofp, "      0 1 nrows 1 sub {\n");
    fprintf(ofp, "        /rn exch def\n");
    fprintf(ofp, "        rn lineheight mul neg /yp exch def\n");
    fprintf(ofp, "        rn nlines idiv 2 mod 0 eq {\n");
    fprintf(ofp, "          %% Draw highlight bar.  4 is just a magic indentation.\n");
    fprintf(ofp, "          xp 4 add yp cw 8 sub lineheight neg Box fill\n");
    fprintf(ofp, "        } if\n");
    fprintf(ofp, "      } for\n");
    fprintf(ofp, "    } for\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Line highlight bar.\n");
    fprintf(ofp, "/line_highlight {       %% x y width height gray -> -\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    /gray exch def\n");
    fprintf(ofp, "    Box gray setgray fill\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Column separator lines.\n");
    fprintf(ofp, "/column_lines {\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    .1 setlinewidth\n");
    fprintf(ofp, "    0 d_footer_h translate\n");
    fprintf(ofp, "    /cw d_output_w cols div def\n");
    fprintf(ofp, "    1 1 cols 1 sub {\n");
    fprintf(ofp, "      cw mul 0 moveto\n");
    fprintf(ofp, "      0 d_output_h rlineto stroke\n");
    fprintf(ofp, "    } for\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Column borders.\n");
    fprintf(ofp, "/column_borders {\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    .1 setlinewidth\n");
    fprintf(ofp, "    0 d_footer_h moveto\n");
    fprintf(ofp, "    0 d_output_h rlineto\n");
    fprintf(ofp, "    d_output_w 0 rlineto\n");
    fprintf(ofp, "    0 d_output_h neg rlineto\n");
    fprintf(ofp, "    closepath stroke\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Do the actual underlay drawing\n");
    fprintf(ofp, "/draw_underlay {\n");
    fprintf(ofp, "  ul_style 0 eq {\n");
    fprintf(ofp, "    ul_str true charpath stroke\n");
    fprintf(ofp, "  } {\n");
    fprintf(ofp, "    ul_str show\n");
    fprintf(ofp, "  } ifelse\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Underlay\n");
    fprintf(ofp, "/underlay {     %% - -> -\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    0 d_page_h translate\n");
    fprintf(ofp, "    d_page_h neg d_page_w atan rotate\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "    ul_gray setgray\n");
    fprintf(ofp, "    ul_font setfont\n");
    fprintf(ofp, "    /dw d_page_h dup mul d_page_w dup mul add sqrt def\n");
    fprintf(ofp, "    ul_str stringwidth pop dw exch sub 2 div ul_h_ptsize -2 div moveto\n");
    fprintf(ofp, "    draw_underlay\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/user_underlay {        %% - -> -\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    ul_x ul_y translate\n");
    fprintf(ofp, "    ul_angle rotate\n");
    fprintf(ofp, "    ul_gray setgray\n");
    fprintf(ofp, "    ul_font setfont\n");
    fprintf(ofp, "    0 0 ul_h_ptsize 2 div sub moveto\n");
    fprintf(ofp, "    draw_underlay\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Page prefeed\n");
    fprintf(ofp, "/page_prefeed {         %% bool -> -\n");
    fprintf(ofp, "  statusdict /prefeed known {\n");
    fprintf(ofp, "    statusdict exch /prefeed exch put\n");
    fprintf(ofp, "  } {\n");
    fprintf(ofp, "    pop\n");
    fprintf(ofp, "  } ifelse\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Wrapped line markers\n");
    fprintf(ofp, "/wrapped_line_mark {    %% x y charwith charheight type -> -\n");
    fprintf(ofp, "  /type exch def\n");
    fprintf(ofp, "  /h exch def\n");
    fprintf(ofp, "  /w exch def\n");
    fprintf(ofp, "  /y exch def\n");
    fprintf(ofp, "  /x exch def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  type 2 eq {\n");
    fprintf(ofp, "    %% Black boxes (like TeX does)\n");
    fprintf(ofp, "    gsave\n");
    fprintf(ofp, "      0 setlinewidth\n");
    fprintf(ofp, "      x w 4 div add y M\n");
    fprintf(ofp, "      0 h rlineto w 2 div 0 rlineto 0 h neg rlineto\n");
    fprintf(ofp, "      closepath fill\n");
    fprintf(ofp, "    grestore\n");
    fprintf(ofp, "  } {\n");
    fprintf(ofp, "    type 3 eq {\n");
    fprintf(ofp, "      %% Small arrows\n");
    fprintf(ofp, "      gsave\n");
    fprintf(ofp, "        .2 setlinewidth\n");
    fprintf(ofp, "        x w 2 div add y h 2 div add M\n");
    fprintf(ofp, "        w 4 div 0 rlineto\n");
    fprintf(ofp, "        x w 4 div add y lineto stroke\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "        x w 4 div add w 8 div add y h 4 div add M\n");
    fprintf(ofp, "        x w 4 div add y lineto\n");
    fprintf(ofp, "        w 4 div h 8 div rlineto stroke\n");
    fprintf(ofp, "      grestore\n");
    fprintf(ofp, "    } {\n");
    fprintf(ofp, "      %% do nothing\n");
    fprintf(ofp, "    } ifelse\n");
    fprintf(ofp, "  } ifelse\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% EPSF import.\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/BeginEPSF {\n");
    fprintf(ofp, "  /b4_Inc_state save def                %% Save state for cleanup\n");
    fprintf(ofp, "  /dict_count countdictstack def        %% Count objects on dict stack\n");
    fprintf(ofp, "  /op_count count 1 sub def             %% Count objects on operand stack\n");
    fprintf(ofp, "  userdict begin\n");
    fprintf(ofp, "  /showpage { } def\n");
    fprintf(ofp, "  0 setgray 0 setlinecap\n");
    fprintf(ofp, "  1 setlinewidth 0 setlinejoin\n");
    fprintf(ofp, "  10 setmiterlimit [ ] 0 setdash newpath\n");
    fprintf(ofp, "  /languagelevel where {\n");
    fprintf(ofp, "    pop languagelevel\n");
    fprintf(ofp, "    1 ne {\n");
    fprintf(ofp, "      false setstrokeadjust false setoverprint\n");
    fprintf(ofp, "    } if\n");
    fprintf(ofp, "  } if\n");
    fprintf(ofp, "} bind def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/EndEPSF {\n");
    fprintf(ofp, "  count op_count sub { pos } repeat     %% Clean up stacks\n");
    fprintf(ofp, "  countdictstack dict_count sub { end } repeat\n");
    fprintf(ofp, "  b4_Inc_state restore\n");
    fprintf(ofp, "} bind def\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "%% Check PostScript language level.\n");
    fprintf(ofp, "/languagelevel where {\n");
    fprintf(ofp, "  pop /gs_languagelevel languagelevel def\n");
    fprintf(ofp, "} {\n");
    fprintf(ofp, "  /gs_languagelevel 1 def\n");
    fprintf(ofp, "} ifelse\n");
    fprintf(ofp, "%%%%EndResource\n");
    fprintf(ofp, "%%%%BeginResource: procset Enscript-Encoding-88591 1.6 3\n");
    fprintf(ofp, "/encoding_vector [\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/space          /exclam         /quotedbl       /numbersign\n");
    fprintf(ofp, "/dollar         /percent        /ampersand      /quoteright\n");
    fprintf(ofp, "/parenleft      /parenright     /asterisk       /plus\n");
    fprintf(ofp, "/comma          /hyphen         /period         /slash\n");
    fprintf(ofp, "/zero           /one            /two            /three\n");
    fprintf(ofp, "/four           /five           /six            /seven\n");
    fprintf(ofp, "/eight          /nine           /colon          /semicolon\n");
    fprintf(ofp, "/less           /equal          /greater        /question\n");
    fprintf(ofp, "/at             /A              /B              /C\n");
    fprintf(ofp, "/D              /E              /F              /G\n");
    fprintf(ofp, "/H              /I              /J              /K\n");
    fprintf(ofp, "/L              /M              /N              /O\n");
    fprintf(ofp, "/P              /Q              /R              /S\n");
    fprintf(ofp, "/T              /U              /V              /W\n");
    fprintf(ofp, "/X              /Y              /Z              /bracketleft\n");
    fprintf(ofp, "/backslash      /bracketright   /asciicircum    /underscore\n");
    fprintf(ofp, "/quoteleft      /a              /b              /c\n");
    fprintf(ofp, "/d              /e              /f              /g\n");
    fprintf(ofp, "/h              /i              /j              /k\n");
    fprintf(ofp, "/l              /m              /n              /o\n");
    fprintf(ofp, "/p              /q              /r              /s\n");
    fprintf(ofp, "/t              /u              /v              /w\n");
    fprintf(ofp, "/x              /y              /z              /braceleft\n");
    fprintf(ofp, "/bar            /braceright     /tilde          /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/.notdef        /.notdef        /.notdef        /.notdef\n");
    fprintf(ofp, "/space          /exclamdown     /cent           /sterling\n");
    fprintf(ofp, "/currency       /yen            /brokenbar      /section\n");
    fprintf(ofp, "/dieresis       /copyright      /ordfeminine    /guillemotleft\n");
    fprintf(ofp, "/logicalnot     /hyphen         /registered     /macron\n");
    fprintf(ofp, "/degree         /plusminus      /twosuperior    /threesuperior\n");
    fprintf(ofp, "/acute          /mu             /paragraph      /bullet\n");
    fprintf(ofp, "/cedilla        /onesuperior    /ordmasculine   /guillemotright\n");
    fprintf(ofp, "/onequarter     /onehalf        /threequarters  /questiondown\n");
    fprintf(ofp, "/Agrave         /Aacute         /Acircumflex    /Atilde\n");
    fprintf(ofp, "/Adieresis      /Aring          /AE             /Ccedilla\n");
    fprintf(ofp, "/Egrave         /Eacute         /Ecircumflex    /Edieresis\n");
    fprintf(ofp, "/Igrave         /Iacute         /Icircumflex    /Idieresis\n");
    fprintf(ofp, "/Eth            /Ntilde         /Ograve         /Oacute\n");
    fprintf(ofp, "/Ocircumflex    /Otilde         /Odieresis      /multiply\n");
    fprintf(ofp, "/Oslash         /Ugrave         /Uacute         /Ucircumflex\n");
    fprintf(ofp, "/Udieresis      /Yacute         /Thorn          /germandbls\n");
    fprintf(ofp, "/agrave         /aacute         /acircumflex    /atilde\n");
    fprintf(ofp, "/adieresis      /aring          /ae             /ccedilla\n");
    fprintf(ofp, "/egrave         /eacute         /ecircumflex    /edieresis\n");
    fprintf(ofp, "/igrave         /iacute         /icircumflex    /idieresis\n");
    fprintf(ofp, "/eth            /ntilde         /ograve         /oacute\n");
    fprintf(ofp, "/ocircumflex    /otilde         /odieresis      /divide\n");
    fprintf(ofp, "/oslash         /ugrave         /uacute         /ucircumflex\n");
    fprintf(ofp, "/udieresis      /yacute         /thorn          /ydieresis\n");
    fprintf(ofp, "] def\n");
    fprintf(ofp, "%%%%EndResource\n");
    fprintf(ofp, "%%%%EndProlog\n");
    fprintf(ofp, "%%%%BeginSetup\n");
    fprintf(ofp, "%%%%IncludeResource: font Courier\n");
    fprintf(ofp, "%%%%IncludeResource: font Times-Italic\n");
    fprintf(ofp, "/HFpt_w 10 def\n");
    fprintf(ofp, "/HFpt_h 10 def\n");
    fprintf(ofp, "/Times-Italic /HF-gs-font MF\n");
    fprintf(ofp, "/HF /HF-gs-font findfont [HFpt_w 0 0 HFpt_h 0 0] makefont def\n");
    fprintf(ofp, "/Courier /F-gs-font MF\n");
    fprintf(ofp, "/F-gs-font 12 12 SF\n");
    fprintf(ofp, "/#copies 1 def\n");
    fprintf(ofp, "%% Pagedevice definitions:\n");
    fprintf(ofp, "gs_languagelevel 1 gt {\n");
    fprintf(ofp, "  <<\n");
    fprintf(ofp, "    /PageSize [%d %d]\n", PAGE_WIDTH, PAGE_HEIGHT);
    fprintf(ofp, "  >> setpagedevice\n");
    fprintf(ofp, "} if\n");
    fprintf(ofp, "%%%%BeginResource: procset Enscript-Header-simple 1.6 3\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "/do_header {    %% print default simple header\n");
    fprintf(ofp, "  gsave\n");
    fprintf(ofp, "    d_header_x d_header_y HFpt_h 3 div add translate\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "    HF setfont\n");
    fprintf(ofp, "    user_header_p {\n");
    fprintf(ofp, "      5 0 moveto user_header_left_str show\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "      d_header_w user_header_center_str stringwidth pop sub 2 div\n");
    fprintf(ofp, "      0 moveto user_header_center_str show\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "      d_header_w user_header_right_str stringwidth pop sub 5 sub\n");
    fprintf(ofp, "      0 moveto user_header_right_str show\n");
    fprintf(ofp, "    } {\n");
    fprintf(ofp, "      5 0 moveto fname show\n");
    fprintf(ofp, "      45 0 rmoveto fmodstr show\n");
    fprintf(ofp, "      45 0 rmoveto pagenumstr show\n");
    fprintf(ofp, "    } ifelse\n");
    fprintf(ofp, "\n");
    fprintf(ofp, "  grestore\n");
    fprintf(ofp, "} def\n");
    fprintf(ofp, "%%%%EndResource\n");
    fprintf(ofp, "/d_page_w 547 def\n");
    fprintf(ofp, "/d_page_h 794 def\n");
    fprintf(ofp, "/d_header_x 0 def\n");
    fprintf(ofp, "/d_header_y 740 def\n");
    fprintf(ofp, "/d_header_w 547 def\n");
    fprintf(ofp, "/d_header_h 15 def\n");
    fprintf(ofp, "/d_footer_x 0 def\n");
    fprintf(ofp, "/d_footer_y 0 def\n");
    fprintf(ofp, "/d_footer_w 547 def\n");
    fprintf(ofp, "/d_footer_h 0 def\n");
    fprintf(ofp, "/d_output_w 547 def\n");
    fprintf(ofp, "/d_output_h 779 def\n");
    fprintf(ofp, "/cols 1 def\n");
    fprintf(ofp, "%%%%EndSetup\n");
}

/**
 * The real main used for converting Text to Postscript.
 * @param argc count of arguments
 * @param argv the arguments
 * @return true or false
 */
int main(int argc, char **argv)
{
    static char *ofilename = NULL;
    FILE *ofp = stdout;

    static char *ifilename = NULL;
    FILE *ifp = stdin;

    static gboolean *help = NULL;
    static char *title = NULL;
    static char *version = NULL;

    static GOptionEntry entries[] =
    {
        {"help",    'h', 0, G_OPTION_ARG_NONE,   &help,         "Help",             NULL},
        {"title",   't', 0, G_OPTION_ARG_STRING, &title,        "Title",            NULL},
        {"version", 'v', 0, G_OPTION_ARG_NONE,   &version,      "Version",          NULL},
        {"input",   'i', 0, G_OPTION_ARG_STRING, &ifilename,    "Input filename",   NULL},
        {"output",  'o', 0, G_OPTION_ARG_STRING, &ofilename,    "Output filename",  NULL},
        { NULL}
    };

    GError *error = NULL;
    GOptionContext *context = g_option_context_new("- convert text to postscript");
    g_option_context_add_main_entries(context, entries, NULL);
    g_option_context_add_group(context, gtk_get_option_group(TRUE));
    g_option_context_parse(context, &argc, &argv, &error);

    if (help != NULL)
    {
        usage();
        return 0;
    }

    if (version != NULL)
    {
        fprintf(stdout, "Version: ");
    }

    if (ifilename != NULL)
    {
        ifp = fopen(ifilename, "r");
        if (ifp == NULL)
        {
            /* error */
            fprintf(stderr, "Unable to open input file: %s\n", ifilename);
            return 1;
        }
    }

    if (ofilename != NULL)
    {
        ofp = fopen(ofilename, "w");
        if (ofp == NULL)
        {
            /* error */
            fprintf(stderr, "Unable to open output file: %s\n", ofilename);
            fclose( ifp);
            return 1;
        }
    }

    Text2PS(ifp, ofp, ifilename, title);

    fclose(ifp);
    fclose(ofp);

    return 0;
}

/**
 * Outputs a usage message.
 */
static void usage()
{
    fprintf(stderr, "text2ps %s: convert TEXT files to PostScript Level 2.\n", VERSION);
    fprintf(stderr, "usage: text2ps textfile > psfile\n");
    fprintf(stderr, "-v[ersion] displays the version\n");
    fprintf(stderr, "-t[itle] <header> Header, placed center top\n");
    fprintf(stderr, "-i[nput] <filename> JPEG input filename\n");
    fprintf(stderr, "-o[utput] <filename> Postscript output filename\n");
}

#ifdef G_OS_WIN32
/**
 * Used to fake out windows into thinking this is a windows gui app
 * @param instance
 * @param prev_instance
 * @param command_line the command line args as a single string
 * @param show_command
 * @return True or False
 */
static void setargv(int *argcPtr, char ***argvPtr);
extern int main(int argc, char **argv);
int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, char *command_line, int show_command)
{
    int    argc;
    char** argv;
    char filename[MAX_PATH];
    int result;

    setlocale(LC_ALL, "C");
    setargv(&argc,  &argv);

    GetModuleFileName(NULL, filename, sizeof(filename));
    argv[0] = filename;    // call the user specified main function

    result = main(argc, argv);

    free(argv);
    return result;
}

/**
 * setargv --
 *
 *  Parse the Windows command line string into argc/argv.  Done here
 *  because we don't trust the builtin argument parser in crt0.
 *  Windows applications are responsible for breaking their command
 *  line into arguments.
 *
 *  2N backslashes + quote -> N backslashes + begin quoted string
 *  2N + 1 backslashes + quote -> literal
 *  N backslashes + non-quote -> literal
 *  quote + quote in a quoted string -> single quote
 *  quote + quote not in quoted string -> empty string
 *  quote -> begin quoted string
 *
 * Results:
 *  Fills argcPtr with the number of arguments and argvPtr with the
 *  array of arguments.
 *
 * Side effects:
 *  Memory allocated.
 *
 * @param argcPtr filled with the number of argument strings
 * @param argcvPtr filled with argument strings (malloc'd)
 */
static void setargv(int *argcPtr, char ***argvPtr)
{
    char *cmdLine, *p, *arg, *argSpace;
    char **argv;
    int argc, size, inquote, copy, slashes;

    cmdLine = GetCommandLine(); /* INTL: BUG */

    /*
     * Precompute an overly pessimistic guess at the number of arguments
     * in the command line by counting non-space spans.
     */

    size = 2;
    for (p = cmdLine; *p != '\0'; p++)
    {
        if ((*p == ' ') || (*p == '\t'))
        {  /* INTL: ISO space. */
            size++;
            while ((*p == ' ') || (*p == '\t'))
            { /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }
        }
    }

    /* get memory for arguments */
    argSpace = (char *) malloc((unsigned) (size * sizeof(char *) + strlen(cmdLine) + 1));
    if (argSpace != NULL)
    {
        argv = (char **) argSpace;
        argSpace += size * sizeof(char *);
        size--;

        p = cmdLine;
        for (argc = 0; argc < size; argc++)
        {
            argv[argc] = arg = argSpace;
            while ((*p == ' ') || (*p == '\t'))
            {   /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }

            inquote = 0;
            slashes = 0;
            while (1)
            {
                copy = 1;
                while (*p == '\\')
                {
                    slashes++;
                    p++;
                }
                if (*p == '"')
                {
                    if ((slashes & 1) == 0)
                    {
                        copy = 0;
                        if ((inquote) && (p[1] == '"'))
                        {
                            p++;
                            copy = 1;
                        }
                        else
                        {
                            inquote = !inquote;
                        }
                    }
                    slashes >>= 1;
                }

                while (slashes)
                {
                    *arg = '\\';
                    arg++;
                    slashes--;
                }

                if ((*p == '\0')
                    || (!inquote && ((*p == ' ') || (*p == '\t'))))
                { /* INTL: ISO space. */
                    break;
                }
                if (copy != 0)
                {
                    *arg = *p;
                    arg++;
                }
                p++;
            }
            *arg = '\0';
            argSpace = arg + 1;
        }
        argv[argc] = NULL;

        *argcPtr = argc;
        *argvPtr = argv;
    }
}
#endif /* G_OS_WIN32 */
