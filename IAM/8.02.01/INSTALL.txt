
#########################################################################
INSTALL.txt                                                 
#########################################################################
    
# Add a path reference to the BIG.txt in the Software Handover (SWHO) Memo.
#########################################################################    

CI Name: IAM
Version:  8.01.01 ( %VERSION )
UA_CMD_RELEASE: 1.00.02 ( %UA_CMD_RELEASE%)
Platform(s): el6.x86_64
Engineering/Developer Contact: David Pham
   Phone: 			281-792-2272

CI Identifier: (CI Number)      0000017237
Project Identifier: (Project ID) P092312
Software Lead: (Project lead's name)John Schan
WRs: (AR/SR number and definition)   
INC0024661 - IAM 8.0 Pop-up Hierachy Disables IAM Because it Hides a Pop-up

#########################################################################
INSTALLATION PROCEDURES
#########################################################################

The purpose of the installation instructions is to provide detailed 
information for how to install a new CI/version into an operational 
environment.
    
INSTALLATION/DELIVERABLE PRODUCTS:

       iam

INSTALLATION PREREQUISITES:

        Reference the Runtime dependencies described below.

DETAILED INSTALLATION PROCEDURES:

  A. Installation for MCC21

    1.Install the following deliverables into Ops History (OH) with the indicated file type:

       Source Path: /ms/scm/ua/asp/iam/<%VERSION%>/bin/el6.x86_64

        iam (exec)
        jpeg2ps (exec)
        text2ps (exec)

    2. The target OH installation location for the above file is:

       /jsc/ms/files/applications/el6.x86_64/asp/iam/<%VERSION%>/bin

    3. The deliverables are required to be accessible from 
       Ops History as indicated in the MOD Software Catalogue.

    4. A run script is used to specify the environment variables and shared libraries
       required at run-time.  The run script used is called “run_iam” and is uploaded
       into Ops History with the binary executable:

         /jsc/ms/files/applications/el6.x86_64/asp/iam/<%VERSION%>/ga/run_iam

    5. The version specific configuration set shall have the following properties:

       a. All deliverable files specified above are in this config set.
       b. The run_iam script is also included in this config set.
       c. The default target is: ga/run_iam
       d. The envVarName variable name is: IAM_BASE_PATH
       e. The basePath for this config set is
            /jsc/ms/files/applications/el6.x86_64/asp/IAM/<%VERSION%>/cset
       f. The deployTo path for this config set is: N/A
       g. The libcmduif_gtk.so shared library is included in this config set, as follows:

            <uri deployTo="lib64" type="lib" version=%latest%>/jsc/ms/files/applications/el6.x86_64/command/CmdLib/%UA_CMD_RELEASE%/lib64/libcmduif_gtk.so</uri>

       h. The OH installation location for this config set is:
            /jsc/ms/files/applications/el6.x86_64/asp/IAM/<%VERSION%>/cset/iam_cset.xml

    6. The default configuration set shall have the following properties:
       a. The version specific configuration set defined above is specified is included in this configuration set.
       b. The default execution target is: ga/run_iam
       c. The OH installation location for this config set is:
            /jsc/ms/files/applications/el6.x86_64/asp/IAM/cset/iam_cset.xml

  B. Installation for the External partners:

      Source Path: /ms/scm/ua/asp/iam/<%VERSION%>/bin-ep/el6.x86_64
    
            iam (exec)
            jpeg2ps (exec)
            text2ps (exec)


RUNTIME DEPENDENCIES: 

  A. For MCC21

    This software is executed via the Command UIF %UA_CMD_RELEASE% configuration set, which
    contains the Command UIF %UA_CMD_RELEASE% shared library configuration set, and the run script.

    The run script needs to contain the following information:

    1. The following environment variables are required:

         CMD_APPEND set to ua21 for MSDE only, not needed in the MSE and MCE.

    2. Any input files required.
       iam.xml  - located in the IAM/config/ directory
       iam.symbols - located in the IAM/config/ directory

 B. For the External Partners,
       a. There are dependencies on the PVT version of the ISP libraries.
       b. The input files, including the config file and predict files, will be provided by the PTF team from MCC
       c. The PFT team from MCC will provide the runtime setup for the application.
       	 For reference, at run time, these environment variables are required:
       - setenv EP_ISP_BASE_DIR [directory_path_to_ISP_PVT]
       	 	(i.e. /ms/mccs/scm/Builds/el6.x86_64/esa/isp/esa.av1.21.2)        
       - setenv LD_LIBRARY_PATH $EP_ISP_BASE_DIR/fossLibs:$EP_ISP_BASE_DIR/lib64:$EP_ISP_BASE_DIR/isp_source/isp/Sources/Libs

       - setenv IAM_DATA_DIR [path_to_IAM_data_directory]
       - setenv IAM_CONFIG_DIR  $IAM_DATA_DIR/config
       - setenv IAM_CONFIG_FILE  ${IAM_CONFIG_DIR}/iam.xml
       - setenv BROWSER_APP  /usr/bin/firefox
       - setenv iss_flight_id [current_flightID] 
       	 		      (i.e. ISS244)
       - setenv TDRS tdrs

       - setenv ISPOWNER [Owner_name_same_as_ISP]
       - setenv ISPSOURCE [Source_name_same_as_ISP]
       - setenv ISPMODE [Mode_name_same_as_ISP]
       - setenv ISPPVTHOST [workstation_name_where_ISP_runs] (If ISP runs on different workstation)
       # Start iam
       - iam --manager  --config $IAM_CONFIG_FILE -O $ISPOWNER -S $ISPSOURCE -M $ISPMODE  &

DIRECTORY STRUCTURE
      IAM directory structure

     IAM/iss_struct/
     IAM/iss_struct/kuband/
     IAM/iss_struct/kuband2/
     IAM/iss_struct/sband1/
     IAM/iss_struct/sband2/
     IAM/iss_struct/segments/
     IAM/iss_struct/station/
     IAM/config/
     IAM/predict_data/
     IAM/sho/


OTHER INSTALLATION INSTRUCTIONS
    3. Any output files.
    4. Document any miscellaneous runtime dependency information here.
       The iss_structure files and  IAM masks need to be placed in the directory structure shown above.

    5. Command line arguments required for the application.

        ACCESS PERMISSIONS:
            Files loaded into Ops History have the following permissions:
               Security_string_read:  ALL_Access
               Security_string_write: MCCSGA
        
            Files loaded on the User Managed Storage have the following permissions:
               iam       - <user> domain_user 774

################################################################################
# $Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/INSTALL.txt 275 2017-02-22 21:29:05Z dpham1@ndc.nasa.gov $
# End of INSTALL.TXT

