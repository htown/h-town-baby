/* --------------------------------------------------------------------
 * jpeg2ps: convert JPEG files to compressed PostScript Level 2 EPS
 *
 * (C) 1994-1999 Thomas Merz
 *
 * ------------------------------------------------------------------*/

#define VERSION     "V1.8"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <fcntl.h>
#include <malloc.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

#ifdef G_OS_WIN32
    #define _CRTDBG_MAP_ALLOC
    #include <windows.h>
    #include <io.h>

    #define READMODE  "rb"           /* read JPEG files in binary mode */
    #define WRITEMODE "wb"           /* write (some) PS files in binary mode */

    #ifndef _CRT_SECURE_NO_DEPRECATE
        #define _CRT_SECURE_NO_DEPRECATE (1)
    #endif
    #pragma warning(disable:4996)
#else
    #include <unistd.h>
    #include <sys/param.h>

    #define READMODE  "r"
    #define WRITEMODE "w"           /* write (some) PS files in binary mode */
#endif

#include "psimage.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/jpeg2ps/jpeg2ps.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


#ifdef A4
int PageWidth  = 595;           /* page width A4 */
int PageHeight = 842;           /* page height A4 */
#else
int PageWidth  = 612;           /* page width letter */
int PageHeight = 792;           /* page height letter */
#endif

/**
 * Does the JPEG to Postscript conversion
 * @param JPEG holds state information
 * @param ifp input file
 * @param ofp output file
 */
static void JPEGtoPS (ImageData *JPEG, FILE *ifp, FILE *ofp)
{
    char *ColorSpaceNames[] = {"", "Gray", "", "RGB", "CMYK"};

    int llx, lly, urx, ury;        /* Bounding box coordinates */
    size_t n;
    float scale, sx, sy;           /* scale factors            */
    time_t t;
    int i;

    /* read image parameters and fill JPEG struct*/
    if (!AnalyzeJPEG(JPEG, ifp))
    {
        fprintf(stderr, "Error: '%s' is not a proper JPEG file!\n", JPEG->filename);
        return;
    }

    if (!JPEG->quiet)
    {
        fprintf(stderr, "Note on file '%s': %dx%d pixel, %d color component%s\n",
                JPEG->filename, JPEG->width, JPEG->height, JPEG->components,
                (JPEG->components == 1 ? "" : "s"));
    }

    /* "Use resolution from file" was requested, but we couldn't find any */
    if (JPEG->dpi == DPI_USE_FILE && !JPEG->quiet)
    {
        fprintf(stderr,
                "Note: no resolution values found in JPEG file - using standard scaling.\n");
        JPEG->dpi = DPI_IGNORE;
    }

    if (JPEG->dpi == DPI_IGNORE)
    {
        /* switch to landscape if needed */
        if (JPEG->width > JPEG->height && JPEG->autorotate)
        {
            JPEG->landscape = TRUE;
            if (!JPEG->quiet)
            {
                fprintf(stderr,
                        "Note: image width exceeds height - producing landscape output!\n");
            }
        }
        if (!JPEG->landscape)
        {       /* calculate scaling factors */
            sx = (float) (PageWidth - 2*JPEG->margin) / JPEG->width;
            sy = (float) (PageHeight - 2*JPEG->margin) / JPEG->height;
        }
        else
        {
            sx = (float) (PageHeight - 2*JPEG->margin) / JPEG->width;
            sy = (float) (PageWidth - 2*JPEG->margin) / JPEG->height;
        }
        scale = MIN(sx, sy);    /* We use at least one edge of the page */
    }
    else
    {
        if (!JPEG->quiet)
        {
            fprintf(stderr, "Note: Using resolution %d dpi.\n", (int) JPEG->dpi);
        }
        scale = 72 / JPEG->dpi;     /* use given image resolution */
    }

    if (JPEG->landscape)
    {
        /* landscape: move to (urx, lly) */
        urx = PageWidth - JPEG->margin;
        lly = JPEG->margin;
        ury = (int) (JPEG->margin + scale*JPEG->width + 0.9);    /* ceiling */
        llx = (int) (urx - scale * JPEG->height);          /* floor  */
    }
    else
    {
        /* portrait: move to (llx, lly) */
        llx = lly = JPEG->margin;
        urx = (int) (llx + scale * JPEG->width + 0.9);     /* ceiling */
        ury = (int) (lly + scale * JPEG->height + 0.9);    /* ceiling */
    }

    time(&t);

    /* produce EPS header comments */
    fprintf(ofp, "%%!PS-Adobe-3.0 EPSF-3.0\n");
    fprintf(ofp, "%%%%Creator: jpeg2ps %s by Thomas Merz\n", VERSION);
    fprintf(ofp, "%%%%Title: %s\n", JPEG->filename);
    fprintf(ofp, "%%%%CreationDate: %s", ctime(&t));
    fprintf(ofp, "%%%%BoundingBox: %d %d %d %d\n", llx, lly, urx, ury);
    fprintf(ofp, "%%%%DocumentData: %s\n", JPEG->mode == BINARY ? "Binary" : "Clean7Bit");
    fprintf(ofp, "%%%%LanguageLevel: 2\n");
    fprintf(ofp, "%%%%EndComments\n");
    fprintf(ofp, "%%%%BeginProlog\n");
    fprintf(ofp, "%%%%EndProlog\n");
    fprintf(ofp, "%%%%Page: 1 1\n");

    fprintf(ofp, "/languagelevel where {pop languagelevel 2 lt}");
    fprintf(ofp, "{true} ifelse {\n");
    fprintf(ofp, "  (JPEG file '%s' needs PostScript Level 2!", JPEG->filename);
    fprintf(ofp, "\\n) dup print flush\n");
    fprintf(ofp, "  /Helvetica findfont 20 scalefont setfont ");
    fprintf(ofp, "100 100 moveto show showpage stop\n");
    fprintf(ofp, "} if\n");

    fprintf(ofp, "save\n");
    fprintf(ofp, "/RawData currentfile ");

    if (JPEG->mode == ASCIIHEX)            /* hex representation... */
    {
        fprintf(ofp, "/ASCIIHexDecode filter ");
    }
    else if (JPEG->mode == ASCII85)        /* ...or ASCII85         */
    {
        fprintf(ofp, "/ASCII85Decode filter ");
    }
    /* else binary mode: don't use any additional filter! */

    fprintf(ofp, "def\n");

    fprintf(ofp, "/Data RawData << ");
    fprintf(ofp, ">> /DCTDecode filter def\n");

    /* translate to lower left corner of image */
    fprintf(ofp, "%d 50 translate\n", (JPEG->landscape ?
                                       PageWidth - JPEG->margin : JPEG->margin));

    if (JPEG->landscape)                 /* rotation for landscape */
    {
        fprintf(ofp, "90 rotate\n");
    }

    fprintf(ofp, "%.2f %.2f scale\n", JPEG->width * scale, JPEG->height * scale);
    fprintf(ofp, "/Device%s setcolorspace\n", ColorSpaceNames[JPEG->components]);
    fprintf(ofp, "{ << /ImageType 1\n");
    fprintf(ofp, "     /Width %d\n", JPEG->width);
    fprintf(ofp, "     /Height %d\n", JPEG->height);
    fprintf(ofp, "     /ImageMatrix [ %d 0 0 %d 0 %d ]\n", JPEG->width, -JPEG->height, JPEG->height);
    fprintf(ofp, "     /DataSource Data\n");
    fprintf(ofp, "     /BitsPerComponent %d\n", JPEG->bits_per_component);

    /* workaround for color-inverted CMYK files produced by Adobe Photoshop:
     * compensate for the color inversion in the PostScript code
     */
    if (JPEG->adobe && JPEG->components == 4)
    {
        if (!JPEG->quiet)
        {
            fprintf(stderr, "Note: Adobe-conforming CMYK file - applying workaround for color inversion.\n");
        }
        fprintf(ofp, "     /Decode [1 0 1 0 1 0 1 0]\n");
    }
    else
    {
        fprintf(ofp, "     /Decode [0 1");
        for (i = 1; i < JPEG->components; i++)
        {
            fprintf(ofp," 0 1");
        }
        fprintf(ofp, "]\n");
    }

    fprintf(ofp, "  >> image\n");
    fprintf(ofp, "  Data closefile\n");
    fprintf(ofp, "  RawData flushfile\n");
    fprintf(ofp, "  showpage\n");
    fprintf(ofp, "  restore\n");
    fprintf(ofp, "} exec");

    /* seek to start position of JPEG data */
    fseek(ifp, JPEG->startpos, SEEK_SET);

    switch (JPEG->mode)
    {
        case BINARY:
            {
#define BUFFERSIZE 1024
                char buffer[BUFFERSIZE];
                /* important: ONE blank and NO newline */
                fprintf(ofp, " ");
#ifdef G_OS_WIN32
                fflush(ofp);               /* up to now we have CR/NL mapping */
                _setmode(_fileno(ofp), O_BINARY);    /* continue in binary mode */
#endif
                /* copy data without change */
                while ((n = fread(buffer, 1, sizeof(buffer), ifp)) != 0)
                {
                    fwrite(buffer, 1, n, ofp);
                }
#ifdef G_OS_WIN32
                fflush(ofp);                     /* binary yet */
                _setmode(_fileno(ofp), O_TEXT);    /* text mode */
#endif
            }
            break;

        case ASCII85:
            fprintf(ofp, "\n");

            /* ASCII85 representation of image data */
            if (ASCII85Encode(ifp, ofp))
            {
                fprintf(stderr, "Error: internal problems with ASCII85Encode!\n");
                exit(1);
            }
            break;

        case ASCIIHEX:
            /* hex representation of image data (useful for buggy dvips) */
            ASCIIHexEncode(ifp, ofp);
            break;
    }
    fprintf(ofp, "\n%%%%EOF\n");
}

/**
 * Outputs a usage message
 */
static void usage (void)
{
    fprintf(stderr, "jpeg2ps %s: convert JPEG files to PostScript Level 2.\n", VERSION);
    fprintf(stderr, "(C) Thomas Merz 1994-1999\n\n");
    fprintf(stderr, "usage: jpeg2ps [options] jpegfile > epsfile\n");
    fprintf(stderr, "-a        auto rotate: produce landscape output if width > height\n");
    fprintf(stderr, "-b        binary mode: output 8 bit data (default: 7 bit with ASCII85)\n");
    fprintf(stderr, "-h        hex mode: output 7 bit data in ASCIIHex encoding\n");
    fprintf(stderr, "-i <name> input file name\n");
    fprintf(stderr, "-o <name> output file name\n");
    fprintf(stderr, "-p <size> page size name. Known names are:\n");
    fprintf(stderr, "          a0, a1, a2, a3, a4, a5, a6, b5, letter, legal, ledger, p11x17\n");
    fprintf(stderr, "-q        quiet mode: suppress all informational messages\n");
    fprintf(stderr, "-r <dpi>  resolution value (dots per inch)\n");
    fprintf(stderr, "          0 means use value given in file, if any (disables autorotate)\n");
}

/**
 * Real main for processing the conversion
 * @param argc count of args
 * @param argv the arguments
 * @return true or false
 */
int main (int argc, char **argv)
{
    static char *ofilename = NULL;
    FILE *ofp = stdout;

    static char *ifilename = NULL;
    FILE *ifp = stdin;

    ImageData image =
    {
        NULL,       /* jpeg filename */
        0,          /* width */
        0,          /* height */
        0,          /* color components */
        0,          /* bits per color components */
        DPI_IGNORE, /* image resolution */
        ASCII85,    /* output mode, 8bit, ascii, ascii85 */
        0L,         /* start position within jpeg file */
        FALSE,      /* quite mode */
        FALSE,      /* landscape */
        FALSE,      /* image includes Adobe comment marker */
        FALSE,      /* auto rotate */
        5           /* margin */
    };

    static gboolean help = FALSE;
    static gboolean autorotate = FALSE;
    static char *mode = NULL;
    static char *pagesize = NULL;
    static char *resolution = NULL;
    static char *version = NULL;
    static gboolean quiet = FALSE;

    static GOptionEntry entries[] =
    {
        {"help",       'h', 0, G_OPTION_ARG_NONE,   &help,            "Help",               NULL},
        {"autorotate", 'a', 0, G_OPTION_ARG_NONE,   &autorotate,      "Auto rotation",      NULL},
        {"mode",       'm', 0, G_OPTION_ARG_STRING, &mode,            "Binary mode",        NULL},
        {"pagesize",   'p', 0, G_OPTION_ARG_STRING, &pagesize,        "Page size",          NULL},
        {"quiet",      'q', 0, G_OPTION_ARG_NONE,   &quiet,           "Quiet mode",         NULL},
        {"resolution", 'r', 0, G_OPTION_ARG_STRING, &resolution,      "Resolution in DPI",  NULL},
        {"version",    'v', 0, G_OPTION_ARG_NONE,   &version,         "Version",            NULL},
        {"input",      'i', 0, G_OPTION_ARG_STRING, &ifilename,       "Input filename",     NULL},
        {"output",     'o', 0, G_OPTION_ARG_STRING, &ofilename,       "Output filename",    NULL},
        { NULL}
    };

    GError *error = NULL;
    GOptionContext *context = g_option_context_new("- convert jpeg to postscript");
    g_option_context_add_main_entries(context, entries, NULL);
    g_option_context_add_group(context, gtk_get_option_group(TRUE));
    g_option_context_parse(context, &argc, &argv, &error);

    if (help == TRUE || ifilename == NULL)
    {
        usage();
        return 0;
    }

    image.autorotate = autorotate;
    image.quiet = quiet;

    if (mode != NULL)
    {
        if (g_ascii_strcasecmp(mode, "binary") == 0)
        {
            image.mode = BINARY;
        }
        else if (g_ascii_strcasecmp(mode, "hex") == 0)
        {
            image.mode = ASCIIHEX;
        }
    }

    if (resolution != NULL)
    {
        image.dpi = (float) atof(resolution);
        if (image.dpi < 0)
        {
            fprintf(stderr, "Error: bad resolution value %f !\n", image.dpi);
            return 1;
        }
    }

    if (pagesize != NULL)
    {
        typedef struct
        {
            const char *name; int width; int height;
        } PageSize_s;

        PageSize_s PageSizes[] = {
            {"a0",  2380, 3368},
            {"a1",  1684, 2380},
            {"a2",  1190, 1684},
            {"a3",  842, 1190},
            {"a4",  595, 842},
            {"a5",  421, 595},
            {"a6",  297, 421},
            {"b5",  501, 709},
            {"letter",  612, 792},
            {"legal",   612, 1008},
            {"ledger",  1224, 792},
            {"p11x17",  792, 1224}
        };

#define PAGESIZELIST    (sizeof(PageSizes)/sizeof(PageSizes[0]))

        int pagesizeindex;
        for (pagesizeindex=0; pagesizeindex < PAGESIZELIST; pagesizeindex++)
        {
            if (g_ascii_strcasecmp(pagesize, PageSizes[pagesizeindex].name) == 0)
            {
                PageHeight = PageSizes[pagesizeindex].height;
                PageWidth = PageSizes[pagesizeindex].width;
                break;
            }
        }

        if (pagesizeindex == PAGESIZELIST)
        {  /* page size name not found */
            fprintf(stderr, "Error: Unknown page size %s.\n", pagesize);
            return 1;
        }
    }

    if (ifilename != NULL)
    {
        image.filename = ifilename;
        ifp = fopen(ifilename, READMODE);
        if (ifp == NULL)
        {
            /* error */
            return 1;
        }
    }
    else
    {
        fprintf(stderr, "Error: must enter input file to convert!");
        return 1;
    }

    if (ofilename != NULL)
    {
        ofp = fopen(ofilename, "w");
        if (ofp == NULL)
        {
            fclose( ifp );
            /* error */
            return 1;
        }
    }

    JPEGtoPS(&image, ifp, ofp);      /* convert JPEG data */

    fclose(ifp);
    fclose(ofp);

    return 0;
}

#ifdef G_OS_WIN32
/**
 * Used to fake out windows into thinking this is a windows gui app
 * @param instance
 * @param prev_instance
 * @param command_line the command line args as a single string
 * @param show_command
 * @return True or False
 */
static void setargv(int *argcPtr, char ***argvPtr);
extern int main(int argc, char **argv);
int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, char *command_line, int show_command)
{
    int    argc;
    char** argv;
    char filename[MAX_PATH];
    int result;

    setlocale(LC_ALL, "C");
    setargv(&argc,  &argv);

    GetModuleFileName(NULL, filename, sizeof(filename));
    argv[0] = filename;    // call the user specified main function

    result = main(argc, argv);

    free(argv);
    return result;
}

/**
 * setargv --
 *
 *  Parse the Windows command line string into argc/argv.  Done here
 *  because we don't trust the builtin argument parser in crt0.
 *  Windows applications are responsible for breaking their command
 *  line into arguments.
 *
 *  2N backslashes + quote -> N backslashes + begin quoted string
 *  2N + 1 backslashes + quote -> literal
 *  N backslashes + non-quote -> literal
 *  quote + quote in a quoted string -> single quote
 *  quote + quote not in quoted string -> empty string
 *  quote -> begin quoted string
 *
 * Results:
 *  Fills argcPtr with the number of arguments and argvPtr with the
 *  array of arguments.
 *
 * Side effects:
 *  Memory allocated.
 *
 * @param argcPtr filled with the number of argument strings
 * @param argcvPtr filled with argument strings (malloc'd)
 */
static void setargv(int *argcPtr, char ***argvPtr)
{
    char *cmdLine, *p, *arg, *argSpace;
    char **argv;
    int argc, size, inquote, copy, slashes;

    cmdLine = GetCommandLine(); /* INTL: BUG */

    /*
     * Precompute an overly pessimistic guess at the number of arguments
     * in the command line by counting non-space spans.
     */

    size = 2;
    for (p = cmdLine; *p != '\0'; p++)
    {
        if ((*p == ' ') || (*p == '\t'))
        {  /* INTL: ISO space. */
            size++;
            while ((*p == ' ') || (*p == '\t'))
            { /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }
        }
    }

    /* allocate space for the args */
    argSpace = (char *) malloc((unsigned) (size * sizeof(char *) + strlen(cmdLine) + 1));
    if (argSpace != NULL)
    {
        argv = (char **) argSpace;
        argSpace += size * sizeof(char *);
        size--;

        p = cmdLine;
        for (argc = 0; argc < size; argc++)
        {
            argv[argc] = arg = argSpace;
            while ((*p == ' ') || (*p == '\t'))
            {   /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }

            inquote = 0;
            slashes = 0;
            while (1)
            {
                copy = 1;
                while (*p == '\\')
                {
                    slashes++;
                    p++;
                }
                if (*p == '"')
                {
                    if ((slashes & 1) == 0)
                    {
                        copy = 0;
                        if ((inquote) && (p[1] == '"'))
                        {
                            p++;
                            copy = 1;
                        }
                        else
                        {
                            inquote = !inquote;
                        }
                    }
                    slashes >>= 1;
                }

                while (slashes)
                {
                    *arg = '\\';
                    arg++;
                    slashes--;
                }

                if ((*p == '\0')
                    || (!inquote && ((*p == ' ') || (*p == '\t'))))
                { /* INTL: ISO space. */
                    break;
                }
                if (copy != 0)
                {
                    *arg = *p;
                    arg++;
                }
                p++;
            }
            *arg = '\0';
            argSpace = arg + 1;
        }
        argv[argc] = NULL;

        *argcPtr = argc;
        *argvPtr = argv;
    }
}
#endif /* G_OS_WIN32 */
