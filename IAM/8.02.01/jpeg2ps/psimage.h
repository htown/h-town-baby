#ifndef __PSIMAGE_H__
#define __PSIMAGE_H__

/* -------------------------- psimage.h ------------------------- */
/* data output mode: binary, ascii85, hex-ascii */

typedef enum
{
    BINARY, ASCII85, ASCIIHEX
} DATAMODE;

typedef struct
{
    char     *filename;             /* name of jpeg file */
    int      width;                 /* pixels per line */
    int      height;                /* rows */
    int      components;            /* number of color components */
    int      bits_per_component;    /* bits per color component */
    float    dpi;                   /* image resolution in dots per inch */
    DATAMODE mode;                  /* output mode: 8bit, ascii, ascii85 */
    long     startpos;              /* offset to jpeg data */
    gboolean quiet;                 /* quiet mode */
    gboolean landscape;             /* rotate image to landscape mode? */
    gboolean adobe;                 /* image includes Adobe comment marker */
    gboolean autorotate;            /* auto rotation */
    int      margin;                /* margin */
} ImageData;

#define DPI_IGNORE  -1.0            /* dummy value for imagedata.dpi */
#define DPI_USE_FILE     0.0        /* dummy value for imagedata.dpi */

gboolean            AnalyzeJPEG                     (ImageData *image, FILE *in);
int                 ASCII85Encode                   (FILE *in, FILE *out);
void                ASCIIHexEncode                  (FILE *in, FILE *out);

#endif
