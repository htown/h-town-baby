BEGIN{
#   FS=":"
   min_len=100
}
{
   parm=$1
   parm_len=length(parm)
   if(parm_len < min_len)
   {
      min_len=parm_len
#      printf("parm=%s (%d)\n",parm,parm_len)
   }
}
END{
#   printf("minimum parameter length: %d\n",min_len)
   print min_len
}
