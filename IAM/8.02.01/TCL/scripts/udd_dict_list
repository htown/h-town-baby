#!/bin/ksh -f

#************************************************************************#
# set variables

fullscriptname=$0
script=${fullscriptname##*/}

#*****************************************************************************
# define functions

function display_description
{
   print $script
   print "generates a dictionary list of sitf udd file parameters from a udd file"
}

function display_usage
{
   print "Usage:   $script <sitf udd file name>"
   print "         where"
   print "         <sitf udd file name> is a sitf udd file name in the isp logs directory."
   print "                          It is not necessary to include the .udd extension"
   print "         Note: isp dictionary environment variable (ISPDICTIONARY) must be set"
   print "               isp logs directory environment variable (ISPLOGDIR) must be set"
   print "               sitf file dictionary directory environment variable (DICT_FILES) must be set"
   print "               test scripts directory environment variable (TEST_SCRIPTS) must be set"
}

function display_example
{
   print "Example:"
   print "         $script Joint_Angle_Test   (generate a dictionary list of sitf udd file parameters"
   print "                                       for Joint_Angle_Test.udd file)"
}

function file_init
{
   file=$1
   cat /dev/null > $file
   chmod 775 $file
}

function get_file_name      # retrieve file name
{
   filepath=$1
   file_name=${filepath##*/}
   print $file_name
}

function get_file_name_no_ext      # retrieve file name without extension
{
   filepath=$1
   file_name=${filepath##*/}
   file_no_ext=${file_name%%.*}
   print $file_no_ext
}

function get_min_parm_len      # retrieve file name without extension
{
   filepath=$1
   min_parm_len=`awk -f $TEST_SCRIPTS/parm_len.awk $filepath`
   print $min_parm_len
}

function delete_file
{
   file=$1
   if [[ -a $file ]]
   then
      rm $file
   fi
}

#*****************************************************************************
# check for proper usage, provide help if requested

number_of_arguments=$#

if (($number_of_arguments == 0))
then
   display_usage
   exit 0
elif (($number_of_arguments == 1)) && [[ $1 = "help" ]]
then
   display_description
   display_usage
   display_example
   exit 0
fi

#************************************************************************#
# check for setting of isp dictionary and isp log directory environment variables

if [[ $ISPDICTIONARY != "" ]] && [[ $ISPLOGDIR != "" ]]
then

   #************************************************************************#
   # retrieve sitf udd file name argument
   
   print "input argument: $1"

   SITF_TEST=$(get_file_name_no_ext $1)
   
   print "input file name: $SITF_TEST"

   SITF_UDD_FILE=$SITF_TEST".udd"

   print "sitf udd file name: $SITF_UDD_FILE"

   SITF_DICT_FILE="D_"$SITF_TEST".sitf"

   print "sitf dictionary file name: $SITF_DICT_FILE"
   
   if [[ -a $ISPLOGDIR/$SITF_UDD_FILE ]]
   then
      SITF_UDD_FILE_PATH=$ISPLOGDIR/$SITF_UDD_FILE

      print "$script: processing $SITF_UDD_FILE"

      #************************************************************************#
      # set up temp files

      PARM_ENTRY="/tmp/P_"$SITF_UDD_FILE
      file_init $PARM_ENTRY

      FOUND="/tmp/F_"$SITF_UDD_FILE
      file_init $FOUND
      print "Dictionary listing found for:" > $FOUND

      NOT_FOUND="/tmp/NF_"$SITF_UDD_FILE
      file_init $NOT_FOUND
      print "No dictionary listing found for:" > $NOT_FOUND

      SITF_UDD_LIST="/tmp/S_"$SITF_UDD_FILE
      file_init $SITF_UDD_LIST

      DICT_LIST="/tmp/D_"$SITF_UDD_FILE
      file_init $DICT_LIST

      AWKFILE="/tmp/AWKFILE"

      #************************************************************************#
      # produce a dictionary list of sitf udd file parameters

      if [[ -a $DICT_FILES/$SITF_DICT_FILE ]]
      then
         DICT_FILE=$DICT_FILES/$SITF_DICT_FILE
         min_parm_len=$(get_min_parm_len $DICT_FILE)
	 print "$SITF_DICT_FILE found"
      else
         min_parm_len=3
	 print "$SITF_DICT_FILE does not exist"
      fi
      print "min udd parameter length: $min_parm_len"
      
      strings -n $min_parm_len $SITF_UDD_FILE_PATH > $SITF_UDD_LIST
   
      nparm=`cat $SITF_UDD_LIST | wc -l`
      found=0
      notfound=0

      for parm in `cat $SITF_UDD_LIST`
      do
         print -n "."
         print "BEGIN{FS=\":\"}\$1 == \"$parm\"{printf(\"%-30s %-50s\\\n\",\$1,\$4)}" > $AWKFILE

         awk -f $AWKFILE $ISPDICTIONARY > $PARM_ENTRY
         if [[ -s $PARM_ENTRY ]]
         then
            awk -f $AWKFILE $ISPDICTIONARY >> $DICT_LIST
            ((found = found+1))
            print "   $parm" >> $FOUND
         else
            print "   $parm" >> $NOT_FOUND
            ((notfound = notfound+1))
         fi
      done

      print
      print "dictionary list of $found out of $nparm sitf udd file parameters for $SITF_UDD_FILE in $DICT_LIST"
      if ((notfound != 0))
      then
         cat $NOT_FOUND
      fi

      delete_file $PARM_ENTRY
      delete_file $FOUND
      delete_file $NOT_FOUND
      delete_file $SITF_UDD_LIST
      delete_file $AWKFILE

   else
      print "$ISPLOGDIR/$SITF_UDD_FILE does not exist"
      exit
   fi
else
   print "$script: ISPDICTIONARY or ISPLOGDIR not set"
fi
