#!/bin/bash
###############################################################################
##+----------------------------------------------------------------------------
##|
##| make-ep IAM_GTK
##|
##| DESCRIPTION:
##|    Contains Makefile wrapper for IAM_GTK executables.
##|
##| USAGE:
##|    Intended to be run using GNU make.
##|
##| NOTES:
##|    This is the wrapper script used with the corresponding Makefile to build targets
##|    for external partners. It inserts a few overrides onto
##|    the command line, just before any command line arguments you give it.
##|
##|    % make-ep [make args...]
##|
##|    For SCM builds, this Makefile sets $UA_CMD_TOP and $UA_CMD_RELEASE to
##|    the appropriate areas.  For other builds, you can set these below or as
##|    environment variables, e.g., to /ms/ua/asp/ua_cmd_apps, or a personal
##|    development area.
##|
##| RCS:
##|    $Header: $
##|
##| COPYRIGHT:
##|    Copyright (C) 2017, National Aeronautics and Space Administration.
##|    All Rights Reserved.
##|
##| NOTICE OF COMPUTER PROGRAM USE RESTRICTIONS:
##|    This computer program is furnished on the condition that it be used only
##|    in connection with the specified cooperative project, grant or contract
##|    under which it is provided and that no further use or dissemination
##|    shall be made without prior written permission of the NASA Forwarding
##|    Office.  NMI 2210.2B (12/13/90).
##|
##+----------------------------------------------------------------------------
###############################################################################

extraCflags="\$(GCC_ANY_CFLAGS)    \
   -Wmissing-prototypes            \
   -Wmissing-declarations          \
   -Wimplicit-function-declaration \
   -g \$(IFLAGS) \$(WARNFLAGS) \$(VERSION_FLAGS) -DMCC_LINUX -D_EXT_PARTNERS"

esaIsp=/ms/mccs/scm/Builds/el6.x86_64/esa/isp/esa.av1.21.2/isp_source/isp
ispInc=$esaIsp/Include

ldLibs="\$(GTK_LIBS)               \
   -L$esaIsp/Sources/Libs          \
   -lIt"

/usr/bin/make             \
   BD='bin-ep/$(OS_TYPE)' \
   OD='obj-ep/$(OS_TYPE)' \
   CFLAGS+="$extraCflags" \
   ISP_INC=$ispInc        \
   LDLIBS="$ldLibs"       \
   "$@"
