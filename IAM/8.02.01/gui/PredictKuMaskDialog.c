/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "PredictKuMaskDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "IspSymbols.h"
#include "MaskFile.h"
#include "MemoryHandler.h"
#include "PredictDialog.h"
#include "keywords.h"
#include "MessageHandler.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PredictKuMaskDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define USE_X 0
#define USE_Y 1

/* local class variables */
static GtkWidget    *thisDialog  = NULL;   /* widget for the PPL dialog */

/* define the ok and apply buttons */
static GtkWidget    *thisOkButton = NULL;
static GtkWidget    *thisApplyButton = NULL;
static GtkWidget    *thisUseRealtimeButton = NULL;

static gboolean     thisUseRealtimeButtonFlag = True;

/* notebook instance */
static GtkWidget    *thisNotebook = NULL;

/* holds the values for the kuband masks */
static KuMaskInfo   thisKuMaskInfo[MAX_KUBAND_MASKS] = {{NULL}};

/* used for the popup */
static GtkWidget    *thisMenu = NULL;

/* flag indicating realtime mask data selected */
static gboolean     thisRealtimeMasksEnabled = False;
/*************************************************************************/

/**
 * This method creates the dialog
 *
 * @param parent attach dialog to this window
 */
void PKMD_Create(GtkWindow *parent)
{
    GtkWidget *vbox;
    GtkWidget *container;
    GtkWidget *separator;
    GtkWidget *button;

    if (thisDialog == NULL)
    {
        thisMenu = gtk_menu_new();

        /* make the dialog with ok,cancel,clear buttons; the clear button will
         * use the close response
         */
        thisDialog = gtk_dialog_new();
        gtk_window_set_transient_for(GTK_WINDOW(thisDialog), parent);
        gtk_window_set_title(GTK_WINDOW(thisDialog), "Predict KuMask");
        thisOkButton = gtk_dialog_add_button(GTK_DIALOG(thisDialog), GTK_STOCK_OK, GTK_RESPONSE_OK);
        thisApplyButton = gtk_dialog_add_button(GTK_DIALOG(thisDialog), GTK_STOCK_APPLY, GTK_RESPONSE_APPLY);
        button = gtk_dialog_add_button(GTK_DIALOG(thisDialog), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(response_cb), NULL);

        vbox = gtk_vbox_new(False, 5);
        {
            /* create dialog components */
            container = create_pkmd_container();
            gtk_box_pack_start(GTK_BOX(vbox), container, True, False, 3);

            separator = gtk_hseparator_new();
            gtk_box_pack_start(GTK_BOX(vbox), separator, True, False, 0);

            container = create_buttons();
            gtk_box_pack_start(GTK_BOX(vbox), container, True, False, 3);
        }
        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), vbox);

        /* initially load static mask data */
        load_predict_masks();
    }
}

/**
 * This function pops up the Predict KuMask Dialog
 */
void PKMD_Open(void)
{
    if (thisDialog != NULL)
    {
        gtk_widget_show_all(thisDialog);
        gtk_widget_set_sensitive(thisUseRealtimeButton, thisUseRealtimeButtonFlag);
    }
}

/**
 * Creates the gui components.
 *
 * @return notebook widget
 */
static GtkWidget *create_pkmd_container(void)
{
    guint i;
    GtkWidget *table;
    GtkWidget *label;
    GtkWidget *hbox;
    GtkWidget *separator;

    thisNotebook = gtk_notebook_new();

    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(thisNotebook), GTK_POS_TOP);
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(thisNotebook), True);
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(thisNotebook), True);
    gtk_notebook_popup_enable(GTK_NOTEBOOK(thisNotebook));

#define LOWER_LABEL     "Lower"
#define UPPER_LABEL     "Upper"
#define XEL_LABEL       "XEL"
#define EL_LABEL        "EL"
#define PRIMARY_LABEL   "Primary"
#define AUTOTRACK_LABEL "Auto-Track"
#define OPENLOOP_LABEL  "Open-Loop"
#define SPIRAL_LABEL    "Spiral"
#define RT_ENABLED_LABEL "(rt)"

#define WIDTH_CHARS 6

    for (i=0; i<MAX_KUBAND_MASKS; i++)
    {
        table = gtk_table_new(4, 7, False);
        {
            GtkWidget *menu_label;
            gchar mask_name_buf[32];
            gchar *mask_name;
            KuMaskInfo *kumask_info = &thisKuMaskInfo[i];

            /* row 1 */
            label = gtk_label_new(LOWER_LABEL);
            insert_table_item(table, label, 1, 2, GAO_NONE, GAO_NONE, 2, 2);

            label = gtk_label_new(RT_ENABLED_LABEL);
            insert_table_item(table, label, 1, 3, GAO_NONE, GAO_NONE, 2, 2);

            label = gtk_label_new(UPPER_LABEL);
            insert_table_item(table, label, 1, 4, GAO_NONE, GAO_NONE, 2, 2);

            label = gtk_label_new(RT_ENABLED_LABEL);
            insert_table_item(table, label, 1, 5, GAO_NONE, GAO_NONE, 2, 2);

            /* row 2 */
            label = gtk_label_new(EL_LABEL);
            insert_table_item(table, label, 2, 1, GAO_NONE, GAO_NONE, 2, 2);

            kumask_info->entry_w[LOWER_EL] = gtk_entry_new();
            gtk_entry_set_width_chars(GTK_ENTRY(kumask_info->entry_w[LOWER_EL]), WIDTH_CHARS);
            insert_table_item(table, kumask_info->entry_w[LOWER_EL], 2, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->rt_enabled[LOWER_EL] = gtk_check_button_new();
            gtk_widget_set_sensitive(kumask_info->rt_enabled[LOWER_EL], False);
            insert_table_item(table, kumask_info->rt_enabled[LOWER_EL], 2, 3, GAO_NONE, GAO_NONE, 2, 2);

            kumask_info->entry_w[UPPER_EL] = gtk_entry_new();
            gtk_entry_set_width_chars(GTK_ENTRY(kumask_info->entry_w[UPPER_EL]), WIDTH_CHARS);
            insert_table_item(table, kumask_info->entry_w[UPPER_EL], 2, 4, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->rt_enabled[UPPER_EL] = gtk_check_button_new();
            gtk_widget_set_sensitive(kumask_info->rt_enabled[UPPER_EL], False);
            insert_table_item(table, kumask_info->rt_enabled[UPPER_EL], 2, 5, GAO_NONE, GAO_NONE, 2, 2);

            /* row 3 */
            label = gtk_label_new(XEL_LABEL);
            insert_table_item(table, label, 3, 1, GAO_NONE, GAO_NONE, 2, 2);

            kumask_info->entry_w[LOWER_XEL] = gtk_entry_new();
            gtk_entry_set_width_chars(GTK_ENTRY(kumask_info->entry_w[LOWER_XEL]), WIDTH_CHARS);
            insert_table_item(table, kumask_info->entry_w[LOWER_XEL], 3, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->rt_enabled[LOWER_XEL] = gtk_check_button_new();
            gtk_widget_set_sensitive(kumask_info->rt_enabled[LOWER_XEL], False);
            insert_table_item(table, kumask_info->rt_enabled[LOWER_XEL], 3, 3, GAO_NONE, GAO_NONE, 2, 2);

            kumask_info->entry_w[UPPER_XEL] = gtk_entry_new();
            gtk_entry_set_width_chars(GTK_ENTRY(kumask_info->entry_w[UPPER_XEL]), WIDTH_CHARS);
            insert_table_item(table, kumask_info->entry_w[UPPER_XEL], 3, 4, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->rt_enabled[UPPER_XEL] = gtk_check_button_new();
            gtk_widget_set_sensitive(kumask_info->rt_enabled[UPPER_XEL], False);
            insert_table_item(table, kumask_info->rt_enabled[UPPER_XEL], 3, 5, GAO_NONE, GAO_NONE, 2, 2);

            /* place separator between lower/upper and checkboxes */
            /* make the separator stretch from top to bottom */
            separator = gtk_vseparator_new();
            gtk_table_attach(GTK_TABLE(table), separator, 5, 6, 0, 6, GAO_NONE, GAO_EXPAND_FILL, 2, 2);

            /* last column all rows */
            kumask_info->visibility[PRIMARY_MASK] = gtk_check_button_new_with_label(PRIMARY_LABEL);
            insert_table_item(table, kumask_info->visibility[PRIMARY_MASK], 1, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->visibility[AUTOTRACK_MASK] = gtk_check_button_new_with_label(AUTOTRACK_LABEL);
            insert_table_item(table, kumask_info->visibility[AUTOTRACK_MASK], 2, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->visibility[OPENLOOP_MASK] = gtk_check_button_new_with_label(OPENLOOP_LABEL);
            insert_table_item(table, kumask_info->visibility[OPENLOOP_MASK], 3, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            kumask_info->visibility[SPIRAL_MASK] = gtk_check_button_new_with_label(SPIRAL_LABEL);
            insert_table_item(table, kumask_info->visibility[SPIRAL_MASK], 4, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 2);

            mask_name = MF_GetKuBandMaskName(i);
            if (mask_name == NULL)
            {
                g_sprintf(mask_name_buf, DEFAULT_KUBAND_MASK_NAME,i+1);
                mask_name = mask_name_buf;
            }
            menu_label = gtk_label_new(mask_name);

            hbox = gtk_hbox_new(False, 0);
            {
                label = gtk_label_new("    ");
                gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 0);

                kumask_info->mask_name = gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(kumask_info->mask_name), mask_name);
                gtk_editable_set_editable(GTK_EDITABLE(kumask_info->mask_name), True);
                gtk_box_pack_start(GTK_BOX(hbox), kumask_info->mask_name, False, False, 0);

                gtk_widget_show_all(hbox);
            }
            gtk_notebook_append_page_menu(GTK_NOTEBOOK(thisNotebook), table, hbox, menu_label);
        }
    }

    g_signal_connect(GTK_NOTEBOOK(thisNotebook), "switch_page", G_CALLBACK(switch_page_cb), NULL);

    return thisNotebook;
}

/**
 * Creates buttons for using realtime data, clearing the current
 * notebook page and for clearing all notebook pages. Each button
 * has a signal handler attached.
 *
 * @return container that has buttons
 */
static GtkWidget *create_buttons(void)
{
    GtkWidget *hbox;
    GtkWidget *button;

    hbox = gtk_hbox_new(False, 3);
    {
        thisUseRealtimeButton = gtk_button_new_with_label("Use Realtime");
        gtk_box_pack_end(GTK_BOX(hbox), thisUseRealtimeButton, False, False, 3);
        g_signal_connect(thisUseRealtimeButton, "clicked", G_CALLBACK(use_realtime_cb), NULL);

        button = gtk_button_new_with_label("Clear Current");
        gtk_box_pack_end(GTK_BOX(hbox), button, False, False, 3);
        g_signal_connect(button, "clicked", G_CALLBACK(clear_current_cb), NULL);

        button = gtk_button_new_with_label("Clear All");
        gtk_box_pack_end(GTK_BOX(hbox), button, False, False, 3);
        g_signal_connect(button, "clicked", G_CALLBACK(clear_all_cb), NULL);
    }
    gtk_widget_show_all(hbox);

    return hbox;
}

/**
 * Handles enabling/disabling the Use Realtime button.
 */
void PKD_EnableUseRealtime(gboolean enabled)
{
    if (thisDialog != NULL)
    {
        gtk_widget_set_sensitive(thisUseRealtimeButton, enabled);
        set_kuband_mask_visibility();
        PD_SetRedraw(True);
        PD_DrawData(False);
    }
 
    thisUseRealtimeButtonFlag = enabled;
}

/**
 * Rows and columns are number starting with 1.
 *
 * @param table table widget
 * @param item item to add to table
 * @param row the row
 * @param col the column
 * @param xoptions table x attach options
 * @param yoptions table y attach options
 * @param xpadding table x padding options
 * @param ypadding table y padding options
 */
static void insert_table_item(GtkWidget *table, GtkWidget *item, gint row, gint col,
                              GtkAttachOptions xoptions, GtkAttachOptions yoptions,
                              gint xpadding, gint ypadding)
{
    gtk_table_attach(GTK_TABLE(table), item, col-1, col, row-1, row, xoptions, yoptions, xpadding, ypadding);
}

/**
 * Gets the realtime mask data and loads them into each
 * lower/upper el/xel field. Also sets the rt active button
 * to indicate that the value loaded is from realtime.
 */
static void load_predict_masks(void)
{
    guint i;
    guint mask_num;
    KuMaskInfo *kumask_info;
    PolyLine *mask_data;
    gchar text_buf[32];

    for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
    {
        kumask_info = &thisKuMaskInfo[mask_num];

        /* get the predict mask data previously loaded */
        mask_data = MF_GetPredictMaskData(mask_num);
        if (mask_data != NULL)
        {
            if (mask_data->ptCount == 4)
            {
                g_sprintf(text_buf, "%d", mask_data->pts[LOWER_EL_INDEX].y/10);
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_EL]), text_buf);
                set_kuband_realtime_button(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_EL]),
                                           mask_data->pts[LOWER_EL_INDEX].y, mask_num, LOWER_EL_INDEX, USE_Y);

                g_sprintf(text_buf, "%d", mask_data->pts[UPPER_EL_INDEX].y/10);
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_EL]), text_buf);
                set_kuband_realtime_button(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_EL]),
                                           mask_data->pts[UPPER_EL_INDEX].y, mask_num, UPPER_EL_INDEX, USE_Y);

                g_sprintf(text_buf, "%d", mask_data->pts[LOWER_XEL_INDEX].x/10);
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_XEL]), text_buf);
                set_kuband_realtime_button(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_XEL]),
                                           mask_data->pts[LOWER_XEL_INDEX].x, mask_num, LOWER_XEL_INDEX, USE_X);

                g_sprintf(text_buf, "%d", mask_data->pts[UPPER_XEL_INDEX].x/10);
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_XEL]), text_buf);
                set_kuband_realtime_button(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_XEL]),
                                           mask_data->pts[UPPER_XEL_INDEX].x, mask_num, UPPER_XEL_INDEX, USE_X);

                kumask_info->mask_data = mask_data;
            }
        }

        for (i=0; i<MAX_VISIBILITY_MASKS; i++)
        {
            /* get the predict mask data previously loaded */
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->visibility[i]),
                                         MF_GetKuBandVisibility(PREDICT, mask_num, i));
        }
    }
}


/**
 * Handles the notebook tab switching. Enables editing of the tab
 * name for the raised tab and disables the other.
 *
 * @param widget calling widget
 * @param page current page
 * @param page_num page number
 * @param user_data user data
 */
static void switch_page_cb(GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data)
{
    GtkNotebook *notebook = GTK_NOTEBOOK(widget);
    gint old_page_num = gtk_notebook_get_current_page(notebook);
    KuMaskInfo *kumask_info;

    if ((gint)page_num != old_page_num)
    {
        kumask_info = &thisKuMaskInfo[page_num];
        if (kumask_info != NULL)
        {
            gtk_editable_set_editable(GTK_EDITABLE(kumask_info->mask_name), True);
            gtk_entry_set_has_frame(GTK_ENTRY(kumask_info->mask_name), True);

            if (old_page_num >= 0)
            {
                kumask_info = &thisKuMaskInfo[old_page_num];
                if (kumask_info != NULL)
                {
                    gtk_editable_set_editable(GTK_EDITABLE(kumask_info->mask_name), False);
                    gtk_entry_set_has_frame(GTK_ENTRY(kumask_info->mask_name), False);
                }
            }
        }
    }
}

/**
 * Gets the realtime data and loads the text entry fields.
 * Also sets a local variable indicating that realtime data
 * has been enabled.
 *
 * @param button button making call
 * @param data user data
 */
static void use_realtime_cb(GtkButton *button, void * data)
{
    guint mask_num;
    guint i;
    KuMaskInfo *kumask_info;
    PolyLine *mask_data;
    gchar text_buf[32];

    /* bootstrip the predict mask buffer with realtime */
    MF_SetKuBandPredictMasks();

    /* rip thru the masks */
    for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
    {
        kumask_info = &thisKuMaskInfo[mask_num];

        /* get the realtime mask data */
        mask_data = MF_GetRealtimeMaskData(mask_num);
        if (mask_data != NULL)
        {
            if (mask_data->ptCount == 4)
            {
                g_sprintf(text_buf, "%d", (mask_data->pts[LOWER_EL_INDEX].y/10));
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_EL]), text_buf);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_EL]), True);

                g_sprintf(text_buf, "%d", (mask_data->pts[UPPER_EL_INDEX].y/10));
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_EL]), text_buf);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_EL]), True);

                g_sprintf(text_buf, "%d", (mask_data->pts[LOWER_XEL_INDEX].x/10));
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_XEL]), text_buf);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_XEL]), True);

                g_sprintf(text_buf, "%d", (mask_data->pts[UPPER_XEL_INDEX].x/10));
                gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_XEL]), text_buf);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_XEL]), True);

                kumask_info->mask_data = mask_data;
            }
        }

        for (i=0; i<MAX_VISIBILITY_MASKS; i++)
        {
            /* get the current realtime mask settings */
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->visibility[i]),
                                         MF_GetKuBandVisibility(PREDICT, mask_num, i));
        }
    }
    thisRealtimeMasksEnabled = True;
}

/**
 * Clears the current tab page. All text entries fields, checkboxes
 * are reset.
 *
 * @param button button making call
 * @param data user data
 */
static void clear_current_cb(GtkButton *button, void * data)
{
    guint i;
    gint current_page;
    KuMaskInfo *kumask_info;

    current_page = gtk_notebook_get_current_page(GTK_NOTEBOOK(thisNotebook));
    if (current_page != -1)
    {
        kumask_info = &thisKuMaskInfo[current_page];

        for (i=0; i<MAX_VISIBILITY_MASKS; i++)
        {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->visibility[i]), False);
        }

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_EL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_EL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_EL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_EL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_XEL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_XEL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_XEL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_XEL]), False);
    }
}

/**
 * Clears all text entry fields; resets the checkboxes, and
 * clears the local variable indicating realtime is enabled.
 *
 * @param button button making call
 * @param data user data
 */
static void clear_all_cb(GtkButton *button, void * data)
{
    guint i;
    guint mask_num;
    KuMaskInfo *kumask_info;

    /* set the data values and the visibility state for each mask */
    for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
    {
        kumask_info = &thisKuMaskInfo[mask_num];

        for (i=0; i<MAX_VISIBILITY_MASKS; i++)
        {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->visibility[i]), False);
        }

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_EL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_EL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_EL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_EL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[LOWER_XEL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[LOWER_XEL]), False);

        gtk_entry_set_text(GTK_ENTRY(kumask_info->entry_w[UPPER_XEL]), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(kumask_info->rt_enabled[UPPER_XEL]), False);
    }
    thisRealtimeMasksEnabled = False;
}

/**
 * This method handles the button events
 *
 * @param widget calling widget
 * @param response_id action that got us here
 * @param data user data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;

    switch (response_id)
    {
        case GTK_RESPONSE_APPLY:
            doUnmanage = False;
            /* fall thru */
        case GTK_RESPONSE_OK:
            set_kuband_mask_values();
            set_kuband_mask_visibility();
            save_kuband_mask_names();
            PD_SetRedraw(True);
            PD_DrawData(False);
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            /* do nothing */
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}

/**
 * Loads the masks values for the predict display. The data in
 * each text entry field is loaded into the predict mask info
 * structure.
 */
static void set_kuband_mask_values(void)
{
    guint i;
    guint mask_num;
    guint symbol_id = IS_KU_MASK_FIRST_SYMBOL;
    //   gint ival;
   gint ival[4];
   const gchar *sval;
   KuMaskInfo *kumask_info;
   char title[100];
   char msg[100];
   gboolean warn;
    /* set the data values and the visibility state for each mask */
    for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
    {
        kumask_info = &thisKuMaskInfo[mask_num];

        for (i=0; i<4; i++)
        {
            sval = gtk_entry_get_text(GTK_ENTRY(kumask_info->entry_w[i]));
            if (sscanf(sval, "%d", &ival[i]) == 1)
            {
                /* get lower/upper el and lower/upper xel */
	      //      MF_SetKuBandMaskValue(PREDICT, symbol_id-IS_KU_MASK_FIRST_SYMBOL, ival*10);
                symbol_id++;
            }
            else
            {
                MF_ClearKuBandMaskPtCount(PREDICT, symbol_id-IS_KU_MASK_FIRST_SYMBOL);
                symbol_id += 4;
                break;
            }
        }
	sprintf(title, "Kuband Mask error");
	sprintf(msg, "Mask %s definition is incorrect:", 
		gtk_entry_get_text(GTK_ENTRY(kumask_info->mask_name)));
	warn = FALSE;
	if (ival[0] > ival[1]) {
	  warn = TRUE;
	  sprintf(msg, "%s EL [%d] > [%d]\n", 
		  msg, ival[0], ival[1]);
	}  
	if (ival[2] > ival[3]) {
	  warn = TRUE;
	  sprintf(msg, "%s XEL [%d] > [%d]\n", 
		  msg, ival[2], ival[3]);
	}
	if (warn) {
	  WarningMessage(AM_GetActiveDisplay(), title, msg);
	}
	else {
	  for (i=0; i<4; i++) {
	    MF_SetKuBandMaskValue(PREDICT, i, ival[i]*10);
	  }
	}
    }
}

/**
 * Sets the visibility for the masks. Each visibility button for all notebook
 * pages is checked to see if they are enabled or not. The state is then set
 * in the predict mask info structure.
 */
static void set_kuband_mask_visibility(void)
{
    guint i;
    guint mask_num;
    guint symbol_id = IS_KU_MASK_FIRST_SYMBOL;
    KuMaskInfo *kumask_info;

    /* set the data values and the visibility state for each mask */
    for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
    {
        kumask_info = &thisKuMaskInfo[mask_num];

        for (i=0; i<MAX_VISIBILITY_MASKS; i++)
        {
            MF_SetKuBandVisibility(PREDICT, symbol_id-IS_KU_MASK_FIRST_SYMBOL, i,
                                   gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(kumask_info->visibility[i])));
        }
        symbol_id++;
    }
}

/**
 * Determines if the value being loaded is equal to the realtime value.
 * If it is then the check button is enabled.
 *
 * @param button realtime button
 * @param predict_value predict data value to compare against realtime data
 * @param mask_num mask number
 * @param value_index index into realtime mask data
 * @param use_xy flag indicating which value to use, x or y
 */
static void set_kuband_realtime_button(GtkToggleButton *button, gint predict_value, gint mask_num, gint value_index, gint use_xy)
{
    gint realtime_value = 0;
    PolyLine *rt_mask_data = MF_GetRealtimeMaskData(mask_num);
    if (rt_mask_data != NULL)
    {
        if (use_xy == USE_X)
        {
            realtime_value = rt_mask_data->pts[value_index].x;
        }
        else
        {
            realtime_value = rt_mask_data->pts[value_index].y;
        }

        if (predict_value == realtime_value)
        {
            gtk_toggle_button_set_active(button, True);
        }
        else
        {
            gtk_toggle_button_set_active(button, False);
        }
    }
}

/**
 * Outputs the kuband mask names found in the tab fields.
 */
static void save_kuband_mask_names(void)
{
    guint i;
    KuMaskInfo *kumask_info;
    const gchar *mask_name;
    gboolean write_config = False;

    for (i=0; i<MAX_KUBAND_MASKS; i++)
    {
        kumask_info = &thisKuMaskInfo[i];

        mask_name = gtk_entry_get_text(GTK_ENTRY(kumask_info->mask_name));
        if (is_default_name(mask_name, i+1) == False)
        {
            gchar key[32];

            g_sprintf(key, KUBAND_MASK_KEYWORD_FMT, i+1);
            CP_UpdateConfigValue(key, (gchar *)mask_name);
            write_config = True;

            /* update the mask info */
            MF_LoadMaskName(i);
        }
    }

    if (write_config == True)
    {
        CP_WriteConfigFile();
    }
}

/**
 * Determines if the new mask name is the same or has changed.
 *
 * @param new_mask_name new mask name
 * @param mask_num mask number
 *
 * @return True or False
 */
static gboolean is_default_name(const gchar *new_mask_name, gint mask_num)
{
    gchar key[32];
    gchar value[32];

    g_sprintf(key, KUBAND_MASK_KEYWORD_FMT, mask_num);

    if (CP_GetConfigData(key, value) == True)
    {
        if (g_ascii_strcasecmp(new_mask_name, value) == 0)
        {
            return True;
        }
    }

    return False;
}
