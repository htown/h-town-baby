/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PRIVATE
#include "RealtimeDataHandler.h"
#undef PRIVATE

#include "ConfigParser.h"
#include "Conversions.h"
#include "FontHandler.h"
#include "IspHandler.h"
#include "MemoryHandler.h"
#include "PixmapHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "RealtimeRFDialog.h"
#include "WindowGrid.h"

#ifdef G_OS_WIN32
/* so the windows it.lib will link */
extern "C"
{
#include "status.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/RealtimeDataHandler.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");

}
#else
    #include "status.h"
#endif
/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define  COPYNULLSTR(x, y)  g_stpcpy(x, (y != NULL) ? y : " ")
#define  ERR_STRING         "Err"

static gint         thisMaxTime     = LD_DEFAULT_LIMIT;

static JointAngle   thisJointAngle;

#define MAX_PREDICTS 2
static PredictSet   *thisPredicts   [MAX_PREDICTS] = { NULL, NULL};
static GList        *thisCurPredPt  [MAX_PREDICTS] = { NULL, NULL};

static RtTrack      thisPTS         [MAX_TDRS];

static gint         thisColorId     [RTD_ViewMax] = { GC_GRAY, GC_CYAN, GC_GREEN};

static gdouble      thisLGABoresightAz = 0.0;
static gdouble      thisLGABoresightEl = 0.0;

static gboolean     thisTracksInitialized = True;
/*************************************************************************/

/**
 * Initializes class data structures
 */
void RTDH_Constructor(void)
{
    gint i;

    for (i=0; i<MAX_PREDICTS; i++)
    {
        thisPredicts[i] = NULL;
        thisCurPredPt[i] = NULL;
    }

    initialize_tracks();
}

/**
 * Releases all memory
 */
void RTDH_Destructor(void)
{
    gint i;
    for (i=0; i<MAX_TDRS; i++)
    {
        if (thisPTS[i].pts != NULL)
        {
            MH_Free(thisPTS[i].pts);
            thisPTS[i].pts = NULL;
        }
    }
}

/**
 * This method initializes the track data buffers
 */
static void initialize_tracks(void)
{
    gint i,j;
    gchar *labels[MAX_TDRS] = { "W", "E", " "};
    gchar data[256];
    gint colorId;

    /*--------------------------------*/
    /* initialize all TDRS track data */
    /*--------------------------------*/
    for (i=0; i<MAX_TDRS; i++)
    {
        g_stpcpy(thisPTS[i].label, labels[i] );
        thisPTS[i].size    = thisMaxTime + 10;
        thisPTS[i].pts     = (PTimePoint) MH_Calloc(sizeof(TimePoint) * thisPTS[i].size, __FILE__, __LINE__);
        thisPTS[i].head    = 0;
        thisPTS[i].tail    = 0;
        thisPTS[i].pts[0].timeStr[0] = '\0';
        thisPTS[i].pts[0].timeSecs   = -1;
        thisPTS[i].pts[0].inView     = RTD_NotInView;
        for (j=0; j<NUM_COORDS; j++)
        {
            thisPTS[i].pts[0].pt[j].x = 0;
            thisPTS[i].pts[0].pt[j].y = 0;
        }
    }

    /*----------------------------------------*/
    /*Determine the colors of the event types */
    /*----------------------------------------*/
    /* TDRS selected event type */
    if (CP_GetConfigData(REALTIME_SELECTED_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisColorId[RTD_Selected] = colorId;
    }

    /* TDRS in view, not selected event type */
    if (CP_GetConfigData(REALTIME_INVIEW_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisColorId[RTD_InViewNotSelected] = colorId;
    }

    /* TDRS not in view event type */
    if (CP_GetConfigData(REALTIME_NOTINVIEW_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisColorId[RTD_NotInView] = colorId;
    }

    /* Get the S-Band LGA pointing direction in S-Band Az */
    if (CP_GetConfigData(S_BAND_LGA_BORESIGHT_AZ, data))
    {
        sscanf(data, "%lf", &thisLGABoresightAz);
    }

    /* Get the S-Band LGA pointing direction in S-Band El */
    if (CP_GetConfigData(S_BAND_LGA_BORESIGHT_EL, data))
    {
        sscanf(data, "%lf", &thisLGABoresightEl);
    }

    thisTracksInitialized = True;
}

/**
 * Returns the given tdrs track points
 *
 * @param tdrs which tdrs
 *
 * @return RtTrack pointer
 */
RtTrack *RTDH_GetTrack(gint tdrs)
{
    if (tdrs == TD_TDRE || tdrs == TD_TDRW || tdrs == TD_SUN)
    {
        return &thisPTS[tdrs];
    }

    return NULL;
}

/**
 * Returns the color to use for the tdrs track number
 *
 * @param in_view tdrs in view state
 *
 * @return color
 */
gint RTDH_GetTrackColor(gint in_view)
{
    if (in_view >= 0 && in_view < RTD_ViewMax)
        return thisColorId[in_view];

    return GC_WHITE;
}

/**
 * This method returns the AZ LGA bore/sight value found in the
 * configuration file.
 *
 * @return AZ LGA bore-sight value
 */
gdouble RTDH_GetLGABoresightAz(void)
{
    return thisLGABoresightAz;
}

/**
 * This method returns the EL LGA bore/sight value found in the
 * configuration file.
 *
 * @return EL LGA bore-sight value
 */
gdouble RTDH_GetLGABoresightEl(void)
{
    return thisLGABoresightEl;
}

/**
 * Returns the current predict set for the given tdrs
 *
 * @param tdrs which tdrs
 *
 * @return predict set
 */
PredictSet *RTDH_GetPredicts(gint tdrs)
{
    return thisPredicts[tdrs];
}

/**
 * Returns the current predict pt for the given tdrs
 *
 * @param tdrs which tdrs
 *
 * @return current predict pt
 */
GList *RTDH_GetCurPredictPt(gint tdrs)
{
    return thisCurPredPt[tdrs];
}

/**
 * This method returns the joint angle structure
 *
 * @return JointAngle structure
 */
JointAngle *RTDH_GetJointAngle(void)
{
    return &thisJointAngle;
}

/**
 * This function sets to NULL the pointers into the prediction data structures.
 * It is called whenever the prediction data is reloaded, to prevent erroneous
 * pointers to old data in freed memory areas.
 */
void RTDH_ResetPredicts()
{
    thisPredicts[TD_TDRW]  = NULL;
    thisPredicts[TD_TDRE]  = NULL;
    thisCurPredPt[TD_TDRW] = NULL;
    thisCurPredPt[TD_TDRE] = NULL;
}

/**
 * This function gets the predicts for the current time for use in the realtime
 * display.  It checks to see if there currently are realtime predicts.  If not,
 * or if the current ones are no longer valid, it loads the correct ones into
 * memory.
 *
 * @param coord the coordinate needing the predict data
 * @param tdrs which tdrs
 * @param force force load
 */
void RTDH_LoadRtPredicts(gint coord, gint tdrs, gint force)
{
    glong secs;
    GList *psList = PDH_GetData();

    /* validate the current time and make sure the label is valid
       if the label is invalid, then the selected TDRS is unknown */
    if (ValidCurrentUtc() && thisPTS[tdrs].label[0] != '0')
    {
        secs = CurrentUtcSecs();
        if (force ||
            (thisPredicts[tdrs] == NULL) ||
            (thisPredicts[tdrs]->losSecs < secs))
        {
            if (thisPredicts[tdrs] != NULL)
            {
                /* no longer displaying this predict */
                thisPredicts[tdrs]->displayed -= DISPLAY_RD;
            }

            thisPredicts[tdrs]  = NULL;
            thisCurPredPt[tdrs] = NULL;

            /* find the next predict to show */
            for (; psList != NULL; psList = psList->next)
            {
                PredictSet *ps = (PredictSet *)psList->data;
                if (ps == NULL)
                    break;

                /* does it fall within the current time */
                if ((secs <= ps->losSecs) &&
                    (ps->trackLabel[0] == thisPTS[tdrs].label[0]))
                {
                    /* start displaying this predict */
                    ps->displayed |= DISPLAY_RD;
                    thisPredicts[tdrs] = ps;
                    thisCurPredPt[tdrs] = ps->ptList;
                    break;
                }
            }
        }
    }
    else
    {
        thisPredicts[tdrs] = NULL;
        thisCurPredPt[tdrs] = NULL;
    }
}

/**
 * This function updates the limit for how glong (in time) the real-time tracks
 * may be, and truncates any tracks that are made too glong by this.
 *
 * @param limit new limit
 */
void RTDH_UpdateLimits(gint limit)
{
    gint i;

    if (((limit >= 5) && (limit <= 120)) && (limit != thisMaxTime))
    {
        thisMaxTime = limit;

        RTDH_CheckRtLimits();
        for (i = 0; i < MAX_TDRS; i++)
        {
            copy_rt_track(&(thisPTS[i]), thisPTS[i].size);
        }

        RTD_DrawData();
    }
}

/**
 * This function checks to see if the track indicated by index needs to be
 * shortened to fit within the set time limit.  It moves the tail toward the
 * head as needed to shorten the track.
 */
void RTDH_CheckRtLimits(void)
{
    glong duration;
    RtTrack *track;
    gint i;

    for (i=0; i<MAX_TDRS; i++)
    {
        track = &thisPTS[i];
        if ((track != NULL) &&
            (track->head >= 0) && (track->head < track->size) &&
            (track->tail >= 0) && (track->tail < track->size))
        {
            duration = CurrentUtcSecs() - track->pts[track->tail].timeSecs;

            while ((track->head != track->tail) &&
                   (duration > (thisMaxTime * 60)))
            {
                clear_segment(track, False);
                track->tail = (track->tail + 1) % track->size;
                duration = CurrentUtcSecs() - track->pts[track->tail].timeSecs;
            }
        }
    }
}

/**
 * This function clears the drawing area in order to force a redraw of the
 * initial or final line segment of the track.  It clears a square around the
 * end point and the next to end point (if one exists).  It then clears a
 * rectangle enclosing the final segment.  If the line segment wraps around
 * the display, it clears the  entire drawing if the line segment wraps.
 *
 * @param track realtime track data
 * @param head list head
 */
static void clear_segment(RtTrack *track, gint head)
{
    Area *area;
    PTimePoint pt1, pt2;
    Point p1, p2;
    gint height;
    gint width;
    gint temp;

    if (RTD_IsResizing()  == False &&
        RTD_IsIconified() == False &&
        track != NULL)
    {
        /* if clearing first segment of track... */
        if (head)
        {
            /* get first two points in queue */
            pt1 = &(track->pts[track->head]);
            if (track->head == track->tail)
            {
                pt2 = NULL;
            }
            else if (track->head > 0)
            {
                pt2 = &(track->pts[track->head - 1]);
            }
            else
            {
                pt2 = &(track->pts[track->size - 1]);
            }
        }

        /* else if clearing last segment of track ... */
        else
        {
            /* get last two points in queue */
            pt1 = &(track->pts[track->tail]);
            if (track->head == track->tail)
            {
                pt2 = NULL;
            }
            else if (track->tail < (track->size - 1))
            {
                pt2 = &(track->pts[track->tail + 1]);
            }
            else
            {
                pt2 = &(track->pts[0]);
            }
        }

        /* if there isn't a second point, or if it equals the first point... */
        if ((pt2 == NULL) ||
            ((pt1->pt[STATION].x == pt2->pt[STATION].x) &&
             (pt1->pt[STATION].y == pt2->pt[STATION].y)))
        {
            /* do nothing */
            ;
        }
        else
        {
            /* convert points to appropriate coordinates */
            gint coord = RTD_GetCoordinateType();
            if (coord != UNKNOWN_COORD)
            {
                p1.x = pt1->pt[coord].x;
                p1.y = pt1->pt[coord].y;
                p2.x = pt2->pt[coord].x;
                p2.y = pt2->pt[coord].y;

                /* if there is a screen wrap between the two points ... */
                if (PDH_CheckWrap(NULL, &p1, &p2, NULL, coord) != 0)
                {
                    /* clear the entire display */
                    RTD_DrawData();
                }
                else
                {
                    /* otherwise, do the normal clear routine */
                    /* do the label once, won't be different for the other coordinate types */
                    WindowData *windata = RTD_GetWindowData(coord);
                    if (windata != NULL)
                    {
                        PangoLayout *layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
                        pango_layout_set_text(layout, track->label, -1);
                        width  = FH_GetFontWidth(layout)  + 15;
                        height = FH_GetFontHeight(layout) + 15;
                        g_object_unref(layout);

                        /* get the screen position of first (or last) point */
                        WG_PointToWindow(coord,
                                         windata->drawing_area->allocation.width,
                                         windata->drawing_area->allocation.height,
                                         p1.x, p1.y, &p1.x, &p1.y);

                        /* get the screen position of the second point */
                        WG_PointToWindow(coord,
                                         windata->drawing_area->allocation.width,
                                         windata->drawing_area->allocation.height,
                                         p2.x, p2.y, &p2.x, &p2.y);

                        /* get the two points in order */
                        if (p1.x > p2.x)
                        {
                            temp = p1.x;
                            p1.x = p2.x;
                            p2.x = temp;
                        }
                        if (p1.y > p2.y)
                        {
                            temp = p1.y;
                            p1.y = p2.y;
                            p2.y = temp;
                        }

                        /* and add rectangle (including space for label at either point) */
                        /* to list of areas to be cleared */
                        area = RTD_GetArea(coord);
                        if (area != NULL)
                        {
                            D_AddArea(windata, area,
                                      p1.x - (width/2),    p1.y - (height/2),
                                      p2.x - p1.x + width, p2.y - p1.y + height);

                            if (area->count >= MAX_AREAS)
                            {
                                D_RefreshAreas(windata,
                                               AM_Drawable(windata,SRC_DRAWABLE), windata->track_data->pixmap,
                                               area, False);
                            }
                        }
                    }
                }
            }
        }
    }
}


/**
 * This function figures the closest position on any current TDRS event that is
 * either an original data point, or an interpolated data point that lies
 * within  a mask described by mask and that is nearest to the position (x,y).
 * IMPORTANT:  although this function returns a pointer to a PredictPt, the
 * calling code DOES NOT have control of the returned structure.  It may look
 * at it for data, but it should NEVER delete it or otherwise modify it.
 *
 * @param coord current coordinate type
 * @param width width of area
 * @param height height of area
 * @param window_x x location
 * @param window_y y location
 * @param x output x position
 * @param y output y position
 */
void RTDH_NearestPoint(gint coord, gint width, gint height, gint window_x, gint window_y, gint *x, gint *y)
{
    WG_PointToWindow(coord, width, height, window_x, window_y, x, y);
}

/**
 * Calulates the realtime point and time
 *
 * @param symbol symbol that has value
 * @param x output x value
 * @param y output y value
 * @param z output z value
 * @param pt_time time at point
 *
 * @return True or False
 */
gboolean RTDH_CalculateRtPoint(SymbolRec *symbol, gdouble *x, gdouble *y, gdouble *z, gchar *pt_time)
{
    if (symbol == NULL)
    {
        return False;
    }

    if (strlen(symbol->value_str) == 0)
    {
        return False;
    }

    if (sscanf(symbol->value_str, "%lf,%lf,%lf", x, y, z) != 3)
    {
        return False;
    }

    if (pt_time)
    {
        g_stpcpy(pt_time, CurrentUtc());
    }

    return True;
}

/**
 * This method adds a tracking point for the tdrs
 *
 * @param tdrs which tdrs
 * @param x x value
 * @param y y value
 * @param z z value
 * @param inView in view type
 */
void RTDH_AddPoint(gint tdrs, gdouble x, gdouble y, gdouble z, gint inView)
{
    gint az, el;                  /* coordinates of the new point */
    PTimePoint pt1;              /* the current head point */
    PTimePoint pt2;              /* the point before the current head */
    RtTrack *track;              /* the track to be added to */
    gchar timeStr[TIME_STR_LEN];  /* time of the new point */
    glong timeSecs;
    Point p1, p2;

    /* check if tracks initialized and selected valid track */
    if (! thisTracksInitialized || (tdrs < 0) || (tdrs >= MAX_TDRS))
    {
        return;
    }

    XYZtoStationAE(x, y, z, &az, &el );
    g_stpcpy(timeStr, CurrentGmt());
    timeSecs = StrToSecs(TS_GMT, timeStr);
    track = RTDH_GetTrack(tdrs);

    /* get point and make sure its valid */
    pt1 = &(track->pts[track->head]);
    if (pt1 == NULL)
    {
        return;
    }
    p1  = pt1->pt[STATION];

    /* if track in view / selected state changes, clear its history */
    if (pt1->inView != inView)
    {
        track->tail = track->head;
    }

    /* check if new point changed enough from previous point */
    if ((abs(p1.x - az) > 10) || (abs(p1.y - el) > 10) ||
        (pt1->inView != inView) || (pt1->timeSecs != timeSecs))
    {
        /* check if track contains at least one point */
        if (track->pts[track->head].timeStr[0] != '\0')
        {
            if (track->head == track->tail)
            {
                pt2 = NULL;
            }
            else
            {
                /* get second point */
                if (track->head > 0)
                {
                    pt2 = &(track->pts[track->head - 1]);
                }
                else
                {
                    pt2 = &(track->pts[track->size - 1]);
                }
                p2 = pt2->pt[STATION];
            }

            clear_segment(track, True);

            /* advance queue head (to add new point as opposed to overwriting
               current head) if... */
            /* ... there is no room (head == tail) */
            if (pt2 == NULL)
            {
                advance_head(track);
            }

            /* ... or the inView status has changed with this new point */
            else if (inView != pt1->inView)
            {
                advance_head(track);
            }

            /* ... or the inView status changed with the last point */
            else if (pt1->inView != pt2->inView)
            {
                advance_head(track);
            }

            /* or if the point has moved at least 2 degrees in some direction */
            else if ((abs(p1.x - p2.x) > 20) || (abs(p1.y - p2.y) > 20))
            {
                advance_head(track);
            }

            /* or if the current head is at least a minute old */
            else if (pt1->timeSecs - pt2->timeSecs >= 60)
            {
                advance_head(track);
            }
        }

        pt1 = &(track->pts[track->head]);

        XYZtoGenericAE(x, y, z, &(pt1->pt[STATION].x), &(pt1->pt[STATION].y));
        XYZtoSBandAE(x, y, z, &(pt1->pt[SBAND1].x), &(pt1->pt[SBAND1].y));

        /* use sband1 for sband2 */
        pt1->pt[SBAND2].x = pt1->pt[SBAND1].x;
        pt1->pt[SBAND2].y = pt1->pt[SBAND1].y;

        XYZtoKuBand(x, y, z, &(pt1->pt[KUBAND].x), &(pt1->pt[KUBAND].y), KUBAND);
        XYZtoKuBand(x, y, z, &(pt1->pt[KUBAND2].x), &(pt1->pt[KUBAND2].y), KUBAND2);

        pt1->inView = inView;
        g_stpcpy(pt1->timeStr, timeStr);
        pt1->timeSecs = timeSecs;
        clear_segment(track, True);
    }
}

/**
 * This function advances the position of the head of the point queue by one.
 * If this would put the head on top of the tail, it first increases the size
 * of the queue to avoid overlap.
 *
 * @param track realtime track
 */
static void advance_head(RtTrack *track)
{
    if (((track->head + 1) == track->tail) ||
        ((track->head == (track->size - 1)) && (track->tail == 0)))
        copy_rt_track(track, track->size + 10);

    track->head = (track->head + 1) % track->size;
}

/**
 * This function replaces the queue in track with a queue of size "size".  It
 * assumes that the new size is large enough to hold all of the data in the
 * queue.  The new queue is rotated so that the tail is the first point in the
 * array, and the head is the last point with data.
 *
 * @param track realtime track
 * @param size how many tracks
 */
static void copy_rt_track(RtTrack *track, gint size)
{
    gint i, n;
    PTimePoint pts;

    pts = (PTimePoint) MH_Calloc(sizeof(TimePoint) * size, __FILE__, __LINE__);

    n = 0;
    for (i = track->tail; i != track->head; i = (i + 1) % track->size)
    {
        memcpy( (void *)&(pts[n]), (void *)&(track->pts[i]), sizeof(TimePoint) );
        n++;
    }
    memcpy( (void *)&(pts[n]), (void *)&(track->pts[track->head]), sizeof(TimePoint) );

    MH_Free(track->pts);
    track->pts = pts;
    track->tail = 0;
    track->head = n;
    track->size = size;
}

/**
 * This method clears all track data for the dialog. It does not change
 * which tracks area displayed, it simply alters the data so the each track
 * contains at most one point
 */
void RTDH_ClearAll(void)
{
    gint i;
    for (i=0; i<MAX_TDRS; i++)
    {
        if (thisPTS[i].head != thisPTS[i].tail)
        {
            thisPTS[i].tail = thisPTS[i].head;
        }
    }
}

/**
 * Loads the tdrs label
 *
 * @param tdrs which tdrs
 * @param value value at tdrs
 */
void RTDH_SetTdrsLabel(gint tdrs, gchar *value)
{
    if (tdrs == TD_TDRE || TD_TDRW == TD_TDRW)
    {
        g_stpcpy(thisPTS[tdrs].label, value);
    }
}

/**
 * This method processes all incoming data value messages from ISP. It saves the
 * value state information for the given msid, then dispatches the request to
 * the appropriate component. If the msid 'value' changed from the last
 * cycle, then the data value is updated.
 *
 * @param symbol load this symbol
 */
void RTDH_SetValue(SymbolRec *symbol)
{
    if (RTD_IsIconified() == False)
    {
        /* load the value into the msid string buffer */
        switch (symbol->symbol_id)
        {
            case IS_2BJointAngle:
            case IS_4BJointAngle:
            case IS_2AJointAngle:
            case IS_4AJointAngle:
            case IS_PAJointAngle:
            case IS_SAJointAngle:
            case IS_1AJointAngle:
            case IS_3AJointAngle:
            case IS_1BJointAngle:
            case IS_3BJointAngle:
            case IS_PTCSJointAngle:
            case IS_STCSJointAngle:
                {
                    gint angle_index = get_angle_index(symbol->symbol_id);
                    if (angle_index != -1)
                    {
                        thisJointAngle.angleValue[angle_index] = symbol->value;
                    }
                }
                break;

            default:
                break;
        }

        /* now set the value */
        RTRFD_SetValue(symbol);
    }
}

/**
 * Converts the symbol id to an index for the joint angle data
 *
 * @param symbol_id symbol id value
 *
 * @return angle_index or -1
 */
static gint get_angle_index(gint symbol_id)
{
    gint angle_index = -1;

    switch (symbol_id)
    {
        case IS_2BJointAngle:
            angle_index = PORT_ANGLE_2B;
            break;

        case IS_4BJointAngle:
            angle_index = PORT_ANGLE_4B;
            break;

        case IS_2AJointAngle:
            angle_index = PORT_ANGLE_2A;
            break;

        case IS_4AJointAngle:
            angle_index = PORT_ANGLE_4A;
            break;

        case IS_PAJointAngle:
            angle_index = PORT_ANGLE_PA;
            break;

        case IS_SAJointAngle:
            angle_index = STBD_ANGLE_SA;
            break;

        case IS_1AJointAngle:
            angle_index = STBD_ANGLE_1A;
            break;

        case IS_3AJointAngle:
            angle_index = STBD_ANGLE_3A;
            break;

        case IS_1BJointAngle:
            angle_index = STBD_ANGLE_1B;
            break;

        case IS_3BJointAngle:
            angle_index = STBD_ANGLE_3B;
            break;

        case IS_PTCSJointAngle:
            angle_index = PORT_ANGLE_PTCS;
            break;

        case IS_STCSJointAngle:
            angle_index = STBD_ANGLE_STCS;
            break;

        default:
            break;
    }

    return angle_index;
}
