/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <fcntl.h>

#include <glib.h> /* Include early to get G_OS_WIN32 etc */
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <unistd.h>
#endif

#ifdef G_OS_WIN32
    #include <ctype.h>

    #ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
    #endif

    #include <direct.h>
    #include <io.h>
    #include <winsock2.h>        /* For gethostname */
    #include <ws2tcpip.h>

    #include <tchar.h>

    #ifndef S_IRUSR
    #define S_IRUSR S_IREAD
    #endif

    #ifndef S_IWUSR
    #define S_IWUSR S_IWRITE
    #endif

    #ifndef S_IXUSR
    #define S_IXUSR S_IEXEC
    #endif
#endif

#include "gdk/gdkkeysyms.h"
#include "gtk/gtkprivate.h"
#include "gtk/gtk.h"

#define PRIVATE
#include "gtkjfilechooser.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/gtkjfilechooser.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");

#undef PRIVATE

/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* initial width and height */
#define DIALOG_WIDTH    500
#define DIALOG_HEIGHT   325

#define BUFSIZE            MAX_PATH
#define FILESYSNAMEBUFSIZE MAX_PATH

enum Properties
{
    PROP_0,
    PROP_SHOW_FILEOPS,
    PROP_FILENAME,
    PROP_SELECT_MULTIPLE
};

enum ColumnIndex
{
    ICON_COL,
    FOLDER_COL,
    SIZE_COL,
    TYPE_COL,
    MODIFIED_COL,
    ATTRIBUTES_COL
};

/* parent class instance */
static GtkWindowClass       *thisParentClass    = NULL;

/* the current file filter pattern */
static GPatternSpec         *thisPattern        = NULL;

/* container widgets for the preview */
static GtkWidget            *thisPreviewTable   = NULL;
static GtkWidget            *thisPreviewContainer=NULL;

static gint                  thisMenuPosition    = -1;

GtkWidget *jfile_chooser_new_with_parent(GtkWindow *parent, const gchar *title);
void gtk_jfile_chooser_set_dialog_title(GtkJFileChooser *jfile_chooser, const gchar *title);

/*************************************************************************/

/**
 * Instantiate a file chooser dialog with a title only.
 *
 * @param title dialog title
 *
 * @return file chooser dialog
 */
GtkWidget *gtk_jfile_chooser_new(const gchar *title)
{
    return GTK_WIDGET(jfile_chooser_new_empty(NULL, title, (GtkDialogFlags)0));
}

/**
 * Instantiate a file chooser dialog centered and owned by the parent with a title.
 *
 * @param parent the parent for this dialog
 * @param title dialog title
 *
 * @return file chooser dialog
 */
GtkWidget *jfile_chooser_new_with_parent(GtkWindow *parent, const gchar *title)
{
    return GTK_WIDGET(jfile_chooser_new_empty(parent, title, (GtkDialogFlags)0));
}

/**
 * Instantiate a file chooser dialog centered and owned by the parent, with a title,
 * and using the dialog flags.
 *
 * @param parent the parent for this dialog
 * @param title dialog title
 * @param flags GtkDialogFlags - GTK_DIALOG_MODAL,
 *                               GTK_DIALOG_DESTROY_WITH_PARENT,
 *                               GTK_DIALOG_NO_SEPARATOR
 *
 * @return file chooser dialog
 */
GtkWidget *gtk_jfile_chooser_new_with_all(GtkWindow *parent, const gchar *title, GtkDialogFlags flags)
{
    return GTK_WIDGET(jfile_chooser_new_empty(parent, title, flags));
}

/**
 * Instantiate a file chooser dialog with a title only.
 *
 * @param parent the parent component this dialog belongs to
 * @param title dialog title
 * @param flags GtkDialogFlags - GTK_DIALOG_MODAL,
 *                               GTK_DIALOG_DESTROY_WITH_PARENT,
 *                               GTK_DIALOG_NO_SEPARATOR
 *
 * @return file chooser dialog
 */
static GtkWidget* jfile_chooser_new_empty(GtkWindow *parent, const gchar *title, GtkDialogFlags flags)
{
    GtkJFileChooser *jfile_chooser;

    jfile_chooser = (GtkJFileChooser*)g_object_new(GTK_TYPE_JFILE_CHOOSER, NULL);

    if (title)
    {
        gtk_window_set_title(GTK_WINDOW(jfile_chooser), title);
    }

    if (parent)
    {
        gtk_window_set_transient_for(GTK_WINDOW(jfile_chooser), parent);
    }

    if (flags & GTK_DIALOG_MODAL)
    {
        gtk_window_set_modal(GTK_WINDOW(jfile_chooser), TRUE);
    }

    if (flags & GTK_DIALOG_DESTROY_WITH_PARENT)
    {
        gtk_window_set_destroy_with_parent(GTK_WINDOW(jfile_chooser), TRUE);
    }

    if (flags & GTK_DIALOG_NO_SEPARATOR)
    {
        gtk_dialog_set_has_separator(GTK_DIALOG(jfile_chooser), FALSE);
    }

    return GTK_WIDGET(jfile_chooser);
}

/**
 * Initializes the file chooser class type.
 *
 * @return file_chooser_info
 */
GType gtk_jfile_chooser_get_type (void)
{
    static GType file_chooser_type = 0;

    if (!file_chooser_type)
    {
        static const GTypeInfo file_chooser_info =
        {
            sizeof (GtkJFileChooserClass),
            NULL,       /* base_init */
            NULL,       /* base_finalize */
            (GClassInitFunc) jfile_chooser_class_init,
            NULL,       /* class_finalize */
            NULL,       /* class_data */
            sizeof (GtkJFileChooser),
            0,          /* n_preallocs */
            (GInstanceInitFunc) jfile_chooser_init,
        };

        file_chooser_type = g_type_register_static (GTK_TYPE_DIALOG,
                                                    "GtkJFileChooser",
                                                    &file_chooser_info,
                                                    (GTypeFlags)0);
    }

    return file_chooser_type;
}

/**
 * Sets the title for the dialog
 *
 * @param jfile_chooser this instance
 * @param title the dialog title
 */
void gtk_jfile_chooser_set_dialog_title(GtkJFileChooser *jfile_chooser, const gchar *title)
{
    gtk_window_set_title(GTK_WINDOW(jfile_chooser), title);
}

/**
 * Takes the given file and takes the directory part. Opens the directory and selects the
 * file. If the file does not have a directory part, the users home directory is opened;
 * and if the file is not found in the user's home directory, nothing happens.
 *
 * @param jfile_chooser this instance
 * @param file the file to select
 */
void gtk_jfile_chooser_set_current_directory(GtkJFileChooser *jfile_chooser, gchar *file)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    /* verify the file is valid */
    if (file != NULL && file[0] != '\0')
    {
        /* is the directory valid ? */
        struct stat buf;
        if (stat(file, &buf) == 0)
        {
            class_data->base_dir = g_strdup(file);
        }
    }
}

/**
 * Returns the currently selected directory
 *
 * @param jfile_chooser this instance
 *
 * @return currently selected directory
 */
gchar *gtk_jfile_chooser_get_current_directory(GtkJFileChooser *jfile_chooser)
{
    GtkLookinOptionMenuData *menu_data;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser), NULL);

    menu_data = jfile_chooser_get_data(jfile_chooser);
    if (menu_data != NULL)
    {
        return g_path_get_dirname(menu_data->pathname);
    }

    return NULL;
}

/**
 * Adds a file selection pattern to use. The default is 'All Files'.
 *
 * @param jfile_chooser this instance
 * @param pattern something that will be compiled into a pattern spec
 * @param pattern_description an option description for the pattern. If the
 * description is NULL, then the pattern string will be used.
 */
void gtk_jfile_chooser_add_file_filter(GtkJFileChooser *jfile_chooser, const gchar *pattern, const gchar *pattern_description)
{
    GtkWidget *menu;
    GtkWidget *menu_item;
    FileFilter *file_filter;
    GtkJFileChooserClassData *class_data;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (jfile_chooser));

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    file_filter = (FileFilter*)g_new(FileFilter, 1);

    file_filter->pattern = g_pattern_spec_new(pattern);
    file_filter->pattern_string = g_strdup(pattern);
    if (pattern_description != NULL &&
        strlen(pattern_description) > 0)
    {
        file_filter->description = g_strdup(pattern_description);
    }
    else
    {
        file_filter->description = g_strdup(pattern);
    }
    class_data->file_filter = g_list_append(class_data->file_filter, file_filter);

    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->filter_option_menu));
    if (menu != NULL)
    {
        menu_item = gtk_menu_item_new_with_label(file_filter->description);
        g_object_set_data(G_OBJECT(menu_item), FILTER_KEY, file_filter->description);
        gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
        gtk_widget_show_all(menu_item);
    }
}

/**
 * Returns the current file filter in use.
 *
 * @param jfile_chooser this instance
 *
 * @return pattern spec
 */
const gchar *gtk_jfile_chooser_get_file_filter(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data;
    GtkWidget *menu;
    GtkWidget *menu_item;
    GtkWidget *label;

    g_return_val_if_fail (GTK_IS_JFILE_CHOOSER (jfile_chooser), NULL);

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->filter_option_menu));
    if (menu != NULL)
    {
        menu_item = gtk_menu_get_active(GTK_MENU(menu));
        if (menu_item != NULL)
        {
            label = GTK_BIN(menu_item)->child;
            if (label != NULL)
            {
                return gtk_label_get_text(GTK_LABEL(label));
            }
        }
    }

    return NULL;
}

/**
 * Enables or disables GTK_SELECTION_MULTIPLE for the view.
 *
 * @param jfile_chooser this class instance
 * @param selection TRUE for GTK_SELECTION_MULTIPLE enabled otherwise FALSE
 * for GTK_SELECTION_SINGLE.
 */
void gtk_jfile_chooser_set_multi_selection(GtkJFileChooser *jfile_chooser, gboolean selection)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    GtkTreeSelection *tree_selection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(class_data->tree_view)));
    if (selection == (gboolean)TRUE)
    {
        gtk_tree_selection_set_mode(tree_selection, GTK_SELECTION_MULTIPLE);
    }
}

/**
 * Determines if GTK_SELECTION_MULTIPLE is enabled or not.
 *
 * @param jfile_chooser this class instance
 *
 * @return TRUE if GTK_SELECTION_MULTIPLE is enabled otherwise FALSE
 */
gboolean gtk_jfile_chooser_is_multi_selection_enabled(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    GtkTreeSelection *tree_selection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(class_data->tree_view)));
    if (gtk_tree_selection_get_mode(tree_selection) == GTK_SELECTION_MULTIPLE)
    {
        return TRUE;
    }

    return FALSE;
}

/**
 * Sets the view mode. Default is FILE_AND_DIRECTORIES.
 *
 * @param jfile_chooser this class instance
 * @param view_mode One of FILES_ONLY, DIRECTORIES_ONLY, or FILE_AND_DIRECTORIES.
 */
void gtk_jfile_chooser_set_file_selection_mode(GtkJFileChooser *jfile_chooser, GtkJFileChooserViewMode view_mode)
{
    GtkJFileChooserClassData *class_data;

    g_return_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser));
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    class_data->view_mode = view_mode;
    if (view_mode != GTK_FILES_ONLY            &&
        view_mode != GTK_DIRECTORIES_ONLY      &&
        view_mode != GTK_FILE_AND_DIRECTORIES)
    {
        class_data->view_mode = GTK_FILE_AND_DIRECTORIES;
    }
}

/**
 * Returns the currently set view mode.
 *
 * @param jfile_chooser this class instance
 *
 * @return One of FILES_ONLY, DIRECTORIES_ONLY, or FILE_AND_DIRECTORIES.
 */
GtkJFileChooserViewMode gtk_jfile_chooser_get_file_selection_mode(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser), GTK_UNKNOWN_FILE_SELECTION);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    return class_data->view_mode;
}

/**
 * Returns the current item found in the entry widget. If this method is
 * called while mulit-selection is on and the entry widget has mutliple
 * files showing, the last file showing is returned.
 *
 * @param jfile_chooser this instance
 *
 * @return selected filename or NULL if none showing
 */
gchar *gtk_jfile_chooser_get_file(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;
    gchar *filename = NULL;
    GList *glist;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser), NULL);

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    menu_data = jfile_chooser_get_data(jfile_chooser);
    if (menu_data != NULL)
    {
        glist = class_data->selected_filenames;
        if (g_list_length(glist) == 1)
        {
            filename = (gchar *)g_malloc((gulong)strlen(menu_data->pathname)+
                                         strlen(G_DIR_SEPARATOR_S)+
                                         strlen((gchar*)glist->data)+1);
            g_sprintf(filename, "%s%s%s",
                    menu_data->pathname,
                    G_DIR_SEPARATOR_S,
                    (gchar*)glist->data);
        }
        else
        {
            filename = g_strdup(menu_data->pathname);
        }
    }

    return filename;
}

/**
 * Returns the list of files found in the entry widget.
 *
 * @param jfile_chooser this class instance
 *
 * @return list of files or NULL if none found
 */
gchar **get_jfile_chooser_get_files(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data;
    gchar **filename = NULL;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser), NULL);

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    return filename;
}

/**
 * Sets file hiding on or off. If True, hidden files are not shown in the file chooser.
 *
 * @param jfile_chooser this instance
 * @param enable TRUE = do not show hidden files
 *               FALSE = show all files
 */
void gtk_jfile_chooser_set_hiding_enabled(GtkJFileChooser *jfile_chooser, gboolean enable)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    class_data->hiding_enabled = enable;
}

/**
 * Returns True if hidden files are not shown in the file chooser; otherwise, returns False.
 *
 * @param jfile_chooser this instance
 *
 * @return TRUE hidden files are not being shown, otherwise FALSE, they are
 */
gboolean gtk_jfile_chooser_is_hiding_enabled(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    return class_data->hiding_enabled;
}

/**
 * Adds the preview accessory to the right of the file selection area
 *
 * @param jfile_chooser this instance
 * @param preview the preview component
 */
void gtk_jfile_chooser_set_preview_accessory(GtkJFileChooser *jfile_chooser, GtkWidget *preview)
{
    /* preview goes here */
    if (thisPreviewContainer == NULL)
    {
        thisPreviewContainer = gtk_frame_new(NULL);

        gtk_table_attach(GTK_TABLE(thisPreviewTable), thisPreviewContainer, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_EXPAND_FILL, 2, 2);
    }

    gtk_container_add(GTK_CONTAINER(thisPreviewContainer), preview);
    gtk_widget_show_all(thisPreviewContainer);
}

/**
 * Initializes the file chooser class
 *
 * @param klass jfile chooser class data
 */
static void jfile_chooser_class_init(GtkJFileChooserClass *klass)
{
    GObjectClass *gobject_class;
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;

    gobject_class = (GObjectClass*) klass;
    object_class  = (GtkObjectClass*) klass;
    widget_class  = (GtkWidgetClass*) klass;

    thisParentClass = (GtkWindowClass*)g_type_class_peek_parent (klass);

    gobject_class->finalize     = jfile_chooser_finalize;
    gobject_class->set_property = jfile_chooser_set_property;
    gobject_class->get_property = jfile_chooser_get_property;

    object_class->destroy = dialog_destroy_cb;

    widget_class->map = jfile_chooser_map;

    g_signal_new("update-preview",
                 G_OBJECT_CLASS_TYPE(object_class),
                 G_SIGNAL_RUN_LAST,
                 G_STRUCT_OFFSET(GtkJFileChooserClass, update_preview),
                 NULL, NULL,
                 g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0);
}

/**
 * File chooser finializer
 *
 * @param object the file chooser object
 */
static void jfile_chooser_finalize (GObject *object)
{
    g_return_if_fail (GTK_IS_JFILE_CHOOSER (object));
    G_OBJECT_CLASS (thisParentClass)->finalize(object);
}

/**
 * File chooser destroyer, releases memory.
 *
 * @param object the file chooser object
 */
static void dialog_destroy_cb(GtkObject *object)
{
    GtkJFileChooserClassData *class_data;
    FileFilter *file_filter;
    guint i;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER(object));

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(GTK_JFILE_CHOOSER(object));

    if (class_data->file_filter != NULL)
    {
        for (i=0; i<g_list_length(class_data->file_filter); i++)
        {
            file_filter = (FileFilter*)g_list_nth_data(class_data->file_filter, i);

            if (file_filter->pattern != NULL)
            {
                g_pattern_spec_free(file_filter->pattern);
                file_filter->pattern = NULL;
            }

            if (file_filter->pattern_string != NULL)
            {
                g_free(file_filter->pattern_string);
                file_filter->pattern_string = NULL;
            }

            if (file_filter->description != NULL)
            {
                g_free(file_filter->description);
                file_filter->description = NULL;
            }

            g_free(file_filter);
        }

        g_list_free(class_data->file_filter);
        class_data->file_filter = NULL;
    }

    if (class_data->selected_filenames)
    {
        g_list_free(class_data->selected_filenames);
        class_data->selected_filenames = NULL;
    }

    if (class_data->base_dir)
    {
        g_free(class_data->base_dir);
        class_data->base_dir = NULL;
    }

    GTK_OBJECT_CLASS (thisParentClass)->destroy (object);
}

/**
 * Instantiates the file chooser class data.
 *
 * @return file chooser class data
 */
static GtkJFileChooserClassData *jfile_chooser_class_data_init(void)
{
    GtkJFileChooserClassData *class_data = g_new(GtkJFileChooserClassData, 1);
    FileFilter *file_filter;

    class_data->base_dir = g_strdup(g_get_home_dir());

    class_data->lookin_option_menu = NULL;

    class_data->ok_button = NULL;
    class_data->cancel_button = NULL;
    class_data->selection_entry = NULL;
    class_data->tree_view = NULL;

    class_data->filter_option_menu = NULL;

    class_data->file_filter = NULL;
    file_filter = g_new(FileFilter, 1);
    file_filter->pattern_string = g_strdup("*");
    thisPattern = file_filter->pattern = g_pattern_spec_new(file_filter->pattern_string);
    file_filter->description = g_strdup("All Files");
    class_data->file_filter = g_list_append(class_data->file_filter, file_filter);

    class_data->view_model = GTK_LIST_VIEW_MODEL;
    class_data->view_mode = GTK_FILE_AND_DIRECTORIES;

    class_data->selected_filenames = NULL;

    class_data->hiding_enabled = TRUE;

    return class_data;
}

/**
 * Builds the gui interface that makes up the file chooser
 *
 * @param jfile_chooser what it is
 */
static void jfile_chooser_init(GtkJFileChooser *jfile_chooser)
{
    GtkDialog *dialog;
    GtkJFileChooserClassData *class_data;
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *scrolled_window;
    GtkWidget *menu;
    GtkWidget *menu_item;
    GtkWidget *table;
    GtkTooltips *tooltips;
    FileFilter *file_filter;

    gtk_widget_push_composite_child();

    dialog = GTK_DIALOG(jfile_chooser);
    class_data = jfile_chooser_class_data_init();

    gtk_widget_set_name(GTK_WIDGET(dialog), "file-chooser");
    gtk_window_set_default_size(GTK_WINDOW(dialog), DIALOG_WIDTH, DIALOG_HEIGHT);
    g_signal_connect(dialog, "destroy", G_CALLBACK (dialog_destroy_cb), jfile_chooser);
    g_signal_connect(dialog, "response", G_CALLBACK(dialog_response_cb), NULL);

    /*  The OK button  */
    class_data->ok_button = gtk_dialog_add_button(dialog,
                                                  GTK_STOCK_OK,
                                                  GTK_RESPONSE_OK);

    /*  The Cancel button  */
    class_data->cancel_button = gtk_dialog_add_button(dialog,
                                                      GTK_STOCK_CANCEL,
                                                      GTK_RESPONSE_CANCEL);

    gtk_widget_grab_default(class_data->ok_button);

    tooltips = gtk_tooltips_new();

    /* create box to hold all components */
    vbox = gtk_vbox_new(FALSE, 0);
    {
        /* create the 'look in' drop down box */
        hbox = gtk_hbox_new(FALSE, 0);
        {
            label = gtk_label_new("Look In:");
            gtk_widget_show(label);
            gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

            /* option menu, map callback will populate it */
            class_data->lookin_option_menu = gtk_option_menu_new();
            gtk_box_pack_start(GTK_BOX(hbox), class_data->lookin_option_menu, TRUE, FALSE, 2);
            class_data->option_menu_handler_id = g_signal_connect(class_data->lookin_option_menu, "changed", G_CALLBACK(lookin_option_menu_changed_cb), jfile_chooser);

            /* icon buttons */
            button = get_button_icon(get_icon(ICON_UP_ONE_LEVEL));
            gtk_tooltips_set_tip(tooltips, button, "Up One Level", NULL);
            gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 2);
            g_signal_connect(button, "clicked", G_CALLBACK(up_one_level_button_clicked_cb), jfile_chooser);

            button = get_button_icon(get_icon(ICON_DESKTOP));
            gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 2);
            gtk_tooltips_set_tip(tooltips, button, "Home", NULL);
            g_signal_connect(button, "clicked", G_CALLBACK(home_button_clicked_cb), jfile_chooser);

            button = get_button_icon(get_icon(ICON_NEW_FOLDER));
            gtk_tooltips_set_tip(tooltips, button, "Create New Folder", NULL);
            gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 2);

            button = get_button_icon(get_icon(ICON_LIST));
            gtk_tooltips_set_tip(tooltips, button, "List", NULL);
            gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 2);
            g_signal_connect(button, "clicked", G_CALLBACK (list_button_clicked_cb), jfile_chooser);

            button = get_button_icon(get_icon(ICON_DETAILS));
            gtk_tooltips_set_tip(tooltips, button, "Details", NULL);
            gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 2);
            g_signal_connect(button, "clicked", G_CALLBACK (details_button_clicked_cb), jfile_chooser);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
        gtk_widget_show_all(hbox);

        thisPreviewTable = gtk_table_new(1, 1, TRUE);
        {
            /* scrolled view for the contents of selected item */
            scrolled_window = gtk_scrolled_window_new(NULL, NULL);
            {
                gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
                                               GTK_POLICY_AUTOMATIC,
                                               GTK_POLICY_AUTOMATIC);
                gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled_window),
                                                    GTK_SHADOW_ETCHED_IN);

                /* create the tree view, the model is created when the data is displayed */
                class_data->tree_view = gtk_tree_view_new();

                /* handle gdouble-click on row */
                g_signal_connect_after(class_data->tree_view, "row-activated", G_CALLBACK(tree_view_row_activated_cb), jfile_chooser);

                gtk_container_add(GTK_CONTAINER(scrolled_window), GTK_WIDGET(class_data->tree_view));
            }
            gtk_table_attach(GTK_TABLE(thisPreviewTable), scrolled_window, 0, 1, 0, 1, GAO_EXPAND_FILL, GAO_EXPAND_FILL, 2, 2);
            gtk_widget_show_all(scrolled_window);
        }
        gtk_box_pack_start(GTK_BOX(vbox), thisPreviewTable, TRUE, TRUE, 2);

        table = gtk_table_new(2, 2, FALSE);
        {
#define CENTER_ALIGN (float)0.5
#define LEFT_ALIGN   (float)0.05
            /* file name label and entry field */
            label = gtk_label_new_with_mnemonic("File _Name:");
            gtk_widget_show(label);
            gtk_misc_set_alignment(GTK_MISC(label), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GAO_FILL, GAO_NONE, 2, 0);

            class_data->selection_entry = gtk_entry_new();
            gtk_widget_show(class_data->selection_entry);
            gtk_table_attach(GTK_TABLE(table), class_data->selection_entry, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            g_signal_connect (class_data->selection_entry, "key-press-event",
                              G_CALLBACK (selection_entry_key_press_event_cb),
                              jfile_chooser);

            g_signal_connect (class_data->selection_entry, "insert-text",
                              G_CALLBACK (selection_entry_insert_text_cb),
                              jfile_chooser);

            g_signal_connect_swapped (class_data->selection_entry, "focus-in-event",
                                      G_CALLBACK (selection_entry_focus_in_event_cb),
                                      class_data->ok_button);

            g_signal_connect_swapped (class_data->selection_entry, "activate",
                                      G_CALLBACK (gtk_button_clicked),
                                      class_data->ok_button);

            /* file type filter */
            label = gtk_label_new_with_mnemonic("Files of _Type:");
            gtk_misc_set_alignment(GTK_MISC(label), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GAO_FILL, GAO_NONE, 2, 0);

            class_data->filter_option_menu = gtk_option_menu_new();
            menu = gtk_menu_new();

            file_filter = (FileFilter*)g_list_nth_data(class_data->file_filter, 0);
            menu_item = gtk_menu_item_new_with_label(file_filter->description);
            g_object_set_data(G_OBJECT(menu_item), FILTER_KEY, file_filter->description);

            gtk_option_menu_set_menu(GTK_OPTION_MENU(class_data->filter_option_menu), menu);
            gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
            gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
            gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
            g_signal_connect(class_data->filter_option_menu, "changed", G_CALLBACK(filter_option_menu_changed_cb), jfile_chooser);

            gtk_table_attach(GTK_TABLE(table), class_data->filter_option_menu, 1, 2, 1, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
            gtk_widget_show_all(class_data->filter_option_menu);

            gtk_widget_show_all(table);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);
    }
    gtk_widget_show_all(vbox);

    gtk_box_pack_start(GTK_BOX(dialog->vbox), vbox, TRUE, TRUE, 0);
    gtk_widget_show_all(dialog->vbox);

    /* save the class data with the file chooser instance */
    jfile_chooser->class_data = (GtkJFileChooserClassData*)class_data;

    gtk_widget_pop_composite_child();
}

/**
 * Returns the private class data associated with the file_chooser
 *
 * @param jfile_chooser this instance
 *
 * @return GtkJFileChooserClassData
 */
static GtkJFileChooserClassData *jfile_chooser_get_class_data(GtkJFileChooser *jfile_chooser)
{
    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(jfile_chooser), NULL);
    return(GtkJFileChooserClassData*)jfile_chooser->class_data;
}

/**
 * Creates a list liststore model. The data is layout in simple columns;
 * icon + foldername, no header.
 *
 * @param jfile_chooser this class instance
 *
 * @return model
 */
static GtkListStore *create_list_model(GtkJFileChooser *jfile_chooser)
{
    GtkListStore *model = NULL;
    GtkTreeViewColumn *column;
    GtkCellRenderer *renderer;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    /* remove old columns */
    file_list_clear(jfile_chooser);

    /* new model */
    model = gtk_list_store_new (1, FILE_LIST_INFO_TYPE);

    column = gtk_tree_view_column_new ();
    gtk_tree_view_column_set_title (column, "Name");

    renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_column_pack_start (column, renderer, FALSE);
    gtk_tree_view_column_set_cell_data_func (column, renderer, file_list_set_icon,
                                             NULL, NULL);

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_column_pack_start (column, renderer, TRUE);
    gtk_tree_view_column_set_cell_data_func (column, renderer, file_list_set_name,
                                             NULL, NULL);

    gtk_tree_view_append_column (GTK_TREE_VIEW (class_data->tree_view), column);

    /* turn column headers off */
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(class_data->tree_view), FALSE);

    gtk_tree_selection_set_select_function(gtk_tree_view_get_selection(GTK_TREE_VIEW(class_data->tree_view)),
                                           jfile_chooser_tree_select_func, jfile_chooser, NULL);
    return model;
}

/**
 * Creates a details liststore model. The name, size, type, and
 * modified column headings is created with headers.
 *
 * @param jfile_chooser this class instance
 *
 * @return model
 */
static GtkListStore *create_details_model(GtkJFileChooser *jfile_chooser)
{
    guint i;

    GtkListStore *model = NULL;
    GtkTreeViewColumn *column;
    GtkCellRenderer *renderer;
    GList *list;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    /* remove old columns */
    file_list_clear(jfile_chooser);

    /* new model */
    model = gtk_list_store_new (1, FILE_LIST_INFO_TYPE);

    column = gtk_tree_view_column_new ();
    gtk_tree_view_column_set_title (column, "Name");

    renderer = gtk_cell_renderer_pixbuf_new ();
    gtk_tree_view_column_pack_start (column, renderer, FALSE);
    gtk_tree_view_column_set_cell_data_func (column, renderer, file_list_set_icon,
                                             NULL, NULL);

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_column_pack_start (column, renderer, TRUE);
    gtk_tree_view_column_set_cell_data_func (column, renderer, file_list_set_name,
                                             NULL, NULL);

    gtk_tree_view_append_column (GTK_TREE_VIEW (class_data->tree_view), column);

    renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (class_data->tree_view),
                                                -1,
                                                "Size", renderer, file_list_set_size,
                                                NULL, NULL);

    gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (class_data->tree_view),
                                                -1,
                                                "Type", renderer, file_list_set_type,
                                                NULL, NULL);

    gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (class_data->tree_view),
                                                -1,
                                                "Modified", renderer, file_list_set_mod,
                                                NULL, NULL);

    gtk_tree_view_insert_column_with_data_func (GTK_TREE_VIEW (class_data->tree_view),
                                                -1,
                                                "Attibutes", renderer, file_list_set_attrib,
                                                NULL, NULL);

    /* turn column headers on */
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(class_data->tree_view), TRUE);

    list = gtk_tree_view_get_columns(GTK_TREE_VIEW(class_data->tree_view));
    for (i=0; i<g_list_length(list); i++)
    {
        column = (GtkTreeViewColumn*)g_list_nth_data(list, i);
        gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
        gtk_tree_view_column_set_resizable(column, TRUE);
    }
    g_list_free(list);

    return model;
}

/**
 * Registers a boxed type for the treeview model. This boxed type
 * encapulates the icon and name in the viewer
 *
 * @return GtkFileListInfo type
 */
static GType file_list_info_get_type (void)
{
    static GType our_type = 0;

    if (our_type == 0)
    {
        our_type = g_boxed_type_register_static ("GtkFileListInfo",
                                                 (GBoxedCopyFunc) file_list_info_copy,
                                                 (GBoxedFreeFunc) file_list_info_free);
    }

    return our_type;
}

/**
 * Releases the GtkFileListInfo type
 *
 * @param info release this memory
 */
static void file_list_info_free (GtkFileListInfo *info)
{
    g_free(info->name);
    g_free(info->size);
    g_free(info->type);
    g_free(info->modified);
    g_free(info->attributes);

    if (info->icon)
    {
        g_object_unref (info->icon);
    }

    g_free (info);
}

/**
 * Copies the GtkFileListInfo data to a newly allocated info.
 *
 * @param src copy this
 *
 * @return new GtkFileListInfo
 */
static GtkFileListInfo* file_list_info_copy(GtkFileListInfo *src)
{
    GtkFileListInfo *info;

    info = g_new(GtkFileListInfo, 1);

    info->name       = g_strdup(src->name);
    info->size       = g_strdup(src->size);
    info->type       = g_strdup(src->type);
    info->modified   = g_strdup(src->modified);
    info->attributes = g_strdup(src->attributes);

    info->icon = src->icon;
    if (info->icon)
    {
        g_object_ref (info->icon);
    }

    return info;
}

/**
 * Sets the icon for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_icon(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", info->icon, NULL);
        file_list_info_free (info);
    }
}

/**
 * Sets the folder name for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_name(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "text", info->name, "xalign", 0.0, NULL);
        file_list_info_free (info);
    }
}

/**
 * Sets the file size for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_size(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "text", info->size, "xalign", 1.0, NULL);
        file_list_info_free (info);
    }
}

/**
 * Sets the file type for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_type(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "text", info->type, "xalign", 0.0, NULL);
        file_list_info_free (info);
    }
}

/**
 * Sets the file modification date and time for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_mod(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "text", info->modified, "xalign", 0.0, NULL);
        file_list_info_free (info);
    }
}

/**
 * Sets the file attributes for the model.
 *
 * @param tree_column the column
 * @param cell the cell
 * @param model the model
 * @param iter the row/col
 * @param data user data
 */
static void file_list_set_attrib(GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data)
{
    GtkFileListInfo *info;

    gtk_tree_model_get (model, iter, 0, &info, -1);
    if (info)
    {
        g_object_set (GTK_CELL_RENDERER (cell), "text", info->attributes, "xalign", 0.0, NULL);
        file_list_info_free (info);
    }
}

/**
 * Processes the focus-in-event for the file chooser entry widget.
 *
 * @param widget the entry widget
 * @param event the event
 * @param data user data
 *
 * @return FALSE
 */
static gboolean selection_entry_focus_in_event_cb(GtkWidget *widget, GdkEventFocus *event, void * data)
{
    gtk_widget_grab_default (widget);
    return FALSE;
}

/**
 * Creates a button with an icon image
 *
 * @param image
 *
 * @return button
 */
static GtkWidget *get_button_icon(GtkWidget *image)
{
    GtkWidget *button;

    button = gtk_button_new();
    gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);

    gtk_container_add (GTK_CONTAINER(button), image);
    gtk_widget_show_all(button);

    return button;
}

/**
 * Gets a pixbuf representation of an icon
 *
 * @param which_icon the icon to retrieve and convert
 *
 * @return pixbuf to an icon
 */
static GdkPixbuf *get_pixbuf_icon(gint which_icon)
{
    return gtk_image_get_pixbuf(GTK_IMAGE(get_icon(which_icon)));
}

/**
 * Gets the icon used for the drop-down lookin menu. The icon
 * image is converted to a transparency image.
 *
 * @param which_icon the icon to retrieve
 *
 * @return icon image
 */
static GtkWidget *get_icon(gint which_icon)
{
    GdkPixbuf *pixbuf = NULL;
    GdkPixbuf *transparent;
    GtkWidget *image;

    switch (which_icon)
    {
        default:
        case ICON_FOLDER:
        case ICON_DIRECTORY:
            pixbuf = gdk_pixbuf_new_from_xpm_data(folder_icon_xpm);
            break;

        case ICON_COMPUTER:
            pixbuf = gdk_pixbuf_new_from_xpm_data(computer_icon_xpm);
            break;

        case ICON_DISK:
            pixbuf = gdk_pixbuf_new_from_xpm_data(disk_icon_xpm);
            break;

        case ICON_DESKTOP:
            pixbuf = gdk_pixbuf_new_from_xpm_data(desktop_icon_xpm);
            break;

        case ICON_UP_ONE_LEVEL:
            pixbuf = gdk_pixbuf_new_from_xpm_data(uponelevel_icon_xpm);
            break;

        case ICON_NEW_FOLDER:
            pixbuf = gdk_pixbuf_new_from_xpm_data(newfolder_icon_xpm);
            break;

        case ICON_LIST:
            pixbuf = gdk_pixbuf_new_from_xpm_data(list_icon_xpm);
            break;

        case ICON_DETAILS:
            pixbuf = gdk_pixbuf_new_from_xpm_data(details_icon_xpm);
            break;

        case ICON_DOCUMENT:
            pixbuf = gdk_pixbuf_new_from_xpm_data(document_icon_xpm);
            break;
    }
    /* make it transparent */
    transparent = gdk_pixbuf_add_alpha (pixbuf, TRUE, 0xff, 0xff, 0xff);
    image = gtk_image_new_from_pixbuf(transparent);

#define ICON_SIZE 16
    gtk_widget_set_size_request(image, ICON_SIZE, ICON_SIZE);

    return image;
}

/**
 * File chooser property initializer
 *
 * @param object file chooser object
 * @param prop_id property id
 * @param value property value
 * @param pspec property spec
 */
static void jfile_chooser_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    switch (prop_id)
    {
        case PROP_FILENAME:
            break;

        case PROP_SHOW_FILEOPS:
            break;

        case PROP_SELECT_MULTIPLE:
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

/**
 * File chooser property
 *
 * @param object file chooser object
 * @param prop_id property id
 * @param value property value
 * @param pspec property spec
 */
static void jfile_chooser_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
    switch (prop_id)
    {
        case PROP_FILENAME:
            break;

        case PROP_SHOW_FILEOPS:
            break;

        case PROP_SELECT_MULTIPLE:
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

/**
 * Maps the file chooser instance.
 *
 * @param widget file chooser object
 */
static void jfile_chooser_map(GtkWidget *widget)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;

    g_return_if_fail(GTK_IS_JFILE_CHOOSER(widget));

    jfile_chooser = GTK_JFILE_CHOOSER (widget);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    /* create the menu items and add it to the option menu */
    jfile_chooser_add_menu_items(jfile_chooser);

    /* set the initial directory */
    jfile_chooser_set_dir(jfile_chooser, class_data);

    /* show the dialog */
    gtk_widget_show_all(class_data->lookin_option_menu);

    GTK_WIDGET_CLASS (thisParentClass)->map (widget);
}

/**
 * Processes the key press event for the file entry field.
 *
 * @param widget the entry widget
 * @param event the event itself
 * @param data user data
 *
 * @return TRUE or FALSE
 */
static gint selection_entry_key_press_event_cb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;
    gchar *basename;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(data), FALSE);
    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    if ((event->keyval == GDK_Tab ||
         event->keyval == GDK_KP_Tab) &&
        (event->state & gtk_accelerator_get_default_mod_mask ()) == 0)
    {
        jfile_chooser = GTK_JFILE_CHOOSER (data);
        class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
        basename = g_strdup (gtk_entry_get_text (GTK_ENTRY (class_data->selection_entry)));

        menu_data = jfile_chooser_make_data(jfile_chooser, basename);
        if (menu_data)
        {
            populate_filelist(jfile_chooser, menu_data);
            g_free (basename);
            return TRUE;
        }
    }

    return FALSE;
}

/**
 * Processes the insert-text event for the file chooser entry widget.
 *
 * @param widget the entry widget
 * @param new_text new text entered
 * @param new_text_length what it is
 * @param position the position of the cursor
 * @param data user data
 *
 * @return TRUE or FALSE
 */
static gint selection_entry_insert_text_cb(GtkWidget *widget, const gchar *new_text, gint new_text_length, gint *position, void * data)
{
    gchar *filename;

    filename = g_filename_from_utf8 (new_text, new_text_length, NULL, NULL, NULL);

    if (!filename)
    {
        gdk_display_beep (gtk_widget_get_display (widget));
        g_signal_stop_emission_by_name (widget, "insert_text");
        return FALSE;
    }

    g_free (filename);

    return TRUE;
}

/**
 * Handles the response from the gtk_dialog_run event
 *
 * @param dialog this dialog
 * @param response_id the response posted
 */
static void dialog_response_cb(GtkDialog *dialog, gint response_id)
{
    /* Act only on response IDs we recognize */
    if (!(response_id == GTK_RESPONSE_ACCEPT ||
          response_id == GTK_RESPONSE_OK     ||
          response_id == GTK_RESPONSE_YES    ||
          response_id == GTK_RESPONSE_APPLY))
        return;
}

/**
 * Processes the single-click. The 'paths_currently_selected' indicate which
 * items will be selected and those that are soon to be deselected. FALSE indicates
 * that the item will be selected, TRUE indicates it is currently selected and
 * will be deselected later. This routine builds the list of names selected.
 *
 * @param selection the tree selection
 * @param model the tree model
 * @param path the current tree path
 * @param path_currently_selected what it is
 * @param data this instance
 *
 * @return TRUE to continue the selection
 */
static gboolean jfile_chooser_tree_select_func(GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path, gboolean path_currently_selected, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;
    GtkTreeIter iter;
    GtkFileListInfo *info;
    GList *glist;
    gboolean found = FALSE;
    gboolean add_file = TRUE;

    g_return_val_if_fail(GTK_IS_JFILE_CHOOSER(data), FALSE);

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    gtk_tree_model_get_iter(model, &iter, path);
    gtk_tree_model_get(model, &iter, 0, &info, -1);

    g_message("jfile_chooser_tree_select_func: single-selection");

    /* get only those names that are not selected */
    if (path_currently_selected == FALSE)
    {
        /* before adding, see if its already in the list */
        glist = g_list_first(class_data->selected_filenames);
        while (glist)
        {
            if (g_ascii_strcasecmp((gchar *)glist->data, info->name) == 0)
            {
                found = TRUE;
                break;
            }
            glist = g_list_next(glist);
        }

        if (found == FALSE)
        {
            /* now build a file and see what it is */
            menu_data = jfile_chooser_make_data(jfile_chooser, info->name);
            if (menu_data)
            {
                add_file = TRUE;

                if ((class_data->view_mode == GTK_FILES_ONLY &&
                     is_dir_file(menu_data->pathname) == (gboolean)TRUE))
                {
                    add_file = FALSE;
                }

                if (add_file == (gboolean)TRUE)
                {
                    class_data->selected_filenames = g_list_append(class_data->selected_filenames, g_strdup(info->name));
                }
            }
        }
    }
    else
    {
        /* remove this item */
        glist = g_list_first(class_data->selected_filenames);
        while (glist)
        {
            if (g_ascii_strcasecmp((gchar *)glist->data, info->name) == 0)
            {
                found = TRUE;
                break;
            }
            glist = g_list_next(glist);
        }

        if (found == (gboolean)TRUE)
        {
            g_free(glist->data);
            class_data->selected_filenames = g_list_delete_link(class_data->selected_filenames, glist);
        }
    }

    /* load the entry with the names */
    jfile_chooser_add_filenames(GTK_JFILE_CHOOSER(data));

    return TRUE;
}

/**
 * Handles the gdouble-click event in the tree view. When the user gdouble clicks
 * a row, we want to interrogate the type of file it is. If its a regular file, place
 * it into the file selection entry widget and exit the dialog. If its a directory.
 * Open the directory, and list its contents into the filelist viewer. Then update
 * the option menu with the new item; either add or remove from the option menu.
 *
 * @param treeview the tree
 * @param path the item selected
 * @param column the column selected
 * @param data file_chooser instance data
 */
static void tree_view_row_activated_cb(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    gchar *filename;

    g_return_if_fail(GTK_IS_JFILE_CHOOSER(data));

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    g_message("tree_view_row_activated_cb: gdouble-click");

    /* get the currently selected file in the directory */
    filename = jfile_chooser_get_single_selection(path, column, jfile_chooser);

    /* now add the file to the menu */
    jfile_chooser_add_filename_to_menu(jfile_chooser, filename);

    /* load the entry with the names */
    jfile_chooser_add_filename(jfile_chooser, filename);

    g_free(filename);
}

/**
 * Adds the filename to the option menu
 *
 * @param jfile_chooser this instance
 * @param filename add this to the option menu
 */
static void jfile_chooser_add_filename_to_menu(GtkJFileChooser *jfile_chooser, gchar *filename)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    GtkLookinOptionMenuData *menu_data = NULL;
    GtkWidget *menu;
    GtkWidget *menu_item;
    gint pos;

    /* now process this entry */
    menu_data = jfile_chooser_make_data(jfile_chooser, filename);
    if (menu_data)
    {
        /* item must be directory (folder) */
        if (is_dir_file(menu_data->pathname))
        {
            /* are we adding or selecting */
            if (menu_data->add == (gboolean)TRUE)
            {
                menu_item = get_lookin_menu_item(FALSE,
                                                 GPOINTER_TO_INT(menu_data->displayname),
                                                 menu_data->displayname,
                                                 menu_data->pathname,
                                                 ICON_FOLDER,
                                                 menu_data->indent_level,
                                                 FALSE);

                if (menu_item != NULL)
                {
                    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->lookin_option_menu));
                    if (menu != NULL)
                    {
                        pos = gtk_option_menu_get_history(GTK_OPTION_MENU(class_data->lookin_option_menu));
                        gtk_menu_shell_insert(GTK_MENU_SHELL(menu), menu_item, pos+1);
                        gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
                        gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
                    }
                }
            }
            /* just select the item */
            else
            {
                menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->lookin_option_menu));
                if (menu != NULL)
                {
                    menu_item = jfile_chooser_get_menu_item(menu, filename);
                    if (menu_item != NULL)
                    {
                        gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
                        gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
                    }
                }
            }
        }
    }
}

/**
 * Returns the current filename selected.
 *
 * @param path location of selection
 * @param column the column selected
 * @param jfile_chooser this instance
 *
 * @return filename
 */
static gchar *jfile_chooser_get_single_selection(GtkTreePath *path, GtkTreeViewColumn *column, GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data;
    GtkFileListInfo *info;
    GtkTreeModel *model;
    GtkTreeIter iter;
    const gchar *filename;

    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    model = gtk_tree_view_get_model(GTK_TREE_VIEW(class_data->tree_view));
    gtk_tree_model_get_iter(model, &iter, path);

    gtk_tree_model_get(model, &iter, 0, &info, -1);
    filename = info->name;

    return g_strdup(filename);
}

/**
 * Loads the filename into the text entry widget.
 *
 * @param jfile_chooser this instance
 * @param filename the filename to load
 */
static void jfile_chooser_add_filename(GtkJFileChooser *jfile_chooser, gchar *filename)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    gtk_entry_set_text(GTK_ENTRY(class_data->selection_entry), filename);
    g_signal_emit_by_name(jfile_chooser, "update-preview");
}

/**
 * Loads multi filenames into the text entry widget.
 *
 * @param jfile_chooser this instance
 */
static void jfile_chooser_add_filenames(GtkJFileChooser *jfile_chooser)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    if (g_list_length(class_data->selected_filenames) == 1)
    {
        jfile_chooser_add_filename(jfile_chooser, (gchar *)class_data->selected_filenames->data);
    }
    else
    {
        GList *glist = class_data->selected_filenames;
        gchar name[MAXPATHLEN] = {0};
        gchar *filenames = NULL;

        while (glist)
        {
            g_sprintf(name, "\"%s\" ",(gchar*)glist->data);
            if (filenames != NULL)
            {
                filenames = g_strconcat(filenames, name, NULL);
            }
            else
            {
                filenames = g_strconcat(name, NULL);
            }
            glist = g_list_next(glist);
        }

        if (filenames != NULL)
        {
            jfile_chooser_add_filename(jfile_chooser, filenames);
        }
        else
        {
            jfile_chooser_add_filename(jfile_chooser, "");
        }
    }
}

/**
 * Processes the button click for up-one-level event
 *
 * @param button the button itself
 * @param data has file_chooser instance
 */
static void up_one_level_button_clicked_cb(GtkWidget *button, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;
    GtkWidget *menu;
    GtkWidget *menu_item;
    GList *glist;
    const gchar *key;
    gint indent_level = -1;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (data));

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->lookin_option_menu));
    if (menu != NULL)
    {
        menu_item = gtk_menu_get_active(GTK_MENU(menu));
        if (menu_item != NULL)
        {
            key = gtk_widget_get_name(menu_item);
            if (key)
            {
                menu_data = (GtkLookinOptionMenuData*)g_object_get_data(G_OBJECT(menu_item), key);
                if (menu_data)
                {
                    indent_level = menu_data->indent_level;
                }
            }
        }

        /* get children of menu */
        glist = gtk_container_children(GTK_CONTAINER(menu));
        while (glist != NULL)
        {
            if (GTK_IS_MENU_ITEM(glist->data))
            {
                key = gtk_widget_get_name(GTK_WIDGET(glist->data));
                if (key)
                {
                    menu_data = (GtkLookinOptionMenuData*)g_object_get_data(G_OBJECT(glist->data), key);
                    if (menu_data)
                    {
                        if (menu_data->indent_level == indent_level)
                        {
                            break;
                        }
                        menu_item = GTK_WIDGET(glist->data);
                    }
                }
            }
            glist = glist->next;
        }
        g_list_free (glist);

        if (menu_item)
        {
            gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
            gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
        }
    }
}

/**
 * Processes the button click for the list button.
 *
 * @param button the button itself
 * @param data has file_chooser instance
 */
static void list_button_clicked_cb(GtkWidget *button, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (data));

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    if (class_data->view_model != GTK_LIST_VIEW_MODEL)
    {
        class_data->view_model = GTK_LIST_VIEW_MODEL;

        /* get the currently selected item */
        menu_data = jfile_chooser_get_data(jfile_chooser);
        if (menu_data)
        {
            /* go populate the file list view */
            populate_filelist(jfile_chooser, menu_data);
        }
    }
}

/**
 * Processes the button click for the details button.
 *
 * @param button the button itself
 * @param data user data
 */
static void details_button_clicked_cb(GtkWidget *button, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (data));

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    if (class_data->view_model != GTK_DETAILS_VIEW_MODEL)
    {
        class_data->view_model = GTK_DETAILS_VIEW_MODEL;

        /* get the currently selected item */
        menu_data = jfile_chooser_get_data(jfile_chooser);
        if (menu_data)
        {
            /* go populate the file list view */
            populate_filelist(jfile_chooser, menu_data);
        }
    }
}

/**
 * Handles the callback for the pulldown selection. When moving off of a primary (keeper), we
 * remove it from the option list.
 *
 * @param option_menu GtkOptionMenu object
 * @param data user data
 */
static void lookin_option_menu_changed_cb(GtkOptionMenu *option_menu, void * data)
{
    gint current_pos;
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (data));

    /* if the option menu choice changed, then process */
    current_pos = gtk_option_menu_get_history(option_menu);
    if (thisMenuPosition != current_pos)
    {
        thisMenuPosition = current_pos;

        jfile_chooser = GTK_JFILE_CHOOSER(data);
        class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

        /* clear the selected_filenames list */
        g_list_foreach(class_data->selected_filenames, jfile_chooser_free_selected_filenames, NULL);
        g_list_free(class_data->selected_filenames);
        class_data->selected_filenames = NULL;

        /* get the currently selected item */
        menu_data = jfile_chooser_get_data(jfile_chooser);
        if (menu_data)
        {
            /* find any children and remove */
            jfile_chooser_remove_menu_item(GTK_WIDGET(option_menu), menu_data);

            /* go populate the file list view */
            populate_filelist(jfile_chooser, menu_data);
        }
    }
}

/**
 * Applies the file filter to the filelist.
 *
 * @param option_menu option menu
 * @param data user data
 */
static void filter_option_menu_changed_cb(GtkOptionMenu *option_menu, void * data)
{
    static gint last_pos = 0;

    GtkWidget *menu;
    GtkWidget *menu_item;
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkLookinOptionMenuData *menu_data;

    FileFilter *file_filter;
    gchar *label_string;
    gint current_pos;
    guint i;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER(data));

    /* if the option menu choice changed, then process */
    current_pos = gtk_option_menu_get_history(option_menu);
    if (last_pos != current_pos)
    {
        last_pos = current_pos;

        jfile_chooser = GTK_JFILE_CHOOSER(data);
        class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

        /* clear the selected_filenames list */
        g_list_foreach(class_data->selected_filenames, jfile_chooser_free_selected_filenames, NULL);
        g_list_free(class_data->selected_filenames);
        class_data->selected_filenames = NULL;

        menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->filter_option_menu));
        if (menu != NULL)
        {
            /* check the file against the current file filter */
            menu_item = gtk_menu_get_active(GTK_MENU(menu));
            if (menu_item != NULL)
            {
                label_string = (gchar*)g_object_get_data(G_OBJECT(menu_item), FILTER_KEY);
                for (i=0; i<g_list_length(class_data->file_filter); i++)
                {
                    file_filter = (FileFilter*)g_list_nth_data(class_data->file_filter, i);
                    if (g_ascii_strcasecmp(file_filter->description, label_string) == 0)
                    {
                        thisPattern = file_filter->pattern;
                        break;
                    }
                }
            }
        }

        /* get the currently selected directory item */
        menu_data = jfile_chooser_get_data(jfile_chooser);
        if (menu_data)
        {
            /* go populate the file list view with this filter */
            populate_filelist(jfile_chooser, menu_data);
        }
    }
}

/**
 * Releases memory in the GList that holds the selected filenames
 *
 * @param selected_filename data to free
 * @param data user data - not used
 */
static void jfile_chooser_free_selected_filenames(void * selected_filename, void * data)
{
    g_free(selected_filename);
}

/**
 * Creates a new chooser data structure with the basename loaded. Indent level is
 * also incremented. All other fields are the same as the current directory selection.
 *
 * @param jfile_chooser this class instance
 * @param basename the new filename to use
 *
 * @return new file_chooser data
 */
static GtkLookinOptionMenuData *jfile_chooser_make_data(GtkJFileChooser *jfile_chooser, gchar *basename)
{
    GtkLookinOptionMenuData *new_menu_data = NULL;
    GtkLookinOptionMenuData *menu_data = NULL;

    /* get the currently selected item */
    menu_data = jfile_chooser_get_data(jfile_chooser);
    if (menu_data)
    {
        new_menu_data = g_new(GtkLookinOptionMenuData, 1);
        g_stpcpy(new_menu_data->displayname, basename);
        g_stpcpy(new_menu_data->basename, basename);

        if (menu_data->pathname[strlen(menu_data->pathname)-1] == G_DIR_SEPARATOR)
        {
            g_sprintf(new_menu_data->pathname, "%s%s", menu_data->pathname, basename);
        }
        else
        {
            g_sprintf(new_menu_data->pathname, "%s%s%s", menu_data->pathname, G_DIR_SEPARATOR_S, basename);
        }
        new_menu_data->parent_key = menu_data->key;
        new_menu_data->indent_level = menu_data->indent_level+1;
        new_menu_data->add = TRUE;
    }

    return new_menu_data;
}

/**
 * Gets the currently selected item in the option menu, then retrieves
 * the corresponding file chooser data and makes a copy.
 *
 * @param jfile_chooser
 *
 * @return GtkLookinOptionMenuData
 */
static GtkLookinOptionMenuData *jfile_chooser_get_data(GtkJFileChooser *jfile_chooser)
{
    GtkWidget *menu;
    GtkWidget *selected_menu_item;
    const gchar *key;
    GtkLookinOptionMenuData *menu_data = NULL;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->lookin_option_menu));
    if (menu)
    {
        selected_menu_item = gtk_menu_get_active(GTK_MENU(menu));
        {
            key = gtk_widget_get_name(selected_menu_item);
            if (key)
            {
                menu_data = (GtkLookinOptionMenuData*)g_object_get_data(G_OBJECT(selected_menu_item), key);
            }
        }
    }

    return menu_data;
}

/**
 * Populates the file list
 *
 * @param jfile_chooser this class instance
 * @param menu_data current list of data in option menu
 */
static void populate_filelist(GtkJFileChooser *jfile_chooser, GtkLookinOptionMenuData *menu_data)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    GDir *dir;
    GError *error = NULL;
    struct stat sbuf;
    const gchar *fname;
    gboolean show_file = TRUE;
    GtkFileListData *filedata;
    GPtrArray *file_list_data = g_ptr_array_new();

    g_message("populate_filelist: pathname=%s",menu_data->pathname);

    dir = g_dir_open(menu_data->pathname, 0, &error);
    if (dir != NULL)
    {
        while ((fname = g_dir_read_name(dir)) != NULL)
        {
            /* add this file or not ? */
            gchar *tmpname = g_build_filename(menu_data->pathname, fname, NULL);

            show_file = jfile_chooser_show_file(class_data, tmpname);
            if (show_file == (gboolean)TRUE)
            {
                filedata = (GtkFileListData*)g_malloc(sizeof(GtkFileListData));
                if (stat(tmpname, &sbuf) == 0)
                {
                    filedata->size = sbuf.st_size;
                    filedata->modified = (gint)sbuf.st_mtime;
                    filedata->attributes = sbuf.st_mode;
                }

                g_stpcpy(filedata->basename, fname);
                filedata->file_type = get_file_type(tmpname);
                g_ptr_array_add(file_list_data, filedata);
            }
            g_free(tmpname);
        }

        g_dir_close(dir);
    }
    else
    {
        if (error != NULL)
        {
            g_warning(error->message);
            g_error_free(error);
        }
    }

#ifdef G_OS_WIN32
    win32_populate_filelist(jfile_chooser, menu_data, file_list_data);
#endif

    /* sort by directory */
    g_ptr_array_sort(file_list_data, file_list_compare);

    /* add file/dirs to view */
    add_files(jfile_chooser, file_list_data);

    /* release array of files */
    g_ptr_array_free(file_list_data, TRUE);
}

/**
 * Adds the array of data to the filelist view. Determines the file type of
 * the file and gets an appropriate icon. Then adds the icon with the name
 * of the file to a button. The data array is then sorted by directory and
 * name; then added to the view.
 *
 * @param jfile_chooser this instance
 * @param file_list_data array of data to add to the view
 */
static void add_files(GtkJFileChooser *jfile_chooser, GPtrArray *file_list_data)
{
    GtkTreeIter *row_iter;
    GtkListStore *model;
    GtkFileListData *filedata;
    GtkFileListInfo info;
    guint i;
    guint col = 0;
    guint row = 0;
    guint max_rows = 0;
    guint max_cols = 0;
    gint w, h;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    /* get the size of the icon */
    gtk_icon_size_lookup (GTK_ICON_SIZE_BUTTON, &w, &h);

    /* for list view we need max rows and columns to view */
    if (class_data->view_model == GTK_LIST_VIEW_MODEL)
    {
        model = create_list_model(jfile_chooser);
    }
    else
    {
        model = create_details_model(jfile_chooser);
    }

    /* rows and columns */
    max_rows = file_list_data->len;
    max_cols = 1;

    gtk_tree_view_set_model(GTK_TREE_VIEW(class_data->tree_view), GTK_TREE_MODEL(model));
    g_object_unref(model);

    /* only process if we have data */
    if (max_cols > 0)
    {
        /* get the row iterators, this is for primarily for the list view */
        row_iter = get_row_iter(model, max_rows);

        for (i=0; i<file_list_data->len; i++)
        {
            filedata = (GtkFileListData*)g_ptr_array_index(file_list_data, i);

            /* the icon to use for the name */
            info.icon = file_list_get_icon(filedata , w, h);

            /* the file/dir name */
            info.name = file_list_get_name(filedata);
            g_message("\tname=%s",info.name);

            /* the size of the file */
            info.size = file_list_get_size(filedata);

            /* the type of file */
            info.type = file_list_get_type(filedata);

            /* the modification date */
            info.modified = file_list_get_mod(filedata);

            /* the attributes of the file */
            info.attributes = file_list_get_attrib(filedata);

            /* add data to the view */
            add_to_view(class_data->view_model, model, &info, &row_iter[row], col);

            /* compute row and col */
            row++;
        }

        g_free(row_iter);
    }
}

/**
 * Determines if the given file should be shown or not.
 *
 * @param class_data holds data
 * @param filename file to check
 *
 * @return TRUE or FALSE
 */
static gboolean jfile_chooser_show_file(GtkJFileChooserClassData *class_data, gchar *filename)
{
    /* is file hidden and hiding disabled ? */
    if (class_data->hiding_enabled == (gboolean)TRUE &&
        is_hidden_file(filename)   == (gboolean)TRUE)
    {
        return FALSE;
    }

    /* is file not a directory and */
    if (is_dir_file(filename) == FALSE)
    {
        /* directory view is enabled */
        if (class_data->view_mode == GTK_DIRECTORIES_ONLY)
        {
            return FALSE;
        }

        if (thisPattern != NULL)
        {
            if (g_pattern_match_string(thisPattern, filename) == FALSE)
            {
                return FALSE;
            }
        }
    }

    return TRUE;
}

/**
 * Generates the row iterators for the given model
 *
 * @param model the model currently being viewed
 * @param max_rows total rows for the model
 *
 * @return row iterators
 */
static GtkTreeIter *get_row_iter(GtkListStore *model, guint max_rows)
{
    guint i;
    GtkTreeIter *row_iter = g_new(GtkTreeIter, max_rows);

    for (i=0; i<max_rows; i++)
    {
        gtk_list_store_append (model, &row_iter[i]);
    }

    return row_iter;
}

/**
 * Adds the info data to the current view
 *
 * @param view_model the current model selected
 * @param model has the data to view
 * @param info file list information
 * @param row the row iterator
 * @param col the column, one for the icon, the other is for the data
 */
static void add_to_view(GtkJFileChooserViewModel view_model, GtkListStore *model, GtkFileListInfo *info, GtkTreeIter *row, guint col)
{
    gtk_list_store_set (model, row, 0, info, -1);
}

/**
 * Clears the file list view.
 *
 * @param jfile_chooser this class instance
 */
static void file_list_clear(GtkJFileChooser *jfile_chooser)
{
    guint i;
    GtkTreeViewColumn *column;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    GList *glist = gtk_tree_view_get_columns(GTK_TREE_VIEW(class_data->tree_view));
    for (i=0; i<g_list_length(glist); i++)
    {
        column = (GtkTreeViewColumn*)g_list_nth_data(glist, i);
        gtk_tree_view_remove_column(GTK_TREE_VIEW(class_data->tree_view), column);
    }
    g_list_free(glist);
}

/**
 * Sorts the filelist array, first by directory, then by name
 *
 * @param a first element
 * @param b second element
 *
 * @return -1 if a is less than b;
 *          0 if a and b are equal;
 *          1 if a is greater than b
 */
static gint file_list_compare(gconstpointer a, gconstpointer b)
{
    GtkFileListData *adata = *((GtkFileListData**)a);
    GtkFileListData *bdata = *((GtkFileListData**)b);

    /* which file is a directory */
    /* we want directories listed first */
    if (adata->file_type == GTK_FILE_TYPE_IS_DIR &&
        bdata->file_type == GTK_FILE_TYPE_IS_REGULAR)
    {
        return -1;
    }

    if (bdata->file_type == GTK_FILE_TYPE_IS_DIR &&
        adata->file_type == GTK_FILE_TYPE_IS_REGULAR)
    {
        return 1;
    }

    /* both files are the same type, sort by name */
    return(g_ascii_strcasecmp(adata->basename, bdata->basename));
}

/**
 * Determines the type of icon for the given file.
 *
 * @param filedata has file_type for file
 * @param width the width of an icon
 * @param height the height of an icon
 *
 * @return scaled pixbuf icon
 */
static GdkPixbuf *file_list_get_icon(GtkFileListData *filedata, gint width, gint height)
{
    static GdkPixbuf *icon;
    static GdkPixbuf *scaled;

    switch (filedata->file_type)
    {
        default:
        case GTK_FILE_TYPE_IS_UNKNOWN:
        case GTK_FILE_TYPE_IS_REGULAR:
            icon = get_pixbuf_icon(ICON_DOCUMENT);
            break;

        case GTK_FILE_TYPE_IS_DIR:
            icon = get_pixbuf_icon(ICON_DIRECTORY);
            break;
    }

    scaled = gdk_pixbuf_scale_simple (icon, width, height, GDK_INTERP_BILINEAR);
    g_object_unref(icon);

    return scaled;
}

/**
 * Gets the folder name to use. For win32, the .lnk is removed
 *
 * @param filedata has the basename to check
 *
 * @return new name
 */
static gchar *file_list_get_name(GtkFileListData *filedata)
{
#ifdef G_OS_WIN32
    gchar tmp[MAXPATHLEN];
    gchar *p;

    g_stpcpy(tmp, filedata->basename);

    p = g_strrstr(tmp, ".");
    if (p != NULL)
    {
        if (g_ascii_strcasecmp((p)+1, LNK) == 0)
        {
            tmp[strlen(tmp)-strlen(p)] = 0;
            return g_strdup(tmp);
        }
    }
#endif
    return g_strdup(filedata->basename);
}

/**
 * Gets the size of the file.
 *
 * @param filedata has the file size
 *
 * @return size of file
 */
static gchar *file_list_get_size(GtkFileListData *filedata)
{
    gchar str[32];
    gint size;

    if (filedata->file_type == GTK_FILE_TYPE_IS_DIR)
    {
        str[0] = 0;
    }
    else
    {
#define KB_SIZE 1024

        size = filedata->size/KB_SIZE;
        if (size == 0)
        {
            g_stpcpy(str, "1 KB");
        }
        else if (size < KB_SIZE)
        {
            g_sprintf(str, "%d KB", size);
        }
        else
        {
            g_sprintf(str, "%d MB", size/KB_SIZE);
        }
    }

    return g_strdup(str);
}

/**
 * Based on the file extension, returns a name representation.
 *
 * @param filedata has the file_type
 *
 * @return new file type string
 */
static gchar *file_list_get_type(GtkFileListData *filedata)
{
    gchar *p = NULL;

    if (filedata->file_type == GTK_FILE_TYPE_IS_DIR)
    {
        p = g_strdup("File Folder");
    }
    else
    {
        /* check file extension */
        p = g_strrstr(filedata->basename, ".");
        if (p != NULL)
        {
            /* move past '.' */
            p++;

            /* find type of file */
            if (g_ascii_strcasecmp(p, LNK) == 0)
            {
                p = g_strdup("Shortcut");
            }
            else if (g_ascii_strcasecmp(p, TXT) == 0)
            {
                p = g_strdup("Text Document");
            }
            else if (g_ascii_strcasecmp(p, DOC) == 0)
            {
                p = g_strdup("Microsoft Word Document");
            }
            else if (g_ascii_strcasecmp(p, EXE) == 0)
            {
                p = g_strdup("Application");
            }
            else if (g_ascii_strcasecmp(p, ZIP) == 0 ||
                     g_ascii_strcasecmp(p, GZIP) == 0)
            {
                p = g_strdup("Zip File");
            }
            else if (g_ascii_strcasecmp(p, PDF) == 0)
            {
                p = g_strdup("Adobe Acrobat Document");
            }
            else if (g_ascii_strcasecmp(p, RTF) == 0)
            {
                p = g_strdup("Rich Text Document");
            }
            else if (g_ascii_strcasecmp(p, WAB) == 0)
            {
                p = g_strdup("Address Book File");
            }
            else if (g_ascii_strcasecmp(p, HTM) == 0 ||
                     g_ascii_strcasecmp(p, HTML) == 0)
            {
                p = g_strdup("HTML Document");
            }
            else if (g_ascii_strcasecmp(p, CL4) == 0)
            {
                p = g_strdup("Easy CD Creator Document");
            }
            else if (g_ascii_strcasecmp(p, JWL) == 0)
            {
                p = g_strdup("Jewel Case Creator Document");
            }
            else if (g_ascii_strcasecmp(p, B4S) == 0)
            {
                p = g_strdup("Winamp Media File");
            }
            else if (g_ascii_strcasecmp(p, XPM) == 0)
            {
                p = g_strdup("XPM File");
            }
            else if (g_ascii_strcasecmp(p, JAR) == 0)
            {
                p = g_strdup("Executable Jar File");
            }
            else if (g_ascii_strcasecmp(p, JPG) == 0)
            {
                p = g_strdup("JPG Image");
            }
            else if (g_ascii_strcasecmp(p, GIF) == 0)
            {
                p = g_strdup("GIF Image");
            }
            else if (g_ascii_strcasecmp(p, BMP) == 0)
            {
                p = g_strdup("Bitmap Image");
            }
            else if (g_ascii_strcasecmp(p, MIX) == 0)
            {
                p = g_strdup("Microsoft Picture It! Document");
            }
            else if (g_ascii_strcasecmp(p, MPA) == 0 ||
                     g_ascii_strcasecmp(p, MPEG) == 0)
            {
                p = g_strdup("Movie Clip");
            }
            else if (g_ascii_strcasecmp(p, INI) == 0)
            {
                p = g_strdup("Configuration Settings");
            }
            else if (g_ascii_strcasecmp(p, PS) == 0)
            {
                p = g_strdup("PostScript");
            }
            else if (g_ascii_strcasecmp(p, VSD) == 0)
            {
                p = g_strdup("Microsoft Visio Viewer");
            }
            else if (g_ascii_strcasecmp(p, PPT) == 0)
            {
                p = g_strdup("Microsoft PowerPoint Presentation");
            }
            else if (g_ascii_strcasecmp(p, XLS) == 0)
            {
                p = g_strdup("Microsoft Excel Worksheet");
            }
            else if (g_ascii_strcasecmp(p, WAV) == 0)
            {
                p = g_strdup("Windows Media Player");
            }
            else if (g_ascii_strcasecmp(p, JS) == 0)
            {
                p = g_strdup("JScript Script File");
            }
            else if (g_ascii_strcasecmp(p, FON) == 0)
            {
                p = g_strdup("Font file");
            }
            else if (g_ascii_strcasecmp(p, TIF) == 0)
            {
                p = g_strdup("TrueType Font file");
            }
            else if (g_ascii_strcasecmp(p, CMD) == 0)
            {
                p = g_strdup("Window NT Command Script");
            }
            else if (g_ascii_strcasecmp(p, BAT) == 0)
            {
                p = g_strdup("MS-DOS Batch File");
            }
            else if (g_ascii_strcasecmp(p, REG) == 0)
            {
                p = g_strdup("Registration Entries");
            }
            else if (g_ascii_strcasecmp(p, SYS) == 0)
            {
                p = g_strdup("System file");
            }
            else if (g_ascii_strcasecmp(p, V386) == 0)
            {
                p = g_strdup("Virtual device driver");
            }
            else if (g_ascii_strcasecmp(p, OCX) == 0)
            {
                p = g_strdup("ActiveX Control");
            }
            else if (g_ascii_strcasecmp(p, WMZ) == 0)
            {
                p = g_strdup("Windows Media Player Skin Package");
            }
            else if (g_ascii_strcasecmp(p, HLP) == 0)
            {
                p = g_strdup("Help File");
            }
            else
            {
                p = g_strconcat(g_ascii_strup(p, -1), " File", NULL);
            }
        }
        else
        {
            p = g_strdup("File");
        }
    }

    return p;
}

/**
 * Gets the modification time of the file. Format = mm/dd/yy hh:mm
 *
 * @param filedata has the modification time
 *
 * @return new modification time formatted
 */
static gchar *file_list_get_mod(GtkFileListData *filedata)
{
    struct tm *m_time;
    gchar str[32] = {0};

    m_time = localtime((const time_t*)&filedata->modified);
    if (m_time)
    {
        strftime(str, 32, "%m/%d/%Y %I:%M %p", m_time);
    }

    return g_strdup(str);
}

/**
 * Gets the attributes of the file.
 *
 * @param filedata has the file attributes
 *
 * @return new attribute string
 */
static gchar *file_list_get_attrib(GtkFileListData *filedata)
{
    gchar attrib[4] = {0};

    if (filedata->attributes&S_IRUSR)
        attrib[0] = 'r';
    else
        attrib[0] = '-';

    if (filedata->attributes&S_IWUSR)
        attrib[1] = 'w';
    else
        attrib[1] = '-';

    if (filedata->attributes&S_IXUSR)
        attrib[2] = 'x';
    else
        attrib[2] = '-';

    return g_strdup(attrib);
}

/**
 * Adds items to the option menu.
 *
 * @param jfile_chooser jfile chooser class
 */
static void jfile_chooser_add_menu_items(GtkJFileChooser *jfile_chooser)
{
    GtkWidget *menu;
    GDir *dir;
    GError *error = NULL;
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    dir = g_dir_open(class_data->base_dir, 0, &error);
    if (dir != NULL)
    {
        menu = gtk_menu_new();
        gtk_option_menu_set_menu(GTK_OPTION_MENU(class_data->lookin_option_menu), menu);
        gtk_widget_show(menu);

#if defined G_OS_WIN32
        win32_add_menu_items(menu);
#else
        unix_add_menu_items(menu);
#endif

        g_dir_close(dir);
    }
    else
    {
        if (error != NULL)
        {
            g_warning(error->message);
            g_error_free(error);
        }
    }
}

/**
 * Adds a file component to the 'Look In' pulldown menu.
 *
 * @param doAdd force the add even if the item is not found on the file system.
 *              This handles the case of win32 'My Computer', etc...
 * @param key hash key used for lookup
 * @param displayname how the item appears in the lookin pulldown
 * @param pathname actual location of name
 * @param which_icon icon associated with name
 * @param indent_level how far to indent icon and name
 * @param keep indicates if the menu item can be deleted or not
 *
 * @return menu_item
 */
static GtkWidget *get_lookin_menu_item(gboolean doAdd,
                                       gint key,
                                       gchar *displayname,
                                       const gchar *pathname,
                                       gint which_icon,
                                       gint indent_level,
                                       gboolean keep)
{
    GtkWidget *hbox;
    GtkWidget *image;
    GtkWidget *label;
    GtkWidget *menu_item;
    GtkLookinOptionMenuData *menu_data;
    gchar *basename;
    gchar *dirname;
    gint i;

    /* add only directories or if its a forced add */
    if (doAdd || g_file_test(pathname, G_FILE_TEST_IS_DIR))
    {
        /* a place to put the name */
        menu_item = gtk_menu_item_new();

        /* container for icon and name */
        hbox = gtk_hbox_new(FALSE, 0);

        /* get icon for folder */
        image = get_icon(which_icon);

        /* make indentation */
        for (i=0; i<indent_level; i++)
        {
            label = gtk_label_new("   ");
            gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
        }
        gtk_box_pack_start(GTK_BOX(hbox), image, FALSE, FALSE, 0);

        /* now for the folder name */
        label = gtk_label_new(displayname);
        gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
        gtk_container_add(GTK_CONTAINER(menu_item), hbox);

        /* get just the directory */
        dirname = g_path_get_dirname(pathname);

        /* get just the name */
        basename = g_path_get_basename(pathname);
        if (basename[0] == G_DIR_SEPARATOR)
        {
            g_free(basename);
            basename = (gchar*)g_malloc((gulong)(strlen(pathname)+1));
            g_stpcpy(basename, pathname);
        }

        /* save some information about this item */
        menu_data = g_new(GtkLookinOptionMenuData, 1);
        menu_data->key = key;
        g_stpcpy(menu_data->displayname, displayname);
        g_stpcpy(menu_data->pathname, pathname);
        g_stpcpy(menu_data->basename, basename);
        menu_data->indent_level = indent_level;
        menu_data->keep = keep;

        /* store data with widget */
        gtk_widget_set_name(menu_item, basename);
        g_object_set_data(G_OBJECT(menu_item), basename, menu_data);

        g_free(basename);
        g_free(dirname);

        gtk_widget_show_all(menu_item);
        return menu_item;
    }

    return NULL;
}

/**
 * Adds the menu item to a menu
 *
 * @param menu add to
 * @param menu_item add from
 */
static void add_lookin_menu_item(GtkWidget *menu, GtkWidget *menu_item)
{
    if (menu_item != NULL)
    {
        gtk_menu_shell_append(GTK_MENU_SHELL(menu), menu_item);
    }
}

/**
 * Removes all children of this menu data
 *
 * @param option_menu has the menu
 * @param menu_data the current menu_data, delete its siblings
 */
static void jfile_chooser_remove_menu_item(GtkWidget *option_menu, GtkLookinOptionMenuData *menu_data)
{
    GList *glist;
    GtkLookinOptionMenuData *child_menu_data;
    const gchar *key;

    /* initialize to the current nodes keep value, True represents primary node */
    gboolean start_delete = menu_data->keep;

    GtkWidget *menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(option_menu));

    /* get children of menu */
    glist = gtk_container_children(GTK_CONTAINER(menu));
    while (glist != NULL)
    {
        if (GTK_IS_MENU_ITEM(glist->data))
        {
            key = gtk_widget_get_name(GTK_WIDGET(glist->data));
            if (key)
            {
                child_menu_data = (GtkLookinOptionMenuData*)g_object_get_data(G_OBJECT(glist->data), key);
                if (child_menu_data != NULL)
                {
                    /* ready to delete? remove siblings only */
                    if (start_delete && child_menu_data->keep == FALSE)
                    {
                        g_free(child_menu_data);
                        gtk_widget_destroy(GTK_WIDGET(glist->data));
                    }
                    else
                    {
                        /* are the data nodes the same ? */
                        if (child_menu_data == menu_data)
                        {
                            /* we're on the selected node, delete all siblings that
                               are not marked to keep */
                            start_delete = TRUE;
                        }
                    }
                }
            }
        }
        glist = glist->next;
    }
    g_list_free (glist);
}

/**
 * Returns the menu_item associated with the displayname
 *
 * @param menu the menu that has the items
 * @param displayname the name of the menu_item
 *
 * @return menu item or NULL if not found
 */
static GtkWidget *jfile_chooser_get_menu_item(GtkWidget *menu, gchar *displayname)
{
    GList *glist;
    GtkWidget *menu_item = NULL;
    const gchar *key;

    glist = gtk_container_children(GTK_CONTAINER(menu));
    while (glist != NULL)
    {
        if (GTK_IS_MENU_ITEM(glist->data))
        {
            key = gtk_widget_get_name(GTK_WIDGET(glist->data));
            if (key)
            {
                if (g_ascii_strcasecmp(displayname, key) == 0)
                {
                    menu_item = GTK_WIDGET(glist->data);
                    break;
                }
            }
        }
        glist = glist->next;
    }
    g_list_free (glist);

    return menu_item;
}

/**
 * Sets the current directory
 *
 * @param jfile_chooser this instance
 * @param class_data this dialog's class data
 */
static void jfile_chooser_set_dir(GtkJFileChooser *jfile_chooser, GtkJFileChooserClassData *class_data)
{
    gchar *basename;
    GSList *path_components = NULL;

    /* get the basename from the base directory */
    basename = g_path_get_basename(class_data->base_dir);

    /* add last element first */
    path_components = g_slist_prepend(path_components, basename);

    /* get directory components */
    getDirectoryComponents(class_data->base_dir, &path_components);

    /* block signal handlers during the next phase of processing */
    g_signal_handler_block(class_data->lookin_option_menu, class_data->option_menu_handler_id);

    /* Search the option menu for items */
    getOptionMenuItems(jfile_chooser, class_data->lookin_option_menu, path_components);

    /* turn on handlers */
    g_signal_handler_unblock(class_data->lookin_option_menu, class_data->option_menu_handler_id);

    /* free the list */
    g_slist_free(path_components);
}

/**
 * Searchs the current directory and gets all directories and
 * puts them in the single list
 *
 *
 * @param base_dir
 * @param path_components
 */
static void getDirectoryComponents(gchar *base_dir, GSList **path_components)
{
    gchar name[MAXPATHLEN];
    gchar *dir_basename;
    gchar *dirname;

    g_stpcpy(name, base_dir);
    while ((dirname = g_path_get_dirname(name))[0] != '.')
    {
        g_stpcpy(name, dirname);

        dir_basename = g_path_get_basename(dirname);

        /* are we at the beginning of the path? */
        if (dir_basename[0] == G_DIR_SEPARATOR)
        {
            /* only add one separator */
            if (dirname[strlen(dirname)-1] == G_DIR_SEPARATOR)
            {
                *path_components = g_slist_prepend(*path_components, dirname);
            }
            else
            {
                *path_components = g_slist_prepend(*path_components, g_strconcat(dirname, dir_basename, NULL));
            }
            break;
        }
        else
        {
            *path_components = g_slist_prepend(*path_components, dir_basename);
        }
    }
}

/**
 * Takes the menu items and build the view into the file chooser
 *
 * @param jfile_chooser
 * @param lookin_option_menu
 * @param path_components
 */
static void getOptionMenuItems(GtkJFileChooser *jfile_chooser, GtkWidget *lookin_option_menu, GSList *path_components)
{
    /* get the menu */
    GtkWidget *menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(lookin_option_menu));
    if (menu != NULL)
    {
        gchar *dirname;
        GList *glist = gtk_container_get_children(GTK_CONTAINER(menu));
        GList *tmp = glist;
        gboolean found = FALSE;

        /* traverse the list*/
        dirname = (gchar *)g_slist_nth_data(path_components, 0);
        while (tmp)
        {
            /* only process menu items*/
            const gchar *key;
            if (GTK_IS_MENU_ITEM(tmp->data))
            {
                key = gtk_widget_get_name(GTK_WIDGET(tmp->data));
                if (g_ascii_strcasecmp(key, dirname) == 0)
                {
                    GtkWidget *menu_item = jfile_chooser_get_menu_item(menu, dirname);
                    if (menu_item != NULL)
                    {
                        found = TRUE;
                        gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
                        gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
                    }
                    break;
                }
            }
            tmp = g_list_next(tmp);
        }

        g_list_free(glist);

        if (found == (gboolean)TRUE)
        {
            addPathComponents(jfile_chooser, lookin_option_menu, menu, path_components);
        }
    }
}

/**
 * Adds the directories to the view
 *
 * @param jfile_chooser
 * @param lookin_option_menu
 * @param menu
 * @param path_components
 */
static void addPathComponents(GtkJFileChooser *jfile_chooser, GtkWidget *lookin_option_menu, GtkWidget *menu, GSList *path_components)
{
    guint i;
    GtkWidget *menu_item;
    gchar *dirname = NULL;

    for (i=1; i<g_slist_length(path_components); i++)
    {
        dirname = (gchar *)g_slist_nth_data(path_components, i);
        jfile_chooser_add_filename_to_menu(jfile_chooser, dirname);
    }

    /* activate the last item so the filelist is populated */
    menu_item = jfile_chooser_get_menu_item(menu, dirname);
    if (menu_item != NULL)
    {
        GtkLookinOptionMenuData *menu_data;

        /* select the item */
        gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), menu_item);
        gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), menu_item, FALSE);
        thisMenuPosition = gtk_option_menu_get_history(GTK_OPTION_MENU(lookin_option_menu));

        /* get the currently selected item */
        menu_data = jfile_chooser_get_data(jfile_chooser);
        if (menu_data)
        {
            /* go populate the file list view */
            populate_filelist(jfile_chooser, menu_data);
        }
    }
}

/**
 * Processes the button click for home
 *
 * @param button the button itself
 * @param data has file_chooser instance
 */
static void home_button_clicked_cb(GtkWidget *button, void * data)
{
    GtkJFileChooser *jfile_chooser;
    GtkJFileChooserClassData *class_data;
    GtkWidget *menu;
    GtkWidget *selected_menu_item;
    const gchar *key;
    GtkLookinOptionMenuData *menu_data;

    g_return_if_fail (GTK_IS_JFILE_CHOOSER (data));

    jfile_chooser = GTK_JFILE_CHOOSER(data);
    class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);

    menu = gtk_option_menu_get_menu(GTK_OPTION_MENU(class_data->lookin_option_menu));
    if (menu)
    {
        gtk_menu_set_active(GTK_MENU(menu), 0);
        selected_menu_item = gtk_menu_get_active(GTK_MENU(menu));
        {
            key = gtk_widget_get_name(selected_menu_item);
            if (key)
            {
                menu_data = (GtkLookinOptionMenuData*)g_object_get_data(G_OBJECT(selected_menu_item), key);
                if (menu_data)
                {
                    /* destroy the option menu from root and rebuild */
                    jfile_chooser_remove_menu_item(class_data->lookin_option_menu, menu_data);

                    gtk_menu_shell_select_item(GTK_MENU_SHELL(menu), selected_menu_item);
                    gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu), selected_menu_item, FALSE);
                    thisMenuPosition = gtk_option_menu_get_history(GTK_OPTION_MENU(class_data->lookin_option_menu));

                    /* now load home dir */
                    jfile_chooser_set_dir(jfile_chooser, class_data);
                }
            }
        }
    }
}

/**
 * Determines if the file is hidden or not
 *
 * @param filename check this file to see if its hidden or not
 *
 * @return TRUE or FALSE
 */
static gboolean is_hidden_file(const gchar *filename)
{
    if (filename != NULL)
    {
#ifdef G_OS_WIN32
        DWORD dwFileAttributes = GetFileAttributes(filename);
        if ((dwFileAttributes != 0xFFFFFFFF) && (dwFileAttributes & FILE_ATTRIBUTE_HIDDEN))
        {
            return TRUE;
        }
#endif
#ifdef G_OS_UNIX
        if (filename[0] == '.')
        {
            return TRUE;
        }
#endif
    }
    return FALSE;
}

/**
 * Determines if the filename is a directory or not
 *
 * @param filename
 *
 * @return TRUE or FALSE
 */
static gboolean is_dir_file(const gchar *filename)
{
    return g_file_test(filename, G_FILE_TEST_IS_DIR);
}

/**
 * Returns the type of file, directory or regular
 *
 * @param filename
 *
 * @return GTK_FILE_TYPE_IS_DIR, GTK_FILE_TYPE_IS_REGULAR
 * or if unknown GTK_FILE_TYPE_IS_UNKNOWN
 */
static FileType get_file_type(const gchar *filename)
{
    /* check for directory */
    if (g_file_test(filename, G_FILE_TEST_IS_DIR))
    {
        return GTK_FILE_TYPE_IS_DIR;
    }

    if (g_file_test(filename, G_FILE_TEST_IS_REGULAR))
    {
        return GTK_FILE_TYPE_IS_REGULAR;
    }

    return GTK_FILE_TYPE_IS_UNKNOWN;
}

/********* Unix specific code *****************************/
#ifdef G_OS_UNIX

/**
 * Adds the basic menu_items to the menu.
 *
 * @param menu where to put the menu_items
 */
static void unix_add_menu_items(GtkWidget *menu)
{
    gchar *root_dir = G_DIR_SEPARATOR_S;
    add_lookin_menu_item(menu, get_lookin_menu_item(TRUE,
                                                    GPOINTER_TO_INT(root_dir),
                                                    root_dir,
                                                    root_dir,
                                                    ICON_DISK,
                                                    0,
                                                    TRUE));
}

#endif

/******** Windows specific code **************************/
#ifdef G_OS_WIN32

/**
 * Adds the basic menu_items to the menu. If the current directory is in the
 * list, the directory items are added to the viewable area.
 *
 * @param menu where to put the menu_items
 */
static void win32_add_menu_items(GtkWidget *menu)
{
    /* add drives */
    win32_add_drives(menu);
}

/**
 * Gets the windows drive information and adds them to the pulldown
 *
 * @param menu add to this menu
 */
static void win32_add_drives(GtkWidget *menu)
{
    gchar *str_drive;
    gchar szTemp[BUFSIZE];
    gchar drive_info[BUFSIZE];
    guint drive_type;
    gchar volume_name_buffer[BUFSIZE];
    gchar file_sys_name_buffer[BUFSIZE];
    DWORD dwSysFlags;     // flags that describe the file system

    /* Get the drives string */
    GetLogicalDriveStrings (BUFSIZE-1, szTemp);

    /* Add the drives as necessary */
    str_drive = szTemp;

    while (*str_drive != '\0')
    {
        /* Ignore floppies (?) */
        drive_type = GetDriveType(str_drive);
        {
            volume_name_buffer[0] = '\0';
            file_sys_name_buffer[0] = '\0';

            /* Build the actual displayable string */
            GetVolumeInformation(str_drive,
                                 volume_name_buffer, BUFSIZE,
                                 NULL, NULL,
                                 &dwSysFlags,
                                 file_sys_name_buffer, BUFSIZE);

            /* some volumes are blank */
            if (strlen(volume_name_buffer) > 0)
            {
                g_sprintf(drive_info, "%s on %s",volume_name_buffer, str_drive);
            }
            else
            {
                g_sprintf(drive_info, "%s", str_drive);
            }

            add_lookin_menu_item(menu, get_lookin_menu_item(TRUE,
                                                            GPOINTER_TO_INT(drive_info),
                                                            drive_info,
                                                            str_drive,
                                                            ICON_DISK,
                                                            0,
                                                            TRUE));
        }
        str_drive += (strlen (str_drive) + 1);
    }
}

/**
 * Populates the filelist.
 *
 * @param jfile_chooser jfile chooser class
 * @param menu_data menu data
 * @param file_list_data file list data
 */
static void win32_populate_filelist(GtkJFileChooser *jfile_chooser, GtkLookinOptionMenuData *menu_data, GPtrArray *file_list_data)
{
    GtkJFileChooserClassData *class_data = GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser);
    GDir *dir;
    GError *error = NULL;
    struct stat sbuf;
    const gchar *fname;
    gboolean show_file = TRUE;
    GtkFileListData *filedata;

    /* handle desktop differently; need to also check
       the 'All Users' area for additional files,
       also need to add My Documents, My Computer, and My Network Places
    */
    if (g_ascii_strcasecmp(menu_data->basename, DESKTOP) == 0)
    {
        /* remove the user's id from the directory */
        gchar *all_desktop = win32_get_all_desktop(menu_data->pathname);

        dir = g_dir_open(all_desktop, 0, &error);
        if (dir != NULL)
        {
            while ((fname = g_dir_read_name(dir)) != NULL)
            {
                /* add this file or not ? */
                gchar *tmpname = g_build_filename(all_desktop, fname, NULL);

                show_file = jfile_chooser_show_file(class_data, tmpname);
                if (show_file == (gboolean)TRUE)
                {
                    filedata = (GtkFileListData*)g_malloc(sizeof(GtkFileListData));

                    if (stat(tmpname, &sbuf) == 0)
                    {
                        filedata->size = sbuf.st_size;
                        filedata->modified = (gint)sbuf.st_mtime;
                        filedata->attributes = sbuf.st_mode;
                    }

                    g_stpcpy(filedata->basename, fname);
                    filedata->file_type = get_file_type(tmpname);
                    g_ptr_array_add(file_list_data, filedata);
                }
                g_free(tmpname);
            }

            g_dir_close(dir);
        }
        else
        {
            if (error != NULL)
            {
                g_warning(error->message);
                g_error_free(error);
            }
        }
        g_free(all_desktop);
    }
}

/**
 * Builds a directory path for 'All Users'; removes the user id.
 *
 * @param pathname pathname to parse
 *
 * @return pathname with 'All Users' added
 */
static gchar *win32_get_all_desktop(const gchar *pathname)
{
    /* remove 'desktop' */
    gchar *dirname = g_path_get_dirname(pathname);

    /* remove user id */
    dirname = g_path_get_dirname(dirname);

    /* build all desktop */
    return g_build_filename(dirname, "All Users", DESKTOP, NULL);
}

#endif
