/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "HelpHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "Dialog.h"
#include "MessageHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/HelpHandler.c 197 2013-08-05 19:53:42Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/*************************************************************************/

/**
 * Launches internet browser; loads the iam user guide.
 */
void HH_HelpIam(void)
{
    GError *err = NULL;
    GPid child_pid;

    gchar browser[MAXPATHLEN];
    gchar user_guide[128];
    gchar *p_user_guide;
    gchar message[256];

    gint i;
    gchar *argv[16];

    /* get browser location */
    if (CP_GetConfigData(IAM_BROWSER_EXE, browser) == True)
    {
        i = 0;
        argv[i++] = browser;
        if (CP_GetConfigData(IAM_USER_GUIDE, user_guide) == True)
        {
            p_user_guide = g_strconcat("file:///", user_guide, NULL);
            argv[i++] = p_user_guide;
            argv[i++] = NULL;

            if (g_spawn_async(NULL, argv, NULL,
                              (GSpawnFlags)(G_SPAWN_SEARCH_PATH),
                              NULL, NULL, &child_pid, &err)
                == FALSE)
            {
                g_sprintf(message,
                        "Unable to start browser executable!\n\n"
                        "Verify execution path : %s",
                        argv[0]);

                ErrorMessage(NULL, "Help Error", message);
                g_warning(err->message);
                g_error_free (err);
            }

            g_free(p_user_guide);
        }
    }
}

/**
 * Shows some info about the iam app, including support information.
 *
 * @param parent owner of the help dialog
 */
void HH_AboutIam(GtkWidget *parent)
{
    gchar message[1024];
    gchar support_phone[32] = {0};
    gchar support_email[256] = {0};

    if (CP_GetConfigData(IAM_SUPPORT_PHONE, support_phone) == False)
    {
        g_warning("IAM_SUPPORT_PHONE not defined in the config file!");
        g_stpcpy(support_phone, "undefined");
    }
    if (CP_GetConfigData(IAM_SUPPORT_EMAIL, support_email) == False)
    {
        g_warning("IAM_SUPPORT_EMAIL not defined in the config file!");
        g_stpcpy(support_email, "undefined");
    }

    g_sprintf(message,
            "IAM Version: %s\n"
            "\n"
            "Technical Support Phone: %s\n"
            "Technical Support Email: %s\n",
            PROGRAM_VERSION,
            support_phone,
            support_email);

    InfoMessage(parent, "About IAM", message);
}

/**
 * Show some information about GTK+.
 *
 * @param parent owner of the help dialog
 */
void HH_AboutGTK(GtkWidget *parent)
{
    gchar message[2048];

    g_sprintf(message,
            "IAM was built using the GIMP toolkit, GTK+.\n"
            "GTK+ is a multi-platform toolkit for creating graphical user interfaces. "
            "Offering a complete set of widgets, GTK+ is suitable for projects ranging "
            "from small one-off projects to complete application suites.\n"
            "\n"
            "GTK+ is based on three libraries developed by the GTK+ team:\n"
            BULLET_SYMBOL"GLib is the low-level core library that forms the basis of GTK+ and GNOME. "
            "It provides data structure handling for C, portability wrappers, and "
            "interfaces for such runtime functionality as an event loop, threads, "
            "dynamic loading, and an object system.\n"
            BULLET_SYMBOL"Pango is a library for layout and rendering of text, with an emphasis on "
            "internationalization. It forms the core of text and font handling for GTK+-2.0.\n"
            BULLET_SYMBOL"The ATK library provides a set of interfaces for accessibility. By supporting "
            "the ATK interfaces, an application or toolkit can be used with such tools as "
            "screen readers, magnifiers, and alternative input devices.\n"
            "\n"
            "GTK Version: %d.%d.%d\n"
            "GLIB Version: %d.%d.%d\n",
            GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION,
            GLIB_MAJOR_VERSION, GLIB_MINOR_VERSION, GLIB_MICRO_VERSION);
    g_message("HH_AboutGTK: len=%d",(gint)strlen(message));

    InfoMessage(parent, "About GTK+", message);
}
