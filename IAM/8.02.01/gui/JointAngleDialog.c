/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "JointAngleDialog.h"
#undef PRIVATE

#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/JointAngleDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define JOINT_ANGLE_DIALOG_WIDTH    325
#define JOINT_ANGLE_DIALOG_HEIGHT   215

#define ANGLE_VALUE_ALIGNMENT       1.0

/* local class variables */
static GtkWidget    *thisDialog     = NULL;
static GtkWidget    *thisPortTable  = NULL;
static GtkWidget    *thisStbdTable  = NULL;
static GtkWidget    *thisAngleLabel[MAX_ANGLES];
static JointAngle   thisJointAngleData;
/*************************************************************************/

/**
 * This method creates all the gui components for the joint angle dialog
 *
 * @param parent attach dialog to this window.
 */
void JAD_Create(GtkWindow *parent)
{
    if (thisDialog == NULL)
    {
        GtkWidget *hbox;
        GtkWidget *main_frame;
        GtkWidget *frame;

        thisDialog = gtk_dialog_new_with_buttons("Predicted Joint Angles",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK(gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(response_cb), NULL);

        /* create main frame */
        main_frame = gtk_frame_new("Joint Angles");

        /* create box for port and starboard frames */
        hbox = gtk_hbox_new(True, 5);

        /* add the horizontal box to the main frame */
        gtk_container_add(GTK_CONTAINER(main_frame), hbox);

        frame = gtk_frame_new("Port");
        gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 5);

        thisPortTable = gtk_table_new(MAX_PORT_ANGLES, NUM_JOINT_ANGLE_COLUMNS, True);
        gtk_container_add(GTK_CONTAINER(frame), thisPortTable);

        /* set the spacing to 10 on x and 10 on y */
        gtk_table_set_row_spacings (GTK_TABLE (thisPortTable), 5);
        gtk_table_set_col_spacings (GTK_TABLE (thisPortTable), 5);

        frame = gtk_frame_new("Starboard");
        gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 5);

        thisStbdTable = gtk_table_new(MAX_STBD_ANGLES, NUM_JOINT_ANGLE_COLUMNS, True);
        gtk_container_add(GTK_CONTAINER(frame), thisStbdTable);

        /* set the spacing to 10 on x and 10 on y */
        gtk_table_set_row_spacings (GTK_TABLE (thisStbdTable), 5);
        gtk_table_set_col_spacings (GTK_TABLE (thisStbdTable), 5);

        /* now add the main frame to the dialog */
        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), main_frame);
    }
}

/**
 * This method opens the dialog for viewing
 */
void JAD_Open(void)
{
    if (thisDialog != NULL)
    {
        GList *currentPt = PDH_GetCurrentPt();
        if (currentPt == NULL)
        {
            /* this will load the joint angle structure with zeros and
             * load the labels
             */
            DD_GetAngles(PD_GetCoordinateType(), NULL, &thisJointAngleData);
        }

        else
        {
            PredictPt *cpt = (PredictPt *)currentPt->data;

            /* this will load the joint angle structure with data maybe and
             * load the labels
             */
            DD_GetAngles(PD_GetCoordinateType(), cpt->timeStr, &thisJointAngleData);
        }

        load_angle_data(&thisJointAngleData);

        gtk_widget_show_all(thisDialog);
    }
}

/**
 * If the dialog is opened, the data points are updated.
 */
void JAD_Update(void)
{
    if (thisDialog != NULL)
    {
        GList *currentPt = PDH_GetCurrentPt();
        if (currentPt != NULL)
        {
            PredictPt *cpt = (PredictPt *)currentPt->data;
            DD_GetAngles(PD_GetCoordinateType(), cpt->timeStr, &thisJointAngleData);
            update_angle_data(&thisJointAngleData);
        }
    }
}

/**
 * This method process the button events; only button is close. This method
 * destroys the dialog.
 *
 * @param widget widget that made call
 * @param response_id type of response
 * @param data user data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    if (thisDialog != NULL)
    {
        gtk_widget_destroy(thisDialog);
    }
    thisDialog = NULL;
}

/**
 * This method loads the data into the list
 *
 * @param ja array of joint angle data
 */
static void load_angle_data(JointAngle *ja)
{
#define PADDING 2

    GtkWidget *label;
    gchar value[32];
    gint row;
    gint i;

    for (row=0, i=FIRST_PORT_ANGLE; i<LAST_PORT_ANGLE; row++, i++)
    {
        label = gtk_label_new(ja->angleLabel[i]);
        gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);

        gtk_table_attach(GTK_TABLE(thisPortTable), label,
                         0, 1, row, row+1,
                         GTK_FILL, GTK_FILL, PADDING, PADDING);

        g_sprintf(value, "%f", ja->angleValue[i]);

        thisAngleLabel[i] = gtk_label_new(value);
        gtk_misc_set_alignment (GTK_MISC (thisAngleLabel[i]), 0.0, 0.5);

        gtk_table_attach(GTK_TABLE(thisPortTable), thisAngleLabel[i],
                         1, 2, row, row+1,
                         GTK_FILL, GTK_FILL, PADDING, PADDING);
    }

    for (row=0, i=FIRST_STBD_ANGLE; i<LAST_STBD_ANGLE; row++, i++)
    {
        label = gtk_label_new(ja->angleLabel[i]);
        gtk_misc_set_alignment (GTK_MISC (label), 1.0, 0.5);

        gtk_table_attach(GTK_TABLE(thisStbdTable), label,
                         0, 1, row, row+1,
                         GTK_FILL, GTK_FILL, PADDING, PADDING);

        g_sprintf(value, "%f", ja->angleValue[i]);

        thisAngleLabel[i] = gtk_label_new(value);
        gtk_misc_set_alignment (GTK_MISC (thisAngleLabel[i]), 0.0, 0.5);

        gtk_table_attach(GTK_TABLE(thisStbdTable), thisAngleLabel[i],
                         1, 2, row, row+1,
                         GTK_FILL, GTK_FILL, PADDING, PADDING);
    }
}

/**
 * Updates the joint angle data in the dialog. Dialog should
 * already be opened.
 *
 * @param ja
 */
static void update_angle_data(JointAngle *ja)
{
#define PADDING 2

    gchar value[32];
    gint row;
    gint i;

    for (row=0, i=FIRST_PORT_ANGLE; i<LAST_PORT_ANGLE; row++, i++)
    {
        g_sprintf(value, "%f", ja->angleValue[i]);
        gtk_label_set_text(GTK_LABEL(thisAngleLabel[i]), value);
    }

    for (row=0, i=FIRST_STBD_ANGLE; i<LAST_STBD_ANGLE; row++, i++)
    {
        g_sprintf(value, "%f", ja->angleValue[i]);
        gtk_label_set_text(GTK_LABEL(thisAngleLabel[i]), value);
    }
}
