/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <expat.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "SolarDraw.h"
#undef PRIVATE

#include "Dialog.h"
#include "ColorHandler.h"
#include "ConfigParser.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "WindowGrid.h"
#include "XML_Parse.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/SolarDraw.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define  SUN_COLOR   GC_YELLOW

static GList *thisSolarPointList = NULL;

/*************************************************************************/

/**
 * This function loads the solar data into memory.
 */
void SD_Constructor(void)
{
    /* place holder */
}

/**
 * This function frees the current solar data.
 */
void SD_Destructor(void)
{
    SD_FreeSolarPointList(thisSolarPointList);
}

/**
 * Loads the solar list with data
 *
 * @param solarList
 */
void SD_SetData(GList *solarList)
{
    thisSolarPointList = solarList;
}

/**
 * Returns the link list of solar points
 *
 *
 * @return GList*
 */
GList *SD_GetData(void)
{
    return thisSolarPointList;
}

/**
 * Frees the solar point list, frees the internal data and the
 * list itself.
 *
 * @param spList
 */
void SD_FreeSolarPointList(GList *spList)
{
    GList *_spList;
    for (_spList = spList; _spList != NULL; _spList = g_list_next(_spList))
    {
        /* delete the solar points */
        MH_Free(_spList->data);
        _spList->data = NULL;
    }
    g_list_free(spList);
    spList = NULL;
}

/**
 * This function returns the current position of the sun.
 *
 * @param coord coordinate type
 * @param current_time current time for point
 * @param x x position
 * @param y y position
 *
 * @return True or False
 */
gboolean SD_GetPoint(gint coord, gchar *current_time, gint *x, gint *y)
{
    gboolean rc = False;

    /* verify we have a valid solar list */
    if (thisSolarPointList != NULL)
    {
        /* locate point for current time */
        GList *spList = locatePoint(current_time);
        if (spList != NULL)
        {
            SolarPoint *sp = (SolarPoint *)spList->data;
            rc = True;

            /* if this solar point matches the current time, we're good to go */
            if (g_ascii_strcasecmp(sp->timeStr, current_time) == 0)
            {
                *x = sp->pt[coord].x;
                *y = sp->pt[coord].y;
            }
            else
            {
                if (g_list_next(spList) != NULL)
                {
                    glong sec1, sec2, sec3;
                    SolarPoint *spNext = (SolarPoint *)spList->next->data;

                    /* time not exact, pick closest point */
                    sec1 = StrToSecs(TS_UTC, current_time);
                    sec2 = StrToSecs(TS_UTC, sp->timeStr);
                    sec3 = StrToSecs(TS_UTC, spNext->timeStr);
                    if ((sec1 - sec2) <= (sec3 - sec1))
                    {
                        *x = sp->pt[coord].x;
                        *y = sp->pt[coord].y;
                    }
                    else
                    {
                        *x = spNext->pt[coord].x;
                        *y = spNext->pt[coord].y;
                    }
                }
            }
        }
    }
    return rc;
}

/**
 * This function draws the sun symbol on the display at the coordinates
 * matching the specified time.  It is used by the Predict dialog.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param current_time current time at point
 * @param drawable draw to this
 */
void SD_DrawSymbolAtTime(WindowData *windata, gint coord, gchar *current_time, GdkDrawable *drawable)
{
    if (thisSolarPointList != NULL)
    {
        gint x, y;

        /* locate point for current time */
        if (SD_GetPoint(coord, current_time, &x, &y) == True)
        {
            /* Draw the sun symbol */
            drawSymbol(windata, coord, (gint)x, (gint)y, drawable);
        }
    }
}


/**
 * This function draws the sun symbol on the display at the specified
 * coordinates.  It is used by the Real-Time dialog.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param x x position
 * @param y y position
 * @param drawable draw to this
 */
void SD_DrawSymbolAtPoint(WindowData *windata, gint coord, gint x, gint y, GdkDrawable *drawable)
{
    if (thisSolarPointList != NULL)
    {
        /* Draw the sun symbol */
        drawSymbol(windata, coord, x, y, drawable);
    }
}

/**
 * This function draws the predicted solar track that matches the prediction
 * events currently displayed.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param sets link list of predict sets
 * @param drawable draw to this
 */
void SD_DrawData(WindowData *windata, gint coord, GList *sets, GdkDrawable *drawable)
{
    if (thisSolarPointList != NULL && sets != NULL)
    {
        GList *psList;

        /* set foreground color to yellow */
        GdkGC *gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(SUN_COLOR));

        /* get the start and stop times from the current prediction events */
        for (psList = sets; psList != NULL; psList = psList->next)
        {
            SolarPoint *sp1, *sp2;
            PredictSet *ps = (PredictSet *)psList->data;

            /* draw solar data only for those events selected for viewing */
            if ((ps->displayed&DISPLAY_PD) && (ps->ptList != NULL))
            {
                GList *spList2;

                /* get the first solar point */
                GList *spList1 = locatePoint(ps->aosStr);
                if (spList1 == NULL)
                {
                    GList *spLast = g_list_last(thisSolarPointList);
                    SolarPoint *lastSolarPoint = (SolarPoint *)spLast->data;
                    SolarPoint *firstSolarPoint = (SolarPoint *)g_list_first(thisSolarPointList)->data;

                    /* not found, see if time is within our data set */
                    if ((g_ascii_strcasecmp(lastSolarPoint->timeStr, ps->aosStr) <= 0) ||
                        (g_ascii_strcasecmp(firstSolarPoint->timeStr, ps->losStr) >  0))
                    {
                        /* break out of for loop */
                        break;
                    }

                    /* use the first data point */
                    sp1 = (SolarPoint *)g_list_first(thisSolarPointList)->data;
                }

                /* move to next */
                spList2 = g_list_next(spList1);

                /* draw the solar line for this event */
                while (spList2 != NULL)
                {
                    sp2 = (SolarPoint *)spList2->data;
                    if (g_ascii_strcasecmp(sp2->timeStr, ps->losStr) <= 0)
                    {
                        /* Draw the solar line */
                        switch (coord)
                        {
                            case SBAND1:
                                drawSband1Line(windata, gc, spList1, spList2, drawable);
                                break;

                            case SBAND2:
                                drawSband2Line(windata, gc, spList1, spList2, drawable);
                                break;

                            case KUBAND:
                            case KUBAND2:
                                drawKubandLine(windata, gc, spList1, spList2, drawable, coord);
                                break;

                            case STATION:
                                drawStationLine(windata, gc, spList1, spList2, drawable);
                                break;

                            default:
                                break;
                        }
                    }

                    /* next data point */
                    spList1 = spList2;
                    spList2 = g_list_next(spList2);
                }
            }
        }
        g_object_unref(gc);
    }
}

/**
 * This function locates the solar point closest to the specified time.
 *
 * @param time time at point
 *
 * @return list pointer
 */
static GList *locatePoint(gchar *time)
{
    GList *spList;

    for (spList = thisSolarPointList; spList != NULL; spList = g_list_next(spList))
    {
        SolarPoint *sp = (SolarPoint *)spList->data;

        /* check if exact time found */
        if (g_ascii_strcasecmp(sp->timeStr, time) == 0)
            break;

        /* check if passed specified time */
        if (g_ascii_strcasecmp(sp->timeStr, time) > 0)
        {
            if (g_list_previous(spList) != NULL)
            {
                spList = g_list_previous(spList);
            }
            else
            {
                spList = NULL;
            }
            break;
        }
    }

    return spList;
}


/**
 * This function draws the symbol at the specified coordinates.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param x x position
 * @param y y position
 * @param drawable draw to this
 */
static void drawSymbol(WindowData *windata, gint coord, gint x, gint y, GdkDrawable *drawable)
{
    GdkGC *gc;

    /* set foreground color to yellow */
    gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(SUN_COLOR));

    /* convert point to window coordinates */
    WG_PointToWindow(coord,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     x, y, &x, &y);

    /* draw the sun symbol */
    gdk_draw_line(drawable, gc, x-6, y,   x+6, y  );
    gdk_draw_line(drawable, gc, x,   y-6, x,   y+6);
    gdk_draw_line(drawable, gc, x-4, y-4, x+4, y+4);
    gdk_draw_line(drawable, gc, x-4, y+4, x+4, y-4);

    gdk_draw_arc(drawable,gc,False, x-4, y-4, 8, 8, 0, 361 * 64);

    g_object_unref(gc);
}

/**
 * This function draws a solar trace line in S-Band 1 coordinates.
 *
 * @param windata window data
 * @param gc graphics context for drawing
 * @param s1 first solar point
 * @param s2 second solar point
 * @param drawable draw to this
 */
static void drawSband1Line(WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable)
{
    SolarPoint *sp1 = (SolarPoint *)spList1->data;
    SolarPoint *sp2 = (SolarPoint *)spList2->data;
    gint x1, y1, x2, y2;

    WG_PointToWindow(SBAND1,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     sp1->pt[SBAND1].x, sp1->pt[SBAND1].y, &x1, &y1);
    WG_PointToWindow(SBAND1,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     sp2->pt[SBAND1].x, sp2->pt[SBAND1].y, &x2, &y2);

    gdk_draw_line(drawable, gc, x1, y1, x2, y2);
}

/**
 * This function draws a solar trace line in S-Band 2 coordinates.
 *
 * @param windata window data
 * @param gc graphics context for drawing
 * @param s1 first solar point
 * @param s2 second solar point
 * @param drawable draw to this
 */
static void drawSband2Line(WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable)
{
    SolarPoint *sp1 = (SolarPoint *)spList1->data;
    SolarPoint *sp2 = (SolarPoint *)spList2->data;
    gint x1, y1, x2, y2;

    WG_PointToWindow(SBAND2,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     sp1->pt[SBAND2].x, sp1->pt[SBAND2].y, &x1, &y1);
    WG_PointToWindow(SBAND2,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     sp2->pt[SBAND2].x, sp2->pt[SBAND2].y, &x2, &y2);

    gdk_draw_line(drawable, gc, x1, y1, x2, y2);
}

/**
 * This function draws a solar trace line in Ku-Band coordinates.
 *
 * @param windata window data
 * @param gc graphics context for drawing
 * @param s1 first solar point
 * @param s2 second solar point
 * @param drawable draw to this
 */
static void drawKubandLine(WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable, gint coord)
{
    SolarPoint *sp1 = (SolarPoint *)spList1->data;
    SolarPoint *sp2 = (SolarPoint *)spList2->data;
    gint x1, y1, x2, y2;

    x1 = sp1->pt[coord].x;
    y1 = sp1->pt[coord].y;
    x2 = sp2->pt[coord].x;
    y2 = sp2->pt[coord].y;

    if (((y1 >= MIN_KU_ELEVATION*10) && (y1 <= MAX_KU_ELEVATION*10)) ||
        ((y2 >= MIN_KU_ELEVATION*10) && (y2 <= MAX_KU_ELEVATION*10)))
    {
        WG_PointToWindow(coord,
                         windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                         x1, y1, &x1, &y1);
        WG_PointToWindow(coord,
                         windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                         x2, y2, &x2, &y2);

        gdk_draw_line(drawable, gc, x1, y1, x2, y2);
    }
}


/**
 * This function draws a solar trace line in Station coordinates.
 *
 * @param windata window data
 * @param gc graphics context for drawing
 * @param s1 first solar point
 * @param s2 second solar point
 * @param drawable draw to this
 */
static void drawStationLine(WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable)
{
    SolarPoint *sp1 = (SolarPoint *)spList1->data;
    SolarPoint *sp2 = (SolarPoint *)spList2->data;
    gdouble y, ratio;
    gint az1, el1, az2, el2;
    gint x1, y1, x2, y2, dx, dy;
    gint wrap;
    gint max_az = MAX_STATION_AZIMUTH*10;
    gint min_az = MIN_STATION_AZIMUTH*10;
    Point *p0, *p3;

    az1 = sp1->pt[STATION].x;
    el1 = sp1->pt[STATION].y;
    az2 = sp2->pt[STATION].x;
    el2 = sp2->pt[STATION].y;
    WG_PointToWindow(STATION,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     az1, el1, &x1, &y1);
    WG_PointToWindow(STATION,
                     windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                     az2, el2, &x2, &y2);

    p0 = (spList1->prev == NULL ? NULL : &((SolarPoint *)(spList1->prev->data))->pt[STATION]);
    p3 = (spList2->next == NULL ? NULL : &((SolarPoint *)(spList2->next->data))->pt[STATION]);
    wrap = PDH_CheckWrap(p0, &(sp1->pt[STATION]), &(sp2->pt[STATION]), p3, STATION);
    if (wrap != 0)
    {
        if (y1 == y2)
        {
            y = (gdouble)y1;
        }
        else
        {
            if (wrap > 0)
            {
                ratio = (max_az - (gdouble)az1) / (max_az*2 + (az2 - az1));
            }
            else
            {
                ratio = (max_az + (gdouble)az1) / (max_az*2 - (az2 + az1));
            }

            y = el1 + (el2 - el1) * ratio;
        }

        WG_PointToWindow(STATION,
                         windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                         min_az, (gint)ROUND(y), &dx, &dy);

        gdk_draw_line(drawable, gc, x1, y1, (wrap > 0) ? 0 : dx, dy);
        gdk_draw_line(drawable, gc, (wrap > 0) ? dx : 0, dy, x2, y2);
    }
    else
    {
        gdk_draw_line(drawable, gc, x1, y1, x2, y2);
    }
}
