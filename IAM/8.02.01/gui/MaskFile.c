/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "MaskFile.h"
#undef PRIVATE

#include "ColorHandler.h"
#include "ConfigParser.h"
#include "Geometry.h"
#include "IspHandler.h"
#include "MessageHandler.h"
#include "MemoryHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "RealtimeDataHandler.h"
#include "WindowGrid.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/MaskFile.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* holds the values for the masks loaded from the data files */
static GPtrArray *thisMaskInfo[NUM_DISPLAYS][NUM_COORDS] =
{
    {NULL, NULL, NULL, NULL, NULL},
    {NULL, NULL, NULL, NULL, NULL}
};

/* holds the values for the kuband realtime masks from telemetry */
static GPtrArray *thisRealtimeKuBandMaskInfo = NULL;

/* holds the values the user provided via the PredictKuMaskDialog */
static GPtrArray *thisPredictKuBandMaskInfo = NULL;

/* holds the kuband mask names */
static gchar *thisKuBandMaskNames[MAX_KUBAND_MASKS];

/* GAO degree: 1=autotrack, 7=openloop */
static gint thisGAODegree = -1;

/* KuBand Cont Retry: 1=Enabled 0=Disabled */
static gint thisContRetry = -1;

/* KuBand TRC Antenna Mode: 0=Inhibit, 1=Openloop, 2=Autotrack, 3=Error */
static gint thisAntennaMode = -1;

/* the defined spiral degree value, found in iam.xml */
static gint thisSpiralDegree = -1;

/* KuBand mask colors */
static gint thisKuBandPrimaryMaskColor   = GC_GOLD;
static gint thisKuBandAutoTrackMaskColor = GC_WHITE;
static gint thisKuBandOpenLoopMaskColor  = GC_WHITE;
static gint thisKuBandSpiralMaskColor    = GC_BLUE;

/* holds the redraw state for each coordinate */
static gboolean thisRedraw[NUM_DISPLAYS][NUM_VIEWS] =
{
    { False, False, False, False, False, False, False, False},
    { False, False, False, False, False, False, False, False}
};

#define VALID_MASKNUM(masknum, len) ((masknum >= 0) && (masknum < len))

/*************************************************************************/

/**
 * Allocates storage for class variables and reads the mask data files.
 * This function is currently fragile, and will fail gracelessly upon trying to
 * read a bad file.
 */
void MF_Constructor(void)
{
    guint i, j;
    MaskInfo *mask_info;

    /* load the kuband mask colors */
    load_kuband_mask_colors();

    /* load the static blockage masks */
    load_static_masks();

    /* load the spiral degree value */
    load_spiral_degree();

    /* load kuband mask names */
    load_kuband_mask_names();

    /* allocate the realtime kuband mask information buffers */
    if (thisRealtimeKuBandMaskInfo == NULL)
    {
        thisRealtimeKuBandMaskInfo = g_ptr_array_new();
        for (i=0; i<MAX_KUBAND_MASKS; i++)
        {
            /* allocate mask information structure */
            mask_info = (MaskInfo *)MH_Calloc(sizeof(MaskInfo), __FILE__, __LINE__);

            /* load the path and data */
            mask_info->primary_visible = False;
            mask_info->autotrack_visible = False;
            mask_info->openloop_visible = False;
            mask_info->spiral_visible = False;
            mask_info->mask_path = NULL;
            mask_info->mask_name = NULL; /* not used */

            mask_info->mask_data = (PolyLine*)MH_Calloc(sizeof(PolyLine), __FILE__, __LINE__);
            mask_info->mask_data->ptCount = 0;
            mask_info->mask_data->pts = (Point*)MH_Calloc(sizeof(Point)*4, __FILE__, __LINE__);
            mask_info->mask_data->colorId = thisKuBandPrimaryMaskColor;
            mask_info->mask_data->fill = GC_STIPPLED;
            mask_info->mask_data->closed = GC_CLOSED;
            mask_info->mask_data->next = NULL;

            g_ptr_array_add(thisRealtimeKuBandMaskInfo, mask_info);
        }
    }

    if (thisPredictKuBandMaskInfo == NULL)
    {
        thisPredictKuBandMaskInfo = g_ptr_array_new();
        for (i=0; i<MAX_KUBAND_MASKS; i++)
        {
            /* allocate mask information structure */
            mask_info = (MaskInfo *)MH_Calloc(sizeof(MaskInfo), __FILE__, __LINE__);

            /* load the path and data */
            mask_info->primary_visible = False;
            mask_info->autotrack_visible = False;
            mask_info->openloop_visible = False;
            mask_info->spiral_visible = False;
            mask_info->mask_path = NULL;
            mask_info->mask_name = NULL; /* not used */

            mask_info->mask_data = (PolyLine*)MH_Calloc(sizeof(PolyLine), __FILE__, __LINE__);
            mask_info->mask_data->ptCount = 0;
            mask_info->mask_data->pts = (Point*)MH_Calloc(sizeof(Point)*4, __FILE__, __LINE__);
            mask_info->mask_data->colorId = thisKuBandPrimaryMaskColor;
            mask_info->mask_data->fill = GC_STIPPLED;
            mask_info->mask_data->closed = GC_CLOSED;
            mask_info->mask_data->next = NULL;

            g_ptr_array_add(thisPredictKuBandMaskInfo, mask_info);
        }
    }

    /* initialize the redraw flags */
    for (i=0; i<NUM_DISPLAYS; i++)
    {
        for (j=0; j<NUM_VIEWS; j++)
        {
            thisRedraw[i][j] = True;
        }
    }
}

/**
 * Releases memory for class variables
 */
void MF_Destructor(void)
{
    guint i;

    /* release the memory for the static masks */
    delete_static_masks();

    /* release the memory for the realtime masks */
    if (thisRealtimeKuBandMaskInfo != NULL)
    {
        for (i=0; i<thisRealtimeKuBandMaskInfo->len; i++)
        {
            MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisRealtimeKuBandMaskInfo, i);
            if (mask_info != NULL && mask_info->mask_data != NULL)
            {
                /* free the data */
                MH_Free(mask_info->mask_data->pts);
                MH_Free(mask_info->mask_data);
                mask_info->mask_data = NULL;

                g_free(mask_info->mask_name);
                mask_info->mask_name = NULL;

                MH_Free(mask_info->mask_path);
                mask_info->mask_path = NULL;

                MH_Free(mask_info);
                mask_info = NULL;
            }
        }

        g_ptr_array_free(thisRealtimeKuBandMaskInfo, False);
        thisRealtimeKuBandMaskInfo = NULL;
    }

    if (thisPredictKuBandMaskInfo != NULL)
    {
        for (i=0; i<thisPredictKuBandMaskInfo->len; i++)
        {
            MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisPredictKuBandMaskInfo, i);
            if (mask_info != NULL && mask_info->mask_data != NULL)
            {
                /* free the data */
                MH_Free(mask_info->mask_data->pts);
                MH_Free(mask_info->mask_data);
                mask_info->mask_data = NULL;

                g_free(mask_info->mask_name);
                mask_info->mask_name = NULL;

                MH_Free(mask_info->mask_path);
                mask_info->mask_path = NULL;

                MH_Free(mask_info);
                mask_info = NULL;
            }
        }

        g_ptr_array_free(thisPredictKuBandMaskInfo, False);
        thisPredictKuBandMaskInfo = NULL;
    }

    for (i=0; i<MAX_KUBAND_MASKS; i++)
    {
        g_free(thisKuBandMaskNames[i]);
    }
}

/**
 * Determines what the blockage mask is based on the input given
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param mask_num mask number
 *
 * @return blockage mask
 */
guint64 MF_GetBlockageMask(gint display, gint coord, gint mask_num)
{
    guint64 bit_mask = MF_NO_MASK;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            bit_mask = (guint64)(MF_SHIFT_MASK << (mask_num - 1));
        }
    }

    return bit_mask;
}

/**
 * This function sets the mask flags contained in the variable pt->masks to
 * reflect which masks the point pt lies within.  This function was designed to
 * set the mask values for a temporary interpolated point, hence the name. Used
 * by predict only.
 *
 * @param pt predict point
 * @param ptInISSAzElFlag point flag
 */
void MF_SetInterpolatedPointMask(PredictPt *pt, gint ptInISSAzElFlag)
{
    guint i, j, k;
    PolyLine *mask_data;
    Point cpt;
    gint coord = PD_GetCoordinateType();
    Dimension *d = PD_GetDrawingAreaSize();

    if (d != NULL)
    {
        if ((coord == SBAND1 || coord == SBAND2) && ptInISSAzElFlag != 1)
        {
            WG_PointToFlatCoord(coord,
                                d->width, d->height,
                                pt->sbandPt.x, pt->sbandPt.y, &(cpt.x), &(cpt.y), 1);

            for (i=0; i<thisMaskInfo[PREDICT][coord]->len; i++)   /* for each mask */
            {
                MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[PREDICT][coord], i);
                if (mask_info != NULL)
                {
                    for (mask_data = mask_info->mask_data; mask_data != NULL; mask_data = mask_data->next)
                    {
                        for (k=0; k<(guint)mask_data->ptCount; k++)
                        {
                            WG_PointToFlatCoord(coord,
                                                d->width, d->height,
                                                mask_data->pts[k].x, mask_data->pts[k].y,
                                                &(mask_data->pts[k].x), &(mask_data->pts[k].y), 1);
                        }
                    }
                }
            }
        }
        else if (coord == KUBAND)
        {
            cpt = pt->kubandPt;
        }
        else if (coord == KUBAND2)
        {
            cpt = pt->kubandPt2;
        }
        else
        {
            cpt = pt->rawPt;
        }

        for (i=0; i<thisMaskInfo[PREDICT][coord]->len; i++)
        {
            MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[PREDICT][coord], i);
            if (mask_info != NULL)
            {
                for (mask_data = mask_info->mask_data; mask_data != NULL; mask_data = mask_data->next)
                {
                    if (mask_data->closed == GC_CLOSED)
                    {
                        if (GP_InsidePolygon(cpt, mask_data->pts, mask_data->ptCount))
                        {
                            pt->masks |= PowerOf2(i);
                            break;
                        }
                    }
                    else
                    {
                        for (j=1; j<(guint)(mask_data->ptCount - 1); j++)
                        {
                            if (CCW(mask_data->pts[j-1], mask_data->pts[j], cpt) == 0)
                            {
                                pt->masks |= PowerOf2(i);
                                mask_data = NULL;
                                break;
                            }
                        }
                        if (mask_data == NULL)
                        {
                            break;
                        }
                    }
                }
            }
        }

        if ((coord == SBAND1 || coord == SBAND2) && ptInISSAzElFlag != 1)
        {
            for (i=0; i<thisMaskInfo[PREDICT][coord]->len; i++)   /* for each mask */
            {
                MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[PREDICT][coord], i);
                if (mask_info != NULL)
                {
                    for (mask_data = mask_info->mask_data; mask_data != NULL; mask_data = mask_data->next)
                    {
                        for (k=0; k<(guint)mask_data->ptCount; k++)
                        {
                            WG_PointToFlatCoord(coord,
                                                d->width, d->height,
                                                mask_data->pts[k].x, mask_data->pts[k].y,
                                                &(mask_data->pts[k].x), &(mask_data->pts[k].y), 0);
                        }
                    }
                }
            }
        }

        MH_Free(d);
    }
}

/**
 * Reloads the static masks. This usally occurs because the
 * user selected a different dynamic structure to display.
 */
void MF_ChangeMasks(void)
{
    MaskInfo *mask_info;

    /* array of booleans that keeps visibility state information */
    GArray *mask_visibility[NUM_DISPLAYS][NUM_COORDS] =
    {
        {NULL, NULL, NULL, NULL, NULL},
        {NULL, NULL, NULL, NULL, NULL}
    };
    gint d;
    gint c;
    guint i;

    /* allocate the mask information buffers */
    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            if (mask_visibility[d][c] == NULL)
            {
                mask_visibility[d][c] = g_array_new(True, True, sizeof(gboolean));
            }
        }
    }

    /* get the masks that are currently enabled */
    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            for (i=0; i<thisMaskInfo[d][c]->len; i++)
            {
                mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[d][c], i);
                if (mask_info != NULL)
                {
                    /* save the state of the primary mask */
                    g_array_append_val(mask_visibility[d][c], mask_info->primary_visible);
                }
            }
        }
    }

    /* release old mask data */
    delete_static_masks();

    /* load new masks, this will reallocate the mask ptr array, size
       can change */
    load_static_masks();

    /* re-enable masks */
    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            /* loop through on the size of the temporary mask buffer */
            for (i=0; i<mask_visibility[d][c]->len; i++)
            {
                /* get the state of the mask as it was */
                gboolean visibility = g_array_index(mask_visibility[d][c], gboolean, i);
                /* set the mask visibility */
                MF_SetVisibility(d, c, i, visibility);
            }
        }
    }

    /* release memory */
    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            g_array_free(mask_visibility[d][c], False);
        }
    }
}

/**
 * Reads the iam.xml file and gets the current value to use
 * for the spiral degree, the default is 2.
 */
static void load_spiral_degree(void)
{
    gchar value[32];

    if (CP_GetConfigData(SPIRAL_DEGREE, value) == True)
    {
        sscanf(value, "%d", &thisSpiralDegree);
    }
    else
    {
        thisSpiralDegree = DEFAULT_SPIRAL_DEGREE;
    }
}

/**
 * Loads the kuband mask colors.
 */
static void load_kuband_mask_colors(void)
{
    gchar value[32];

    if (CP_GetConfigData(KUBAND_PRIMARY_MASK_COLOR, value) == True)
    {
        thisKuBandPrimaryMaskColor = CH_GetColor(value);
    }

    if (CP_GetConfigData(KUBAND_AUTOTRACK_MASK_COLOR, value) == True)
    {
        thisKuBandAutoTrackMaskColor = CH_GetColor(value);
    }

    if (CP_GetConfigData(KUBAND_OPENLOOP_MASK_COLOR, value) == True)
    {
        thisKuBandOpenLoopMaskColor = CH_GetColor(value);
    }

    if (CP_GetConfigData(KUBAND_SPIRAL_MASK_COLOR, value) == True)
    {
        thisKuBandSpiralMaskColor = CH_GetColor(value);
    }
}

/**
 * Reads the iam.xml file and gets the names to use
 * for the kuband masks. If a mask name is not found,
 * the default name to use is KuBand Mask #n where n
 * is the next value to use in a series.
 */
static void load_kuband_mask_names(void)
{
    guint i;
    gchar value[32];
    gchar key[32];

    for (i=0; i<MAX_KUBAND_MASKS; i++)
    {
        g_sprintf(key, KUBAND_MASK_KEYWORD_FMT, i+1);
        if (CP_GetConfigData(key, value) == False)
        {
            g_sprintf(value, DEFAULT_KUBAND_MASK_NAME, i+1);
        }

        thisKuBandMaskNames[i] = g_strdup(value);
    }
}

/**
 * Reads the iam.xml, gets the mask name associated
 * with the mask number.
 *
 * @param mask_num mask number starting with zero.
 */
void MF_LoadMaskName(gint mask_num)
{
    gchar value[32];
    gchar key[32];

    if (VALID_MASKNUM(mask_num, (gint)MAX_KUBAND_MASKS))
    {
        g_sprintf(key, KUBAND_MASK_KEYWORD_FMT, mask_num+1);
        if (CP_GetConfigData(key, value) == False)
        {
            g_sprintf(value, DEFAULT_KUBAND_MASK_NAME, mask_num+1);
        }

        g_free(thisKuBandMaskNames[mask_num]);
        thisKuBandMaskNames[mask_num] = g_strdup(value);
    }
}

/**
 * Loads or reloads the static blockage masks
 */
static void load_static_masks(void)
{
    guint d, c;
    MaskInfo *mask_info = NULL;

    /* delete old mask data */
    delete_static_masks();

    /* allocate the mask information buffers */
    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            if (thisMaskInfo[d][c] == NULL)
            {
                thisMaskInfo[d][c] = g_ptr_array_new();
            }
        }
    }

    /* Read in each user defined masks */
    for (c=0; c<NUM_COORDS; c++)
    {
        gchar *coord_s = (c == SBAND1 ? "sband1" :
                          c == SBAND2 ? "sband2" :
                          c == KUBAND ? "kuband" :
                          c == KUBAND2 ? "kuband2" : "station");

        gchar *mask_coord_file = g_strconcat("masks.", coord_s, NULL);

        /* get the primary mask file that holds the list of mask names */
        gchar *mask_def_file = CP_GetMaskDefFile(coord_s);
        if (mask_def_file != NULL)
        {
            /* now open the file */
            FILE *fp = fopen(mask_def_file, "r");
            if (fp != NULL)
            {
                gchar line[256];
                while (fgets(line, 256, fp) != NULL)
                {
                    /* skip comments */
                    if (line[0] != '#')
                    {
                        /* find the newline */
                        gchar *p = strrchr(line, '\n');
                        if (p != NULL)
                        {
                            line[p - line] = '\0';
                        }

                        for (d=0; d<NUM_DISPLAYS; d++)
                        {
                            struct stat ss;

                            /* allocate mask information structure */
                            mask_info = (MaskInfo *)MH_Calloc(sizeof(MaskInfo), __FILE__, __LINE__);

                            /* load the path and data */
                            mask_info->primary_visible = False;
                            mask_info->autotrack_visible = False;
                            mask_info->openloop_visible = False;
                            mask_info->spiral_visible = False;
                            mask_info->mask_path = CP_GetMaskFile(coord_s, line);
                            mask_info->mask_data = load_mask_data(mask_info->mask_path);

                            if (mask_info->mask_path[0] && (stat(mask_info->mask_path, &ss) == 0))
                            {
                                FILE  *fp;
                                gchar line[256];
                                gchar name[256];

                                fp = fopen(mask_info->mask_path, "r");
                                if (fgets(line, 256, fp) != NULL)
                                {
                                    sscanf(line, "%*[^\"]\"%256[^\"]\"", name);
                                    mask_info->mask_name = g_strdup(name);
                                }
                                fclose(fp);
                            }
                            g_ptr_array_add(thisMaskInfo[d][c], mask_info);
                        }
                    }
                }

                fclose(fp);
            }
            else
            {
                gchar message[256];
                g_sprintf(message, "Unable to open blockage mask: %s!", mask_def_file);
                ErrorMessage(AM_GetActiveDisplay(), "Mask Open Error", message);

            }

            g_free(mask_def_file);
        }

        g_free(mask_coord_file);
    }
}

/**
 * Reloads the mask data. Deletes old data and loads new data.
 */
void MF_LoadMaskData(void)
{
    guint d, c, n;
    guint mask_count;
    MaskInfo *mask_info;

    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            mask_count = MF_GetMaskFileCount(d, c);
            for (n=0; n<mask_count; n++)
            {
                mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[d][c], n);
                if (mask_info != NULL)
                {
                    /* free old data */
                    delete_mask_data(mask_info->mask_data);

                    /* load new data */
                    mask_info->mask_data = load_mask_data(mask_info->mask_path);
                }
            }
        }
    }
}

/**
 * This function returns the data associated with the coordinate
 * and mask number provided.
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param mask_num mask number
 *
 * @return  PolyLine * - requested mask
 */
PolyLine *MF_GetMaskData(gint display, gint coord, gint mask_num)
{
    MaskInfo *mask_info;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                return mask_info->mask_data;
            }
        }
    }

    return NULL;
}

/**
 * Returns the realtime kuband mask data for the given mask number.
 *
 * @param mask_num mask number
 *
 * @return PolyLine mask data or NULL
 */
PolyLine *MF_GetRealtimeMaskData(gint mask_num)
{
    MaskInfo *mask_info;

    if (VALID_MASKNUM(mask_num, (gint)thisRealtimeKuBandMaskInfo->len))
    {
        mask_info = (MaskInfo *)g_ptr_array_index(thisRealtimeKuBandMaskInfo, mask_num);
        if (mask_info != NULL)
        {
            return mask_info->mask_data;
        }
    }

    return NULL;
}

/**
 * Returns the predict kuband mask data for the given mask number.
 *
 * @param mask_num
 *
 * @return PolyLine mask data or NULL
 */
PolyLine *MF_GetPredictMaskData(gint mask_num)
{
    MaskInfo *mask_info;

    if (VALID_MASKNUM(mask_num, (gint)thisPredictKuBandMaskInfo->len))
    {
        mask_info = (MaskInfo *)g_ptr_array_index(thisPredictKuBandMaskInfo, mask_num);
        if (mask_info != NULL)
        {
            return mask_info->mask_data;
        }
    }

    return NULL;
}

/**
 * Returns the realtime mask name for the given mask number.
 * Except for the limit mask.
 *
 * @param mask_num mask number
 *
 * @return make name or NULL
 */
gchar *MF_GetKuBandMaskName(gint mask_num)
{
    if (mask_num >= 0 && mask_num < MAX_KUBAND_MASKS)
    {
        return thisKuBandMaskNames[mask_num];
    }

    return NULL;
}

/**
 * Returns the number of masks found for the given coordinate
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 *
 * @return number of files found for coordinate or zero
 */
guint MF_GetMaskFileCount(gint display, gint coord)
{
    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (thisMaskInfo[display][coord] != NULL)
        {
            return thisMaskInfo[display][coord]->len;
        }
    }

    return 0;
}

/**
 * This function looks in a mask file to get the internal name of the mask.
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param mask_num mask number
 *
 * @return name of mask read from file
 */
gchar *MF_GetMaskName(gint display, gint coord, gint mask_num)
{
    MaskInfo *mask_info = NULL;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                return mask_info->mask_name;
            }
        }
    }

    return NULL;
}

/**
 * This method returns the color id associated with the mask number
 *
 * @param display PREDICT or REALTIME
 * @param coord one of SBAND1, SBAND2, KUBAND, or STATION
 * @param mask_num the mask number used for lookup
 *
 * @return color id or GC_WHITE if not found
 */
gint MF_GetMaskColor(gint display, gint coord, gint mask_num)
{
    MaskInfo *mask_info = NULL;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                return mask_info->mask_data->colorId;
            }
        }
    }

    return GC_WHITE;
}

/**
 * This function determines if a data file for the given mask exists,
 * and returns 1 if it does or 0 if it doesn't.
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param mask_num mask number
 *
 * @return 0 if doesn't exist, 1 if exists
 */
gint MF_MaskFileExists(gint display, gint coord, gint mask_num)
{
    MaskInfo *mask_info = NULL;
    struct stat ss;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                return mask_info->mask_path[0] && (stat(mask_info->mask_path, &ss) == 0);
            }
        }
    }

    return False;
}

/**
 * Finds the limit file and returns the mask number (index)
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 *
 * @return mask_num or -1 if not found
 */
guint MF_GetLimitMaskNum(gint display, gint coord)
{
    guint i;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (thisMaskInfo[display][coord] != NULL)
        {
            for (i=0; i<thisMaskInfo[display][coord]->len; i++)
            {
                if (MF_IsLimitMask(display, coord, i) == True)
                {
                    return i;
                }
            }
        }
    }

    return -1;
}

/**
 * Determines if the mask is the limits file
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param mask_num mask number
 *
 * @return True if this is the limits file otherwise False
 */
gboolean MF_IsLimitMask(gint display, gint coord, gint mask_num)
{
    gboolean isLimits = False;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (mask_num != -1)
        {
            MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                gchar *basename = g_path_get_basename(mask_info->mask_path);
                isLimits = g_str_has_suffix(basename, LIMITS);
                g_free(basename);
            }
        }
    }

    return isLimits;
}

/**
 * This function scans through all of the realtime masks and determines, for
 * which masks the antenna point lies within.  It returns False if not in
 * a mask and True if we are pointing in a mask.
 *
 * @param coord current coordinate type
 * @param width width of mask
 * @param height height of mask
 * @param azimuth azimuth
 * @param elevation elevation
 *
 * @return maskFlag for all realtime antenna positions
 */
gboolean MF_IsPointInMask(gint coord, gint width, gint height, gint azimuth, gint elevation)
{
    guint i;
    Point cpt;
    MaskInfo *mask_info;
    PolyLine *mask_data;
    gboolean ptInMask = False;

    /* process kuband separately */
    if (coord == KUBAND || coord == KUBAND2)
    {
        ptInMask = is_point_in_kuband_mask(azimuth, elevation);
        if (ptInMask == False)
        {
            /* check realtime kuband limit mask */
            gint mask_num = MF_GetLimitMaskNum(REALTIME, coord);
            if (mask_num != -1)
            {
                mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[REALTIME][coord], mask_num);
                if (mask_info != NULL && mask_info->primary_visible == True)
                {
                    cpt.x = azimuth;
                    cpt.y = elevation;
                    ptInMask = is_point_in_mask(REALTIME, coord, &cpt, MF_GetMaskData(REALTIME, coord, mask_num));
                }
            }
        }
    }
    else
    {
        switch (coord)
        {
            case SBAND1_VIEW:
            case SBAND2_VIEW:
                WG_PointToFlatCoord(coord,
                                    width, height,
                                    azimuth, elevation, &(cpt.x), &(cpt.y), 1);
                break;

            default:
                cpt.x = azimuth;
                cpt.y = elevation;
                break;
        }

        for (i=0; i<thisMaskInfo[REALTIME][coord]->len; i++)
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[REALTIME][coord], i);
            if (mask_info != NULL && mask_info->primary_visible == True)
            {
                mask_data = mask_info->mask_data;

                ptInMask = is_point_in_mask(REALTIME, coord, &cpt, mask_data);
                if (ptInMask == True)
                {
                    break;
                }
            }
        }
    }

    return ptInMask;
}

/**
 * Processes the logic for determining if the current realtime kuband masks
 * are intersecting the current tdrs location.
 *
 * @param azimuth azimuth
 * @param elevation elevation
 *
 * @return True or False
 */
static gboolean is_point_in_kuband_mask(gint azimuth, gint elevation)
{
    guint i;
    Point cpt;
    MaskInfo *mask_info;
    PolyLine *mask_data;
    gboolean ptInMask = False;

    cpt.x = azimuth;
    cpt.y = elevation;
    for (i=0; i<thisRealtimeKuBandMaskInfo->len && ptInMask == False; i++)
    {
        /* realtime display ignores the other visibility flags */
        /* the only flag its interested in, is the primary flag */
        mask_info = (MaskInfo *)g_ptr_array_index(thisRealtimeKuBandMaskInfo, i);
        if (mask_info != NULL && mask_info->primary_visible == True)
        {
            mask_data = mask_info->mask_data;
            ptInMask = is_point_in_mask(REALTIME, KUBAND_VIEW, &cpt, mask_data);
            if (ptInMask == False)
            {
                /* now check autotrack or openloop mask */
                if (thisGAODegree > 0)
                {
                    /* go build a mask data structure */
                    PolyLine *mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                               (thisGAODegree == 1?
                                                                thisKuBandAutoTrackMaskColor:
                                                                thisKuBandOpenLoopMaskColor),
                                                               thisGAODegree);
                    if (mask_data != NULL)
                    {
                        ptInMask = is_point_in_mask(REALTIME, KUBAND_VIEW, &cpt, mask_data);

                        /* release mask data */
                        MH_Free(mask_data->pts);
                        MH_Free(mask_data);
                    }
                }

                /* do the spiral mask */
                if (ptInMask == False && thisContRetry != -1 && thisAntennaMode != -1)
                {
                    /* check for enabled or not open_loop*/
                    if (thisContRetry == ENABLED || thisAntennaMode != OPENLOOP)
                    {
                        PolyLine *mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                                   thisKuBandSpiralMaskColor,
                                                                   thisGAODegree+thisSpiralDegree);
                        if (mask_data != NULL)
                        {
                            ptInMask = is_point_in_mask(REALTIME, KUBAND_VIEW, &cpt, mask_data);

                            /* release mask data */
                            MH_Free(mask_data->pts);
                            MH_Free(mask_data);
                        }
                    }
                }
            }
        }
    }

    return ptInMask;
}

/**
 * Traverses through the mask data to determine if the current point
 * is within the mask itself.
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param cpt current point
 * @param mask_data mask data
 *
 * @return True or False
 */
static gboolean is_point_in_mask(gint display, gint coord, Point *cpt, PolyLine *mask_data)
{
    /* while polygon isn't null */
    while (mask_data != NULL)
    {
        /* we got here because mask is on, check if point is inside polygon */
        if ((mask_data->closed == GC_CLOSED) &&
            GP_InsidePolygon(*cpt, mask_data->pts, mask_data->ptCount))
        {
            /* stop checking polygons in mask */
            return True;
        }
        /* otherwise, advance to next */
        else
        {
            mask_data = mask_data->next;
        }
    }

    return False;
}

/**
 * Draws the static mask indicated by flag to the screen in the drawingarea widget
 *
 * @param windata the window data
 * @param display current display
 * @param coord current coordinate type
 * @param mask_data mask data
 * @param drawable draw into this drawable
 */
static void draw_static_mask(WindowData *windata, gint display, gint coord, PolyLine *mask_data, GdkDrawable *drawable)
{
    gint i;
    gint x;
    gint y;
    gint count;
    GdkPoint *polygon;

    while (mask_data != NULL)
    {
        GdkGC *gc;
        GdkGCValues _values;
        GdkColor *color;
        GdkColor gcolor;

        count = mask_data->ptCount;
        if (mask_data->closed == GC_CLOSED)
        {
            count++;
        }

        polygon = (GdkPoint *) MH_Calloc(sizeof(GdkPoint) * (count), __FILE__, __LINE__);
        for (i=0; i<mask_data->ptCount; i++)
        {
            WG_PointToWindow(coord,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height,
                             mask_data->pts[i].x,
                             mask_data->pts[i].y,
                             &x, &y);

            polygon[i].x = x;
            polygon[i].y = y;
        }

        if (mask_data->closed == GC_CLOSED)
        {
            polygon[count-1] = polygon[0];
        }

        /* set the GC color */
        color = CH_GetGdkColorByName(CH_GetColorName(mask_data->colorId));
        gcolor.red = color->red;
        gcolor.green = color->green;
        gcolor.blue = color->blue;
        gcolor.pixel = color->pixel;
        _values.foreground = gcolor;
        gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FOREGROUND);

        gdk_draw_lines(drawable, gc, polygon, count);

        /* fill in polygon if closed and requested */
        if (mask_data->fill != GC_NONE &&
            mask_data->closed == GC_CLOSED &&
            count > 0)
        {
            set_fill_stype(windata, gc, mask_data->fill);
            gdk_draw_polygon(drawable, gc, True, polygon, count);
        }

        MH_Free(polygon);
        polygon = NULL;

        g_object_unref(gc);

        mask_data = mask_data->next;
    }
}

/**
 * Draws the realtime kuband masks that are enabled. Also if the gao degree value is
 * valid, then the mask is drawn. If the autotrack cont retry is enabled or the antenna
 * mode is not open loop, then draw the spiral mask.
 *
 * @param windata the window data
 * @param display current display
 * @param coord current coordinate type
 * @param drawable draw into this drawable
 */
static void draw_realtime_kuband_mask(WindowData *windata, gint display, gint coord, GdkDrawable *drawable)
{
    guint i;
    MaskInfo *mask_info;

    if (thisRealtimeKuBandMaskInfo != NULL)
    {
        for (i=0; i<thisRealtimeKuBandMaskInfo->len; i++)
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisRealtimeKuBandMaskInfo, i);
            if (mask_info != NULL)
            {

                /* Make sure terms are up to date */
                IH_UpdateKuMasks();

		// David
		MF_CheckKuBandMaskValue(REALTIME, i);

                /* draw primary mask if enabled */
                /* from the primary we determine which secondary masks are drawn */
                if (mask_info->primary_visible == True)
                {
                    /* draw primary mask */
                    draw_static_mask(windata, display, coord, mask_info->mask_data, drawable);

                    /* do the autotrack/openloop mask */
                    if (thisGAODegree > 0)
                    {
                        /* go build a mask data structure */
                        PolyLine *mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                                   (thisGAODegree == 1?
                                                                    thisKuBandAutoTrackMaskColor:
                                                                    thisKuBandOpenLoopMaskColor),
                                                                   thisGAODegree);
                        if (mask_data != NULL)
                        {
                            if (thisGAODegree == 1)
                            {
                                MF_SetKuBandVisibility(display, i, AUTOTRACK_MASK, True);
                            }
                            else
                            {
                                MF_SetKuBandVisibility(display, i, OPENLOOP_MASK, True);
                            }

                            /* call the draw method - draw the autotrack/openloop mask */
                            draw_static_mask(windata, display, coord, mask_data, drawable);

                            /* release mask data */
                            MH_Free(mask_data->pts);
                            MH_Free(mask_data);
                        }
                    }

                    /* do the spiral mask */
                    if (thisContRetry != -1 && thisAntennaMode != -1)
                    {
                        /* check for enabled or not open_loop*/
                        if (thisContRetry == ENABLED || thisAntennaMode != OPENLOOP)
                        {
                            PolyLine *mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                                       thisKuBandSpiralMaskColor,
                                                                       thisGAODegree+thisSpiralDegree);
                            if (mask_data != NULL)
                            {

                                MF_SetKuBandVisibility(display, i, SPIRAL_MASK, True);

                                /* call the draw method - draw the spiral mask */
                                draw_static_mask(windata, display, coord, mask_data, drawable);

                                /* release mask data */
                                MH_Free(mask_data->pts);
                                MH_Free(mask_data);
                            }
                        }
                    }
                }
                else
                {
                    /* turn off all secondary masks associated with */
                    /* this primary mask */
                    MF_SetKuBandVisibility(display, i, AUTOTRACK_MASK, False);
                    MF_SetKuBandVisibility(display, i, OPENLOOP_MASK, False);
                    MF_SetKuBandVisibility(display, i, SPIRAL_MASK, False);
                }
            }
        }
    }
}

/**
 * Draws the predict kuband masks that are enabled. Also if the gao degree value is
 * valid, then the mask is drawn. If the autotrack cont retry is enabled or the antenna
 * mode is not open loop, then draw the spiral mask.
 *
 * @param windata the window data
 * @param display current display
 * @param coord current coordinate type
 * @param drawable draw into this drawable
 */
static void draw_predict_kuband_mask(WindowData *windata, gint display, gint coord, GdkDrawable *drawable)
{
    guint i;
    MaskInfo *mask_info;

    if (thisPredictKuBandMaskInfo != NULL)
    {
        for (i=0; i<thisPredictKuBandMaskInfo->len; i++)
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisPredictKuBandMaskInfo, i);
            if (mask_info != NULL)
            {
                PolyLine *mask_data =  NULL;

                /* draw primary mask ? */
                if (mask_info->primary_visible == True) 
                {
                    draw_static_mask(windata, display, coord, mask_info->mask_data, drawable);
                }

                /* draw autotrack mask ? */
                if (mask_info->autotrack_visible == True)
                {
                    /* go build a mask data structure */
                    mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                     thisKuBandAutoTrackMaskColor,
                                                     1);
                    if (mask_data != NULL)
                    {
                        /* call the draw method - draw the autotrack mask */
                        draw_static_mask(windata, display, coord, mask_data, drawable);

                        /* release mask data */
                        MH_Free(mask_data->pts);
                        MH_Free(mask_data);
                        mask_data = NULL;
                    }
                }

                /* draw openloop mask ? */
                if (mask_info->openloop_visible == True)
                {
                    /* go build a mask data structure */
                    mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                     thisKuBandOpenLoopMaskColor,
                                                     7);
                    if (mask_data != NULL)
                    {
                        /* call the draw method - draw the openloop mask */
                        draw_static_mask(windata, display, coord, mask_data, drawable);

                        /* release mask data */
                        MH_Free(mask_data->pts);
                        MH_Free(mask_data);
                        mask_data = NULL;
                    }
                }

                /* draw spiral mask ? */
                if (mask_info->spiral_visible == True)
                {
                    mask_data = get_kuband_mask_data(mask_info->mask_data,
                                                     thisKuBandSpiralMaskColor,
                                                     1+thisSpiralDegree);
                    if (mask_data != NULL)
                    {
                        /* call the draw method - draw the spiral mask */
                        draw_static_mask(windata, display, coord, mask_data, drawable);

                        /* release mask data */
                        MH_Free(mask_data->pts);
                        MH_Free(mask_data);
                        mask_data = NULL;
                    }
                }
            }
        }
    }
}

/**
 * This method sets the initial values of the predict kuband
 * mask buffer to the realtime values as it knows it now. If
 * this method is called prior to an update from telemetry,
 * the result will not be reflected in the settings.
 */
void MF_SetKuBandPredictMasks(void)
{
    guint i;
    guint mask_num;

    /* make sure the predict mask buffer is valid */
    if (thisPredictKuBandMaskInfo != NULL)
    {
        for (mask_num=0; mask_num<MAX_KUBAND_MASKS; mask_num++)
        {
            for (i=0; i<MAX_VISIBILITY_MASKS; i++)
            {
                /* get the realtime mask settings */
                gboolean visibility = MF_GetKuBandVisibility(REALTIME, mask_num, i);

                /* set the predict masks */
                MF_SetKuBandVisibility(PREDICT, mask_num, i, visibility);
            }
        }
    }
}

void MF_ForceRedraw (gint display, gint coord_view)
{
    thisRedraw[display][coord_view] = True;
}

/**
 * Draws all of the masks marked as being active, except for the
 * limit mask, which is drawn separately.
 *
 * @param windata window data
 * @param display current display
 * @param coord_view current coordinate view
 */
void MF_DrawMasks(WindowData *windata, gint display, gint coord_view)
{
    guint i;
    gboolean pixmap_alloc = False;
    MaskInfo *mask_info = NULL;

    gint coord = windata->coordinate_type;
    if (coord != UNKNOWN_COORD)
    {
        if ((pixmap_alloc = AM_AllocatePixmapData(windata, windata->mask_data, False)) == True ||
            thisRedraw[display][coord_view] == True)
        {
            thisRedraw[display][coord_view] = False;

            /**
             * do we need to clear the drawable ?
             * if allocation did not occur, then the pixmap was not cleared
             * consequently we need to clear it before writing to it.
             */
            if (pixmap_alloc == False)
            {
                AM_ClearDrawable(windata->mask_data->pixmap,
                                 windata->drawing_area->style->black_gc,
                                 0, 0,
                                 windata->mask_data->width,
                                 windata->mask_data->height);
            }

            /* draw regular non-kuband masks */
            if (thisMaskInfo[display][coord] != NULL)
            {
                for (i=0; i<thisMaskInfo[display][coord]->len; i++)
                {
                    mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], i);
                    if (mask_info != NULL)
                    {
                        /* only draw those masks that are marked as visible and not the limits */
                        if (mask_info->primary_visible == True &&
                            MF_IsLimitMask(display, coord, i) == False)
                        {
                            draw_static_mask(windata, display, coord, mask_info->mask_data, windata->mask_data->pixmap);
                        }
                    }
                }
            }
        }

        /* render pixmap to drawable */
        if (windata->mask_data->pixmap != NULL)
        {
            AM_CopyPixmap(windata, windata->mask_data->pixmap);
        }
    }
}

/**
 * Draws the KuBand masks for the realtime and predict displays.
 * If the use_realtime flag is set and the display PREDICT, then
 * we'll use the realtime masks, otherwise the Predict mask data
 * will be used as defined by the user in the kuband mask
 * dialog.
 *
 * @param windata
 * @param display
 * @param coord_view
 */
void MF_DrawKuBandMasks(WindowData *windata, gint display, gint coord_view)
{
    guint i;
    gboolean pixmap_alloc = False;
    MaskInfo *mask_info = NULL;

    if ((pixmap_alloc = AM_AllocatePixmapData(windata, windata->mask_data, False)) == True ||
        thisRedraw[display][coord_view] == True)
    {
        gint coord = windata->coordinate_type;
        thisRedraw[display][coord_view] = False;

        /**
         * do we need to clear the drawable ?
         * if allocation did not occur, then the pixmap was not cleared
         * consequently we need to clear it before writing to it.
         */
        if (pixmap_alloc == False)
        {
            AM_ClearDrawable(windata->mask_data->pixmap,
                             windata->drawing_area->style->black_gc,
                             0, 0,
                             windata->mask_data->width,
                             windata->mask_data->height);
        }

        /* draw regular non-kuband masks */
        if (thisMaskInfo[display][coord] != NULL)
        {
            for (i=0; i<thisMaskInfo[display][coord]->len; i++)
            {
                mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], i);
                if (mask_info != NULL)
                {
                    /* only draw those masks that are marked as visible and not the limits */
                    if (mask_info->primary_visible == True &&
                        MF_IsLimitMask(display, coord, i) == False)
                    {
                        draw_static_mask(windata, display, coord, mask_info->mask_data, windata->mask_data->pixmap);
                    }
                }
            }
        }

        if (display == REALTIME)
        {
            draw_realtime_kuband_mask(windata, display, coord, windata->mask_data->pixmap);
        }
        else
        {
            draw_predict_kuband_mask(windata, display, coord, windata->mask_data->pixmap);
        }
    }

    /* render pixmap to drawable */
    if (windata->mask_data->pixmap != NULL)
    {
        AM_CopyPixmap(windata, windata->mask_data->pixmap);
    }
}

/**
 * Draws the limit mask for the display and coordinate view
 *
 * @param windata has the drawable
 * @param display the display
 * @param coord_view the view
 */
void MF_DrawLimitMask(WindowData *windata, gint display, gint coord_view)
{
    gint coord = windata->coordinate_type;
    GdkDrawable *drawable = AM_Drawable(windata,windata->which_drawable);

    draw_limit_mask(windata, display, coord, drawable);
}

/**
 * Draws the limit mask for the specified display and coordinate view.
 *
 * @param windata window data
 * @param display current display
 * @param coord current coordinate type
 * @param drawable draw into this drawable
 */
static void draw_limit_mask(WindowData *windata, gint display, gint coord, GdkDrawable *drawable)
{
    gint mask_num = MF_GetLimitMaskNum(display, coord);

    if (mask_num != -1)
    {
        MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
        if (mask_info != NULL)
        {
            if (mask_info->primary_visible == True)
            {
                draw_static_mask(windata, display, coord, mask_info->mask_data, drawable);
            }
        }
    }
}

/**
 * Returns the primary visibility state for the given mask
 *
 * @param display which display, predict or realtime
 * @param coord which coordinate type
 * @param mask_num which mask
 *
 * @return True if mask is visible, otherwise False
 */
gboolean MF_GetVisibility(gint display, gint coord, gint mask_num)
{
    MaskInfo *mask_info = NULL;

    if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
    {
        mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
        if (mask_info != NULL)
        {
            return mask_info->primary_visible;
        }
    }

    return False;
}

/**
 * Sets the visibility state for the given mask
 *
 * @param display
 * @param coord
 * @param mask_num
 * @param visible
 */
void MF_SetVisibility(gint display, gint coord, gint mask_num, gboolean visible)
{
    MaskInfo *mask_info = NULL;

    if (VALID_DISPLAY(display) && VALID_COORD(coord))
    {
        if (VALID_MASKNUM(mask_num, (gint)thisMaskInfo[display][coord]->len))
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[display][coord], mask_num);
            if (mask_info != NULL)
            {
                mask_info->primary_visible = visible;
                switch (coord)
                {
                    case SBAND1:
                        thisRedraw[display][SBAND1_VIEW] = True;
                        thisRedraw[display][S1KU_SBAND_VIEW] = True;
                        break;

                    case SBAND2:
                        thisRedraw[display][SBAND2_VIEW] = True;
                        thisRedraw[display][S2KU_SBAND_VIEW] = True;
                        break;

                    case KUBAND:
                    case KUBAND2:
                        thisRedraw[display][KUBAND_VIEW] = True;
                        thisRedraw[display][S1KU_KUBAND_VIEW] = True;
                        thisRedraw[display][S2KU_KUBAND_VIEW] = True;
                        break;

                    case STATION:
                        thisRedraw[display][STATION_VIEW] = True;
                        break;

                    default:
                        thisRedraw[display][coord] = True;
                        break;
                }
            }
        }
    }
}

/**
 * Returns the bit mask of all masks currently visible for the given
 * display and coordinate modes
 *
 * @param display current display
 * @param coord current coordinate type
 *
 * @return bit-mask of visible blockage masks or 0
 */
guint64 MF_GetBitMask(gint display, gint coord)
{
    MaskInfo *mask_info = NULL;
    guint i;
    guint64 bit_mask = 0;

    /* for realtime and kuband, get data from different location */
    if (display == REALTIME && (coord == KUBAND || coord == KUBAND2))
    {
        for (i=0; i<thisRealtimeKuBandMaskInfo->len; i++)
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisRealtimeKuBandMaskInfo, i);
            if (mask_info != NULL)
            {
                if (mask_info->primary_visible == True)
                {
                    /* make a bit mask */
                    bit_mask |= (guint64)PowerOf2(i);
                }
            }
        }
    }
    else
    {
        for (i=0; i<thisMaskInfo[display][coord]->len; i++)
        {
            mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[REALTIME][coord], i);
            if (mask_info != NULL)
            {
                if (mask_info->primary_visible == True &&
                    MF_IsLimitMask(display, coord, i) == False)
                {
                    bit_mask |= (guint64)PowerOf2(i);
                }
            }
        }
    }

    return bit_mask;
}

/**
 * Sets the visible flag for the realtime kuband masks.
 *
 * @param display current display, PREDICT or REALTIME
 * @param mask_num mask number
 * @param which_mask which mask flag
 * @param active is mask active flag
 */
void MF_SetKuBandVisibility(gint display, gint mask_num, gint which_mask, gboolean active)
{
    gboolean redraw = False;
    GPtrArray *ptr_array = get_kuband_mask_info(display);

    /* validate mask number */
    if (VALID_MASKNUM(mask_num, (gint)ptr_array->len))
    {
        MaskInfo *mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
        if (mask_info != NULL)
        {
            switch (which_mask)
            {
                default:
                case PRIMARY_MASK:
                    if (mask_info->primary_visible != active)
                    {
                        mask_info->primary_visible = active;
                        redraw = True;
                    }
                    break;

                case AUTOTRACK_MASK:
                    if (mask_info->autotrack_visible != active)
                    {
                        mask_info->autotrack_visible = active;
                        redraw = True;
                    }
                    break;

                case OPENLOOP_MASK:
                    if (mask_info->openloop_visible != active)
                    {
                        mask_info->openloop_visible = active;
                        redraw = True;
                    }
                    break;

                case SPIRAL_MASK:
                    if (mask_info->spiral_visible != active)
                    {
                        mask_info->spiral_visible = active;
                        redraw = True;
                    }
                    break;
            }

            if (redraw == True)
            {
                RTD_SetRedraw(True);
                thisRedraw[display][KUBAND_VIEW] = True;
                thisRedraw[display][S1KU_KUBAND_VIEW] = True;
                thisRedraw[display][S2KU_KUBAND_VIEW] = True;
            }
        }
    }
}

/**
 * Sets the visible flag for the realtime kuband masks.
 *
 * @param display current display, PREDICT or REALTIME
 * @param mask_num mask number
 * @param which_mask which mask flag
 * @param active is mask active flag
 */
void MF_SetKuBandVisibility2(gint display, gint mask_num, gint which_mask, gboolean active)
{
    gboolean redraw = False;
    GPtrArray *ptr_array = get_kuband_mask_info(display);

    /* validate mask number */
    if (VALID_MASKNUM(mask_num, (gint)ptr_array->len))
    {
        MaskInfo *mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
        if (mask_info != NULL)
        {
            switch (which_mask)
            {
                default:
                case PRIMARY_MASK:
                    if (mask_info->primary_visible != active)
                    {
                        mask_info->primary_visible = active;
                        redraw = True;
                    }
                    break;

                case AUTOTRACK_MASK:
                    if (mask_info->autotrack_visible != active)
                    {
                        mask_info->autotrack_visible = active;
                        redraw = True;
                    }
                    break;

                case OPENLOOP_MASK:
                    if (mask_info->openloop_visible != active)
                    {
                        mask_info->openloop_visible = active;
                        redraw = True;
                    }
                    break;

                case SPIRAL_MASK:
                    if (mask_info->spiral_visible != active)
                    {
                        mask_info->spiral_visible = active;
                        redraw = True;
                    }
                    break;
            }

            if (redraw == True)
            {
                RTD_SetRedraw(True);
                thisRedraw[display][KUBAND_VIEW] = True;
                thisRedraw[display][S1KU_KUBAND_VIEW] = True;
                thisRedraw[display][S2KU_KUBAND_VIEW] = True;
            }
        }
    }
}

/**
 * Gets the visibility flag for the realtime kuband masks.
 *
 * @param display current display
 * @param mask_num mask number
 * @param which_mask which mask flag
 */
gboolean MF_GetKuBandVisibility(gint display, gint mask_num, gint which_mask)
{
    gboolean visibility = False;
    GPtrArray *ptr_array = get_kuband_mask_info(display);

    /* validate mask number */
    if (VALID_MASKNUM(mask_num, (gint)ptr_array->len))
    {
        MaskInfo *mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
        if (mask_info != NULL)
        {
            switch (which_mask)
            {
                default:
                case PRIMARY_MASK:
                    visibility = mask_info->primary_visible;
                    break;

                case AUTOTRACK_MASK:
                    visibility = mask_info->autotrack_visible;
                    break;

                case OPENLOOP_MASK:
                    visibility = mask_info->openloop_visible;
                    break;

                case SPIRAL_MASK:
                    visibility = mask_info->spiral_visible;
                    break;
            }
        }
    }

    return visibility;
}

/**
 * Stores the limit values into the mask_info for the given mask number.
 *
 * @param display current display
 * @param mask_limit mask limit value
 * @param value value for mask
 */
void MF_SetKuBandMaskValue(gint display, gint mask_limit, gint value)
{
    gint mask_num = mask_limit/4;
    GPtrArray *ptr_array = get_kuband_mask_info(display);

    /* validate mask number */
    if (VALID_MASKNUM(mask_num, (gint)ptr_array->len))
    {
        MaskInfo *mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
        if (mask_info != NULL)
        {
            /* set the point count, eventually we get 4 points */
            mask_info->mask_data->ptCount = 4;

            /* load the point */
            switch (mask_limit%4)
            {
                case LOWER_EL:
                    /* all lower el masks are placed into slots 0 and 1 */
                    mask_info->mask_data->pts[0].y = value;
                    mask_info->mask_data->pts[1].y = value;
                    break;

                case UPPER_EL:
                    /* all upper el masks are placed into slots 2 and 3 */
                    mask_info->mask_data->pts[2].y = value;
                    mask_info->mask_data->pts[3].y = value;
                    break;

                case LOWER_XEL:
                    /* all lower xel masks are placed into slots 0 and 3 */
                    mask_info->mask_data->pts[0].x = value;
                    mask_info->mask_data->pts[3].x = value;
                    break;

                case UPPER_XEL:
                    /* all upper xel masks are placed into slots 1 and 2 */
                    mask_info->mask_data->pts[1].x = value;
                    mask_info->mask_data->pts[2].x = value;
                    break;
            }
        }

        /* we need to draw the mask */
        thisRedraw[display][KUBAND_VIEW] = True;
        thisRedraw[display][S1KU_KUBAND_VIEW] = True;
        thisRedraw[display][S2KU_KUBAND_VIEW] = True;
    }
}

/* 
 * We can only check a mask after getting all 4 pts
 *
 */
void MF_CheckKuBandMaskValue(gint display, gint mask_num)
{
  GPtrArray *ptr_array = get_kuband_mask_info(display);
  MaskInfo *mask_info;
  char title[100];
  char msg[100];
  gboolean warn;

    mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
    sprintf(title, "Kuband Mask error");
    sprintf(msg, "Mask %s definition is incorrect:", 
	    MF_GetKuBandMaskName(mask_num));

    warn = FALSE;
    // Check EL
    if (mask_info->mask_data->pts[0].y > mask_info->mask_data->pts[2].y) {
	  warn = TRUE;
	  sprintf(msg, "%s EL [%d] > [%d]\n", 
		  msg, mask_info->mask_data->pts[0].y/ 10, mask_info->mask_data->pts[2].y/10);
    }
    // Check XEL
    if (mask_info->mask_data->pts[0].x > mask_info->mask_data->pts[1].x) {
	  warn = TRUE;
	  sprintf(msg, "%s XEL [%d] > [%d]\n", 
		  msg, mask_info->mask_data->pts[0].x/ 10, mask_info->mask_data->pts[1].x/ 10);
    }
    if (warn) {
      WarningMessage(AM_GetActiveDisplay(), title, msg);
    }

}

/**
 * Clears the point count and the values to zero
 *
 * @param display either PREDICT or REALTIME
 * @param mask_limit each mask has four limit values
 */
void MF_ClearKuBandMaskPtCount(gint display, gint mask_limit)
{
    gint mask_num = mask_limit/4;
    GPtrArray *ptr_array = get_kuband_mask_info(display);

    /* validate mask number */
    if (VALID_MASKNUM(mask_num, (gint)ptr_array->len))
    {
        MaskInfo *mask_info = (MaskInfo*)g_ptr_array_index(ptr_array, mask_num);
        if (mask_info != NULL)
        {
            /* clear the point count */
            mask_info->mask_data->ptCount = 0;

            /* clear the points */

            /* all lower el masks are placed into slots 0 and 1 */
            mask_info->mask_data->pts[0].y = 0;
            mask_info->mask_data->pts[1].y = 0;

            /* all upper el masks are placed into slots 2 and 3 */
            mask_info->mask_data->pts[2].y = 0;
            mask_info->mask_data->pts[3].y = 0;

            /* all lower xel masks are placed into slots 0 and 3 */
            mask_info->mask_data->pts[0].x = 0;
            mask_info->mask_data->pts[3].x = 0;

            /* all upper xel masks are placed into slots 1 and 2 */
            mask_info->mask_data->pts[1].x = 0;
            mask_info->mask_data->pts[2].x = 0;
        }
    }
}

/**
 * Saves the kuband gao value. This is used to determine what
 * degree to draw around the primary mask. The degree indicates
 * the size of the mask to draw; either 1 or 7 degree usally.
 *
 * @param degree degree value
 */
void MF_SetGAOMask(gint degree)
{
    thisGAODegree = degree;
}

/**
 * Returns the GAO degree value currently active from the
 * downlink.
 *
 * @return gint GAO degree
 */
gint MF_GetGAOMask(void)
{
    return thisGAODegree;
}

/**
 * Saves the kuband autotrack cont retry value. This is used
 * with the trc antenna mode to determine how to draw the
 * spiral mask.
 *
 * @param cont_retry continue retry flag
 */
void MF_SetContRetry(gint cont_retry)
{
    thisContRetry = cont_retry;
}

/**
 * Returns the kuband autotrack cont retry value from the
 * downlink.
 *
 * @return gint kuband autotrack cont retry value
 */
gint MF_GetContRetry(void)
{
    return thisContRetry;
}

/**
 * Saves the kuband trc antenna mode value. This is used
 * with the autotrack cont retry value to determine how
 * to draw the spiral mask.
 *
 * @param antenna_mode antenna mode flag
 */
void MF_SetAntennaMode(gint antenna_mode)
{
    thisAntennaMode = antenna_mode;
}

/**
 * Returns the kuband trc antenna mode from the downlink.
 *
 * @return gint kuband trc antenna mode
 */
gint MF_GetAntennaMode(void)
{
    return thisAntennaMode;
}

/**
 * Returns the kuband mask information.
 *
 * @param display which mask info to return
 *
 * @return GPtrArray* has the MaskInfo
 */
static GPtrArray *get_kuband_mask_info(gint display)
{
    if (display == PREDICT)
    {
        /* return predict info */
        return thisPredictKuBandMaskInfo;
    }

    /* return realtime info */
    return thisRealtimeKuBandMaskInfo;
}

/**
 * Given the data and degree offset, this method computes a new
 * set of mask data to be drawn.
 *
 * @param data polyline data
 * @param colorId color to use for line
 * @param degree degree value
 *
 * @return Polyline data or NULL
 */
static PolyLine *get_kuband_mask_data(PolyLine *data, gint colorId, gint degree)
{
    PolyLine *mask_data = NULL;
    guint degree_offset = degree * 10;

    if (data != NULL && data->ptCount > 0)
    {
        mask_data = (PolyLine *)MH_Calloc(sizeof(PolyLine), __FILE__, __LINE__);

        mask_data->pts = (Point *)MH_Calloc(sizeof(Point)*data->ptCount, __FILE__, __LINE__);
        mask_data->ptCount = data->ptCount;
        mask_data->closed = GC_CLOSED;
        mask_data->colorId = colorId;
        mask_data->fill = GC_NONE;
        mask_data->next = NULL;

#define LOWER_XEL_0 0
#define LOWER_EL_0  0
#define UPPER_XEL_1 1
#define LOWER_EL_1  1
#define UPPER_XEL_2 2
#define UPPER_EL_2  2
#define LOWER_XEL_3 3
#define UPPER_EL_3  3

        /* subtract from lower and add to upper */
        mask_data->pts[LOWER_XEL_0].x += data->pts[LOWER_XEL_0].x - degree_offset;
        mask_data->pts[LOWER_EL_0].y  += data->pts[LOWER_EL_0].y - degree_offset;

        mask_data->pts[UPPER_XEL_1].x += data->pts[UPPER_XEL_1].x + degree_offset;
        mask_data->pts[LOWER_EL_1].y  += data->pts[LOWER_EL_1].y - degree_offset;

        mask_data->pts[UPPER_XEL_2].x += data->pts[UPPER_XEL_2].x + degree_offset;
        mask_data->pts[UPPER_EL_2].y  += data->pts[UPPER_EL_2].y  + degree_offset;

        mask_data->pts[LOWER_XEL_3].x += data->pts[LOWER_XEL_3].x - degree_offset;
        mask_data->pts[UPPER_EL_3].y  += data->pts[UPPER_EL_3].y  + degree_offset;
    }

    return mask_data;
}

/**
 * Sets the fill style for the gc. If the fill style is stipple, then a background
 * stipple bitmap is created and set for the gc
 *
 * @param windata window data
 * @param gc graphics context
 * @param fillStyle line fill style
 */
static void set_fill_stype(WindowData *windata, GdkGC *gc, gint fillStyle)
{
    /**
     * the width/height define the spacing in the stipple
     * the larger the number, causes increased spacing
     */
#define gray50_width    2
#define gray50_height   3

    gchar gray50_bits[] ={
        0x01,
        0x02
    };
    GdkBitmap *mask;

    if (gc != NULL)
    {
        if (fillStyle == GC_STIPPLED)
        {
            fillStyle = GDK_STIPPLED;

            mask = gdk_bitmap_create_from_data(NULL, gray50_bits, gray50_width, gray50_height);
            gdk_gc_set_stipple(gc, mask);
            g_object_unref(mask);
        }

        else
        {
            fillStyle = GDK_SOLID;
        }

        gdk_gc_set_line_attributes(gc,1,GDK_LINE_SOLID,GDK_CAP_BUTT,GDK_JOIN_BEVEL);
        gdk_gc_set_fill(gc, (GdkFill)fillStyle);
    }
}

/**
 * Frees the memory associated with the static masks
 */
static void delete_static_masks(void)
{
    guint d, c, j;

    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_COORDS; c++)
        {
            if (thisMaskInfo[d][c] != NULL)
            {
                for (j=0; j<thisMaskInfo[d][c]->len; j++)
                {
                    MaskInfo *mask_info = (MaskInfo *)g_ptr_array_index(thisMaskInfo[d][c], j);
                    if (mask_info != NULL)
                    {
                        if (mask_info->mask_data != NULL)
                        {
                            delete_mask_data(mask_info->mask_data);  /* free PolyLines for mask */
                        }
                        mask_info->mask_data = NULL;           /* set mask to NULL */

                        g_free(mask_info->mask_path);
                        mask_info->mask_path = NULL;

                        MH_Free(mask_info);
                        mask_info = NULL;
                    }
                }
            }

            /* now free the mask information */
            if (thisMaskInfo[d][c] != NULL)
            {
                g_ptr_array_free(thisMaskInfo[d][c], False);
                thisMaskInfo[d][c] = NULL;
            }
        }
    }
}

/**
 * This function disposes of the memory allocated to a PolyLine, including
 * successive PolyLines contained in the linked list.
 * UPDATES   PolyLine *pLine - deletes all data pointed to
 *
 * @param pLine polyline array
 */
static void delete_mask_data(PolyLine *pLine)
{
    PolyLine *pLine2;            /* pointer to next PolyLine */

    while (pLine != NULL)        /* while the PolyLine exists */
    {
        pLine2 = pLine->next;    /* get pointer to next PolyLine */
        if (pLine->pts != NULL)
        {
            MH_Free(pLine->pts);    /* free space for points */
        }
        MH_Free(pLine);             /* free space for PolyLine */
        pLine = pLine2;          /* advance to next PolyLine */
    }
}

/**
 * This function reads a mask from the given file and returns it.
 *
 * @param fname filename
 *
 * @return loaded mask
 */
static PolyLine *load_mask_data(gchar *fname)
{
    FILE  *in;
    PolyLine *first = NULL;
    PolyLine *pl    = NULL;
    gchar line[256];   /* input line from file */
    gchar color[20];
    gint plCount;     /* number of PolyLines for current mask */
    gint i, j;

    if ((in = fopen(fname, "r")) == NULL)
    {
        return NULL;
    }

    pl = NULL;
    first = NULL;
    line[0] = '\0';
    if (!feof(in))
    {
        /* get first line */
        fgets(line, 256, in);
    }

    if (sscanf(line, "%d", &plCount) == 1)
    {
        for (i = 0; i < plCount; i++)
        {
            /* if it is the first polyLine for the mask */
            if (pl == NULL)
            {
                /* create polyline */
                pl = (PolyLine *) MH_Calloc(sizeof(PolyLine), __FILE__, __LINE__);
                first = pl;
            }
            else
            {
                /* create polyline at end of list and advance to end of list */
                pl->next = (PolyLine *) MH_Calloc(sizeof(PolyLine), __FILE__, __LINE__);
                pl = pl->next;
            }
            /* initialize next pointer for final node */
            pl->next = NULL;
            line[0] = '\0';

            if (!feof(in))
            {
                /* get next line */
                fgets(line, 256, in);
            }
            if (sscanf(line, "%d %d", &(pl->ptCount), &(pl->closed)) == 2)
            {
                line[0] = '\0';
                if (!feof(in))
                {
                    fgets(line, 256, in); /* read the next line */
                }

                sscanf(line, "%d %15s", &(pl->fill), color);
                pl->colorId = CH_GetColor(color);
                if (pl->colorId == GC_INVALIDCOLOR)
                {
                    pl->colorId = GC_WHITE;
                }

                /* allocate space for points */
                pl->pts = (Point *) MH_Calloc(sizeof(Point)*(pl->ptCount), __FILE__, __LINE__);
                for (j = 0; j < pl->ptCount; j++)
                {
                    line[0] = '\0';
                    if (!feof(in))
                    {
                        /* read the next line */
                        fgets(line, 256, in);
                    }

                    pl->pts[j].x = 0;
                    pl->pts[j].y = 0;

                    /* get the x,y values */
                    sscanf(line, "%d %d", &(pl->pts[j].x), &(pl->pts[j].y));
                }
            }
        }
    }
    fclose(in);

    return first;
}
