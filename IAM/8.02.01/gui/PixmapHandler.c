/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "PixmapHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "ColorHandler.h"
#include "Conversions.h"
#include "Dialog.h"
#include "FontHandler.h"
#include "MemoryHandler.h"
#include "RealtimeDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PixmapHandler.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define POLAR_BORDER        50
#define NUM_GRID_SEGMENTS   5

#define MAJOR_COLOR         GC_GRAY
#define MINOR_COLOR         GC_GRAY50
#define MINOR_TEXT_COLOR    GC_LAVENDER
#define MINOR_TEXT_COLOR2   GC_DARKSLATEGRAY

static GtkWidget            *thisWidget     = NULL;
static gint                  thisWidth       = 0;
static gint                  thisHeight      = 0;

static const gchar           *LABEL_0        = "0";

static const gchar           *LABEL_NEG_25   = "-25";
static const gchar           *LABEL_25       = "25";

static const gchar           *LABEL_NEG_30   = "-30";
static const gchar           *LABEL_30       = "30";

static const gchar           *LABEL_NEG_45   = "-45";
static const gchar           *LABEL_45       = "45";

static const gchar           *LABEL_NEG_50   = "-50";
static const gchar           *LABEL_50       = "50";

static const gchar           *LABEL_NEG_60   = "-60";
static const gchar           *LABEL_60       = "60";

static const gchar           *LABEL_NEG_75   = "-75";
static const gchar           *LABEL_75       = "75";

static const gchar           *LABEL_NEG_90   = "-90";
static const gchar           *LABEL_90       = "90";

static const gchar           *LABEL_NEG_135  = "-135";
static const gchar           *LABEL_135      = "135";

static const gchar           *LABEL_NEG_180  = "-180";
static const gchar           *LABEL_180      = "180";

static const gchar           *LABEL_NEG_235  = "-235";
static const gchar           *LABEL_235      = "235";

static const gchar           *LABEL_AZ       = "AZ";
static const gchar           *LABEL_ELEV     = "Elev";

static const gchar           *LABEL_AFT      = "AFT";
static const gchar           *LABEL_AFT_X    = "AFT (-X)";
static const gchar           *LABEL_FWD      = "FWD";
static const gchar           *LABEL_FWD_X    = "FWD (+X)";

static const gchar           *LABEL_PORT_Y   = "PORT (-Y)";
static const gchar           *LABEL_STBD_Y   = "STBD (+Y)";

static const gchar           *LABEL_CROSS_ELEV = "Cross-Elevation";
/*************************************************************************/

/**
 * Allocates storage for the pixmaps
 */
void PH_Constructor(void)
{
}

/**
 * Deallocates memory for the pixmaps
 */
void PH_Destructor(void)
{
}

/**
 * Sets parameter values
 *
 * @param windata
 */
static void setParams(WindowData *windata)
{
    g_assert (windata != NULL);
    g_assert (GTK_IS_DRAWING_AREA(windata->drawing_area));

    /* save the pointer to the widget */
    thisWidget = windata->drawing_area;

    /* get the width and height for convenience */
    thisWidth  = thisWidget->allocation.width;
    thisHeight = thisWidget->allocation.height;
}

/**
 * Resets the class variables
 */
static void resetParams(void)
{
    thisWidget = NULL;
}

/**
 * Gets the drawing area index from the coordinate view.
 * Only the primary drawing areas are returned, the
 * left/bottom drawing areas cannot be determined from
 * the view.
 *
 * @param coord_view current coordinate view
 *
 * @return drawing area index or -1
 */
static gint getDrawingAreaFromCoordView(gint coord_view)
{
    gint drawing_area = -1;

    switch (coord_view)
    {
        case SBAND1_VIEW:
            drawing_area = SBand1DrawingArea;
            break;

        case SBAND2_VIEW:
            drawing_area = SBand2DrawingArea;
            break;

        case KUBAND_VIEW:
            drawing_area = KuBandDrawingArea;
            break;

        case STATION_VIEW:
            drawing_area = StationDrawingArea;
            break;

        case S1KU_SBAND_VIEW:
            drawing_area = S1KU_SBandDrawingArea;
            break;

        case S1KU_KUBAND_VIEW:
            drawing_area = S1KU_KuBandDrawingArea;
            break;

        case S2KU_SBAND_VIEW:
            drawing_area = S2KU_SBandDrawingArea;
            break;

        case S2KU_KUBAND_VIEW:
            drawing_area = S2KU_KuBandDrawingArea;
            break;

        default:
            break;
    }

    return drawing_area;
}

/**
 * This method draws the basic background that the main drawing area will use; for instance
 * the sband will have a black circle, the kuband/station will have a rectangle.
 *
 * @param windata window data
 * @param display current display
 * @param coord_view current coordinate view
 */
void PH_DrawBackground(WindowData *windata, gint display, gint coord_view)
{
    gint which_drawing_area = getDrawingAreaFromCoordView(coord_view);
    if (which_drawing_area != -1)
    {
        if (AM_AllocatePixmapData(windata, windata->bgpixmap_data, False) == True)
        {
            setParams(windata);
            switch (coord_view)
            {
                case SBAND1_VIEW:
                case SBAND2_VIEW:
                case S1KU_SBAND_VIEW:
                case S2KU_SBAND_VIEW:
                    createSBand(windata, windata->bgpixmap_data->pixmap);
                    break;

                case KUBAND_VIEW:
                case S1KU_KUBAND_VIEW:
                case S2KU_KUBAND_VIEW:
                    createKuBand(windata, KuBandDrawingArea, windata->bgpixmap_data->pixmap);
                    break;

                case STATION_VIEW:
                    createStation(windata, StationDrawingArea, windata->bgpixmap_data->pixmap);
                    break;

                default:
                    break;
            }
            resetParams();
        }

        if (windata->bgpixmap_data->pixmap != NULL)
        {
            AM_CopyPixmap(windata, windata->bgpixmap_data->pixmap);
        }
    }
}

/**
 * Front-end to the kuband and station foreground drawing routines for
 * the left and bottom label areas. Draw directly into the window data
 * pixmap drawable.
 *
 * @param windata window data
 * @param display current display
 * @param which_drawing_area which drawable to use
 */
void PH_DrawLeftBottomPixmap(WindowData *windata, gint display, gint which_drawing_area)
{
    GdkDrawable *drawable = AM_Drawable(windata,windata->which_drawable);

    if (windata->redraw_pixmap == True)
    {
        setParams(windata);
        switch (which_drawing_area)
        {
            case KuBandLeftDrawingArea:
            case KuBandBottomDrawingArea:
            case S1KU_KuBandLeftDrawingArea:
            case S1KU_KuBandBottomDrawingArea:
            case S2KU_KuBandLeftDrawingArea:
            case S2KU_KuBandBottomDrawingArea:
                createKuBand(windata, which_drawing_area, drawable);
                break;

            case StationLeftDrawingArea:
            case StationBottomDrawingArea:
                createStation(windata, which_drawing_area, drawable);
                break;

            default:
                break;
        }
        resetParams();
    }

    D_DrawDrawable(windata);
}

/**
 * Builds the SBand background view.
 *
 * @param windata window data
 * @param drawable draw to this drawable
 */
static void createSBand(WindowData *windata, GdkDrawable *drawable)
{
    gint diameter = MIN(thisWidth, thisHeight) - (2 * POLAR_BORDER);
    gint segment = diameter / NUM_GRID_SEGMENTS;

    gint i;

    /* draw circles */
    for (i=0; i<NUM_GRID_SEGMENTS; i++)
    {
        drawSBandCircle(drawable,
                        (i==0 ? GDK_LINE_SOLID : GDK_LINE_ON_OFF_DASH),
                        thisWidth, thisHeight, (gint)(diameter - segment * i));
    }

    /* draw lines at 15 degree increments */
    for (i=0; i<180; i+=15)
    {
        drawSBandLine(drawable, thisWidth, thisHeight, i, False);
    }

    /* draw lines at gimbal stops */
    drawSBandLine(drawable, thisWidth, thisHeight, 235, True);
    drawSBandLine(drawable, thisWidth, thisHeight, -235, True);

    /* draw labels */
    drawSBandLabels(drawable, thisWidth, thisHeight);
}

/**
 * Draws the circles that make the sband view
 *
 * @param drawable draw to this drawable
 * @param line_style line style to use
 * @param w width of circle
 * @param h height of circle
 * @param diameter diameter of circle
 */
static void drawSBandCircle(GdkDrawable *drawable, GdkLineStyle line_style, gint w, gint h, gint diameter)
{
    GdkGC *gc;
    GdkColor *color;
    GdkColor gcolor;
    GdkGCValues _values;

    gint radius = diameter / 2;
    gint x = w / 2 - radius;
    gint y = h / 2 - radius;

    _values.line_width = 1;
    _values.line_style = line_style;
    _values.join_style = GDK_JOIN_ROUND;
    _values.cap_style = GDK_CAP_ROUND;
    if (line_style == GDK_LINE_SOLID)
    {
        color = CH_GetGdkColorByIndex(MAJOR_COLOR);
    }
    else
    {
        color = CH_GetGdkColorByIndex(MINOR_COLOR);
    }

    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;

    /* make the gc use line style and color */
    gc = gdk_gc_new_with_values(drawable, &_values,
                                (GdkGCValuesMask)
                                (GDK_GC_LINE_WIDTH|GDK_GC_LINE_STYLE|
                                 GDK_GC_CAP_STYLE|GDK_GC_JOIN_STYLE|
                                 GDK_GC_FOREGROUND));

    gdk_draw_arc(drawable, gc, False, x, y, diameter, diameter, 0, 360*64);

    g_object_unref(gc);
}

/**
 * Draws the lines that make the sband view
 *
 * @param drawable draw to this drawable
 * @param w width of line
 * @param h height of line
 * @param angle angle of line
 * @param draw_radius flag indicating if radius line should also be drawn
 */
static void drawSBandLine(GdkDrawable *drawable, gint w, gint h, gint angle, gboolean draw_radius)
{
    GdkGC *gc;
    GdkGCValues _values;
    GdkColor *color;
    GdkColor gcolor;
    GdkLineStyle line_style;
    gint fg;
    gint x1, y1;
    gint x2, y2;
    gint radius;
    gint x_orig, y_orig;
    gdouble x, y;
    gdouble radians;

    /* Determine line style and color based on angle */
    if (abs(angle) == 235)
    {
        /* gimbal stop */
        line_style = GDK_LINE_SOLID;
        fg = MINOR_COLOR;
    }
    else if (angle%45 == 0)
    {
        /* multiple of 45 degrees */
        line_style = GDK_LINE_SOLID;
        fg = MAJOR_COLOR;
    }
    else
    {
        /* all others */
        line_style = GDK_LINE_ON_OFF_DASH;
        fg = MINOR_COLOR;
    }

    /* make the gc use line style and color */
    _values.line_width = 1;
    _values.line_style = line_style;
    _values.join_style = GDK_JOIN_ROUND;
    _values.cap_style = GDK_CAP_ROUND;
    color = CH_GetGdkColorByIndex(fg);
    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;
    gc = gdk_gc_new_with_values(drawable, &_values,
                                (GdkGCValuesMask)
                                (GDK_GC_LINE_WIDTH|GDK_GC_LINE_STYLE|
                                 GDK_GC_CAP_STYLE|GDK_GC_JOIN_STYLE|
                                 GDK_GC_FOREGROUND));
    /* compute grid radius */
    x_orig = w/2;
    y_orig = h/2;
    radius = ((gint)MIN(w, h) / 2) - POLAR_BORDER;

    if (abs(angle) == 235)
    {
        /* gimbal stops extend past the outer circle */
        radius += POLAR_BORDER/4;
    }

    else if (line_style == GDK_LINE_SOLID)
    {
        /* solid lines extend past the outer circle */
        radius += POLAR_BORDER/2;
    }

    /* Convert angle/elevation to x/y coordinates */
    radians  = DEGREES_TO_RADIANS( (gdouble)angle );
    x        = radius * sin(radians);
    y        = radius * cos(radians);
    x1       = (gint)(x_orig + x);
    y1       = (gint)(y_orig - y);

    /* Convert opposite angle/elevation to x/y coordinates */
    if (draw_radius)
    {
        x2 = x_orig;
        y2 = y_orig;
    }
    else
    {
        radians  = DEGREES_TO_RADIANS( (gdouble)angle - 180.0 );
        x        = radius * sin(radians);
        y        = radius * cos(radians);
        x2       = (gint)(x_orig + x);
        y2       = (gint)(y_orig - y);
    }

    /* draw the line */
    gdk_draw_line(drawable, gc, x1, y1, x2, y2);
    g_object_unref(gc);
}

/**
 * Draws the labels for the sband view
 *
 * @param drawable draw to this drawable
 * @param w width of label
 * @param h height of label
 */
static void drawSBandLabels(GdkDrawable *drawable, gint w, gint h)
{
    gint i;
    gint radius;
    gint x, y;
    gint x45, x55, y35;
    gint x_orig, y_orig;
    gint elev;
    gint segment, seg_elevation;
    gchar label[20];
    PangoLayout *layout;
    GdkColor *color;
    GdkColor gcolor;
    GdkGC *gc;
    GdkGCValues _values;
    /* foreground color for text */
    color = CH_GetGdkColorByIndex(MINOR_TEXT_COLOR);
    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;
    gc = gdk_gc_new_with_values(drawable, &_values, GDK_GC_FOREGROUND);

    gint zero_width;
    gint pos_45_height;
    gint pos_135_height;
    gint neg_45_height;
    gint neg_135_height;

    /* get radius and point of origin in window coordinates */
    radius = (gint)MIN( w, h ) / 2 - POLAR_BORDER;
    x_orig = w/2;
    y_orig = h/2;
    x45 = (gint)((radius + POLAR_BORDER/2) * sin(DEGREES_TO_RADIANS(45)));
    x55 = (gint)((radius + POLAR_BORDER/4) * sin(DEGREES_TO_RADIANS(55)));
    y35 = (gint)((radius + POLAR_BORDER/4) * sin(DEGREES_TO_RADIANS(35)));

    /* get pango layout for drawing text */
    layout = gtk_widget_create_pango_layout(thisWidget, "");

    /* draw various labels */
    pango_layout_set_text (layout, LABEL_0, -1);
    x = x_orig - (FH_GetFontWidth(layout)/2);
    y = y_orig - (radius + POLAR_BORDER/2 + FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    zero_width = FH_GetFontWidth(layout);

    pango_layout_set_text (layout, LABEL_AZ, -1);
    x = x_orig + (FH_GetFontWidth(layout)/2 + zero_width);
    y = y_orig - (radius + POLAR_BORDER/2 + FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_45, -1);
    x = x_orig + (x45 - FH_GetFontWidth(layout)/2);
    y = y_orig - (x45 + FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    pos_45_height = FH_GetFontHeight(layout);

    pango_layout_set_text (layout, LABEL_FWD_X, -1);
    x = x_orig + (x45 - FH_GetFontWidth(layout)/2);
    y = y_orig - (x45 + FH_GetFontHeight(layout) + pos_45_height);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_90, -1);
    x = x_orig + (radius + POLAR_BORDER/2);
    y = y_orig - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_135, -1);
    x = x_orig + (x45 - FH_GetFontWidth(layout)/2);
    y = y_orig + (x45);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pos_135_height = FH_GetFontHeight(layout);

    pango_layout_set_text(layout, LABEL_STBD_Y, -1);
    x = x_orig + (x45 - FH_GetFontWidth(layout)/2);
    y = y_orig + (x45 + pos_135_height);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_180, -1);
    x = x_orig - (FH_GetFontWidth(layout)/2);
    y = y_orig + (radius + POLAR_BORDER/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_NEG_135, -1);
    x = x_orig - (x45 + FH_GetFontWidth(layout)/2);
    y = y_orig + (x45);
    gdk_draw_layout(drawable, gc, x, y, layout);

    neg_135_height = FH_GetFontHeight(layout);

    pango_layout_set_text(layout, LABEL_AFT_X, -1);
    x = x_orig - (x45 + FH_GetFontWidth(layout)/2);
    y = y_orig + (x45 + neg_135_height);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_NEG_90, -1);
    x = x_orig - (radius + POLAR_BORDER/2 + FH_GetFontWidth(layout));
    y = y_orig - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_NEG_45, -1);
    x = x_orig - (x45 + FH_GetFontWidth(layout)/2);
    y = y_orig - (x45 + FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    neg_45_height = FH_GetFontHeight(layout);

    pango_layout_set_text(layout, LABEL_PORT_Y, -1);
    x = x_orig - (x45 + FH_GetFontWidth(layout)/2);
    y = y_orig - (x45 + FH_GetFontHeight(layout) + neg_45_height);
    gdk_draw_layout(drawable, gc, x, y, layout);

    /* set gc with different color */
    gdk_gc_set_foreground(gc, CH_GetGdkColorByIndex(MAJOR_COLOR));

    /* draw gimbal s"tops labels */
    pango_layout_set_text(layout, LABEL_235, -1);
    x = x_orig - (x55 + FH_GetFontWidth(layout)/2);
    y = y_orig + (y35);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text(layout, LABEL_NEG_235, -1);
    x = x_orig + (x55 - FH_GetFontWidth(layout)/2);
    y = y_orig + (y35);
    gdk_draw_layout(drawable, gc, x, y, layout);

    /* draw elevation labels */
    segment = radius / NUM_GRID_SEGMENTS;
    seg_elevation = MAX_POLAR_RADIUS/NUM_GRID_SEGMENTS;

    pango_layout_set_text(layout, LABEL_ELEV, -1);
    x = x_orig + radius + 5;
    y = (gint)(y_orig - 2 * FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    /* do the elevation values */
    for (i=1; i<=NUM_GRID_SEGMENTS; i++)
    {
        elev = seg_elevation * i;
        g_sprintf(label, "%d", elev);
        pango_layout_set_text(layout, label, -1);

        if (i == NUM_GRID_SEGMENTS)
        {
            x = x_orig + radius - FH_GetFontWidth(layout)/3;
        }
        else
        {
            x = x_orig + segment*i - FH_GetFontWidth(layout)/3;
        }

        y = y_orig - FH_GetFontHeight(layout);

        gdk_draw_layout(drawable, gc, x, y, layout);
    }

    g_object_unref(gc);
    g_object_unref(layout);
}

/**
 * Builds the kuband background view
 *
 * @param windata window data
 * @param which_drawing_area which draw method to use
 * @param drawable the drawable to use
 */
static void createKuBand(WindowData *windata, gint which_drawing_area, GdkDrawable *drawable)
{
    switch (which_drawing_area)
    {
        default:
        case KuBandDrawingArea:
        case S1KU_KuBandDrawingArea:
        case S2KU_KuBandDrawingArea:
            drawKuBand(drawable);
            break;

        case KuBandLeftDrawingArea:
        case S1KU_KuBandLeftDrawingArea:
        case S2KU_KuBandLeftDrawingArea:
            drawKuBandLeft(drawable);
            break;

        case KuBandBottomDrawingArea:
        case S1KU_KuBandBottomDrawingArea:
        case S2KU_KuBandBottomDrawingArea:
            drawKuBandBottom(drawable);
            break;
    }
}

/**
 * Draws the main view for the kuband
 *
 * @param drawable
 */
static void drawKuBand(GdkDrawable *drawable)
{
    gint w = thisWidth;
    gint h = thisHeight;

    GdkColor *color;
    GdkColor gcolor;
    GdkGC *gc;
    GdkGCValues _values;
    _values.line_width = 1;
    _values.line_style = GDK_LINE_SOLID;
    _values.join_style = GDK_JOIN_ROUND;
    _values.cap_style = GDK_CAP_ROUND;
    color = CH_GetGdkColorByIndex(MAJOR_COLOR);
    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;
    gc = gdk_gc_new_with_values(drawable, &_values,
                                (GdkGCValuesMask)
                                (GDK_GC_LINE_WIDTH|GDK_GC_LINE_STYLE|
                                 GDK_GC_CAP_STYLE|GDK_GC_JOIN_STYLE|
                                 GDK_GC_FOREGROUND));

    /* draw the cross-hairs */
    /* first line is the horizontal line */
    gdk_draw_line(drawable, gc, 0, h/2, w, h/2);

    /* this line is the vertical line */
    gdk_draw_line(drawable, gc, w/2, 0, w/2, h);

    g_object_unref(gc);

    _values.line_width = 1;
    _values.line_style = GDK_LINE_ON_OFF_DASH;
    _values.join_style = GDK_JOIN_ROUND;
    _values.cap_style = GDK_CAP_ROUND;
    color = CH_GetGdkColorByIndex(MINOR_COLOR);
    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;
    gc = gdk_gc_new_with_values(drawable, &_values,
                                (GdkGCValuesMask)
                                (GDK_GC_LINE_WIDTH|GDK_GC_LINE_STYLE|
                                 GDK_GC_CAP_STYLE|GDK_GC_JOIN_STYLE|
                                 GDK_GC_FOREGROUND));

    /* the vertical dash lines */
    gdk_draw_line(drawable, gc, w/6, 0, w/6, h);
    gdk_draw_line(drawable, gc, w/3, 0, w/3, h);
    gdk_draw_line(drawable, gc, ((gint)w * 2) / 3, 0, ((gint)w * 2) / 3, h);
    gdk_draw_line(drawable, gc, ((gint)w * 5) / 6, 0, ((gint)w * 5) / 6, h);

    /* the horizontal dash lines */
    gdk_draw_line(drawable, gc, 0, h/6, w, h/6 );
    gdk_draw_line(drawable, gc, 0, h/3, w, h/3 );
    gdk_draw_line(drawable, gc, 0, ((gint)h * 2) / 3, w, ((gint)h * 2) / 3 );
    gdk_draw_line(drawable, gc, 0, ((gint)h * 5) / 6, w, ((gint)h * 5) / 6 );
    g_object_unref(gc);
}

/**
 * Draws the text labels on the left side of the kuband main viewing area
 *
 * @param drawable draw into this
 */
static void drawKuBandLeft(GdkDrawable *drawable)
{
    gint x = 0;
    gint y = 0;
    gint w = thisWidth;
    gint h = thisHeight;

    GdkGC *gc = thisWidget->style->black_gc;

    /* get pango layout for drawing text */
    PangoLayout *layout = gtk_widget_create_pango_layout(thisWidget, "");

    pango_layout_set_text (layout, LABEL_NEG_135, -1);
    x = w - FH_GetFontWidth(layout);
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_90, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/6) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_45, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/3) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_0, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/2) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_45, -1);
    x = w - FH_GetFontWidth(layout);
    y = ((gint)h * 2) / 3 - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_90, -1);
    x = w - FH_GetFontWidth(layout);
    y = ((gint)h * 5) / 6 - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_135, -1);
    x = w - FH_GetFontWidth(layout);
    y = h - FH_GetFontHeight(layout);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_FWD, -1);
    x = w - FH_GetFontWidth(layout) - 5;
    y = FH_GetFontHeight(layout);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_ELEV, -1);
    x = w - FH_GetFontWidth(layout) - 10;
    y = (h/2) - (FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_AFT, -1);
    x = w - FH_GetFontWidth(layout) - 5;
    y = h - (FH_GetFontHeight(layout)*2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    g_object_unref(layout);
}

/**
 * Draws the text labels on the bottom of the kuband main viewing area
 *
 * @param drawable draw into this
 */
static void drawKuBandBottom(GdkDrawable *drawable)
{
    gint x = 0;
    gint y = 0;
    gint w = thisWidth;

    GdkGC *gc = thisWidget->style->black_gc;

    /* get pango layout for drawing text */
    PangoLayout *layout = gtk_widget_create_pango_layout(thisWidget, "");

    pango_layout_set_text (layout, LABEL_75, -1);
    x = 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_50, -1);
    x = w/6;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_25, -1);
    x = w/3;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_0, -1);
    x = w/2;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_25, -1);
    x = ((gint)w * 2) / 3 - 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_50, -1);
    x = ((gint)w * 5) / 6 - 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_75, -1);
    x = w - FH_GetFontWidth(layout) - 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_PORT_Y, -1);
    x = 20;
    y = 20;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_CROSS_ELEV, -1);
    x = w/2 - (FH_GetFontWidth(layout)/2);
    y = 20;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_STBD_Y, -1);
    x = w - FH_GetFontWidth(layout) - 15;
    y = 20;
    gdk_draw_layout(drawable, gc, x, y, layout);

    g_object_unref(layout);
}

/**
 * Builds the stations background view
 *
 * @param windata window data
 * @param which_drawing_area which draw method to use
 * @param drawable draw into this
 */
static void createStation(WindowData *windata, gint which_drawing_area, GdkDrawable *drawable)
{
    switch (which_drawing_area)
    {
        default:
        case StationDrawingArea:
            drawStation(drawable);
            break;

        case StationLeftDrawingArea:
            drawStationLeft(drawable);
            break;

        case StationBottomDrawingArea:
            drawStationBottom(drawable);
            break;
    }
}

/**
 * Draws the main view for the station
 *
 * @param drawable draw to this
 */
static void drawStation(GdkDrawable *drawable)
{
    gint w = thisWidth;
    gint h = thisHeight;
    GdkColor *color;
    GdkColor gcolor;
    GdkGC *gc;
    GdkGCValues _values;
    color = CH_GetGdkColorByIndex(MAJOR_COLOR);
    gcolor.red = color->red;
    gcolor.green = color->green;
    gcolor.blue = color->blue;
    gcolor.pixel = color->pixel;
    _values.foreground = gcolor;
    gc = gdk_gc_new_with_values(drawable, &_values, GDK_GC_FOREGROUND);

    gdk_draw_line(drawable, gc, 0, h/2, w, h/2);
    gdk_draw_line(drawable, gc, w/2, 0, w/2, h );
    gdk_draw_line(drawable, gc, w/4, 0, w/4, h );
    gdk_draw_line(drawable, gc, ((gint)w * 3) / 4, 0, ((gint)w * 3) / 4, h );

    g_object_unref(gc);
}

/**
 * Draws the text labels on the left side of the station main viewing area
 *
 * @param drawable draw to this
 */
static void drawStationLeft(GdkDrawable *drawable)
{
    gint x = 0;
    gint y = 0;
    gint w = thisWidth;
    gint h = thisHeight;

    GdkGC *gc = thisWidget->style->black_gc;

    /* get pango layout for drawing text */
    PangoLayout *layout = gtk_widget_create_pango_layout(thisWidget, "");

    pango_layout_set_text (layout, LABEL_90, -1);
    x = w - FH_GetFontWidth(layout);
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_60, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/6) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_30, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/3) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_0, -1);
    x = w - FH_GetFontWidth(layout);
    y = (h/2) - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_30, -1);
    x = w - FH_GetFontWidth(layout);
    y = ((gint)h * 2) / 3 - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_60, -1);
    x = w - FH_GetFontWidth(layout);
    y = ((gint)h * 5) / 6 - (FH_GetFontHeight(layout)/2);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_90, -1);
    x = w - FH_GetFontWidth(layout);
    y = h - FH_GetFontHeight(layout);
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_ELEV, -1);
    x = w - FH_GetFontWidth(layout) - 10;
    y = (h/2) - (FH_GetFontHeight(layout));
    gdk_draw_layout(drawable, gc, x, y, layout);

    g_object_unref(layout);
}

/**
 * Draws the text labels on the bottom of the station main viewing area
 *
 * @param drawable draw to this
 */
static void drawStationBottom(GdkDrawable *drawable)
{
    gint x = 0;
    gint y = 0;
    gint w = thisWidth;

    GdkGC *gc = thisWidget->style->black_gc;

    /* get pango layout for drawing text */
    PangoLayout *layout = gtk_widget_create_pango_layout(thisWidget, "");

    pango_layout_set_text (layout, LABEL_180, -1);
    x = 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_90, -1);
    x = w/4;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_0, -1);
    x = w/2;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_90, -1);
    x = ((gint)w * 3) / 4 - 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_NEG_180, -1);
    x = w - FH_GetFontWidth(layout) - 5;
    y = 0;
    gdk_draw_layout(drawable, gc, x, y, layout);

    pango_layout_set_text (layout, LABEL_AZ, -1);
    x = w/2 - (FH_GetFontWidth(layout)/2) + 2;
    y = 20;
    gdk_draw_layout(drawable, gc, x, y, layout);

    g_object_unref(layout);
}
