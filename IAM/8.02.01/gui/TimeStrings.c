/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "TimeStrings.h"
#undef PRIVATE

#include "AntMan.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/TimeStrings.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define LEAP_YEAR(y)  ((((y)%400)==0) || ((((y)%100)!=0) && (((y)%4)==0)))
#define IS_LEAP_YEAR(Y) (((Y)>0) && !((Y)%4) && (((Y)%100) || !((Y)%400)))

static gchar        thisPET_UTC[TIME_STR_LEN]   = UNSET_TIME; /* base PET time in UTC   */
static gchar        thisCurrentUTC[TIME_STR_LEN]= UNSET_TIME; /* current time from ISP  */
static glong        thisCurrentUTCSecs          = -1;         /* current time in seconds */
static gchar        thisCurrentGMT[TIME_STR_LEN]= UNSET_TIME;
static glong        thisCurrentGMTSecs          = -1;         /* current time in seconds */

static gint         thisDayInMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static gchar        *thisTimeTypeName[3] = { "UTC", "GMT", "PET"};
/*************************************************************************/

/**
 * Converts total secs to gmt time string
 *
 * @param secs total seconds to convert
 */
void SetGmt(gint secs)
{
    SecsToGmt(secs, thisCurrentGMT);
}

/**
 * This function returns name of the specified time type.
 *
 * @param timeType one of TS_UTC, TS_GMT, or TS_PET
 *
 * @return name of time type
 */
gchar *GetTimeName(gint timeType)
{
    gchar *timeName = "???";

    switch (timeType)
    {
        case TS_UTC:
        case TS_GMT:
        case TS_PET:
            timeName = thisTimeTypeName[timeType];

        default:
            break;
    }

    return timeName;
}

/**
 * This function returns the current ISP time in UTC format.
 *
 * @return current ISP time in UTC format
 */
gchar *CurrentUtc(void)
{
    return(thisCurrentUTC);
}


/**
 * This function returns the current ISP time in total seconds.
 *
 * @return current time in total seconds
 */
glong CurrentUtcSecs()
{
    return(thisCurrentUTCSecs);
}

/**
 * Returns the current ISP time in GMT format.
 *
 * @return current ISP time in GMT format
 */
gchar *CurrentGmt(void)
{
    return thisCurrentGMT;
}

/**
 * This function returns the base PET time, which is the UTC time equivalent to
 * 000/00:00:00 in PET reference.
 *
 * @return  base PET time
 */
gchar *GetBasePet()
{
    return(thisPET_UTC);
}

/**
 * This functin sets the current time in GMT.
 *
 * @param time_str
 */
void SetCurrentGmt(gchar *time_str)
{
    g_stpcpy(thisCurrentGMT, time_str);
    thisCurrentGMTSecs = StrToSecs((gint)TS_GMT, thisCurrentGMT);
}

/**
 * This function sets the current time in UTC.
 *
 * @param time_str convert this string
 */
void SetCurrentUtc(gchar *time_str)
{
    g_stpcpy(thisCurrentUTC, time_str);
    thisCurrentUTCSecs = StrToSecs(TS_UTC, thisCurrentUTC);
}

/**
 * This function sets the base PET time in UTC.  This is used later to convert
 * between UTC and PET times.
 *
 * @param time_str convert this string
 */
void SetBasePet(gchar *time_str)
{
    g_stpcpy(thisPET_UTC, time_str);
}


/**
 * This function converts the specified time from UTC format to GMT format.
 * * @param time_str convert this string
 */
void UtcToGmt(gchar *time_str)
{
    glong s1;

    s1 = StrToSecs(TS_UTC, time_str);
    SecsToStr(TS_GMT, s1, time_str);
}


/**
 * This function converts the specified time from GMT format to UTC format.
 *
 * @param time_str convert this string
 */
void GmtToUtc(gchar *time_str)
{
    glong s1;

    s1 = StrToSecs(TS_GMT, time_str);
    SecsToStr(TS_UTC, s1, time_str);
}


/**
 * This function updates time (presumed to be in UTC format) to reflect the
 * same time in PET reference.
 *
 * @param time_str convert this string
 */
void UtcToPet(gchar *time_str)
{
    glong s1, s2;

    s1 = StrToSecs(TS_UTC, time_str);
    s2 = StrToSecs(TS_UTC, thisPET_UTC);
    SecsToStr(TS_PET, s1 - s2, time_str);
}


/**
 * This function converts time from PET reference to UTC format
 *
 * @param time_str convert this string
 */
void PetToUtc(gchar *time_str)
{
    glong s1, s2;

    s1 = StrToSecs(TS_PET, time_str);
    s2 = StrToSecs(TS_UTC, thisPET_UTC);
    SecsToStr(TS_UTC, s1 + s2, time_str);
}


/**
 * This function converts a time string to the appropriate total seconds.
 * For time types UTC and GMT, total seconds is since epoch
 * (defined as 01/01/1992).  All other times are in total elapsed time.
 *
 * @param time_type TS_UTC, TS_GMT_, TS_PET, or TS_ELAPSED
 * @param time_str convert this string
 *
 * @return time in seconds
 */
glong StrToSecs(gint time_type, gchar *time_str)
{
    gint y = 0, mm = 0, d = 0, h = 0, m = 0, s = 0;
    gint i;
    glong days = 0;

    switch (time_type)
    {
        case TS_UTC:
            sscanf(time_str, "%d:%d:%d:%d:%d:%d", &y, &mm, &d, &h, &m, &s);

            days = d;
            for (i=1992; i<y; i++)
            {
                days += (IS_LEAP_YEAR(i) ? 366 : 365);
            }

            for (i=1; i<mm; i++)
            {
                days += thisDayInMonth[i-1];
                if ((i == 2) && IS_LEAP_YEAR(y))  days++;
            }
            break;

        case TS_PET:
        case TS_ELAPSED:
            sscanf(time_str, "%d:%d:%d:%d:%d", &y, &d, &h, &m, &s);
            days = y * 365 + d;
            break;

        case TS_GMT:
        default:
            sscanf(time_str, "%d %d/%d:%d:%d", &y, &d, &h, &m, &s);

            days = d;
            for (i=1992; i<y; i++)
            {
                days += (IS_LEAP_YEAR(i) ? 366 : 365);
            }
            break;
    }

    return ((days * 24 + h) * 60 + m) * 60 + s;
}


/**
 * This function converts total seconds to the appropriate time string.
 * For time type UTC, total seconds is since epoch (defined as 01/01/1992)
 * and is converted to UTC format.  All other times are in total elapsed time
 * and are converted to "yyyy:ddd:hh:mm:ss".
 *
 * @param time_type TS_UTC, TS_GMT_, TS_PET, or TS_ELAPSED
 * @param total_secs total seconds to convert
 * @param time_str output conversion
 */
void SecsToStr(gint time_type, glong total_secs, gchar *time_str)
{
    gint y, mm, d, h, m, s;
    gint dd;
    glong total_days;

    /* validate the total_secs */
    if (total_secs < 0)
    {
        total_secs = 0;
    }

    /*-----------------------------------------------------------*/
    /* Compute hours, minutes, seconds, and total number of days */
    /*-----------------------------------------------------------*/
    s = (gint)(total_secs % 60);
    total_secs = total_secs / 60;
    m = (gint)(total_secs % 60);
    total_secs = total_secs / 60;
    h = (gint)(total_secs % 24);
    total_days = total_secs / 24;

    switch (time_type)
    {
        case TS_UTC:
            /*-------------------------------*/
            /* Determine the year from epoch */
            /*-------------------------------*/
            y = 1992;   dd = 366;
            while (total_days > dd)
            {
                total_days -= dd;
                y++;
                dd = (IS_LEAP_YEAR(y)) ? 366 : 365;
            }

            /*----------------------------------------------*/
            /* Determine the month and day for current year */
            /*----------------------------------------------*/
            mm = 1;   dd = thisDayInMonth[0];
            while (total_days > dd)
            {
                total_days -= dd;
                mm++;
                dd = thisDayInMonth[mm-1];
                if ((mm == 2) && IS_LEAP_YEAR(y))  dd++;
            }
            d = (gint)total_days;

            g_sprintf(time_str, "%04d:%02d:%02d:%02d:%02d:%02d",
                    y, mm, d, h, m, s);
            break;

        case TS_PET:
        case TS_ELAPSED:
            {
                /*--------------------------------*/
                /* Build elapsed time time format */
                /*--------------------------------*/
                d = (gint)(total_days % 365);
                y = (gint)(total_days / 365);

                g_sprintf(time_str, "%04d:%03d:%02d:%02d:%02d", y, d, h, m, s);
            }
            break;

        case TS_GMT:
        default:
            /*-------------------------------*/
            /* Determine the year from epoch */
            /*-------------------------------*/
            y = 1992;   dd = 366;
            while (total_days > dd)
            {
                total_days -= dd;
                y++;
                dd = (IS_LEAP_YEAR(y)) ? 366 : 365;
            }
            d = (gint)total_days;

            g_sprintf(time_str, "%04d %03d/%02d:%02d:%02d", y, d, h, m, s);
            break;
    }
}


/**
 * This function modifies an elapsed time string so that it has leading blanks
 * instead of leading zero-filled sections,
 * e.g., "0000:000:01:30:00" becomes "01:30:00".
 *
 * @param time_str string to strip
 */
void StripTimeBlanks(gchar *time_str)
{
    if (ValidTime(TS_ELAPSED, time_str))
    {
        gint  y      = 0;
        gint  d      = 0;
        gint  h      = 0;
        gint  m      = 0;
        gint  s      = 0;
        gchar sy[6]  = "";
        gchar sd[5]  = "";
        gchar sh[4]  = "   ";

        sscanf(time_str, "%d:%d:%d:%d:%d", &y, &d, &h, &m, &s);

        if (y != 0)
            g_sprintf(sy, "%04d:", y);

        if ((d != 0) || (y != 0))
            g_sprintf(sd, "%03d:", d);

        if ((h != 0) || (d != 0) || (y != 0))
            g_sprintf(sh, "%02d:", h);

        g_sprintf(time_str, "%s%s%s%02d:%02d", sy, sd, sh, m, s);
    }
}


/**
 * This function determines if the given string can be parsed as a valid time.
 * It will accept a partial string (days only, days & hours only, or days &
 * hours & minutes only), and will accept a '/' or a ':' for each delimiter.
 * If the string can be parsed, then a 1 is returned and the string is modified
 * to be a full time string.
 *
 * @param time_type TS_UTC, TS_GMT_, TS_PET, or TS_ELAPSED
 * @param time_str output result
 *
 * @return 1 if valid, 0 if not valid
 */
gint ValidTime(gint time_type, gchar *time_str)
{
    gint year   = 0;
    gint month  = 0;
    gint day    = 0;
    gint hour   = 0;
    gint min    = 0;
    gint sec    = 0;
    gint cv;
    gint valid = False;
    gchar div1[2], div2[2], div3[2], div4[2], div5[2];

    switch (time_type)
    {
        case TS_UTC:
            cv = sscanf(time_str, "%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d",
                        &year, div1, &month, div2, &day, div3, &hour, div4,
                        &min, div5, &sec);

            if (cv == 11)
            {
                if ((year  >= 0)
                    && (month >= 1) && (month <= 12)
                    && (day   >= 1) && (day   <= 31)
                    && (hour  >= 0) && (hour  <  24)
                    && (min   >= 0) && (min   <  60)
                    && (sec   >= 0) && (sec   <  60))
                {
                    valid = True;
                }
            }
            break;

        case TS_PET:
        case TS_ELAPSED:
            cv = sscanf(time_str, "%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d",
                        &year, div1, &day, div2, &hour, div3, &min, div4, &sec);

            if ((cv <= 9) && (cv % 2 == 1))
            {
                if ((year  >= 0)
                    && (day   >= 0) && (day   <= 365)
                    && (hour  >= 0) && (hour  <  24)
                    && (min   >= 0) && (min   <  60)
                    && (sec   >= 0) && (sec   <  60))
                {
                    g_sprintf(time_str, "%04d:%03d:%02d:%02d:%02d",
                            year, day, hour, min, sec);
                    valid = True;
                }
            }
            break;

        case TS_GMT:
        default:
            cv = sscanf(time_str, "%d%1[ ]%d%1[/:]%d%1[:]%d%1[:]%d",
                        &year, div1, &day, div2, &hour, div3, &min, div4, &sec);

            if (cv == 9)
            {
                if ((year  >= 0)
                    && (day   >= 1) && (day   <= 366)
                    && (hour  >= 0) && (hour  <  24)
                    && (min   >= 0) && (min   <  60)
                    && (sec   >= 0) && (sec   <  60))
                {
                    g_sprintf(time_str, "%04d %03d/%02d:%02d:%02d",
                            year, day, hour, min, sec);
                    valid = True;
                }
            }
            break;
    }

    return valid;
}


/**
 * This function determines if the current time has been set, and returns True
 * if it has been set.
 *
 * @return 1 if valid, 0 if not valid
 */
gint ValidCurrentUtc(void)
{
    return(g_ascii_strcasecmp(thisCurrentUTC, UNSET_TIME));
}


/**
 * This function determines if the PET base time has been set, and returns True
 * if it has been set.
 *
 * @return 1 if valid, 0 if not valid
 */
gint ValidBasePet(void)
{
    return(g_ascii_strcasecmp(thisPET_UTC, UNSET_TIME));
}

/**
 * This function accepts a string assumed to be in the format "+00:00"
 * representing hours and minutes, and converts into a equivalent number of
 * seconds.
 *
 * @param offset input offset value
 *
 * @return offset in seconds
 */
gint OffsetToSecs(gchar *offset)
{
    gint h = 0;
    gint m = 0;

    sscanf(offset, "%d:%d", &h, &m);
    return(3600 * h + 60 * m);
}

/**
 * Converts total seconds to gmt format
 *
 * @param total_secs total seconds
 * @param time_str output string
 */
void SecsToGmt(gint total_secs, gchar *time_str)
{
    gint d, h, m, s;
    SecsToTime(total_secs, &d, &h, &m, &s);
    TimeToStr(time_str, d, h, m, s);
}

/**
 * Converts total seconds to individual discrete fields.
 *
 * @param total_secs total seconds
 * @param d day output
 * @param h hour output
 * @param m month output
 * @param s seconds output
 */
void SecsToTime(gint total_secs, gint *d, gint *h, gint *m, gint *s)
{
    if (total_secs < 0)
    {
        total_secs = 0;
    }

    *d = total_secs / (60 * 60 * 24);
    total_secs -= (60 * 60 * 24) * (*d);

    *h = total_secs / (60 * 60);
    total_secs -= 60 * 60 * (*h);

    *m = total_secs / 60;
    *s = total_secs - 60 * (*m);
}

/**
 * Converts day, hour, minute, and seconds to a time.
 *
 * @param time_str output formatted time string
 * @param day day
 * @param hour hour
 * @param min minutes
 * @param sec seconds
 */
void TimeToStr(gchar *time_str, gint day, gint hour, gint min, gint sec)
{
    while (sec < 0)
    {
        min--;
        sec += 60;
    }

    if (sec >= 60)
    {
        min += sec / 60;
        sec = sec % 60;
    }

    while (min < 0)
    {
        hour--;
        min += 60;
    }

    if (min >= 60)
    {
        hour += min / 60;
        min = min % 60;
    }

    while (hour < 0)
    {
        day--;
        hour += 24;
    }

    if (hour >= 24)
    {
        day += hour / 24;
        hour = hour % 24;
    }

    g_sprintf(time_str, "%03d/%02d:%02d:%02d", day, hour, min, sec);
}

/**
 * This function converts a time in seconds to an equivalent string in the
 * format "+hh:mm", rounded to the nearest second.
 *
 * @param secs seconds used for conversion
 * @param offset offset value used for compute
 */
void SecsToOffset(gint secs, gchar *offset)
{
    gint h = 0;
    gint m = 0;
    gint minusFlag = FALSE;

    if (secs < 0)
    {
        minusFlag = True;
        secs = 0 - secs;
    }

    h = secs / 3600;
    secs -= 3600 * h;
    m = secs / 60;
    if (minusFlag)
    {
        h = 0 - h;
    }

    g_sprintf(offset, "%+03d:%02d", h, m);
}

/**
 * This method determines what format the string is in. If its not already
 * in UTC format, the time string is then converted to the UTC format. The
 * input string must be a buffer that can hold the output results also.
 *
 * @param time_str the input/output time string buffer to convert
 */
void ConvertToUTC(gchar *time_str)
{
    if (IsGMT(time_str) == True)
    {
        GmtToUtc(time_str);
    }
    else if (IsPET(time_str) == True)
    {
        PetToUtc(time_str);
    }
    else
    {
        // do nothing already in utc format
    }
}

/**
 * This method checks the time string to see if it's a valid GMT time format.
 *
 * @param time_str the time string to verify
 *
 * @return True or False
 */
gboolean IsGMT(gchar *time_str)
{
    gint year   = 0;
    gint day    = 0;
    gint hour   = 0;
    gint min    = 0;
    gint sec    = 0;
    gchar div1[2], div2[2], div3[2], div4[2];

    /* must have slash in the format */
    if (strstr(time_str, "/") != NULL)
    {
        gint cv = sscanf(time_str, "%d%1[ ]%d%1[/:]%d%1[:]%d%1[:]%d",
                        &year, div1, &day, div2, &hour, div3, &min, div4, &sec);

        if (cv == 9)
        {
            if ((year  >= 0)
                && (day   >= 1) && (day   <= 366)
                && (hour  >= 0) && (hour  <  24)
                && (min   >= 0) && (min   <  60)
                && (sec   >= 0) && (sec   <  60))
            {
                return True;
            }
        }
    }

    return False;
}

/**
 * This method checks the time string to see if it's a valid UTC time format.
 *
 * @param time_str the time string to verify
 *
 * @return True or False
 */
gboolean IsUTC(gchar *time_str)
{
    gint year   = 0;
    gint month  = 0;
    gint day    = 0;
    gint hour   = 0;
    gint min    = 0;
    gint sec    = 0;
    gchar div1[2], div2[2], div3[2], div4[2], div5[2];

    /* must have 5 colon separators */
    gchar **tokens = g_strsplit(time_str, ":", -1);
    if (g_strv_length(tokens) == 6)
    {
        gint cv = sscanf(time_str, "%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d",
                        &year, div1, &month, div2, &day, div3, &hour, div4,
                        &min, div5, &sec);

        if (cv == 11)
        {
            if ((year  >= 0)
                && (month >= 1) && (month <= 12)
                && (day   >= 1) && (day   <= 31)
                && (hour  >= 0) && (hour  <  24)
                && (min   >= 0) && (min   <  60)
                && (sec   >= 0) && (sec   <  60))
            {
                return True;
            }
        }
    }
    g_strfreev(tokens);

    return False;
}

/**
 * This method checks the time string to see if it's a valid PET time format.
 *
 * @param time_str the time string to verify
 *
 * @return True or False
 */
gboolean IsPET(gchar *time_str)
{
    gint year   = 0;
    gint day    = 0;
    gint hour   = 0;
    gint min    = 0;
    gint sec    = 0;
    gchar div1[2], div2[2], div3[2], div4[2];

    /* must have 4 colon separators */
    gchar **tokens = g_strsplit(time_str, ":", -1);
    if (g_strv_length(tokens) == 5)
    {
        gint cv = sscanf(time_str, "%d%1[:]%d%1[:]%d%1[:]%d%1[:]%d",
                        &year, div1, &day, div2, &hour, div3, &min, div4, &sec);

        if ((cv <= 9) && (cv % 2 == 1))
        {
            if ((year  >= 0)
                && (day   >= 0) && (day   <= 365)
                && (hour  >= 0) && (hour  <  24)
                && (min   >= 0) && (min   <  60)
                && (sec   >= 0) && (sec   <  60))
            {
                return True;
            }
        }
    }
    g_strfreev(tokens);

    return False;
}
