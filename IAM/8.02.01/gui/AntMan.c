/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #ifndef __USE_BSD
       #define __USE_BSD
    #endif
    #include <unistd.h>
    #undef __USE_BSD
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif
    #include <winsock2.h> /* for gethostname */
    #include <ws2tcpip.h>
    #include <process.h> /* for _getpid */
#endif

#define PRIVATE
#include "AntMan.h"
#undef PRIVATE

#include "ColorHandler.h"
#include "ConfigParser.h"
#include "EventDialog.h"
#include "IspClient.h"
#include "IspHandler.h"
#include "IspSymbols.h"
#include "MaskFile.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PixmapHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "SolarDraw.h"
#include "TimeStrings.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/AntMan.c 197 2013-08-05 19:53:42Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/**
 * application mode, MANAGER or PUBLIC
 */
static gint      thisAppMode = PUBLIC;

/**
 * auto update flag
 */
static gboolean thisAutoUpdateMode = False;

/**
 * Coordinate type names matching the coordinate type enumeration
 * found in the header file.
 */
static gchar     *thisViewName[NUM_VIEWS] =
{
    "SBand String 1",
    "SBand String 2",
    "KuBand",
    "KuBand",
    "Generic Az/El",
    "SBand String 1/KuBand",
    "SBand String 2/KuBand"
};

/**
 * bitwise flag indicating which displays are active
 */
static gint      thisActiveDisplay   = 0;

/**
 * Coverage identifier
 */
static gchar     thisCoverageIdentifier[128] = {0};

/**
 * Structure identifier
 */
static gchar     thisStructureIdentifier[128] = {0};

/**
 * Time Type currently being used for viewing time related fields
 * Default is GMT
 */
static gint      thisTimeType        = TS_GMT;
/*************************************************************************/

/**
 * This routine is the handler for all signals.
 *
 * @param sig the signal. Everything except SIGTERM causes
 * abnormal termination.
 */
static void shutdownHandler(gint sig)
{
    switch (sig)
    {
        /*----------------------------------------------*/
        /* The following signals are normal termination */
        /*----------------------------------------------*/
        case SIGTERM:
            AM_Exit(0);
            break;

            /*------------------------------------------------------*/
            /* All other signals are not normal, generate core file */
            /*------------------------------------------------------*/
        default:
            AM_Exit(1);
    }
}

/**
 * This method keeps track of which displays are currently active
 *
 * @param display the display requesting change
 * @param state the display state
 */
void AM_SetActiveDisplay(gint display, gboolean state)
{
    thisActiveDisplay = display;
}

/**
 * This method clears the active display
 */
void AM_ClearActiveDisplay(void)
{
    thisActiveDisplay = 0;
}

/**
 * Makes a best guess at which display to return for the caller.
 * If both the realtime and predict are active, then what to do?
 * I guess the predict display is returned.
 *
 * @return predict, realtime display windows or NULL
 */
GtkWidget *AM_GetActiveDisplay(void)
{
    if (thisActiveDisplay == DISPLAY_PD)
    {
        return PD_GetDialog();
    }

    if (thisActiveDisplay == DISPLAY_RD)
    {
        return RTD_GetDialog();
    }

    return NULL;
}

/**
 * Returns the display id that is active
 *
 * @return gint DISPLAY_PD, DISPLAY_RD, DISPLAY_ED
 */
gint AM_GetActiveDisplayID(void)
{
    return thisActiveDisplay;
}

/**
 * This function returns a string containing the version of the application.
 * It is used to set the version in various dialog titles.  The version is
 * stored in a format to work with the "ident" function so that an ident
 * performed on the executable will identify the version of the application.
 * The ident version string is located in "Release.h".
 *
 * @return  string containing version
 */
gchar *AM_GetVersion(void)
{
    static gchar s[32];  /* returned version string */

//    g_sprintf(s, "%d.%d.%d", IAM_MAJOR_VERSION, IAM_MINOR_VERSION, IAM_BUILD_VERSION);
      g_sprintf(s, "%s",PROGRAM_VERSION);
    return(s);
}

/**
 * Builds a dialog title based on the current structure and
 * coverage files being used.
 *
 * @param which_dialog PREDICT or REALTIME
 * @param coord which coordinate name to use
 *
 * @return newly allocated string used for dialog title.
 */
gchar *AM_GetTitle(gchar *which_dialog, gint coord)
{
#define DISPLAY_STR "Display"
#define STRUCTURE_STR "Structure"
#define COVERAGE_STR "Coverage"
    gchar *title;
    gchar *app_name = g_ascii_strup(g_get_application_name(), -1);
    gchar *version = AM_GetVersion();
    gchar *coord_name = AM_GetViewName(coord);
    
    if (thisCoverageIdentifier[0] == '\0')
    {
        g_stpcpy(thisCoverageIdentifier, PTG_COVERAGE_STR);
    }

    if (thisStructureIdentifier[0] == '\0')
    {
        if (CP_GetISSConfigValue(thisStructureIdentifier) == False)
        {
            g_stpcpy(thisStructureIdentifier, "Structure Unknown");
        }
    }

    title = (gchar *)MH_Calloc((gint)(strlen(app_name)+1+
                                      strlen(version)+2+
                                      strlen(STRUCTURE_STR)+1+
                                      strlen(thisStructureIdentifier)+2+
                                      strlen(COVERAGE_STR)+1+
                                      strlen(thisCoverageIdentifier)+4+
                                      strlen(which_dialog)+1+
                                      strlen(coord_name)+1+
                                      strlen(DISPLAY_STR)+1),
                               __FILE__, __LINE__);
    g_sprintf(title, "%s %s: %s(%s) %s(%s) - %s %s %s",
              app_name,
              version,
              STRUCTURE_STR,
              thisStructureIdentifier,
              COVERAGE_STR,
              thisCoverageIdentifier,
              which_dialog,
              coord_name,
              DISPLAY_STR);

    g_free(app_name);

    return title;
}

/**
 * Sets the configuration identifier used for dialog titles
 *
 * @param structure_identifier the new configuration identifier
 */
void AM_SetStructureIdentifier(gchar *structure_identifier)
{
    if (structure_identifier != NULL)
    {
        g_stpcpy(thisStructureIdentifier, structure_identifier);
    }
}

/**
 * Sets the coverage identifier used for dialog titles
 *
 * @param coverage_identifier the new coverage identifier
 */
void AM_SetCoverageIdentifier(gchar *coverage_identifier)
{
    if (coverage_identifier != NULL)
    {
        g_stpcpy(thisCoverageIdentifier, coverage_identifier);
    }
}

/**
 * This function is the Exit routine. It frees all allocated data, disconnects
 * from ISP, and exits the antman program.
 * If exit_code is -1, abort is used instead of exit so that a core file can
 * be generated.
 *
 * @param exit_code when not zero, force core dump then exit.
 */
void AM_Exit(gint exit_code)
{
    /* clear published value */
    AM_ClearAutoUpdateMode();

    /* free dialog data */
    PDH_Destructor();
    PD_Destructor();
    ED_Destructor();
    RTDH_Destructor();
    RTD_Destructor();
    CH_Destructor();
    CP_Destructor();

    /* free obscuration data */
    PH_Destructor();
    MF_Destructor();
    OM_Destructor();

    /* free memory allocated to the ISP Symbol table */
    IS_Destructor();

    /* free solar prediction data */
    SD_Destructor();

    /* free joint angles data */
    DD_Destructor( True );

    /* disconnect from ISP server and exit */
    IC_DisconnectIsp();

    /* Release isp handler memory */
    IH_Destructor();

    /* check exit code, make core if necessary */
    if (exit_code != 0)
    {
        signal(SIGABRT, SIG_DFL);
        abort();   /* if signal handler needs to generate a core file, abort */
    }

    exit(exit_code);   /* otherwise just exit */
}


/**
 * This function returns the application mode.
 *
 * @return the application mode
 */
gint AM_GetAppMode(void)
{
    return thisAppMode;
}

/**
 * Gets the environment variable 'ACTIVITY_TYPE'; if the
 * string returned is 'MIS', then mission type; if the
 * string returned is 'SIM', then simulation type; otherwise
 * unknown.
 *
 * @return ACTIVITY_TYPE_MIS, or ACTIVITY_TYPE_SIM, or
 * UNKNOWN_ACTIVITY_TYPE
 */
gint AM_GetActivityType(void)
{
    const gchar *activity_type = g_getenv(ACTIVITY_TYPE);
    if (activity_type != NULL)
    {
        if (g_ascii_strcasecmp(activity_type, MIS) == 0)
        {
            return ACTIVITY_TYPE_MIS;
        }
        else if (g_ascii_strcasecmp(activity_type, SIM) == 0)
        {
            return ACTIVITY_TYPE_SIM;
        }
        else
        {
            return UNKNOWN_ACTIVITY_TYPE;
        }
    }

    return UNKNOWN_ACTIVITY_TYPE;
}

/**
 * Gets the environment variable 'sw_lvl'; if the
 * string returned is 'cert_apps', then certified application;
 * if the string returned is 'uncert_apps', then uncertified
 * application, otherwise unknown sw_lvl.
 *
 * @return SW_LVL_CERT_APPS, or SW_LVL_UNCERT_APPS, or
 * UNKNOWN_SW_LVL
 */
gint AM_GetSwLvl(void)
{
    const gchar *sw_lvl = g_getenv(SW_LVL);
    if (sw_lvl != NULL)
    {
        if (g_ascii_strcasecmp(sw_lvl, CERT_APPS) == 0)
        {
            return SW_LVL_CERT_APPS;
        }
        else if (g_ascii_strcasecmp(sw_lvl, UNCERT_APPS) == 0)
        {
            return SW_LVL_UNCERT_APPS;
        }
        else
        {
            return UNKNOWN_SW_LVL;
        }
    }

    return UNKNOWN_SW_LVL;
}

/**
 * Sets the application mode. When the app mode is manager, this
 * allows the user to generate predict data.
 *
 * @param app_mode MANAGER = cato, PUBLIC = all others
 */
void AM_SetAppMode(gint app_mode)
{
    if (app_mode == MANAGER || app_mode == PUBLIC)
    {
        thisAppMode = app_mode;
    }
}

/**
 * This function returns the automatic data update mode.
 *
 * @return the automatic data update mode
 */
gboolean AM_GetAutoUpdateMode(void)
{
    return thisAutoUpdateMode;
}

/**
 * This function updates the automatic data update mode for the application.
 * If the application no longer has automatic update control, the menu item
 * for selecting automatic update control is activated, otherwise it is
 * deactivated.
 *
 * @param msg
 */
void AM_SetAutoUpdateMode(gchar *msg)
{
    gchar   host[255] = {0};    /* host name of process doing auto update  */
    gchar   myHost[255] = {0};  /* host name for this process              */
    gchar   s[255];             /* message string                          */
    gint    process;            /* process id of process doing auto update */
    gint    myProcess;          /* process id for this process             */

    /* decode the message */
    sscanf(msg, "%[^,],%d", host, &process);

    /* verify host/pid are valid */
    if (host[0] != '\0' && process > 0)
    {
        /* get my host and pid */
        gethostname(myHost, 255);
#ifdef G_OS_WIN32
        myProcess = (gint) _getpid();
#else
        myProcess = (gint) getpid();
#endif

        g_message("AM_SetAutoUpdateMode(): host=%s myHost=%s; process=%d myProcess=%d",
                  host, myHost, process, myProcess);

        /* is this my host and process? */
        /* if it is then we're doing the auto update */
        if ((g_ascii_strcasecmp(host, myHost) == 0) && (process == myProcess))
        {
            thisAutoUpdateMode = True;
            InfoMessage(AM_GetActiveDisplay(), "Auto Generation",
                        "This process is now performing automatic updates to the"
                        " predict data." );
        }
        else
        {
            /* another host is doing the update, maybe? */
            thisAutoUpdateMode = False;
            g_sprintf(s, "The IAM process running on %s with process id %d is now doing automatic "
                      "prediction data updates.", host,process);
            InfoMessage(AM_GetActiveDisplay(), "Auto Generation", s);
        }
    }
}

/**
 * Clears the published auto update symbol
 */
void AM_ClearAutoUpdateMode(void)
{
    /* make sure we're doing the autoupdate */
    if (thisAutoUpdateMode == True)
    {
        gchar message[256];
        SymbolRec *symbol = IS_GetSymbol(IS_AntmanAuto);
        g_stpcpy(message, ",");
        IC_PublishIspMsg(message, symbol->symbol_name);
    }
}

/**
 * Sets the time type that is currently in use.
 * Initially the time type is set to GMT.
 *
 * @param time_type the new time type to use
 *
 * @return the time type currently in effect
 */
gint AM_SetTimeType(gint time_type)
{
    switch (time_type)
    {
        case TS_GMT:
        case TS_UTC:
        case TS_PET:
            thisTimeType = time_type;
            break;

        default:
            thisTimeType = TS_GMT;
            break;
    }

    return thisTimeType;
}

/**
 * Gets the time type that is currently in use.
 *
 * @return TS_GMT, TS_UTC, or TS_PET
 */
gint AM_GetTimeType(void)
{
    return thisTimeType;
}

/**
 * Updates the current time type to the next valid time type.
 * Does a round-robin using TS_UTC, TS_GMT, and TS_PET. For
 * TS_PET, the user must enable it through the Predict Dialog.
 *
 * @return new time type, TS_UTC, TS_GMT, or TS_PET
 */
gint AM_UpdateTimeType(void)
{
    /* time type is incremented to cycle thru the valid types */
    thisTimeType++;

    /* set the time type */
    if ((thisTimeType == TS_PET) && (! ValidBasePet()))
    {
        thisTimeType = TS_UTC;
    }
    else if (thisTimeType > TS_PET)
    {
        thisTimeType = TS_UTC;
    }
    else
    {
        /* do nothing */
    }

    return thisTimeType;
}

/**
 * This function returns to coordinate name matching the specified coordinate.
 *
 * @param view current coordinate view
 *
 * @return coordinate name string
 */
gchar *AM_GetViewName(gint coord)
{
    if (coord >= 0 && coord < NUM_COORDS_PLUS_COMBOS)
    {
        return thisViewName[coord];
    }

    return "Unknown View";
}

/**
 * This function sets up all possible signals with a signal handler.
 */
void AM_SetupSignals(void)
{
#ifndef _DEBUG
    signal(SIGSEGV, shutdownHandler);
    signal(SIGINT,  shutdownHandler);
    signal(SIGFPE,  shutdownHandler);
#endif
    signal(SIGILL,  shutdownHandler);
    signal(SIGABRT, shutdownHandler);
    signal(SIGTERM, shutdownHandler);
}

/**
 * Computes a new application window width and height based on the
 * optimum screen width and actual screen width
 *
 * @param widget get the size from this widget
 * @param window_width proposed width
 * @param window_height proposed height
 *
 * @return new window dimensions
 */
Dimension *AM_GetWindowSize(GtkWidget *widget, gint window_width, gint window_height)
{
    Dimension *screen_dim = getScreenDimension(widget);
    Dimension *new_window_dim = (Dimension*)MH_Calloc(sizeof(Dimension), __FILE__, __LINE__);

    new_window_dim->width  = (window_width * screen_dim->width) / OPTIMUM_SCREEN_WIDTH;
    new_window_dim->height = (window_height * screen_dim->height) / OPTIMUM_SCREEN_HEIGHT;

    return new_window_dim;
}

/**
 * Gets the dimension's of the screen
 *
 * @param widget or NULL
 *
 * @return dimension (don't free)
 */
Dimension *getScreenDimension(GtkWidget *widget)
{
    static Dimension dim;
    GdkScreen *screen;
    gint i = -1;

    if (widget != NULL)
    {
        /* make sure window has been realized */
        if (widget->window == NULL)
        {
            gtk_widget_realize(widget);
        }

        screen = gtk_widget_get_screen(widget);
        i = gdk_screen_get_monitor_at_window(screen, widget->window);

        if (i < 0)
        {
            dim.width = gdk_screen_get_width(screen);
            dim.height = gdk_screen_get_height(screen);
        }
        else
        {
            GdkRectangle monitor;
            gdk_screen_get_monitor_geometry(screen, i, &monitor);

            /* some dual monitor setups span both monitors */
            /* check the width vs height ratio, if the width */
            /* is more than twice the height, reduce the size by half */
            dim.width = monitor.width;
            if ((monitor.width>>1) > monitor.height)
            {
                dim.width >>= 1;
            }

            /* the height takes into account the start-bar */
            dim.height = monitor.height;
        }
    }
    else
    {
        screen = gdk_screen_get_default();

        dim.width = gdk_screen_get_width(screen);
        dim.height = gdk_screen_get_height(screen);
    }

    return &dim;
}

/**
 * Returns the absolute path location of the file provided. All rc files are assumed to
 * be located in the config directory.
 *
 * @param rcfile resource file to create with absolute path
 *
 * @return NULL or location of gtkrc-2.0 file
 */
gchar *AM_GetRCFile(gchar *rcfile)
{
    const gchar *tmp_dir = g_get_tmp_dir();
    gchar *filename = (gchar *)MH_Calloc((gint)(strlen(tmp_dir)+strlen(rcfile)+2), __FILE__, __LINE__);
    g_sprintf(filename, "%s%s%s", tmp_dir, G_DIR_SEPARATOR_S, rcfile);

    return filename;
}

/**
 * Adds an rc file to the default rc file list
 *
 * @param rcfile resource file to create
 */
void AM_AddRCFile(gchar *rcfile)
{
    FILE *fp;
    gchar *filename = AM_GetRCFile(rcfile);

    /* clear the file */
    if ((fp = fopen(filename, "w+")) != NULL)
    {
        fclose(fp);
    }

    gtk_rc_add_default_file(filename);

    MH_Free(filename);
}

/**
 * Adds an rc style resource to the given file rcfile.
 * The rcfile should only be the trailing name, the config dir
 * will be prefixed to it prior to opening it.
 *
 * @param rcfile resource file to create
 * @param rcstyle resource style to add to file
 */
void AM_AddResource(gchar *rcfile, gchar *rcstyle)
{
    FILE *fp;
    gchar *filename = AM_GetRCFile(rcfile);

    /* open the file for writing, truncate */
    fp = fopen(filename, "w+");
    if (fp)
    {
        fputs(rcstyle, fp);
        fclose(fp);
    }

    MH_Free(filename);

    gtk_rc_reparse_all();
}

/**
 * Converts the coordinate view type to coordinate type.
 *
 * @param coord_view converts coordinate view type to
 * coordinate type.
 *
 * @return coord_type or -1
 */
gint AM_CoordViewToCoordType(gint coord_view)
{
    gint coord_type = UNKNOWN_COORD;

    switch (coord_view)
    {
        case SBAND1_VIEW:
        case S1KU_SBAND_VIEW:
            coord_type = SBAND1;
            break;

        case SBAND2_VIEW:
        case S2KU_SBAND_VIEW:
            coord_type = SBAND2;
            break;

        case KUBAND_VIEW:
        case S1KU_KUBAND_VIEW:
        case S2KU_KUBAND_VIEW:
            coord_type = KUBAND;
            break;

        case STATION_VIEW:
            coord_type = STATION;
            break;

        default:
            break;
    }

    return coord_type;
}

/**
 * This method returns the current drawable used for rendering graphics
 *
 * @param windata Window data, need the drawable
 * @param which_src_drawable Flag indicating which drawable to use
 * in the window data structure.
 *
 * @return the specific drawable or the src drawable if not specified.
 */
GdkDrawable *AM_Drawable(WindowData *windata, gint which_src_drawable)
{
    switch (which_src_drawable)
    {
        default:
        case SRC_DRAWABLE:
            return GDK_DRAWABLE(windata->src_pixmap);

        case BG_DRAWABLE:
            return GDK_DRAWABLE(windata->bg_pixmap);

        case DST_DRAWABLE:
            return GDK_DRAWABLE(windata->drawing_area->window);
    }
}

/**
 * Allocates pixmap data for a view. Allocation occurs only if
 * the size of the window changed. Or if the force flag is set True.
 *
 * @param windata window data
 * @param pixmap_data pixmap data
 * @param force True indicates force a change, ignore size check.
 *
 * @return True if allocation occured, otherwise False
 */
gboolean AM_AllocatePixmapData(WindowData *windata, PixmapData *pixmap_data, gboolean force)
{
    gboolean allocate = False;

    /**
     * is the force set? or has size changed?
     */
    if (force == True ||
        pixmap_data->width  != windata->drawing_area->allocation.width ||
        pixmap_data->height != windata->drawing_area->allocation.height)
    {
        if (pixmap_data->pixmap != NULL)
        {
            g_object_unref(pixmap_data->pixmap);
        }

        allocate = True;
        pixmap_data->pixmap = gdk_pixmap_new(windata->drawing_area->window,
                                             windata->drawing_area->allocation.width,
                                             windata->drawing_area->allocation.height, -1);

        pixmap_data->width  = windata->drawing_area->allocation.width;
        pixmap_data->height = windata->drawing_area->allocation.height;

        AM_ClearDrawable(pixmap_data->pixmap,
                         windata->drawing_area->style->black_gc,
                         0, 0,
                         pixmap_data->width, pixmap_data->height);
    }

    return allocate;
}

/**
 * Copies the local pixmap image into the primary pixmap.
 *
 * @param windata window data, has the destination drawable, determined by
 * the which_drawable state value.
 * @param src_drawable draw from this drawable
 */
void AM_CopyPixmap(WindowData *windata, GdkDrawable *src_drawable)
{
    GdkGC *gc;
    GdkGCValues _values;

    _values.function = GDK_OR;
    gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

    gdk_draw_drawable(AM_Drawable(windata,windata->which_drawable),
                      gc,
                      GDK_DRAWABLE(src_drawable),
                      0, 0,  /* dst x, y */
                      0, 0,  /* src x, y */
                      -1, -1 /* src width, height, use ALL */ );
    g_object_unref(gc);
}

/**
 * Clears the drawable by drawing a rectangle into it.
 *
 * @param drawable clear this drawable
 * @param src_gc use this gc for clear operation
 * @param x x location for clear operation
 * @param y y location for clear operation
 * @param width width of area to clear
 * @param height height of area to clear
 */
void AM_ClearDrawable(GdkDrawable *drawable, GdkGC *src_gc, gint x, gint y, gint width, gint height)
{
    if (drawable)
    {
        GdkGC *dst_gc;
        GdkGCValues src_values;

        gdk_gc_get_values(src_gc, &src_values);

        GdkGCValues _values;
        _values.function = GDK_COPY;
        _values.background = src_values.background;
        _values.foreground = src_values.foreground;
        dst_gc = gdk_gc_new_with_values(drawable, &_values,
                                        (GdkGCValuesMask)(GDK_GC_BACKGROUND|GDK_GC_FOREGROUND|GDK_GC_FUNCTION));

        /* clear drawable */
        gdk_draw_rectangle(GDK_DRAWABLE(drawable), dst_gc, True, x, y, width, height);

        g_object_unref(dst_gc);
    }
}
