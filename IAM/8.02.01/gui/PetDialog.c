/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#define PRIVATE
#include "PetDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "EventDialog.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "TimeStrings.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PetDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget    *thisDialog  = NULL;   /* widget for the PET dialog */
static GtkWidget    *thisPetEntry= NULL;   /* widget of base PET time in popup */
static gchar         thisPetTime[TIME_STR_LEN] = UNSET_TIME;
/*************************************************************************/

/**
 * This method creates the dialog
 *
 * @param parent attach dialog to this window
 */
void PET_Create(GtkWindow *parent)
{
    if (thisDialog == NULL)
    {
        /* make the dialog with ok,cancel,clear buttons; the clear button will
         * use the close response
         */
        thisDialog = gtk_dialog_new_with_buttons("Set Base PET",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                                  GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                 GTK_STOCK_CLEAR, GTK_RESPONSE_CLOSE,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(response_cb), NULL);

        /* create time entry field and add to the dialog */
        thisPetEntry = gtk_entry_new();
        g_signal_connect(thisPetEntry, "key-press-event",
                         G_CALLBACK(key_press_cb),
                         NULL);
        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), thisPetEntry);
    }
}

/**
 * This function pops up the Set Base PET dialog
 */
void PET_Open(void)
{
    if ((thisDialog != NULL) && (thisPetEntry != NULL))
    {
        gint time_type = AM_GetTimeType();

        /* is PET set ? */
        if (ValidBasePet() == False)
        {
            GList *currentPt = PDH_GetCurrentPt();
            if (currentPt != NULL)
            {
                PredictPt *pt = (PredictPt *)currentPt->data;
                SecsToStr(time_type, pt->timeSecs, thisPetTime);
            }
            else
            {
                SecsToStr(time_type, CurrentUtcSecs(), thisPetTime);
            }
        }
        else
        {
            glong secs = StrToSecs(TS_PET, GetBasePet());
            SecsToStr(time_type, secs, thisPetTime);
            g_stpcpy(thisPetTime, thisPetTime);
        }

        gtk_entry_set_text(GTK_ENTRY(thisPetEntry), thisPetTime);
        gtk_widget_show_all(thisDialog);
    }
}

/**
 * This method handles the button events
 *
 * @param widget widget making call
 * @param response_id user selection
 * @param data user data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;
    gchar *petPtr;
    gchar petStr[TIME_STR_LEN] = {0};

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            {
                gint time_type = AM_GetTimeType();

                petPtr = (gchar*)gtk_entry_get_text(GTK_ENTRY(thisPetEntry));
                g_stpcpy(petStr, petPtr);
                if (ValidTime(time_type, petPtr) == False)
                {
                    /* New value is not valid;  reset old value, leaving dialog up */
                    gdk_beep();
                    gtk_entry_set_text(GTK_ENTRY(thisPetEntry), thisPetTime);
                    doUnmanage = FALSE;
                }
                else
                {
                    glong secs = StrToSecs(time_type, petStr);
                    SecsToStr(TS_PET, secs, petStr);
                    SetBasePet(petStr);
                    if (ValidBasePet() == True)
                    {
                        AM_SetTimeType(TS_PET);
                        RTD_UpdateTimeType(False);
                        PD_UpdateTimeType(False);
                        ED_UpdateTimeType(False);
                    }
                }
            }
            break;

        case GTK_RESPONSE_CANCEL:
            break;

        default:
        case GTK_RESPONSE_CLOSE: /* clear */
            SetBasePet(UNSET_TIME);
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean key_press_cb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return False;
    }

    gdk_beep();
    return True;
}
