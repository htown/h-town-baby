/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "AntennaDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "DynamicDraw.h"
#include "MessageHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "Vector.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/AntennaDialog.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define NEGATIVE_SIGN '-'
#define POSITIVE_SIGN '+'

static GtkWidget    *thisDialog  = NULL;   /* widget for the PPL dialog */
static gboolean     thisDoRedraw = False;

static gchar         *thisAntennaConfigNames[NUM_COORDS] =
{
    SBAND1_LOCATION,
    SBAND2_LOCATION,
    KUBAND_LOCATION,
    KUBAND2_LOCATION,
    GENERIC_LOCATION
};

static gchar         *thisAntennaNames[NUM_COORDS] =
{
    SBAND1_TITLE,
    SBAND2_TITLE,
    KUBAND_TITLE,
    KUBAND2_TITLE,
    STATION_TITLE
};

#define NUM_XYZ     3

static gchar         *thisXYZNames[NUM_XYZ] =
{
    "X",
    "Y",
    "Z"
};

static GtkWidget    *thisEntry[NUM_COORDS][NUM_XYZ] =
{
    {NULL, NULL, NULL},
    {NULL, NULL, NULL},
    {NULL, NULL, NULL},
    {NULL, NULL, NULL},
    {NULL, NULL, NULL}
};
/*************************************************************************/

/**
 * This method creates the dialog
 *
 * @param parent attach dialog to this window.
 */
void AD_Create(GtkWindow *parent)
{
    GtkWidget *vbox;
    GtkWidget *container;
    GtkWidget *separator;

    if (thisDialog == NULL)
    {
        /* make the dialog with ok,cancel,clear buttons; the clear button will
         * use the close response
         */
        thisDialog = gtk_dialog_new_with_buttons("Change Antenna Location",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                                  GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                 GTK_STOCK_APPLY, GTK_RESPONSE_APPLY,
                                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(response_cb), NULL);

        vbox = gtk_vbox_new(False, 3);
        {
            /* make headers */
            container = create_headers();
            gtk_box_pack_start(GTK_BOX(vbox), container, True, False, 3);

            container = create_entries();
            gtk_box_pack_start(GTK_BOX(vbox), container, True, False, 3);

            separator = gtk_hseparator_new();
            gtk_box_pack_start(GTK_BOX(vbox), separator, True, False, 0);

            container = create_buttons();
            gtk_box_pack_start(GTK_BOX(vbox), container, True, False, 3);
        }

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), vbox);
    }
}

/**
 * Opens the dialog. Prior to loading the dialog, the dynamics class is read
 * and the data for the kuband, sband, lowgain antenna's are loaded into the
 * entry fields. The first time, these values will represent what is found in
 * the config file.
 */
void AD_Open(void)
{
    if (thisDialog != NULL)
    {
        /* load the entry widgets with current values from iam.xml */
        load_from_dynamics();

        /* show the dialog */
        gtk_widget_show_all(thisDialog);
    }
}

/**
 * Calls the dynamic class and loads the current values into
 * the dialog.
 */
static void load_from_dynamics(void)
{
    guint c;
    VECTOR *data;
    gchar sdata[256];
    gchar message[256];

    for (c=0; c<NUM_COORDS; c++)
    {
        data = DD_GetAntennaLocation(c);
        if (data == NULL)
        {
            g_sprintf(message,
                    "%s configuration variable error!\n"
                    "Dynamic Structures not generated.",
                    thisAntennaConfigNames[c]);
            ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
        }
        else
        {
            g_sprintf(sdata, "%f", data->x);
            gtk_entry_set_text(GTK_ENTRY(thisEntry[c][0]), sdata);

            g_sprintf(sdata, "%f", data->y);
            gtk_entry_set_text(GTK_ENTRY(thisEntry[c][1]), sdata);

            g_sprintf(sdata, "%f", data->z);
            gtk_entry_set_text(GTK_ENTRY(thisEntry[c][2]), sdata);
        }
    }
}

/**
 * Takes the data values from the dialog and loads them into the dynamic class.
 * Only those values that have changed will be loaded. Also if a change is
 * detected, then the realtime and predict redraw flags are set.
 */
static void set_dynamic_data(void)
{
    guint c;
    const gchar *text;
    VECTOR data;
    VECTOR *old_data;

    thisDoRedraw = False;

    for (c=0; c<NUM_COORDS; c++)
    {
        /* get original data */
        old_data = DD_GetAntennaLocation(c);

        /* get the data values for the antenna */
        text = gtk_entry_get_text(GTK_ENTRY(thisEntry[c][0]));
        sscanf(text, "%lf", &data.x);

        text = gtk_entry_get_text(GTK_ENTRY(thisEntry[c][1]));
        sscanf(text, "%lf", &data.y);

        text = gtk_entry_get_text(GTK_ENTRY(thisEntry[c][2]));
        sscanf(text, "%lf", &data.z);

        /* has the values changed ? */
        if (old_data->x != data.x ||
            old_data->y != data.y ||
            old_data->z != data.z)
        {
            /* save the values */
            DD_SetAntennaLocation(c, &data);
            thisDoRedraw = True;
        }
    }
}

/**
 * Reads the antenna location values from the config file.
 */
static void load_from_config(void)
{
    guint c,v;
    gdouble data[NUM_XYZ];
    gchar sdata[256];
    gchar message[256];

    for (c=0; c<NUM_COORDS; c++)
    {
        if (CP_GetConfigData(thisAntennaConfigNames[c], sdata) == False)
        {
            g_sprintf(message,
                    "%s configuration variable error!\n"
                    "Dynamic Structures not generated.",
                    thisAntennaConfigNames[c]);
            ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
        }
        else
        {
            if (sscanf(sdata, "%lf %lf %lf", &data[0], &data[1], &data[2] ) != 3)
            {
                g_sprintf(message,
                        "Configuration parameter %s is not formatted correctly!",
                        thisAntennaConfigNames[c]);
                ErrorMessage(AM_GetActiveDisplay(), "Data Error", message);
            }
            else
            {
                for (v=0; v<NUM_XYZ; v++)
                {
                    g_sprintf(sdata, "%f", data[v]);
                    gtk_entry_set_text(GTK_ENTRY(thisEntry[c][v]), sdata);
                }
            }
        }
    }
}

/**
 * Creates the gui header components.
 *
 * @return container that holds all components that make up the header
 */
static GtkWidget *create_headers(void)
{
    guint i;
    GtkWidget *hbox;
    GtkWidget *label;

    hbox = gtk_hbox_new(True, 0);
    {
        label = gtk_label_new("");
        gtk_box_pack_start(GTK_BOX(hbox), label, True, True, 0);
        for (i=0; i<NUM_XYZ; i++)
        {
            label = gtk_label_new(thisXYZNames[i]);
            gtk_box_pack_start(GTK_BOX(hbox), label, True, True, 0);
        }
    }

    return hbox;
}

/**
 * Creates the gui entry components
 *
 * @return container that holds all the entry widgets
 */
static GtkWidget *create_entries(void)
{
    guint i,j;
    GtkWidget *align;
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 3);
    {
        for (i=0; i<NUM_COORDS; i++)
        {
            hbox = gtk_hbox_new(True, 0);
            {
                align = gtk_alignment_new (1.0, 0.5, 0.0, 0.0);
                label = gtk_label_new(thisAntennaNames[i]);
                gtk_container_add(GTK_CONTAINER(align), label);
                gtk_box_pack_start(GTK_BOX(hbox), align, True, True, 5);

                for (j=0; j<NUM_XYZ; j++)
                {
                    thisEntry[i][j] = gtk_entry_new();
                    g_signal_connect(thisEntry[i][j],
                                     "key-press-event",
                                     G_CALLBACK(key_press_cb),
                                     NULL);
                    gtk_box_pack_start(GTK_BOX(hbox), thisEntry[i][j], True, True, 0);
                }
            }
            gtk_box_pack_start(GTK_BOX(vbox), hbox, True, False, 5);
        }
    }

    return vbox;
}

/**
 * Creates the gui button components.
 *
 * @return container has all the button components
 */
static GtkWidget *create_buttons(void)
{
    GtkWidget *hbox;
    GtkWidget *button;

    hbox = gtk_hbox_new(False, 3);
    {
        button = gtk_button_new_with_label("Reset from config");
        gtk_box_pack_end(GTK_BOX(hbox), button, False, False, 3);
        g_signal_connect(button, "clicked", G_CALLBACK(reset_from_config_cb), NULL);
    }

    return hbox;
}

/**
 * Callback for the reset button; loads the data from the config file.
 *
 * @param button not used
 * @param data not used
 */
static void reset_from_config_cb(GtkButton *button, void * data)
{
    /* reload the values from the config file */
    load_from_config();
}

/**
 * This method handles the button events
 *
 * @param widget the calling widget
 * @param response_id the button action that got us here
 * @param data user data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = False;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            doUnmanage = True;
            /* fall thru */

        case GTK_RESPONSE_APPLY:

            /* apply antenna changes */
            set_dynamic_data();

            /* redraw realtime ? */
            if (thisDoRedraw == True)
            {
                RTD_SetRedraw(True);
                RTD_DrawData();

                PD_SetRedraw(True);
                PD_DrawData(False);
            }
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            /* do nothing */
            doUnmanage = True;
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}

/**
 * Callback for processing the key press event for the EL values. It first
 * validates the keystroke, then validates that the lower and upper are
 * numerically lower < upper.
 *
 * @param widget the calling widget
 * @param event event data
 * @param data user data
 *
 * @return True stops the callback, False continues.
 */
static gboolean key_press_cb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    gboolean is_valid = True;
    gchar *value;
    gint position = gtk_editable_get_position(GTK_EDITABLE(widget));

    if (valid_key(event->keyval, position) == True)
    {
        is_valid = False;

        /* handle the period a little differently */
        if (event->keyval == GDK_period)
        {
            /* stop processing, we want to reset the text */
            /* here and reset the cursor position */
            is_valid = True;
            value = format_value(widget, event);
            gtk_entry_set_text(GTK_ENTRY(widget), value);
            g_free(value);

            /* move cursor past period */
            gtk_editable_set_position(GTK_EDITABLE(widget), position+1);
        }
    }

    if (is_valid == True && event->keyval != GDK_period)
    {
        gdk_beep();
    }

    return is_valid;
}

/**
 * Validates the keystroke
 *
 * @param keyval keystroke value
 * @param cursor_position position of cursor when key pressed
 *
 * @return True key value is good, otherwise False
 */
static gboolean valid_key(guint keyval, gint cursor_position)
{
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        (key == NEGATIVE_SIGN && cursor_position == 0)      ||
        (key == POSITIVE_SIGN && cursor_position == 0)      ||
        (keyval == GDK_Home      || keyval == GDK_Left      ||
         keyval == GDK_Up        || keyval == GDK_Right     ||
         keyval == GDK_Down      || keyval == GDK_Prior     ||
         keyval == GDK_Page_Up   || keyval == GDK_Next      ||
         keyval == GDK_Page_Down || keyval == GDK_End       ||
         keyval == GDK_BackSpace || keyval == GDK_Delete    ||
         keyval == GDK_Tab       || keyval == GDK_Return    ||
         keyval == GDK_Shift_L   || keyval == GDK_Shift_R   ||
         keyval == GDK_Control_L || keyval == GDK_Control_R ||
         keyval == GDK_Alt_L     || keyval == GDK_Alt_R     ||
         keyval == GDK_VoidSymbol|| keyval == GDK_period))
    {
        return True;
    }

    return False;
}

/**
 * Formats the entry text with the event data. This method determines where the
 * cursor position is and places the new text at that position. In the case of
 * a delete, the data at the cursor position is removed. For backspace, the
 * data to the left is removed, for delete, data to the right.
 *
 * @param entry_w text entry widget
 * @param event event data
 *
 * @return new formatted string; the string must be freed
 */
static gchar *format_value(GtkWidget *entry_w, GdkEventKey *event)
{
    guint i;
    guint j;
    gchar tmp_value[64];
    gchar value[64];

    g_stpcpy(value, gtk_entry_get_text(GTK_ENTRY(entry_w)));

    if (event != NULL)
    {
        guint cursor_position = gtk_editable_get_position(GTK_EDITABLE(entry_w));
        guint key = gdk_unicode_to_keyval(event->keyval);

        memset(tmp_value, '\0', 64);

        if (g_ascii_isdigit(key) == True)
        {
            for (i=0; i<cursor_position; i++)
            {
                tmp_value[i] = value[i];
            }

            tmp_value[cursor_position] = key;

            for (i=cursor_position; i<(strlen(value)-1); i++)
            {
                tmp_value[i+1] = value[i];
            }
            tmp_value[i+1] = '\0';

            g_stpcpy(value, tmp_value);
        }
        else
        {
            /* check for minus/plus, position of cursor has already been
               validated, it should be at 0 */
            if (key == NEGATIVE_SIGN ||
                key == POSITIVE_SIGN)
            {
                tmp_value[0] = key;
                for (i=0,j=1; i<(strlen(value)-1); i++,j++)
                {
                    tmp_value[j] = value[i];
                }
                tmp_value[j] = '\0';

                g_stpcpy(value, tmp_value);
            }
            else if (event->keyval == GDK_Delete)
            {
                for (i=0; i<cursor_position; i++)
                {
                    tmp_value[i] = value[i];
                }

                for (i=cursor_position, j=cursor_position+1; i<(strlen(value)-1); i++, j++)
                {
                    tmp_value[i] = value[j];
                }
                tmp_value[j] = '\0';

                g_stpcpy(value, tmp_value);
            }
            else if (event->keyval == GDK_BackSpace)
            {
                if (cursor_position > 0)
                {
                    for (i=0; i<cursor_position-1; i++)
                    {
                        tmp_value[i] = value[i];
                    }

                    for (i=cursor_position-1, j=cursor_position; j<strlen(value); i++, j++)
                    {
                        tmp_value[i] = value[j];
                    }
                    tmp_value[i] = '\0';

                    g_stpcpy(value, tmp_value);
                }
            }
            else if (event->keyval == GDK_period)
            {
                gchar tmp_value2[64];
                memset(tmp_value2, '\0', 64);

                for (i=0; i<cursor_position; i++)
                {
                    tmp_value[i] = value[i];
                }

                tmp_value[cursor_position] = key;

                for (i=cursor_position; i<strlen(value); i++)
                {
                    tmp_value[i+1] = value[i];
                }
                tmp_value[i+1] = '\0';

                /* remove original period */
                for (i=0,j=0; i<strlen(tmp_value); i++)
                {
                    if (tmp_value[i] == (gchar)event->keyval &&
                        i != cursor_position)
                    {
                        /* do nothing */
                    }
                    else
                    {
                        tmp_value2[j] = tmp_value[i];
                        ++j;
                    }
                }
                tmp_value2[j] = '\0';


                g_stpcpy(value, tmp_value2);
            }
            else
            {
            }
        }
    }

    return g_strdup(value);
}
