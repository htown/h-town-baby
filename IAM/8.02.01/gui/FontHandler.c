/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "FontHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/FontHandler.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


#define FONT_WIDTH  0
#define FONT_HEIGHT 1


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/*************************************************************************/

gchar *FH_GetFontName(void)
{
    gchar font_name[255];
    if (CP_GetConfigData(IAM_FONT, font_name) == False)
    {
        /* font not found in iam.xml, use fallback; see FontHandler.h */
        g_stpcpy(font_name, FONT_NAME);
    }

    return g_strdup(font_name);
}

gchar *FH_GetLargeBoldFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", BOLD_KEYWORD, " ", _l_font_size, NULL);
}

gchar *FH_GetLargePlainFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", _l_font_size, NULL);
}

gchar *FH_GetDefaultBoldFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", BOLD_KEYWORD, " ", _m_font_size, NULL);
}

gchar *FH_GetDefaultPlainFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", _m_font_size, NULL);
}

gchar *FH_GetSmallBoldFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", BOLD_KEYWORD, " ", _s_font_size, NULL);
}

gchar *FH_GetSmallPlainFont(void)
{
    return g_strconcat(FH_GetFontName(), " ", _s_font_size, NULL);
}

/**
 * Returns the height of the text (pixels) specified in the pango layout
 *
 * @param layout pango layout
 *
 * @return height height of layout
 */
gint FH_GetFontHeight(PangoLayout *layout)
{
    return fontDimension(layout, FONT_HEIGHT);
}

/**
 * Returns the width of the text (pixels) specified in the pango layout
 *
 * @param layout pango layout
 *
 * @return width width of layout
 */
gint FH_GetFontWidth(PangoLayout *layout)
{
    return fontDimension(layout, FONT_WIDTH);
}

/**
 * Adds a font_name style for the given filename
 *
 * @param rcfile resource file
 * @param font_size font size
 * @param style_name name for style
 */
void FH_AddRCFontStyle(gchar *rcfile, guint font_size, const gchar *style_name)
{
    gchar rcstyle[256];

    g_sprintf (rcstyle,
             "style \"%s-default\" {\n"
             "   font_name = \"%s %u\"\n"
             "}\n"
             "widget \"%s.*\" style \"%s-default\"\n",
             style_name, FH_GetFontName(), font_size, style_name, style_name);

    AM_AddResource(rcfile, rcstyle);
}

/**
 * Does the actual work figuring out the font width and height.
 *
 * @param layout has the font information
 * @param dimension_type for width or height
 *
 * @return font width or height in pixels
 */
static gint fontDimension(PangoLayout *layout, gint dimension_type)
{
    PangoRectangle ink_rect;
    PangoRectangle logical_rect;

    pango_layout_get_pixel_extents(layout, &ink_rect, &logical_rect);

    if (dimension_type == FONT_WIDTH)
    {
        return logical_rect.width;
    }

    return logical_rect.height;
}
