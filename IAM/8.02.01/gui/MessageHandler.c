/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #include <TCHAR.H>
    //#include <windows.h>
#endif

#define PRIVATE
#include "MessageHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "ColorHandler.h"
#include "MemoryHandler.h"
#include "PrintHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/MessageHandler.c 271 2017-02-20 20:59:17Z dpham1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define LESS        0
#define MORE        1
#define LESS_STR    "<< Less"
#define MORE_STR    "More >>"
/*************************************************************************/

/**
 * This method displays an infomational dialog with the given message.
 *
 * @param parent attach dialog to this window
 * @param title title for dialog
 * @param message message for this dialog
 */
void InfoMessage(GtkWidget *parent, gchar *title, gchar *message)
{
    GtkWidget *dialog;
    if (parent != NULL && GTK_IS_WINDOW(parent) == True)
    {
        dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_INFO,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(parent));
    }
    else
    {
        dialog = gtk_message_dialog_new(NULL,
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_INFO,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    }

    gtk_window_set_title (GTK_WINDOW (dialog), title);

    /* handle button events */
    g_signal_connect(dialog, "response", G_CALLBACK(response_cb), NULL);

    gtk_widget_show_all(dialog);
}

/**
 * This method displays a warning dialog with the given message.
 *
 * @param parent attach dialog to this window
 * @param title title for dialog
 * @param message message for this dialog
 */
void WarningMessage(GtkWidget *parent, gchar *title, gchar *message)
{
    GtkWidget *dialog;
    if (parent != NULL && GTK_IS_WINDOW(parent) == True)
    {
        dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_WARNING,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(parent));
    }
    else
    {
        dialog = gtk_message_dialog_new(NULL,
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_WARNING,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    }

    gtk_window_set_title (GTK_WINDOW (dialog), title);

    /* handle button events */
    g_signal_connect(dialog, "response", G_CALLBACK(response_cb), NULL);

    gtk_widget_show(dialog);
}

/**
 * This method displays an error dialog with the given message.
 *
 * @param parent attach dialog to this window
 * @param title title for dialog
 * @param message message for this dialog
 */
void ErrorMessage(GtkWidget *parent, gchar *title, gchar *message)
{
    GtkWidget *dialog;
    if (parent != NULL && GTK_IS_WINDOW(parent) == True)
    {
        dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_ERROR,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(parent));
    }
    else
    {
        dialog = gtk_message_dialog_new(NULL,
                                        GTK_DIALOG_DESTROY_WITH_PARENT,
                                        GTK_MESSAGE_ERROR,
                                        GTK_BUTTONS_OK,
                                        message);
        gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    }

    gtk_window_set_title (GTK_WINDOW (dialog), title);

    /* handle button events */
    g_signal_connect(dialog, "response", G_CALLBACK(response_cb), NULL);

    gtk_widget_show_all(dialog); 
}

/**
 * This method displays a question dialog.
 *
 * @param parent attach dialog to this window
 * @param title title for dialog
 * @param message message for this dialog
 * @param default_answer answer to question
 *
 * @return the answer
 */
gboolean QuestionMessage(GtkWidget *parent, gchar *title, gchar *message, gint default_answer)
{
    GtkWidget *dialog;
    gboolean answer = False;
    if (parent != NULL && GTK_IS_WINDOW(parent) == True)
    {
        dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
                                        GTK_DIALOG_MODAL,
                                        GTK_MESSAGE_QUESTION,
                                        GTK_BUTTONS_YES_NO,
                                        message);
        gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(parent));
    }
    else
    {
        dialog = gtk_message_dialog_new(NULL,
                                        GTK_DIALOG_MODAL,
                                        GTK_MESSAGE_QUESTION,
                                        GTK_BUTTONS_YES_NO,
                                        message);
        gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
    }

    gtk_window_set_title (GTK_WINDOW (dialog), title);
    gtk_dialog_set_default_response (GTK_DIALOG (dialog), default_answer);
    g_signal_connect (dialog, "response",
                      G_CALLBACK (answerTheQuestion),
                      &answer);
    gtk_widget_show(dialog);

    gtk_widget_destroy (dialog);

    return answer;
}

/**
 * Callback for the question dialog
 *
 * @param dialog the dialog
 * @param response_id YES or NO
 * @param data use this for returning the response value
 */
static void answerTheQuestion (GtkDialog *dialog, gint response_id, void * data)
{
    (*(gboolean *)data) = (response_id == GTK_RESPONSE_YES ? True : False);
}

/**
 * More flexible message. Builds any kind based on the message type field.
 * If colors are provided, the dialog and text will be colorized.
 *
 * @param parent owner of dialog
 * @param message_type type of dialog
 * @param title title of dialog
 * @param message primary message
 * @param expand_message extra message text user can optionaly show/hide
 * @param fg_color text foreground color
 * @param bg_color text background color
 * @param font_desc text font
 */

void Message(GtkWidget *parent, GtkMessageType message_type, gchar *title, gchar *message, gchar *expand_message, gchar *fg_color, gchar *bg_color, PangoFontDescription *font_desc)
{
    GtkWidget *dialog;
    GtkWidget *hbox;
    GtkWidget *vbox;
    GtkWidget *stock;
    GtkWidget *event_box;
    GtkWidget *label;

    dialog = gtk_dialog_new_with_buttons(title,
                                         GTK_WINDOW(parent),
                                         (GtkDialogFlags)(GTK_DIALOG_DESTROY_WITH_PARENT),
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         GTK_STOCK_PRINT, GTK_RESPONSE_APPLY,
                                         NULL);

    gtk_window_set_resizable(GTK_WINDOW(dialog), True);
    gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER_ON_PARENT);

    vbox = gtk_vbox_new(False, 8);
    {
        /* container for icon and text */
        hbox = gtk_hbox_new(False, 8);
        gtk_container_set_border_width(GTK_CONTAINER(hbox), 8);

        switch (message_type)
        {
            default:
            case GTK_MESSAGE_INFO:
                stock = gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
                break;

            case GTK_MESSAGE_WARNING:
                stock = gtk_image_new_from_stock(GTK_STOCK_DIALOG_WARNING, GTK_ICON_SIZE_DIALOG);
                break;

            case GTK_MESSAGE_ERROR:
                stock = gtk_image_new_from_stock(GTK_STOCK_DIALOG_ERROR, GTK_ICON_SIZE_DIALOG);
                break;
        }
        gtk_box_pack_start(GTK_BOX(hbox), stock, False, False, 0);

        /* use event box for background color, labels have no background, they're transparent */
        event_box = gtk_event_box_new();
        gtk_widget_show(event_box);

        /* make label with message */
        label = gtk_label_new(message);
        gtk_widget_show(label);

        /* change font for message ? */
        if (font_desc != NULL)
        {
            gtk_widget_modify_font(label, font_desc);
        }

        /* change text fg color ? */
        if (fg_color != NULL)
        {
            gtk_widget_modify_fg(label, GTK_STATE_NORMAL, CH_GetGdkColorByName(fg_color));
        }

        /* change text and dialog bg color ? */
        if (bg_color != NULL)
        {
            gtk_widget_modify_bg(event_box, GTK_STATE_NORMAL, CH_GetGdkColorByName(bg_color));
            gtk_widget_modify_bg(dialog, GTK_STATE_NORMAL, CH_GetGdkColorByName(bg_color));
        }

        gtk_container_add(GTK_CONTAINER(event_box), label);
        gtk_box_pack_start(GTK_BOX(hbox), event_box, False, False, 0);
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 0);

        /* add message */
        if (expand_message != NULL)
        {
            GtkWidget *expander = gtk_expander_new(MORE_STR);
            {
                GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
                gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled_window),
                                                    GTK_SHADOW_ETCHED_IN);
                gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scrolled_window),
                                               GTK_POLICY_AUTOMATIC,
                                               GTK_POLICY_AUTOMATIC);
                gtk_scrolled_window_set_placement(GTK_SCROLLED_WINDOW(scrolled_window),
                                                  GTK_CORNER_TOP_LEFT);

                label = gtk_label_new(expand_message);
                gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

                gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), label);
                gtk_container_add(GTK_CONTAINER(expander), scrolled_window);

                g_signal_connect(expander, "activate", G_CALLBACK(activate_cb), dialog);
            }
            gtk_box_pack_start(GTK_BOX(vbox), expander, True, True, 2);
        }

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox);
    }

    g_signal_connect(dialog, "response", G_CALLBACK(result_cb), expand_message);

    /* show the dialog */
    gtk_widget_show_all(dialog);
}

/**
 * retrieves the result of the dialog,closes the dialog and destroys it.
 *
 * @param widget
 * @param result_id
 * @param data
 */
static void result_cb(GtkWidget *widget, gint result_id, gchar *expand_message)
{
    const gchar *title;

    switch (result_id)
    {
        default:
        case GTK_RESPONSE_OK:
            break;

        case GTK_RESPONSE_APPLY:
            
            title =  gtk_window_get_title(GTK_WINDOW(widget));
            print_data(expand_message, (gchar *)title);
            break;
    }
    /* destroy the widget, its so small, we'll just build it each time */
    gtk_widget_destroy (widget);
}

/**
 * Handles the expander button in the message dialog
 *
 * @param expander the expander
 * @param user_data the dialog
 */
static void activate_cb(GtkExpander *expander, void * user_data)
{
    gint width, height;
    GtkWidget *dialog = GTK_WIDGET(user_data);

    if (gtk_expander_get_expanded(expander) == True)
    {
        gdk_window_get_size(GTK_WIDGET(dialog)->window, &width, &height);
        {
            gtk_window_resize(GTK_WINDOW(dialog), width, 10);
        }
        gtk_expander_set_label(expander, MORE_STR);
    }
    else
    {
        gdk_window_get_size(GTK_WIDGET(dialog)->window, &width, &height);
        {
            gtk_window_resize(GTK_WINDOW(dialog), width, 250);
        }
        gtk_expander_set_label(expander, LESS_STR);
    }
}

/**
 * Prints the expanded message data from the Message method.
 * First the buffer is saved in a tmp location, then printed.
 *
 * @param print_data the data to print.
 * @param title print this in the title area
 */
static void print_data(gchar *print_data, gchar *title)
{
    FILE   *fp;
    const gchar *tmp_dir = g_get_tmp_dir();

#define GEN_PREDICT_LOG "gen_predict.log"
    gchar *filename = (gchar *)MH_Calloc((guint)(strlen(tmp_dir)+strlen(GEN_PREDICT_LOG)+2), __FILE__, __LINE__);
    g_sprintf(filename, "%s%s%s", tmp_dir, G_DIR_SEPARATOR_S, GEN_PREDICT_LOG);

    /* output the data to tmp */
    fp = fopen(filename, "w+");
    if (fp != NULL)
    {
        g_fprintf(fp, "%s", print_data);
        fclose(fp);

        PRP_Print(TEXT_FILE_TYPE, filename, title);
    }

    MH_Free(filename);
}

/**
 * Generic handler for the various message dialogs. Basically
 * just closes the dialog and destroys it.
 *
 * @param widget
 * @param response_id
 * @param data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    /* destroy the widget, its so small, we'll just build it each time */
    gtk_widget_destroy(widget);
}
