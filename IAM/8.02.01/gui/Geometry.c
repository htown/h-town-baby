/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define PRIVATE
#include "Geometry.h"
#undef PRIVATE

#include "MemoryHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/Geometry.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/*************************************************************************/

/**
 * This function determines if the line (p0-p2) is counterclockwise from the
 * line formed by (p0-p1).  It returns 1 if counterclockwise, -1 if clockwise,
 * and 0 if neither.  The determination of the "clock state" for three colinear
 * points is done in such a way as to make the other algorithms in this section
 * which use this function work easier.
 *
 * @param p0 first point
 * @param p1 second point
 * @param p2 third point
 *
 * @return 1, -1, or 0
 */
gint CCW( Point p0, Point p1, Point p2 )
{
    Point d1, d2;                   /* p1 and p2 relative to p0 */

    d1.x = p1.x - p0.x;             /* calculate d1 */
    d1.y = p1.y - p0.y;
    d2.x = p2.x - p0.x;             /* calculate d2 */
    d2.y = p2.y - p0.y;

    /* if d1 has greater slope than d2, return 1 */
    if (d1.x * d2.y > d1.y * d2.x)
        return(1);

    /* else if d2 has greater slope than d1, return -1 */
    if (d1.x * d2.y < d1.y * d2.x)
        return(-1);

    /* else (slopes are equal) if p1 and p2 are on opposite sides */
    /* of p0, return -1 */
    if ((d1.x * d2.x < 0) || (d1.y * d2.y < 0))
        return(-1);

    /* else (p1 and p2 lie in same direction from p0) if p2 is */
    /* farther from p0 than p1, return 1 */
    if ((d1.x*d1.x + d1.y*d1.y) < (d2.x*d2.x + d2.y*d2.y))
        return(1);

    /* else (p1 and p2 are same point) return 0 */
    return(0);
}


/**
 * This function determines if the two given line segments intersect, and
 * returns 1 iff they do.  Note that an endpoint touching is considered to
 * be an intersection.
 *
 * @param l1 first line
 * @param l2 second line
 *
 * @return 0 if line segments do NOT intersect
 */
gint GP_Intersect( Line l1, Line l2 )
{
    /* return 1 (True) if endpoints of l1 fall on opposite sides of l2, and
       endpoints of l2 fall on opposite sides of l1 */
    return((CCW(l1.p1, l1.p2, l2.p1) * CCW(l1.p1, l1.p2, l2.p2)) <= 0)
    && ((CCW(l2.p1, l2.p2, l1.p1) * CCW(l2.p1, l2.p2, l1.p2)) <= 0);
}


/**
 * This function determines whether a point t is inside a polygon made up of n
 * points in p.  It does this by constructing a horizontal line from t to
 * positive "infinity" (defined as outside the bounds of any possible polygon,
 * in this system).  It then examines each line segment in the polygon to
 * determine how many crossings there are.  An odd number of crossings
 * indicates that the point is inside the polygon, an even number of crossings
 * indicates that the point is outside the polygon.  Because crossings through
 * the endpoint of a segment can have ambiguous results, sets of consecutive
 * segments that cross the line are treated as a special case.
 *
 * @param t a point
 * @param p another point
 * @param n number
 *
 * @return 0 if point is not inside polygon
 */
gint GP_InsidePolygon( Point t, Point *p, gint n )
{
    gint i;               /* counter */
    gint j       = -1;    /* counter */
    gint count   = 0;     /* number of crossings */
    Line lt, lp;          /* line segments being checked - lt is point to
                              infinity segment, and lp is polygon side segment */
    Point *set;             /* temporary copy of polygon points */
    gint lastOkay;        /* previous point not on line */
    gint didSkip = False; /* flag noting vertex was skipped */
    gint onLine  = False; /* set to 1 iff point falls on line of polygon */

    if (n < 3)
    {
        return(False);
    }

    /* check for point on boundary of polygon -- this case can cause an */
    /* exception in the main logic in some wierd cases */
    for (i = 0; i < n; i++)
    {
        if (CCW(p[i], p[(i + 1) % n], t) == 0)
        {
            return(True);
        }
    }

    /* we want to start on a point NOT on the line to infinity */
    lastOkay = (p[n-1].y != t.y) || (p[n-1].x < t.x);
    for (i = 0; i < n; i++)
    {
        if (((p[i].y != t.y) || (p[i].x < t.x)) && lastOkay)
        {
            j = i;                 /* set j to index of point if one is found */
            i = n;                 /* and stop looking */
        }
        else
        {
            lastOkay = (p[i].y != t.y) || (p[i].x < t.x);
        }
    }

    /* if we didn't find a point... strange case, */
    /* polygon is subset of line from point to max X */
    if ((j == -1 ) || ((p[j].y == t.y) && (p[j].x >= t.x)))
    {
        /* look to see if polygon encompasses t */
        for (i = 0; i < n; i++)
        {
            if (p[i].x == t.x)                  /* if it does, then return 1 */
            {
                return(True);
            }
        }
        return(False);                             /* else, return 0 */
    }

    /* allocate space for temp points */
    set = (Point *) MH_Calloc(sizeof(Point) * (n+2), __FILE__, __LINE__);
    /* put polygon points in temporary array, */
    /* starting with the selected starting point */
    /* at the index of 1, and wrapping around as needed */
    for (i = 1; i <= n; i++)
    {
        set[i] = p[j++];
        if (j >= n)
        {
            j = 0;
        }
    }
    set[0]   = set[n];     /* repeat the first and last points at the */
    set[n+1] = set[1];     /* ends of the array to avoid special end cases */

    j       = 0;           /* set j (index of last polygon vertex checked) */
    lt.p1.x = t.x;         /* set up lt - line segment from point to infinity */
    lt.p1.y = lt.p2.y = t.y;
    lt.p2.x = 5000;

    /* for each vertex in the polygon */
    for (i = 1; i <= n; i++)
    {
        /* if the point falls on the line segment lt, */
        /* then skip over the point */
        if ((set[i].y == t.y) && (set[i].x >= t.x))
        {
            didSkip = True;
        }
        else  /* otherwise */
        {
            /* get polygon line segment starting on point */
            /* and ending on last checked point           */
            lp.p1 = set[i];
            lp.p2 = set[j];

            /* update last chcked point to current point */
            j = i;
            if (didSkip)
            {
                /* if a point has been skipped, then we know */
                /* that the segments between lp.p1 and lp.p2 */
                /* have intersected lt at least once, so we  */
                /* treat it as an intersection if lp.p1 and  */
                /* lp.p2 are on opposite sides of lt         */
                if (CCW(lt.p1, lt.p2 ,lp.p1) != CCW(lt.p1, lt.p2, lp.p2))
                {
                    if (CCW(lp.p1, lp.p2, lt.p1) == 0)
                    {
                        onLine = True;
                    }
                    count++;
                }
            }
            /* otherwise, look for a simple intersection */
            else if (GP_Intersect(lp,lt))
            {
                if (CCW(lp.p1, lp.p2, lt.p1) == 0)
                {
                    onLine = True;
                }
                count++;
            }
            didSkip = False;
        }
    }
    MH_Free(set);
    return((count & 1) || onLine);    /* and return if crossing count was odd */
}


/**
 * This function determines whether two line segments intersect in a line
 * segment, and returns the intersection segment if so.
 *
 * @param l1 first line
 * @param l2 second line
 * @param l output to this line
 *
 * @return 0 if segments do NOT intersect in a line segment
 */
gint GP_IntersectLine(Line l1, Line l2, Line *l)
{
    /* if the segments do not intersect at all, then return 0 */
    if (!GP_Intersect(l1,l2))
    {
        return(False);
    }

    /* if both lines are vertical */
    if ((l1.p1.x == l1.p2.x) && (l2.p1.x == l2.p2.x))
    {
        /* set x-coords of intersection and determine y-coords */
        l->p1.x = l1.p1.x;
        l->p2.x = l1.p1.x;
        l->p1.y = MAX(MIN(l1.p1.y,l1.p2.y), MIN(l2.p1.y,l2.p2.y));
        l->p2.y = MIN(MAX(l1.p1.y,l1.p2.y), MAX(l2.p1.y,l2.p2.y));

        /* if intersection is not point, return 1 */
        if (l->p1.y != l->p2.y)
        {
            return(True);
        }
    }
    /* else if neither line is vertical */
    else if ((l1.p1.x != l1.p2.x) && (l2.p1.x != l2.p2.x))
    {
        /* if lines have the same slope */
        if ((l1.p2.y-l1.p1.y)*(l2.p2.x-l2.p1.x)
            == (l2.p2.y-l2.p1.y)*(l1.p2.x-l1.p1.x))
        {
            /* determine maximum x-coord shared by segments */
            l->p1.x = MAX(MIN(l1.p1.x,l1.p2.x),MIN(l2.p1.x,l2.p2.x));
            /* determine corresponding y-coord of maximum x-coord */
            if (l->p1.x == l1.p1.x)
            {
                l->p1.y = l1.p1.y;
            }
            else if (l->p1.x == l1.p2.x)
            {
                l->p1.y = l1.p2.y;
            }
            else if (l->p1.x == l2.p1.x)
            {
                l->p1.y = l2.p1.y;
            }
            else if (l->p1.x == l2.p2.x)
            {
                l->p1.y = l2.p2.y;
            }
            else
            {
            }

            /* determine minimum x-coord shared by segments */
            l->p2.x = MIN(MAX(l1.p1.x,l1.p2.x),MAX(l2.p1.x,l2.p2.x));
            /* determine corresponding y-coord of minimum x-coord */
            if (l->p2.x == l1.p1.x)
            {
                l->p2.y = l1.p1.y;
            }
            else if (l->p2.x == l1.p2.x)
            {
                l->p2.y = l1.p2.y;
            }
            else if (l->p2.x == l2.p1.x)
            {
                l->p2.y = l2.p1.y;
            }
            else if (l->p2.x == l2.p2.x)
            {
                l->p2.y = l2.p2.y;
            }
            else
            {
            }

            if (l->p1.x != l->p2.x)    /* if intersection is not a point */
            {
                return True;          /* return True */
            }
        }
    }
    return(False); /* return False, because segment intersection was not found */
}


/**
 * This function determines whether the line segments l1 and l2 intersect in a
 * point, and determines the point of intersection.
 *
 * @param l1 first line
 * @param l2 second line
 * @param p output to this point
 *
 * @return 0 if line segments do not intersect in a point
 */
gint GP_IntersectPoint( Line l1, Line l2, Point *p )
{
    Line l;              /* temporary line */
    gdouble s1, s2;       /* slopes of l1 and l2 */
    gdouble i1, i2;       /* y-axis intercepts of lines encompassing l1 and l2 */

    /* if line segments do not intersect, return False */
    if (!GP_Intersect(l1,l2))
    {
        return(False);
    }

    /* if they do intersect, but in a line segment, return False */
    if (GP_IntersectLine(l1,l2,&l))
    {
        return(False);
    }

    /* if the first segment is vertical */
    if (l1.p1.x == l1.p2.x)
    {
        /* x-coordinate of point must be x-coordinate of vertical line */
        p->x = l1.p1.x;

        /* if l2 is vertical, then intersection is touching endpoints */
        if (l2.p1.x == l2.p2.x)
        {
            /* if l1.p1 is touching endpoint */
            if ((l1.p1.x == l2.p1.x) || (l1.p1.x == l2.p2.x))
            {
                p->y = l1.p1.y;     /* then y-coordinate is from l1.p1 */
            }
            else
            {
                p->y = l1.p2.y;     /* else y-coordinate is from l1.p2 */
            }
        }
        else    /* if l2 is not vertical */
        {
            /* get the slope and the intercept of l2*/
            s2   = (gdouble) (l2.p2.y - l2.p1.y) / (l2.p2.x - l2.p1.x);
            i2   = l2.p1.y - s2 * l2.p1.x;

            /* calculate the y-coordinate of the intersection pt */
            p->y = (gint)(s2 * p->x + i2 + 0.5);
        }
    }
    /* else if the second segment is vertical */
    else if (l2.p1.x == l2.p2.x)
    {
        /*calculate the slope and the y-axis intercept of l1 */
        s1   = (gdouble) (l1.p2.y - l1.p1.y) / (l1.p2.x - l1.p1.x);
        i1   = l1.p1.y - s1 * l1.p1.x;

        /* x-coord of intersection is x-coord of vertical line */
        p->x = l2.p1.x;

        /* calculate corresponding y-coord */
        p->y = (gint)(s1 * p->x + i1 + 0.5);
    }
    /* else if neither line is vertical */
    else
    {
        /* get slope and y-axis intercept of l1 */
        s1   = (l1.p2.y - l1.p1.y) / (gdouble) (l1.p2.x - l1.p1.x);
        i1   = l1.p1.y - s1 * l1.p1.x;

        /* get slope and y-axis intercept of l2 */
        s2   = (l2.p2.y - l2.p1.y) / (gdouble) (l2.p2.x - l2.p1.x);
        i2   = l2.p1.y - s2 * l2.p1.x;

        if ((s1 == s2) && (i1 == i2))
        {
            if ((l1.p1.x == l2.p1.x) || (l1.p1.x == l2.p2.x))
            {
                p->x = l1.p1.x;
                p->y = l1.p2.y;
            }
            else
            {
                p->x = l2.p1.x;
                p->y = l2.p2.y;
            }
        }
        else
        {
            /* calculate intersection point */
            p->x = (gint)((i2 - i1) / (s1 - s2) + 0.5);
            p->y = (gint)(s1 * p->x + i1 + 0.5);
        }
    }
    l.p1.x = MAX( MIN(l1.p1.x, l1.p2.x), MIN(l2.p1.x, l2.p2.x));
    l.p2.x = MIN( MAX(l1.p1.x, l1.p2.x), MAX(l2.p1.x, l2.p2.x));
    l.p1.y = MAX( MIN(l1.p1.y, l1.p2.y), MIN(l2.p1.y, l2.p2.y));
    l.p2.y = MIN( MAX(l1.p1.y, l1.p2.y), MAX(l2.p1.y, l2.p2.y));
    p->x = MIN( MAX(p->x, l.p1.x), l.p2.x);
    p->y = MIN( MAX(p->y, l.p1.y), l.p2.y);
    return(True);         /* return True, because point intersection exists */
}
