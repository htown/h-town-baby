/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <locale.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gthread.h>
#include <glib/gprintf.h>

#ifdef G_OS_WIN32
    #include <crtdbg.h>
    #include <windows.h>
#endif

#define PRIVATE
#include "IamMain.h"
#undef PRIVATE

#ifndef _EXT_PARTNERS
#include "UEFramework/CFrameworkClient.h"
#endif
#include "AntMan.h"
#include "ConfigParser.h"
#include "Conversions.h"
#include "EventDialog.h"
#include "FontHandler.h"
#include "IspClient.h"
#include "IspSymbols.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PredictDialog.h"
#include "PredictDataHandler.h"
#include "RealtimeDialog.h"
#include "SolarDraw.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/IamMain.c 271 2017-02-20 20:59:17Z dpham1@ndc.nasa.gov $");

#ifndef _EXT_PARTNERS
extern FrameworkClient *GTKgetSecuredInstance();
#endif

/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* use or don't use ISP */
static gboolean thisISP     = True;

/* start realtime display */
gboolean thisRealtime= False;

/* start predict display, if neither realtime or predict set, this is the default */
gboolean thisPredict = False;

gboolean bRet = False;
/*************************************************************************/

/** \mainpage
 * The International Space Station (ISS) Antenna Management (IAM) application is used to display the
 * telemetry from two SBand radios and one KuBand radio.  It provides a prediction display capability
 * that permits the user to view TDRS tracking, solar tracking, and articulated structure predictions
 * up to six days in advance of Real-time operations.  This information is used by the Communications
 * and Tracking Officer (CATO) in the Mission Control Center (MCC) to support antenna sub-system management.
 * The Real-time capability provides indicators for receiver signal strength, lock indications, tracking
 * mode, active masks, subsystem temperature monitoring, and Tracking and Data Relay Satellite (TDRS)
 * targeting.  As each scheduled TDRS event becomes visible, IAM searches for the associated prediction
 * in the database and displays it on the Real-time display to permit the user to forecast imminent
 * dynamic structural blockage using joint angle telemetry.  The IAM interpretive graphics capabilities
 * and automated database manipulations permit the user to view a graphical depiction of three dynamic,
 * time-dependent functions at-a-glance.  IAM empowers the CATO team to make Real-time calls about
 * communications outages and to make recommendations about how to manage them.
 *
 * \cond
 *
 * @param argc count of arguments
 * @param argv the arguments
 *
 * @return 0
 * \endcond
 */
gint main(gint argc, gchar **argv)
{
    gchar iam_main_iteration_cycles[32] = {0};
    SymbolRec *KuTRC1;
    SymbolRec *KuTRC2;
    gint myCounter = 0;
    gchar message[256];
    gchar *cfg;
    gchar *filename;

    g_thread_init(NULL);

#ifndef _EXT_PARTNERS
    FrameworkClient * UEfc = GTKgetSecuredInstance();
#endif

#ifdef G_OS_WIN32
    _CrtSetDbgFlag (_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif


    /* initialize the toolkit */
    bRet = gtk_init_check(&argc, &argv);
    if (bRet == False)
    {
        /* output error message and exit */
        g_critical("Cannot open the graphics toolkit");
        AM_Exit(1);
    }

    g_set_application_name ("iam");

    /* sends logging messages to file */
    /* NOTE: if for any reason you want messages output to */
    /* the console/xterm, then just #ifdef this call */
    CP_SetLogHandler("iam.log",
                     (GLogLevelFlags)(G_LOG_LEVEL_WARNING |
                                      G_LOG_LEVEL_MESSAGE |
                                      G_LOG_LEVEL_INFO |
                                      G_LOG_LEVEL_DEBUG));

    /* process command line args */
    /* do this before initializing the config-parser, why you ask, well */
    /* the arg processor, needs to set the config file location if its */
    /* passed in via the command-line arg; otherwise the config-parser */
    /* will use the environment variable setting. Also the arg-processor */
    /* needs to ask the config-parser for the start-up app if not */
    /* provided on the command line. this is the old chicken and egg thing */
    processArgs(&argc, &argv);

    /* before proceeding, verify the config file can be loaded */
    if (CP_Constructor() == False)
    {
        cfg = CP_GetConfigFilename();
        if (cfg == NULL)
        {
            g_stpcpy(message,
                     "IAM config file not defined!\n\n"
                     "Either specify on the command line:\n"
                     "-config \"\\absolute path\\iam.xml\"\n"
                     "or\n"
                     "set the environment variable IAM_CONFIG_FILE!\n\n"
                     "Aborting...");
        }
        else
        {
            if (CP_IsConfigFileValid() == FALSE)
            {
                g_sprintf(message,
                        "The config file specified is not valid!\n\n"
                        "Config File Used:\n%s\n\n"
                        "Verify this file is specified correctly.\n"
                        "Aborting...",
                        cfg);
            }
        }

        g_critical(message);
        ErrorMessage(NULL, "Error!", message);

        AM_Exit(1);
    }

    /* load the resource file */
    filename = AM_GetRCFile(IAM_GTKRC);
    gtk_rc_parse(filename);
    MH_Free(filename);

    /* load a file used for changing the font size of resize requests */
    AM_AddRCFile(IAM_GTKRC_REALTIME_FONT_SIZE);
    FH_AddRCFontStyle(IAM_GTKRC_REALTIME_FONT_SIZE, DEFAULT_FONT_SIZE, "Realtime");

    AM_AddRCFile(IAM_GTKRC_PREDICT_FONT_SIZE);
    FH_AddRCFontStyle(IAM_GTKRC_PREDICT_FONT_SIZE, DEFAULT_FONT_SIZE, "Predict");

    AM_AddRCFile(IAM_GTKRC_REALTIME_DATA_FONT_SIZE);
    FH_AddRCFontStyle(IAM_GTKRC_REALTIME_DATA_FONT_SIZE, M_FONT_SIZE, "Realtime-Data");

    /* initialize colors */
    CH_Constructor();

    /* initialize the static masks */
    /* this must be performed prior to the creation of the dialogs */
    /* otherwise the dialogs will not render the masks correctly */
    MF_Constructor();
    LoadStaticBiasMats();

    /* Connect iam to ISP? */
    if (thisISP == True)
    {
        IC_StartIsp(argc, argv);

        // Now wait for ISP to return values before proceeding.
        KuTRC1 = IS_GetSymbol(IS_KuTRC1);
        KuTRC2 = IS_GetSymbol(IS_KuTRC2);

        if(KuTRC1 != NULL && KuTRC2 != NULL)
        {
            // Wait for ISP values to populate.  If it hasn't populated by iteration X then 
            // just move on with initialization.  Testing shows that values typically take 2000
            // iterations or less to show up, but can take up to 15,000 iterations.  We don't want 
            // to wait forever.  Use the value in IAM_MAIN_INTERATION_CYCLES or default to 50,000 
            // if that term isn't set.
            // changed to 50,000 for mcc21

            if (CP_GetConfigData(IAM_MAIN_ITERATION_CYCLES, iam_main_iteration_cycles) == False)
            {
                // IAM_MAIN_ITERATION_CYCLES not found.  Use 50,000 by default.
                while ((KuTRC1->value == INITIAL_VALUE || KuTRC2->value == INITIAL_VALUE) && myCounter < 50000)
                {
                    gtk_main_iteration_do(FALSE);
                    myCounter++;
                }
            }
            else
            {
                // Use the value found in IAM_MAIN_ITERATION_CYCLES.
                int x = atoi(iam_main_iteration_cycles);
                while ((KuTRC1->value == INITIAL_VALUE || KuTRC2->value == INITIAL_VALUE) && myCounter < x)
                {
                    gtk_main_iteration_do(FALSE);
                    myCounter++;
                }
            }
        }
    }
    else
    {
        InfoMessage(NULL, "No ISP", "ISP data not requested.\n\n(noisp option specified)");
    }

    /* initialize the obs mask files */
    if (OM_Constructor() == False)
    {
        g_critical("Failed to load the obscuration masks");
        ErrorMessage(NULL, "Error!", "Failed to load the obscuration masks!\nAborting...");
        AM_Exit(1);
    }

    /* should we start the realtime dialog */
    /* realtime display is the default display to open */
    if (thisRealtime == True ||
        (thisPredict == False && thisRealtime == False))
    {
        /* create the realtime dialog */
        /* this will define data structures needed by isp */
        bRet = RTD_Create();
        if (bRet == False)
        {
            g_critical("Cannot create Realtime Dialog");
            ErrorMessage(NULL, "Error!", "Cannot create Realtime Dialog!\nAborting...");
            AM_Exit(1);
        }
        RTD_Open();
    }

    /* should we open the predict dialog */
    if (thisPredict == True)
    {
        /* create the predict dialog */
        bRet = PD_Create();
        if (bRet == False)
        {
            g_critical("Cannot create Predict Dialog");
            ErrorMessage(NULL, "Error!", "Cannot create Predict Dialog!\nAborting...");
            AM_Exit(1);
        }
        PD_Open();
    }

#ifndef _EXT_PARTNERS
    FrameworkClientRestorationReady(UEfc);
#endif

    /* let's get it on */
    gtk_main();

    return(0);
}

#ifdef G_OS_WIN32
/**
 * Used to fake out windows into thinking this is a windows gui app
 *
 * @param instance
 * @param prev_instance
 * @param command_line the command line args as a single string
 * @param show_command
 *
 * @return True or False
 */
static void setargv(gint *argcPtr, gchar ***argvPtr);
extern gint main(gint argc, gchar **argv);
gint WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, gchar *command_line, gint show_command)
{
    gint    argc;
    gchar** argv;
    gchar filename[MAX_PATH];
    gint result = -1;

    setlocale(LC_ALL, "C");
    setargv(&argc,  &argv);

    if (argv != NULL)
    {
        GetModuleFileName(NULL, filename, sizeof(filename));
        argv[0] = filename;    // call the user specified main function

        /* not returning from here; for completeness */
        /* argv will be lost */
        result = main(argc, argv);
    }
    return result;
}

/**
 * setargv --
 *
 *  Parse the Windows command line string into argc/argv.  Done here
 *  because we don't trust the builtin argument parser in crt0.
 *  Windows applications are responsible for breaking their command
 *  line into arguments.
 *
 *  2N backslashes + quote -> N backslashes + begin quoted string
 *  2N + 1 backslashes + quote -> literal
 *  N backslashes + non-quote -> literal
 *  quote + quote in a quoted string -> single quote
 *  quote + quote not in quoted string -> empty string
 *  quote -> begin quoted string
 *
 * Results:
 *  Fills argcPtr with the number of arguments and argvPtr with the
 *  array of arguments.
 *
 * Side effects:
 *  Memory allocated.
 *
 * @param argcPtr filled with the number of argument strings
 * @param argcvPtr filled with argument strings (malloc'd)
 */
static void setargv(gint *argcPtr, gchar ***argvPtr)
{
    gchar *cmdLine, *p, *arg, *argSpace;
    gchar **argv = NULL;
    gint argc = 0, size, inquote, copy, slashes;

    cmdLine = GetCommandLine(); /* INTL: BUG */

    /*
     * Precompute an overly pessimistic guess at the number of arguments
     * in the command line by counting non-space spans.
     */

    size = 2;
    for (p = cmdLine; *p != '\0'; p++)
    {
        if ((*p == ' ') || (*p == '\t'))
        {  /* INTL: ISO space. */
            size++;
            while ((*p == ' ') || (*p == '\t'))
            { /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }
        }
    }

    /* get memory for arguments */
    argSpace = (gchar *) malloc((unsigned) (size * sizeof(gchar *) + strlen(cmdLine) + 1));
    if (argSpace != NULL)
    {
        /* start moving the windows arguments into an argv structure */
        argv = (gchar **) argSpace;
        argSpace += size * sizeof(gchar *);
        size--;

        p = cmdLine;
        for (argc = 0; argc < size; argc++)
        {
            argv[argc] = arg = argSpace;
            while ((*p == ' ') || (*p == '\t'))
            {   /* INTL: ISO space. */
                p++;
            }
            if (*p == '\0')
            {
                break;
            }

            inquote = 0;
            slashes = 0;
            while (1)
            {
                copy = 1;
                while (*p == '\\')
                {
                    slashes++;
                    p++;
                }
                if (*p == '"')
                {
                    if ((slashes & 1) == 0)
                    {
                        copy = 0;
                        if ((inquote) && (p[1] == '"'))
                        {
                            p++;
                            copy = 1;
                        }
                        else
                        {
                            inquote = !inquote;
                        }
                    }
                    slashes >>= 1;
                }

                while (slashes)
                {
                    *arg = '\\';
                    arg++;
                    slashes--;
                }

                if ((*p == '\0')
                    || (!inquote && ((*p == ' ') || (*p == '\t'))))
                { /* INTL: ISO space. */
                    break;
                }
                if (copy != 0)
                {
                    *arg = *p;
                    arg++;
                }
                p++;
            }
            *arg = '\0';
            argSpace = arg + 1;
        }
        argv[argc] = NULL;
    }

    *argcPtr = argc;
    *argvPtr = argv;
}
#endif /* G_OS_WIN32 */

/**
 * This method processes IAM command line args. After processing the
 * arg count (argc) and arg list (argv) is modified to reflect the
 * arguments processed. Unknown argument (ISP args) are ignored and
 * left intact for later processing.
 *
 * Supported flags:
 * noisp - indicates if isp should not be used (defaults to use)
 * realtime - start realtime display
 * predict - start predict display (default display to show)
 * config - location of iam.xml
 * manager - manager mode - CATO is manager
 *
 * @param argc count of arguments
 * @param **argv argument list
 */
static void processArgs(gint *argc, gchar ***argv)
{
    static gchar *noisp = NULL;
    static gchar *realtime = NULL;
    static gchar *predict = NULL;
    static gchar *config = NULL;
    static gchar *manager = NULL;

    gint c;

    static GOptionEntry entries[] =
    {
        {"noisp", 'n', 0, G_OPTION_ARG_NONE, &noisp, "ISP not used", NULL},
        {"realtime", 'r', 0, G_OPTION_ARG_NONE, &realtime, "Show Realtime Display", NULL},
        {"predict", 'd', 0, G_OPTION_ARG_NONE, &predict, "Show Predict Display", NULL},
        {"config", 'c', 0, G_OPTION_ARG_FILENAME, &config, "Location and name of configuration file", NULL},
        {"manager", 'g', 0, G_OPTION_ARG_NONE, &manager, "CATO is manager", NULL},
        /* DO NOT ADD THE ISP ARGS, THE OPTION PARSER WILL REMOVE THEM !!! */
        { NULL }
    };

    /* output the arguments before processing */
    g_message("main.processArgs: arg count=%d",*argc);
    for (c=0; c<*argc; c++)
    {
        g_message("main.processArgs: argv[%d]=%s",c, (*argv)[c]);
    }

    GError *error = NULL;
    GOptionContext *context = g_option_context_new("- ISS Antennae Manager");
    g_option_context_add_main_entries(context, entries, NULL);
    g_option_context_add_group(context, gtk_get_option_group(TRUE));
    g_option_context_set_ignore_unknown_options(context, True);

    /* process the args, after finishing, the argc/argv will be modified */
    if ( ! g_option_context_parse(context, argc, argv, &error))
    {
        g_message("%s",error->message);
    }
    g_option_context_free(context);

    if (noisp != NULL)
    {
        thisISP = False;
    }

    if (realtime != NULL)
    {
        thisRealtime = True;
    }

    if (predict != NULL)
    {
        thisPredict = True;
    }

    if (config != NULL)
    {
        CP_SetConfigFilename(config);
    }

    if (manager != NULL)
    {
        AM_SetAppMode(MANAGER);
    }

    /*
    ** all done, now see if the realtime or predict was set
    ** if neither was selected, then see if the config file has
    ** the startup dialog specified.
    */
#define REALTIME_DISPLAY    "realtime"
#define PREDICT_DISPLAY     "predict"
    if (thisRealtime == False && thisPredict == False)
    {
        gchar data[32] = "";
        if (CP_GetConfigData(IAM_START_DIALOG, data) == True)
        {
            if (g_ascii_strcasecmp(REALTIME_DISPLAY, data) == 0)
            {
                thisRealtime = True;
            }

            else if (g_ascii_strcasecmp(PREDICT_DISPLAY, data) == 0)
            {
                thisPredict = True;
            }

            else
            {
                thisRealtime = True;
            }

            g_message("main.processArgs: IAM_START_DIALOG config variable=%s\n", data);
        }
    }
}
