/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/timeb.h>
#include <time.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

#include <expat.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "ConfigParser.h"
#undef PRIVATE

#include "AntMan.h"
#include "IspHandler.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/ConfigParser.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* hash table used for quick lookup */
static GHashTable       *thisXmlConfigTable = NULL;

/* used to hold pointer to the current working copy */
static ConfigData       *thisCurrentConfigData = NULL;

/* ptr array used for sequential output */
static GPtrArray        *thisConfigArray    = NULL;
static GPtrArray        *thisXmlConfigArray = NULL;

/**
 * name of config file (absolute path)
 * if not set, then use IAM_CONFIG_FILE
 */
static gchar            thisConfigFilename[MAXPATHLEN]= {0};

/**
 * location of the config directory
 */
static gchar            *thisIamConfigDir   = NULL;

/* logging/print file output */
static gboolean         thisLogEnabled      = False;
static FILE             *thisfp             = NULL;
static gchar            *thisLogFilename    = NULL;
static GLogLevelFlags   thisLogFlags        = (GLogLevelFlags) (G_LOG_LEVEL_ERROR |
                                                                G_LOG_LEVEL_CRITICAL);
#define BUFFER_SIZE 8192
/*************************************************************************/

/**
 * The constructor reads into memory the iam.xml file. The memory
 * is parsed by the "get" functions. If the xml file is not
 * found, then the constructor returns False.
 *
 * @return True if everything ok, otherwise False
 */
gboolean CP_Constructor(void)
{
    gboolean rc = False;
    gchar *fname;
    struct stat buffer;

    fname = getConfigFilename();
    if (fname != NULL)
    {
        if (stat(fname, &buffer) == 0)
        {
            /* create lookup hashtable */
            thisXmlConfigTable = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                       deleteKey,
                                                       deleteValue);
            thisXmlConfigArray = g_ptr_array_new();

            buildXmlConfiguration(fname);
            rc = True;
        }
    }

    return rc;
}

/**
 * Removes allocated memory
 */
void CP_Destructor(void)
{
    if (thisIamConfigDir != NULL)
    {
        MH_Free(thisIamConfigDir);
    }

    if (thisXmlConfigTable != NULL)
    {
        g_message("Removing thisXmlConfigTable hash");
        g_hash_table_foreach_remove( thisXmlConfigTable, hashTableForeach, NULL );
    }

    if (thisXmlConfigArray != NULL)
    {
        g_ptr_array_foreach( thisXmlConfigArray, deleteArrayData, NULL);
        g_ptr_array_free( thisXmlConfigArray, False );
    }

    if (thisConfigArray != NULL)
    {
        g_ptr_array_free(thisConfigArray, False);
    }

    if (thisLogFilename != NULL)
    {
        g_free(thisLogFilename);
    }
}

/**
 * This method sets the name of the configuration filename
 *
 * @param configFilename configuration filename
 */
void CP_SetConfigFilename(gchar *configFilename)
{
    g_stpcpy(thisConfigFilename, configFilename);
}

/**
 * Returns the configFilename
 *
 * @return configuration filename or NULL if not specified
 */
gchar *CP_GetConfigFilename(void)
{
    return (strlen(thisConfigFilename) > 0 ? thisConfigFilename : NULL);
}

/**
 * Tries to make some kind of determination that
 * the config file is valid or not. If its defined
 * and can be opened, then it is assumed to be valid.
 * That doesn't mean the contents is correct.
 *
 * @return True or False
 */
gboolean CP_IsConfigFileValid(void)
{
    FILE *fp;

    if (thisConfigFilename != NULL && strlen(thisConfigFilename) > 0)
    {
        fp = fopen(thisConfigFilename, "r");
        if (fp != NULL)
        {
            fclose(fp);
            return True;
        }
    }

    return False;
}

/**
 * This function removes all leading and trailing blanks from a string
 *
 * @param str string to trim
 */
void CP_StringTrim(gchar *str)
{
    if (str == NULL)
    {
        return;
    }

    /* chug and chomp the string */
    str = g_strchug(str);
    str = g_strchomp(str);
}

/**
 * Gets the just the name part of the file, the
 * extension with the dot is removed.
 *
 * @param filename name that has root
 *
 * @return root name
 */
gchar *CP_GetRootName(gchar *filename)
{
    gchar *new_name = NULL;
    gchar *basename = g_path_get_basename(filename);
    gchar *str = g_strrstr(basename, ".");
    *str = '\0';
    new_name = g_strdup(basename);
    g_free(basename);

    return new_name;
}

/**
 * This function searches the IAM config file for the specified key,
 * and if found, returns the value for that key. If the data found has an
 * '@' sign or a '$', those keyword values are expanded and inserted into
 * the output buffer. The @{keyword} is treated as a configuration value.
 * The ${variable} is treated as an environment variable.
 *
 * @param key the keyword to search for
 * @param data output storage, should be large enough to hold results.
 *
 * @return True if key found, otherwise False
 */
gboolean CP_GetConfigData(gchar *key, gchar *data)
{
    ConfigData *config_data;
    gboolean found = False;

    if (thisXmlConfigTable != NULL)
    {
        config_data = (ConfigData *)g_hash_table_lookup( thisXmlConfigTable, key );
        if (config_data != NULL)
        {
            found = True;
            g_stpcpy(data, expandValue(config_data->value));
        }
    }

    return found;
}

/**
 * Finds the config entry for the given key. When found
 * the old value is freed, and a the new value is loaded.
 *
 * @param key hash key
 * @param value hash value
 *
 * @return True if found and loaded, otherwise False
 */
gboolean CP_UpdateConfigValue(gchar *key, gchar *value)
{
    ConfigData *config_data;
    gboolean found = False;

    updateHash(key, value);
    updateArray(key, value);

    /* search the hash table for the key/value */
    if (thisXmlConfigTable != NULL)
    {
        config_data = (ConfigData *)g_hash_table_lookup(thisXmlConfigTable, key);
        if (config_data != NULL)
        {
            g_free(config_data->value);
            config_data->value = g_strdup(value);
            found = True;
        }
        else
        {
            /* key not found; add to hashtable and ptr array */
            config_data = (ConfigData *)MH_Calloc(sizeof(ConfigData), __FILE__, __LINE__);
            config_data->value = g_strdup(value);
            config_data->key = g_strdup(key);
            config_data->comment = NULL;

            g_hash_table_insert(thisXmlConfigTable, config_data->key, config_data);
            g_ptr_array_add(thisXmlConfigArray, config_data);
        }
    }

    return found;
}

/**
 * Updates the hash table entry for the given key and value.
 *
 * @param key
 * @param value
 */
static void updateHash(gchar *key, gchar *value)
{
    /* search the hash table for the key/value */
    if (thisXmlConfigTable != NULL)
    {
        ConfigData *config_data = (ConfigData *)g_hash_table_lookup(thisXmlConfigTable, key);
        if (config_data != NULL)
        {
            g_free(config_data->value);
            config_data->value = g_strdup(value);
        }
        else
        {
            /* key not found; add to hashtable */
            config_data = (ConfigData *)MH_Calloc(sizeof(ConfigData), __FILE__, __LINE__);
            config_data->value = g_strdup(value);
            config_data->key = g_strdup(key);
            config_data->comment = NULL;

            g_hash_table_insert(thisXmlConfigTable, config_data->key, config_data);
        }
    }
}

/**
 * Updates the pointer array entry with the given key and value.
 *
 * @param key
 * @param value
 */
static void updateArray(gchar *key, gchar *value)
{
    /* find the entry that matches the key */
    if (thisXmlConfigArray != NULL)
    {
        guint i;
        for (i=0; i<thisXmlConfigArray->len; i++)
        {
            ConfigData *config_data = (ConfigData *)g_ptr_array_index( thisXmlConfigArray, i);
            if (config_data)
            {
                /* if key matches, update the value */
                if (g_ascii_strcasecmp(config_data->key, key) == 0)
                {
                    g_free(config_data->value);
                    config_data->value = g_strdup(value);
                    break;
                }
            }
        }
    }
}

/**
 * returns the location of the configuration directory
 *
 * @return configuration directory or NULL
 */
gchar *CP_GetConfigDir(void)
{
    gchar dirpath[MAXPATHLEN];

    if (thisIamConfigDir == NULL)
    {
        if (CP_GetConfigData(IAM_CONFIG_DIR, dirpath) == 1)
        {
            thisIamConfigDir = g_strdup(dirpath);
        }
        else
        {
            thisIamConfigDir = g_strconcat(".", G_DIR_SEPARATOR_S, NULL);
        }
    }

    return g_strdup(thisIamConfigDir);
}

/**
 * Returns the value defined for ISS_CONFIGURATION. This is a special case
 * that requires searching for a fallback value if the primary value is
 * not set.
 *
 * @param data buffer to hold the keyword data
 *
 * @return True if a keyword was found, otherwise False.
 */
gboolean CP_GetISSConfigValue(gchar *data)
{
    /* get primary iss config data */
    if (CP_GetConfigData(ISS_CONFIGURATION, data) == False)
    {
        g_warning("ISS_CONFIGURAITON variable was not set!");

        /* try backup value */
        if (CP_GetConfigData(ISS_CONFIGURATION_FALLBACK, data) == False)
        {
            g_warning("ISS_CONFIGURATION_FALLBACK was not set!");
            return False;
        }
    }

    return True;
}

/**
 * This returns the location of the mask file appended to the mask directory.
 *
 * @param mask_file_spec the mask file; sband1, sband2, kuband, station
 *
 * @return absolute location of file or NULL
 */
gchar *CP_GetMaskDefFile(gchar *mask_file_spec)
{
    gchar mask_dir[MAXPATHLEN] = {0};

    /* get the directory where the masks exist */
    if (CP_GetConfigData(IAM_MASK_DIR, mask_dir) == True)
    {
        gchar iss_config[MAXPATHLEN] = {0};

        /* get the iss struct in use */
        if (CP_GetISSConfigValue(iss_config) == True)
        {
            return g_strconcat(mask_dir, G_DIR_SEPARATOR_S, iss_config, ".", mask_file_spec, NULL);
        }
    }

    return NULL;
}

/**
 * Constructs absolute path to the mask file specified by mask_file.
 *
 * @param coord_dir one of sband1, sband2, kuband, or station
 * @param mask_file the file itself
 *
 * @return mask name or NULL
 */
gchar *CP_GetMaskFile(gchar *coord_dir, gchar *mask_file)
{
    gchar mask_dir[MAXPATHLEN];

    if (CP_GetConfigData(IAM_MASK_DIR, mask_dir) == True)
    {
        return g_strconcat(mask_dir, G_DIR_SEPARATOR_S, coord_dir, G_DIR_SEPARATOR_S, mask_file, NULL);
    }

    return NULL;
}

/**
 * This function constructs a full path to the file "file", presumed to reside
 * in the directory where reports are stored.
 *
 * @param file gets report directory
 *
 * @return file path and name
 */
gchar *CP_GetReportFile(gchar *file)
{
    if (CP_GetConfigData(IAM_REPORT_DIR, file) == True)
    {
        return file;
    }

    return NULL;
}

/**
 * Returns the predict file specified by the pattern. This is used
 * by the gen_predict binary. The keyword is treated as a pattern to be
 * searched in the predict directory
 *
 * @param pattern gen predict pattern file to find
 * @param gen_predict_file buffer where output is placed
 * @param coverage_dir alternate coverage directory to use in place of
 * the directory found in the iam.xml file. Once the keyword/value is
 * return, replace the directory part with the coverage directory. If
 * the coverage directory is null, the original value is used.
 *
 * @return True if keyword found, otherwise False
 */
gboolean CP_GetGenPredictFile(gchar *pattern, gchar *gen_predict_file, gchar *coverage_dir)
{
    gchar predict_dir[MAXPATHLEN] = {0};

    gchar *basename;
    GDir *gdir = NULL;
    G_CONST_RETURN gchar *fname;
    GPatternSpec *pattern_spec;
    GError *error = NULL;
    gboolean found = False;

    /* get just the basename part */
    basename = g_path_get_basename(pattern);

    /* compile the pattern */
    pattern_spec = g_pattern_spec_new(basename);

    /* now set the predict directory location */
    predict_dir[0] = '\0';
    if (coverage_dir != NULL && coverage_dir[0] != '\0')
    {
        /* use the user provided coverage directory */
        g_stpcpy(predict_dir, coverage_dir);
    }
    else
    {
        /* use the iam.xml file directory */
        CP_GetConfigData(IAM_PREDICT_DIR, predict_dir);
    }

    /* if predict directory is valid, open it and look for the file */
    if (predict_dir[0] != '\0')
    {
        if ((gdir = g_dir_open(predict_dir, 0, &error)) != NULL)
        {
            while ((fname = g_dir_read_name(gdir)) != NULL)
            {
                /* if file found, build and return it */
                if (g_pattern_match_string(pattern_spec, fname) == True)
                {
                    g_sprintf(gen_predict_file, "%s%s%s", predict_dir, G_DIR_SEPARATOR_S, fname);
                    found = True;
                    break;
                }
            }

            g_dir_close(gdir);
        }
        else
        {
            if (error != NULL)
            {
                g_warning(error->message);
                g_error_free(error);
            }
            g_message("CP_GetGenPredictFile: %s: unable to open directory",predict_dir);
        }

        g_free(basename);
        g_pattern_spec_free(pattern_spec);
    }

    return found;
}

/**
 * This method attempts to get the location and name of the configuration
 * file. If the variable thisConfigFilename and thisDataDirectory are not set,
 * then use the IAM_CONFIG_FILE environment variable. If the variable
 * thisConfigFilename is not set, but the variable thisDataDirectory is set,
 * then try the environment variable IAM_CONFIG_FILE. If the environment
 * variable is not set then see if a file 'iam.xml' exists in the data directory.
 * If all fails then return NULL.
 *
 * @return filename or NULL
 */
static gchar *getConfigFilename(void)
{
    gchar *configFilename = NULL;

    if (strlen(thisConfigFilename) == 0)
    {
        if ((configFilename = getenv(IAM_CONFIG_FILE)) != NULL)
        {
            g_stpcpy(thisConfigFilename, configFilename);
        }
    }
    else
    {
        configFilename = thisConfigFilename;
    }

    return configFilename;
}

/**
 * Determines if the string has a '@' character in it.
 *
 * @param data string to check
 *
 * @return True or False
 */
static gboolean hasAtSign(gchar *data)
{
    return (g_strrstr(data, "@{") != NULL);
}

/**
 * Determines if the string has a '$' character in it.
 *
 * @param data string to check
 *
 * @return True or False
 */
static gboolean hasDollarSign(gchar *data)
{
    return (g_strrstr(data, "${") != NULL);
}

/**
 * This method finds the first occurrence of the '@(' sign and
 * treats the data following as a keyword that should be defined
 * in the iam config file. Once the @{keyword} is found, the
 * data found is replaces the @{keyword}.
 *
 * @param data the data with @{keyword}
 * @param search_pattern "@{" or "${"
 * @param first_part data that occurs before the search pattern or NULL
 * @param last_part data that occurs after the search pattern or NULL
 *
 * @return keyword the keyword, found between the {...} or NULL
 */
static gchar *get_keyword(gchar *data, gchar *search_pattern, gchar *first_part, gchar *last_part)
{
    gchar *keyword_value = NULL;
    gchar keyword[256];
    gchar *sptr;

    /* save any data that may come before the search pattern */
    g_stpcpy(first_part, data);

    /* get pointer to first occurrence of search pattern */
    sptr = g_strstr_len(data, (gssize)strlen(data), search_pattern);
    if (sptr != NULL)
    {
        /* terminate the first_part */
        first_part[strlen(first_part)-strlen(sptr)] = '\0';

        /* terminate at the beginning of the search pattern */
        /* move past the search pattern */
        *sptr = '\0';
        sptr = sptr+(strlen(search_pattern));

        /* copy keyword */
        g_stpcpy(keyword, sptr);

        /* find end and terminate */
        while (*sptr != '\0' && *sptr != '}')
        {
            sptr++;
        }

        /* how did we get here? If the trailing '}' was not found */
        /* then we give some slack and not worry about it */
        if (*sptr == '\0')
        {
            /* no more data at end */
            last_part[0] = '\0';
        }
        else
        {
            /* terminate the keyword */
            keyword[strlen(keyword)-strlen(sptr)] = '\0';
            keyword_value = g_strdup(keyword);

            /* get the last part if any */
            if (*(sptr+1) != '\0')
            {
                g_stpcpy(last_part, sptr+1);
            }
            else
            {
                last_part[0] = '\0';
            }
        }
    }

    return keyword_value;
}

/**
 * Concatenates the first_part, keyword_data, and last_part together, in that
 * order. If first_part or last_part is null, only the keyword_data is returned.
 *
 * @param first_part the first component to copy
 * @param keyword_data the middle string to copy
 * @param last_part the tail end
 *
 * @return new string with all parts together
 */
static gchar *concat_keyword_data(gchar *first_part, gchar *keyword_data, gchar *last_part)
{
    gchar *new_data = NULL;

    /* build new string */
    /* does the first part exist? */
    if (first_part[0] != '\0')
    {
        if (last_part[0] != '\0')
        {
            new_data = g_strconcat(first_part, keyword_data, last_part, NULL);
        }
        else
        {
            new_data = g_strconcat(first_part, keyword_data, NULL);
        }
    }
    else
    {
        if (last_part[0] != '\0')
        {
            new_data = g_strconcat(keyword_data, last_part, NULL);
        }
        else
        {
            new_data = g_strdup(keyword_data);
        }
    }

    return new_data;
}

/**
 * Sets up logging for the g_message, g_warning, g_error interfaces. In debug mode all
 * messages are output to the log. In release mode, only error or critical are output.
 *
 * @param log_domain domain to log; e'g' GLib, GModule, etc...
 * @param flags logging flags; e.g. G_LOG_LEVEL_ERROR, G_LOG_LEVEL_CRITICAL, etc...
 * @param message the message to log
 * @param user_data user data
 */
static void logToFile(const gchar *log_domain, GLogLevelFlags flags, const gchar *message, void * user_data)
{
#ifdef _DEBUG
    messageOut(message);
#else
    if (flags&thisLogFlags)
    {
        messageOut(message);
    }
#endif

}

/**
 * Sets up logging for the g_print interface. These messages are only output in debug mode.
 *
 * @param message output this message to a file
 */
static void printToFile(const gchar *message)
{
#ifdef _DEBUG
    messageOut(message);
#endif

}

/**
 * Does the actual output of the message. Opens the logging file, outputs a start message.
 * NOTE: the g_fopen, fprintf don't work - g_fopen gets link error LNK2019, and without
 * g_fopen... fprintf is not a valid function (paired).
 *
 * @param message output this message
 */
static void messageOut(const gchar *message)
{
    if (thisfp == NULL)
    {
        if (thisLogFilename == NULL)
        {
            thisLogFilename = g_strconcat(g_get_application_name(), ".log", NULL);
        }

        // can't seem to get g_fopen to link, get LNK2019
        thisfp = fopen(thisLogFilename, "w");
        if (thisfp != NULL)
        {
            loggingStart();
        }
    }

    /* by now the file pointer should be valid */
    if (thisfp == NULL)
    {
        printf("Logging Rerouted to console: %s\n",message);
        return;
    }
    else
    {
        time_t rawtime;
        struct tm *today;
        gchar ext[16];

        struct timeb timebuffer;
        ftime(&timebuffer);

        time(&rawtime);
        today = gmtime(&rawtime);
        strftime(ext, sizeof(ext), "%H:%M:%S", today);

        if (strstr(message, "\n") == NULL)
        {
            // fprintf cannot access thisfp without g_fopen
            fprintf(thisfp, "%s.%hu %s\n", ext, timebuffer.millitm, message);
        }
        else
        {
            fprintf(thisfp, "%s.%hu %s", ext, timebuffer.millitm, message);
        }
        fflush(thisfp);
    }
}

/**
 * Builds a glong formatted date: September 09, 2003 13:56:09
 *
 * @return date formatted date
 */
static gchar *getLongDate(void)
{
    GDate today;
    static gchar theDate[64];

    g_date_set_time(&today, (GTime)time(NULL));
    g_date_strftime(theDate, sizeof(theDate), "%a %b %d %H:%M:%S %Y", &today);

    return theDate;
}

/**
 * Outputs a simple start message for logging with a date tag
 */
static void loggingStart(void)
{
    gchar *theDate = getLongDate();

    thisLogEnabled = True;

    if (thisfp != NULL)
    {
        fprintf(thisfp, "**** Logging Started on %s *****\n", theDate);
    }
    else
    {
        g_printf("**** Logging Started on %s *****\n", theDate);
    }
}

/**
 * Outputs a simple stop message for logging with a date tag
 */
static void loggingStop(void)
{
    gchar *theDate = getLongDate();

    if (thisLogEnabled == True)
    {
        if (thisfp != NULL)
        {
            fprintf(thisfp, "**** Logging Stopped on %s *****\n", theDate);
        }
        else
        {
            g_printf("**** Logging Stopped on %s *****\n", theDate);
        }
    }
}

/**
 * Allows the user to enable all the logging in the executable running.
 */
void CP_SetLogLevel(void)
{
    thisLogFlags = (GLogLevelFlags)(G_LOG_LEVEL_ERROR |
                                    G_LOG_LEVEL_CRITICAL |
                                    G_LOG_LEVEL_WARNING |
                                    G_LOG_LEVEL_MESSAGE |
                                    G_LOG_LEVEL_INFO |
                                    G_LOG_LEVEL_DEBUG);
}

/**
 * Public interface for logging messages. Also sets up an exit handler for
 * outputting a stop message
 *
 * @param logFilename log messages to this file
 * @param flags use this logging flags
 */
void CP_SetLogHandler(gchar *logFilename, GLogLevelFlags flags)
{
    thisLogFilename = g_strdup(logFilename);

    /* set up log handler for different domains */
    g_log_set_handler(NULL,             (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("Gtk",            (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("Gdk",            (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("GLib",           (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("GModule",        (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("GThreads",       (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("GLib-GObject",   (GLogLevelFlags)flags, logToFile, NULL);
    g_log_set_handler("GObject",        (GLogLevelFlags)flags, logToFile, NULL);

    g_set_print_handler(printToFile);

    atexit(loggingStop);
}

/**
 * Sets up the XML parser (expat) to read and parse the iam
 * configuration file.
 *
 * @param xmlConfigFile
 */
static void buildXmlConfiguration(const gchar *xmlConfigFile)
{
    FILE *fp;

    g_message( "open file %s",xmlConfigFile );
    fp = fopen(xmlConfigFile, "r");
    if (fp != NULL)
    {
        /* get parser */
        XML_Parser parser = XML_ParserCreate(NULL);
        /* need reader for comments and data */
        XML_SetCommentHandler(parser, commentConfig);
        XML_SetElementHandler(parser, startConfig, endConfig);

        int done;
        do
        {
            char buf[BUFFER_SIZE];
            /* read a line */
            int len = (int)fread(buf, 1, BUFFER_SIZE, fp);
            done = len < sizeof(buf);
            /* parse this line */
            if (XML_Parse(parser, buf, len, done) == XML_STATUS_ERROR)
            {
                break;
            }
        } while (!done);

        /* release resources */
        XML_ParserFree(parser);
        fclose( fp );
    }
}

/**
 * Handles the processing of the comments in the xml file.
 * It is assumed that all comments precede the data entry.
 *
 * @param data
 * @param comment
 */
static void commentConfig(void *data, const gchar *comment)
{
    /* assumption: comments precede the xml declaration
       so whenever a comment is encountered its assumed
       that the next thing will be the data
    */
    addComment(comment);
}

/**
 * Handles the key and value fields in the configuration file.
 * These items are added to the comment field before inserting
 * into the hash and pointer array.
 *
 * @param data
 * @param el
 * @param attr
 */
static void startConfig(void *data, const gchar *el, const gchar **attr)
{
    if (g_strcasecmp(el, "data") == 0)
    {
        addKeyValue(attr[1], attr[3]);
    }
}

/**
 * Saves the comment into the current configuration instance.
 *
 * @param comment
 */
static void addComment(const gchar *comment)
{
    /* shouldn't happen, but just in case, check */
    if (thisCurrentConfigData != NULL)
    {
        /* release resources */
        deleteKey(thisCurrentConfigData->key);
        deleteValue(thisCurrentConfigData);
    }
    /* get new instance and save comment */
    thisCurrentConfigData = (ConfigData *)MH_Calloc( sizeof(ConfigData ), __FILE__, __LINE__ );
    thisCurrentConfigData->comment = g_strdup(comment);
}

/**
 * Saves the key and value into the current configuration
 * instance. It's possible no config exists since the comment
 * handler invokes it prior to getting here; if the key/value
 * has no comment.
 *
 * @param key
 * @param value
 */
static void addKeyValue(const gchar *key, const gchar *value)
{
    /* if configuraiton instance exists... create it */
    if (thisCurrentConfigData == NULL)
    {
        thisCurrentConfigData = (ConfigData *)MH_Calloc( sizeof(ConfigData ), __FILE__, __LINE__ );
    }

    /* save key and value */
    thisCurrentConfigData->key = g_strdup(key);
    thisCurrentConfigData->value = g_strdup(value);

    /* add to the hash and pointer array */
    addToHash(thisCurrentConfigData);
    addToArray(thisCurrentConfigData);

    /* release resources */
    deleteKey( thisCurrentConfigData->key );
    deleteValue( thisCurrentConfigData );
    thisCurrentConfigData = NULL;
}

/**
 * Creates a new configuration instance and loads into the hash
 * table.
 *
 * @param data
 */
static void addToHash(ConfigData *data)
{
    ConfigData *configData = (ConfigData *)MH_Calloc( sizeof(ConfigData ), __FILE__, __LINE__ );
    configData->comment = (data->comment?g_strdup(data->comment) : NULL);
    configData->key = g_strdup(data->key);
    configData->value = g_strdup(data->value);
    g_hash_table_insert( thisXmlConfigTable, configData->key, configData );
}

/**
 * Creates a new configuration instance and loads into the
 * pointer array.
 *
 * @param data
 */
static void addToArray(ConfigData *data)
{
    ConfigData *configData = (ConfigData *)MH_Calloc( sizeof(ConfigData ), __FILE__, __LINE__ );
    configData->comment = (data->comment?g_strdup(data->comment) : NULL);
    configData->key = g_strdup(data->key);
    configData->value = g_strdup(data->value);
    g_ptr_array_add( thisXmlConfigArray, configData );
}

/**
 * Does the end processing of xml data; currently not used.
 *
 * @param data
 * @param el
 */
static void endConfig(void *data, const gchar *el)
{
}

#define AT_SIGN 0
#define DOLLAR_SIGN 1

/**
 * Expands the imbedded values that point to either other values
 * or environment variables.
 *
 * @param value
 *
 * @return gchar*
 */
static gchar *expandValue(char *value)
{
    gchar *newValue = g_strdup(value);

    /* execute until all @ or $ are exhausted */
    gboolean done = False;
    do
    {
        /* determine if @ or $ exists */
        if (hasAtSign( newValue ) == True)
        {
            newValue = expandSymbol(newValue, "@{", AT_SIGN);
        }
        else if (hasDollarSign( newValue ) == True)
        {
            newValue = expandSymbol(newValue, "${", DOLLAR_SIGN);
        }
        else
        {
            done = True;
        }

    } while (done == False);

    return newValue;
}

/**
 * Expands the @ or $ and returns the new value.
 *
 * @param value
 * @param pattern
 * @param type
 *
 * @return gchar*
 */
static gchar *expandSymbol(gchar *value, gchar *pattern, int type)
{
    gchar *newValue = NULL;
    gchar *key;
    gchar firstPart[MAXPATHLEN];
    gchar lastPart [MAXPATHLEN];

    /* get the key from the value */
    while ((key = get_keyword(value, pattern, firstPart, lastPart)) != NULL)
    {
        gchar *keywordValue = NULL;

        /* does it have @ */
        if (type == AT_SIGN)
        {
            /* dereference the config data for this key and get the value */
            ConfigData *configData = (ConfigData *)g_hash_table_lookup( thisXmlConfigTable, key);
            keywordValue = configData->value;
        }
        else
        {
            /* get the value from the environment */
            keywordValue = (gchar *)g_getenv(key);
        }

        /* if keyword data sent back is valid, build the data */
        if (keywordValue != NULL)
        {
            /* build the new value */
            newValue = concat_keyword_data(firstPart, keywordValue, lastPart);
        }
        else
        {
            /* something went wrong, return the original string */
            newValue = g_strdup( value );
        }
        g_free(key);
    }

    return newValue;
}

/**
 * Does nothing; used only to invoke the destroy methods for the
 * key and data.
 *
 * @param key not used
 * @param value not used
 * @param user_data not used
 *
 * @return True - invokes destroy methods
 */
static gboolean hashTableForeach(void * key, void * value, void * user_data)
{
    return True;
}

/**
 * Deletes the configuration data held in the pointer array.
 *
 * @param data
 * @param user_data
 */
static void deleteArrayData(gpointer data, gpointer user_data)
{
    ConfigData *configData = (ConfigData *)data;
    deleteKey( configData->key );
    deleteValue( configData );
}

/**
 * Releases the memory associated with the hash table key
 *
 * @param data free this data
 */
static void deleteKey(void * data)
{
    g_free(data);
}

/**
 * Releases the memory associated with the hash table data
 *
 * @param data ConfigData
 */
static void deleteValue(void * data)
{
    ConfigData *config_data = (ConfigData *)data;
    if (config_data->value)
    {
        g_free(config_data->value);
    }
    if (config_data->comment)
    {
        g_free(config_data->comment);
    }

    MH_Free(config_data);
}

/**
 * Writes the contents of the pointer array to disk.
 * This method first saves the iam.xml file then
 * writes the new data to iam.xml
 */
void CP_WriteConfigFile(void)
{
    /* before writing new config file, save the old one */
    backupFile(thisConfigFilename);

    /* output contents of pointer array to iam.xml */
    writeIamConfigData();
}

/**
 * Backup a file by renaming it with today's date
 *
 * @param fname file to backup
 */
static void backupFile(gchar *fname)
{
    gchar backup_name[MAXPATHLEN];
    time_t ltime;
    struct tm *today;
    gchar ext[16];

    time(&ltime);
    today = localtime(&ltime);
    if (today != NULL)
    {
        strftime(ext, sizeof(ext), "%m%d%Y-%H%M%S", today);
        g_sprintf(backup_name, "%s.%s",fname,ext);
        rename(fname, backup_name);
    }
}

/**
 * This method outputs the configuration data found in the
 * pointer array to the iam.xml file. Before writing the
 * data, the old iam.xml is backed up using the date as
 * a file extension.
 */
static void writeIamConfigData(void)
{
    guint i;
    guint size = thisXmlConfigArray->len;
    FILE *fp;

    /* open iam config file for writing, destroys contents */
    fp = fopen(thisConfigFilename, "wb");
    if (fp != NULL)
    {
        fprintf( fp, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        fprintf( fp, "<iam-config>\n" );
        fflush(fp);
        for (i=0; i<size; i++)
        {
            ConfigData *config_data = (ConfigData *)g_ptr_array_index( thisXmlConfigArray, i);
            if (config_data)
            {
                if (config_data->comment)
                {
                    fprintf( fp, "\t<!-- %s -->\n", config_data->comment );
                }
                fprintf( fp, "\t<data key=\"%s\" value=\"%s\"/>\n", config_data->key, config_data->value );
                fflush(fp);
            }
        }
        fprintf( fp, "</iam-config>\n" );
        fclose(fp);
    }
}
