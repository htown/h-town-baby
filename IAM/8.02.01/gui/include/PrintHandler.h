/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PRINTHANDLER_H__
#define __PRINTHANDLER_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/* define file_type */
#define TEXT_FILE_TYPE      0
#define IMAGE_FILE_TYPE     1

/*
==============
Public Methods
==============
*/
gboolean            PRP_Print                       (gint file_type, gchar *file, gchar *header);

void                PRP_PrintPredict                (GdkDrawable *drawable, gint width, gint height);
void                PRP_PrintRealtime               (GdkDrawable *drawable, gint width, gint height);
void                PRP_PrintPredictBlockage        (GList *sets);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gchar         *build_report_name              (gchar *basename, gchar *extension);
static gchar         *make_ps_filename               (gchar *file);
static gboolean     save_report_image               (GdkPixbuf *pixbuf, gchar *report_name);
static void         print_blockage_table            (FILE *fp, PredictSet *ps, guint64 masks, gchar *title);
static void         print_inview_table              (FILE *fp, PredictSet *ps);
static void         print_mask_blockage             (FILE *fp, PredictSet *ps);
static void         print_dynamic_blockage          (FILE *fp, PredictSet *ps);

static gboolean     convert_jpeg2ps                 (gchar *file);
static gboolean     convert_text2ps                 (gchar *file, gchar *header);
static gboolean     print_psfile                    (gchar *psfile);
static gboolean     spawn_command                   (gchar **argv);

    #ifdef G_OS_WIN32
static void         sleep                           (gint wait);
    #endif

#endif /* PRIVATE */
#endif /* __PRINTHANDLER_H__ */
