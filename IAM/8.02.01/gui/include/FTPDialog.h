/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __FTPDIALOG_H__
#define __FTPDIALOG_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                FTP_Create                      (GtkWindow *parent);
void                FTP_Open                        (void);
gboolean            FTP_Send                        (GtkWindow *parent);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static GtkWidget    *create_interface               (void);

static void         response_cb                     (GtkWidget *widget, gint response_id, void *data);
static void         toggle_sun_abg_predict_cb       (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_sun_position_predict_cb  (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_tdrs_predict_cb          (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_sho_file_cb              (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_poic_out_cb              (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_esa_out_cb               (GtkToggleButton *togglebutton, void *user_data);
static void         toggle_hsr_out_cb               (GtkToggleButton *togglebutton, void *user_data);

static gboolean     ftp_send_to_drop_box            (void);
static gboolean     ftp_send                        (gchar *put_file, gchar *chdir, gchar *out_name, netbuf *nControl);
static void         set_last_transmission           (void);

static gchar        *get_host                       (void);
static gchar        *get_user                       (void);
static gchar        *get_pass                       (void);
static gchar        *get_poic_out                   (void);
static gchar        *get_hsr_out                    (void);
static gchar        *get_esa_out                    (void);

static gchar        *get_sun_abg_predict            (void);
static gchar        *get_sun_position_predict       (void);
static gchar        *get_tdrs_predict               (void);
static gchar        *get_sho_file                   (void);

#endif /* PRIVATE */
#endif /* __FTPDIALOG_H__ */
