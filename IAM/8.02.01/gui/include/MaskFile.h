/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __MASKFILE_H__
#define __MASKFILE_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/
/**
 * defines the prefix for limits mask files
 */
#define LIMITS              "limits"

/**
 * bit-wise definitions for mask files
 */
#define MF_NO_MASK              0x0         /**< no mask = 0 */
#define MF_SHIFT_MASK           0x1         /**< shift mask value, used to get mask id */
#define MAX_KUBAND_MASKS        20          /**< maximum number of kuband masks */
#define DEFAULT_SPIRAL_DEGREE   2           /**< 2 degree default value for spiral mask */

#define KUBAND_MASK_KEYWORD_FMT  "KUBAND_MASK%d"    /**< kuband mask format specifier */
#define DEFAULT_KUBAND_MASK_NAME "KuBand Mask %d"   /**< kuband mask default format specifier */

#define PowerOf2(x)         (MF_SHIFT_MASK << (x))  /**< faster power macro */

/**
 * Holds the mask information
 */
typedef struct MaskInfo
{
    gchar        *mask_path;         /**< directory path and name */
    gchar        *mask_name;         /**< just the name part */
    PolyLine    *mask_data;         /**< mask data */

    gboolean    primary_visible;    /**< primay mask is enabled flag */

    gboolean    autotrack_visible;  /**< autotrack mask is enabled flag */
    gboolean    openloop_visible;   /**< openloop mask is enabled flag */
    gboolean    spiral_visible;     /**< spiral mask is enabled flag */
} MaskInfo;

/**
 * Enumerations for the visibility states
 */
enum MaskEnum
{
    PRIMARY_MASK,                   /**< Primary mask index */
    AUTOTRACK_MASK,                 /**< Autotrack mask index */
    OPENLOOP_MASK,                  /**< Openloop mask index */
    SPIRAL_MASK,                    /**< Spiral mask index */
    MAX_VISIBILITY_MASKS            /**< Total masks */
};

/**
 * Enumerations for lower/upper el/xel
 */
enum LowerUpperEnum
{
    LOWER_EL    = 0,                /**< Lower EL index */
    UPPER_EL    = 1,                /**< Upper EL index */
    LOWER_XEL   = 2,                /**< Lower XEL index */
    UPPER_XEL   = 3,                /**< Upper XEL index */
    MAX_LOWER_UPPER                 /**< Total indexes */
};

/*
==============
Public Methods
==============
*/
void                MF_Constructor                  (void);
void                MF_Destructor                   (void);

void                MF_ChangeMasks                  (void);
void                MF_LoadMaskName                 (gint mask_num);
void                MF_LoadMaskData                 (void);

void                MF_ForceRedraw                  (gint display, gint coord_view);
guint               MF_GetMaskFileCount             (gint display, gint coord);
guint64             MF_GetBlockageMask              (gint display, gint coord, gint mask_num);
PolyLine            *MF_GetMaskData                 (gint display, gint coord, gint mask_num);
gchar               *MF_GetMaskName                 (gint display, gint coord, gint mask_num);
gint                MF_GetMaskColor                 (gint display, gint coord, gint mask_num);
gint                MF_MaskFileExists               (gint display, gint coord, gint mask_num);

gboolean            MF_IsLimitMask                  (gint display, gint coord, gint mask_num);
guint               MF_GetLimitMaskNum              (gint display, gint coord);

void                MF_SetInterpolatedPointMask     (PredictPt *pt, gint ptInISSAzElFlag);
gboolean            MF_IsPointInMask                (gint coord, gint width, gint height, gint azimuth, gint elevation);

guint64             MF_GetBitMask                   (gint display, gint coord);
void                MF_DrawMasks                    (WindowData *windata, gint display, gint coord_view);
void                MF_DrawLimitMask                (WindowData *windata, gint display, gint coord_view);
void                MF_DrawKuBandMasks              (WindowData *windata, gint display, gint coord_view);

gboolean            MF_GetVisibility                (gint display, gint coord, gint mask_num);
void                MF_SetVisibility                (gint display, gint coord, gint mask_num, gboolean visible);
gboolean            MF_GetKuBandVisibility          (gint display, gint mask_num, gint which_mask);
void                MF_SetKuBandVisibility          (gint display, gint mask_num, gint which_mask, gboolean active);
void                MF_SetKuBandVisibility2         (gint display, gint mask_num, gint which_mask, gboolean active);

void                MF_SetKuBandMaskValue           (gint display, gint mask_limit, gint value);

void MF_CheckKuBandMaskValue(gint display, gint mask_num);

void                MF_ClearKuBandMaskPtCount       (gint display, gint mask_limit);

PolyLine            *MF_GetRealtimeMaskData         (gint mask_num);
PolyLine            *MF_GetPredictMaskData          (gint mask_num);
void                MF_SetKuBandPredictMasks        (void);
gchar               *MF_GetKuBandMaskName           (gint mask_num);

void                MF_SetGAOMask                   (gint degree);
gint                MF_GetGAOMask                   (void);

void                MF_SetContRetry                 (gint cont_retry);
gint                MF_GetContRetry                 (void);

void                MF_SetAntennaMode               (gint antenna_mode);
gint                MF_GetAntennaMode               (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/**
 * Enumerations for fill style
 */
enum FillStyleEnum
{
    GC_NONE,        /**< no fill style */
    GC_STIPPLED,    /**< stippled fill style */
    GC_SOLID        /**< solid fill style */
};

/**
 * Enumerations how the polygon is drawn
 */
enum OpenClosedEnum
{
    GC_OPENED,      /**< Draw polygon using provided points */
    GC_CLOSED       /**< Draw polygon using first point pairs to close polygon */
};

/*
===============
Private Methods
===============
*/
static void         load_static_masks               (void);
static PolyLine     *load_mask_data                 (gchar *fname);
static void         load_spiral_degree              (void);
static void         load_kuband_mask_colors         (void);
static void         load_kuband_mask_names          (void);

static gboolean     is_point_in_kuband_mask         (gint asimuth, gint elevation);
static gboolean     is_point_in_mask                (gint display, gint coord, Point *cpt, PolyLine *mask_data);

static void         delete_static_masks             (void);
static void         delete_mask_data                (PolyLine *pLines);

static void         draw_static_mask                (WindowData *windata, gint display, gint coord, PolyLine *mask_data, GdkDrawable *drawable);
static void         draw_realtime_kuband_mask       (WindowData *windata, gint display, gint coord, GdkDrawable *drawable);
static void         draw_predict_kuband_mask        (WindowData *windata, gint display, gint coord, GdkDrawable *drawable);
static void         draw_limit_mask                 (WindowData *windata, gint display, gint coord, GdkDrawable *drawable);

static void         set_fill_stype                  (WindowData *windata, GdkGC *gc, gint fillStyle);
static PolyLine     *get_kuband_mask_data           (PolyLine *mask_data, gint colorId, gint degree);
static GPtrArray    *get_kuband_mask_info           (gint display);

#endif /* PRIVATE */
#endif /* __MASKFILE_H__ */
