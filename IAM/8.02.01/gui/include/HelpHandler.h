/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __HELPHANDLER_H__
#define __HELPHANDLER_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                HH_HelpIam                      (void);
void                HH_AboutIam                     (GtkWidget *parent);
void                HH_AboutGTK                     (GtkWidget *parent);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __HELPHANDLER_H__ */
