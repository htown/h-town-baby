/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __EVENTDIALOG_H__
#define __EVENTDIALOG_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #include "PredictDialog.h"  /* for TimerInfo and ButtonInfo */
    #define PRIVATE
#else
    #include "AntMan.h"
    #include "PredictDialog.h"
#endif

/*
=================
Public Defintions
=================
*/

/**
 * Optimum event dialog width
 */
#define EVENT_DIALOG_WIDTH  650

/**
 * Optimum event dialog height
 */
#define EVENT_DIALOG_HEIGHT 430

/*
==============
Public Methods
==============
*/
gboolean            ED_Create                       (void);
void                ED_Destructor                   (void);
void                ED_Open                         (void);
void                ED_UpdateList                   (void);
void                ED_Reset                        (void);
void                ED_UpdateTimeType               (gboolean update_time_type);
gboolean            ED_GetLockSel                   (void);


#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/**
 * Defines the list types
 */
enum List
{
    ALL_LIST = 0,       /**< all list */
    SCHEDULED_LIST      /**< schedule list */
};

/**
 * Define the print types
 */
typedef enum _PrintType
{
    PRINT_ALL,
    PRINT_TIME
} PrintType;

/**
 * Print data for the print callback
 */
typedef struct _PrintData
{
    gint  time_type; /* current time format in event list */
    gchar *time_str; /* time to search for, or NULL for ALL */
} PrintData;

/**
 * Defines the response codes for the dialog buttons
 */
enum Button
{
    BTN_TIME,       /**< time button */
    BTN_RESET,      /**< reset button */
    BTN_UPDATE,     /**< update button */
    BTN_SCHEDULE,   /**< schedule button */
    BTN_CLOSE       /**< close button */
};

/**
 * Defines the columns for the view, the first three are used
 * for defining the background, foreground, and font that will
 * be used for each row
 */
enum Column
{
    COLUMN_FONT,        /**< font column - hidden */
    COLUMN_ORBIT,       /**< orbit number */
    COLUMN_TDRS_ID,     /**< tdrs id */
    COLUMN_AOS,         /**< acquision of signal */
    COLUMN_LOS,         /**< lose of signal */
    COLUMN_ELAPSED_TIME,/**< elapsed time */
    COLUMN_DATA,        /**< predict set */
    NUM_EVENT_COLUMNS
};

/* info for the compare func */
typedef struct SortInfo
{
    gint column_id;
    gint sort_direction;
} SortInfo;

/*
===============
Private Methods
===============
*/
static void         createDialog                    (void);
static GtkListStore *createListStore                (void);
static void         addColumns                      (GtkWidget *treeView);

static void         createAllListView               (GtkWidget *notebook);
static void         createScheduleListView          (GtkWidget *notebook);

static void         closeWindow                     (void);
static gboolean     deleteEventCb                   (GtkWidget *widget, GdkEvent *event, gpointer user_data);

static void         setInitialTab                   (GtkNotebook *notebook);

static void         lockselCb                       (GtkToggleButton *button, gpointer data);
static void         timeCb                          (GtkButton *widget, gpointer user_data);
static void         printCb                         (GtkButton *widget, gpointer user_data);
static void         resetCb                         (GtkButton *widget, gpointer user_data);
static void         closeCb                         (GtkButton *button, gpointer user_data);

static gboolean     foreachPrintFunc                (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data);
static gboolean     foreachClearFunc                (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data);
static gboolean     foreachButtonFunc               (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data);

static void         releasedCb                      (GtkWidget *widget, gpointer user_data);
static gint         timerFunc                       (gpointer user_data);
static gboolean     setRepeatTimer                  (void);

static void         buildEventList                  (GtkTreeView *tree_view, GList *sets);
static void         loadEventList                   (GList *sets);

static void         selectionChangeCb               (GtkTreeSelection *selection, gpointer data);
static void         pageSwitchCb                    (GtkWidget *widget, GtkNotebookPage *page, guint page_num, gpointer user_data);
static gboolean     visibilityCb                    (GtkWidget *widget, GdkEvent *event, gpointer user_data);

static void         resetDisplayState               (GList *sets);

static void         setEventButtons                 (gboolean flag);
static void         setShosChanged                  (GList *sets);

static void         updateTime                      (void);
static gboolean     foreachChangeTimeFunc           (GtkTreeModel *child_model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data);

static void         activateEventCb                 (GtkWidget *widget, gpointer user_data);
static void         pressedEventCb                  (GtkWidget *widget, gpointer user_data);
static void         setEvents                       (guint which_direction);

static GtkWidget    *createTimePrintDlg             (void);
static void         openTimePrintDlg                (GtkWidget *dialog);
static void         responseCb                      (GtkWidget *widget, gint response_id, gpointer data);
static void         printEvent                      (gint which_print, gchar *time_str);
static void         activateCb                      (GtkWidget *widget, gpointer data);
static gboolean     processPrintTime                (GtkWidget *time_entry);
static gboolean     keyPressCb                      (GtkWidget *widget, GdkEventKey *event, gpointer data);

static void         updateCb                        (GtkButton *button, gpointer user_data);
static void         rowActivatedCb                  (GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data);
static void         selectEvent                     (void);

static gboolean     listboxDragCb                   (GtkWidget *listbox, GdkEventMotion *event, gpointer data);

static GtkTreeView  *getTreeView                    (void);

#endif /* PRIVATE */
#endif /* __EVENTDIALOG_H__ */
