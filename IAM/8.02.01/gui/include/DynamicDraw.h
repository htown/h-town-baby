/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __DYNAMICDRAW_H__
#define __DYNAMICDRAW_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #include "Vector.h"
    #include "TimeStrings.h"
    #define PRIVATE
#else
    #include "AntMan.h"
    #include "Vector.h"
    #include "TimeStrings.h"
#endif

/*
=================
Public Defintions
=================
*/

#define MAX_SEGMENTS    20
#define LABEL_SIZE      32

#define PORT_LABEL_2B   "2B"
#define PORT_LABEL_4B   "4B"
#define PORT_LABEL_2A   "2A"
#define PORT_LABEL_4A   "4A"
#define PORT_LABEL_PA   "SARJ"
#define PORT_LABEL_PTCS "Rad"

#define STBD_LABEL_1B   "1B"
#define STBD_LABEL_3B   "3B"
#define STBD_LABEL_1A   "1A"
#define STBD_LABEL_3A   "3A"
#define STBD_LABEL_SA   "SARJ"
#define STBD_LABEL_STCS "Rad"

/**
 * Indexes into the JointAngle.angleValue
 */
enum Angle
{
    ANGLE_UNKNOWN = -1,

    FIRST_PORT_ANGLE = 0,

    PORT_ANGLE_2B = FIRST_PORT_ANGLE,
    PORT_ANGLE_4B,
    PORT_ANGLE_2A,
    PORT_ANGLE_4A,
    PORT_ANGLE_PA,
    PORT_ANGLE_PTCS,

    LAST_PORT_ANGLE,
    MAX_PORT_ANGLES = LAST_PORT_ANGLE,

    FIRST_STBD_ANGLE = LAST_PORT_ANGLE,

    STBD_ANGLE_1B = FIRST_STBD_ANGLE,
    STBD_ANGLE_3B,
    STBD_ANGLE_1A,
    STBD_ANGLE_3A,
    STBD_ANGLE_SA,
    STBD_ANGLE_STCS,

    LAST_STBD_ANGLE,
    MAX_STBD_ANGLES = LAST_STBD_ANGLE - FIRST_STBD_ANGLE,

    MAX_ANGLES = MAX_PORT_ANGLES + MAX_STBD_ANGLES
};

/**
 * Defines the joint angle data returned from isp
 */
typedef struct JointAngle
{
    gchar        timeStr[TIME_STR_LEN];              /**< symbol time */
    gdouble      angleValue[MAX_ANGLES];             /**< symbol value */
    gchar        angleLabel[MAX_ANGLES][LABEL_SIZE]; /**< symbol label */
} JointAngle;

/**
 * Defines the polygon list structure
 */
typedef struct POLYGON
{
    gint num_vectors;   /**< how many vectors defined */
    gint first_vector;  /**< pointer to first vector */
    gboolean draw_it;  /**< indicates whether or not to draw the polygon. */
} POLYGON;

/**
 * Defines the segment structure
 */
typedef struct SEGMENT
{
    gchar       filename[256];              /**< segment name */

    gint        rotation_sequence;          /**< rotation sequence */
    gboolean    sarj_valid;                 /**< sarj valid flag */

    VECTOR      rotation_axis_center;       /**< rotation axis vector */
    VECTOR      rotation_axis_head;         /**< rotation axis vector */
    VECTOR      normal_rotation_axis_head;  /**< normal rotation vector */

    gint        number_of_polygons;         /**< total number of polygons */
    POLYGON     *polygon_array;             /**< polygon array */

    /* orignal raw data read from the segment files */
    gint        number_of_vectors;          /**< total number of vectors */
    VECTOR      *vector_array;              /**< vector array */

    VECTOR      *rotated_vectors    [NUM_DISPLAYS];             /**< vectors rotated */
    Point       *coordinate_points  [NUM_DISPLAYS][NUM_COORDS]; /**< coordinate points */
    GdkPoint    *window_points      [NUM_DISPLAYS][NUM_VIEWS];  /**< screen points */

    gint        current_angle_set   [NUM_DISPLAYS][NUM_COORDS]; /**< angle set for each display */
    gdouble     current_bg_angle    [NUM_DISPLAYS][NUM_COORDS]; /**< bg angle for each display */
    gdouble     current_sarj_angle  [NUM_DISPLAYS][NUM_COORDS]; /**< current sarg angle */
} SEGMENT;

/*
==============
Public Methods
==============
*/
void                DD_Constructor                  (void);
void                DD_Destructor                   (gint freeAll);
void                DD_FreeJointAngleData           (GPtrArray *ptrArray);

gboolean            DD_CheckFileStatus              (void);

gboolean            DD_GetAngles                    (gint coord, gchar *entry_time, JointAngle *ja);

void                DD_SetRedraw                    (gboolean flag);
void                DD_SetRedrawForView             (gint display, gint coord_view, gboolean flag);

void                DD_DrawDataAtTime               (WindowData *windata, gint display, gint coord_view, GList *cpt);
void                DD_DrawDataForAngle             (WindowData *windata, gint display, gint coord_view, JointAngle *ja);

gint                DD_PointBlocked                 (gint display, gint coord, gint width, gint height, Point *point, gchar *time_occurs);

VECTOR              *DD_GetAntennaLocation          (gint coord);
void                DD_SetAntennaLocation           (gint coord, VECTOR *data);

gchar               *DD_GetActiveStructure          (void);
void                DD_ChangeStructure              (gchar *structure);
GPtrArray           *DD_GetStructureNames           (void);

void                DD_SetData                      (GPtrArray *jointAngleArray);
GPtrArray           *DD_GetData                     (void);

/*
==================
Private Defintions
==================
*/
#ifdef PRIVATE

/*
===============
Private Methods
===============
*/
static void         loadAntennaLocations            (void);
static gboolean     loadVectorData                  (void);
static void         allocPixmapSegments             (void);
static void         drawSegment                     (WindowData *windata, gint display, gint coord_view, gint segment);
static void         renderPixmaps2Drawable          (WindowData *windata, gint display, gint coord_view);

static gint         findStartingPosition            (guint current_entry, gchar *entry_time);
static void         deleteArrayData                 (gpointer data, gpointer user_data);

static void         loadRotationSequenceHash        (void);
static gboolean     hashTableForeach                (void * key, void * value, void * user_data);
static void         deleteValue                     (void * data);

static void         setRotationSequence             (SEGMENT *seg, gchar *rotation_sequence_str);
static void         setRotationAngles               (gint rotation_sequence, gboolean sarj_valid, gdouble *bg_angle, gdouble *sarj_angle, JointAngle *ja);
static void         setJointAngleLabels             (JointAngle *ja);
static gboolean     getAngleData                    (gchar *entry_time, JointAngle *ja);
static void         rotationPointsAroundAxis        (VECTOR *head_of_axis,
                                                     VECTOR *tail_of_axis,
                                                     gint     num_vectors,
                                                     VECTOR *old_vectors,
                                                     VECTOR *new_vectors,
                                                     gdouble  bg_angle_in_degrees,
                                                     gdouble  sarj_angle_in_degrees);

static void         computeCoordinates              (WindowData *windata, gint display, gint coord_view, SEGMENT *seg);
static void         computeSband1Coords             (WindowData *windata, gint display, gint coord_view, SEGMENT *seg);
static void         computeSband2Coords             (WindowData *windata, gint display, gint coord_view, SEGMENT *seg);
static void         computeKubandCoords             (WindowData *windata, gint display, gint coord_view, SEGMENT *seg, gint coord);
static void         computeStationCoords            (WindowData *windata, gint display, gint coord_view, SEGMENT *seg);

static void         createSmdFile                   (gchar *dynamic_dir);
static void         loadSmdFile                     (void);
static gchar        *getSmdFile                     (gchar *dynamic_dir);

#endif /* PRIVATE */
#endif /* __DYNAMICDRAW_H__ */
