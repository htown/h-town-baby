/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __MESSAGEHANDLER_H__
#define __MESSAGEHANDLER_H__

#include <gtk/gtk.h>

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                InfoMessage                     (GtkWidget *parent, gchar *title, gchar *message);
void                WarningMessage                  (GtkWidget *parent, gchar *title, gchar *message);
void                ErrorMessage                    (GtkWidget *parent, gchar *title, gchar *message);
gboolean            QuestionMessage                 (GtkWidget *parent, gchar *title, gchar *message, gint default_answer);

void                Message                         (GtkWidget *parent, GtkMessageType message_type, gchar *title,
                                                     gchar *message, gchar *expand_message,
                                                     gchar *fg_color, gchar *bg_color, PangoFontDescription *font_desc);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         answerTheQuestion               (GtkDialog *dialog, gint response_id, void * data);

static void         print_data                      (gchar *print_data, gchar *title);
static void         activate_cb                     (GtkExpander *widget, void * user_data);
static void         response_cb                     (GtkWidget *widget, gint response_id, void * data);
static void         result_cb                       (GtkWidget *widget, gint result_id, gchar *expand_message);

#endif /* PRIVATE */
#endif /* __MESSAGEHANDLER_H__ */
