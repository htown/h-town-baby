/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __REALTIMEDIALOG_H__
#define __REALTIMEDIALOG_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "MaskFile.h"
    #include "Dialog.h"
    #include "RealtimeDataHandler.h"
    #define PRIVATE
#else
    #include "MaskFile.h"
    #include "Dialog.h"
    #include "RealtimeDataHandler.h"
#endif

/*
=================
Public Defintions
=================
*/
#define KU_XMIT_STR "Ku Xmit"
#define VIEW_H_to_V "h->V"
#define VIEW_V_to_H "v->H"
#define KU_TOGGLE_LABEL_1 "Ku-band 1"
#define KU_TOGGLE_LABEL_2 "Ku-band 2"

/*
==============
Public Methods
==============
*/
gboolean            RTD_Create                      (void);
void                RTD_Destructor                  (void);
void                RTD_Open                        (void);

void                UEsetRealtimeDialog             (void);

GtkWidget           *RTD_GetDialog                  (void);
WindowData          *RTD_GetWindowData              (gint coordinate_type);
Area                *RTD_GetArea                    (gint coordinate_type);

void                RTD_SetDialogTitle              (void);

gboolean            RTD_IsActive                    (void);
gboolean            RTD_IsResizing                  (void);
gboolean            RTD_IsIconified                 (void);

void                RTD_SetValue                    (SymbolRec *symbol);

void                RTD_SetKuXmitColor              (gint color);
void                RTD_SetKuXmitValue              (SymbolRec *symbol);
void                RTD_SetKuXmitStatus             (SymbolRec *symbol);

void                RTD_SetRedraw                   (gboolean flag);
void                RTD_DrawData                    (void);

void                RTD_DrawCoordPoints             (WindowData *windata, gchar *str, gint x, gint y);

void                RTD_UpdateTimeType              (gboolean update_time_type);

guint               RTD_GetBlockageActionItem       (gint blockage_type, gint which_mask);

gint                RTD_GetCoordinateType           (void);
gint                RTD_GetCoordinateView           (void);
gboolean            RTD_GetKubandIsActive           (void);

void                RTD_ChangeStructure             (const gchar *action_name);

WindowData          *RTD_GetWindowDataByWidget      (GtkWidget *widget);

PangoFontDescription *RTD_GetPangoFontDescription   (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         create_window                   (ResizeFont *resize_font);
static void         create_menu_bar                 (gint view_orientation_val);
static gint         create_orientation_view         (ResizeFont *resize_font);
static void         create_drawing_area             (gint coord, gint view, gint drawing_area, gchar *title, GCallback toolbar_callback, gint *notify_data);
static GtkWidget    *create_drawing_area2           (gint sband_coord, gint sband_view,
                                                     gint sband_drawing_area,
                                                     gchar *sband_title,
                                                     gint *sband_notify_data,
                                                     gint *sband_expose_data,
                                                     gint coord, gint view,
                                                     gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                                                     gchar *sku_title, GCallback toolbar_callback, gint *notify_data,
                                                     gint *expose_data, gint *left_expose_data, gint *bottom_expose_data,
                                                     gint view_orientation_val);
static void         create_drawing_area3            (gint coord, gint view,
                                                     gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                                                     gchar *title, GCallback toolbar_callback, gint *notify_data,
                                                     gint *expose_data, gint *left_expose_data, gint *bottom_expose_data);

static void         create_button_form              (GtkWidget *vbox);

static void         file_menu_action_cb             (GtkAction *action);
static void         settings_menu_action_cb         (GtkAction *action);
static void         settings_menu_view_orientation_cb (GtkAction *action, GtkRadioAction *current);
static void         settings_menu_kuband_view_cb    (GtkAction *action, GtkRadioAction *current);
static void         settings_menu_boresight_cb      (GtkAction *action, GtkRadioAction *current);
static void         blockage_menu_action_cb         (GtkAction *action);
static void         help_menu_action_cb             (GtkAction *action);
static void         view_orientation_pressed_cb     (GtkWidget *widget, void * user_data);
static void	    ku_toggle_pressed_cb	    (GtkWidget *Widget, void * user_data);

static void         structure_change_action_cb      (GtkAction *action, GtkRadioAction *current);

static void         sband1_blockage_action_cb       (GtkAction *action);
static void         sband2_blockage_action_cb       (GtkAction *action);
static void         kuband_blockage_action_cb       (GtkAction *action);
static void         kuband2_blockage_action_cb      (GtkAction *action);
static void         station_blockage_action_cb      (GtkAction *action);

static void         sband1_blockage_clicked_cb      (GtkToolButton *tool_button, void * user_data);
static void         sband2_blockage_clicked_cb      (GtkToolButton *tool_button, void * user_data);
static void         kuband_blockage_clicked_cb      (GtkToolButton *tool_button, void * user_data);
static void         kuband2_blockage_clicked_cb     (GtkToolButton *tool_button, void * user_data);
static void         station_blockage_clicked_cb     (GtkToolButton *tool_button, void * user_data);

static void         set_kuband_view_to_start        ();
static gboolean     visibility_cb                   (GtkWidget *widget, GdkEvent *event, void * user_data);
static gboolean     close_cb                        (GtkWidget *widget, GdkEvent *event, void * user_data);
static gboolean     resize_window_cb                (GtkWidget *widget, GdkEventConfigure *event, void * user_data);
static gboolean     resize_drawing_area_cb          (GtkWidget *widget, GdkEventConfigure *event, void * user_data);
static gboolean     expose_cb                       (GtkWidget *widget, GdkEventExpose *event, void * user_data);
static gboolean     enter_notify_cb                 (GtkWidget *widget, GdkEventCrossing *event, void * user_data);
static gboolean     leave_notify_cb                 (GtkWidget *widget, GdkEventCrossing *event, void * user_data);
static void         page_switch_cb                  (GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data);
static void         time_cb                         (GtkWidget *widget, void * user_data);
static void         refresh_cb                      (GtkWidget *widget, void * user_data);
static void         clear_cb                        (GtkWidget *widget, void * user_data);
static gboolean     focus_in_event_cb               (GtkWidget *widget, GdkEventFocus *event, void * user_data);
static gboolean     focus_out_event_cb              (GtkWidget *widget, GdkEventFocus *event, void * user_data);

static void         set_status                      (GtkWidget *label, SymbolRec *symbol);
static void         set_value                       (GtkWidget *label, SymbolRec *symbol);
static void         set_time_value                  (GtkWidget *label, SymbolRec *symbol, gboolean force);

static void         set_button_mask_visual          (gint coord, guint mask_num, const gchar *mask_name);
static void         set_blockage_mask_by_name       (gint coord, const gchar *mask_name);
static void         set_blockage_mask               (gint coord, gint mask_num);
static void         set_masks_enabled               (gint coord, gchar *config_keyword);

static void         set_coordinate_view             (void);
static void         set_sku_coordinate_type         (gint which_drawing_area);

static void         draw_data_for_view              (gint coord_view);

static void         make_pixmap_data                (WindowData *windata, EventSizeData *event_data, gint which_drawing_area);
static void         draw_rt_tracks                  (WindowData *windata, gint coord);
static void         draw_rt_track                   (WindowData *windata, gint coord, gint tdrs, GdkDrawable *drawable);
static void         draw_rt_track_line              (WindowData *windata, gint coord, gint tdrs, RtTrack *track, GdkDrawable *drawable);
static void         draw_rt_track_text              (WindowData *windata, gint coord, gint tdrs, RtTrack *track, GdkDrawable *drawable);

static void         draw_rt_predicts                (WindowData *windata, gint coord, GdkDrawable *drawable);
static void         draw_rt_predict                 (WindowData *windata, gint coord, gint tdrs, GdkDrawable *drawable);

static void         draw_ant_point                  (WindowData *windata, gint coord, GdkDrawable *drawable);

static void         draw_sband_lga_cross_hair       (WindowData *windata, gint coord, GdkDrawable *drawable);
static PangoLayout  *get_kuband_lga_cross_hair      (WindowData *windata, gint coord, gint az, gint el);

static PredictSet   *validPredicts                  (gint coord, gint tdrs);

static GtkWidget    *get_value_label                (gint symbol_id);
static GtkWidget    *get_status_label               (gint symbol_id);

static WindowData   *get_window_data_by_coord       (gint coord, gint sku_coord);
static gint         get_index_by_coord              (gint coord);

static void         set_view_orientation            (gint view_orientation);
static void         set_kuband_displayed            (gint kuband_to_display);
static void         change_orientation              (GtkWidget **container, gint orientation);

static void         set_pango_font_desc             (ResizeFont *resize_font, PangoFontDescription *font_desc);
static void         set_kuband_view_to_start        (void);
gint                RTD_GetCoordTypeFromView        (gint coord_view);

#endif /* PRIVATE */
#endif /* __REALTIMEDIALOG_H__ */
