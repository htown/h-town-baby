/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __REALTIMERFDIALOG_H__
#define __REALTIMERFDIALOG_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "RealtimeDataHandler.h"
    #define PRIVATE
#else
    #include "RealtimeDataHandler.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
gboolean            RTRFD_Create                    (void);
void                RTRFD_Open                      (void);

void                RTRFD_SetValue                  (SymbolRec *symbol);
void                RTRFD_SetTdrsPredicts           (gint tdrs, guchar color);
void                RTRFD_SetTdrsId                 (gint tdrs, SymbolRec *symbol);
void                RTRFD_SetTdrsValue              (gint tdrs, gint azel_type, SymbolRec *symbol);

gint                RTRFD_GetCoordinateType         (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         create_dialog                   (void);
static GtkWidget    *create_top_data_form           (void);
static GtkWidget    *create_tdrs_form               (void);
static GtkWidget    *create_sun_tracking_form       (void);
static GtkWidget    *create_slot_form               (void);
static GtkWidget    *create_slot1                   (void);
static GtkWidget    *create_slot2                   (void);
static GtkWidget    *create_joint_angle_form        (void);
static GtkWidget    *create_port_angles             (void);
static GtkWidget    *create_stbd_angles             (void);
static GtkWidget    *create_data_temp_form          (void);
static GtkWidget    *create_sband1_form             (void);
static GtkWidget    *create_sband1_data_form        (void);
static GtkWidget    *create_sband1_temp_form        (void);
static GtkWidget    *create_sband2_form             (void);
static GtkWidget    *create_sband2_data_form        (void);
static GtkWidget    *create_sband2_temp_form        (void);
static GtkWidget    *create_kuband_form             (void);
static GtkWidget    *create_kuband_data_form        (void);
static GtkWidget    *create_kuband_temp_form        (void);

static GtkWidget    *get_value_label                (gint symbol_id);
static GtkWidget    *get_status_label               (gint symbol_id);

static void         insert_table_item               (GtkWidget *table, GtkWidget *item, gint row, gint col,
                                                     GtkAttachOptions xoptions, GtkAttachOptions yoptions);

static void         set_status                      (GtkWidget *label, SymbolRec *symbol);
static void         set_value                       (GtkWidget *label, SymbolRec *symbol);

static void         page_switch_cb                  (GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data);
static gboolean     delete_event_cb                 (GtkWidget *widget, GdkEvent *event, void * user_data);
static void         close_cb                        (GtkButton *button, void * user_data);
static gboolean     visibility_cb                   (GtkWidget *widget, GdkEvent *event, void * user_data);
static gint          resize_cb                       (GtkWidget *widget, GdkEventConfigure *event, void * user_data);

static void         close_window                    (void);

#endif /* PRIVATE */
#endif /* __REALTIMERFDIALOG_H__ */
