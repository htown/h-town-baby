/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __GTKJFILECHOOSER_H__
#define __GTKJFILECHOOSER_H__

#include <gdk/gdk.h>
#include <gtk/gtkdialog.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
    #include <unistd.h>
#endif

#ifdef G_OS_WIN32
    #ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
    #endif
    //#include <windows.h>
    #include <winsock2.h> /* for gethostname */
    #include <ws2tcpip.h>
    #include <process.h>  /* for _getpid */
#endif

/*
=================
Public Defintions
=================
*/
#define GTK_TYPE_JFILE_CHOOSER               (gtk_jfile_chooser_get_type())
#define GTK_JFILE_CHOOSER(obj)               (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_JFILE_CHOOSER, GtkJFileChooser))
#define GTK_JFILE_CHOOSER_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_JFILE_CHOOSER, GtkJFileChooserClass))
#define GTK_IS_JFILE_CHOOSER(obj)            (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_JFILE_CHOOSER))
#define GTK_IS_JFILE_CHOOSER_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_JFILE_CHOOSER))
#define GTK_JFILE_CHOOSER_GET_CLASS(obj)     (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_JFILE_CHOOSER, GtkJFileChooserClass))

/**
 * Define the file chooser view mode
 */
typedef enum GtkJFileChooserViewMode
{
    GTK_UNKNOWN_FILE_SELECTION = -1,    /**< view mode unknown */
    GTK_FILES_ONLY,                     /**< file view */
    GTK_DIRECTORIES_ONLY,               /**< directory view */
    GTK_FILE_AND_DIRECTORIES            /**< both file and directory view */
} GtkJFileChooserViewMode;

/**
 * Forward declaration
 */
typedef struct GtkJFileChooser GtkJFileChooser;

/**
 * Defines the file chooser data
 */
struct GtkJFileChooser
{
    GtkDialog parent_instance;          /**< parent */
    void *class_data;                   /**< class data */
};

/**
 * Forward declaration for file chooser class data
 */
typedef struct GtkJFileChooserClass GtkJFileChooserClass;

/**
 * Defines the file chooser class data
 */
struct GtkJFileChooserClass
{
    GtkDialogClass parent_class;        /**< parent class */

    gboolean (* frame_event) (GtkWindow *window, GdkEvent  *event); /**< frame event */

    void (* update_preview) (GtkJFileChooser *chooser); /**< update preview */

    /* Padding for future expansion */
    void (*_gtk_reserved1) (void);
    void (*_gtk_reserved2) (void);
    void (*_gtk_reserved3) (void);
    void (*_gtk_reserved4) (void);
};

/**
 * Some keywords used for displaying
 */
#define DESKTOP             "Desktop"
#define MY_DOCUMENTS        "My Documents"
#define MY_COMPUTER         "My Computer"
#define NETHOOD             "Nethood"
#define MY_NETWORK_PLACES   "My Network Places"


/*
==============
Public Methods
==============
*/
GtkWidget           *gtk_jfile_chooser_new                      (const gchar *title);
GtkWidget           *gtk_jfile_chooser_new_with_parent          (GtkWindow *parent, const gchar *title);
GtkWidget           *gtk_jfile_chooser_new_with_all             (GtkWindow *parent, const gchar *title, GtkDialogFlags flags);

void                gkt_jfile_chooser_set_dialog_title          (GtkJFileChooser *jfile_chooser, const gchar *title);

GType               gtk_jfile_chooser_get_type                  (void);

void                gtk_jfile_chooser_set_current_directory     (GtkJFileChooser *jfile_chooser, gchar *file);
gchar               *gtk_jfile_chooser_get_current_directory    (GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_add_file_filter           (GtkJFileChooser *jfile_chooser, const gchar *pattern, const gchar *pattern_description);
const gchar         *gtk_jfile_chooser_get_file_filter          (GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_set_file_selection_mode   (GtkJFileChooser *jfile_chooser, GtkJFileChooserViewMode view_mode);
GtkJFileChooserViewMode gtk_jfile_chooser_get_file_selection_mode(GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_set_multi_selection       (GtkJFileChooser *jfile_chooser, gboolean enable);
gboolean            gtk_jfile_chooser_is_multi_selection_enabled(GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_set_hiding_enabled        (GtkJFileChooser *jfile_chooser, gboolean enable);
gboolean            gtk_jfile_chooser_is_hiding_enabled         (GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_set_selected_file         (GtkJFileChooser *jfile_chooser, gchar *file);

gchar               *gtk_jfile_chooser_get_file                 (GtkJFileChooser *jfile_chooser);
gchar               **get_jfile_chooser_get_files               (GtkJFileChooser *jfile_chooser);

void                gtk_jfile_chooser_set_preview_accessory     (GtkJFileChooser *jfile_chooser, GtkWidget *preview);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

    #include "gtkjfilechooser_icons.h"

    #define GAO_NONE            (GtkAttachOptions)(0)
    #define GAO_FILL            (GtkAttachOptions)(GTK_FILL)
    #define GAO_EXPAND          (GtkAttachOptions)(GTK_EXPAND)
    #define GAO_EXPAND_FILL     (GtkAttachOptions)(GTK_EXPAND | GTK_FILL)

/* the icons */
enum IconType
{
    ICON_FOLDER,        /**< folder icon */
    ICON_COMPUTER,      /**< computer icon */
    ICON_DISK,          /**< disk icon */
    ICON_DESKTOP,       /**< desktop icon */
    ICON_UP_ONE_LEVEL,  /**< up one level icon */
    ICON_NEW_FOLDER,    /**< new folder icon */
    ICON_LIST,          /**< list icon */
    ICON_DETAILS,       /**< details icon */
    ICON_DOCUMENT,      /**< document icon */
    ICON_DIRECTORY,     /**< directory icon */
    ICON_MAX            /**< total icons */
};

/**
 * Defines used to resolve well known extensions.
 * Simple lookup, need win32 call to get this
 */
    #define LNK             "lnk"   /**< shortcut */
    #define TXT             "txt"   /**< text document */
    #define DOC             "doc"   /**< microsoft word document */
    #define EXE             "exe"   /**< application */
    #define VSD             "vsd"   /**< microsoft visio viewer */
    #define PPT             "ppt"   /**< microsoft powerpoint presentation */
    #define XLS             "xls"   /**< microsoft excel worksheet */
    #define ZIP             "zip"   /**< winzip file */
    #define GZIP            "gz"    /**< gzip file */
    #define PDF             "pdf"   /**< adobe acrobat document */
    #define RTF             "rtf"   /**< rich text document */
    #define WAB             "wab"   /**< address book file */
    #define HTM             "htm"   /**< html document */
    #define HTML            "html"  /**< html document */
    #define B4S             "b4s"   /**< winamp media file */
    #define CL4             "cl4"   /**< easy cd creator document */
    #define JWL             "jwl"   /**< jewel case creator document */
    #define JAR             "jar"   /**< executable jar file */
    #define JS              "js"    /**< jscript script file */
    #define XPM             "xpm"   /**< xpm image */
    #define JPG             "jpg"   /**< jpg image */
    #define GIF             "gf"    /**< gif image */
    #define BMP             "bmp"   /**< bitmap image */
    #define MIX             "mix"   /**< microsoft picture it! document */
    #define MPA             "mpa"   /**< movie clip */
    #define MPEG            "mpeg"  /**< movie clip */
    #define WAV             "wav"   /**< windows media player */
    #define INI             "ini"   /**< configuration settings */
    #define PS              "ps"    /**< postscript */
    #define FON             "fon"   /**< font file */
    #define TIF             "tif"   /**< truetype font file */
    #define CMD             "cmd"   /**< windows nt command script */
    #define BAT             "bat"   /**< ms-dos batch file */
    #define REG             "reg"   /**< registration entries */
    #define SYS             "sys"   /**< system file */
    #define V386            "386"   /**< virtual device driver */
    #define OCX             "ocx"   /**< activex control */
    #define WMZ             "wmz"   /**< windows media player skin package */
    #define HLP             "hlp"   /**< help file */

/**
 * Hash key lookup value
 */
    #define FILTER_KEY      "filter-key"

/**
 * File types
 */
typedef enum FileType
{
    GTK_FILE_TYPE_IS_UNKNOWN = 0,   /**< File type unknown */
    GTK_FILE_TYPE_IS_REGULAR,       /**< file type is regular file */
    GTK_FILE_TYPE_IS_DIR            /**< file type is directory */
} FileType;

/**
 * View model
 */
typedef enum GtkJFileChooserViewModel
{
    GTK_LIST_VIEW_MODEL,            /**< general list view */
    GTK_DETAILS_VIEW_MODEL          /**< details view */
} GtkJFileChooserViewModel;


/**
 * File filter data structure
 */
typedef struct FileFilter FileFilter;
struct FileFilter
{
    GPatternSpec    *pattern;       /**< pattern for file filter */
    gchar           *pattern_string;/**< pattern string */
    gchar           *description;   /**< optional description */
};

/**
 * Macro to return class data
 */
    #define GTK_JFILE_CHOOSER_GET_CLASS_DATA(jfile_chooser) jfile_chooser_get_class_data(jfile_chooser)

/**
 * Define class data
 */
typedef struct GtkJFileChooserClassData GtkJFileChooserClassData;
struct GtkJFileChooserClassData
{
    gchar           *base_dir;              /**< initial directory */

    GtkWidget       *lookin_option_menu;    /**< directory option menu */
    gulong          option_menu_handler_id; /**< handler id */

    GtkWidget       *ok_button;             /**< dialog ok button */
    GtkWidget       *cancel_button;         /**< dialog cancel button */
    GtkWidget       *selection_entry;       /**< text entry */
    GtkWidget       *tree_view;             /**< file list component */

    GtkWidget       *filter_option_menu;    /**< holds the ascii filter string */
    GList           *file_filter;           /**< has the file filter information */

    GtkJFileChooserViewModel view_model;    /**< either list or details */
    GtkJFileChooserViewMode view_mode;      /**< files, directories, or both */

    GList           *selected_filenames;    /**< selected filename or filenames */

    gboolean        hiding_enabled;         /**< show hidden files */
};

/**
 * Define lookin opton menu pulldown data
 */
typedef struct GtkLookinOptionMenuData GtkLookinOptionMenuData;
struct GtkLookinOptionMenuData
{
    gint            key;                    /**< unique key identifying this node */
    gchar           displayname[MAXPATHLEN];/**< how the item appears in the pulldown */
    gchar           pathname   [MAXPATHLEN];/**< the real path and name */
    gchar           basename   [128];       /**< just the name part */
    gint            indent_level;           /**< indentation on pulldown */
    gint            parent_key;             /**< key identifying the parent this node belongs to */
    gboolean        keep;                   /**< True=never remove */
    gboolean        add;                    /**< True=add to menu */
};

/**
 * Define the stat data for the file
 */
typedef struct GtkFileListData GtkFileListData;
struct GtkFileListData
{
    gchar           basename[MAXPATHLEN];   /**< just the name of the file */
    FileType        file_type;              /**< type of file this is */
    gint            size;                   /**< size of file */
    gint            type;                   /**< type of file */
    gint            modified;               /**< modification date */
    gint            attributes;             /**< file access */
};

    #define FILE_LIST_INFO_TYPE file_list_info_get_type()

/**
 * Define the display information derived from the GtkFileListData
 */
typedef struct GtkFileListInfo GtkFileListInfo;
struct GtkFileListInfo
{
    GdkPixbuf       *icon;                  /**< icon for file */
    gchar           *name;                  /**< name of file */
    gchar           *size;                  /**< size of file */
    gchar           *type;                  /**< type of file */
    gchar           *modified;              /**< modification date */
    gchar           *attributes;            /**< file access */
};

/*
===============
Private Methods
===============
*/
static GtkWidget*   jfile_chooser_new_empty         (GtkWindow *parent, const gchar *title, GtkDialogFlags flags);

static GtkJFileChooserClassData *jfile_chooser_get_class_data(GtkJFileChooser *jfile_chooser);

static void         jfile_chooser_init              (GtkJFileChooser *jfile_chooser);
static void         jfile_chooser_class_init        (GtkJFileChooserClass *klass);
static GtkJFileChooserClassData *jfile_chooser_class_data_init(void);
static void         jfile_chooser_finalize          (GObject *object);
static void         dialog_destroy_cb               (GtkObject *object);
static void         jfile_chooser_map               (GtkWidget *widget);
static void         jfile_chooser_set_property      (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void         jfile_chooser_get_property      (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);

static gchar        *jfile_chooser_get_single_selection(GtkTreePath *path, GtkTreeViewColumn *column, GtkJFileChooser *jfile_chooser);
static void         jfile_chooser_add_filenames     (GtkJFileChooser *jfile_chooser);
static void         jfile_chooser_add_filename      (GtkJFileChooser *jfile_chooser, gchar *filename);

static void         jfile_chooser_set_dir           (GtkJFileChooser *jfile_chooser, GtkJFileChooserClassData *class_data);
static void         getDirectoryComponents          (gchar *base_dir, GSList **path_components);
static void         getOptionMenuItems              (GtkJFileChooser *jfile_chooser, GtkWidget *lookin_option_menu, GSList *path_components);
static void         addPathComponents               (GtkJFileChooser *jfile_chooser, GtkWidget *lookin_option_menu, GtkWidget *menu, GSList *path_components);

static GtkWidget    *get_button_icon                (GtkWidget *image);
static GdkPixbuf    *get_pixbuf_icon                (gint which_icon);
static GtkWidget    *get_icon                       (gint which_icon);

static GtkWidget    *get_lookin_menu_item           (gboolean doAdd,
                                                     gint key,
                                                     gchar *displayname,
                                                     const gchar *pathname,
                                                     gint which_icon,
                                                     gint indent_level,
                                                     gboolean keep);
static void         add_lookin_menu_item            (GtkWidget *menu, GtkWidget *menu_item);

static void         jfile_chooser_remove_menu_item  (GtkWidget *option_menu, GtkLookinOptionMenuData *menu_data);
static GtkWidget    *jfile_chooser_get_menu_item    (GtkWidget *menu, gchar *displayname);

static GtkListStore *create_list_model              (GtkJFileChooser *jfile_chooser);
static GtkListStore *create_details_model           (GtkJFileChooser *jfile_chooser);

static GtkTreeIter  *get_row_iter                   (GtkListStore *model, guint max_rows);

static GtkLookinOptionMenuData *jfile_chooser_make_data     (GtkJFileChooser *jfile_chooser, gchar *basename);
static GtkLookinOptionMenuData *jfile_chooser_get_data      (GtkJFileChooser *jfile_chooser);

static void         populate_filelist               (GtkJFileChooser *jfile_chooser, GtkLookinOptionMenuData *lookin_option_menu_data);
static gboolean     is_hidden_file                  (const gchar *filename);
static gboolean     is_dir_file                     (const gchar *filename);
static FileType     get_file_type                   (const gchar *filename);
static void         add_files                       (GtkJFileChooser *jfile_chooser, GPtrArray *file_list_data);
static void         add_to_view                     (GtkJFileChooserViewModel view_model, GtkListStore *model, GtkFileListInfo *info, GtkTreeIter *row, guint col);

static void         jfile_chooser_add_menu_items    (GtkJFileChooser *jfile_chooser);
static void         jfile_chooser_add_filename_to_menu(GtkJFileChooser *jfile_chooser, gchar *filename);
static gboolean     jfile_chooser_show_file         (GtkJFileChooserClassData *class_data, gchar *filename);

static void         file_list_clear                 (GtkJFileChooser *jfile_chooser);
static gint         file_list_compare               (gconstpointer a, gconstpointer b);
static GType        file_list_info_get_type         (void);
static void         file_list_info_free             (GtkFileListInfo *info);
static GtkFileListInfo* file_list_info_copy         (GtkFileListInfo *src);

static void         file_list_set_icon              (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);
static void         file_list_set_name              (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);
static void         file_list_set_size              (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);
static void         file_list_set_type              (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);
static void         file_list_set_mod               (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);
static void         file_list_set_attrib            (GtkTreeViewColumn *tree_column, GtkCellRenderer *cell, GtkTreeModel *model, GtkTreeIter *iter, void * data);

static GdkPixbuf    *file_list_get_icon             (GtkFileListData *filedata, gint width, gint height);
static gchar        *file_list_get_name             (GtkFileListData *filedata);
static gchar        *file_list_get_size             (GtkFileListData *filedata);
static gchar        *file_list_get_type             (GtkFileListData *filedata);
static gchar        *file_list_get_mod              (GtkFileListData *filedata);
static gchar        *file_list_get_attrib           (GtkFileListData *filedata);

/* callbacks */
static void         dialog_response_cb              (GtkDialog *dialog, gint response_id);
static void         up_one_level_button_clicked_cb  (GtkWidget *button, void * data);
static void         list_button_clicked_cb          (GtkWidget *button, void * data);
static void         details_button_clicked_cb       (GtkWidget *button, void * data);

static void         lookin_option_menu_changed_cb   (GtkOptionMenu *option_menu, void * data);

static void         filter_option_menu_changed_cb   (GtkOptionMenu *option_menu, void * data);
static void         tree_view_row_activated_cb      (GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, void * data);
static gint         selection_entry_key_press_event_cb(GtkWidget *widget, GdkEventKey *event, void * data);
static gint         selection_entry_insert_text_cb  (GtkWidget *widget, const gchar *new_text, gint new_text_length, gint *position, void * data);
static gboolean     selection_entry_focus_in_event_cb(GtkWidget *widget, GdkEventFocus *event, void * data);
static void         home_button_clicked_cb          (GtkWidget *button, void * data);

static gboolean     jfile_chooser_tree_select_func  (GtkTreeSelection *selection, GtkTreeModel *model, GtkTreePath *path, gboolean path_currently_selected, void * data);
static void         jfile_chooser_free_selected_filenames(void * selected_filename, void * data);

    #ifdef G_OS_UNIX

static void         unix_add_menu_items             (GtkWidget *menu);

    #endif

    #ifdef G_OS_WIN32

static void         win32_add_menu_items            (GtkWidget *menu);
static void         win32_populate_filelist         (GtkJFileChooser *jfile_chooser, GtkLookinOptionMenuData *lookin_option_menu_data, GPtrArray *file_list_data);
static void         win32_add_drives                (GtkWidget *menu);
static gchar        *win32_get_all_desktop          (const gchar *pathname);

    #endif

#endif /* PRIVATE */
#endif /* __GTKJFILECHOOSER_H__ */
