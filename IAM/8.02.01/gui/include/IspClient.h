/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __ISPCLIENT_H__
#define __ISPCLIENT_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                IC_StartIsp                     (gint argc, gchar **argv);
gint                IC_PublishIspMsg                (gchar *message, gchar *symbol);
void                IC_DisconnectIsp                (void);
void                IC_DisplayIspStatus             (GtkWidget *parent);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         accept_cb                       (ItContext context, ItAcceptPacketStruct *packet, ItCallbackData *data);

static void         subscribe_cb                    (ItContext context, ItSubscribePacketStruct *packet, ItCallbackData *data);

static void         cycle_cb                        (ItContext context, ItStatusEventStruct *packet, ItCallbackData *data);
static void         status_cb                       (ItContext context, ItStatusEventStruct *packet, ItCallbackData *data);

static void         value_cb                        (ItContext context, ItValueEventStruct *packet, ItCallbackData *data);

static void         message_cb                      (ItContext context, ItMessageEventStruct *packet, ItCallbackData *data);
static void         disconnect_cb                   (ItContext context, ItDisconnectEventStruct *packet, ItCallbackData *data);

static void         connectISP                      (void);
static void         disconnectISP                   (void);
static gboolean     reconnectISP                    (void * data);

static void         addTimeout                      (void);
static void         removeTimeout                   (void);
static void         removeIOWatch                   (void);

static void         addIOWatch                      (void);
static gboolean     readSocket                      (GIOChannel *source, GIOCondition condition, void * data);

static void         setPort                         (void);
static gint         getPort                         (gint argc, gchar **argv);

#endif /* PRIVATE */
#endif /* __ISPCLIENT_H__ */
