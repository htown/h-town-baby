/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __DIALOG_H__
#define __DIALOG_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/**
 * Defines the step by functions
 */
enum StepBy
{
    StepByPoint,        /**< Step by data point */
    StepByMinute        /**< Step by time */
};

/**
 * Step_by menu item names
 */
#define STEP_BY_DATA        "Step by Data Point"
#define STEP_BY_MINUTE      "Step by Minute"

/**
 * Defines the action to take when the selects the next
 * or previous button in the predict dialog.
 */
enum StepOperation
{
    StepNext,           /**< Move forward through the event data */
    StepPrev            /**< Move backward through the event data */
};

/**
 * Defines the show states for cone or los
 */
enum ShowConeLos
{
    eSHOW_CONE,         /**< show cone */
    eSHOW_LOS           /**< show los */
};

/**
 * Show_cone menu item names
 */
#define SHOW_CONE           "Display Cone"
#define SHOW_LOS            "Display Los"

/**
 * This structure contains all of the invalidated areas that need to be
 * refreshed in the graphics display
 */
#define MAX_AREAS    20

/**
 * Defines the index values used for the area struct
 */
enum AreaEnum
{
    AREA_X,             /**< x index */
    AREA_Y,             /**< y index */
    AREA_W,             /**< w index */
    AREA_H,             /**< h index */

    AREA_RECT
};

/**
 * Defines area in drawable for clearing
 */
typedef struct Area
{
    gint    count;      /**< the number of rect's */
    gint    rect[MAX_AREAS][AREA_RECT]; /**< the rectangle area */
} Area;

/**
 * Defines the direction for extra drawing areas
 */
enum Direction
{
    HORIZONTAL_DIRECTION,   /**< display horizontally */
    VERTICAL_DIRECTION      /**< display vertically */
};

/**
 * Defines the resize font resource
 */
typedef struct ResizeFont
{
    gchar           rc_string[1024];        /**< resource string for parsing */

    gint            default_font_size;      /**< initial font size */
    gint            previous_font_size;     /**< last font */

    gint            default_width;          /**< initial window width */
    gint            previous_width;         /**< last width */
    gint            new_width;              /**< new width */

    gint            default_height;         /**< initial window height */
    gint            previous_height;        /**< last height */
    gint            new_height;             /**< new height */
} ResizeFont;

/**
 * Handler ids for some widget instance
 */
typedef struct WidgetHandlerData
{
    void * instance;                      /**< widget instance */
    gulong   handler_id;                    /**< signal handler id */
} WidgetHandlerData;

/**
 *  Define event sizing data for resize and expose events
 */
typedef struct EventSizeData
{
    gint x;
    gint y;
    gint width;
    gint height;
} EventSizeData;

/**
 * Defines the width used for the X and Y label areas in
 * the kuband and station views.
 */
#define LABEL_AREA_WIDTH    50
#define LABEL_AREA_HEIGHT   50

/**
 * Defines a small icon size
 */
#define SMALL_ICON_SIZE     16

/**
 * label names for the time button
 */
#define GMT_LABEL           "GMT"
#define UTC_LABEL           "UTC"

/* labels used in the realtime dialog */
#define REFRESH_LABEL       "Refresh"
#define CLEAR_LABEL         "Clear"

/**
 * The degree symbol (utf-8)
 */
#define DEGREE_SYMBOL       "\302\260"
#define BULLET_SYMBOL       "\342\210\231"

/**
 * default font size is 8 point
 * smaller font size is 6 point
 */
#define FONT_KEY            "font-key"      /**< hash key for accessing data in the widget */
#define RESIZE_KEY          "resize-key"    /**< hash key for resize data */

/**
 * Define menu items that may need to be accessed later
 * menu bar menu definitions
 */
#define TOOLBAR                         "ToolBar"
#define MENU_BAR                        "MenuBar"
#define FILE_MENU                       "FileMenu"
#define BLOCKAGE_MENU                   "BlockageMenu"
#define SETTINGS_MENU                   "SettingsMenu"

/**
 * File menu items
 */
#define REALTIME_DISPLAY_MENU           "RealtimeDisplay"
#define PREDICT_EVENTS_MENU             "PredictEvents"
#define PRINT_PREDICT_MENU              "PrintPredict"
#define PRINT_PREDICT_BLOCKAGE_MENU     "PrintPredictBlockage"
#define GENERATE_PREDICTION_DATA_MENU   "GeneratePredictionData"
#define CHECK_DATA_AUTOMATICALLY_MENU   "CheckDataAutomatically"
#define SEND_PREDICTION_DATA_MENU       "SendPredictionData"

#define PREDICT_DISPLAY_MENU            "PredictDisplay"
#define REALTIME_DATA_DISPLAY_MENU      "RealtimeDataDisplay"
#define PRINT_REALTIME_MENU             "PrintRealtime"
#define CHECK_ISP_STATUS_MENU           "CheckISPStatus"

#define CLOSE_MENU                      "Close"
#define EXIT_MENU                       "Exit"

/**
 * Settings menu items
 */
#define TRACK_LIMITS_MENU               "TrackLimits"
#define SHOW_PREDICTS_MENU              "ShowPredicts"
#define SHOW_SOLAR_TRACE_MENU           "ShowSolarTrace"

#define SET_BASE_PET_MENU               "SetBasePET"
#define STEP_BY_MENU                    "StepBy"

#define COORDINATE_TRACKING_MENU        "CoordinateTracking"
#define CHANGE_ANTENNA_LOCATION_MENU    "ChangeAntennaLocation"
#define CHANGE_COVERAGE_DIRECTORY_MENU  "ChangeCoverageDirectory"
#define CHANGE_DYNAMIC_STRUCTURE_MENU   "ChangeDynamicStructure"
#define RELOAD_CONFIG_FILE_MENU         "ReloadConfigFile"

/**
 * Defines the boresights supported
 */
enum Boresight
{
    Boresight_Plus = 0,
    Boresight_Parens
};
#define CHANGE_BORESIGHT                "ChangeBoresight"
#define CHANGE_BORESIGHT_TO_PLUS_MENU   "ChangeBoresightToPlus"
#define CHANGE_BORESIGHT_TO_PARENS_MENU "ChangeBoresightToParens"

/**
 * Defines the view orientation strings
 */
#define HORIZONTAL_STR                  "horizontal"
#define VERTICAL_STR                    "vertical"

/**
 * Defines the orientation types
 */
enum Orientation
{
    View_Orientation_Horizontal = 0,    /**< horizontal view */
    View_Orientation_Vertical           /**< vertical view */
};
#define CHANGE_VIEW_ORIENTATION                     "ChangeViewOrientation"
#define CHANGE_VIEW_ORIENTATION_TO_HORIZONTAL_MENU  "ChangeViewOrientationToHorizontal"
#define CHANGE_VIEW_ORIENTATION_TO_VERTICAL_MENU    "ChangeViewOrientationToVertical"

#define CHANGE_KUBAND_VIEW              "ChangeKubandView"
#define CHANGE_KUBAND_VIEW_TO_ONE_MENU  "ChangeKubandViewToOne"
#define CHANGE_KUBAND_VIEW_TO_TWO_MENU  "ChangeKubandViewToTwo"

/**
 * Blockage menu items
 */
#define DISPLAY_CONE_MENU               "DisplayCone"
#define SHOW_LIMITS_MENU                "ShowLimits"
#define SHOW_SHUTTLE_DOCKED_MENU        "ShowShuttleDocked"

#define SHOW_JOINT_ANGLES_MENU          "ShowJointAngles"

#define SBAND1_MENU                     "SBand1Menu"
#define SBAND2_MENU                     "SBand2Menu"
#define KUBAND_MENU                     "KuBandMenu"
#define KUBAND2_MENU                    "KuBand2Menu"
#define SHOW_KUBAND_MENU                "ShowKuBandMenu"
#define STATION_MENU                    "StationMenu"

/**
 * Help menu items
 */
#define HELP_MENU                       "HelpMenu"
#define HELP_HELP_MENU                  "Help"
#define HELP_GTK_MENU                   "About GTK+"
#define HELP_ABOUT_MENU                 "About IAM"

/*
==============
Public Methods
==============
*/
void                D_DialogConstructor             (void);

void                D_SetIcon                       (GtkWidget *window);

void                D_CreatePixmap                  (WindowData *windata);

void                D_DrawDrawable                  (WindowData *windata);

void                D_AddArea                       (WindowData *windata, Area *area,
                                                     gint x, gint y, gint width, gint height);
void                D_RefreshAreas                  (WindowData *windata,
                                                     GdkDrawable *dst_drawable, GdkDrawable *src_drawable,
                                                     Area *area, gboolean isResizing);

gchar                *D_FgColor                      (void);
gchar                *D_BgColor                      (void);
gchar                *D_DegreeColor                  (void);

GtkWidget           *D_CreateDataFrame              (GtkWidget *widget);
GtkWidget           *D_CreateDataFrameWithLabel     (gchar *frame_title, GtkWidget *widget);

GtkWidget           *D_CreateLabel                  (gchar *label_name, gdouble horizontal_alignment, gchar *text_color);

gchar                *D_CreateMenu                   (const gchar *menu1, const gchar *menu2, const gchar *menu3,
                                                     const gchar *menu4, const gchar *menu5);

GtkActionGroup      *D_AddBlockageItemsToMenubar    (gint display, gint coord, GtkUIManager *ui, gchar *blockage_name, GCallback callback);
void                D_AddBlockageItemsToToolbar     (gint display, gint coord, GtkWidget *toolbar, GList **tool_items, gchar *blockage_name, GCallback callback);
void                D_UpdateBlockageItems           (GtkUIManager *thisUImanager, gint display, gint coord, gint kuband_displayed);

GtkActionGroup      *D_AddStructureItemsToMenuBar   (gint display, GtkUIManager *ui, gchar *menu_name, GCallback callback);

void                D_EnableMenuItem                (GtkUIManager *ui, gchar *menu_item, gboolean enable);
void                D_PreselectMenuItem             (GtkUIManager *ui, gchar *menu_item);

void                D_ContainerAdd                  (const gchar *name, GtkWidget **container, GtkWidget **left_frame, GtkWidget **bottom_frame, GtkWidget **center_frame);

GtkWidget           *D_CreateNotebook               (GtkWidget *container);

void                D_NotebookAdd                   (GtkWidget *notebook, gchar *tab_name, GtkWidget **frame);
GtkWidget           *D_NotebookAdd2                 (GtkWidget *notebook, gchar *tab_name, gint view_orientation,
                                                     GtkWidget **lt_widget, GtkWidget **rb_widget);
void                D_NotebookAdd3                  (GtkWidget *notebook, gchar *tab_name,
                                                     GtkWidget **left_frame, GtkWidget **bottom_frame, GtkWidget **center_frame);
GtkWidget           *D_NotebookAdd4                 (GtkWidget *notebook, gchar *tab_name, gint view_orientation,
                                                     GtkWidget **sband_frame,
                                                     GtkWidget **left_frame, GtkWidget **bottom_frame, GtkWidget **center_frame);

GtkWidget           *D_CreateDrawingArea            (GtkWidget **frame, const gchar *name, GtkWidget *handle_box, GtkWidget *handle_box2, GtkWidget *toolbar, GtkWidget *toolbar2, gint width, gint height);
GtkWidget           *D_CreateDrawingAreaWithExtra   (GtkWidget **frame, gint direction, gint width, gint height);

void                D_SetDialogTitle                (GtkWidget *window, gchar *which_dialog, gint coord);
GtkWidget           *D_CreateArrowButton            (GtkArrowType arrow_type, GtkShadowType shadow_type);

gint                 D_ResizeFont                    (const gchar *widget_name, ResizeFont *resize_font);

void                D_SetButtonMaskVisual           (WindowData *windata, gboolean enable, const gchar *mask_name, gint coord);
void                D_SetBlockageMaskByAction       (GtkUIManager *ui, gchar *menu_bar, gchar *blockage_menu, gchar *mask_menu, const gchar *mask_name);

void                D_SetLabelText                  (GtkWidget *obj, gchar *text, GdkColor *gcolor);
void                D_SetTextColor                  (GtkWidget *widget, gchar *text_color, gchar *bg_color);
void                D_SetLabelColor                 (GtkWidget *widget, gchar *text_color);

void                D_CloseDialog                   (gint which_dialog);
void                D_ExitIam                       (void);

void                D_SetDefaultView                (gint which_dialog, GtkWidget *notebook);

gboolean            D_DrawCoordinateTracking_cb     (GtkWidget *widget, GdkEventMotion *event, void * data);
void                D_UnblockMotionHandlers         (WidgetHandlerData *whd);
void                D_BlockMotionHandlers           (WidgetHandlerData *whd);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         copy_drawable                   (WindowData *windata, GdkGC *gc, GdkDrawable *dst, GdkDrawable *src);

static void         set_menu_items                  (GtkUIManager *ui,
                                                     gint display,
                                                     gint coord,
                                                     gchar *blockage_name,
                                                     gboolean visible);

static void         set_cursor_color                (GtkWidget *widget, GdkColor *cursor_color);

static WindowData   *get_window_data_by_widget      (GtkWidget *widget);

#endif /* PRIVATE */
#endif /* __DIALOG_H__ */
