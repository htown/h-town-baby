/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __CONVERSIONS_H__
#define __CONVERSIONS_H__

#include <gtk/gtk.h>

/*
=================
Public Defintions
=================
*/

#define  MAX_POLAR_RADIUS         150
#define  MIN_KU_ELEVATION        -135
#define  MAX_KU_ELEVATION         135
#define  MIN_KU_CROSS_ELEVATION   -75
#define  MAX_KU_CROSS_ELEVATION    75
#define  MIN_STATION_AZIMUTH     -180
#define  MAX_STATION_AZIMUTH      180
#define  MIN_STATION_ELEVATION    -90
#define  MAX_STATION_ELEVATION     90

/**
 * This is Epsilon from ADA, used to represent a very small number
 * or delta
 */
#define  EPSILON 0.0000001

/**
 * Macro that converts degrees to radians
 */
#define DEGREES_TO_RADIANS(x)   ( (x) * (G_PI / 180.0) )

/**
 * Macro that converts radians to degrees
 */
#define RADIANS_TO_DEGREES(x)   ( (x) * (180.0 / G_PI) )

/**
 * Macro that rounds a value to the nearest whole number
 */
#define ROUND(x)                ( (x) < 0.0 ? (x)-0.5 : (x)+0.5 )

/*
==============
Public Methods
==============
*/
gboolean            LoadStaticBiasMats              (void);
gint                 StationAEtoSBandAE              (gint, gint, gint *, gint * );
gint                 StationAEtoKuBand               (gint, gint, gint *, gint *, gint );
gint                 StationAEtoGeneric              (gint, gint, gint *, gint * );
gint                 XYZtoSBandAE                    (gdouble, gdouble, gdouble, gint *, gint * );
gint                 XYZtoKuBand                     (gdouble, gdouble, gdouble, gint *, gint *, gint );
void                XYZtoStationAE                  (gdouble, gdouble, gdouble, gint *, gint * );
gint                 XYZtoGenericAE                  (gdouble, gdouble, gdouble, gint *, gint * );
void                SBandAEtoStationXYZ             (gint x, gint y, gdouble *sx, gdouble *sy, gdouble *sz);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gboolean     loadBiasMats                    (gchar *keyword, gdouble *col1, gdouble *col2, gdouble *col3);
static void         StationAEtoXYZ                  (gint, gint, gdouble *, gdouble *, gdouble * );

#endif /* PRIVATE */
#endif /* __CONVERSIONS_H__ */
