/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __REALTIMEDATAHANDLER_H__
#define __REALTIMEDATAHANDLER_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #include "DynamicDraw.h"
    #include "ColorHandler.h"
    #include "IspSymbols.h"
    #define PRIVATE
#else
    #include "AntMan.h"
    #include "DynamicDraw.h"
    #include "ColorHandler.h"
    #include "IspSymbols.h"
#endif

/*
=================
Public Defintions
=================
*/

/* limit dialog default limit */
#define  LD_DEFAULT_LIMIT   15

/**
 * struct TimePoint
 * This type contains the information for a point on a realtime track.
 */
typedef struct TimePoint
{
    Point   pt[NUM_COORDS];         /**< point position                           */
    gchar    timeStr[TIME_STR_LEN];  /**< point time in UTC format                 */
    glong    timeSecs;               /**< point time in total seconds              */
    gint     inView;                 /**< point status:                            */
                                    /**<     0=not in view, 1=in view, 2=selected */
} TimePoint, *PTimePoint;

/**
 * Defines the state of a blockage mask, enable/disabled
 */
typedef struct BlockageMask
{
    gint            masknum;
    gboolean        active;
} BlockageMask;

/**
 * Types for tdrs
 */
enum TDRS_Type
{
    AZ_TYPE,
    EL_TYPE,
    MAX_TDRS_TYPES
};

/**
 * Enumerations for the inView flag
 */
enum INView
{
    RTD_NotInView,
    RTD_InViewNotSelected,
    RTD_Selected,
    RTD_ViewMax
};

/**
 * Enumerations for MSID in each group
 */
enum TDRS
{
    TD_TDRW,
    TD_TDRE,
    TD_SUN,
    MAX_TDRS
};

/**
 * Index into the tdrs data
 */
enum TDRS_Slot
{
    TDRS_ID,

    TDRS_PRED_AZ,
    TDRS_PRED_EL,

    TDRS_CMD_AZ,
    TDRS_CMD_EL,

    TDRS_AOS,
    TDRS_LOS,

    MAX_TDRS_SLOT
};

/**
 * flags for kuband spiral search
 */
enum ContRetry
{
    DISABLED = 0,
    ENABLED  = 1
};

/**
 * Defines the different antenna modes supported
 */
enum AntennaMode
{
    INHIBIT    = 0,
    OPENLOOP   = 1,
    AUTOTRACK  = 2,
    ERROR_MODE = 3
};

/**
 * This type contains the information for a realtime track.
 */
typedef struct RtTrack
{
    gchar        label[8];    /**< diaplayed label for track */
    PTimePoint  pts;         /**< dynamic array (queue) of points */
    gint         size;        /**< size of array */
    gint         head;        /**< index of head of queue */
    gint         tail;        /**< index of tail of queue */
} RtTrack;

/*
==============
Public Methods
==============
*/
void                RTDH_Constructor                (void);
void                RTDH_Destructor                 (void);

void                RTDH_LoadRtPredicts             (gint coord, gint tdrs, gint force);
void                RTDH_CheckRtLimits              (void);
void                RTDH_UpdateLimits               (gint limit);
gboolean            RTDH_CalculateRtPoint           (SymbolRec *symbol, gdouble *x, gdouble *y, gdouble *z, gchar *pt_time);

void                RTDH_NearestPoint               (gint coord, gint width, gint height, gint window_x, gint window_y, gint *x, gint *y);

PredictSet          *RTDH_GetPredicts               (gint tdrs);
GList               *RTDH_GetCurPredictPt           (gint tdrs);
gint                RTDH_GetSelectedTdrs            (void);
JointAngle          *RTDH_GetJointAngle             (void);
gint                RTDH_GetTrackColor              (gint in_view);
void                RTDH_ResetPredicts              (void);
void                RTDH_ClearAll                   (void);

void                RTDH_SetValue                   (SymbolRec *symbol);

void                RTDH_SetTdrsLabel               (gint tdrs, gchar *value);
void                RTDH_AddPoint                   (gint tdrs, gdouble x, gdouble y, gdouble z, gint inView);

gint                RTDH_GetAngleIndex              (gint index);

BlockageMask        *RTDH_GetBlockageMasks          (gint coord);

RtTrack             *RTDH_GetTrack                  (gint tdrs);

gdouble             RTDH_GetLGABoresightAz          (void);
gdouble             RTDH_GetLGABoresightEl          (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         initialize_tracks               (void);

static void         advance_head                    (RtTrack *track);
static void         copy_rt_track                   (RtTrack *track, gint size);
static void         clear_segment                   (RtTrack *track, gint head);

static gint         get_angle_index                 (gint symbol_id);

#endif /* PRIVATE */
#endif /* __REALTIMEDATAHANDLER_H__ */
