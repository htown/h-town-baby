/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/
#ifndef __XML_PARSE_H__
#define __XML_PARSE_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/* define predict types that can be processed */
enum PREDICT_TYPES
{
    TDRS_PREDICT = 0,
    SUNPOS_PREDICT,
    SUNABG_PREDICT,
    SHO_PREDICT,
    MAX_PREDICT_TYPES
};
#define VALID_TIMER(t) (t<MAX_PREDICT_TYPES)

/*
==============
Public Methods
==============
*/
gboolean            XML_HaveNewPredict              (gchar *xmlConfigLookupKey, time_t *predictFileTime);
gboolean            XML_HasTDRSPredictPoints        (void);
void                XML_ProcessData                 (gint whichPredict);
void                XML_StartTimer                  (gint whichTimer, GSourceFunc timerFunc);
void                XML_StopTimer                   (gint whichTimer);
void                XML_SetStartTime                (gchar *startTime);
void                XML_SetStopTime                 (gchar *stopTime);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/
static FILE         *openPredictFile                (gchar *whichPredictType);
static gchar        *getPredictFile                 (gchar *whichPredictType);

static glong        getStopTime                     (void);
static glong        getStartTime                    (void);
static glong        getTime                         (gchar *timeStr);
static gchar        *timeToUtcStr                   (gchar *timeStr);
static glong        utcStrToSecs                    (gchar *timeStr);

static gint         getAttrIndex                    (gchar *attrKey, const gchar **attr);

static guint        getWaitTime                     (void);

static void         loadTdrsDataXML                 (void);
static void         startEvent                      (void *data, const gchar *el, const gchar **attr);
static void         endEvent                        (void *data, const gchar *el);
static void         processEvent                    (void *data, const gchar **attr);
static void         addEvent                        (void *data, gchar *tdrs, gchar *tdrsId);
static GList        *removeEventsFromList           (GList *sets);

static void         processView                     (void *data, const gchar **attr);
static void         addTdrsInfo                     (gchar *name, gchar *id, gchar *shoName);

static void         processPredict                  (void *data, const gchar **attr);
static void         addPredict                      (PredictSet *ps, gchar *timeStr, gchar *az, gchar *el);

static void         loadShoData                     (void);
static GList        *getBestMatch                   (GList *sets, gchar *sat, gchar *startSho, gchar *endSho);
static gboolean     isShoProcessed                  (GList *sets, GList *bestSet, PredictSet *psNew);
static gboolean     isShoUnset                      (PredictSet *bestPs, gchar *startSho, gchar *endSho, gchar *status, gint orbit);
static void         checkOrbitNumbers               (GList *sets);

static void         loadSunPosDataXML               (void);
static void         startSunParser                  (void *data, const gchar *el, const gchar **attr);
static void         endSunParser                    (void *data, const gchar *el);
static void         addSunPos                       (gchar *timeStr, gdouble x, gdouble y, gdouble z);

static void         loadSunAbgDataXML               (void);
static void         startAngleParser                (void *data, const gchar *el, const gchar **attr);
static void         endAngleParser                  (void *data, const gchar *el);
static void         addAngle                        (gchar *timeStr, gdouble angle2B, gdouble angle4B, gdouble angle2A,
                                                     gdouble angle4A, gdouble anglePA, gdouble angleSA, gdouble angle1A,
                                                     gdouble angle3A, gdouble angle1B, gdouble angle3B, gdouble anglePTCS,
                                                     gdouble angleSTCS);
/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __XML_PARSE_H__ */
