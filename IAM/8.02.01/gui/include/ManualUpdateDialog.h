/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __MANUALUPDATEDIALOG_H__
#define __MANUALUPDATEDIALOG_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                MUD_Create                      (GtkWindow *parent);
void                MUD_Open                        (void);
gboolean            MUD_GenerateTime                (gchar *start_time, gchar *stop_time);
gchar               *MUD_GetTime                    (glong time_type);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         responseCb                      (GtkWidget *widget, gint response_id, void * data);
static gboolean     keyPressCb                      (GtkWidget *widget, GdkEventKey *event, void * data);
static gboolean     loadAutoConfigData              (gchar *startTime, gchar *stopTime);
static gboolean     validInputData                  (void);

#endif /* PRIVATE */
#endif /* __MANUALUPDATEDIALOG_H__ */
