/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __GTKJFILECHOOSERICONS_H__
#define __GTKJFILECHOOSERICONS_H__

/**
 * computer icon
 */
static const gchar *computer_icon_xpm[] = {
"32 32 5 1",
"  c black",
". c gray40",
"X c gray80",
"o c #CCCCFF",
"O c gray100",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOO          OOOOOOOOOOO",
"OOOOOOOOOO XXXXXXXXXX OOOOOOOOOO",
"OOOOOOOOOO XX      XX OOOOOOOOOO",
"OOOOOOOOOO X oooooo X OOOOOOOOOO",
"OOOOOOOOOO X oooooo X OOOOOOOOOO",
"OOOOOOOOOO X oooooo X OOOOOOOOOO",
"OOOOOOOOOO X oooooo X OOOOOOOOOO",
"OOOOOOOOOO XX      XX OOOOOOOOOO",
"OOOOOOOOOOO XXXXXXXX OOOOOOOOOOO",
"OOOOOOOOO              OOOOOOOOO",
"OOOOOOOOO XXXXXXXXXXXX OOOOOOOOO",
"OOOOOOOOO XXXX...X...X OOOOOOOOO",
"OOOOOOOOO XXXXXXXXXXXX OOOOOOOOO",
"OOOOOOOOO              OOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"
};

/**
 * desktop icon
 */
static const gchar *desktop_icon_xpm[] = {
"32 32 5 1",
"  c black",
". c gray40",
"X c gray80",
"o c #CCCCFF",
"O c gray100",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOO . O  OOOOOOOOOOO",
"OOOOOOOOOOOOOO ...   OOOOOOOOOOO",
"OOOOOOOOOOOOO .....  OOOOOOOOOOO",
"OOOOOOOOOOOO ....... OOOOOOOOOOO",
"OOOOOOOOOOO ......... OOOOOOOOOO",
"OOOOOOOOOO  .........  OOOOOOOOO",
"OOOOOOOOO O ooooooooo O OOOOOOOO",
"OOOOOOOOOOO oo     oo OOOOOOOOOO",
"OOOOOOOOOOO oo XXX oo OOOOOOOOOO",
"OOOOOOOOOOO oo XXX oo OOOOOOOOOO",
"OOOOOOOOOOO oo XX. oo OOOOOOOOOO",
"OOOOOOOOOOO oo XXX oo OOOOOOOOOO",
"OOOOOOOOOOO oo XXX oo OOOOOOOOOO",
"OOOOOOOOOOO           OOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"
};

/**
 * disk icon
 */
static const gchar *disk_icon_xpm[] = {
"32 32 4 1",
"  c black",
". c gray60",
"X c gray80",
"o c gray100",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooo        oooooooooooo",
"oooooooooo  ooXoooXo  oooooooooo",
"ooooooooo oooooXoXXXX. ooooooooo",
"ooooooooo ooXXXXXX.X.. ooooooooo",
"oooooooooo  oXX.X.X.  oooooooooo",
"ooooooooo oo        X. ooooooooo",
"ooooooooo ooXXXXXX.X.. ooooooooo",
"oooooooooo  oXX.X.X.  oooooooooo",
"ooooooooo oo        X. ooooooooo",
"ooooooooo ooXXXXXX.X.. ooooooooo",
"oooooooooo  oXX.X.X.  oooooooooo",
"oooooooooooo        oooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo"
};

/**
 * list icon
 */
static const gchar *list_icon_xpm[] = {
"32 32 3 1",
"  c black",
". c #CCCCFF",
"X c gray100",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXX    XXXX    XXXXXXXXXXX",
"XXXXXXXXX ... XXX ... XXXXXXXXXX",
"XXXXXXXXX .X. XXX .X. XXXXXXXXXX",
"XXXXXXXXX .X. X X .X. X XXXXXXXX",
"XXXXXXXXX ... XXX ... XXXXXXXXXX",
"XXXXXXXXX     XXX     XXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXX    XXXX    XXXXXXXXXXX",
"XXXXXXXXX ... XXX ... XXXXXXXXXX",
"XXXXXXXXX .X. XXX .X. XXXXXXXXXX",
"XXXXXXXXX .X. X X .X. X XXXXXXXX",
"XXXXXXXXX ... XXX ... XXXXXXXXXX",
"XXXXXXXXX     XXX     XXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
};

/**
 * details icon
 */
static const gchar *details_icon_xpm[] = {
"32 32 3 1",
"  c black",
". c #CCCCFF",
"X c gray100",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXX    XXXXXXXXXXXXXXXXXX",
"XXXXXXXXXX ... XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX .X. XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX .X. X        XXXXXXXX",
"XXXXXXXXXX ... XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX     XXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXX    XXXXXXXXXXXXXXXXXX",
"XXXXXXXXXX ... XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX .X. XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX .X. X        XXXXXXXX",
"XXXXXXXXXX ... XXXXXXXXXXXXXXXXX",
"XXXXXXXXXX     XXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
};

/**
 * document icon
 */
static const gchar *document_icon_xpm[] = {
"32 32 3 1",
"  c black",
". c #CCCCFF",
"X c gray100",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXX          XXXXXXXXXXXX",
"XXXXXXXXXX .......  XXXXXXXXXXXX",
"XXXXXXXXXX .XXXXX .X XXXXXXXXXXX",
"XXXXXXXXXX .XXXXXX . XXXXXXXXXXX",
"XXXXXXXXXX .XXXXXXX . XXXXXXXXXX",
"XXXXXXXXXX .XXXXXXXX  XXXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX  XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX .XXXXXXXXX. XXXXXXXXX",
"XXXXXXXXXX ........... XXXXXXXXX",
"XXXXXXXXXX             XXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
};

/**
 * new folder icon
 */
static const gchar *newfolder_icon_xpm[] = {
"32 32 4 1",
"  c black",
". c #666699",
"X c #CCCCFF",
"o c gray100",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooo.....ooooooooo",
"ooooooooooooooooo......ooooooooo",
"ooooooooo        XXXXX ooooooooo",
"oooooooo oooooooo      ooooooooo",
"oooooooo oXXXXXXXooooo ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo               ooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo"
};

/**
 * folder icon
 */
static const gchar *folder_icon_xpm[] = {
"32 32 4 1",
"  c black",
". c #666699",
"X c #CCCCFF",
"o c gray100",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooo.....ooooooooo",
"ooooooooooooooooo......ooooooooo",
"ooooooooo        XXXXX ooooooooo",
"oooooooo oooooooo      ooooooooo",
"oooooooo oXXXXXXXooooo ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo               ooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo"
};

/**
 * up one level icon
 */
static const gchar *uponelevel_icon_xpm[] = {
"32 32 4 1",
"  c black",
". c #666699",
"X c #CCCCFF",
"o c gray100",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooo.....ooooooooo",
"ooooooooooooooooo......ooooooooo",
"ooooooooo        XXXXX ooooooooo",
"oooooooo oooooooo      ooooooooo",
"oooooooo oXXXXXXXooooo ooooooooo",
"oooooooo oXXXXXXXXXXXX ooooooooo",
"oooooooo oXXXXX XXXXXX ooooooooo",
"oooooooo oXXXX   XXXXX ooooooooo",
"oooooooo oXXX     XXXX ooooooooo",
"oooooooo oXX       XXX ooooooooo",
"oooooooo oXXXXX XXXXXX ooooooooo",
"oooooooo               ooooooooo",
"ooooooooooooooo oooooooooooooooo",
"ooooooooooooooo oooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo",
"oooooooooooooooooooooooooooooooo"
};

#endif /* __GTKJFILECHOOSERICONS_H__ */
