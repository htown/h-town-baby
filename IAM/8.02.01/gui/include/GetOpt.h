/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written premission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

/************** RCS Keywords **********************************************
 *
 *
 * $Header$
 *
 *
 * $Revision$
 *
 *
 * $Log$
 *
 *
***************************************************************************/

#ifndef __GETOPT_H__
#define __GETOPT_H__

int                 GetOpt                          (int argc, char **argv, char *opts);

#ifdef PRIVATE

#endif /* PRIVATE */
#endif /* __GETOPT_H__ */
