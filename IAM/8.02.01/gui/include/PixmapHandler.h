/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PIXMAPHANDLER_H__
#define __PIXMAPHANDLER_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                PH_Constructor                  (void);
void                PH_Destructor                   (void);
void                PH_DrawBackground               (WindowData *windata, gint display, gint coord_view);
void                PH_DrawLeftBottomPixmap         (WindowData *windata, gint display, gint which_drawing_area);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gint          getDrawingAreaFromCoordView     (gint coord_view);

static void         setParams                       (WindowData *windata);
static void         resetParams                     (void);

static void         createSBand                     (WindowData *windata, GdkDrawable *drawable);
static void         drawSBandCircle                 (GdkDrawable *, GdkLineStyle line_style, gint w, gint h, gint diameter);
static void         drawSBandLine                   (GdkDrawable *, gint w, gint h, gint angle, gboolean draw_radius);
static void         drawSBandLabels                 (GdkDrawable *, gint w, gint h);

static void         createKuBand                    (WindowData *windata, gint which_drawing_area, GdkDrawable *drawable);
static void         drawKuBand                      (GdkDrawable *);
static void         drawKuBandLeft                  (GdkDrawable *);
static void         drawKuBandBottom                (GdkDrawable *);

static void         createStation                   (WindowData *windata, gint which_drawing_area, GdkDrawable *drawable);
static void         drawStation                     (GdkDrawable *);
static void         drawStationLeft                 (GdkDrawable *);
static void         drawStationBottom               (GdkDrawable *);

#endif /* PRIVATE */
#endif /* __PIXMAPHANDLER_H__ */
