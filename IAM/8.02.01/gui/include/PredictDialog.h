/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PREDICTDIALOG_H__
#define __PREDICTDIALOG_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "MaskFile.h"
    #include "Dialog.h"
    #define PRIVATE
#else
    #include "MaskFile.h"
    #include "Dialog.h"
#endif

/*
=================
Public Defintions
=================
*/
/* millisecond timers used for handling repeat timers */
/* the first timer starts the ball rolling, if the button is held down, */
/* then the second stage delay is applied, after a certain count(50) */
/* the third stage is applied. these timers also have to work for single */
/* push of a button. */
/* ****IMPORTANT**** if the stage 0 delay is set to a small value */
/* then the timer event could be called multiple times before stopping!!! */
#define REPEAT_STAGE0_DELAY 250
#define REPEAT_STAGE1_DELAY 50
#define REPEAT_STAGE2_DELAY 5
#define REPEAT_TIMER_STOP 0
#define REPEAT_TIMER_CONTINUE 1

/* info for repeat timer typically attached to arrow buttons */
/* used by event dialog also */
typedef struct TimerInfo
{
    guint timer_id;
    gint timer_state;
    gint timer_count;
    void *user_data;
} TimerInfo;

/* info for arrow buttons, the sensitive state */
/* used by event dialog also */
typedef struct ButtonInfo
{
    gboolean prev;
    gboolean next;
} ButtonInfo;

/*
==============
Public Methods
==============
*/
gboolean            PD_Create                       (void);
void                PD_Destructor                   (void);
void                PD_Open                         (void);

GtkWidget           *PD_GetDialog                   (void);

void                PD_SetDialogTitle               (void);

gboolean            PD_IsActive                     (void);
gboolean            PD_IsResizing                   (void);

void                PD_UpdateEventArrows            (void);
gint                 PD_GetCoordinateType            (void);
Dimension           *PD_GetDrawingAreaSize          (void);

gboolean            PD_GetKubandIsActive           (void);

void                PD_SetRedraw                    (gboolean flag);
void                PD_DrawData                     (gboolean doText);

void                PD_DrawCoordPoints              (WindowData *windata, gchar *str, gint x, gint y);

void                PD_UpdateTime                   (gint action);
void                PD_UpdateTimeType               (gboolean update_time_type);

void                PD_ChangeStructure              (const gchar *action_name);

WindowData          *PD_GetWindowDataByWidget       (GtkWidget *widget);

PangoFontDescription *PD_GetPangoFontDescription    (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/**
 * Indexes into widget array for the components
 * that hold the points labels
 */
enum AZEL_PointEnum
{
    AZ_POINT,
    EL_POINT,

    MAX_POINTS
};

/*
===============
Private Methods
===============
*/


static void         createWindow                    (ResizeFont *resize_font);

static WindowData   *getWindowDataByCoordinate      (void);
static WindowData   *getWindowDataByView            (gint coord_view);

static void         createDrawingArea               (gint coord, gint view, gint drawing_area, gchar *title,
                                                     GCallback toolbar_callback, gint *expose_data);
static void         createDrawingArea3              (gint coord, gint view,
                                                     gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                                                     gchar *title, GCallback toolbar_callback, gint *notify_data,
                                                     gint *expose_data, gint *left_expose_data, gint *bottom_expose_data);

static void         setCoordinateView               (void);

static void         fileMenuActionCb                (GtkAction *action);
static void         settingsMenuActionCb            (GtkAction *action);
static void         blockageMenuActionCb            (GtkAction *action);
static void         helpMenuActionCb                (GtkAction *action);
static void         settings_menu_kuband_view_cb    (GtkAction *action, GtkRadioAction *current);

static void         structureChangeActionCb         (GtkAction *action, GtkRadioAction *current);

static void         sband1BlockageActionCb          (GtkAction *action);
static void         sband2BlockageActionCb          (GtkAction *action);
static void         kubandBlockageActionCb          (GtkAction *action);
static void         kuband2BlockageActionCb         (GtkAction *action);
static void         stationBlockageActionCb         (GtkAction *action);

static void         sband1BlockageClickedCb         (GtkToolButton *tool_button, void * user_data);
static void         sband2BlockageClickedCb         (GtkToolButton *tool_button, void * user_data);
static void         kubandBlockageClickedCb         (GtkToolButton *tool_button, void * user_data);
static void         kuband2BlockageClickedCb        (GtkToolButton *tool_button, void * user_data);
static void         stationBlockageClickedCb        (GtkToolButton *tool_button, void * user_data);

static void         set_kuband_view_to_start        ();
static void         set_kuband_displayed            (gint kuband_to_display);
static void	    ku_toggle_pressed_cb	    (GtkWidget *Widget, void * user_data);

static void         setButtonMaskVisual             (gint coord, guint mask_num, const gchar *mask_name);
static void         setBlockageMaskByName           (gint coord, const gchar *mask_name);
static void         setBlockageMask                 (gint coord, gint mask_num);
static void         setMasksEnabled                 (gint coord, gchar *config_keyword);

static void         createTopForm                   (void);

static void         enableGenPredictMenuItem        (void);
static void         enableFtpMenuItem               (void);

static gboolean     closeCb                         (GtkWidget *widget, GdkEvent *event, void * user_data);
static gboolean     resizeWindowCb                  (GtkWidget *widget, GdkEventConfigure *event, void * user_data);
static gboolean     resizeDrawingAreaCb             (GtkWidget *widget, GdkEventConfigure *event, void * user_data);
static gboolean     visibilityCb                    (GtkWidget *widget, GdkEvent *event, void * user_data);
static gboolean     exposeCb                        (GtkWidget *widget, GdkEventExpose *event, void * user_data);
static gboolean     buttonPressCb                   (GtkWidget *widget, GdkEventButton *event, void * user_data);
static gboolean     motionCb                        (GtkWidget *widget, GdkEventMotion *event, void * user_data);
static void         pageSwitchCb                    (GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data);
static void         pressedPointsCb                 (GtkWidget *widget, void * user_data);
static void         pressedEventsCb                 (GtkWidget *widget, void * user_data);
static gboolean     focusInEventCb                  (GtkWidget *widget, GdkEventFocus *event, void * user_data);
static gboolean     focusOutEventCb                 (GtkWidget *widget, GdkEventFocus *event, void * user_data);
static gboolean     keyPressCb                      (GtkWidget *widget, GdkEventKey *event, void * user_data);
static void         activateEventsCb                (GtkWidget *widget, void * user_data);
static void         activatePointsCb                (GtkWidget *widget, void * user_data);
static gboolean     keyPressEventCb                 (GtkWidget *widget, GdkEventKey *event, void * user_data);

static void         releasedCb                      (GtkWidget *widget, void *user_data);

static void         setPoints                       (guint which_direction);
static void         setEvents                       (guint which_direction);

static gint         eventsTimerFunc                 (void *user_data);
static gint         pointsTimerFunc                 (void *user_data);

static gboolean     setEventsRepeatTimer            (void);
static gboolean     setPointsRepeatTimer            (void);

static void         setPangoFontDesc                (ResizeFont *resize_font, PangoFontDescription *font_desc);

static void         setDisplayPrintFlag             (gint display);

static void         markPosition                    (gdouble x, gdouble y);

static void         timeCb                          (GtkWidget *widget, void * user_data);
static void         activateTimeCb                  (GtkWidget *widget, void * user_data);
static gboolean     focusOutTimeCb                  (GtkWidget *widget, GdkEventFocus *event, void * user_data);

static void         searchClickedCb                 (GtkButton *button, void * user_data);
static GtkWidget    *createSearchTimeDlg            (void);
static void         openSearchTimeDlg               (GtkWidget *dialog);
static void         searchActivateCb                (GtkWidget *widget, void * data);
static gboolean     searchKeyPressCb                (GtkWidget *widget, GdkEventKey *event, void * data);
static void         searchResponseCb                (GtkWidget *widget, gint response_id, void * data);
static gboolean     processSearchTime               (GtkWidget *parent);

static void         updateEventPoints               (void);
static void         updateEventName                 (void);
static void         updateEventTime                 (void);
static gboolean     updateEventTracks               (gint direction);
static void         updateTrackButtonHandlers       (gint direction, gboolean enabled);

static void         setAzElPoints                   (Point *current_pt, Point *solar_pt);

static void         makePixmapData                  (WindowData *windata, EventSizeData *event_data, gint which_drawing_area);
static void         drawPredictEvents               (WindowData *windata, gint coord_view, GList *sets);
static void         drawPredictEvent                (WindowData *windata, gint coord, GList *sets, GList *cpt, GdkDrawable *drawable);
static void         drawPredictBox                  (WindowData *windata, gint coord, GList *pt, GdkDrawable *drawable);
static void         drawDiamond                     (WindowData *windata, gint coord, GdkGC *gc, Point *p, GdkDrawable *drawablet);
static void         set_kuband_view_to_start        (void);



#endif /* PRIVATE */
#endif /* __PREDICTDIALOG_H__ */
