/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __TIMESTRINGS_H__
#define __TIMESTRINGS_H__


/*
=================
Public Defintions
=================
*/

/**
 * Enumeration for different time information.
 */
enum TimeEnum
{
    TS_UNKNOWN = -1,/**< undefined time format */
    TS_UTC = 0,     /**< utc time format */
    TS_GMT,         /**< gmt time format */
    TS_PET,         /**< pet time format */
    TS_ELAPSED      /**< elapsed time format */
};

/**
 * Enumeration for start or stop time
 */
enum TimeTypeEnum
{
    UD_START,
    UD_STOP
};

/**
 * Maximum length of any time string
 */
#define  TIME_STR_LEN   21

/**
 * Unset time string, all zeros
 */
#define  UNSET_TIME     "0000:00:00:00:00:00"

/*
==============
Public Methods
==============
*/
gchar                *GetTimeName                    (gint timeType);

gchar                *CurrentUtc                     (void);
void                ConvertToUTC                    (gchar *time_str);
glong                CurrentUtcSecs                  (void);
gchar                *GetBasePet                     (void);

void                SetGmt                          (gint secs);
void                SecsToTime                      (gint total_secs, gint *d, gint *h, gint *m, gint *s);
void                TimeToStr                       (gchar *time, gint day, gint hour, gint min, gint sec);

gchar                *CurrentGmt                     (void);
void                SetCurrentGmt                   (gchar *time_str);
void                SetCurrentUtc                   (gchar *time_str);
void                SetBasePet                      (gchar *time_str);
void                SecsToGmt                       (gint total_secs, gchar *time_str);

void                UtcToGmt                        (gchar *timePtr);
void                GmtToUtc                        (gchar *timePtr);

void                UtcToPet                        (gchar *timePtr);
void                PetToUtc                        (gchar *timePtr);

glong                StrToSecs                       (gint time_type, gchar *time_str);
void                SecsToStr                       (gint time_type, glong total_secs, gchar *time_str);

void                StripTimeBlanks                 (gchar *time_str);

gint                 ValidTime                       (gint time_type, gchar *time_str);
gint                 ValidCurrentUtc                 (void);
gint                 ValidBasePet                    (void);

gint                 OffsetToSecs                    (gchar *offset);
void                SecsToOffset                    (gint secs, gchar *offset);

void                ConvertToUTC                    (gchar *time_string);

gboolean            IsGMT                           (gchar *time_string);
gboolean            IsUTC                           (gchar *time_string);
gboolean            IsPET                           (gchar *time_string);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __TIMESTRINGS_H__ */
