/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __ANTMAN_H__
#define __ANTMAN_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "TimeStrings.h"
    #define PRIVATE
#else
    #include "TimeStrings.h"
#endif

/*
=================
Public Defintions
=================
*/

/**
 * Define the major.minor.patch version of iam
 */
//#define IAM_MAJOR_VERSION       7   /**< Major version number */
//#define IAM_MINOR_VERSION       0   /**< Minor version number */
//#define IAM_PATCH_VERSION       1   /**< patch version number */

/**
 * If 'No' or 'Yes' is defined, we want to define
 * our own values
 */
#if defined(No) || defined(Yes)
    #undef No
    #undef Yes
#endif

/**
 * Define the values for 'No' and 'Yes'
 */
enum NoYes
{
    No = 0,
    Yes
};

/**
 * If 'False' or 'True' is defined, we want to define
 * our own values
 */
#if defined(False) || defined(True)
    #undef False
    #undef True
#endif

/**
 * Define the values for 'False' and 'True'
 */
enum FalseTrue
{
    False = 0,
    True
};

#ifdef G_OS_WIN32
#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE (1)
#endif
#pragma warning(disable:4996)
#pragma warning(disable:4005)
#endif
/**
 * Defines the processing mode.
 * MANAGER is typically for CATO users only,
 * while PUBLIC is for all others. MANAGER provides
 * additional functionality.
 */
enum ProcesingMode
{
    MANAGER = 0,                /**< Mode used by CATO */
    PUBLIC                      /**< All other users */
};

/**
 * Defines which display is currently being viewed
 */
enum Display
{
    PREDICT = 0,                /**< Predict Display */
    REALTIME,                   /**< Realtime display */

    NUM_DISPLAYS
};
#define VALID_DISPLAY(d)        (d<NUM_DISPLAYS)

/**
 * Defines which coordinate type in the dialog is being viewed
 */
enum Coordinate
{
    UNKNOWN_COORD = -1,         /**< Coordinate is not defined */
    SBAND1 = 0,                 /**< SBand 1 coordinate type */
    SBAND2,                     /**< SBand 2 coordinate type */
    KUBAND,                     /**< KuBand coordinate type */
    KUBAND2,                    /**< KuBand 2 coordinate type */
    STATION,                    /**< Station coordinate type */

    NUM_COORDS,                 /**< the real number of coordinates supported */

    SBAND1_KUBAND = NUM_COORDS, /**< SBand 1 and KuBand coordinate type */
    SBAND2_KUBAND,              /**< SBand 2 and KuBand coordinate type */
    NUM_COORDS_PLUS_COMBOS      /**< The total number of values enumerated here */
};
#define VALID_COORD(c)          (c<NUM_COORDS)

/**
 * Defines the number of views supported
 */
enum View
{
    UNKNOWN_VIEW = -1,          /**< View is not defined */
    SBAND1_VIEW = 0,            /**< SBand 1 view */
    SBAND2_VIEW,                /**< SBand 2 view */
    KUBAND_VIEW,                /**< KuBand view */
    STATION_VIEW,               /**< Station view */
    S1KU_SBAND_VIEW,            /**< Multi-View SBand 1 view */
    S1KU_KUBAND_VIEW,           /**< Multi-View KuBand view */
    S2KU_SBAND_VIEW,            /**< Multi-View SBand 2 view */
    S2KU_KUBAND_VIEW,           /**< Multi-View KuBand view */

    NUM_VIEWS                   /**< total number of views supported */
};

/**
 * Define the list of drawing areas used
 */
enum DrawingArea
{
    UNKNOWN_DrawingArea = -1,       /**< drawing area not defined */

    SBand1DrawingArea = 0,          /**< SBand 1 drawing area */
    SBand2DrawingArea,              /**< SBand 2 drawing area */

    KuBandDrawingArea,              /**< KuBand main drawing area */
    KuBandLeftDrawingArea,          /**< KuBand left edge */
    KuBandBottomDrawingArea,        /**< KuBand bottom edge */

    StationDrawingArea,             /**< Station main drawing area */
    StationLeftDrawingArea,         /**< Station left edge */
    StationBottomDrawingArea,       /**< Station bottom edge */

    S1KU_SBandDrawingArea,          /**< SBand 1 drawing area for multi-view */
    S1KU_KuBandDrawingArea,         /**< KuBand main drawing area for multi-view */
    S1KU_KuBandLeftDrawingArea,     /**< KuBand left edge for multi-view */
    S1KU_KuBandBottomDrawingArea,   /**< KuBand bottom edge for multi-view */

    S2KU_SBandDrawingArea,          /**< SBand 2 drawing area for multi-view */
    S2KU_KuBandDrawingArea,         /**< KuBand main drawing area for multi-view */
    S2KU_KuBandLeftDrawingArea,     /**< KuBand left edge for multi-view */
    S2KU_KuBandBottomDrawingArea,   /**< KuBand bottom edge for mutli-view */

    MAX_DRAWING_AREAS               /**< total count of drawing areas */
};

/**
 * Defines which drawable to write to
 */
enum Drawable
{
    SRC_DRAWABLE = 0,           /**< pixmap that has it all */
    BG_DRAWABLE,                /**< background pixmap, has mask data */
    DST_DRAWABLE                /**< the main drawing area */
};

/**
 * defines the pixmap data used by the
 * various masks and structure drawing
 * routines.
 */
typedef struct PixmapData
{
    gboolean    redraw;         /**< indicates a redraw of pixmap is required */
    gint        width;          /**< the width of the pixmap */
    gint        height;         /**< the height of the pixmap */
    GdkPixmap   *pixmap;        /**< the pixmap */
} PixmapData;

/**< absolute minimum size of the window (%) of the original */
#define WINDOW_MIN_PERCENT  0.5

/**< screen size that width/height are expecting */
#define OPTIMUM_SCREEN_WIDTH    1280
#define OPTIMUM_SCREEN_HEIGHT   1024

/**< define minus and plus */
#define NEGATIVE_SIGN '-'
#define POSITIVE_SIGN '+'

/**< TIME_TYPE keyword, used to store time type data with widget */
#define TIME_TYPE       "time_type"

/**< title to use for the predict display */
#define PREDICT_TITLE   "Predict"

/**< title to use for the realtime display */
#define REALTIME_TITLE  "RealTime"

/**< generic title for sband type */
#define SBAND_TITLE     "SBand"

/**< define the various view types as strings */
/**< these are used to determine which view to start with */
#define SBAND1_VIEW_STR         "sband1"
#define SBAND2_VIEW_STR         "sband2"
#define STATION_VIEW_STR        "generic"
#define KUBAND_VIEW_STR         "kuband"
#define SBAND1_KUBAND_VIEW_STR  "sband1-kuband"
#define SBAND2_KUBAND_VIEW_STR  "sband2-kuband"

/**
 * Titles to use in the notebook tabs
 */
#define SBAND1_TITLE    "SBand 1"               /**< sband 1 title */
#define SBAND2_TITLE    "SBand 2"               /**< sband 2 title */
#define KUBAND_TITLE    "KuBand"                /**< kuband title */
#define KUBAND2_TITLE   "KuBand2"               /**< kuband 2 title */
#define STATION_TITLE   "Generic AzEl"          /**< station title */
#define SBAND1_KUBAND_TITLE "SBand 1/KuBand"    /**< sband 1 and kuband multi-view title */
#define SBAND2_KUBAND_TITLE "SBand 2/KuBand"    /**< sband 2 and kuband multi-view title */

/**
 * Predict Source definitions.
 * Also used by the gen_predict application.
 */
#define DECAT           "DECAT"
#define MAPS            "MAPS"
#define LOS             "LOS"


/**
 * Labels used for the event data when changing from sband to kuband
 */
#define XEL_LABEL       "XEL"
#define AZ_LABEL        "AZ"
#define EL_LABEL        "EL"

/**
 * MCC Only: Environment variables for the activity type
 */
#define MIS             "MIS"
#define SIM             "SIM"
#define ACTIVITY_TYPE   "ACTIVITY_TYPE"
enum ActivityEnum
{
    UNKNOWN_ACTIVITY_TYPE = -1,
    ACTIVITY_TYPE_MIS = 0,
    ACTIVITY_TYPE_SIM
};

/**
 * MCC Only: Environment variables for cert level type
 */
#define CERT_APPS       "cert_apps"
#define UNCERT_APPS     "uncert_apps"
#define SW_LVL          "sw_lvl"
enum CertLevelEnum
{
    UNKNOWN_SW_LVL = -1,
    SW_LVL_CERT_APPS = 0,
    SW_LVL_UNCERT_APPS
};

/**
 * Default coverage identifier name
 */
#define PTG_COVERAGE_STR "PTG"

/**
 * Defines commonly used table attachment options
 */
#define GAO_NONE        (GtkAttachOptions)(0)
#define GAO_FILL        (GtkAttachOptions)(GTK_FILL)
#define GAO_EXPAND      (GtkAttachOptions)(GTK_EXPAND)
#define GAO_EXPAND_FILL (GtkAttachOptions)(GTK_EXPAND | GTK_FILL)

/**
 * Defines the dimension of something WxH
 */
typedef struct Dimension
{
    gint        width;
    gint        height;
} Dimension;

/**
 * Point is a simple 2-dimensional integer data point.  It is used to represent
 * azimuth-elevation position in 1/10th degrees throughout the ACCT system.
 */
typedef struct Point
{
    gint        x;   /**< x and y-coordinates of point */
    gint        y;
} Point;


/**
 * Line is a pair of Point data structures, used to represent a line segment by
 * storing the end points of the line segment.
 */
typedef struct Line
{
    Point       p1; /**< endpoints of line segment */
    Point       p2;
} Line;


/**
 * PolyLine is a set of points that define the endpoints of a series of
 * contiguous line segments.  The data contained is for an antenna mask.
 * The coordinates are in the antenna map coordinate system (upper left 180,90
 * to lower right -180,-90) multiplied by 100 to keep all coordinates in
 * integer notation.  Thus the point [-450,85] represents the point [-45,8.5]
 * in antenna map space, or [225,81.5] in window coordinates.  Each PolyLine
 * must be either 1) a closed figure with no internal crossings,
 * i.e., each PolyLine encloses exactly one space, or 2) an open figure with
 * no crossings.  The former are sometimes called polygons.
 */
typedef struct PolyLine PolyLine;
struct PolyLine
{
    gint        closed;     /**< 0 if open polyline, 1 if closed polygon   */
    gint        fill;       /**< fill pattern (0 = unfilled, 1 = stippled) */
    gint        colorId;    /**< color ID for polygon/line                 */
                            /**<    matches one of colors in AntManGC      */
    gint        ptCount;    /**< count of point                            */
    Point       *pts;       /**< x- and y-coordinates (azimuth/elevation)  */
                            /**<    in 1/10 degrees                        */
    PolyLine    *next;      /**< pointer to next polyline                  */
};


/**
 * PredictPt represents a data point from the predicted position data provided
 * by FAO.  The data points for a single TDRS event are kept as a linked list
 * of PredictPt data structures.
 */
enum BlockageEnum
{
    PT_BlockageUnknown,     /**< Blockage undefined */
    PT_IsBlocked,           /**< Point is blocked */
    PT_IsNotBlocked         /**< Point is not blocked */
};


/**
 * Structure that holds the tdrs info
 */
typedef struct TdrsInfo
{
    gint  id;               /* TDRS numeric value: 1, 2, 3, 5, 6 */
    gchar wlon[8];          /* TDRS name: 041E, 046S, 275Z, 171W, 174W */
    gchar shoName[128];     /* TDRS SHO name: E-TDE, E-TDS, W-171, W-TDW, E-275 */
    gchar eventName[16];    /* TDRS event name: 1   041E */
} TdrsInfo;

/**
 * PredictSet is a set of positions that represent single TDRS event.
 * It consists of a pointer to a PredictPt that contains the actual point
 * information, as well as other needed information.
 */
#define DISPLAY_PD  1<<0    /* displayed in predict display */
#define DISPLAY_RD  1<<1    /* displayed in realtime display */
#define DISPLAY_ED  1<<2    /* display in select event dialog */
#define DISPLAY_PRT 1<<3    /* display data needed for print from select */

/**
 * Predict data points
 */
typedef struct PredictPt
{
    gchar       timeStr[TIME_STR_LEN];  /**< time string in UTC format */
    glong       timeSecs;               /**< time of point in total seconds */

    Point       rawPt;                  /**< original pt */

    Point       sbandPt;                /**< sband points */
    Point       kubandPt;               /**< kuband points */
    Point       kubandPt2;              /**< kuband 2 points */
    Point       genericPt;              /**< generic points */

    gint        interpolated;           /**< 1 iff point is interpolated */
    gint        shoPt;                  /**< 1 iff point is special SHO schedule point */
    gint        masks;                  /**< bit-masks identifying which masks the point */
                                        /**< lies within for the current coord system */
    gint        Smasks;                 /**< bit-masks identifying which masks the point */
                                        /**< lies within for the S-band coord sysetm. */
                                        /**< Used for writing blockage reports */
    gint        Kumasks;                /**< bit-masks identifying which masks the point */
                                        /**< lies within for the Ku-band coord system */
    gint        dynamicBlock;           /**< flag if point blocked by dynamic part */
    gint        SdynamicBlock;          /**< flag if S-band point blocked by dynamic part */
    gint        KudynamicBlock;         /**< flag if Ku-band point blocked by dynamic part */

    GList       *set;                   /**< pointer to PredictSet containing this point */
} PredictPt;


/**
 * Defines the column data for the list view in the event dialog.
 */
typedef struct EventList
{
    gchar       orbit    [16];          /**< orbit number */
    gchar       tdrs_id  [16];          /**< TDRS Id */
    gchar       aos      [TIME_STR_LEN];/**< Acquisition of signal */
    gchar       los      [TIME_STR_LEN];/**< Loss of signal */
    gchar       elapsed  [TIME_STR_LEN];/**< elapsed time */
} EventList;

/**
 * Predict set data
 */
typedef struct PredictSet
{
    gchar       eventName[10];          /**< name of TDRS for event */
    gchar       trackLabel[2];          /**< display string for TDRS predict track */
    gchar       aosStr[TIME_STR_LEN];   /**< time of first point */
    glong       aosSecs;                /**< time of first point in total secs */
    gchar       losStr[TIME_STR_LEN];   /**< time of last point */
    glong       losSecs;                /**< time of last point in total secs */
    gint        trackNumber;
    gchar       shoStart[TIME_STR_LEN]; /**< time of sho start */
    gchar       shoEnd[TIME_STR_LEN];   /**< time of sho end */
    gint        ShoChanged;             /**< A flag to indicate that the updated SHO */
                                        /**<  data is different from the current SHO */
                                        /**<  data. */
    gint        ShoEventNumber;         /**< Since multiple events can occur during */
                                        /**< one track, it is necessary to keep track */
                                        /**< of how many events are scheduled.  This */
                                        /**< is necessary because of the new */
                                        /**< Scheduled/ALL capability. */
    gchar       shoStatus[16];          /**< sho label (NORMAL or EARLY) */
    gint        orbit;                  /**< orbit of track */
    gint        displayed;              /**< mask: 1 if displayed in predict dialog; */
                                        /**<       2 if selected in realtime dialog; */
                                        /**<       4 if displayed in event dialog; */
                                        /**<       8 if printed from event dialog; */
    gboolean    locksel;                /**< flag indicating event is currently locked */
                                        /**< and has the current point ready to be used */
    gboolean    listed;                 /**< Flags  to indicate whether this event is */
                                        /**< listed in the Scheduled/ALL Event window.*/

    GList       *ptList;                /**< has the list of points */

    GList       *interpolatedPt;        /**< current interpolated pt for event, if any */
    GList       *equivalentPt;          /**< point at same time as currently selected */
                                        /**< point on another track, if any */

    EventList   text;                   /**< has list data for event dialog */
    glong       filepos;                /**< file position where event starts in file */
} PredictSet;

/**
 * Defines the various information used for drawing the pixmaps.
 */
typedef struct WindowData
{
    GtkWidget   *drawing_area;          /**< dst: drawing area widget, where to put pixmap */
    GtkWidget   *handle_box;            /**< handle box for toolbar */
    GtkWidget   *handle_box2;           /**< handle box for toolbar */
    GtkWidget   *toolbar;               /**< toolbar for this view */
    GtkWidget   *toolbar2;              /**< second toolbar for this view  - only used for ku-band */
    GList       *tool_items;            /**< toolbar instances and handler ids */
    GList       *tool_items2;           /**< second toolbar instances and handler ids  - only for ku-band */
    GdkPixmap   *src_pixmap;            /**< src: main pixmap image, has it all */
    GdkPixmap   *bg_pixmap;             /**< bg: has all mask data, no tracks */

    PixmapData  *bgpixmap_data;         /**< background data */
    PixmapData  *dynamic_data;          /**< where the dynamic structure is drawn */
    PixmapData  *mask_data;             /**< where the static masks are drawn */
    PixmapData  *obs_data;              /**< where the obscuration masks are drawn */
    PixmapData  *track_data;            /**< where the tdrs tracks are drawn */

    gint         coordinate_type;        /**< what type of coordinate type this is */
    gint         which_drawable;         /**< flag indicating which src pixmap to use */

    gboolean    is_resized;             /**< drawable has been resized */

    gboolean    redraw_pixmap;          /**< change occurred, redraw */
    gint         x;                      /**< drawing location for pixmap */
    gint         y;                      /**< drawing location for pixmap */
    gint         width;                  /**< drawing width for pixmap */
    gint         height;                 /**< drawing height for pixmap */
    gint         fontSize;               /**< current font size to use */
} WindowData;

/*
==============
Public Methods
==============
*/
void                AM_Exit                         (gint);
gint                AM_GetAppMode                   (void);
void                AM_SetAppMode                   (gint app_mode);

gboolean            AM_GetAutoUpdateMode            (void);
void                AM_SetAutoUpdateMode            (gchar *msg);
void                AM_ClearAutoUpdateMode          (void);

gchar               *AM_GetViewName                 (gint coord);
gint                AM_GetSwLvl                     (void);
gint                AM_GetActivityType              (void);

void                AM_SetupSignals                 (void);

gint                AM_SetTimeType                  (gint time_type);
gint                AM_GetTimeType                  (void);
gint                AM_UpdateTimeType               (void);

GdkDrawable         *AM_Drawable                    (WindowData *windata, gint which_src_drawable);
gboolean            AM_AllocatePixmapData           (WindowData *windata, PixmapData *pixmap_data, gboolean force);
void                AM_CopyPixmap                   (WindowData *windata, GdkDrawable *drawable);
void                AM_ClearDrawable                (GdkDrawable *drawable, GdkGC *gc, gint x, gint y, gint width, gint height);

void                AM_SetActiveDisplay             (gint display, gboolean state);
void                AM_ClearActiveDisplay           (void);
GtkWidget           *AM_GetActiveDisplay            (void);
gint                AM_GetActiveDisplayID           (void);

Dimension           *AM_GetWindowSize               (GtkWidget *widget, gint window_width, gint window_height);

gchar               *AM_GetRCFile                   (gchar *rcfile);
void                AM_AddRCFile                    (gchar *rcfile);
void                AM_AddResource                  (gchar *rcfile, gchar *rcstyle);

gchar               *AM_GetVersion                  (void);

gint                AM_CoordViewToCoordType         (gint coord_view);

gchar               *AM_GetTitle                    (gchar *which_dialog, gint coord);
void                AM_SetStructureIdentifier       (gchar *structure_identifier);
void                AM_SetCoverageIdentifier        (gchar *coverage_identifier);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static Dimension    *getScreenDimension             (GtkWidget *widget);
static void         shutdownHandler                 (gint sig);

#endif /* PRIVATE */
#endif /* __ANTMAN_H__ */
