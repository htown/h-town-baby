/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __JOINTANGLEDIALOG_H__
#define __JOINTANGLEDIALOG_H__

#ifdef PRIVATE
#undef PRIVATE
#include "DynamicDraw.h"
#define PRIVATE
#else
#include "DynamicDraw.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                JAD_Create                      (GtkWindow *parent);
void                JAD_Open                        (void);
void                JAD_Update                      (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/**
 * Enumerations used for displaying joint angle data in the dialog.
 */
enum JointAngleEnum
{
    COLUMN_JOINT_ANGLE_LABEL,   /**< Joint angle column name */
    COLUMN_JOINT_ANGLE_VALUE,   /**< Joint angle column value */
    NUM_JOINT_ANGLE_COLUMNS     /**< Total joint angle columns */
};

/*
===============
Private Methods
===============
*/
static void         response_cb                     (GtkWidget *widget, gint response_id, void * data);
static void         load_angle_data                 (JointAngle *ja);
static void         update_angle_data               (JointAngle *ja);

#endif /* PRIVATE */
#endif /* __JOINTANGLEDIALOG_H__ */
