/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PETDIALOG_H__
#define __PETDIALOG_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                PET_Create                      (GtkWindow *parent);
void                PET_Open                        (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         response_cb                     (GtkWidget *widget, gint response_id, void * data);
static gboolean     key_press_cb                    (GtkWidget *widget, GdkEventKey *event, void * data);

#endif /* PRIVATE */
#endif /* __PETDIALOG_H__ */
