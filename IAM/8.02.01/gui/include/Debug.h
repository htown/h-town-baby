/* @(#)Debug.h	13.1 7/1/97 09:03:40 */
/*======================= MSC RSL Standard Abstract ==========================
*
* Synopsis:                    Debug macros
*
* Name:                        Debug.h
* Version:                     N/A
* Type:                        include file
* Date Complete:               05/27/97
*
* Object Name:                 N/A
* Object Code:                 N/A
* Object Number:               N/A
*
* Language:                    C
* Computer:                    Dec/Alpha
* OS:                          OSF/1
* OS Version:                  N/A
* Compiler:                    C
*
* Owner:                       NASA/JSC
* System:                      PS
* Subsystem:                   EIS
* Application:                 EIS - General
* Confidence Level:            SIOT
* Delivery/Release:            Pioneer
*
* Author:                      Richard Scott
* Author's Location:           LMSIS/Houston
* Secondary Contact:           Bruce Lewis
*
* Description::           This file contains debug macros for use in
*                         EIS software. When compiled with the -DDEBUG_MACROS
*                         option, EIS software can use these macros to print
*                         debug information to a file.
*
*                         To use, do the following things:
*
*                         1) Add these lines to the top of your main() file
*                              #define MAIN
*                              #include Debug.h
*                         2) Add this line as the first executable statement
*                            in your main()
*                              PENTRY(MyProgram);
*                         3) Then add macro lines to your code. For tracing,
*                              use macros in your subprograms as follows:
*                              
*                              int my_function(int x)
*                              {
*                                 int rc;
*                                 ENTRY(my_function);
*                                 ...code...
*                                 RTND(rc);
*                              }
*
* Embedded Routines:           N/A
* External Dependencies:       N/A
*     Include files: stdio.h
*                    sys/types.h
*                    string.h
*                    stdlib.h
*                    unistd.h
*
* Constraints:                 N/A
* Resource Utilization:        N/A
*
*=============================================================================
*
* Revision History:
*
*     Revision Date: 05-27-97
*        Revised By: Richard Scott
*   Revision Number: IDR-000000
*          Comments: Initial Pioneer build
* 
*==================== End of MSC RSL Standard Abstract ======================*/


#ifndef DEBUG_H
#define DEBUG_H

#if defined (__cplusplus) || defined (c_plusplus)
   extern "C" {
#endif

/*=========================================================================*/
/*                             INCLUDES                                    */
/*=========================================================================*/
#include <stdio.h>             /* file i/o */
#include <sys/types.h>         /* type definitions */
#include <string.h>            /* needed for strlen() and others */
#include <stdlib.h>            /* needed for exit() and others */
//#include <unistd.h>            /* needed for getpid() and others */

/*=========================================================================*/
/*                          DEFINES AND CONSTANTS                          */
/*=========================================================================*/
#ifdef __STDC__
#define Const    const
#else
#define Const
#endif

#ifndef TRUE
#define TRUE        1                 /* Boolean TRUE  */
#define FALSE       0                 /* Boolean FALSE */
#endif

#ifndef True
#define True        1                 /* Boolean True  */
#define False       0                 /* Boolean False */
#endif

#ifndef YES
#define YES         1                 /* Boolean YES */
#define NO          0                 /* Boolean NO  */
#endif

#ifndef Yes
#define Yes         1                 /* Boolean Yes */
#define No          0                 /* Boolean No  */
#endif

#ifndef ON
#define ON          1                 /* Boolean ON  */
#define OFF         0                 /* Boolean OFF */
#endif


#ifndef OK
#define OK          (0)               /* For returning 0 as "TRUE"         */
#define FAIL        (1)               /* For returning 1 as "FALSE"        */
#endif

#define EOL        '\n'               /* ASCII end-of-line character       */
#define NUL        '\0'               /* ASCII Nul character               */

#ifndef NULL
#define NULL        0                 /* NULL ptr - TYPECAST when using!!! */
#endif
#define NULP (void *)NULL             /* null pointer in function call */

/*-----------------------
 * Message types
 *----------------------*/
#define   MSUCCESS          0
#define   MINFORMATION     -1
#define   MINFO            -1
#define   MWARNING         -2
#define   MERROR           -3
#define   MFATAL           -4
#define   MDEFAULT          1

typedef gboolean    Boolean;     /* Boolean :-)                       */
typedef int              Addr;        /* machine type for ptr math */
typedef unsigned int     Bit;         /* machine type for bit fields */
typedef gboolean    Byte;        /* unsigned 8 bits */
typedef char             Char;        /* unsigned 7 bits (8th undetermined) */
typedef short            Short;       /* signed 16 bit integer */
typedef unsigned short   UShort;      /* unsigned 16 bits */
typedef int              Long;        /* signed 32 bit integer */
typedef unsigned int     ULong;       /* unsigned 32 bit integer */
typedef int              Int;         /* signed machine type */
typedef unsigned int     UInt;        /* unsigned machine type */
typedef float            Float;       /* 32 bit floating point */
typedef double           Double;      /* 64 bit floating point */

#define Void            void          /* no return value function type */
#define Extern          extern        /* imported global reference */
#define Private         static        /* locally defined local reference */
#define Public                        /* locally defined global reference */
#define Static          static        /* static storage local variable */

/*------------------------------------------------------------------------
 * Define our own Global "keyword". This allows us to define global
 * variables in an include file such that they are "externs" if included
 * in any file but the main().
 *-----------------------------------------------------------------------*/
#ifdef MAIN
#define Global                        /* global definition (usually in .h) */
#else
#define Global Extern                 /* declaration (func.c includes .h) */
#endif

#define BREAK(label)     goto label   /* Semi structured use of goto to    */
                                      /* escape from within nested for or  */
                                      /* switch statements.                */

#ifndef PRINT
Extern char *gfmt(char *buf, ...);

#define PRINT (Void)fprintf           /* optimized fprintf */
#endif

/*****************************************************************************
 * C debugging aid -- these macro's allow the tracing of a function. They
 *                    can be added to a program such that they are only 
 *                    expanded during testing.
 *
 *    The macro's are enabled by defining the symbol DEBUG_MACROS.
 *    Each routine should contain an ENTRY and a RTN or EXIT macro.
 *
 *        PENTRY(pn)               pn - program name
 *        PSENTRY(pn)              pn - program name (char *)
 *        ENTRY(fn)                fn - function name
 *        EXIT(code)               Exit from a program with exit code
 *        RTN()                    Return from a Void function
 *        RTNA(type, val)          Return addr val from a function of type
 *        RTNx(val)                Return val from a function of type x
 *        ALLOC(ptr, type, size)   Allocate memory
 *        CALLOC(ptr, type, size)  Allocate clear memory
 *        RALLOC(ptr, type, size)  Reallocate memory
 *        FREE(ptr)                Free memory
 *        DBG(code fragment)       Include code fragment (during debug)
 *        ADDR(adr)                Address
 *        CHR(chr)                 Character
 *        DEC(val)                 Decimal (used for booleans too)
 *        DMP(adr,len)             Write from adr for len
 *        FLT(val)                 Floating Point
 *        HEX(val)                 Hex
 *        HEXD(buf,numbytes)       Formatted HEX/ASCII dump of buf for numbytes
 *        MSG(msg)                 Message
 *        MSGA(msg, adr)           Message w/address
 *        MSGD(msg, val)           Message w/decimal value
 *        MSGF(msg, val)           Message w/floating point value
 *        MSGS(msg, str)           Message w/string
 *        MSGH(msg, val)           Message w/hexadecimal value
 *        OCT(val)                 Octal
 *        STR(str)                 String
 *        STRN(str, len)           String max Len
 *        UDEC(val)                Unsigned
 *
 *    Note: F is always printed as C type double
 *          D, O, U, and X are always printed as ld, o, lu, and x
 *
 ***************************************************************************/
#ifdef DEBUG_MACROS

#ifdef MAIN
Public FILE *__dbg;
Public Char __dbb[] =
"                                                                          ";
Public Int __dbt = 74;                   /* length of debug blank string */
#else
Extern FILE *__dbg;
Extern char __dbb[];
Extern Int __dbt;
#endif

#ifndef DBGINC
#define DBGINC    1                       /* debug indentation increment */
#endif
#define DBGI (__dbb+__dbt)                /* debug indentation */
#define FDBG (Void) fflush(__dbg)

#define EXIT(code) {\
    Int __tmp=(code);\
    PRINT(__dbg,"%s< [%s] exiting = %d\n",DBGI,__name,__tmp);FDBG;\
    exit(__tmp);}

#define RTN() {\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s]\n",DBGI,__name);FDBG;\
    return;}

#define RTNP(typ, adr) {\
    typ __tmp=(typ)(adr);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = @%#x\n", DBGI,__name,(ULong)__tmp);FDBG;\
    return(__tmp);}

#define RTNA(typ, adr) {\
    typ __tmp=(typ)(adr);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = @%#x\n", DBGI,__name,(ULong)__tmp);FDBG;\
    return(__tmp);}

#define RTNC(chr) {\
    Char __tmp=(chr);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %c (%#2x)\n",DBGI,__name, __tmp, __tmp);FDBG;\
    return(__tmp);}

#define RTND(val) {\
    Long __tmp=(val);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %d\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define RTNF(val) {\
    Double __tmp=(val);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %g\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define RTNO(val) {\
    ULong __tmp=(val);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %#o\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define RTNS(str) {\
    Char *__tmp=(str);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s]: = ~%s~\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define RTNU(val) {\
    ULong __tmp=(val);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %lu\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define RTNX(val) {\
    ULong __tmp=(val);\
    __dbt += DBGINC;\
    PRINT(__dbg,"%s< [%s] = %#x\n",DBGI,__name,__tmp);FDBG;\
    return(__tmp);}

#define DBG(do)        {do}


/***************************************************************************
 * Some macros are defined a little differently if the __STDC__ compiler
 * macro is defined. On the Alphas(R), using the -std1 flag with cc or c89
 * causes ANSI standard checking and defines __STDC__.
 ***************************************************************************/
#ifndef __STDC__

#define PENTRY(fn) \
    Static Char __name[]={"fn"};\
    __dbg = fopen("/usr/tmp/fn.dbg", "w");\
    PRINT(__dbg,"> [%s]\n",__name);FDBG;

#define PSENTRY(fn) \
    Static Char *__name;\
    Static Char *__fname;\
    __name = (char *)malloc(strlen(fn)+6);\
    sprintf(__name,"%s_%d", fn, getpid());\
    __fname = (char *)malloc(strlen(fn)+10);\
    sprintf(__fname,"%s.dbg",__name);\
    __dbg = fopen(/usr/tmp/__fname, "w");\
    PRINT(__dbg,"> [%s]\n",__name);FDBG;

#define ENTRY(fn) \
    Static Char __name[]={"fn"};\
    PRINT(__dbg,"%s> [%s]\n",DBGI,__name);FDBG;\
    __dbt -= DBGINC

#define ALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: allocating memory for %s\n",DBGI,__name,"ptr");FDBG;\
    ptr = (type) malloc((UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define CALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: allocating clear memory for %s\n",\
        DBGI,__name,"ptr");FDBG;\
    ptr = (type) calloc(1,(UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define RALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: reallocating memory for %s\n",\
        DBGI,__name, "ptr");FDBG;\
    ptr = (type) realloc(ptr, (UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define FREE(ptr) {\
    PRINT(__dbg, "%s..%s: %s freeing at @%#x\n", DBGI,__name,"ptr",ptr);FDBG;\
    free(ptr);\
}

#define ADDR(adr) \
    PRINT(__dbg,"%s..%s: %s = %#x\n",DBGI,__name,"adr",(ULong)(adr));FDBG
#define CHR(chr) \
    PRINT(__dbg,"%s..%s: %s = %c (%#2x)\n",\
            DBGI,__name,"chr",(Char)(chr),(Char)(chr));FDBG
#define DEC(val) \
    PRINT(__dbg,"%s..%s: %s = %d\n",DBGI,__name,"val",(Long)(val));FDBG
#define DMP(ptr, len) {\
    if (!(ptr) || !(len)) {\
        PRINT(__dbg,"%s..%s: %s = ~(NULL)~\n",DBGI,__name,"ptr");FDBG;\
    } else {\
        PRINT(__dbg,"%s..%s: %s = ~>",DBGI,__name,"ptr");FDBG;\
        (Void) fwrite((ptr), (len), 1, __dbg);\
        PRINT(__dbg,"<~\n");FDBG;\
    }\
}
#define DBL(val) PRINT(__dbg,"%s..%s: %s = %lf\n",DBGI,__name,"val",\
    (double)(val));FDBG
#define FLT(val) PRINT(__dbg,"%s..%s: %s = %f\n",DBGI,__name,"val",\
    (float)(val));FDBG
#define HEX(val) \
    PRINT(__dbg,"%s..%s: %s = %#x\n",DBGI,__name,"val",(val));FDBG

/*==============================================================================
 * Formatted HEX dump of an area (buf) for length (numbytes) (HEX and ASCII)
 *----------------------------------------------------------------------------*/
#define HEXD(buf, numbytes)\
{\
   int i, j, k, _nbytes;\
   _nbytes = numbytes;\
   PRINT(__dbg, "%s..%s: %s for %d bytes - addr %x\n",\
         DBGI, __name, "buf", _nbytes, buf);\
   k = _nbytes % 16;\
   _nbytes -= k;\
   /* dump full lines of 16 bytes */\
   for (j = 0; j < _nbytes; j += 16)\
   {\
       PRINT(__dbg, "%s..%s:    %5.5X:",DBGI, __name, j);\
       for (i = j; i < j + 16; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "%2.2X", buf[i]&0xFF);\
       }\
       PRINT(__dbg, " [");\
       for (i = j; i < j + 16; i++)\
       {\
           PRINT(__dbg, "%c",\
                ((buf[i] > 0x1F) && (buf[i] < 0x7F)) ? buf[i] : '.');\
       }\
       PRINT(__dbg, "]\n");\
   }\
   /* dump partial lines */\
   if (k > 0)\
   {\
       PRINT(__dbg, "%s..%s:    %5.5X:", DBGI, __name, j);\
       for (i = j; i < j + k; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "%2.2X", buf[i]&0xFF);\
       }\
       for (i = k; i < 16; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "  ");\
       }\
       PRINT(__dbg, " [");\
       for (i = j; i < j + k; i++)\
       {\
           PRINT(__dbg, "%c",\
                ((buf[i] > 0x1F) && (buf[i] < 0x7F)) ? buf[i] : '.');\
       }\
       PRINT(__dbg, "]\n");\
   }\
   FDBG;\
}

#define MSG(msg) PRINT(__dbg,"%s..%s: %s\n",DBGI,__name,"msg");FDBG
#define MSGA(msg,adr) \
    PRINT(__dbg,"%s..%s: %s, @%#x\n",DBGI,__name,"msg",(ULong)(adr));FDBG
#define MSGD(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %d\n",DBGI,__name,"msg",(val));FDBG
#define MSGF(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %g\n",DBGI,__name,"msg",(val));FDBG
#define MSGS(msg,str) \
    PRINT(__dbg,"%s..%s: %s, ~%s~\n",DBGI,__name,"msg",(str));FDBG
#define MSGH(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %#x\n",DBGI,__name,"msg",(ULong)(val));FDBG
#define OCT(val) \
    PRINT(__dbg,"%s..%s: %s = %#o\n",DBGI,__name,"val",(ULong)(val));FDBG
#ifdef NULLDUMPS
#define STR(str) \
    PRINT(__dbg,"%s..%s: %s = ~%s~\n",DBGI,__name,"str",\
    (str)?(str):"~NULL~");FDBG
#else
#define STR(str) \
    PRINT(__dbg,"%s..%s: %s = ~%s~\n",DBGI,__name,"str",(str));FDBG
#endif
#define STRN(str, len) {\
   Char __tbuf[(len)+1];\
   (Void) strncpy(__tbuf, (str), (len));\
    __tbuf[len] = NUL;\
   PRINT(__dbg,"%s..%s: %s = ~%s~\n",DBGI,__name,"str",__tbuf);FDBG;\
}
#define UDEC(val) \
    PRINT(__dbg,"%s..%s: %s = %lu\n",DBGI,__name,"val",(ULong)(val));FDBG


#else /* __STDC__ */

#define PENTRY(fn) \
    Static Char __name[]={#fn};\
    __dbg = fopen("/usr/tmp/"#fn".dbg", "w");\
    PRINT(__dbg,"> [%s]\n",__name);FDBG;

#define PSENTRY(fn) \
    Static Char *__name;\
    Static Char *__fname;\
    __name = (char *)malloc(strlen(fn)+6);\
    sprintf(__name,"%s_%d", fn, getpid());\
    __fname = (char *)malloc(strlen(fn)+10);\
    sprintf(__fname,"%s.dbg",__name);\
    __dbg = fopen(/usr/tmp/__fname, "w");\
    PRINT(__dbg,"> [%s]\n",__name);FDBG;

#define ENTRY(fn) \
    Static Char __name[]={#fn};\
    PRINT(__dbg,"%s> [%s]\n",DBGI,__name);FDBG;\
    __dbt -= DBGINC

#define ALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: allocating memory for %s\n",DBGI,__name,#ptr);FDBG;\
    ptr = (type) malloc((UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define CALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: allocating clear memory for %s\n",\
        DBGI,__name,#ptr);FDBG;\
    ptr = (type) calloc(1,(UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define RALLOC(ptr, type, size) {\
    PRINT(__dbg, "%s..%s: reallocating memory for %s\n",\
        DBGI,__name, #ptr);FDBG;\
    ptr = (type) realloc(ptr, (UInt)((size)));\
    PRINT(__dbg, "%s..%s: allocated at @%#x\n", DBGI,__name, ptr);FDBG;\
}

#define FREE(ptr) {\
    PRINT(__dbg, "%s..%s: %s freeing at @%#x\n", DBGI,__name,#ptr,ptr);FDBG;\
    free(ptr);\
}

#define ADDR(adr) \
    PRINT(__dbg,"%s..%s: %s = %#x\n",DBGI,__name,#adr,(ULong)(adr));FDBG
#define CHR(chr) \
    PRINT(__dbg,"%s..%s: %s = %c (%#2x)\n",\
            DBGI,__name,#chr,(Char)(chr),(Char)(chr));FDBG
#define DEC(val) \
    PRINT(__dbg,"%s..%s: %s = %d\n",DBGI,__name,#val,(Long)(val));FDBG
#define DMP(ptr, len) {\
    if (!(ptr) || !(len)) {\
        PRINT(__dbg,"%s..%s: %s = ~(NULL)~\n",DBGI,__name,#ptr);FDBG;\
    } else {\
        PRINT(__dbg,"%s..%s: %s = ~>",DBGI,__name,#ptr);FDBG;\
        (Void) fwrite((ptr), (len), 1, __dbg);\
        PRINT(__dbg,"<~\n");FDBG;\
    }\
}
#define DBL(val) PRINT(__dbg,"%s..%s: %s = %lf\n",DBGI,__name,#val,(val));FDBG
#define FLT(val) PRINT(__dbg,"%s..%s: %s = %f\n",DBGI,__name,#val,(val));FDBG
#define HEX(val) PRINT(__dbg,"%s..%s: %s = %#x\n",DBGI,__name,#val,(val));FDBG

/*==============================================================================
 * Formatted HEX dump of an area (buf) for length (numbytes) (HEX and ASCII)
 *----------------------------------------------------------------------------*/
#define HEXD(buf, numbytes)\
{\
   int i, j, k, _nbytes;\
   _nbytes = numbytes;\
   PRINT(__dbg, "%s..%s: %s for %d bytes - addr %x\n",\
         DBGI, __name, #buf, _nbytes, buf);\
   k = _nbytes % 16;\
   _nbytes -= k;\
   /* dump full lines of 16 bytes */\
   for (j = 0; j < _nbytes; j += 16)\
   {\
       PRINT(__dbg, "%s..%s:    %5.5X:",DBGI, __name, j);\
       for (i = j; i < j + 16; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "%2.2X", buf[i]&0xFF);\
       }\
       PRINT(__dbg, " [");\
       for (i = j; i < j + 16; i++)\
       {\
           PRINT(__dbg, "%c",\
                ((buf[i] > 0x1F) && (buf[i] < 0x7F)) ? buf[i] : '.');\
       }\
       PRINT(__dbg, "]\n");\
   }\
   /* dump partial lines */\
   if (k > 0)\
   {\
       PRINT(__dbg, "%s..%s:    %5.5X:", DBGI, __name, j);\
       for (i = j; i < j + k; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "%2.2X", buf[i]&0xFF);\
       }\
       for (i = k; i < 16; i++)\
       {\
           if ((i%2) == 0) PRINT(__dbg, " ");\
           PRINT(__dbg, "  ");\
       }\
       PRINT(__dbg, " [");\
       for (i = j; i < j + k; i++)\
       {\
           PRINT(__dbg, "%c",\
                ((buf[i] > 0x1F) && (buf[i] < 0x7F)) ? buf[i] : '.');\
       }\
       PRINT(__dbg, "]\n");\
   }\
   FDBG;\
}

#define MSG(msg) PRINT(__dbg,"%s..%s: %s\n",DBGI,__name,#msg);FDBG
#define MSGA(msg,adr) \
    PRINT(__dbg,"%s..%s: %s, @%#x\n",DBGI,__name,#msg,(adr));FDBG
#define MSGD(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %d\n",DBGI,__name,#msg,(val));FDBG
#define MSGF(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %g\n",DBGI,__name,#msg,(val));FDBG
#define MSGS(msg,str) \
    PRINT(__dbg,"%s..%s: %s, ~%s~\n",DBGI,__name,#msg,(str));FDBG
#define MSGH(msg,val) \
    PRINT(__dbg,"%s..%s: %s, %#x\n",DBGI,__name,#msg,(ULong)(val));FDBG
#define OCT(val) \
    PRINT(__dbg,"%s..%s: %s = %#o\n",DBGI,__name,#val,(ULong)(val));FDBG
#define STR(str) \
    PRINT(__dbg,"%s..%s: %s = ~%s~\n",DBGI,__name,#str,(str));FDBG
#define STRN(str, len) {\
   Char __tbuf[(len)+1];\
   (Void) strncpy(__tbuf, (str), (len));\
    __tbuf[len] = NUL;\
   PRINT(__dbg,"%s..%s: %s = ~%s~\n",DBGI,__name,#str,__tbuf);FDBG;\
}
#define UDEC(val) \
    PRINT(__dbg,"%s..%s: %s = %lu\n",DBGI,__name,#val,(ULong)(val));FDBG


#endif /* ifndef __STDC__ else */


#else /* ! DEBUG_MACROS */

/**********************************************************************
 ** These are the macros used when NOT compiling with debug turned on.
 ** Most of them are null macros. The exceptions are the return, exit,
 ** malloc, and free macros.  Those call the appropriate system call
 ** without printing any debug information.
 **********************************************************************/
#define PENTRY(fn)
#define PSENTRY(fn)
#define ENTRY(fn)
#define EXIT(code)                 exit(code)
#define RTN()                      return
#define RTNP(typ, adr)             return((typ)(adr))
#define RTNA(typ, adr)             return((typ)(adr))
#define RTNC(chr)                  return(chr)
#define RTND(val)                  return(val)
#define RTNF(val)                  return(val)
#define RTNO(val)                  return(val)
#define RTNS(str)                  return(str)
#define RTNU(val)                  return(val)
#define RTNX(val)                  return(val)
#define ALLOC(ptr, type, size)     (ptr = (type)malloc((UInt)((size))))
#define CALLOC(ptr, type, size)    (ptr = (type)calloc(1, (UInt)((size))))
#define RALLOC(ptr, type, size)    (ptr = (type)realloc((ptr), (UInt)((size))))
#define FREE(ptr)                  free(ptr)
#define DBG(do)
#define ADDR(adr)
#define CHR(chr)
#define DEC(val)
#define DMP(ptr,len)
#define DBL(val)
#define FLT(val)
#define HEX(val)
#define HEXD(buf, numbytes)
#define MSG(msg)
#define MSGA(msg,adr)
#define MSGD(msg,val)
#define MSGF(msg,val)
#define MSGS(msg,str)
#define MSGH(msg,val)
#define OCT(val)
#define STR(str)
#define STRN(str, len)
#define UDEC(val)

#endif /* ifdef DEBUG_MACROS else */

#if defined (__cplusplus) || defined (c_plusplus)
   }    /* end brace for extern "C" */
#endif

#endif
