/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __ISPSYMBOLS_H__
#define __ISPSYMBOLS_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "MaskFile.h"
    #include "TimeStrings.h"
    #define PRIVATE
#else
    #include "MaskFile.h"
    #include "TimeStrings.h"
#endif

#ifdef G_OS_WIN32
/* so the windows it.lib will link */
extern "C"
{
    #include "status.h"
}
#else
    #include "status.h"
#endif

/*
=================
Public Defintions
=================
*/
#define INITIAL_VALUE     -999.999
#define ISP_MESSAGE_SIZE    1024

#define RESERVED_SYMBOL     "RESERVED"


/**
 * List of Symbol IDs for all ISP symbols known to ISS AntMan
 * These IDs are used to access the Symbol Table
 * This list MUST be in sync with iam.symbols file found in
 * the config directory.
 */
enum SYMBOL_IDS
{
    IS_AntmanAuto,

    IS_Gmt,
    IS_GmtYear,

    IS_SunCoordsX,
    IS_SunCoordsY,
    IS_SunCoordsZ,

    IS_TdreSatId,
    IS_TdreCoordsX,
    IS_TdreCoordsY,
    IS_TdreCoordsZ,

    IS_TdrwSatId,
    IS_TdrwCoordsX,
    IS_TdrwCoordsY,
    IS_TdrwCoordsZ,

    IS_TdrsSelectFlag,
    IS_RiseSetQuality,
    IS_RiseSetTarget1,
    IS_RiseSetTarget2,
    IS_RiseSetTarget3,
    IS_RiseSetTarget4,
    IS_RiseSetFlag1,
    IS_RiseSetFlag2,
    IS_RiseSetFlag3,
    IS_RiseSetFlag4,

    /**
     * these symbols are used to calculate the
     * TRRJ port and starboard joint angles
     */
    IS_P_TRRJ_DSM,
    IS_P_TRRJ_Gamma,
    IS_P_TRRJ_Readout,
    IS_S_TRRJ_DSM,
    IS_S_TRRJ_Gamma,
    IS_S_TRRJ_Readout,

    /* must be in sequence */
    IS_ANGLE_FIRST_SYMBOL,
    IS_2BJointAngle = IS_ANGLE_FIRST_SYMBOL,
    IS_4BJointAngle,
    IS_2AJointAngle,
    IS_4AJointAngle,
    IS_PAJointAngle,
    IS_SAJointAngle,
    IS_1AJointAngle,
    IS_3AJointAngle,
    IS_1BJointAngle,
    IS_3BJointAngle,
    IS_PTCSJointAngle,
    IS_STCSJointAngle,
    IS_ANGLE_LAST_SYMBOL,
    IS_ANGLE_MAX_SYMBOLS = (IS_ANGLE_LAST_SYMBOL - IS_ANGLE_FIRST_SYMBOL),

    /* must be in sequence */
    IS_PATHGO_ATM_FIRST_SYMBOL = IS_ANGLE_LAST_SYMBOL,
    IS_PathGO = IS_PATHGO_ATM_FIRST_SYMBOL,
    IS_SG1,
    IS_SG2,
    IS_PATHGO_ATM_LAST_SYMBOL,
    IS_PATHGO_ATM_MAX_SYMBOLS = (IS_PATHGO_ATM_LAST_SYMBOL - IS_PATHGO_ATM_FIRST_SYMBOL),

    /* must be in sequence */
    IS_KUBAND_FIRST_SYMBOL = IS_PATHGO_ATM_LAST_SYMBOL,
    IS_KuPwrAmpAct = IS_KUBAND_FIRST_SYMBOL,
    IS_KuPwrAmpMisCmpr,
    IS_KuPwrAmpPend,
    IS_KuPLCAct,
    IS_KuPLCMisCmpr,
    IS_KuPLCPend,
    IS_KuPtgAct,
    IS_KuPtgMisCmpr,
    IS_KuPtgPend,
    IS_KuTDRSSelect,
    IS_KuAutotrack,
    IS_KuHandover,
    IS_KuRetry,
    IS_KuGAO,
    IS_KuActualXEL,
    IS_KuActualEL,
    IS_KuPWRL,
    IS_KuFwdPwrVolts,
    IS_KuReflPwrVolts,
    IS_KuIFOutVolts,
    IS_KuIFInVolts,
    IS_KuSG1Temp,
    IS_KuSG2Temp,
    IS_KuSG3Temp,
    IS_KuSG4Temp,
    IS_KuSG5Temp,
    IS_KuSG6Temp,
    IS_KuSG7Temp,
    IS_KuSG8Temp,
    IS_KuTRCS1Temp,
    IS_KuTRCS2Temp,
    IS_KuTRCS3Temp,
    IS_KuTRCPwrAmpTemp,
    IS_KuTRCPwrSupTemp,
    IS_KuHRMTemp,
    IS_KuHRFMTemp,
    IS_KuVBSPTemp,
    IS_KuTRC1,
    IS_KuTRC2,
    IS_KUBAND_LAST_SYMBOL,
    IS_KUBAND_MAX_SYMBOLS = (IS_KUBAND_LAST_SYMBOL - IS_KUBAND_FIRST_SYMBOL),

    /* must be in sequence */
    IS_SBAND_FIRST_SYMBOL = IS_KUBAND_LAST_SYMBOL,
    IS_SBAND1_FIRST_SYMBOL = IS_KUBAND_LAST_SYMBOL,
    IS_Sb1FrameLk = IS_SBAND1_FIRST_SYMBOL,
    IS_Sb1CarrLk,
    IS_Sb1BitDet,
    IS_Sb1Handover,
    IS_Sb1Tracking,
    IS_Sb1SatSel,
    IS_Sb1CmdLnk,
    IS_Sb1RELPCh1,
    IS_Sb1RELPCh2,
    IS_Sb1MUXCh1,
    IS_Sb1MUXCh2,
    IS_Sb1CmdAz,
    IS_Sb1CmdEl,
    IS_Sb1AGC,
    IS_Sb1AZTemp,
    IS_Sb1ELTemp,
    IS_Sb1RFGTemp,
    IS_Sb1TransTemp,
    IS_Sb1BSPTemp,
    IS_SBAND1_LAST_SYMBOL,
    IS_SBAND1_MAX_SYMBOLS = (IS_SBAND1_LAST_SYMBOL - IS_SBAND1_FIRST_SYMBOL),

    /* must be in sequence */
    IS_SBAND2_FIRST_SYMBOL = IS_SBAND1_LAST_SYMBOL,
    IS_Sb2FrameLk = IS_SBAND2_FIRST_SYMBOL,
    IS_Sb2CarrLk,
    IS_Sb2BitDet,
    IS_Sb2Handover,
    IS_Sb2Tracking,
    IS_Sb2SatSel,
    IS_Sb2CmdLnk,
    IS_Sb2RELPCh1,
    IS_Sb2RELPCh2,
    IS_Sb2MUXCh1,
    IS_Sb2MUXCh2,
    IS_Sb2CmdAz,
    IS_Sb2CmdEl,
    IS_Sb2AGC,
    IS_Sb2AZTemp,
    IS_Sb2ELTemp,
    IS_Sb2RFGTemp,
    IS_Sb2TransTemp,
    IS_Sb2BSPTemp,
    IS_SBAND2_LAST_SYMBOL,
    IS_SBAND2_MAX_SYMBOLS = (IS_SBAND2_LAST_SYMBOL - IS_SBAND2_FIRST_SYMBOL),

    IS_SBAND_LAST_SYMBOL = IS_SBAND2_LAST_SYMBOL,
    IS_SBAND_MAX_SYMBOLS = (IS_SBAND1_MAX_SYMBOLS + IS_SBAND2_MAX_SYMBOLS),

    /**
     * This is where the kuband mask enumerations would normally be:
     *  1 thru MAX_KUBAND_MASKS
     *  IS_KuPtgMask
     *  IS_KuLowerELMask
     *  IS_KuUpperELMask
     *  IS_KuLowerXELMask
     *  IS_KuUpperXELMask
     */

    IS_KU_PTG_MASK_FIRST_SYMBOL = IS_SBAND2_LAST_SYMBOL,
    IS_KU_PTG_MASK_LAST_SYMBOL = (IS_KU_PTG_MASK_FIRST_SYMBOL + MAX_KUBAND_MASKS),
    IS_KU_PTG_MASK_MAX_SYMBOLS = (IS_KU_PTG_MASK_LAST_SYMBOL - IS_KU_PTG_MASK_FIRST_SYMBOL),

    IS_KU_MASK_FIRST_SYMBOL = IS_KU_PTG_MASK_LAST_SYMBOL,
    IS_KU_MASK_LAST_SYMBOL = (IS_KU_MASK_FIRST_SYMBOL + (MAX_KUBAND_MASKS * 4)),
    IS_KU_MASK_MAX_SYMBOLS = (IS_KU_MASK_LAST_SYMBOL - IS_KU_MASK_FIRST_SYMBOL),

    IS_MaxNumberOfSymbols = IS_KU_MASK_LAST_SYMBOL
};

/**
 * Max lenght of the symbol name and tag
 */
#define  IS_NAME_LEN   32

/**
 * Defines the isp symbol record, this data will be
 * loaded and eventually passed thru to the realtime
 * data handler. The realtime data handler will then
 * push this data to the gui.
 */
typedef struct SymbolRec
{
    guint symbol_id;                 /**< symbol ID, matches index into symbol array */
    gchar        symbol_name[IS_NAME_LEN];   /**< symbol name */
    gchar        symbol_tag [IS_NAME_LEN];   /**< symbol tag */

    gdouble      value;                      /**< current value */
    gchar        value_str[ISP_MESSAGE_SIZE];/**< message or converted value to string */
    size_t      value_str_len;               /**< length of value_str */
    guchar value_color;                      /**< color for value */

    StStatusStruct status;                   /**< telemetry status */
    gboolean    status_update;               /**< flag indicating the data is ready for update */

    gdouble      event_time;                 /**< time of event */
    gboolean    event_time_update;           /**< flag indicating event is ready for update */
} SymbolRec;


/*
==============
Public Methods
==============
*/
void                IS_Constructor                  (void);
void                IS_Destructor                   (void);

gint                 IS_GetSymbolCount               (void);
SymbolRec           *IS_GetSymbol                   (guint symbol_id);
void                IS_SetSymbolStatus              (gboolean status_update);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __ISPSYMBOLS_H__ */
