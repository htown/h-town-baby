/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __GEOMETRY_H__
#define __GEOMETRY_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
gint                 CCW                             (Point, Point, Point);
gint                 GP_Intersect                    (Line, Line);
gint                 GP_InsidePolygon                (Point, Point *, gint);
gint                 GP_IntersectLine                (Line, Line, Line *);
gint                 GP_IntersectPoint               (Line, Line, Point *);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __GEOMETRY_H__ */
