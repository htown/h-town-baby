/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __ANTENNADIALOG_H__
#define __ANTENNADIALOG_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                AD_Create                       (GtkWindow *parent);
void                AD_Open                         (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static GtkWidget    *create_headers                 (void);
static GtkWidget    *create_entries                 (void);
static GtkWidget    *create_buttons                 (void);

static void         load_from_dynamics              (void);
static void         load_from_config                (void);

static void         reset_from_config_cb            (GtkButton *button, void * data);

static void         response_cb                     (GtkWidget *widget, gint response_id, void * data);
static gboolean     key_press_cb                    (GtkWidget *widget, GdkEventKey *event, void * data);
static gboolean     valid_key                       (guint keyval, gint cursor_position);
static gchar         *format_value                   (GtkWidget *entry_w, GdkEventKey *event);

#endif /* PRIVATE */
#endif /* __ANTENNADIALOG_H__ */
