/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PREDICTKUMASKDIALOG_H__
#define __PREDICTKUMASKDIALOG_H__

void                PKMD_Create                     (GtkWindow *parent);
void                PKMD_Open                       (void);

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #include "MaskFile.h"
    #define PRIVATE
#else
    #include "AntMan.h"
    #include "MaskFile.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/

void PKD_EnableUseRealtime(gboolean enabled);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

    #define LOWER_EL_INDEX  0   /**< lower el index into mask_data */
    #define UPPER_EL_INDEX  2   /**< upper el index into mask_data */
    #define LOWER_XEL_INDEX 0   /**< lower xel index into mask_data */
    #define UPPER_XEL_INDEX 2   /**< upper xel index into mask_data */

/**
 * Defines the kuband mask widget data that makes
 * up the dialog components.
 */
typedef struct KuMaskInfo
{
    GtkWidget   *mask_name;                         /**< mask name field */

    GtkWidget   *entry_w   [MAX_LOWER_UPPER];       /**< upper/lower el/xel fields */
    GtkWidget   *rt_enabled[MAX_LOWER_UPPER];       /**< checkboxes indicating realtime data */

    GtkWidget   *visibility[MAX_VISIBILITY_MASKS];  /**< checkboxes indicating visibility */

    PolyLine    *mask_data;                         /**< mask data */
} KuMaskInfo;

/*
===============
Private Methods
===============
*/
static GtkWidget    *create_pkmd_container          (void);
static GtkWidget    *create_buttons                 (void);
static void         load_predict_masks              (void);
static void         insert_table_item               (GtkWidget *table, GtkWidget *item, gint row, gint col,
                                                     GtkAttachOptions xoptions, GtkAttachOptions yoptions,
                                                     gint xpadding, gint ypadding);

static void         switch_page_cb                  (GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data);
static void         use_realtime_cb                 (GtkButton *button, void * data);
static void         clear_current_cb                (GtkButton *button, void * data);
static void         clear_all_cb                    (GtkButton *button, void * data);
static void         response_cb                     (GtkWidget *widget, gint response_id, void * data);

static void         set_kuband_mask_values          (void);
static void         set_kuband_mask_visibility      (void);
static void         set_kuband_realtime_button      (GtkToggleButton *button, gint predict_value, gint mask_num, gint value_index, gint use_xy);
static void         save_kuband_mask_names          (void);
static gboolean     is_default_name                 (const gchar *new_mask_name, gint mask_num);

#endif /* PRIVATE */
#endif /* __PREDICTKUMASKDIALOG_H__ */
