/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __CONFIGPARSER_H__
#define __CONFIGPARSER_H__

#include <gtk/gtk.h>

/*
=================
Public Defintions
=================
*/

/**
 * GEN PREDICT exit codes
 */
#define GEN_PREDICT_OK      (0)         /**< No errors */
#define NO_PREDICT_SOURCE   (1<<0)      /**< PREDICT_SOURCE not defined */
#define INVALID_SOURCE      (1<<1)      /**< PREDICT_SOURCE value not correct */
#define NO_START_STOP_TIME  (1<<2)      /**< Start and Stop times not set */
#define EMPTY_SUNABG        (1<<3)      /**< Output file for sun abg is zero length */
#define EMPTY_SUN_POSITION  (1<<4)      /**< Output file for sun position is zero length */
#define EMPTY_TDRS          (1<<5)      /**< Output file for tdrs is zero length */
#define NO_SUNABG           (1<<6)      /**< Output file for sun abg not generated */
#define NO_SUN_POSITION     (1<<7)      /**< Output file for sun position not generated */
#define NO_TDRS             (1<<8)      /**< Output file for tdrs not generated */
#define NO_CONFIG_FILE      (1<<9)      /**< Config file (iam.cfg) not specified */
#define SUNABG_COPY_FAILED  (1<<10)     /**< Copy failed for sun abg */
#define SUN_POSITION_COPY_FAILED   (1<<11)/**< Copy failed for sun position */
#define TDRS_COPY_FAILED    (1<<12)     /**< Copy failed for tdrs */
#define MAX_GEN_PREDICT_EXIT_CODES 13

/**
 * These are the predefined keywords that iam uses when searching
 * the iam.cfg file. This provides a consistent string search and
 * comparison when processing the config file.
 */
#define ISS_CONFIGURATION           "ISS_CONFIGURATION"
#define ISS_CONFIGURATION_FALLBACK  "ISS_CONFIGURATION_FALLBACK"

#define IAM_ISP_SYMBOL_FILE         "IAM_ISP_SYMBOL_FILE"

#define IAM_TOP_DATA_DIR            "IAM_TOP_DATA_DIR"
#define IAM_PREDICT_DIR             "IAM_PREDICT_DIR"

#define IAM_START_DIALOG            "IAM_START_DIALOG"

#define IAM_EVENT_DIALOG_INITIAL_TAB "IAM_EVENT_DIALOG_INITIAL_TAB"

#define IAM_IMAGES_DIR              "IAM_IMAGES_DIR"
#define IAM_IMAGE_ICON              "IAM_IMAGE_ICON"

#define IAM_AUTO_UPDATE_START       "IAM_AUTO_UPDATE_START"
#define IAM_AUTO_UPDATE_STOP        "IAM_AUTO_UPDATE_STOP"
#define IAM_AUTO_UPDATE_FREQUENCY   "IAM_AUTO_UPDATE_FREQUENCY"
#define IAM_MAX_AUTO_UPDATE_HOURS   "IAM_MAX_AUTO_UPDATE_HOURS"

#define IAM_CONFIG_DIR              "IAM_CONFIG_DIR"
#define IAM_CONFIG_FILE             "IAM_CONFIG_FILE"

#define IAM_MASK_DIR                "IAM_MASK_DIR"

#define IAM_REPORT_DIR              "IAM_REPORT_DIR"

#define IAM_DYNAM_STR_FILE          "IAM_DYNAM_STR_FILE"
#define IAM_DYNAMIC_DIR             "IAM_DYNAMIC_DIR"
#define IAM_DYNAMIC_FILE            "IAM_DYNAMIC_FILE"

#define IAM_SHO_FILE                "IAM_SHO_FILE"

#define IAM_JOINT_ANGLE_THRESHOLD   "IAM_JOINT_ANGLE_THRESHOLD"

#define IAM_PREDICT_SOURCE          "IAM_PREDICT_SOURCE"

#define IAM_DECAT_SIM_FILE          "IAM_DECAT_SIM_FILE"
#define IAM_DECAT_TEMPLATE          "IAM_DECAT_TEMPLATE"
#define IAM_DECAT_SIM_FILE          "IAM_DECAT_SIM_FILE"
#define IAM_DECAT_STATION           "IAM_DECAT_STATION"
#define IAM_DECAT_SUNABG_OUT        "IAM_DECAT_SUNABG_OUT"

#define IAM_BROWSER_EXE             "IAM_BROWSER_EXE"
#define IAM_USER_GUIDE              "IAM_USER_GUIDE"

#define IAM_SEND_PREDICT_DATA       "IAM_SEND_PREDICT_DATA"
#define IAM_LAST_DROP_BOX_TRANSMISSION   "IAM_LAST_DROP_BOX_TRANSMISSION"
#define IAM_HOST                    "IAM_HOST"
#define IAM_LOGIN                   "IAM_LOGIN"
#define IAM_PWD                     "IAM_PWD"
#define IAM_POIC_OUT                "IAM_POIC_OUT"
#define IAM_HSR_OUT                 "IAM_HSR_OUT"
#define IAM_ESA_OUT                 "IAM_ESA_OUT"
#define IAM_SHO_FILE_OUT            "IAM_SHO_FILE_OUT"
#define IAM_FORCE_FTP               "IAM_FORCE_FTP"

#define IAM_WAIT_PERIOD             "IAM_WAIT_PERIOD"

#define PREDICT_TDRS_INSHO_COLOR    "PREDICT_TDRS_INSHO_COLOR"
#define PREDICT_TDRS_OVERLAP_COLOR  "PREDICT_TDRS_OVERLAP_COLOR"
#define PREDICT_TDRS_OUTSHO_COLOR   "PREDICT_TDRS_OUTSHO_COLOR"
#define REALTIME_SELECTED_COLOR     "REALTIME_SELECTED_COLOR"
#define REALTIME_INVIEW_COLOR       "REALTIME_INVIEW_COLOR"
#define REALTIME_NOTINVIEW_COLOR    "REALTIME_NOTINVIEW_COLOR"
#define S_BAND_LGA_BORESIGHT_AZ     "S_BAND_LGA_BORESIGHT_AZ"
#define S_BAND_LGA_BORESIGHT_EL     "S_BAND_LGA_BORESIGHT_EL"

#define TEMP_CAUTION_MARGIN_DEG     "TEMP_CAUTION_MARGIN_DEG"

#define SBAND1_BSP_BSPLT_OP_MAX     "SBAND1_BSP_BSPLT_OP_MAX"
#define SBAND1_BSP_BSPLT_OP_MIN     "SBAND1_BSP_BSPLT_OP_MIN"
#define SBAND1_XPNDR_BSPLT_OP_MAX   "SBAND1_XPNDR_BSPLT_OP_MAX"
#define SBAND1_XPNDR_BSPLT_OP_MIN   "SBAND1_XPNDR_BSPLT_OP_MIN"
#define SBAND1_RFG_BSPLT_OP_MAX     "SBAND1_RFG_BSPLT_OP_MAX"
#define SBAND1_RFG_BSPLT_OP_MIN     "SBAND1_RFG_BSPLT_OP_MIN"
#define SBAND1_RFG_AZ_GMBL_OP_MAX   "SBAND1_RFG_AZ_GMBL_OP_MAX"
#define SBAND1_RFG_AZ_GMBL_OP_MIN   "SBAND1_RFG_AZ_GMBL_OP_MIN"
#define SBAND1_RFG_EL_GMBL_OP_MAX   "SBAND1_RFG_EL_GMBL_OP_MAX"
#define SBAND1_RFG_EL_GMBL_OP_MIN   "SBAND1_RFG_EL_GMBL_OP_MIN"

#define SBAND2_BSP_BSPLT_OP_MAX     "SBAND2_BSP_BSPLT_OP_MAX"
#define SBAND2_BSP_BSPLT_OP_MIN     "SBAND2_BSP_BSPLT_OP_MIN"
#define SBAND2_XPNDR_BSPLT_OP_MAX   "SBAND2_XPNDR_BSPLT_OP_MAX"
#define SBAND2_XPNDR_BSPLT_OP_MIN   "SBAND2_XPNDR_BSPLT_OP_MIN"
#define SBAND2_RFG_BSPLT_OP_MAX     "SBAND2_RFG_BSPLT_OP_MAX"
#define SBAND2_RFG_BSPLT_OP_MIN     "SBAND2_RFG_BSPLT_OP_MIN"
#define SBAND2_RFG_AZ_GMBL_OP_MAX   "SBAND2_RFG_AZ_GMBL_OP_MAX"
#define SBAND2_RFG_AZ_GMBL_OP_MIN   "SBAND2_RFG_AZ_GMBL_OP_MIN"
#define SBAND2_RFG_EL_GMBL_OP_MAX   "SBAND2_RFG_EL_GMBL_OP_MAX"
#define SBAND2_RFG_EL_GMBL_OP_MIN   "SBAND2_RFG_EL_GMBL_OP_MIN"

#define KBAND_SG1_OP_MAX            "KBAND_SG1_OP_MAX"
#define KBAND_SG1_OP_MIN            "KBAND_SG1_OP_MIN"
#define KBAND_SG2_OP_MAX            "KBAND_SG2_OP_MAX"
#define KBAND_SG2_OP_MIN            "KBAND_SG2_OP_MIN"
#define KBAND_SG3_OP_MAX            "KBAND_SG3_OP_MAX"
#define KBAND_SG3_OP_MIN            "KBAND_SG3_OP_MIN"
#define KBAND_SG4_OP_MAX            "KBAND_SG4_OP_MAX"
#define KBAND_SG4_OP_MIN            "KBAND_SG4_OP_MIN"
#define KBAND_SG5_OP_MAX            "KBAND_SG5_OP_MAX"
#define KBAND_SG5_OP_MIN            "KBAND_SG5_OP_MIN"
#define KBAND_SG6_OP_MAX            "KBAND_SG6_OP_MAX"
#define KBAND_SG6_OP_MIN            "KBAND_SG6_OP_MIN"
#define KBAND_SG7_OP_MAX            "KBAND_SG7_OP_MAX"
#define KBAND_SG7_OP_MIN            "KBAND_SG7_OP_MIN"
#define KBAND_SG8_OP_MAX            "KBAND_SG8_OP_MAX"
#define KBAND_SG8_OP_MIN            "KBAND_SG8_OP_MIN"
#define KBAND_TRC_PWR_SUP_OP_MAX    "KBAND_TRC_PWR_SUP_OP_MAX"
#define KBAND_TRC_PWR_SUP_OP_MIN    "KBAND_TRC_PWR_SUP_OP_MIN"
#define KBAND_TRC_PWR_AMP_OP_MAX    "KBAND_TRC_PWR_AMP_OP_MAX"
#define KBAND_TRC_PWR_AMP_OP_MIN    "KBAND_TRC_PWR_AMP_OP_MIN"
#define KBAND_TRC_SENSOR1_OP_MAX    "KBAND_TRC_SENSOR1_OP_MAX"
#define KBAND_TRC_SENSOR1_OP_MIN    "KBAND_TRC_SENSOR1_OP_MIN"
#define KBAND_TRC_SENSOR2_OP_MAX    "KBAND_TRC_SENSOR2_OP_MAX"
#define KBAND_TRC_SENSOR2_OP_MIN    "KBAND_TRC_SENSOR2_OP_MIN"
#define KBAND_TRC_SENSOR3_OP_MAX    "KBAND_TRC_SENSOR3_OP_MAX"
#define KBAND_TRC_SENSOR3_OP_MIN    "KBAND_TRC_SENSOR3_OP_MIN"
#define KBAND_HRM_OP_MAX            "KBAND_HRM_OP_MAX"
#define KBAND_HRM_OP_MIN            "KBAND_HRM_OP_MIN"
#define KBAND_HRFM_OP_MAX           "KBAND_HRFM_OP_MAX"
#define KBAND_HRFM_OP_MIN           "KBAND_HRFM_OP_MIN"
#define KBAND_VBSP_OP_MAX           "KBAND_VBSP_OP_MAX"
#define KBAND_VBSP_OP_MIN           "KBAND_VBSP_OP_MIN"

/**
 * define 3 autotrack and 3 openloop values
 */
#define MAX_GAO_VALUES  3

#define GAO_AUTO_TRACK1             "GAO_AUTO_TRACK1"
#define GAO_AUTO_TRACK2             "GAO_AUTO_TRACK2"
#define GAO_AUTO_TRACK3             "GAO_AUTO_TRACK3"

#define GAO_OPEN_LOOP1              "GAO_OPEN_LOOP1"
#define GAO_OPEN_LOOP2              "GAO_OPEN_LOOP2"
#define GAO_OPEN_LOOP3              "GAO_OPEN_LOOP3"

#define KUBAND_PRIMARY_MASK_COLOR   "KUBAND_PRIMARY_MASK_COLOR"
#define KUBAND_OPENLOOP_MASK_COLOR  "KUBAND_OPENLOOP_MASK_COLOR"
#define KUBAND_AUTOTRACK_MASK_COLOR "KUBAND_AUTOTRACK_MASK_COLOR"
#define KUBAND_SPIRAL_MASK_COLOR    "KUBAND_SPIRAL_MASK_COLOR"

#define SPIRAL_DEGREE               "SPIRAL_DEGREE"

#define SBAND_COORD_TRACKING_LIMIT  "SBAND_COORD_TRACKING_LIMIT"

#define SBAND1_LOCATION             "SBAND1_LOCATION"
#define SBAND2_LOCATION             "SBAND2_LOCATION"
#define KUBAND_LOCATION             "KUBAND_LOCATION"
#define KUBAND2_LOCATION            "KUBAND2_LOCATION"
#define GENERIC_LOCATION            "GENERIC_LOCATION"

#define IAM_BG_COLOR                "IAM_BG_COLOR"
#define IAM_FG_COLOR                "IAM_FG_COLOR"
#define IAM_DEGREE_COLOR            "IAM_DEGREE_COLOR"
#define IAM_TRACKING_COLOR          "IAM_TRACKING_COLOR"

#define IAM_BORESIGHT_COLOR         "IAM_BORESIGHT_COLOR"
#define IAM_FONT                    "IAM_FONT"

#define IAM_SUPPORT_PHONE           "IAM_SUPPORT_PHONE"
#define IAM_SUPPORT_EMAIL           "IAM_SUPPORT_EMAIL"

#define IAM_VIEW_ORIENTATION        "IAM_VIEW_ORIENTATION"
#define IAM_REALTIME_VIEW_STARTUP   "IAM_REALTIME_VIEW_STARTUP"
#define IAM_PREDICT_VIEW_STARTUP    "IAM_PREDICT_VIEW_STARTUP"
#define IAM_KUBAND_VIEW_STARTUP     "IAM_KUBAND_VIEW_STARTUP"
#define IAM_MAIN_ITERATION_CYCLES  "IAM_MAIN_ITERATION_CYCLES"

#define IAM_REALTIME_SBAND1_ENABLED_MASKS    "IAM_REALTIME_SBAND1_ENABLED_MASKS"
#define IAM_REALTIME_SBAND2_ENABLED_MASKS    "IAM_REALTIME_SBAND2_ENABLED_MASKS"
#define IAM_REALTIME_KUBAND_ENABLED_MASKS    "IAM_REALTIME_KUBAND_ENABLED_MASKS"
#define IAM_REALTIME_KUBAND2_ENABLED_MASKS   "IAM_REALTIME_KUBAND2_ENABLED_MASKS"
#define IAM_REALTIME_STATION_ENABLED_MASKS   "IAM_REALTIME_STATION_ENABLED_MASKS"

#define IAM_PREDICT_SBAND1_ENABLED_MASKS    "IAM_PREDICT_SBAND1_ENABLED_MASKS"
#define IAM_PREDICT_SBAND2_ENABLED_MASKS    "IAM_PREDICT_SBAND2_ENABLED_MASKS"
#define IAM_PREDICT_KUBAND_ENABLED_MASKS    "IAM_REALTIME_KUBAND_ENABLED_MASKS"
#define IAM_PREDICT_KUBAND2_ENABLED_MASKS   "IAM_REALTIME_KUBAND2_ENABLED_MASKS"
#define IAM_PREDICT_STATION_ENABLED_MASKS   "IAM_PREDICT_STATION_ENABLED_MASKS"

#define IAM_GTKRC                           "gtkrc-2.0"
#define IAM_GTKRC_REALTIME_FONT_SIZE        "gtkrc-realtime-font-size"
#define IAM_GTKRC_PREDICT_FONT_SIZE         "gtkrc-predict-font-size"
#define IAM_GTKRC_REALTIME_DATA_FONT_SIZE   "gtkrc-realtime-data-font-size"

#define IAM_PRINT_ENGINE            "IAM_PRINT_ENGINE"
#define IAM_JPEG_2_PS_CONVERTER     "IAM_JPEG_2_PS_CONVERTER"
#define IAM_TEXT_2_PS_CONVERTER     "IAM_TEXT_2_PS_CONVERTER"

#define TDRS_PREDICT_XML    "TDRS_PREDICT_XML"
#define SUNABG_PREDICT_XML  "SUNABG_PREDICT_XML"
#define SUNPOS_PREDICT_XML  "SUNPOS_PREDICT_XML"

/*
==============
Public Methods
==============
*/
gboolean            CP_Constructor                  (void);
void                CP_Destructor                   (void);

void                CP_StringTrim                   (gchar* s);
gchar                *CP_GetRootName                 (gchar *filename);

gboolean            CP_GetConfigData                (gchar *keyword, gchar *data);
gboolean            CP_GetGenPredictFile            (gchar *keyword, gchar *gen_predict_file, gchar *coverage_dir);
gboolean            CP_GetISSConfigValue            (gchar *data);

gchar                *CP_GetConfigDir                (void);
void                CP_SetConfigFilename            (gchar *configFilename);
gchar                *CP_GetConfigFilename           (void);
gboolean            CP_IsConfigFileValid            (void);

gboolean            CP_UpdateConfigValue            (gchar *key, gchar *value);
void                CP_WriteConfigFile              (void);

gchar                *CP_GetMaskDefFile              (gchar *file);
gchar                *CP_GetMaskFile                 (gchar *coord_dir, gchar *file);

gchar                *CP_GetReportFile               (gchar *file);

void                CP_SetLogLevel                  (void);
void                CP_SetLogHandler                (gchar *logFilename, GLogLevelFlags flags);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/**
 * Internal configuration data that is used by
 * the config parser.
 */
typedef struct ConfigData
{
    gchar *key;          /**< the hash key used for lookup */
    gchar *comment;      /**< comment field; if the iam.cfg has comments */
                        /**< this is where they are stored. this is */
                        /**< only used if the config file is written */
                        /**< back out; this will retain the comments. */
    gchar *value;        /**< the value stored at the key */
} ConfigData;

/*
===============
Private Methods
===============
*/
static void         buildXmlConfiguration           (const gchar *xmlConfigFile);
static void         commentConfig                   (void *data, const gchar *comment);
static void         startConfig                     (void *data, const char *el, const char **attr);
static void         endConfig                       (void *data, const char *el);
static gchar        *expandValue                    (gchar *value);
static gchar        *expandSymbol                   (gchar *value, gchar *pattern, int type);
static void         addComment                      (const gchar *comment);
static void         addKeyValue                     (const gchar *key, const gchar *value);
static void         addToHash                       (ConfigData *data);
static void         addToArray                      (ConfigData *data);
static void         updateHash                      (gchar *key, gchar *value);
static void         updateArray                     (gchar *key, gchar *value);
static void         deleteArrayData                 (gpointer data, gpointer user_data);

static gboolean     hashTableForeach                (void * key, void * value, void * user_data);
static void         deleteKey                       (void * data);
static void         deleteValue                     (void * data);


static void         backupFile                      (gchar *fname);
static void         writeIamConfigData              (void);

static gboolean     hasAtSign                       (gchar *data);
static gboolean     hasDollarSign                   (gchar *data);

static gchar        *get_keyword                    (gchar *data, gchar *search_pattern, gchar *first_part, gchar *last_part);
static gchar        *concat_keyword_data            (gchar *first_part, gchar *keyword_data, gchar *last_part);

static gchar        *getConfigFilename              (void);

static void         logToFile                       (const gchar *log_domain, GLogLevelFlags flags, const gchar *message, void * user_data);
static void         printToFile                     (const gchar *message);
static void         messageOut                      (const gchar *message);
static gchar        *getLongDate                    (void);
static void         loggingStart                    (void);
static void         loggingStop                     (void);

#endif /* PRIVATE */
#endif /* __CONFIGPARSER_H__ */
