/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __VECTOR_H__
#define __VECTOR_H__

/*
=================
Public Defintions
=================
*/

/**
 * Defines a vector
 */
typedef struct VECTOR
{
    gdouble      x;
    gdouble      y;
    gdouble      z;
} VECTOR;

/**
 * The transformation matrix
 */
typedef struct TRANSFORMATION_MATRIX
{
    VECTOR      col_1;
    VECTOR      col_2;
    VECTOR      col_3;
} TRANSFORMATION_MATRIX;

/**
 * Define vector macros
 */
#define SQR(x) ( (x)*(x) )
#define VSET(vA, aX, aY, aZ) { ((vA)->x = aX); ((vA)->y = aY); ((vA)->z = aZ); }
#define VCONST(k, vA, vC) {\
    (vC)->x = ((vA)->x*k); \
    (vC)->y = ((vA)->y*k); \
    (vC)->z = ((vA)->z*k); \
}
#define VADD(vA, vB, vC) {\
    (vC)->x = ((vA)->x + (vB)->x); \
    (vC)->y = ((vA)->y + (vB)->y); \
    (vC)->z = ((vA)->z + (vB)->z); \
}
#define VSUB(vA, vB, vC) {\
   (vC)->x = ((vA)->x - (vB)->x); \
   (vC)->y = ((vA)->y - (vB)->y); \
   (vC)->z = ((vA)->z - (vB)->z); \
}
#define VMAG(v) ( (sqrt( SQR((v)->x) + SQR((v)->y) + SQR((v)->z) )) )
#define VUNIT(vA, vB) {\
   gdouble magA = VMAG(vA); \
   if (magA == 0.0) \
      (vB)->x = (vB)->y = (vB)->z =0.0; \
   else \
   { \
      (vB)->x = ((vA)->x / magA); \
      (vB)->y = ((vA)->y / magA); \
      (vB)->z = ((vA)->z / magA); \
   } \
}

#define VDOT(vA, vB) ( ((vA)->x*(vB)->x + (vA)->y*(vB)->y + (vA)->z * (vB)->z) )
#define VCROSS(vA, vB, vC) {\
    ( (vC)->x = (((vA)->y*(vB)->z) - ((vA)->z*(vB)->y)) ); \
    ( (vC)->y = (((vA)->z*(vB)->x) - ((vA)->x*(vB)->z)) ); \
    ( (vC)->z = (((vA)->x*(vB)->y) - ((vA)->y*(vB)->x)) ); \
}
#define TRANSFORM_VECTOR(tA, vB, vC) {\
   (vC)->x = ( ((tA)->col_1.x * (vB)->x) + ((tA)->col_2.x * (vB)->y) + ((tA)->col_3.x * (vB)->z) ); \
   (vC)->y = ( ((tA)->col_1.y * (vB)->x) + ((tA)->col_2.y * (vB)->y) + ((tA)->col_3.y * (vB)->z) ); \
   (vC)->z = ( ((tA)->col_1.z * (vB)->x) + ((tA)->col_2.z * (vB)->y) + ((tA)->col_3.z * (vB)->z) ); \
}

/*
==============
Public Methods
==============
*/
void                Vset                            (VECTOR *, gdouble , gdouble , gdouble);
VECTOR              *Vconst                         (gdouble, VECTOR *);
VECTOR              *Vadd                           (VECTOR *, VECTOR *);
VECTOR              *Vsub                           (VECTOR *, VECTOR *);
gdouble              Vmag                            (VECTOR *);
VECTOR              *Vunit                          (VECTOR *);
gdouble              Vdot                            (VECTOR *, VECTOR *);
VECTOR              *Vcross                         (VECTOR *, VECTOR *);

VECTOR              *TransformVector                (TRANSFORMATION_MATRIX *, VECTOR *, VECTOR *);

void                PrintMatrix                     (gchar *matrixName, TRANSFORMATION_MATRIX *);
void                PrintVector                     (gchar *vectorName, VECTOR *);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __VECTOR_H__ */
