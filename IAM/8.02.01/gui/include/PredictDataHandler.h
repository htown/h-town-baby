/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __PREDICTDATAHANDLER_H__
#define __PREDICTDATAHANDLER_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
GList               *PDH_GetCurrentPt               (void);
gint                PDH_GetPointColor               (GList *pt);

void                PDH_Constructor                 (void);
void                PDH_Destructor                  (void);

GList               *PDH_GetData                    (void);
void                PDH_SetData                     (GList *psList);

void                PDH_StepEvent                   (gint direction);
GList               *PDH_StepPoint                  (gint dir, gint stepType);
void                PDH_SetPoint                    (gint location);

gboolean            PDH_UpdateEvents                (void);
void                PDH_SelectPoint                 (void);

gboolean            PDH_HasEvent                    (gint direction);
gboolean            PDH_HasPoint                    (gint direction);

void                PDH_MarkPosition                (GList *newPt);
GList               *PDH_InterpolatePoint           (GList *ps, gchar *time_str);
GList               *PDH_NearestPoint               (WindowData *windata, GList *ps, Point click);

void                PDH_SetTdrsInfo                 (GPtrArray *tdrsInfo);
TdrsInfo            *PDH_GetTdrsInfoById            (gint id);
TdrsInfo            *PDH_GetTdrsInfoByShoName       (gchar *shoName);

void                PDH_SetAutoTimes                (gchar *startTime, gchar *stopTime);
void                PDH_SetFreqOffsetSecs           (gint freqOffsetSecs);
gint                PDH_CheckWrap                   (Point *p0, Point *p1, Point *p2, Point *p3, gint coord);
gboolean            PDH_IsWrapped                   (gint coord, GList *pt);

void                PDH_GetCoverage                 (GtkWindow *parent);

Point               *PDH_GetCoordinatePoint         (gint coord, PredictPt *ppt);

void                PDH_DeletePredictDataPts        (GList *predicts);


#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         setColorIds                     (void);
static void         setCoverageDir                  (void);
static void         setFreqOffset                   (void);
static void         setFtpAccess                    (void);

static GList        *nextPoint                      (GList *pt, gint stepType);
static GList        *previousPoint                  (GList *pt, gint stepType);

static GList        *interpolateOnEvent             (GList *pts, gchar *time, Point *point);

static void         freePredictSetList              (GList *psList);
static void         freePredictPtList               (GList *ptList);
static void         freeTdrsInfoArray               (GPtrArray *tdrs);

static gboolean     showEvent                       (PredictSet *ps, PredictSet *ps2);
static gboolean     setSHO                          (PredictSet *ps, PredictSet *ps2);
static gboolean     setEventsToShow                 (void);
static void         setEventToShowByLos             (void );

static gboolean     checkXMLFileStatus              (void *data);
static gboolean     hasXmlCoverageFiles             (gchar *coverageDir);

static void         checkWraps                      (GList *sets, gint line_type);
static void         checkEndWraps                   (GList *ptList);
static PredictPt    *doWrap                         (GList *ptList, gint dir);

static gboolean     generateTime                    (gchar *start_time, gchar *stop_time);
static gboolean     generateTimeFromXml             (GDir *dir, gint *start_year, gchar *start_time, gint *stop_year, gchar *stop_time);
static void         resetCoverage                   (gchar *coverage_dir);
static gchar        *getCoverageIdentifier          (void);

static void         setCoordinatePoint              (gint coord, PredictPt *ppt, Point *point);

static void         selectEventLocksel              (gint direction);
static void         selectEventDefault              (gint direction);

static void         infoMessage0                    (gboolean predict_changed, gboolean sun_position_changed, gboolean sunabg_changed, gboolean sho_changed);
static void         infoMessage1                    (void);

static gboolean     getAutoUpdateValue              (gchar *autoUpdateValue, gchar *timeStr);


#endif /* PRIVATE */
#endif /* __PREDICTDATAHANDLER_H__ */
