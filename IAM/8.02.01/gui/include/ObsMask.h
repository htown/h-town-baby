/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __OBSMASK_H__
#define __OBSMASK_H__

#include <stdio.h>

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/
/**
 * Define internal data structures
 */
typedef struct WindowInfo
{
    gint          width;
    gint          height;
    GdkRectangle *winpts;
}  WindowInfo;

typedef struct ObsPoint
{
    gint          in_range;
    GdkPoint     pt;
}  ObsPoint;

typedef struct ObsInfo
{
    gchar        file_name[50];
    gint         count;
    gint         in_range_count;
    ObsPoint   *mask;
    WindowInfo  rt_window;
    WindowInfo  pd_window;
} ObsInfo;

typedef struct AntennaInfo
{
    ObsInfo  obscuration;
    ObsInfo  shuttle;
} AntennaInfo;

/*
==============
Public Methods
==============
*/
gboolean            OM_Constructor                  (void);
void                OM_Destructor                   (void);
void                OM_DrawMasks                    (WindowData *windata, gint display, gint coord_view);
void                OM_ChangeMasks                  (void);
void                OM_ShowCone                     (gint display, gboolean shown);
void                OM_ShowShuttle                  (gint display, gboolean shown);
gint                 OM_PointBlocked                 (Point *pt, gint coord);
void                OM_PrintBlockage                (FILE *fp, PredictSet *ps);
void                OM_ForceRedraw                  (gint display, gint coord_view);


#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gboolean     load_all_masks                  (void);
static gboolean     load_mask                       (ObsInfo *obs, gchar *title, gint coord);
static gint          mask_entry_compare              (const void *a, const void *b);
static AntennaInfo  *select_antenna                 (gint coord, gint isCone, gchar *title_head);
static void         draw_mask                       (WindowData *windata, gint display, ObsInfo *obs, gchar *color_name, gint coord, GdkDrawable *drawable);
static gint          find_point                      (ObsInfo *obs, Point *pt);
static void         print_blockage                  (ObsInfo *obs, FILE *fp, PredictSet *ps, gchar *title);
static void         delete_mask                     (ObsInfo *obs);

#endif /* PRIVATE */
#endif /* __OBSMASK_H__ */
