/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __SOLARDRAW_H__
#define __SOLARDRAW_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #include "TimeStrings.h"
    #include "Conversions.h"
    #define PRIVATE
#else
    #include "AntMan.h"
    #include "TimeStrings.h"
    #include "Conversions.h"
#endif

/*
=================
Public Defintions
=================
*/

/**
 * Defines the solar point data
 */
typedef struct SolarPoint
{
    gchar               timeStr[TIME_STR_LEN];  /**< time at solar point */
    Point               pt[NUM_COORDS];         /**< data point */
} SolarPoint;

/*
==============
Public Methods
==============
*/
void                SD_Constructor                  (void);
void                SD_Destructor                   (void);
void                SD_FreeSolarPointList           (GList *spList);

void                SD_DrawData                     (WindowData *, gint coord, GList *sets, GdkDrawable *drawable);
void                SD_DrawSymbolAtTime             (WindowData *, gint coord, gchar *current_time, GdkDrawable *drawable);
void                SD_DrawSymbolAtPoint            (WindowData *windata, gint coord, gint x, gint y, GdkDrawable *drawable);

gboolean            SD_GetPoint                     (gint coord, gchar *current_time, gint *x, gint *y);

void                SD_SetData                      (GList *solarList);
GList               *SD_GetData                     (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static GList        *locatePoint                    (gchar *time);
static void         drawSymbol                      (WindowData *windata, gint coord, gint x, gint y, GdkDrawable *drawable );
static void         drawSband1Line                  (WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable );
static void         drawSband2Line                  (WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable );
static void         drawKubandLine                  (WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable, gint coord );
static void         drawStationLine                 (WindowData *windata, GdkGC *gc, GList *spList1, GList *spList2, GdkDrawable *drawable );

#endif /* PRIVATE */
#endif /* __SOLARDRAW_H__ */
