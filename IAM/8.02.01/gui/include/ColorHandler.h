/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __COLORHANDLER_H__
#define __COLORHANDLER_H__

#include <gtk/gtk.h>

/*
=================
Public Defintions
=================
*/

/**
 * Defines the color entry information.
 */
#define COLOR_TABLE_INCREMENT   5

typedef struct ColorEntry
{
    gchar        name[32];       /**< the name of the color */
    GdkColor    *color;         /**< has the pixel and rgb value */
} ColorEntry;

/**
 * Commonly used colors for various ISP status'
 */
#define GOOD_STATUS     "Green"     /**< Valid ISP Status */
#define DEAD_STATUS     "Red"       /**< Dead ISP Status */
#define BAD_STATUS      "Red"       /**< Bad ISP Status */

#define GOOD_VALUE      "Green"     /**< ISP Value is good */
#define BAD_VALUE       "Red"       /**< ISP Value is bad */
#define WARN_VALUE      "Yellow"    /**< ISP Value is out of range */

#define BG_COLOR        "#999999"   /**< Background color to use */
#define FG_COLOR        "Black"     /**< Foreground color to use */
#define DEGREE_COLOR    "#FAFAF0"   /**< Degree symbol color */

/**
 * The following are the enumerations for the default colors which
 * must match the color name list defined in the source file.
 */
enum Color
{
    GC_BG           = -3,
    GC_INVALIDCOLOR = -2,
    GC_USECURRENT   = -1,
    GC_FIRSTCOLOR   =  0,
    GC_BLACK        = GC_FIRSTCOLOR,
    GC_WHITE,
    GC_RED,
    GC_MAROON,
    GC_PINK,
    GC_ORANGERED,
    GC_ORANGE,
    GC_DARKORANGE,
    GC_YELLOW,
    GC_GOLD,
    GC_YELLOWGREEN,
    GC_GREEN,
    GC_PALEGREEN,
    GC_SPRINGREEN,
    GC_TURQUOISE,
    GC_CHARTREUSE,
    GC_SEAGREEN,
    GC_FORESTGREEN,
    GC_BLUE,
    GC_CYAN,
    GC_LIGHTBLUE,
    GC_SKYBLUE,
    GC_MIDNIGHTBLUE,
    GC_NAVYBLUE,
    GC_CORNFLOWERBLUE,
    GC_STEELBLUE,
    GC_SLATEBLUE,
    GC_CADETBLUE,
    GC_VIOLET,
    GC_PURPLE,
    GC_MAGENTA,
    GC_LAVENDER,
    GC_VIOLETRED,
    GC_BROWN,
    GC_ROSYBROWN,
    GC_INDIANRED,
    GC_SIENNA,
    GC_BEIGE,
    GC_WHEAT,
    GC_DIMGRAY,
    GC_LIGHTGRAY,
    GC_GRAY,
    GC_DARKSLATEGRAY,
    GC_GRAY50,

    GC_LASTCOLOR,
    GC_NUMCOLORS = GC_LASTCOLOR
};

/*
==============
Public Methods
==============
*/
void                CH_Constructor                  (void);
void                CH_Destructor                   (void);
gboolean            CH_AddColor                     (gchar *color_name, GdkColor *gcolor);
void                CH_SetColor                     (GdkGC *, GdkColor *);
gint                 CH_GetColor                     (gchar *);
gchar                *CH_GetColorName                (gint);
GdkGC               *CH_SetColorGC                  (GtkWidget *widget, gchar *color_name);
GdkColor            *CH_GetGdkColorByIndex          (gint colorIndex);
GdkColor            *CH_GetGdkColorByName           (gchar *color_name);
gchar                *CH_GetColorNameByGdkColor      (GdkColor *gcolor);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gboolean     isColor                         (gchar *color_name);
static gint          getColorIndex                   (gchar *color_name);
static ColorEntry   *addColor                       (gchar *color_name, GdkColor *gcolor);
static ColorEntry   *addGdkColor                    (GdkColor *gcolor);

static gboolean     hashTableForeach                (void * key, void * value, void * user_data);
static void         deleteHashTableKey              (void * data);
static void         deleteHashTableValue            (void * data);

#endif /* PRIVATE */
#endif /* __COLORHANDLER_H__ */
