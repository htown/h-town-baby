/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __FONTHANDLER_H__
#define __FONTHANDLER_H__

#include <pango/pango.h>

/*
=================
Public Defintions
=================
*/
/**
 * The default font information
 */
#define     FONT_NAME           "Sans"
#ifdef G_OS_WIN32
#define     XL_FONT_SIZE        12
#define     L_FONT_SIZE         10
#define     M_FONT_SIZE         8
#define     S_FONT_SIZE         6
#define     XS_FONT_SIZE        5
#else
#define     XL_FONT_SIZE        13
#define     L_FONT_SIZE         10
#define     M_FONT_SIZE         9
#define     S_FONT_SIZE         6
#define     XS_FONT_SIZE        5
#endif

/**
 * convert integer token to string
 * these macros are used to convert numbers to strings
 */
#define     xstr(s)             str(s)
#define     str(s)              #s

#define     _xl_font_size       xstr(XL_FONT_SIZE)
#define     _l_font_size        xstr(L_FONT_SIZE)
#define     _m_font_size        xstr(M_FONT_SIZE)
#define     _s_font_size        xstr(S_FONT_SIZE)

#define     BOLD_KEYWORD        "Bold"

#define     DEFAULT_FONT_SIZE   L_FONT_SIZE

/**
 * Text string alignment constants
 */
#define CENTER_ALIGN            (float)(0.5)

#define LEFT_ALIGN              (float)(0.0)
#define RIGHT_ALIGN             (float)(1.0)

#define TOP_ALIGN               (float)(0.0)
#define BOTTOM_ALIGN            (float)(1.0)

/*
==============
Public Methods
==============
*/
gchar                *FH_GetFontName                 (void);
gchar                *FH_GetLargeBoldFont            (void);
gchar                *FH_GetLargePlainFont           (void);
gchar                *FH_GetDefaultBoldFont          (void);
gchar                *FH_GetDefaultPlainFont         (void);
gchar                *FH_GetSmallBoldFont            (void);
gchar                *FH_GetSmallPlainFont           (void);
gint                 FH_GetFontHeight                (PangoLayout *layout);
gint                 FH_GetFontWidth                 (PangoLayout *layout);
void                FH_AddRCFontStyle               (gchar *filename, guint font_size, const gchar *style_name);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static gint          fontDimension                   (PangoLayout *layout, gint dimension_type);

#endif /* PRIVATE */
#endif /* __FONTHANDLER_H__ */
