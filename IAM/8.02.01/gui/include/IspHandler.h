/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __ISPHANDLER_H__
#define __ISPHANDLER_H__

#ifdef PRIVATE
    #undef PRIVATE
    #include "IspSymbols.h"
    #define PRIVATE
#else
    #include "IspSymbols.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                IH_Constructor                  (void);
void                IH_Destructor                   (void);

void                IH_UpdateTime                   (void);
void                IH_UpdateRiseSet                (void);
void                IH_UpdateSBand                  (void);
void                IH_UpdateKuBand                 (void);
void                IH_UpdateKuMisCmpr              (void);
void                IH_UpdatePathGO                 (void);
void                IH_UpdateKuMasks                (void);
void                IH_UpdateTDRSSel                (void);
void                IH_UpdateTDRS                   (void);
void                IH_UpdateTDRSValues             (gint coord, gboolean add_point, gboolean update_tdrs_value);
void                IH_UpdateAngle                  (void);
void                IH_AutoUpdate                   (void);

void                IH_LoadTempLimits               (void);

void                IH_SetDeadStatus                (void);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

#define IS_STATIC(_status)  (_status == STSTATIC)
#define IS_DEAD(_status)    (_status == STDEAD)
#define IS_MISSING(_status) (_status == STMISSING)
#define IS_GOOD(_status)    (_status == STDEFAULT)
#define IS_REALLY_GOOD(_status)   ((_status != STSTATIC) && (_status != STDEAD) && (_status != STMISSING))

/**
 * Data node type
 */
typedef struct DataNodeType
{
    gint     num_stored;             /**< number of stored nodes */
    gint     num_to_store;           /**< number to store */
    gint     min_num;                /**< minimum nodes to store */
    gint     buff_index;             /**< current index */
    gdouble  *X_data;                /**< X data */
    gdouble  *T_data;                /**< T data */
} DataNodeType;

/**
 * Returns number of elements
 */
#define ELEMENTS(str)       (sizeof(str)/sizeof(((str)[0])))

/**
 * Determines the which is the best status to return
 */
#define MOST_SIGNIFICANT_STATUS_GOOD(_status) ((STDEFAULT == _status) || \
                                              ((STMISSING != _status) && (STDEAD != _status)))

/**
 * Copies null to end of string
 */
#define COPYNULLSTR(x,y)    g_stpcpy(x, (y != NULL) ? y : " ")

/**
 * Temperation range types
 */
enum TempRangeType
{
    MAX_TEMP,                       /**< max temp */
    MIN_TEMP,                       /**< min temp */

    MAX_TEMPS
};

/**
 * Temperature range indexes for sband and kuband
 */
enum ORU_TempType
{
    SB1_AZ_RANGE,                   /**< sband1 az */
    SB1_EL_RANGE,                   /**< sband1 el */
    SB1_RFG_RANGE,                  /**< sband1 rfg */
    SB1_TRANS_RANGE,                /**< sband1 trans */
    SB1_BSP_RANGE,                  /**< sband1 bsp */

    SB2_AZ_RANGE,                   /**< sband2 az */
    SB2_EL_RANGE,                   /**< sband2 el */
    SB2_RFG_RANGE,                  /**< sband2 rfg */
    SB2_TRANS_RANGE,                /**< sband2 trans */
    SB2_BSP_RANGE,                  /**< sband2 bsp */

    KU_SG1_RANGE,                   /**< kuband sg1 */
    KU_SG2_RANGE,                   /**< kuband sg2 */
    KU_SG3_RANGE,                   /**< kuband sg3 */
    KU_SG4_RANGE,                   /**< kuband sg4 */
    KU_SG5_RANGE,                   /**< kuband sg5 */
    KU_SG6_RANGE,                   /**< kuband sg6 */
    KU_SG7_RANGE,                   /**< kuband sg7 */
    KU_SG8_RANGE,                   /**< kuband sg8 */
    KU_TRCS1_RANGE,                 /**< kuband trcs1 */
    KU_TRCS2_RANGE,                 /**< kuband trcs2 */
    KU_TRCS3_RANGE,                 /**< kuband trcs3 */
    KU_PWR_AMP_RANGE,               /**< kuband power amp */
    KU_PWR_SUP_RANGE,               /**< kuband power supply */
    KU_HRM_RANGE,                   /**< kuband hrm */
    KU_HRFM_RANGE,                  /**< kuband hrfm */
    KU_VBSP_RANGE,                  /**< kuband vbsp */

    MAX_TEMP_RANGES
};

/**
 * TDRS indexes for TDRW, TDRE, and SUN
 */
enum AZEL_Type
{
    TDRW_AZIMUTH,
    TDRW_ELEVATION,

    TDRE_AZIMUTH,
    TDRE_ELEVATION,

    SUN_AZIMUTH,
    SUN_ELEVATION
};

/*
===============
Private Methods
===============
*/
static void         load_temp_limits                (void);
static void         load_gao                        (void);
static void         load_joint_angle_threshold      (void);
static gboolean     check_threshold                 (gdouble value, gint angle_idx);

static StStatusStruct *get_status                   (SymbolRec *x, SymbolRec *y, SymbolRec *z);
static gboolean     status_or_value_changed         (SymbolRec *x, SymbolRec *y, SymbolRec *z);
static gint         comp_tdrs_status                (gint tdrs);

static gint         get_temp_range_index            (gint symbol_id);
static gdouble      get_gao_degree                  (gdouble value);

static guchar       set_value_color                 (SymbolRec *symbol);
static void         update_kumask                   (SymbolRec *symbol);
static void         update_sat_id                   (void);
static void         update_inview                   (void);

static guchar       convert_status2color            (StStatusStruct status);

static void         set_pathgo_value                (SymbolRec *symbol, char *good_val);

static void         setKuPWRL                       (SymbolRec *symbol);
static void         setKuVolts                      (SymbolRec *symbol);
static void         setKuTemp                       (SymbolRec *symbol);
static void         setKuTRC                        (SymbolRec *symbol);
static void         setKuXel                        (SymbolRec *symbol);
static void         setKuAmp                        (SymbolRec *symbol);
static void         setKuPLCAct                     (SymbolRec *symbol);
static void         setKuPLCPend                    (SymbolRec *symbol);
static void         setKuPtgAct                     (SymbolRec *symbol);
static void         setKuPtgPend                    (SymbolRec *symbol);
static void         setKuTDRSSelect                 (SymbolRec *symbol);
static void         setKuAutotrack                  (SymbolRec *symbol);
static void         setKuHandover                   (SymbolRec *symbol);
static void         setKuRetry                      (SymbolRec *symbol);
static void         setKuGAO                        (SymbolRec *symbol);


#endif /* PRIVATE */
#endif /* __ISPHANDLER_H__ */
