/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __AUTOUPDATEDIALOG_H__
#define __AUTOUPDATEDIALOG_H__

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                AUD_Create                      (GtkWindow *parent);
void                AUD_Open                        (void);
gchar               *AUD_GetTime                    (glong time_type);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
static void         responseCb                      (GtkWidget *widget, gint response_id, void * data);
static gboolean     keyPressTimeCb                  (GtkWidget *widget, GdkEventKey *event, void * data);
static gboolean     keyPressFreqCb                  (GtkWidget *widget, GdkEventKey *event, void * data);
static gboolean     loadAutoConfigData              (gchar *start_time, gchar *stop_time, gchar *frequency);

static gboolean     validInputData                  (void);
static gboolean     validOffset                     (gchar *offset, gint maxStopOffset);

#endif /* PRIVATE */
#endif /* __AUTOUPDATEDIALOG_H__ */
