/********************************************************/
/***  Copyright (C) 2000                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#ifndef __WINDOWGRID_H__
#define __WINDOWGRID_H__

#include <gtk/gtk.h>

#ifdef PRIVATE
    #undef PRIVATE
    #include "AntMan.h"
    #define PRIVATE
#else
    #include "AntMan.h"
#endif

/*
=================
Public Defintions
=================
*/

/*
==============
Public Methods
==============
*/
void                WG_PointToWindow                (gint coord, gint width, gint height, gint coord_x, gint coord_y, gint *window_x, gint *window_y);
void                WG_WindowToPoint                (gint coord, gint width, gint height, gint window_x, gint window_y, gint *coord_x, gint *coord_y);
void                WG_PointToFlatCoord             (gint coord, gint width, gint height, gint coord_x, gint coord_y, gint *window_x, gint *window_y, gint fwdConversion);

#ifdef PRIVATE

/*
==================
Private Defintions
==================
*/

/*
===============
Private Methods
===============
*/
#endif /* PRIVATE */
#endif /* __WINDOWGRID_H__ */
