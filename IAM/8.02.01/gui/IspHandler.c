/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronaut    ics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <limits.h>

#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "IspHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "Conversions.h"
#include "MessageHandler.h"
#include "MemoryHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDataHandler.h"
#include "RealtimeDialog.h"
#include "RealtimeRFDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/IspHandler.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


#ifdef G_OS_WIN32
/* so the windows it.lib will link */
extern "C"
{
    #include "status.h"
}
#else
    #include "status.h"
#endif

/**************************************************************************
* Private Data Definitions
**************************************************************************/
static gdouble thisOpsTempRanges[MAX_TEMP_RANGES][MAX_TEMPS];
static gdouble thisMax_Min_Range[MAX_TEMP_RANGES][MAX_TEMPS];

gint gmt_valid = False;

/* True if selected; index 0 TDRW, 1 TDRE */
enum TdrsSelect
{
    NotSelected,
    Selected
};
static gint thisSelected[2] = { False, False };

/* True if in view; index 0 TDRW, 1 TDRE, 2 SUN */
static gint thisInView[MAX_TDRS] = { False, False, False };

#define SEC_PER_DAY  86400
#define SEC_PER_HOUR 3600

#define GO_STR          "GO"
#define SG1_STR         "SG1"
#define SG2_STR         "SG2"

#define DASH_STRING     "----"
#define LOCK_STRING     "Lock"
#define BAD_STRING      "Bad"
#define ERR_STRING      "Err"
#define E_STRING        "E"
#define EARLY_STRING    "Early"
#define LATE_STRING     "Late"
#define STOP_STRING     "Stop"
#define NORMAL          "Normal"
#define MANUAL_STRING   "Manual"
#define AUTO_STRING     "Auto"
#define INACTIVE_STRING "InActive"
#define ACTIVE_STRING   "Active"
#define RESET_STRING    "Reset"
#define OFF_STRING      "Off"
#define ON_STRING       "On"
#define INHIBIT_STRING  "Inhibit"
#define ENABLED_STRING  "Enable"
#define OPLOOP_STRING   "OpLoop"
#define ATRK_STRING     "A-Trk"
#define NONE_STRING     "None"

#define LeapYear(y) ((((y)%400)==0) || ((((y)%100)!=0) && (((y)%4)==0)))

static gboolean thisShowGAOMessage = True;
static guint thisAutotrackValues[MAX_GAO_VALUES] = { 0, 0, 0 };
static guint thisOpenloopValues[MAX_GAO_VALUES] = { 0, 0, 0 };

/* define holding place for values that aren't defined */
/* in the iam.xml file */
static guint thisPPLID = 0;
static guint thisPPLID_Type = -1; /* OPENLOOP_MASK or AUTOTRACK_MASK */

typedef struct ValidateSymbol
{
    gdouble  value;
    gboolean valid;
} ValidateSymbol;

/* recalculate joint angle vectors when this threshold is meet */
static gdouble thisJointAngleThreshold = 0.0;
static gdouble thisAngleValue[IS_ANGLE_MAX_SYMBOLS];

static ValidateSymbol thisGAO = {0.0, No};
static ValidateSymbol thisAntennaMode = {0.0, No};
/*************************************************************************/

/**
 * Initializes internal datastructs
 */
void IH_Constructor(void)
{
    gint i;

    /* load the max/min sband/kuband temperature limits */
    load_temp_limits();

    /* load the joint angle threshold value */
    load_joint_angle_threshold();

    /* initialize the angle values; use inconjunction with threshold */
    for (i=0; i<IS_ANGLE_MAX_SYMBOLS; i++)
    {
        thisAngleValue[i] = 0.0;
    }

    /* load the gao autotrack and openloop values */
    thisShowGAOMessage = True;
    load_gao();
}

/**
 * Releases any allocated memory
 */
void IH_Destructor(void)
{
}

/**
 * Loads or reloads the temperature limits
 */
void IH_LoadTempLimits(void)
{
    load_temp_limits();
}

/**
 * Updates the gmt/year. Also updates the tdrs predicts.
 */
void IH_UpdateTime (void)
{
    gchar time_utc[TIME_STR_LEN];
    gchar time_gmt[TIME_STR_LEN];
    gchar time_str[TIME_STR_LEN];

    SymbolRec *symbol_gmt = IS_GetSymbol(IS_Gmt);
    SymbolRec *symbol_year = IS_GetSymbol(IS_GmtYear);

    if (symbol_gmt != NULL && symbol_year != NULL)
    {
        if (symbol_gmt->status_update)
        {
            if (symbol_gmt->status.valid && symbol_year->status.valid)
            {
		gmt_valid = True;
                SetGmt((gint)(symbol_gmt->value/1000.0));
                g_stpcpy(time_str, CurrentGmt());
                g_sprintf(time_gmt, "%04d ", ((gint)symbol_year->value));
                strcat(time_gmt, time_str);
                SetCurrentGmt(time_gmt);

                g_stpcpy(time_utc, time_gmt);
                GmtToUtc(time_utc);
                SetCurrentUtc(time_utc);

                g_stpcpy(symbol_gmt->value_str, time_gmt);
                symbol_gmt->value_str_len = strlen(time_gmt);
                symbol_gmt->value_color = set_value_color(symbol_gmt);

                update_sat_id();

                RTRFD_SetTdrsPredicts(TD_TDRE, symbol_gmt->value_color);
                RTRFD_SetTdrsPredicts(TD_TDRW, symbol_gmt->value_color);
            }
            else
            {
                if (IS_MISSING(symbol_gmt->status.stclass) ||
                     IS_DEAD(symbol_year->status.stclass))
                {
                    symbol_gmt->value_str[0] = 0;
                    symbol_gmt->value_str_len = 0;
                }
            }

            symbol_gmt->value_color = set_value_color(symbol_gmt);
            RTD_SetValue(symbol_gmt);
        }
    }
}

/**
 * Updates the rise/set quality value.
 */
void IH_UpdateRiseSet(void)
{
    SymbolRec *symbol;

    /* get symbol and check validity */
    if ((symbol = IS_GetSymbol(IS_RiseSetQuality)) != NULL)
    {
        /* process symbol */
        symbol->value_str[0] = 0;
        symbol->value_str_len = 0;

        if (symbol->status_update)
        {
            if (symbol->status.valid)
            {
                switch ((gint)symbol->value)
                {
                    case 0:
                        g_stpcpy(symbol->value_str, "Invalid");
                        break;
                    case 1:
                        g_stpcpy(symbol->value_str, "Valid");
                        break;
                    case 2:
                        g_stpcpy(symbol->value_str, "Degraded");
                        break;
                    default:
                        g_stpcpy(symbol->value_str, ERR_STRING);
                        break;
                }

                symbol->value_str_len = strlen(symbol->value_str);
            }
            else
            {
                if (IS_MISSING(symbol->status.stclass) == True ||
                    IS_DEAD(symbol->status.stclass) == True)
                {
                    symbol->value_str[0] = 0;
                    symbol->value_str_len = 0;
                }
            }

            symbol->value_color = set_value_color(symbol);
            RTDH_SetValue(symbol);
        }
    }
}

/**
 * Updates the sband data values.
 */
void IH_UpdateSBand()
{
    gint i;
    gchar *sptr;
    gchar str[32];
    SymbolRec *symbol;

    for (i=IS_SBAND_FIRST_SYMBOL; i<IS_SBAND_LAST_SYMBOL; i++)
    {
        /* get symbol and check validity */
        if ((symbol = IS_GetSymbol(i)) == NULL) continue;

        // process this symbol
        symbol->value_str[0] = 0;
        symbol->value_str_len = 0;

        if (symbol->status_update)
        {
            if (symbol->status.valid)
            {
                switch (i)
                {
                    case IS_Sb1AZTemp:
                    case IS_Sb1ELTemp:
                    case IS_Sb1RFGTemp:
                    case IS_Sb1TransTemp:
                    case IS_Sb1BSPTemp:
                    case IS_Sb2AZTemp:
                    case IS_Sb2ELTemp:
                    case IS_Sb2RFGTemp:
                    case IS_Sb2TransTemp:
                    case IS_Sb2BSPTemp:
                        if (g_sprintf(symbol->value_str, "%6.1f", symbol->value) > 6)
                        {
                            g_stpcpy(symbol->value_str, ERR_STRING);
                        }
                        symbol->value_str_len = strlen(symbol->value_str);
                        break;

                    case IS_Sb1AGC:
                    case IS_Sb2AGC:
                        {
                            /* valid range can be -32768 to 32767 */
                            gint value = (gint)symbol->value;
                            if (value <= SHRT_MIN || value >= SHRT_MAX)
                            {
                                g_stpcpy(str, ERR_STRING);
                            }
                            else
                            {
                                g_sprintf(str, "%6d", (gint)value);
                            }
                            g_stpcpy(symbol->value_str, str);
                            symbol->value_str_len = strlen(str);
                        }
                        break;

                    case IS_Sb1CmdAz:
                    case IS_Sb1CmdEl:
                    case IS_Sb2CmdAz:
                    case IS_Sb2CmdEl:
                        if (g_sprintf(str, "%6.2f", symbol->value) > 8)
                        {
                            g_stpcpy(str, ERR_STRING);
                        }
                        g_stpcpy(symbol->value_str, str);
                        symbol->value_str_len = strlen(str);
                        break;

                    case IS_Sb1FrameLk:
                    case IS_Sb1CarrLk:
                    case IS_Sb1BitDet:
                    case IS_Sb2FrameLk:
                    case IS_Sb2CarrLk:
                    case IS_Sb2BitDet:
                        switch ((gint)symbol->value)
                        {
                            case 0:   sptr = DASH_STRING;  break;
                            case 1:   sptr = LOCK_STRING;  break;
                            default:  sptr = DASH_STRING;  break;
                        }
                        g_stpcpy(symbol->value_str, sptr);
                        symbol->value_str_len = strlen(sptr);
                        break;

                    case IS_Sb1Handover:
                    case IS_Sb2Handover:
                        switch ((gint) symbol->value)
                        {
                            case 1:   sptr = EARLY_STRING; break;
                            case 2:   sptr = LATE_STRING;  break;
                            default:  sptr = ERR_STRING;   break;
                        }
                        g_stpcpy(symbol->value_str, sptr);
                        symbol->value_str_len = strlen(sptr);
                        break;

                    case IS_Sb1Tracking:
                    case IS_Sb2Tracking:
                        switch ((gint) symbol->value)
                        {
                            case 1:   sptr = STOP_STRING;   break;
                            case 2:   sptr = MANUAL_STRING; break;
                            case 3:   sptr = AUTO_STRING;   break;
                            default:  sptr = ERR_STRING;    break;
                        }
                        g_stpcpy(symbol->value_str, sptr);
                        symbol->value_str_len = strlen(sptr);
                        break;

                    case IS_Sb1SatSel:
                    case IS_Sb2SatSel:
                        switch ((gint) symbol->value)
                        {
                            case 1:   sptr = MANUAL_STRING; break;
                            case 2:   sptr = AUTO_STRING;   break;
                            default:  sptr = ERR_STRING;    break;
                        }
                        g_stpcpy(symbol->value_str, sptr);
                        symbol->value_str_len = strlen(sptr);
                        break;

                    case IS_Sb1MUXCh1:
                    case IS_Sb1MUXCh2:
                    case IS_Sb2MUXCh1:
                    case IS_Sb2MUXCh2:
                        switch ((gint) symbol->value)
                        {
                            default:
                            case 0:   sptr = INACTIVE_STRING; break;
                            case 1:   sptr = ACTIVE_STRING;   break;
                        }
                        g_stpcpy(symbol->value_str, sptr);
                        symbol->value_str_len = strlen(sptr);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                /* if status is missing or dead, then clear string */
                if (IS_MISSING(symbol->status.stclass) == True ||
                    IS_DEAD(symbol->status.stclass) == True)
                {
                    symbol->value_str[0] = 0;
                }
            }

            /* set the color for the value string */
            symbol->value_color = set_value_color(symbol);

            RTDH_SetValue(symbol);
        }
        symbol->status_update = False;
    }
}

/**
 * Processes the kuband data values.
 */
void IH_UpdateKuBand(void)
{
    gint i;
    SymbolRec *symbol;

    for (i=IS_KUBAND_FIRST_SYMBOL; i<IS_KUBAND_LAST_SYMBOL; i++)
    {
        /* get symbol and check validity */
        if ((symbol = IS_GetSymbol(i)) == NULL) continue;

        /* process this symbol */
        symbol->value_str[0] = 0;
        symbol->value_str_len = 0;

        /* is symbol ready for update? */
        if (symbol->status_update)
        {
            /* is status valid? */
            if (symbol->status.valid)
            {
                switch (i)
                {
                    case IS_KuPWRL:
                        setKuPWRL(symbol);
                        break;

                    case IS_KuFwdPwrVolts:
                    case IS_KuReflPwrVolts:
                    case IS_KuIFOutVolts:
                    case IS_KuIFInVolts:
                        setKuVolts(symbol);
                        break;

                    case IS_KuSG1Temp:
                    case IS_KuSG2Temp:
                    case IS_KuSG3Temp:
                    case IS_KuSG4Temp:
                    case IS_KuSG5Temp:
                    case IS_KuSG6Temp:
                    case IS_KuSG7Temp:
                    case IS_KuSG8Temp:
                    case IS_KuTRCS1Temp:
                    case IS_KuTRCS2Temp:
                    case IS_KuTRCS3Temp:
                    case IS_KuTRCPwrAmpTemp:
                    case IS_KuTRCPwrSupTemp:
                    case IS_KuHRMTemp:
                    case IS_KuHRFMTemp:
                    case IS_KuVBSPTemp:
                        setKuTemp(symbol);
                        break;

                    case IS_KuTRC1:
		    case IS_KuTRC2:
		        setKuTRC(symbol);
			break;

                    case IS_KuActualXEL:
                    case IS_KuActualEL:
                        setKuXel(symbol);
                        break;

                    case IS_KuPwrAmpAct:
                    case IS_KuPwrAmpPend:
                        setKuAmp(symbol);
                        break;

                    case IS_KuPLCAct:
                        setKuPLCAct(symbol);
                        break;

                    case IS_KuPLCPend:
                        setKuPLCPend(symbol);
                        break;

                    case IS_KuPtgAct:
                        setKuPtgAct(symbol);
                        /* fall through */
                    case IS_KuPtgPend:
                        setKuPtgPend(symbol);
                        break;

                    case IS_KuTDRSSelect:
                        setKuTDRSSelect(symbol);
                        break;

                    case IS_KuAutotrack:
                        setKuAutotrack(symbol);
                        break;

                    case IS_KuHandover:
                        setKuHandover(symbol);
                        break;

                    case IS_KuRetry:
                        setKuRetry(symbol);
                        break;

                    case IS_KuGAO:
                        setKuGAO(symbol);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                /* if status is missing or dead, then clear string */
                if (IS_MISSING(symbol->status.stclass) == True ||
                    IS_DEAD(symbol->status.stclass) == True)
                {
                    symbol->value_str[0] = 0;
                }
            }

            /* set the value color */
            symbol->value_color = set_value_color(symbol);

            /* update the display */
            RTDH_SetValue(symbol);
        }
        symbol->status_update = False;
    }
}

/**
 * Updates the Ku Power symbol
 *
 * @param symbol
 */
static void setKuPWRL(SymbolRec *symbol)
{
    if (g_sprintf(symbol->value_str, "%6.2f", symbol->value) > 6)
    {
        g_stpcpy(symbol->value_str, ERR_STRING);
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku volt symbols
 *
 * @param symbol
 */
static void setKuVolts(SymbolRec *symbol)
{
    if (g_sprintf(symbol->value_str, "%5.2f", symbol->value) > 6)
    {
        g_stpcpy(symbol->value_str, ERR_STRING);
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku temp symbols
 *
 * @param symbol
 */
static void setKuTemp(SymbolRec *symbol)
{
    if (g_sprintf(symbol->value_str, "%6.1f", symbol->value) > 6)
    {
        g_stpcpy(symbol->value_str, ERR_STRING);
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku TRC symbols
 *
 * @param symbol
 */
static void setKuTRC(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, OFF_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, ON_STRING);  break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku Xel and El symbols
 *
 * @param symbol
 */
static void setKuXel(SymbolRec *symbol)
{
    if (g_sprintf(symbol->value_str, "%6.2f", symbol->value) > 8)
    {
        g_stpcpy(symbol->value_str, ERR_STRING);
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku amp symbols
 *
 * @param symbol
 */
static void setKuAmp(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, OFF_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, ON_STRING);  break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku PLC actual
 *
 * @param symbol
 */
static void setKuPLCAct(SymbolRec *symbol)
{
    SymbolRec symbol_xmit;
    memcpy(&symbol_xmit, symbol, sizeof(SymbolRec));

    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, RESET_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, NORMAL);       break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING);   break;
    }
    symbol->value_str_len = strlen(symbol->value_str);

    if (g_ascii_strncasecmp(symbol->value_str, NORMAL, 6) == 0)
    {
        g_stpcpy(symbol_xmit.value_str, KU_XMIT_STR);
        symbol_xmit.value_str_len = strlen(KU_XMIT_STR);
        symbol_xmit.value_color = CH_GetColor("White");

        RTD_SetKuXmitColor(GC_BLUE);
        RTD_SetKuXmitStatus(&symbol_xmit);
        RTD_SetKuXmitValue(&symbol_xmit);
    }
    else
    {
        symbol_xmit.value_str[0] = 0;
        symbol_xmit.value_str_len = 0;
        symbol_xmit.value_color = CH_GetColor(GOOD_VALUE);

        RTD_SetKuXmitColor(GC_BG);
        RTD_SetKuXmitStatus(&symbol_xmit);
        RTD_SetKuXmitValue(&symbol_xmit);
    }
}

/**
 * Updates the Ku PLC pending
 *
 * @param symbol
 */
static void setKuPLCPend(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, RESET_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, NORMAL); break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates Ku Pointing Actual
 *
 * @param symbol
 */
static void setKuPtgAct(SymbolRec *symbol)
{
    MF_SetAntennaMode((gint)symbol->value);
    thisAntennaMode.value = symbol->value;
    thisAntennaMode.valid = True;
}

/**
 * Updates the Ku Pointing Pending
 *
 * @param symbol
 */
static void setKuPtgPend(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, INHIBIT_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, OPLOOP_STRING); break;
        case 2:   g_stpcpy(symbol->value_str, ATRK_STRING); break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku TDRS selected mask
 *
 * @param symbol
 */
static void setKuTDRSSelect(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, NONE_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, "1"); break;
        case 2:   g_stpcpy(symbol->value_str, "2"); break;
        case 3:   g_stpcpy(symbol->value_str, "3"); break;
        case 4:   g_stpcpy(symbol->value_str, "4"); break;
        case 5:   g_stpcpy(symbol->value_str, "5"); break;
        case 6:   g_stpcpy(symbol->value_str, "6"); break;
        case 7:   g_stpcpy(symbol->value_str, "7"); break;
        case 8:   g_stpcpy(symbol->value_str, "8"); break;
        case 9:   g_stpcpy(symbol->value_str, "9"); break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku auto track
 *
 * @param symbol
 */
static void setKuAutotrack(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, "Not Selected"); break;
        case 1:   g_stpcpy(symbol->value_str, "Open Loop Slew"); break;
        case 2:   g_stpcpy(symbol->value_str, "Acquisition 1"); break;
        case 3:   g_stpcpy(symbol->value_str, "Acquisition 2"); break;
        case 4:   g_stpcpy(symbol->value_str, "Acquisition 3"); break;
        case 5:   g_stpcpy(symbol->value_str, "No Target"); break;
        case 6:   g_stpcpy(symbol->value_str, "Acquisition Lock"); break;
        case 7:   g_stpcpy(symbol->value_str, "Acquisition Fail"); break;
        case 8:   g_stpcpy(symbol->value_str, "Signal Lost");break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}


/**
 * Sets the Ku Handover
 *
 * @param symbol
 */
static void setKuHandover(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        default:
        case 0:   g_stpcpy(symbol->value_str, ERR_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, EARLY_STRING); break;
        case 2:   g_stpcpy(symbol->value_str, LATE_STRING); break;
        case 3:   g_stpcpy(symbol->value_str, MANUAL_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
}

/**
 * Updates the Ku Retry
 *
 * @param symbol
 */
static void setKuRetry(SymbolRec *symbol)
{
    switch ((gint) symbol->value)
    {
        case 0:   g_stpcpy(symbol->value_str, INHIBIT_STRING); break;
        case 1:   g_stpcpy(symbol->value_str, ENABLED_STRING); break;
        default:  g_stpcpy(symbol->value_str, ERR_STRING); break;
    }
    symbol->value_str_len = strlen(symbol->value_str);
    MF_SetContRetry((gint)symbol->value);
}

/**
 * Updates the Ku GAO
 *
 * @param symbol
 */
static void setKuGAO(SymbolRec *symbol)
{
    g_message("IH_UpdateKuBand: gao=%f",symbol->value);
    g_sprintf(symbol->value_str, "%2.1f", get_gao_degree(symbol->value));
    symbol->value_str_len = strlen(symbol->value_str );

    /* save gao value for drawing the realtime kuband enabled masks */
    MF_SetGAOMask((gint)get_gao_degree(symbol->value));

    thisGAO.value = symbol->value;
    thisGAO.valid = True;
}

/**
 * This method implements the mis compare logic for the actual and
 * pending values related to the PLC, Pwr Amp, and Pointing mode.
 * This logic was converted from ispatom logic:
 * CT_KuTRCmisCmp1 = if (LADP01MDARPBJ == Z1CK04FC0005J, 0, 1)
 * CT_KuTRCmisCmp2 = if (LADP01MDARP3J == Z1CK04FC0009J, 0, 1)
 * CT_KuTRCmisCmp3 = if (LADP01MDARP1J == Z1CK04FC0010J, 0, 1)
 */
void IH_UpdateKuMisCmpr(void)
{
    SymbolRec *mis_cmpr_symbol;
    SymbolRec *a_symbol;
    SymbolRec *b_symbol;
    StStatusStruct *most_significant_status;
    gchar *sptr;
    guint i;

    /* define the ids, order is important */
    gint mis_cmpr_symbol_ids[] = {IS_KuPwrAmpMisCmpr, IS_KuPLCMisCmpr, IS_KuPtgMisCmpr};
    gint act_symbol_ids[]      = {IS_KuPwrAmpAct,     IS_KuPLCAct,     IS_KuPtgAct};
    gint pend_symbol_ids[]     = {IS_KuPwrAmpPend,    IS_KuPLCPend,    IS_KuPtgPend};

    /* do for plc, pwr amp, and ptg symbols */
    for (i=0; i<sizeof(mis_cmpr_symbol_ids)/sizeof(mis_cmpr_symbol_ids[0]); i++)
    {
        if ((mis_cmpr_symbol = IS_GetSymbol(mis_cmpr_symbol_ids[i])) == NULL) break;
        if ((a_symbol = IS_GetSymbol(act_symbol_ids[i])) == NULL) break;
        if ((b_symbol = IS_GetSymbol(pend_symbol_ids[i])) == NULL) break;

        /* get the status */
        most_significant_status = get_status(a_symbol, b_symbol, NULL);
        memcpy(&mis_cmpr_symbol->status, most_significant_status, sizeof(StStatusStruct));

        /* make sure both symbols are good before proceeding */
        if (a_symbol->status.valid && b_symbol->status.stclass)
        {
            /* set the mis_cmpr value */
            if ((gint)a_symbol->value == (gint)b_symbol->value)
            {
                mis_cmpr_symbol->value = 0.0;
            }
            else
            {
                mis_cmpr_symbol->value = 1.0;
            }

            /* format the mis_cmpr value */
            switch ((gint) mis_cmpr_symbol->value)
            {
                case 0:   sptr = " "; break;
                case 1:   sptr = "X"; break;
                default:  sptr = E_STRING; break;
            }
            g_stpcpy(mis_cmpr_symbol->value_str, sptr);
            mis_cmpr_symbol->value_str_len = strlen(sptr);
        }
        else
        {
            /* if status is missing or dead, then clear string */
            if (IS_MISSING(mis_cmpr_symbol->status.stclass) == True ||
                IS_DEAD(mis_cmpr_symbol->status.stclass) == True)
            {
                mis_cmpr_symbol->value_str[0] = 0;
            }
        }

        mis_cmpr_symbol->value_color = set_value_color(mis_cmpr_symbol);

        RTRFD_SetValue(mis_cmpr_symbol);
    }
}

/**
 * Updates the pathgo, sg1, sg2 for sband1 and sband2.
 */
void IH_UpdatePathGO(void)
{
    gint i;

    SymbolRec *tmp_symbol;

    gint is_index[2][3] =
    {
        {IS_Sb1CmdLnk, IS_Sb1RELPCh1, IS_Sb1RELPCh2},
        {IS_Sb2CmdLnk, IS_Sb2RELPCh1, IS_Sb2RELPCh2}
    };

    /* try and use the ispatom symbols */
    SymbolRec *cmdlnk  = IS_GetSymbol(IS_PathGO);
    SymbolRec *relpch1 = IS_GetSymbol(IS_SG1);
    SymbolRec *relpch2 = IS_GetSymbol(IS_SG2);

    for (i=0; i<2; i++)
    {
        if ((tmp_symbol = IS_GetSymbol(is_index[i][0])) != NULL)
        {
            tmp_symbol->value = 0.0;
            if (cmdlnk != NULL)
            {
                if (cmdlnk->status.valid)
                {
                    tmp_symbol->value = cmdlnk->value;
                    set_pathgo_value(tmp_symbol, GO_STR);
                }
            }
            memcpy(&tmp_symbol->status, &cmdlnk->status, sizeof(StStatusStruct));
            RTRFD_SetValue(tmp_symbol);
        }

        if ((tmp_symbol = IS_GetSymbol(is_index[i][1])) != NULL)
        {
            tmp_symbol->value = 0.0;
            if (tmp_symbol->status.valid)
            {
                if (relpch1 != NULL)
                {
                    tmp_symbol->value = relpch1->value;
                    set_pathgo_value(tmp_symbol, SG1_STR);
                }
            }
            memcpy(&tmp_symbol->status, &relpch1->status, sizeof(StStatusStruct));
            RTRFD_SetValue(tmp_symbol);
        }

        if ((tmp_symbol = IS_GetSymbol(is_index[i][2])) != NULL)
        {
            tmp_symbol->value = 0.0;
            if (tmp_symbol->status.valid)
            {
                if (relpch2 != NULL)
                {
                    tmp_symbol->value = relpch2->value;
                    set_pathgo_value(tmp_symbol, SG2_STR);
                }
            }
            memcpy(&tmp_symbol->status, &relpch2->status, sizeof(StStatusStruct));
            RTRFD_SetValue(tmp_symbol);
        }
    }
}

/**
 * Updates the kuband mask data values. These values are used
 * to create the realtime masks that are enabled via telemetry.
 */
void IH_UpdateKuMasks(void)
{
    gint i;
    gchar *sptr;
    gchar str[32];
    SymbolRec *symbol;

    for (i=IS_KU_PTG_MASK_FIRST_SYMBOL; i<IS_KU_PTG_MASK_LAST_SYMBOL; i++)
    {
        /* get symbol and check validity */
        if ((symbol = IS_GetSymbol(i)) == NULL) continue;

        // process symbol
        symbol->value_str[0] = 0;
        symbol->value_str_len = 0;

        if (symbol->status_update)
        {
            gint value = (gint)symbol->value;

            switch (value)
            {
                case 0:
                    sptr = " ";
                    break;

                case 1:
                    {
                        gint ptg_mask = i-IS_KU_PTG_MASK_FIRST_SYMBOL+1;
                        g_sprintf(str, "%d", ptg_mask);
                        sptr = str;
                    }
                    break;

                default:
                    sptr = E_STRING;
                    break;
            }

            g_stpcpy(symbol->value_str, sptr);
            symbol->value_str_len = strlen(sptr);
            symbol->value_color = set_value_color(symbol);

            /* set the active flag */
            update_kumask(symbol);

            /* set the value in the gui */
            RTDH_SetValue(symbol);

            symbol->status_update = False;
        }
        else
        {
            /* set the active flag */
            update_kumask(symbol);
        }
    }

    for (i=IS_KU_MASK_FIRST_SYMBOL; i<IS_KU_MASK_LAST_SYMBOL; i++)
    {
        /* get the symbol and check validity */
        if ((symbol = IS_GetSymbol(i)) == NULL) break;

        /* process the symbol */
        symbol->value_str[0] = 0;
        symbol->value_str_len = 0;

        if (symbol->status_update)
        {
            /* normalize the value */
            /* when using ceil and the value is negative, the rounded */
            /* value is rounded to zero. also we want to take off */
            /* some of the decimal places so positive numbers are not */
            /* rounded to zero, but up. something like that! */
            gboolean isneg = False;

            /* keep only 2 decimal signficance */
            gint ival = (gint)(symbol->value*100.0);
            gdouble dval = (gdouble)((gdouble)ival/100.0);

            /* if the value is negative, invert the sign */
            if (dval < 0.0)
            {
                isneg = True;
                dval = -dval;
            }

            /* now round up */
            dval = ceil(dval*10.0);

            /* put sign back */
            if (isneg == True)
            {
                dval = -dval;
            }

            /* give normalize value */
            MF_SetKuBandMaskValue(REALTIME, i-IS_KU_MASK_FIRST_SYMBOL, (gint)dval);
            MF_SetKuBandMaskValue(PREDICT, i-IS_KU_MASK_FIRST_SYMBOL, (gint)dval);
        }
        symbol->status_update = False;
    }

}

/**
 * Updates the tdrs select for tdrs east and west.
 */
void IH_UpdateTDRSSel(void)
{
    gint value;
    SymbolRec *symbol;

    /* get symbol and check validity */
    if ((symbol = IS_GetSymbol(IS_TdrsSelectFlag)) != NULL)
    {
        if (symbol->status_update)
        {
            if (symbol->status.valid)
            {
                gint tdrs_index = (gint)symbol->value;

                /* make sure value is valid */
                if (tdrs_index >= 0 && tdrs_index < 10)
                {
                    TdrsInfo *tdrsInfo = PDH_GetTdrsInfoById(tdrs_index);
                    if (tdrsInfo)
                    {
                        g_stpcpy(symbol->value_str, tdrsInfo->eventName);
                        symbol->value_str_len = strlen(symbol->value_str);
                    }
                }
                else
                {
                    /* value is bad */
                    g_stpcpy(symbol->value_str, ERR_STRING);
                    symbol->value_str_len = strlen(symbol->value_str);
                }
            }
            else
            {
                if (IS_MISSING(symbol->status.stclass) == True ||
                    IS_DEAD(symbol->status.stclass) == True)
                {
                    symbol->value_str[0] = 0;
                    symbol->value_str_len = 0;
                }
            }
            symbol->value_color = set_value_color(symbol);

            RTDH_SetValue(symbol);

            value = (gint)symbol->value;
            if ((value == 1)  || (value == 2)  || (value == 3))
            {
                thisSelected[TD_TDRE] = Selected;
                thisSelected[TD_TDRW] = NotSelected;
            }
            else
            {
                thisSelected[TD_TDRE] = NotSelected;
                thisSelected[TD_TDRW] = Selected;
            }
        }
    }
}

/**
 * Updates the tdrs and sun azel values.
 */
void IH_UpdateTDRS(void)
{
    update_inview();

    /* since the realtime dialog and the realtime data dialog */
    /* can have different views up at the same time, the tdrs */
    /* calculations have to be done separately */

    /* compute tdrs location based on the realtime */
    /* main window's selected coordinate */
    IH_UpdateTDRSValues(RTD_GetCoordinateType(), True, False);

    /* now compute tdrs location based on the realtime */
    /* data dialog's selected coordinate */
    IH_UpdateTDRSValues(RTRFD_GetCoordinateType(), False, True);
}

/**
 * Updates the tdrs and sun azel values. Usally the add_point flag is
 * set on while the update_tdrs_value is off and vice-versa.
 *
 * @param coord process data for this coordinate type
 * @param add_point flag indicating if point should be added
 * @param update_tdrs_value flag indicating if the gui for the tdrs should be
 * updated.
 */
void IH_UpdateTDRSValues(gint coord, gboolean add_point, gboolean update_tdrs_value)
{
    gint i;
    gint az, el;
    StStatusStruct *most_significant_status;

    const struct SYMBOL_XYZ
    {
        gint x;
        gint y;
        gint z;
    } symbol_xyz[] =
    {
        {IS_TdrwCoordsX, IS_TdrwCoordsY, IS_TdrwCoordsZ},
        {IS_TdreCoordsX, IS_TdreCoordsY, IS_TdreCoordsZ},
        {IS_SunCoordsX,  IS_SunCoordsY,  IS_SunCoordsZ}
    };

    const struct SAE
    {
        gint satid;
        gint azimuth;
        gint elevation;
    } sae[] =
    {
        {TD_TDRW, TDRW_AZIMUTH, TDRW_ELEVATION},
        {TD_TDRE, TDRE_AZIMUTH, TDRE_ELEVATION},
        {TD_SUN,  SUN_AZIMUTH,  SUN_ELEVATION},
    };

    SymbolRec symbol_azel[9];
    SymbolRec *symbol_x;
    SymbolRec *symbol_y;
    SymbolRec *symbol_z;

    for (i=0; i<ELEMENTS(sae); i++)
    {
        if ((symbol_x = IS_GetSymbol(symbol_xyz[i].x)) == NULL) break;
        if ((symbol_y = IS_GetSymbol(symbol_xyz[i].y)) == NULL) break;
        if ((symbol_z = IS_GetSymbol(symbol_xyz[i].z)) == NULL) break;

        /* zero out the output buffer */
        symbol_azel[sae[i].azimuth].symbol_id = -1;
        symbol_azel[sae[i].azimuth].value = 0.0;
        g_sprintf(symbol_azel[sae[i].azimuth].value_str, "%6.1f",
                symbol_azel[sae[i].azimuth].value);
        symbol_azel[sae[i].azimuth].value_str_len = 0;

        symbol_azel[sae[i].elevation].symbol_id = -1;
        symbol_azel[sae[i].elevation].value = 0.0;
        g_sprintf(symbol_azel[sae[i].elevation].value_str, "%6.1f",
                symbol_azel[sae[i].elevation].value);
        symbol_azel[sae[i].elevation].value_str_len = 0;

        if (status_or_value_changed(symbol_x, symbol_y, symbol_z) == True)
        {
            most_significant_status = get_status(symbol_x, symbol_y, symbol_z);

            memcpy(&symbol_azel[sae[i].azimuth].status, most_significant_status, sizeof(StStatusStruct));
            symbol_azel[sae[i].azimuth].value_color = set_value_color(&symbol_azel[sae[i].azimuth]);

            memcpy(&symbol_azel[sae[i].elevation].status, most_significant_status, sizeof(StStatusStruct));
            symbol_azel[sae[i].elevation].value_color = set_value_color(&symbol_azel[sae[i].elevation]);

            if (MOST_SIGNIFICANT_STATUS_GOOD(most_significant_status->stclass))
            {
                switch (coord)
                {
                    default:
                    case SBAND1:
                    case SBAND2:
                        XYZtoSBandAE(symbol_x->value, symbol_y->value, symbol_z->value, &az, &el);
                        break;

                    case KUBAND:
                    case KUBAND2:
                        XYZtoKuBand(symbol_x->value, symbol_y->value, symbol_z->value, &az, &el, coord);
                        break;

                    case STATION:
                        XYZtoGenericAE(symbol_x->value, symbol_y->value, symbol_z->value, &az, &el);
                        break;
                }

                symbol_azel[sae[i].azimuth].symbol_id = -1;
                symbol_azel[sae[i].azimuth].value = (az/10.0);

                g_sprintf(symbol_azel[sae[i].azimuth].value_str, "%6.1f",
                        symbol_azel[sae[i].azimuth].value);
                symbol_azel[sae[i].azimuth].value_str_len = strlen(symbol_azel[sae[i].azimuth].value_str);
                symbol_azel[sae[i].azimuth].value_color = set_value_color(&symbol_azel[sae[i].azimuth]);

                symbol_azel[sae[i].elevation].symbol_id = -1;
                symbol_azel[sae[i].elevation].value = (el/10.0);

                g_sprintf(symbol_azel[sae[i].elevation].value_str, "%6.1f",
                        symbol_azel[sae[i].elevation].value);
                symbol_azel[sae[i].elevation].value_str_len = strlen(symbol_azel[sae[i].elevation].value_str);
                symbol_azel[sae[i].elevation].value_color = set_value_color(&symbol_azel[sae[i].elevation]);

                if (add_point == True)
                {
                    RTDH_AddPoint(sae[i].satid,
                                  symbol_x->value,
                                  symbol_y->value,
                                  symbol_z->value,
                                  comp_tdrs_status(sae[i].satid));
                }
            }

            if (update_tdrs_value == True)
            {
                RTRFD_SetTdrsValue(sae[i].satid, AZ_TYPE,
                                   &symbol_azel[sae[i].azimuth]);
                RTRFD_SetTdrsValue(sae[i].satid, EL_TYPE,
                                   &symbol_azel[sae[i].elevation]);
            }
        }
    }
}

/**
 * Updates the joint angle data. If any angle is changed, then
 * the draw method is called.
 */
void IH_UpdateAngle(void)
{
    gint i;
    gboolean redraw = False;
    SymbolRec *symbol = NULL;

    /* index into the angle values buffer */
    gint angle_idx = 0;

    for (i=IS_ANGLE_FIRST_SYMBOL; i<IS_ANGLE_LAST_SYMBOL; i++)
    {
        /* get the symbol and check for validity */
        if ((symbol = IS_GetSymbol(i)) == NULL) continue;

        /* press on, process this symbol */
        if (symbol->status_update)
        {
            if (symbol->status.valid)
            {
                gchar str[32];

                if (g_sprintf(str, "%7.2f", symbol->value) > 7)
                {
                    g_stpcpy(str, ERR_STRING);
                }
                g_stpcpy(symbol->value_str, str);
                symbol->value_str_len = strlen(str);
            }
            else
            {
                if (IS_MISSING(symbol->status.stclass) == True ||
                    IS_DEAD(symbol->status.stclass) == True)
                {
                    symbol->value_str[0] = 0;
                    symbol->value_str_len = 0;
                }
            }

            /* now check to see if the threshold value has been meet */
            /* if any angle has meet the threshold, then force redraw */
            if (check_threshold(symbol->value, angle_idx) == True)
            {
                redraw = True;
            }

            /* get the color for the value */
            symbol->value_color = set_value_color(symbol);
            RTDH_SetValue(symbol);
        }
        symbol->status_update = False;
        angle_idx++;
    }

    /* redraw only if something changed. */
    if (redraw == True)
    {
        RTD_SetRedraw(True);
    }
}

/**
 * Updates the internal comp for AutoUpdating.
 */
void IH_AutoUpdate()
{
    SymbolRec *symbol;
    if ((symbol = IS_GetSymbol(IS_AntmanAuto)) != NULL)
    {
        if (symbol->status_update)
        {
            g_message("IH_AutoUpdate: call AM_SetAutoUpdateMode()");
            AM_SetAutoUpdateMode(symbol->value_str);
        }
        symbol->status_update = False;
    }
}

/**
 * Converts the gao ppl id to a degree value. The values are
 * checked against iam.xml values that user provided. These
 * values can be changed via the realtime-settings menu.
 *
 * @param value ppl id value
 *
 * @return degree value
 */
gdouble get_gao_degree(gdouble value)
{
    guint i;
    gboolean found = False;
    gdouble gao = 0.0;
    guint ppl_id = (guint)value;

    /* find autotrack or openloop defined in the iam.xml file */
    for (i=0; i<MAX_GAO_VALUES; i++)
    {
        if (thisAutotrackValues[i] == ppl_id)
        {
            gao = 1.0;
            found = True;
            break;
        }

        if (thisOpenloopValues[i] == ppl_id)
        {
            gao = 7.0;
            found = True;
            break;
        }
    }

    /* see if the session ppl_id matches */
    if (thisPPLID == ppl_id)
    {
        found = True;
        gao = (thisPPLID_Type == AUTOTRACK_MASK ? 1.0 : 7.0);
    }

    /* if not found and the message hasn't been shown */
    /* then ask user for the ppl id type */
    if (found == False && thisShowGAOMessage == True)
    {
        GtkWidget *dialog;
        GtkWidget *label;
        gchar message[256];

        /* save the new ppl id */
        thisPPLID = ppl_id;

        g_sprintf(message,
                "Prim_CCS_Gimbal_Ang_Off_PPL_24_Ver_ID indicates '%u'.\n"
                "This value does not match predefined values found in iam.xml.\n\n"
                "Indicate one of the following for this value of the PPL.",
                ppl_id);

        dialog = gtk_dialog_new_with_buttons("Invalid PPL ID", GTK_WINDOW(AM_GetActiveDisplay()),
                                             (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                             "Open Loop",  OPENLOOP_MASK,
                                             "Auto Track", AUTOTRACK_MASK,
                                             NULL);
        label = gtk_label_new(message);
        gtk_widget_show(label);


        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), label);

        thisPPLID_Type = gtk_dialog_run(GTK_DIALOG(dialog));
        gao = (thisPPLID_Type == AUTOTRACK_MASK ? 1.0 : 7.0);
        gtk_widget_destroy(dialog);

        /* only show the message once */
        /* this was the requirement from CATO */
        thisShowGAOMessage = False;
    }

    return gao;
}

/**
 * Sets all symbol's status to dead, then calls the
 * update methods.
 */
void IH_SetDeadStatus(void)
{
    gint i;

    /* set symbol status to dead */
    for (i=0; i<IS_GetSymbolCount(); i++)
    {
        SymbolRec *symbol;
        if ((symbol = IS_GetSymbol(i)) != NULL)
        {
            symbol->status.stclass = STDEAD;
            symbol->status.indicator = 'D';
            symbol->status.color = GC_RED;
            symbol->status_update = True;
        }
    }

    /* update all data fields */
    IH_UpdateTime();
    IH_UpdateRiseSet();
    IH_UpdateSBand();
    IH_UpdateKuBand();
    IH_UpdateKuMasks();
    IH_UpdateTDRSSel();
    IH_UpdateTDRS();
    IH_UpdateAngle();
}

/**
 * Updates the inview value for the tdrs
 */
static void update_inview(void)
{
    SymbolRec *symbol;

    if ((symbol = IS_GetSymbol(IS_RiseSetFlag3)) != NULL)
    {
        if (symbol->status.valid)
        {
            thisInView[TD_TDRE] = (gint)symbol->value;
        }
        else
        {
            thisInView[TD_TDRE] = -1;
        }
    }

    if ((symbol = IS_GetSymbol(IS_RiseSetFlag2)) != NULL)
    {
        if (symbol->status.valid)
        {
            thisInView[TD_TDRW] = (gint)symbol->value;
        }
        else
        {
            thisInView[TD_TDRW] = -1;
        }
    }
}

/**
 * Updates the satellite id for east and west
 */
static void update_sat_id(void)
{
    SymbolRec *symbol;

    static gint tdre = -1;
    static gint tdrw = -1;
    static gchar tdre_status = ' ';
    static gchar tdrw_status = ' ';

    if ((symbol = IS_GetSymbol(IS_TdreSatId)) != NULL)
    {
        if ((gint)symbol->value != tdre ||
            symbol->status.indicator != tdre_status)
        {
            tdre = (gint)symbol->value;
            tdre_status = symbol->status.indicator;

            g_sprintf (symbol->value_str, "%2d", tdre);
            symbol->value_str_len = strlen(symbol->value_str);
            symbol->value_color = set_value_color(symbol);

            RTRFD_SetTdrsId(TD_TDRE, symbol);
            RTDH_SetTdrsLabel(TD_TDRE, g_strstrip(symbol->value_str));
        }
    }

    if ((symbol = IS_GetSymbol(IS_TdrwSatId)) != NULL)
    {
        if ((gint)symbol->value != tdrw ||
            symbol->status.indicator != tdrw_status)
        {
            tdrw = (gint)symbol->value;
            tdrw_status = symbol->status.indicator;

            g_sprintf (symbol->value_str, "%2d", tdrw);
            symbol->value_str_len = strlen(symbol->value_str);
            symbol->value_color = set_value_color(symbol);

            RTRFD_SetTdrsId(TD_TDRW, symbol);
            RTDH_SetTdrsLabel(TD_TDRW, g_strstrip(symbol->value_str));
        }
    }
}

/**
 * Determines which status to use.
 *
 * @param x first symbol
 * @param y second symbol
 * @param z third symbol
 *
 * @return status of either x, y, or z, defaults to x
 */
static StStatusStruct *get_status(SymbolRec *x, SymbolRec *y, SymbolRec *z)
{
    StStatusStruct *stat = NULL;
    gchar cur_stat = STDEFAULT;

    if (x != NULL)
    {
        if (x->status.stclass < cur_stat)
        {
            stat = &x->status;
            cur_stat = x->status.stclass;
        }
    }

    if (y != NULL)
    {
        if (y->status.stclass < cur_stat)
        {
            stat = &y->status;
            cur_stat = y->status.stclass;
        }
    }

    if (z != NULL)
    {
        if (z->status.stclass < cur_stat)
        {
            stat = &z->status;
            cur_stat = z->status.stclass;
        }
    }

    /* set stat to something */
    if (stat == NULL)
    {
        stat = &x->status;
    }

    if (cur_stat == STULIMIT || cur_stat == STLLIMIT)
    {
        stat->stclass = STDEFAULT;
    }

    return stat;
}

/**
 * Determines if the status changed for the symbols.
 *
 * @param x first symbol
 * @param y second symbol
 * @param z third symbol
 *
 * @return True or False
 */
static gboolean status_or_value_changed(SymbolRec *x, SymbolRec *y, SymbolRec *z)
{
    if (x)
    {
        if (x->status_update == True)
        {
            return True;
        }
    }

    if (y)
    {
        if (y->status_update == True)
        {
            return True;
        }
    }

    if (z)
    {
        if (z->status_update == True)
        {
        return True;
    }
    }

    return False;
}

/**
 * Performs the inview/selected logic for the given tdrs.
 *
 * @param tdrs tdrs type
 *
 * @return inview state
 */
static gint comp_tdrs_status(gint tdrs)
{
    switch (tdrs)
    {
        case TD_SUN:
            return RTD_Selected;
            break;

        case TD_TDRW:
        case TD_TDRE:
            if (thisInView[tdrs])
            {
                if (thisSelected[tdrs])
                {
                    thisInView[tdrs] = RTD_Selected;
                }
                else
                {
                    thisInView[tdrs] = RTD_InViewNotSelected;
                }
            }
            else
            {
                thisInView[tdrs] = RTD_NotInView;
            }
            break;

        default:
            break;
    }

    return thisInView[tdrs];
}

/**
 * Based on the symbol id, a cooresponding temp range index is returned.
 * This index is used for accessing the temperature array.
 *
 * @param symbol_id check this symbol's temperature value
 *
 * @return temperature index
 */
static gint get_temp_range_index(gint symbol_id)
{
    gint temp_index = -1;

    switch (symbol_id)
    {
        case IS_Sb1AZTemp:
            temp_index = SB1_AZ_RANGE;
            break;

        case IS_Sb2AZTemp:
            temp_index = SB2_AZ_RANGE;
            break;

        case IS_Sb1ELTemp:
            temp_index = SB1_EL_RANGE;
            break;

        case IS_Sb2ELTemp:
            temp_index = SB2_EL_RANGE;
            break;

        case IS_Sb1RFGTemp:
            temp_index = SB1_RFG_RANGE;
            break;

        case IS_Sb2RFGTemp:
            temp_index = SB2_RFG_RANGE;
            break;

        case IS_Sb1TransTemp:
            temp_index = SB1_TRANS_RANGE;
            break;

        case IS_Sb2TransTemp:
            temp_index = SB2_TRANS_RANGE;
            break;

        case IS_Sb1BSPTemp:
            temp_index = SB1_BSP_RANGE;
            break;

        case IS_Sb2BSPTemp:
            temp_index = SB2_BSP_RANGE;
            break;

        case IS_KuSG1Temp:
            temp_index = KU_SG1_RANGE;
            break;

        case IS_KuSG2Temp:
            temp_index = KU_SG2_RANGE;
            break;

        case IS_KuSG3Temp:
            temp_index = KU_SG3_RANGE;
            break;

        case IS_KuSG4Temp:
            temp_index = KU_SG4_RANGE;
            break;

        case IS_KuSG5Temp:
            temp_index = KU_SG5_RANGE;
            break;

        case IS_KuSG6Temp:
            temp_index = KU_SG6_RANGE;
            break;

        case IS_KuSG7Temp:
            temp_index = KU_SG7_RANGE;
            break;

        case IS_KuSG8Temp:
            temp_index = KU_SG8_RANGE;
            break;

        case IS_KuTRCS1Temp:
            temp_index = KU_TRCS1_RANGE;
            break;

        case IS_KuTRCS2Temp:
            temp_index = KU_TRCS2_RANGE;
            break;

        case IS_KuTRCS3Temp:
            temp_index = KU_TRCS3_RANGE;
            break;

        case IS_KuTRCPwrAmpTemp:
            temp_index = KU_PWR_AMP_RANGE;
            break;

        case IS_KuTRCPwrSupTemp:
            temp_index = KU_PWR_SUP_RANGE;
            break;

        case IS_KuHRMTemp:
            temp_index = KU_HRM_RANGE;
            break;

        case IS_KuHRFMTemp:
            temp_index = KU_HRFM_RANGE;
            break;

        case IS_KuVBSPTemp:
            temp_index = KU_VBSP_RANGE;
            break;

        default:
            break;
    }

    return temp_index;
}

/**
 * Sets the visibility of the mask.
 *
 * @param symbol check this symbols kuband mask value
 */
static void update_kumask(SymbolRec *symbol)
{
    if (symbol->symbol_id >= IS_KU_PTG_MASK_FIRST_SYMBOL &&
        symbol->symbol_id <  IS_KU_PTG_MASK_LAST_SYMBOL)
    {
        gint mask_num = (symbol->symbol_id - IS_KU_PTG_MASK_FIRST_SYMBOL);
        gboolean active = False;

        /* is mask selected ? */
        if ((gint)symbol->value == 1)
        {
            active = True;
        }
        if (RTD_GetKubandIsActive())
            MF_SetKuBandVisibility(REALTIME, mask_num, PRIMARY_MASK, active);
        else
            MF_SetKuBandVisibility(REALTIME, mask_num, PRIMARY_MASK, False);

        MF_SetKuBandVisibility(PREDICT, mask_num, PRIMARY_MASK, active);
    }
}

/**
 * Loads the autotrack and openloop values from the iam.xml.
 * These values are used for comparison checking with the
 * telemetry value.
 */
static void load_gao(void)
{
    guint i;

    gchar *autotrack[] =
    {
        GAO_AUTO_TRACK1, GAO_AUTO_TRACK2, GAO_AUTO_TRACK3
    };
    gchar *openloop[] =
    {
        GAO_OPEN_LOOP1, GAO_OPEN_LOOP2, GAO_OPEN_LOOP3
    };

    for (i=0; i<MAX_GAO_VALUES; i++)
    {
        gchar tmp_buf[32];

        if (CP_GetConfigData(autotrack[i], tmp_buf) == True)
        {
            sscanf(tmp_buf, "%u", &thisAutotrackValues[i]);
        }

        if (CP_GetConfigData(openloop[i], tmp_buf) == True)
        {
            sscanf(tmp_buf, "%u", &thisOpenloopValues[i]);
        }
    }
}

/**
 * Loads the temperature limits for sband and kuband
 */
static void load_temp_limits(void)
{
    gdouble caution_margin = 0.0;
    gchar data[256];
    gint i, j;

    const struct _temp_info
    {
        gchar *config_name;
        gint  temp_type;
    } temp_info [] =
    {
        {SBAND1_RFG_AZ_GMBL_OP_MAX, MAX_TEMP},
        {SBAND1_RFG_AZ_GMBL_OP_MIN, MIN_TEMP},
        {SBAND1_RFG_EL_GMBL_OP_MAX, MAX_TEMP},
        {SBAND1_RFG_EL_GMBL_OP_MIN, MIN_TEMP},
        {SBAND1_RFG_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND1_RFG_BSPLT_OP_MIN, MIN_TEMP},
        {SBAND1_XPNDR_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND1_XPNDR_BSPLT_OP_MIN, MIN_TEMP},
        {SBAND1_BSP_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND1_BSP_BSPLT_OP_MIN, MIN_TEMP},

        {SBAND2_RFG_AZ_GMBL_OP_MAX, MAX_TEMP},
        {SBAND2_RFG_AZ_GMBL_OP_MIN, MIN_TEMP},
        {SBAND2_RFG_EL_GMBL_OP_MAX, MAX_TEMP},
        {SBAND2_RFG_EL_GMBL_OP_MIN, MIN_TEMP},
        {SBAND2_RFG_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND2_RFG_BSPLT_OP_MIN, MIN_TEMP},
        {SBAND2_XPNDR_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND2_XPNDR_BSPLT_OP_MIN, MIN_TEMP},
        {SBAND2_BSP_BSPLT_OP_MAX, MAX_TEMP},
        {SBAND2_BSP_BSPLT_OP_MIN, MIN_TEMP},

        {KBAND_SG1_OP_MAX, MAX_TEMP},
        {KBAND_SG1_OP_MIN, MIN_TEMP},
        {KBAND_SG2_OP_MAX, MAX_TEMP},
        {KBAND_SG2_OP_MIN, MIN_TEMP},
        {KBAND_SG3_OP_MAX, MAX_TEMP},
        {KBAND_SG3_OP_MIN, MIN_TEMP},
        {KBAND_SG4_OP_MAX, MAX_TEMP},
        {KBAND_SG4_OP_MIN, MIN_TEMP},
        {KBAND_SG5_OP_MAX, MAX_TEMP},
        {KBAND_SG5_OP_MIN, MIN_TEMP},
        {KBAND_SG6_OP_MAX, MAX_TEMP},
        {KBAND_SG6_OP_MIN, MIN_TEMP},
        {KBAND_SG7_OP_MAX, MAX_TEMP},
        {KBAND_SG7_OP_MIN, MIN_TEMP},
        {KBAND_SG8_OP_MAX, MAX_TEMP},
        {KBAND_SG8_OP_MIN, MIN_TEMP},
        {KBAND_TRC_PWR_SUP_OP_MAX, MAX_TEMP},
        {KBAND_TRC_PWR_SUP_OP_MIN, MIN_TEMP},
        {KBAND_TRC_PWR_AMP_OP_MAX, MAX_TEMP},
        {KBAND_TRC_PWR_AMP_OP_MIN, MIN_TEMP},
        {KBAND_TRC_SENSOR1_OP_MAX, MAX_TEMP},
        {KBAND_TRC_SENSOR1_OP_MIN, MIN_TEMP},
        {KBAND_TRC_SENSOR2_OP_MAX, MAX_TEMP},
        {KBAND_TRC_SENSOR2_OP_MIN, MIN_TEMP},
        {KBAND_TRC_SENSOR3_OP_MAX, MAX_TEMP},
        {KBAND_TRC_SENSOR3_OP_MIN, MIN_TEMP},
        {KBAND_HRM_OP_MAX, MAX_TEMP},
        {KBAND_HRM_OP_MIN, MIN_TEMP},
        {KBAND_HRFM_OP_MAX, MAX_TEMP},
        {KBAND_HRFM_OP_MIN, MIN_TEMP},
        {KBAND_VBSP_OP_MAX, MAX_TEMP},
        {KBAND_VBSP_OP_MIN, MIN_TEMP}
    };

    /**
     * Load the alowable margin between the current temp and any temperature extreme.
     */
    if (CP_GetConfigData(TEMP_CAUTION_MARGIN_DEG, data))
    {
        CP_StringTrim(data);
        sscanf(data, "%lf", &caution_margin);
    }
    else
    {
        ErrorMessage(RTD_GetDialog(), "Config Error",
                     "TEMP_CAUTION_MARGIN_DEG not defined in the config file!\n"
                     "Variable not set.");
    }

    for (i=0,j=0; i<ELEMENTS(temp_info); i++)
    {
        /**
         * Load the max and min temperature limits for sband/kuband
         */
        if (CP_GetConfigData(temp_info[i].config_name, data))
        {
            CP_StringTrim(data);
            sscanf(data, "%lf", &thisOpsTempRanges[j][temp_info[i].temp_type]);
            if (i != 0)
            {
                if (i%2)
                {
                    j++;
                }
            }
        }
        else
        {
            gchar message[256];
            g_sprintf(message,
                      "%s not defined in the config file!\n",
                      temp_info[i].config_name);
            ErrorMessage(RTD_GetDialog(), "Config Error", message);
        }
    }

    for (i=0; i<MAX_TEMP_RANGES; i++)
    {
        /* calculate the high temp margin */
        thisMax_Min_Range[i][MAX_TEMP] = thisOpsTempRanges[i][MAX_TEMP] - caution_margin;

        /* calculate the low temp margin */
        thisMax_Min_Range[i][MIN_TEMP] = thisOpsTempRanges[i][MIN_TEMP] + caution_margin;

        g_message("load_temp_limits: %d -- max/min %5.2f %5.2f, caution max/min %5.2f %5.2f",
                  i,
                  thisOpsTempRanges[i][MAX_TEMP], thisOpsTempRanges[i][MIN_TEMP],
                  thisMax_Min_Range[i][MAX_TEMP], thisMax_Min_Range[i][MIN_TEMP]);
    }
}

/**
 * Gets the joint angle threshold value from the config file.
 * The value is stored in a class variable.
 */
static void load_joint_angle_threshold(void)
{
    gchar data[32];

    /* get threshold value from config file */
    if (CP_GetConfigData(IAM_JOINT_ANGLE_THRESHOLD, data) == False)
    {
        /* config value not found, set the threshold to 0.0 */
        g_warning("IAM_JOINT_ANGLE_THRESHOLD config variable not defined.");
        g_warning("Setting the threshold value to 0.0");
        thisJointAngleThreshold = 0.0;
    }
    else
    {
        sscanf(data, "%lf", &thisJointAngleThreshold);
    }
}

/**
 * Checks the joint angle threshold with the given value.
 * If the threshold condition is meet, the given value is stored
 * as the new cached value to check against.
 *
 * @param value check this value against cached value
 * @param angle_idx index into cached value
 *
 * @return True if threshold meet, otherwise False
 */
static gboolean check_threshold(gdouble value, gint angle_idx)
{
    gboolean threshold_meet = False;
    gdouble angle_value = thisAngleValue[angle_idx] + thisJointAngleThreshold;
    gdouble threshold = fabs(angle_value - value);

    /* check value's threshold against the joint angle threshold */
    if (threshold > thisJointAngleThreshold)
    {
        /* threshold meet, save this value for future checks */
        thisAngleValue[angle_idx] = value;
        threshold_meet = True;
    }

    return threshold_meet;
}

/**
 * If the symbol value string meets certain criteria, then the color
 * is set to red, otherwise, the status is used as the criteria.
 * Some exceptions exist based on the symbol and the value of the
 * symbol.
 * IS_KuPLCAct: when text="normal", color="white"
 * IS_SbAGC: when color="green", reset color="white"
 *
 * @param symbol check this symbol's status for color
 *
 * @return color
 */
static guchar set_value_color(SymbolRec *symbol)
{
    guchar color;

    /* if status is good, then check text */
    if (IS_GOOD(symbol->status.stclass) == True)
    {
        /* now do color for the value */
        if ((g_ascii_strcasecmp(symbol->value_str, DASH_STRING) == 0) ||
            (g_ascii_strcasecmp(symbol->value_str, ERR_STRING)  == 0) ||
            (g_ascii_strcasecmp(symbol->value_str, BAD_STRING)  == 0) ||
            (g_ascii_strcasecmp(symbol->value_str, E_STRING)    == 0))
        {
            return CH_GetColor(BAD_VALUE);
        }

        if ((g_ascii_strcasecmp(symbol->value_str,INACTIVE_STRING)== 0))
        {
            return CH_GetColor("Gray");
        }
    }

    /* check special puis */
    switch (symbol->symbol_id)
    {
        /* do color for temperature */
        case IS_Sb1AZTemp:
        case IS_Sb1ELTemp:
        case IS_Sb1RFGTemp:
        case IS_Sb1TransTemp:
        case IS_Sb1BSPTemp:
        case IS_Sb2AZTemp:
        case IS_Sb2ELTemp:
        case IS_Sb2RFGTemp:
        case IS_Sb2TransTemp:
        case IS_Sb2BSPTemp:
        case IS_KuSG1Temp:
        case IS_KuSG2Temp:
        case IS_KuSG3Temp:
        case IS_KuSG4Temp:
        case IS_KuSG5Temp:
        case IS_KuSG6Temp:
        case IS_KuSG7Temp:
        case IS_KuSG8Temp:
        case IS_KuTRCS1Temp:
        case IS_KuTRCS2Temp:
        case IS_KuTRCS3Temp:
        case IS_KuTRCPwrAmpTemp:
        case IS_KuTRCPwrSupTemp:
        case IS_KuHRMTemp:
        case IS_KuHRFMTemp:
        case IS_KuVBSPTemp:
            {
                gint range = get_temp_range_index(symbol->symbol_id);
                gdouble value = symbol->value;

                if (((value >= thisMax_Min_Range[range][MAX_TEMP]) &&
                     (value <  thisOpsTempRanges[range][MAX_TEMP])) ||

                    ((value <= thisMax_Min_Range[range][MIN_TEMP]) &&
                     (value >  thisOpsTempRanges[range][MIN_TEMP])))
                {
                    color = CH_GetColor(WARN_VALUE);
                }
                else if ((value >= thisOpsTempRanges[range][MAX_TEMP]) ||
                         (value <= thisOpsTempRanges[range][MIN_TEMP]))
                {
                    color = CH_GetColor(BAD_VALUE);
                }
                else
                {
                    /* get the default color */
                    color = convert_status2color(symbol->status);
                }
            }
            break;

        default:
            /* get the default color */
            color = convert_status2color(symbol->status);
            break;
    }

    return color;
}

/**
 * if the status is default, force to green, otherwise
 * use the isp color index returned in the status buffer.
 *
 * @param status isp status
 *
 * @return color
 */
static guchar convert_status2color(StStatusStruct status)
{
    guchar color = status.color;
    if (status.stclass == STDEFAULT)
    {
        color = GC_GREEN;
    }

    return color;
}

/**
 * If the value is 1, then load the given good_val, otherwise, "Bad".
 *
 * @param symbol check this symbol
 * @param good_val string to use when value is 1
 */
static void set_pathgo_value(SymbolRec *symbol, gchar *good_val)
{
    gchar *sptr;

    switch ((gint)symbol->value)
    {
        case 0:   sptr = BAD_STRING; break;
        case 1:   sptr = good_val;   break;
        default:  sptr = BAD_STRING; break;
    }
    g_stpcpy(symbol->value_str, sptr);
    symbol->value_str_len = strlen(sptr);

    symbol->value_color = set_value_color(symbol);
}
