/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/stat.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <expat.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
    #include <gdk/gdkwin32.h>
#endif

#define PRIVATE
#include "DynamicDraw.h"
#undef PRIVATE

#include "ColorHandler.h"
#include "ConfigParser.h"
#include "Conversions.h"
#include "Geometry.h"
#include "gtkjfilechooser.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PredictDialog.h"
#include "PredictDataHandler.h"
#include "RealtimeDialog.h"
#include "WindowGrid.h"
#include "XML_Parse.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/DynamicDraw.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* the name of the structure, if it fails to load, use blue */
static gchar *thisStructureColor = "structure_color";

#define STRUCTURE_COLOR_RED     0
#define STRUCTURE_COLOR_GREEN   45900
#define STRUCTURE_COLOR_BLUE    42948

#define SMD_SUFFIX "smd"

static guint thisNumberOfSegments = 0;
static SEGMENT thisSegmentTable[MAX_SEGMENTS];

/* array of JointAngles */
static GPtrArray *thisJointAngleArray = NULL;

/* lookup table for segment rotation value */
static GHashTable *thisAngleSegmentsHash = NULL;

/* holds pixmap data for each segment */
static PixmapData *thisAnglePixmaps[NUM_DISPLAYS][NUM_VIEWS] =
{
    { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
    { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
};


static gint thisMaxVectorsPerPolygon= 0;

static VECTOR thisAntennaLocation[NUM_COORDS] =
{
    {0.0, 0.0, 0.0},    /* s-band */
    {0.0, 0.0, 0.0},    /* s-band 2 */
    {0.0, 0.0, 0.0},    /* ku-band */
    {0.0, 0.0, 0.0},    /* ku-band 2 */
    {0.0, 0.0, 0.0}     /* station az/el (lowgain) */
};

static GdkPoint *thisPolygonData = NULL;

static gchar thisSegmentMotionDefinitionFile[MAXPATHLEN] = {0};

/*
** Define a polygon that represents the whole display.  We use
** it to determine whether a point is inside or outside of the
** display.
*/
static Point thisDisplayPolygon [4] =
{
    {MAX_KU_CROSS_ELEVATION * 10, MAX_KU_ELEVATION * 10},
    {MIN_KU_CROSS_ELEVATION * 10, MAX_KU_ELEVATION * 10},
    {MIN_KU_CROSS_ELEVATION * 10, MIN_KU_ELEVATION * 10},
    {MAX_KU_CROSS_ELEVATION * 10, MIN_KU_ELEVATION * 10}
};

static Line thisDisplayLine1;
static Line thisDisplayLine2;
static Line thisDisplayLine3;
static Line thisDisplayLine4;

typedef struct RotationSequence
{
    gchar angleName[LABEL_SIZE];
    gint  angle;
} RotationSequence;

/*************************************************************************/

/**
 * This function loads into memory the structure files for the dynamic parts.
 */
void DD_Constructor(void)
{
    GdkColor gcolor;

    if (CP_GetConfigData(IAM_DYNAM_STR_FILE, thisSegmentMotionDefinitionFile) == False)
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "IAM_DYNAM_STR_FILE not defined in the config file!\n"
                     "Dynamic Structures not generated.");
        return;
    }

    /* release memory */
    DD_Destructor(True);

    /* load sequence rotation hash - needs happen before the smd file load */
    loadRotationSequenceHash();

    /* open and read the segment motion definition file */
    loadSmdFile();

    /* load the data from the segment files */
    if (loadVectorData() == False)
    {
        DD_Destructor(True);
        thisNumberOfSegments = 0;
        return;
    }

    /* allocate pixmap buffers for the segments */
    allocPixmapSegments();

    /* add the structure color to our color map */
    gcolor.red   = STRUCTURE_COLOR_RED;
    gcolor.green = STRUCTURE_COLOR_GREEN;
    gcolor.blue  = STRUCTURE_COLOR_BLUE;
    if (CH_AddColor(thisStructureColor, &gcolor) == False)
    {
        /* failed to load, add "blue" */
        thisStructureColor = "Blue";
    }

    /* get the antenna locations from the iam.xml file */
    loadAntennaLocations();

    /*
    ** Define the boundaries of the display made from the corner
    ** coordinates of the display.  We need these lines because
    ** we have to determine the coordinates of the point where
    ** a line connecting polygon points intersects the display.
    */
    thisDisplayLine1.p1.x = thisDisplayPolygon[0].x;
    thisDisplayLine1.p1.y = thisDisplayPolygon[0].y;
    thisDisplayLine1.p2.x = thisDisplayPolygon[1].x;
    thisDisplayLine1.p2.y = thisDisplayPolygon[1].y;

    thisDisplayLine2.p1.x = thisDisplayPolygon[1].x;
    thisDisplayLine2.p1.y = thisDisplayPolygon[1].y;
    thisDisplayLine2.p2.x = thisDisplayPolygon[2].x;
    thisDisplayLine2.p2.y = thisDisplayPolygon[2].y;

    thisDisplayLine3.p1.x = thisDisplayPolygon[2].x;
    thisDisplayLine3.p1.y = thisDisplayPolygon[2].y;
    thisDisplayLine3.p2.x = thisDisplayPolygon[3].x;
    thisDisplayLine3.p2.y = thisDisplayPolygon[3].y;

    thisDisplayLine4.p1.x = thisDisplayPolygon[3].x;
    thisDisplayLine4.p1.y = thisDisplayPolygon[3].y;
    thisDisplayLine4.p2.x = thisDisplayPolygon[0].x;
    thisDisplayLine4.p2.y = thisDisplayPolygon[0].y;
}

/**
 * This function frees the current dynamic parts data.
 *
 * @param freeAll flag indicating if all data should be freed;
 * otherwise just the joint angle data will be freed.
 */
void DD_Destructor(gint freeAll)
{
    guint s,d,c,v;

    if (freeAll)
    {
        g_message("DD_Destructor: number of segments=%d",thisNumberOfSegments);
        for (s=0; s<thisNumberOfSegments; s++)
        {
            if (thisSegmentTable[s].polygon_array != NULL)
            {
                MH_Free(thisSegmentTable[s].polygon_array);
            }
            thisSegmentTable[s].polygon_array = NULL;

            if (thisSegmentTable[s].vector_array != NULL)
            {
                MH_Free(thisSegmentTable[s].vector_array);
            }
            thisSegmentTable[s].vector_array = NULL;

            for (d=0; d<NUM_DISPLAYS; d++)
            {
                if (thisSegmentTable[s].rotated_vectors[d] != NULL)
                {
                    MH_Free(thisSegmentTable[s].rotated_vectors[d]);
                }
                thisSegmentTable[s].rotated_vectors[d] = NULL;

                for (c=0; c<NUM_COORDS; c++)
                {
                    if (thisSegmentTable[s].coordinate_points[d][c] != NULL)
                    {
                        MH_Free(thisSegmentTable[s].coordinate_points[d][c]);
                    }
                    thisSegmentTable[s].coordinate_points[d][c] = NULL;
                }

                for (v=0; v<NUM_VIEWS; v++)
                {
                    if (thisSegmentTable[s].window_points[d][v] != NULL)
                    {
                        MH_Free(thisSegmentTable[s].window_points[d][v]);
                    }
                    thisSegmentTable[s].window_points[d][v] = NULL;
                }
            }
        }
        thisNumberOfSegments = 0;

        /* release the segment pixmaps */
        for (d=0; d<NUM_DISPLAYS; d++)
        {
            for (v=0; v<NUM_VIEWS; v++)
            {
                if (thisAnglePixmaps[d][v] != NULL)
                {
                    MH_Free(thisAnglePixmaps[d][v]);
                    thisAnglePixmaps[d][v] = NULL;
                }
            }
        }

        /* release joint angle data */
        DD_FreeJointAngleData(thisJointAngleArray);

        if (thisAngleSegmentsHash != NULL)
        {
            /* release the hash table rotation data*/
            g_message("Removing the thisAngleSegmensHash");
            g_hash_table_foreach_remove(thisAngleSegmentsHash, hashTableForeach, NULL);
        }

        /* release the polygon data */
        if (thisPolygonData != NULL)
        {
            MH_Free(thisPolygonData);
            thisPolygonData = NULL;
        }
        thisMaxVectorsPerPolygon = 0;
    }
}

/**
 * Returns just the head part of the smd filename, e.g. 13A.
 * The local class variable is checked first, if for some
 * reason its not set, the config variable ISS_CONFIGURATION
 * is checked. If its not set, the something is really wrong.
 *
 * @return current structure defined or NULL
 */
gchar *DD_GetActiveStructure(void)
{
    gchar *structure = NULL;

    if (thisSegmentMotionDefinitionFile[0] != '\0')
    {
        structure = CP_GetRootName(thisSegmentMotionDefinitionFile);
    }
    else
    {
        gchar tmp[32];
        if (CP_GetISSConfigValue(tmp) == True)
        {
            structure = g_strdup(tmp);
        }
        else
        {
            g_warning("Unable to retrieve the structure name!");
        }
    }

    return structure;
}

/**
 * Allows the user to select different dynamic structures to use.
 *
 * @param structure use this structure
 */
void DD_ChangeStructure(gchar *structure)
{
    gchar structure_dir[MAXPATHLEN];

    /* get starting directory for file dialog */
    if (CP_GetConfigData(IAM_DYNAMIC_DIR, structure_dir) == True)
    {
        gchar *dir_part = g_path_get_dirname(structure_dir);
        gchar *smd = g_build_filename(dir_part, structure, NULL);

        /* update the iam.xml file with the new structure selected */
        if (CP_UpdateConfigValue(ISS_CONFIGURATION, structure) == False)
        {
            g_warning("Update of config variable ISS_CONFIGURATION failed!");
        }
        else
        {
            CP_WriteConfigFile();
        }

        /* release memory */
        DD_Destructor(True);

        /* set the smd file class variable */
        createSmdFile(smd);

        /* load the smd file data */
        loadSmdFile();

        /* load the data from the segment files */
        if (loadVectorData() == False)
        {
            /* something bad happened */
            DD_Destructor(True);
            thisNumberOfSegments = 0;
            return;
        }

        /* allocate pixmap buffers for the segments */
        allocPixmapSegments();

        /* reload the joint angle data, the destructor released the memory */
        XML_ProcessData(SUNABG_PREDICT);

        /* change to the correct masks */
        MF_ChangeMasks();
        OM_ChangeMasks();

        /* change structure identifier for dialog title */
        gchar *structure_identifier = CP_GetRootName(thisSegmentMotionDefinitionFile);
        if (structure_identifier != NULL)
        {
            AM_SetStructureIdentifier(structure_identifier);
            g_free(structure_identifier);

            /* set the titles */
            PD_SetDialogTitle();
            RTD_SetDialogTitle();
        }

        /* force redraw */
        PD_SetRedraw(True);
        RTD_SetRedraw(True);

        /* do draw */
        PD_DrawData(False);
        RTD_DrawData();
    }
}

/**
 * Finds all dynamic structures that are defined with via the
 * smd file.
 *
 * @return Pointer array of available dynamic structures or NULL
 */
GPtrArray *DD_GetStructureNames(void)
{
    gchar structure_dir[MAXPATHLEN];
    GPtrArray *ptr_array = NULL;

    /* get starting directory for file dialog */
    if (CP_GetConfigData(IAM_DYNAMIC_DIR, structure_dir) == True)
    {
        GDir *dir;
        GError *error = NULL;
        const gchar *fname;
        gchar *dir_part = g_path_get_dirname(structure_dir);

        /* search the directory for the structure files */
        dir = g_dir_open(dir_part, 0, &error);
        if (dir != NULL)
        {
            while ((fname = g_dir_read_name(dir)) != NULL)
            {
                /* is the file a directory */
                gchar *tname = g_build_filename(dir_part, fname, NULL);
                if (g_file_test(tname, G_FILE_TEST_IS_DIR) == True)
                {
                    error = NULL;
                    GDir *tdir = g_dir_open(tname, 0, &error);
                    if (tdir != NULL)
                    {
                        /* search this directory for the smd file */
                        while ((fname = g_dir_read_name(tdir)) != NULL)
                        {
                            if (g_str_has_suffix(fname, SMD_SUFFIX) == True)
                            {
                                /* smd suffix off and return just the head */
                                gchar *name = CP_GetRootName((gchar *)fname);
                                if (ptr_array == NULL)
                                {
                                    ptr_array = g_ptr_array_new();
                                }
                                g_ptr_array_add(ptr_array, (gpointer)g_strdup(name));
                                g_free(name);
                            }
                        }
                        g_dir_close(tdir);
                    }
                    else
                    {
                        if (error != NULL)
                        {
                            g_warning(error->message);
                            g_error_free(error);
                        }
                    }
                }
            }
            g_dir_close(dir);
        }
        else
        {
            g_warning("Unable to open the structure directory: %s",structure_dir);
            if (error != NULL)
            {
                g_warning(error->message);
                g_error_free(error);
            }
        }
    }
    else
    {
        g_warning("IAM_DYNAMIC_DIR not found in the iam.xml file!");
    }

    return ptr_array;
}

/**
 * Sets the class variable 'thisSegmentMotionDefinitionFile'
 *
 * @param dynamic_dir the directory where the smd file is found
 */
static void createSmdFile(gchar *dynamic_dir)
{
    if (g_file_test(dynamic_dir, G_FILE_TEST_IS_DIR) == True)
    {
        gchar *smd_file = getSmdFile(dynamic_dir);
        if (smd_file != NULL)
        {
            g_stpcpy(thisSegmentMotionDefinitionFile, smd_file);
            g_free(smd_file);
        }
    }
    else
    {
        /* does this file have the smd suffix, is it valid? */
        if (g_str_has_suffix(dynamic_dir, SMD_SUFFIX) == True)
        {
            g_stpcpy(thisSegmentMotionDefinitionFile, dynamic_dir);
        }
        else
        {
            gchar *dirpart = g_path_get_dirname(dynamic_dir);
            gchar *smd_file = getSmdFile(dirpart);
            if (smd_file != NULL)
            {
                g_stpcpy(thisSegmentMotionDefinitionFile, smd_file);
                g_free(smd_file);
            }
            g_free(dirpart);
        }
    }

    g_message("createSmdFile: smd file=%s",thisSegmentMotionDefinitionFile);
}

/**
 * Gets the segment motion file (smd) from the given
 * dynamic directory. If multiple smd files are found
 * in the directory, the smd file with the same base
 * directory name will be returned.
 *
 * @param dynamic_dir directory where the smd file is
 *
 * @return newly allocated string or NULL
 */
static gchar *getSmdFile(gchar *dynamic_dir)
{
    GDir *dir;
    GError *error = NULL;
    const gchar *fname;
    gchar *smd_file = NULL;

    /*
    ** get the directory part where the smd file is located
    ** this will be used to determine the name of the smd
    ** file. this eliminates files that end with the same
    ** suffix.
    */
    gchar *smd_prefix = g_path_get_basename(dynamic_dir);

    dir = g_dir_open(dynamic_dir, 0, &error);
    if (dir != NULL)
    {
        while ((fname = g_dir_read_name(dir)) != NULL)
        {
            /* is this the smd file ? */
            if (g_str_has_suffix(fname, SMD_SUFFIX) == True)
            {
                /*
                ** now see if this is really the file
                ** since we don't know what the user is
                ** really wanting, we'll use the basename
                ** of the directory location as the filename
                ** part. if the user meant something else, then
                ** the user should explicitly select the smd file
                */
                if (g_str_has_prefix(fname, smd_prefix) == True)
                {
                    smd_file = g_strconcat(dynamic_dir, G_DIR_SEPARATOR_S, fname, NULL);
                    break;
                }
            }
        }

        g_dir_close(dir);
    }
    else
    {
        if (error != NULL)
        {
            g_warning(error->message);
            g_error_free(error);
        }
    }

    return smd_file;
}

/**
 * Loads the smd file information.
 */
static void loadSmdFile(void)
{
    gchar line[100];
    gchar fname[256];
    gchar *dynamic_dir_p;
    gchar segments_dir[MAXPATHLEN];
    gchar item_name[25];
    gchar rotation_sequence[64];
    gchar *ch;
    guint i;
    gint d,c,v;
    gdouble x,y,z;

    FILE *fp = fopen(thisSegmentMotionDefinitionFile, "r");
    if (fp == NULL)
    {
        gchar message[256];
        g_sprintf(message, "%s: unable to open dynamic data file!\n"
                  "Dynamic Structures not generated.", thisSegmentMotionDefinitionFile);
        ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
        AM_Exit(1);
    }
    else
    {
        /*
        ** build the dynamic segments directory
        ** the segments directory is where the *.dat files
        */
        dynamic_dir_p = g_path_get_dirname(thisSegmentMotionDefinitionFile);
        g_sprintf(segments_dir, "%s%ssegments", dynamic_dir_p, G_DIR_SEPARATOR_S);
        g_free(dynamic_dir_p);

        i = 0;
        while (fgets(line, sizeof(line)-1, fp))
        {
            sscanf(line, "%24s %255s", item_name, fname);
            if ((g_ascii_strncasecmp(item_name, "SEGMENT_", 8) != 0) ||
                (g_ascii_strncasecmp(&item_name[strlen(item_name)-9], "_FILENAME", 9) != 0))
            {
                continue;
            }

            /* look for the last forward slash */
            ch = strrchr(fname, '/');
            if (ch == NULL)
            {
                ch = fname;
            }
            else
            {
                ch++;
            }
            g_sprintf(thisSegmentTable[i].filename, "%s%s%s", segments_dir, G_DIR_SEPARATOR_S, ch );

            fgets(line, sizeof(line)-1, fp);  /* axis center */
            sscanf(line, "%*s %lf %lf %lf", &x, &y, &z);
            VSET(&thisSegmentTable[i].rotation_axis_center, x, y, z);

            fgets(line, sizeof(line)-1, fp);  /* rotation sequence */
            sscanf(line, "%*s %63s", rotation_sequence);
            setRotationSequence(&thisSegmentTable[i], rotation_sequence);

            fgets(line, sizeof(line)-1, fp);  /* rotation axis head */
            sscanf(line, "%*s %lf %lf %lf", &x, &y, &z);
            VSET(&thisSegmentTable[i].rotation_axis_head, x, y, z);

            fgets(line, sizeof(line)-1, fp);  /* gamma (normal) axis head */
            sscanf(line, "%*s %lf %lf %lf", &x, &y, &z);
            VSET(&thisSegmentTable[i].normal_rotation_axis_head, x, y, z);

            thisSegmentTable[i].number_of_polygons = 0;
            thisSegmentTable[i].polygon_array      = NULL;
            thisSegmentTable[i].number_of_vectors  = 0;
            thisSegmentTable[i].vector_array       = NULL;

            for (d=0; d<NUM_DISPLAYS; d++)
            {
                thisSegmentTable[i].rotated_vectors   [d] = NULL;

                for (c=0; c<NUM_COORDS; c++)
                {
                    thisSegmentTable[i].current_angle_set [d][c] = False;
                    thisSegmentTable[i].current_bg_angle  [d][c] = 0.0;
                    thisSegmentTable[i].current_sarj_angle[d][c] = 0.0;
                    thisSegmentTable[i].coordinate_points [d][c] = NULL;
                }

                for (v=0; v<NUM_VIEWS; v++)
                {
                    thisSegmentTable[i].window_points[d][v] = NULL;
                }
            }

            i++;
        }

        thisNumberOfSegments = i;
        fclose(fp);
    }
}

/**
 * This function loads the vectors defining the shape of the dynamic parts.
 *
 * @return True or False
 */
static gboolean loadVectorData(void)
{
    FILE *fp;
    gchar line[100];
    guint s, p, v, d, c;
    gdouble x, y, z;
    gboolean invalid_file;

    gint rc;

    struct stat buffer;

    gint polygon_index = 0;
    gint vector_count = 0;
    gint vector_index = 0;
    gint total_allocated = 0;

    /* loop thru the segments */
    g_message("loadVectorData: number of segments=%d",thisNumberOfSegments);
    for (s=0; s<thisNumberOfSegments; s++)
    {
        /* open the segment's file */
        fp = fopen( thisSegmentTable[s].filename, "r" );
        if (fp == NULL)
        {
            gchar message[256];
            g_sprintf(message, "%s: unable to 'open' ISS structure data file!",
                      thisSegmentTable[s].filename);
            ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
            return False;
        }

        /*
        ** get stats on the file, instead of parsing the file a
        ** little at a time, lets read it all in and parse memory
        */
        if (stat(thisSegmentTable[s].filename, &buffer) == -1)
        {
            gchar message[256];
            g_sprintf(message, "%s: unable to 'status' ISS structure data file!",
                      thisSegmentTable[s].filename);
            ErrorMessage(AM_GetActiveDisplay(), "Stat Error", message);
            fclose(fp);
            return False;
        }

        /* determine the number of polygons and vectors */
        invalid_file = False;
        p = 0;
        v = 0;
        polygon_index = 0;
        vector_count = 0;
        vector_index = 0;
        while (fgets( line, sizeof(line)-1, fp ) != NULL)
        {
            /* filter out comment or newline or blank line */
            if (line[0] != '#' && line[0] != '\n' && line[0] != ' ')
            {
                rc = sscanf(line, "%lf %lf %lf", &x, &y, &z);
                if (rc == 1)
                {
                    v = (gint)x;
                    thisSegmentTable[s].number_of_polygons++;
                    thisSegmentTable[s].number_of_vectors += v;
                    thisMaxVectorsPerPolygon = MAX(thisMaxVectorsPerPolygon, (gint)v);
                }
                else if (rc == 3)
                {
                    continue;
                }
                else
                {
                    /* error */
                    invalid_file = True;
                    break;
                }
            }
        }

        /* any errors ? */
        if (invalid_file || (thisMaxVectorsPerPolygon <= 0))
        {
            gchar message[256];
            if (invalid_file)
            {
                g_sprintf(message, "%s: invalid file format!",
                          thisSegmentTable[s].filename);
            }
            else
            {
                g_sprintf(message, "%s: incorrect max vector per polygon!",
                          thisSegmentTable[s].filename);
            }

            ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
            fclose(fp);
            return False;
        }

        /* allocate polygon and vector arrays */
        if (thisSegmentTable[s].polygon_array != NULL)
        {
            MH_Free(thisSegmentTable[s].polygon_array);
        }
        total_allocated += thisSegmentTable[s].number_of_polygons * sizeof(POLYGON);

        if (thisSegmentTable[s].number_of_polygons > 0)
        {
            thisSegmentTable[s].polygon_array =
                (POLYGON *) MH_Calloc(thisSegmentTable[s].number_of_polygons * sizeof(POLYGON), __FILE__, __LINE__);
        }

        if (thisSegmentTable[s].vector_array != NULL)
        {
            MH_Free(thisSegmentTable[s].vector_array);
        }
        total_allocated += thisSegmentTable[s].number_of_vectors * sizeof(VECTOR);

        if (thisSegmentTable[s].number_of_vectors > 0)
        {
            thisSegmentTable[s].vector_array =
                (VECTOR *) MH_Calloc(thisSegmentTable[s].number_of_vectors * sizeof(VECTOR), __FILE__, __LINE__);
        }

        for (d=0; d<NUM_DISPLAYS; d++)
        {
            if (thisSegmentTable[s].rotated_vectors[d] != NULL)
            {
                MH_Free(thisSegmentTable[s].rotated_vectors[d]);
            }
            total_allocated += thisSegmentTable[s].number_of_vectors * sizeof(VECTOR);

            if (thisSegmentTable[s].number_of_vectors > 0)
            {
                thisSegmentTable[s].rotated_vectors[d] =
                    (VECTOR *) MH_Calloc(thisSegmentTable[s].number_of_vectors *  sizeof(VECTOR), __FILE__, __LINE__);
            }

            for (c=0; c<NUM_COORDS; c++)
            {
                if (thisSegmentTable[s].coordinate_points[d][c] != NULL)
                {
                    MH_Free(thisSegmentTable[s].coordinate_points[d][c]);
                }
                total_allocated += thisSegmentTable[s].number_of_vectors * sizeof(Point);

                if (thisSegmentTable[s].number_of_vectors > 0)
                {
                    thisSegmentTable[s].coordinate_points[d][c] =
                        (Point *) MH_Calloc(thisSegmentTable[s].number_of_vectors *  sizeof(Point), __FILE__, __LINE__);
                }
            }

            /*
            ** allocate the window array using the count that includes extra
            ** value for last point in polygon
            */
            for (v=0; v<NUM_VIEWS; v++)
            {
                if (thisSegmentTable[s].window_points[d][v] != NULL)
                {
                    MH_Free(thisSegmentTable[s].window_points[d][v]);
                }
                total_allocated += thisSegmentTable[s].number_of_vectors * sizeof(GdkPoint);

                if (thisSegmentTable[s].number_of_vectors > 0)
                {
                    thisSegmentTable[s].window_points[d][v] =
                        (GdkPoint *) MH_Calloc(thisSegmentTable[s].number_of_vectors *  sizeof(GdkPoint), __FILE__, __LINE__);
                }
            }
        }

        /* load all vectors from the file */
        p = 0;
        v = 0;
        rewind(fp);
        while (fgets( line, sizeof(line)-1, fp ))
        {
            if (line[0] != '#') /* filter out comment lines */
            {
                rc = sscanf(line, "%lf %lf %lf", &x, &y, &z);
                if (rc == 1)
                {
                    thisSegmentTable[s].polygon_array[p].first_vector = v;
                    thisSegmentTable[s].polygon_array[p].num_vectors = 0;
                    thisSegmentTable[s].polygon_array[p].draw_it = True;
                    p++;
                }
                else if (rc == 3)
                {
                    thisSegmentTable[s].vector_array[v].x = x;
                    thisSegmentTable[s].vector_array[v].y = y;
                    thisSegmentTable[s].vector_array[v].z = z;
                    v++;

                    thisSegmentTable[s].polygon_array[p-1].num_vectors++;
                }
            }
        }
        fclose(fp);
    }

    /* allocate buffer for polygon data */
    if (thisMaxVectorsPerPolygon > 0)
    {
        if (thisPolygonData != NULL)
        {
            MH_Free(thisPolygonData);
        }
        total_allocated += (sizeof(GdkPoint) * (thisMaxVectorsPerPolygon + 1));

        thisPolygonData = (GdkPoint *)
                          MH_Calloc((gint)(sizeof(GdkPoint) * (thisMaxVectorsPerPolygon + 1)), __FILE__, __LINE__);
    }

    g_message("loadVectorData: total allocated=%4.1fmb",
              (float)((float)total_allocated/(1024*1024)));
    return True;
}

/**
 * This function loads the locations of the antennas from the config file
 */
static void loadAntennaLocations(void)
{
    gchar data[256];
    gdouble x, y, z;

    if (! CP_GetConfigData(KUBAND_LOCATION, data))
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "KUBAND_LOCATION not defined in the config file!\n"
                     "Dynamic Structures not generated.");
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &x, &y, &z ) != 3)
        {
            ErrorMessage(AM_GetActiveDisplay(), "Data Error",
                         "Configuration parameter KUBAND_LOCATION is not "
                         "formatted correctly!");
        }
        else
        {
            thisAntennaLocation[KUBAND].x = x;
            thisAntennaLocation[KUBAND].y = y;
            thisAntennaLocation[KUBAND].z = z;
        }
    }

    if (! CP_GetConfigData(KUBAND2_LOCATION, data))
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "KUBAND_LOCATION not defined in the config file!\n"
                     "Dynamic Structures not generated.");
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &x, &y, &z ) != 3)
        {
            ErrorMessage(AM_GetActiveDisplay(), "Data Error",
                         "Configuration parameter KUBAND2_LOCATION is not "
                         "formatted correctly!");
        }
        else
        {
            thisAntennaLocation[KUBAND2].x = x;
            thisAntennaLocation[KUBAND2].y = y;
            thisAntennaLocation[KUBAND2].z = z;
        }
    }

    if (! CP_GetConfigData(SBAND1_LOCATION, data))
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "SBAND1_LOCATION not defined in the config file!\n"
                     "Dynamic Structures not generated.");
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &x, &y, &z ) != 3)
        {
            ErrorMessage(AM_GetActiveDisplay(), "Data Error",
                         "Configuration parameter SBAND1_LOCATION is not "
                         "formatted correctly!");
        }
        else
        {
            thisAntennaLocation[SBAND1].x = x;
            thisAntennaLocation[SBAND1].y = y;
            thisAntennaLocation[SBAND1].z = z;
        }
    }

    if (! CP_GetConfigData(SBAND2_LOCATION, data))
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "SBAND2_LOCATION not defined in the config file!\n"
                     "Dynamic Structures not generated.");
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &x, &y, &z ) != 3)
        {
            ErrorMessage(AM_GetActiveDisplay(), "Data Error",
                         "Configuration parameter SBAND2_LOCATION is not "
                         "formatted correctly!");
        }
        else
        {
            thisAntennaLocation[SBAND2].x = x;
            thisAntennaLocation[SBAND2].y = y;
            thisAntennaLocation[SBAND2].z = z;
        }
    }


    if (! CP_GetConfigData(GENERIC_LOCATION, data))
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "GENERIC_LOCATION not defined in the config file!\n"
                     "Dynamic Structures not generated.");
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &x, &y, &z ) != 3)
        {
            ErrorMessage(AM_GetActiveDisplay(), "Data Error",
                         "Configuration parameter GENERIC_LOCATION is not "
                         "formatted correctly!");
        }
        else
        {
            thisAntennaLocation[STATION].x = x;
            thisAntennaLocation[STATION].y = y;
            thisAntennaLocation[STATION].z = z;
        }
    }
}

/**
 * Allocates individual pixmaps for each dynamic segment. These
 * pixmaps are eventually rendered into the dynamic_data
 * drawable associated with the view.
 */
static void allocPixmapSegments(void)
{
    guint d, c, s;

    for (d=0; d<NUM_DISPLAYS; d++)
    {
        for (c=0; c<NUM_VIEWS; c++)
        {
            /* allocate pixmaps for each segment */
            thisAnglePixmaps[d][c] = (PixmapData *)MH_Calloc(thisNumberOfSegments*sizeof(PixmapData), __FILE__, __LINE__);

            for (s=0; s<thisNumberOfSegments; s++)
            {
                thisAnglePixmaps[d][c][s].pixmap = NULL;
                thisAnglePixmaps[d][c][s].redraw = True;
                thisAnglePixmaps[d][c][s].width = 0;
                thisAnglePixmaps[d][c][s].height = 0;
            }
        }
    }
}

/**
 * Returns the current values stored for the antenna location
 * for the given display and coordinate.
 *
 * @param coord current coordinate type
 *
 * @return VECTOR of antenna location
 */
VECTOR *DD_GetAntennaLocation(gint coord)
{
    return &thisAntennaLocation[coord];
}

/**
 * Sets the antenna location values for the given
 * display and coordinate.
 *
 * @param coord current coordinate type
 * @param data the VECTOR data
 */
void DD_SetAntennaLocation(gint coord, VECTOR *data)
{
    thisAntennaLocation[coord].x = data->x;
    thisAntennaLocation[coord].y = data->y;
    thisAntennaLocation[coord].z = data->z;
}

/**
 * This function returns the current joint angles
 *
 * @param coord current coordinate type
 * @param entry_time time of entry
 * @param ja joint angle array data
 *
 * @return flag True if successful, otherwise False
 */
gboolean DD_GetAngles(gint coord, gchar *entry_time, JointAngle *ja)
{
    if (thisNumberOfSegments == 0)
    {
        return False; /* in case dynamics not loaded */
    }

    return getAngleData(entry_time, ja);
}

/**
 * This function draws the dynamic parts at the specified time.
 *
 * @param windata the window data
 * @param display current display type (REALTIME or PREDICT)
 * @param coord_view current coodinate view
 * @param cpt current point
 */
void DD_DrawDataAtTime(WindowData *windata, gint display, gint coord_view, GList *cpt)
{
    JointAngle ja;

    if (thisNumberOfSegments > 0)
    {
        PredictPt *pt = (cpt != NULL ? (PredictPt *)cpt->data : NULL);

        /* load angle data */
        getAngleData((pt != NULL ? pt->timeStr : NULL), &ja);

        /* draw the data */
        DD_DrawDataForAngle(windata, display, coord_view, &ja);
    }
}

/**
 * This method draws the dynamic parts for the specified joint angle.
 *
 * @param windata the window data
 * @param display current display type (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param ja joint angle array
 */
void DD_DrawDataForAngle(WindowData *windata, gint display, gint coord_view, JointAngle *ja)
{
    guint s;
    gint p;
    gdouble bg_angle;
    gdouble sarj_angle;
    gboolean recompute_angles = False;
    gboolean recompute_window = False;

    gint coord = windata->coordinate_type;
    if (coord != UNKNOWN_COORD)
    {
        /* if the dymamic's drawable was NOT reallocated then the */
        /* drawable must be cleared prior to rendering the segments to it */
        if ((recompute_window = AM_AllocatePixmapData(windata,
                                                      windata->dynamic_data,
                                                      False)) == False)
        {
            AM_ClearDrawable(windata->dynamic_data->pixmap,
                             windata->drawing_area->style->black_gc,
                             0, 0,
                             windata->dynamic_data->width,
                             windata->dynamic_data->height);
        }

        /* check for any change in the drawing */
        for (s=0; s<thisNumberOfSegments; s++)
        {
            recompute_angles = False;
            setRotationAngles(thisSegmentTable[s].rotation_sequence, thisSegmentTable[s].sarj_valid, &bg_angle, &sarj_angle, ja);

            /* rotate all the segment's vectors if the angle has changed */

            if ((bg_angle != 0.0) || (sarj_angle != 0.0))
            {
                rotationPointsAroundAxis(&thisSegmentTable[s].rotation_axis_head,
                                         &thisSegmentTable[s].rotation_axis_center,
                                         thisSegmentTable[s].number_of_vectors,
                                         thisSegmentTable[s].vector_array,
                                         thisSegmentTable[s].rotated_vectors[display],
                                         bg_angle, sarj_angle);
                recompute_angles = True;
            }
            else
            {
                for (p=0; p<thisSegmentTable[s].number_of_vectors; p++)
                {
                    thisSegmentTable[s].rotated_vectors[display][p]
                        = thisSegmentTable[s].vector_array[p];
                }
            }

            thisSegmentTable[s].current_angle_set [display][coord] = True;
            thisSegmentTable[s].current_bg_angle  [display][coord] = bg_angle;
            thisSegmentTable[s].current_sarj_angle[display][coord] = sarj_angle;

            /*
            ** if anything has changed, then
            ** convert vectors and window points to az/el
            */
            computeCoordinates(windata, display, coord_view, &thisSegmentTable[s]);
            drawSegment(windata, display, coord_view, s);
        }

        /* render all the pixmaps into the drawable */
        renderPixmaps2Drawable(windata, display, coord_view);
    }
}

/**
 * This function returns flag if the specified point is blocked by the
 * dynamic structure positions at the specified time.
 *
 * @param display current display type (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param width width of area
 * @param height height of area
 * @param point point to check
 * @param time_occurs time at data point
 *
 * @return 1 if point is blocked by a structure; 0 if not
 */
gint DD_PointBlocked(gint display, gint coord, gint width, gint height, Point *point, gchar *time_occurs )
{
    VECTOR *rotated_vectors;
    Point *coordinate_points;
    gdouble bg_angle;
    gdouble sarj_angle;
    guint s;
    gint p;
    gint v;
    gint i;
    gint count;
    gint x, y;
    JointAngle ja;
    gint found = False;
    Point cpt;

    if (thisNumberOfSegments == 0)    /* in case dynamics not loaded */
    {
        return False;
    }

    coordinate_points = (Point *)
                        MH_Calloc(sizeof(Point) * thisMaxVectorsPerPolygon, __FILE__, __LINE__);

    getAngleData(time_occurs, &ja);

    /*
    ** If these are s-band coordinates, then we need to convert
    ** from polar to a flat 2-d coor system for the GP_InsidePolygon
    ** function to work correctly
    */
    switch (coord)
    {
        case SBAND1:
        case SBAND2:
        case SBAND1_KUBAND:
        case SBAND2_KUBAND:
            /* convert cpt in sbAz/El to flat 2-D coord  */
            WG_PointToFlatCoord(coord,
                                width, height,
                                point->x, point->y, &(cpt.x), &(cpt.y), 1);
            break;

        default:
            cpt = *point;
            break;
    }

    for (s=0; (s<thisNumberOfSegments) && (! found); s++)
    {
        rotated_vectors = (VECTOR *)
                          MH_Calloc( sizeof(VECTOR) * thisSegmentTable[s].number_of_vectors, __FILE__, __LINE__);

        setRotationAngles(thisSegmentTable[s].rotation_sequence, thisSegmentTable[s].sarj_valid, &bg_angle, &sarj_angle, &ja);

        if (( bg_angle != 0.0 ) || ( sarj_angle != 0.0 ))
        {
            rotationPointsAroundAxis( &thisSegmentTable[s].rotation_axis_head,
                                      &thisSegmentTable[s].rotation_axis_center,
                                      thisSegmentTable[s].number_of_vectors,
                                      thisSegmentTable[s].vector_array,
                                      rotated_vectors,
                                      bg_angle, sarj_angle );
        }
        else
        {
            for (p=0; p<thisSegmentTable[s].number_of_vectors; p++)
            {
                rotated_vectors[p] = thisSegmentTable[s].vector_array[p];
            }
        }


        for (p=0; (p<thisSegmentTable[s].number_of_polygons) && (! found); p++)
        {
            count = thisSegmentTable[s].polygon_array[p].num_vectors;
            v = thisSegmentTable[s].polygon_array[p].first_vector;

            for (i = 0; i<count; i++)
            {
                VECTOR vC;
                VECTOR vu;

                VSUB(&rotated_vectors[v+i], &thisAntennaLocation[coord], &vC);
                VUNIT(&vC, &vu);
                switch (coord)
                {
                    case SBAND1:
                    case SBAND2:
                        /*
                        ** If these are s-band coordinates, then we need to convert
                        ** from polar to a flat 2-d coor system for the GP_InsidePolygon
                        ** function to work correctly
                        */
                        XYZtoSBandAE( vu.x, vu.y, vu.z, &x, &y );
                        WG_PointToFlatCoord(coord,
                                            width, height,
                                            x, y, &x, &y, 1);
                        break;

                    case KUBAND:
                    case KUBAND2:
                        XYZtoKuBand( vu.x, vu.y, vu.z, &x, &y, coord );
                        break;

                    case STATION:
                        XYZtoGenericAE( vu.x, vu.y, vu.z, &x, &y );
                        break;

                    default:
                        y = 0;
                        x = 0;
                        break;
                }

                coordinate_points[i].x = x;
                coordinate_points[i].y = y;
            }

            if (GP_InsidePolygon(cpt, coordinate_points, count))
            {
                found = True;
            }
        }

        MH_Free(rotated_vectors);
    }

    MH_Free(coordinate_points);

    return found;
}

/**
 * Sets the pointer array for the joint angles.
 *
 * @param jointAngleArray
 */
void DD_SetData(GPtrArray *jointAngleArray)
{
    thisJointAngleArray = jointAngleArray;
}

/**
 * Returns the pointer array of joint angle data
 *
 *
 * @return GPtrArray
 */
GPtrArray *DD_GetData(void)
{
    return thisJointAngleArray;
}

/**
 * Release the joint angle pointer array and its contents.
 *
 * @param ptrArray GPtrArray memory to release
 */
void DD_FreeJointAngleData(GPtrArray *ptrArray)
{
    if (ptrArray != NULL)
    {
        g_ptr_array_foreach(ptrArray,  deleteArrayData, NULL);
        g_ptr_array_free(ptrArray, False);
        ptrArray = NULL;
    }
}

/**
 * Does nothing; used only to invoke the destroy methods for the
 * key and data.
 *
 * @param key not used
 * @param value not used
 * @param user_data not used
 *
 * @return True - invokes destroy methods
 */
static gboolean hashTableForeach(void * key, void * value, void * user_data)
{
    return True;
}

/**
 * Releases the memory associated with the hash table data
 *
 * @param data RotationSequence
 */
static void deleteValue(void * data)
{
    RotationSequence *rotationSequence = (RotationSequence *)data;
    if (rotationSequence)
    {
        MH_Free(rotationSequence);
    }
}

/**
 * Loads the rotation sequence hash which allows for easier lookup.
 */
static void loadRotationSequenceHash(void)
{
    RotationSequence *rotationSequence;

    if (thisAngleSegmentsHash != NULL)
    {
        g_message("Removing AngleSegmentsHash");
        g_hash_table_foreach_remove( thisAngleSegmentsHash, hashTableForeach, NULL );
    }

    thisAngleSegmentsHash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, deleteValue);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, PORT_LABEL_2B);
    rotationSequence->angle = PORT_ANGLE_2B;
    g_hash_table_insert(thisAngleSegmentsHash, PORT_LABEL_2B, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, PORT_LABEL_4B);
    rotationSequence->angle = PORT_ANGLE_4B;
    g_hash_table_insert(thisAngleSegmentsHash, PORT_LABEL_4B, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, PORT_LABEL_2A);
    rotationSequence->angle = PORT_ANGLE_2A;
    g_hash_table_insert(thisAngleSegmentsHash, PORT_LABEL_2A, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, PORT_LABEL_4A);
    rotationSequence->angle = PORT_ANGLE_4A;
    g_hash_table_insert(thisAngleSegmentsHash, PORT_LABEL_4A, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, "PA");
    rotationSequence->angle = PORT_ANGLE_PA;
    g_hash_table_insert(thisAngleSegmentsHash, "PA", rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, "PTCS");
    rotationSequence->angle = PORT_ANGLE_PTCS;
    g_hash_table_insert(thisAngleSegmentsHash, "PTCS", rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, "SA");
    rotationSequence->angle = STBD_ANGLE_SA;
    g_hash_table_insert(thisAngleSegmentsHash, "SA", rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, STBD_LABEL_1A);
    rotationSequence->angle = STBD_ANGLE_1A;
    g_hash_table_insert(thisAngleSegmentsHash, STBD_LABEL_1A, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, STBD_LABEL_3A);
    rotationSequence->angle = STBD_ANGLE_3A;
    g_hash_table_insert(thisAngleSegmentsHash, STBD_LABEL_3A, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, STBD_LABEL_1B);
    rotationSequence->angle = STBD_ANGLE_1B;
    g_hash_table_insert(thisAngleSegmentsHash, STBD_LABEL_1B, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, STBD_LABEL_3B);
    rotationSequence->angle = STBD_ANGLE_3B;
    g_hash_table_insert(thisAngleSegmentsHash, STBD_LABEL_3B, rotationSequence);

    rotationSequence = (RotationSequence *)MH_Calloc(sizeof(RotationSequence),  __FILE__,  __LINE__);
    g_stpcpy(rotationSequence->angleName, "STCS");
    rotationSequence->angle = STBD_ANGLE_STCS;
    g_hash_table_insert(thisAngleSegmentsHash, "STCS", rotationSequence);
}

/**
 * Sets the rotation_sequence. Determines the rotation seqeuence from the
 * given string, then sets the equivalent integer value.
 *
 * @param seg current segment array
 * @param rotation_sequence_str which segment
 */
static void setRotationSequence(SEGMENT *seg, gchar *rotation_sequence_str)
{
    /* make the key without the asterisk */
    gchar **key = g_strsplit(rotation_sequence_str, "*", 0);
    /* get the rotation sequence */
    RotationSequence *rotationSequence = (RotationSequence *)g_hash_table_lookup(thisAngleSegmentsHash, key[0]);
    if (rotationSequence != NULL)
    {
        /* set the sequence */
        seg->rotation_sequence = rotationSequence->angle;
    }
    g_strfreev(key);

    /* determine if the sarj is valid or not */
    if (strspn("*",rotation_sequence_str) == 1)
    {
        seg->sarj_valid = False;
    }
    else
    {
        seg->sarj_valid = True;
    }
}

/**
 * This method sets the bg and sarj angles based on the rotation sequence
 *
 * @param rotation_sequence rotation sequence
 * @param sarj_valid sarj valid flag
 * @param bg_angle bg angle value
 * @param sarj_angle sarj angle value
 * @param ja joint angle array
 */
static void setRotationAngles(gint rotation_sequence, gboolean sarj_valid, gdouble *bg_angle, gdouble *sarj_angle, JointAngle *ja)
{
    switch (rotation_sequence)
    {
        default:
        case ANGLE_UNKNOWN:
            *bg_angle = 0.0;
            *sarj_angle = 0.0;
            break;

        case PORT_ANGLE_2B:
            *bg_angle = ja->angleValue[PORT_ANGLE_2B];
            *sarj_angle = ja->angleValue[PORT_ANGLE_PA];
            break;

        case PORT_ANGLE_4B:
            *bg_angle = ja->angleValue[PORT_ANGLE_4B];
            *sarj_angle = ja->angleValue[PORT_ANGLE_PA];
            break;

        case PORT_ANGLE_2A:
            *bg_angle = ja->angleValue[PORT_ANGLE_2A];
            *sarj_angle = ja->angleValue[PORT_ANGLE_PA];
            break;

        case PORT_ANGLE_4A:
            *bg_angle = ja->angleValue[PORT_ANGLE_4A];
            *sarj_angle = ja->angleValue[PORT_ANGLE_PA];
            break;

        case PORT_ANGLE_PA:
            *bg_angle = 0.0;
            *sarj_angle = ja->angleValue[PORT_ANGLE_PA];
            break;

        case PORT_ANGLE_PTCS:
            *bg_angle = ja->angleValue[PORT_ANGLE_PTCS];
            *sarj_angle = 0.0;
            break;

        case STBD_ANGLE_SA:
            *bg_angle = 0.0;
            *sarj_angle = ja->angleValue[STBD_ANGLE_SA];
            break;

        case STBD_ANGLE_1A:
            *bg_angle = ja->angleValue[STBD_ANGLE_1A];
            *sarj_angle = ja->angleValue[STBD_ANGLE_SA];
            break;

        case STBD_ANGLE_3A:
            *bg_angle = ja->angleValue[STBD_ANGLE_3A];
            *sarj_angle = ja->angleValue[STBD_ANGLE_SA];
            break;

        case STBD_ANGLE_1B:
            *bg_angle = ja->angleValue[STBD_ANGLE_1B];
            *sarj_angle = ja->angleValue[STBD_ANGLE_SA];
            break;

        case STBD_ANGLE_3B:
            *bg_angle = ja->angleValue[STBD_ANGLE_3B];
            *sarj_angle = ja->angleValue[STBD_ANGLE_SA];
            break;

        case STBD_ANGLE_STCS:
            *bg_angle = ja->angleValue[STBD_ANGLE_STCS];
            *sarj_angle = 0.0;
            break;
    }

    if (sarj_valid == False)
    {
        *sarj_angle = 0.0;
    }
}

/**
 * Loads string names associated with the joint angles.
 *
 * @param ja joint angle array
 */
static void setJointAngleLabels(JointAngle *ja)
{
    g_stpcpy(ja->angleLabel[PORT_ANGLE_2B], PORT_LABEL_2B);
    g_stpcpy(ja->angleLabel[PORT_ANGLE_4B], PORT_LABEL_4B);
    g_stpcpy(ja->angleLabel[PORT_ANGLE_2A], PORT_LABEL_2A);
    g_stpcpy(ja->angleLabel[PORT_ANGLE_4A], PORT_LABEL_4A);
    g_stpcpy(ja->angleLabel[PORT_ANGLE_PA], PORT_LABEL_PA);
    g_stpcpy(ja->angleLabel[PORT_ANGLE_PTCS], PORT_LABEL_PTCS);

    g_stpcpy(ja->angleLabel[STBD_ANGLE_SA], STBD_LABEL_SA);
    g_stpcpy(ja->angleLabel[STBD_ANGLE_1A], STBD_LABEL_1A);
    g_stpcpy(ja->angleLabel[STBD_ANGLE_3A], STBD_LABEL_3A);
    g_stpcpy(ja->angleLabel[STBD_ANGLE_1B], STBD_LABEL_1B);
    g_stpcpy(ja->angleLabel[STBD_ANGLE_3B], STBD_LABEL_3B);
    g_stpcpy(ja->angleLabel[STBD_ANGLE_STCS], STBD_LABEL_STCS);
}

/**
 * This function returns the joint angle at the specified time.
 *
 * @param entry_time time entry
 * @param ja joint angle array
 *
 * @return All of the ISS structural joint angles.
 */
static gboolean getAngleData(gchar *entry_time, JointAngle *ja)
{
    static guint current_entry = 0;
    JointAngle *jointAngle;
    guint i;
    gint start;
    gint found = False;
    glong search_sec;
    glong sec1;
    glong sec2;

    /* clear the target buffer */
    for (i=0; i<MAX_ANGLES; i++)
    {
        ja->angleValue[i] = 0.0;
    }

    /* validate the time and joint angle array */
    if (entry_time == NULL || thisJointAngleArray == NULL)
    {
        return False;
    }

    /* find starting position */
    start = findStartingPosition(current_entry, entry_time);

    /* convert time to utc format for comparing */
    search_sec = StrToSecs(TS_UTC, entry_time);

    /* locate angle list entry for specified time */
    for (i=start; i<thisJointAngleArray->len; i++)
    {
        /* get joint angle from array */
        jointAngle = (JointAngle *)g_ptr_array_index(thisJointAngleArray, i);

        /* search for exact match */
        if (g_ascii_strcasecmp(jointAngle->timeStr, entry_time) == 0)
        {
            memcpy(ja,  jointAngle,  sizeof(JointAngle));
            found = True;
            break;
        }

        /* find nearest match */
        if (g_ascii_strcasecmp(jointAngle->timeStr, entry_time) > 0)
        {
            /* time not exact, pick closest point */
            if (i > 0)
            {
                JointAngle *jointAnglePrev = (JointAngle *)g_ptr_array_index(thisJointAngleArray, i-1);

                sec1 = StrToSecs(TS_UTC, jointAnglePrev->timeStr);
                sec2 = StrToSecs(TS_UTC, jointAngle->timeStr);
                if ((search_sec - sec1) <= (sec2 - search_sec))
                {
                    memcpy(ja, jointAnglePrev, sizeof(JointAngle));
                    found = True;
                    break;
                }
            }
        }
    }

    /* always set the labels - the memcpy above wipes them out, so reset them here */
    setJointAngleLabels(ja);

    /* if not found, reset the entry start value*/
    if (found == False)
    {
        current_entry = 0;
    }

    return found;
}


/**
 * Searches the joint angle array for an entry that is less than
 * or equal to the entry time.
 *
 * @param current_entry
 * @param entry_time
 *
 * @return gint start index
 */
static gint findStartingPosition(guint current_entry, gchar *entry_time)
{
    gint start = 0;

    /* validate the start value */
    if ((current_entry >= 0) &&
        (thisJointAngleArray != NULL) &&
        (thisJointAngleArray->len > current_entry))
    {
        JointAngle *jointAngle = (JointAngle *)g_ptr_array_index(thisJointAngleArray, current_entry);
        if (g_ascii_strcasecmp(jointAngle->timeStr, entry_time) <= 0)
        {
            /* return this value */
            start = current_entry;
        }
    }
    else
    {
        /* start from beginning */
        start = 0;
    }

    return start;
}

/**
 * Deletes the JointAngle data from the pointer array.
 *
 * @param data
 * @param user_data
 */
static void deleteArrayData(gpointer data, gpointer user_data)
{
    JointAngle *jointAngle = (JointAngle *)data;
    MH_Free(jointAngle);
}

/**
 * This function computes a set of vectors from the specified original vectors
 * using the specified rotation angle.
 *
 * @param head_of_axis axis head
 * @param tail_of_axis axis tail
 * @param num_vectors number of vectors
 * @param old_vectors previous vector array
 * @param new_vectors new vector array
 * @param bg_angle_in_degrees bg angle in degrees
 * @param sarj_angle_in_degrees sarj angle in degrees
 */
static void rotationPointsAroundAxis( VECTOR  *head_of_axis,
                                      VECTOR  *tail_of_axis,
                                      gint    num_vectors,
                                      VECTOR  *old_vectors,
                                      VECTOR  *new_vectors,
                                      gdouble bg_angle_in_degrees,
                                      gdouble sarj_angle_in_degrees )
{
    gint i;
    VECTOR final_rotation_axis;
    VECTOR unit_x_prime_axis;
    VECTOR unit_y_prime_axis;
    VECTOR unit_z_prime_axis;
    VECTOR general_delta_vector;
    VECTOR temp_head_vector;
    VECTOR temp_tail_vector;
    VECTOR temp_old_vectors;

    VECTOR delta_vector;
    VECTOR position_of_prime_origin;

    VECTOR point_minus_tail;
    VECTOR old_point;
    VECTOR new_point_in_prime;
    VECTOR new_point_in_VFB;

    TRANSFORMATION_MATRIX  trans_prime_to_VFB;

    gdouble bg_angle_in_radians;
    gdouble sarj_angle_in_radians;
    gdouble ssarj;  /* sine of the SARJ angle */
    gdouble csarj;  /* cosine of the SARJ angle */
    gdouble magnitude_perpendicular_distance;

    if (sarj_angle_in_degrees != 0.0)
    {
        if (tail_of_axis->y < 0.0)
        {
            sarj_angle_in_degrees = 360.0  - sarj_angle_in_degrees;
        }
        sarj_angle_in_radians = DEGREES_TO_RADIANS(sarj_angle_in_degrees);
        ssarj = sin(sarj_angle_in_radians);
        csarj = cos(sarj_angle_in_radians);

        temp_head_vector.x = (head_of_axis->x * csarj) + (head_of_axis->z * ssarj);
        temp_head_vector.y = head_of_axis->y;
        temp_head_vector.z = (head_of_axis->x * -ssarj) + (head_of_axis->z * csarj);

        temp_tail_vector.x = (tail_of_axis->x * csarj) + (tail_of_axis->z * ssarj);
        temp_tail_vector.y = tail_of_axis->y;
        temp_tail_vector.z = (tail_of_axis->x * -ssarj) + (tail_of_axis->z * csarj);

        head_of_axis = &temp_head_vector;
        tail_of_axis = &temp_tail_vector;

        VSUB(head_of_axis, tail_of_axis, &final_rotation_axis);
    }
    else
    {
        ssarj = 0.0;
        csarj = 1.0;
        VSUB(head_of_axis, tail_of_axis, &final_rotation_axis);
    }

    VUNIT(&final_rotation_axis, &unit_z_prime_axis);
    trans_prime_to_VFB.col_3 = unit_z_prime_axis;

    bg_angle_in_radians = DEGREES_TO_RADIANS(bg_angle_in_degrees);
    general_delta_vector.x = cos(bg_angle_in_radians) - 1.0;
    general_delta_vector.y = sin(bg_angle_in_radians);
    general_delta_vector.z = 0.0;

    if (sarj_angle_in_degrees != 0.0)
    {
        for (i=0; i<num_vectors; i++)
        {
            VECTOR vC;

            temp_old_vectors.x = ( old_vectors[i].x * csarj) + (old_vectors[i].z * ssarj);
            temp_old_vectors.y = old_vectors[i].y;
            temp_old_vectors.z = (old_vectors[i].x * -ssarj) + (old_vectors[i].z * csarj);

            VSUB(&temp_old_vectors, tail_of_axis, &point_minus_tail);
            VCONST(VDOT(&point_minus_tail, &unit_z_prime_axis), &unit_z_prime_axis, &vC);
            VADD(tail_of_axis, &vC, &position_of_prime_origin);
            VCROSS(&unit_z_prime_axis, &point_minus_tail, &vC);
            VUNIT(&vC, &unit_y_prime_axis);
            trans_prime_to_VFB.col_2 = unit_y_prime_axis;

            VCROSS(&unit_y_prime_axis, &unit_z_prime_axis, &vC);
            VUNIT(&vC, &unit_x_prime_axis);
            trans_prime_to_VFB.col_1 = unit_x_prime_axis;

            VCROSS(&unit_z_prime_axis, &point_minus_tail, &vC);
            magnitude_perpendicular_distance = VMAG(&vC);

            VCONST(magnitude_perpendicular_distance, &general_delta_vector, &delta_vector);
            VSET(&old_point, magnitude_perpendicular_distance, 0.0, 0.0);
            VADD(&old_point, &delta_vector, &new_point_in_prime);
            TRANSFORM_VECTOR(&trans_prime_to_VFB, &new_point_in_prime, &new_point_in_VFB);
            VADD(&new_point_in_VFB, &position_of_prime_origin, &new_vectors[i]);
        }
    }
    else
    {
        for (i=0; i<num_vectors; i++)
        {
            VECTOR vC;

            temp_old_vectors = old_vectors[i];

            VSUB(&temp_old_vectors, tail_of_axis, &point_minus_tail);
            VCONST(VDOT(&point_minus_tail, &unit_z_prime_axis), &unit_z_prime_axis, &vC);
            VADD(tail_of_axis, &vC, &position_of_prime_origin);
            VCROSS(&unit_z_prime_axis, &point_minus_tail, &vC);
            VUNIT(&vC, &unit_y_prime_axis);
            trans_prime_to_VFB.col_2 = unit_y_prime_axis;

            VCROSS(&unit_y_prime_axis, &unit_z_prime_axis, &vC);
            VUNIT(&vC, &unit_x_prime_axis);
            trans_prime_to_VFB.col_1 = unit_x_prime_axis;

            VCROSS(&unit_z_prime_axis, &point_minus_tail, &vC);
            magnitude_perpendicular_distance = VMAG(&vC);

            VCONST(magnitude_perpendicular_distance, &general_delta_vector, &delta_vector);
            VSET(&old_point, magnitude_perpendicular_distance, 0.0, 0.0);
            VADD(&old_point, &delta_vector, &new_point_in_prime);
            TRANSFORM_VECTOR(&trans_prime_to_VFB, &new_point_in_prime, &new_point_in_VFB);
            VADD(&new_point_in_VFB, &position_of_prime_origin, &new_vectors[i]);
        }
    }
}


/**
 * This function converts the vectors in a segments polygons to the
 * equivalent Az/El coordinates
 *
 * @param windata the window data
 * @param display current display (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param seg segment
 */
static void computeCoordinates(WindowData *windata, gint display, gint coord_view, SEGMENT *seg)
{
    gint coord = windata->coordinate_type;
    switch (coord)
    {
        case SBAND1:
            computeSband1Coords(windata, display, coord_view, seg);
            break;

        case SBAND2:
            computeSband2Coords(windata, display, coord_view, seg);
            break;

        case KUBAND:
        case KUBAND2:
            computeKubandCoords(windata, display, coord_view, seg, coord);
            break;

        case STATION:
            computeStationCoords(windata, display, coord_view, seg);
            break;

        default:
            break;
    }
}

/**
 * Computes the vector for the sband1 segment
 *
 * @param windata the window data
 * @param display current display (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param seg segment
 */
static void computeSband1Coords(WindowData *windata, gint display, gint coord_view, SEGMENT *seg)
{
    gint p, v, i;
    gint x, y;
    gint not_in_range = 0;


    for (p=0; p<seg->number_of_polygons; p++)
    {
        v = seg->polygon_array[p].first_vector;

        /*
        ** At the start of processing for each polygon, make
        ** sure that decisions made about previous polygons
        ** don't impact processing of the current polygon. Do
        ** this my setting not_in_range to "0".
        */
        not_in_range = 0;
        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            VECTOR vu;
            VECTOR vC;

            VSUB(&seg->rotated_vectors[display][v+i], &thisAntennaLocation[SBAND1], &vC);
            VUNIT(&vC, &vu);

            if (XYZtoSBandAE(vu.x, vu.y, vu.z,
                             &seg->coordinate_points[display][SBAND1][v+i].x,
                             &seg->coordinate_points[display][SBAND1][v+i].y))
            {
                /*
                ** We must perform XYZtoSBandAE for every vertex of the
                ** polygon so that we can make decisions about how to
                ** process the polygon.
                */
            }
            else
            {
                /*
                ** Keep track of how many vertices are outside the
                ** boundary of the display because if all of them are
                ** outside then we will not draw the polygon at all.
                ** Do this by incrementing "not_in_range".
                */
                ++not_in_range;
            }
        }

        /*
        ** If none of the vertices in the polygon are inside the
        ** field-of-view of the display, then don't draw it at
        ** all.  Do this by setting the "draw_it" flag to False
        ** So, if "not_in_range" is greater than or equal to the
        ** number of vectors in the polygon, then none of the
        ** polygon is inside the field-of-view.
        */
        if (not_in_range >= seg->polygon_array[p].num_vectors)
        {
            seg->polygon_array[p].draw_it = False;
        }

        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            if (seg->polygon_array[p].draw_it)
            {
                WG_PointToWindow(SBAND1,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 seg->coordinate_points[display][SBAND1][v+i].x,
                                 seg->coordinate_points[display][SBAND1][v+i].y,
                                 &x, &y);

                seg->window_points[display][coord_view][v+i].x = x;
                seg->window_points[display][coord_view][v+i].y = y;
            }
        }
    }
}

/**
 * Computes the vector for the sband2 segment
 *
 * @param windata the window data
 * @param display current display (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param seg segment
 */
static void computeSband2Coords(WindowData *windata, gint display, gint coord_view, SEGMENT *seg)
{
    gint p, v, i;
    gint x, y;
    gint not_in_range = 0;

    for (p=0; p<seg->number_of_polygons; p++)
    {
        v = seg->polygon_array[p].first_vector;

        /*
        ** At the start of processing for each polygon, make
        ** sure that decisions made about previous polygons
        ** don't impact processing of the current polygon. Do
        ** this my setting not_in_range to "0".
        */
        not_in_range = 0;
        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            VECTOR vC;
            VECTOR vu;

            VSUB(&seg->rotated_vectors[display][v+i], &thisAntennaLocation[SBAND2], &vC);
            VUNIT(&vC, &vu);

            if (XYZtoSBandAE(vu.x, vu.y, vu.z,
                             &seg->coordinate_points[display][SBAND2][v+i].x,
                             &seg->coordinate_points[display][SBAND2][v+i].y))
            {
                /*
                ** We must perform XYZtoSBandAE for every vertex of the
                ** polygon so that we can make decisions about how to
                ** process the polygon.
                */
            }
            else
            {
                /*
                ** Keep track of how many vertices are outside the
                ** boundary of the display because if all of them are
                ** outside then we will not draw the polygon at all.
                ** Do this by incrementing "not_in_range".
                */
                ++not_in_range;
            }
        }

        /*
        ** If none of the vertices in the polygon are inside the
        ** field-of-view of the display, then don't draw it at
        ** all.  Do this by setting the "draw_it" flag to False.
        ** So, if "not_in_range" is greater than or equal to the
        ** number of vectors in the polygon, then none of the
        ** polygon is inside the field-of-view.
        */
        if (not_in_range >= seg->polygon_array[p].num_vectors)
        {
            seg->polygon_array[p].draw_it = False;
        }

        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            if (seg->polygon_array[p].draw_it)
            {
                WG_PointToWindow(SBAND2,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 seg->coordinate_points[display][SBAND2][v+i].x,
                                 seg->coordinate_points[display][SBAND2][v+i].y,
                                 &x, &y);

                seg->window_points[display][coord_view][v+i].x = x;
                seg->window_points[display][coord_view][v+i].y = y;
            }
        }
    }
}

/**
 * Computes the vector for the kuband segment
 *
 * @param windata the window data
 * @param display current display (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param seg segment
 */
static void computeKubandCoords(WindowData *windata, gint display, gint coord_view, SEGMENT *seg, gint coord)
{
    gint p, v, i;
    gint x, y;
    gint CropPolygon = False;
    gint not_in_range = 0;
    for (p=0; p<seg->number_of_polygons; p++)
    {
        /*
        ** At the start of processing for each polygon, make
        ** sure that decisions made about previous polygons
        ** don't impact processing of the current polygon. Do
        ** this my setting CropPolygon to "False" and
        ** not_in_range to "0".
        */
        CropPolygon = False;
        not_in_range = 0;

        /*
        ** Set the pointer to the start of the polygon by
        ** storing the number of the first vector in the
        ** array of vectors that are associated with this
        ** polygon.
        */
        v = seg->polygon_array[p].first_vector;

        /*
        ** Now go through the polygon, one vector at at time
        ** and decide how to process it.
        */
        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            VECTOR vC;
            VECTOR vu;

            /* Convert the vector to a unit vector. */
            VSUB(&seg->rotated_vectors[display][v+i], &thisAntennaLocation[coord], &vC);
            VUNIT(&vC, &vu);

            /*
            ** Convert the unit vector to the elevation and
            ** cross-elevation of the Ku-band coordinate
            ** system.  This function returns a True/False
            ** indicator, "in_range" that tells us whether
            ** the vector points to a place inside the range
            ** of the display, or outside of it.
            ** True = in range, False = out of range.
            */
            if (XYZtoKuBand(vu.x, vu.y, vu.z,
                            &seg->coordinate_points[display][coord][v+i].x,
                            &seg->coordinate_points[display][coord][v+i].y, coord))
            {
                /*
                ** We must perform XYZtoKuBand for every vertex of the
                ** polygon so that we can make decisions about how to
                ** process the polygon.
                */
            }
            else
            {
                /*
                ** If we are here, then One of the vertices of the
                ** polygon are outside the boundary of the display, so
                ** we have to crop it.  Do this by setting the
                ** CropPolygon flag to "True" to signal additional
                ** processing
                */
                CropPolygon = True;

                /*
                ** Keep track of how many vertices are outside the
                ** boundary of the display because if all of them are
                ** outside then we will not draw the polygon at all.
                ** Do this by incrementing "not_in_range".
                */
                ++not_in_range;

            }
            /*
            ** At this point, we are finished with all the initial processing
            ** of the polygon.  We looked at every vector, converted it to
            ** Ku-band coordinates, and checked to see if it was inside or
            ** outside of the display.  We decided whether to keep the whole
            ** polygon, crop it, or ignore it.
            */
        }

        /*
        ** If none of the vertices in the polygon are inside the
        ** field-of-view of the display, then don't draw it at
        ** all.  Do this by setting the "draw_it" flag to False.
        ** Also, since we are not going to draw it at all, set
        ** the CropPolygon flag to False so that we don't try to
        ** crop the polygon, either.
        ** So, if "not_in_range" is greater than or equal to the
        ** number of vectors in the polygon, then none of the
        ** polygon is inside the field-of-view.
        */
        if (not_in_range >= seg->polygon_array[p].num_vectors)
        {
            seg->polygon_array[p].draw_it = False;
            CropPolygon = False;
        }

#ifdef GIGO
        /* If we decided to crop the polygon. */
        if ((CropPolygon) && (seg->polygon_array[p].draw_it))
        {
            crop_polygon(coord_view, display, seg, p, v);
        }
#endif

        /*
        ** Now go through the polygon and compute the window coordinates
        ** for each vertex.
        */
        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            if (seg->polygon_array[p].draw_it)
            {
                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 seg->coordinate_points[display][coord][v+i].x,
                                 seg->coordinate_points[display][coord][v+i].y,
                                 &x, &y);

                seg->window_points[display][coord_view][v+i].x = x;
                seg->window_points[display][coord_view][v+i].y = y;
            }
        }
    }
}

/**
 * Computes the vector for the station segment
 *
 * @param windata the window data
 * @param display current display (REALTIME or PREDICT)
 * @param coord_view current coordinate view
 * @param seg segment
 */
static void computeStationCoords(WindowData *windata, gint display, gint coord_view, SEGMENT *seg)
{
    gint p, v, i;
    gint x, y;

    for (p=0; p<seg->number_of_polygons; p++)
    {
        v = seg->polygon_array[p].first_vector;

        for (i=0; i<seg->polygon_array[p].num_vectors; i++)
        {
            VECTOR vC;
            VECTOR vu;

            VSUB(&seg->rotated_vectors[display][v+i], &thisAntennaLocation[STATION], &vC);
            VUNIT(&vC, &vu);

            XYZtoGenericAE(vu.x, vu.y, vu.z,
                           &seg->coordinate_points[display][STATION][v+i].x,
                           &seg->coordinate_points[display][STATION][v+i].y);

            WG_PointToWindow(STATION,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height,
                             seg->coordinate_points[display][STATION][v+i].x,
                             seg->coordinate_points[display][STATION][v+i].y,
                             &x, &y);

            seg->window_points[display][coord_view][v+i].x = x;
            seg->window_points[display][coord_view][v+i].y = y;
        }
    }
}

/**
 * Draws the specified segment into it's pixmap. This pixmap is
 * eventually rendered into the main dynamic_data drawable.
 *
 * @param windata
 * @param display
 * @param coord_view
 * @param segment
 */
static void drawSegment(WindowData *windata, gint display, gint coord_view, gint segment)
{
    gboolean pixmap_alloc = AM_AllocatePixmapData(windata, &thisAnglePixmaps[display][coord_view][segment], False);
    GdkColor *color = CH_GetGdkColorByName(thisStructureColor);
    if (color != NULL)
    {
        guint p;
        GdkGC *gc;
        GdkGCValues _values;
        GdkColor gcolor;
        GdkDrawable *drawable = GDK_DRAWABLE(thisAnglePixmaps[display][coord_view][segment].pixmap);

        /**
         ** do we need to clear the drawable ?
         ** if allocation did not occur, then the pixmap was not cleared
         ** consequently we need to clear it before writing to it.
         */
        if (pixmap_alloc == False)
        {
            AM_ClearDrawable(drawable,
                             windata->drawing_area->style->black_gc,
                             0, 0,
                             windata->mask_data->width,
                             windata->mask_data->height);
        }

        gcolor.red = color->red;
        gcolor.green = color->green;
        gcolor.blue = color->blue;
        gcolor.pixel = color->pixel;
        _values.foreground = gcolor;
        gc = gdk_gc_new_with_values(drawable, &_values, GDK_GC_FOREGROUND);

#ifdef G_OS_WIN32
        /* need handle */
        HDC hdc = gdk_win32_hdc_get(drawable, gc, GDK_GC_FOREGROUND);

        /* create a null pen, this eliminates the spider-web effect in the polygon */
        HPEN hpen = CreatePen(PS_NULL, 1, RGB(0,0,0));
        if (hpen != NULL)
        {
            SelectObject(hdc, hpen);
        }
#endif

        for (p=0; p<(guint)thisSegmentTable[segment].number_of_polygons; p++)
        {
            if (thisSegmentTable[segment].polygon_array[p].draw_it)
            {
                gint i;
                gint v = thisSegmentTable[segment].polygon_array[p].first_vector;
                gint count = thisSegmentTable[segment].polygon_array[p].num_vectors;

                /* copy the polygon */
                for (i=0; i<count; i++)
                {
                    thisPolygonData[i] = thisSegmentTable[segment].window_points[display][coord_view][v+i];
                }

                /* close the polygon */
                thisPolygonData[count] = thisPolygonData[0];
            }
            else
            {
                /*
                ** Now that you are through with this flag,
                ** set it back to True so that if the
                ** polygon moves back into the display
                ** field of view after a new alpha-beta
                ** rotation, it will be drawn at that time.
                */
                thisSegmentTable[segment].polygon_array[p].draw_it = True;
            }
#ifdef G_OS_UNIX
            gdk_draw_polygon(drawable,
                             gc,
                             True,
                             thisPolygonData,
                             thisSegmentTable[segment].polygon_array[p].num_vectors+1);
#else
            Polygon(hdc,
                    (POINT*)thisPolygonData,
                    thisSegmentTable[segment].polygon_array[p].num_vectors+1);
#endif
        }

#ifdef G_OS_WIN32
        if (hpen != NULL)
        {
            /* release the pen */
            DeleteObject(hpen);
        }
        /* release the hdc, otherwise the gc gets messed up */
        gdk_win32_hdc_release((GdkDrawable *)drawable, gc, GDK_GC_FOREGROUND);
#endif
        gdk_gc_unref(gc);
    }
}

/**
 * Renders all individual segments into the dynamic drawable.
 *
 * @param windata has the dynamic drawable
 * @param display which display
 * @param coord_view which view
 */
static void renderPixmaps2Drawable(WindowData *windata, gint display, gint coord_view)
{
    /* get a gc to work with, use the primary drawing area's window */
    GdkGC *gc;
    GdkGCValues _values;
    guint s;

    _values.function = GDK_OR;
    gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

    for (s=0; s<thisNumberOfSegments; s++)
    {
        if (thisAnglePixmaps[display][coord_view][s].pixmap != NULL)
        {
            gdk_draw_drawable(GDK_DRAWABLE(windata->dynamic_data->pixmap),
                              gc,
                              thisAnglePixmaps[display][coord_view][s].pixmap,
                              0, 0,  /* dst x, y */
                              0, 0,  /* src x, y */
                              -1, -1 /* src width, height, use ALL */ );
        }
    }

    /* done with the gc */
    g_object_unref(gc);

    /* render dynamics to drawable */
    if (windata->dynamic_data->pixmap != NULL)
    {
        AM_CopyPixmap(windata, windata->dynamic_data->pixmap);
    }
}
