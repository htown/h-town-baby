/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "Conversions.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "MessageHandler.h"
#include "Vector.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/Conversions.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static TRANSFORMATION_MATRIX thisSBandStaticBiasMat;
static TRANSFORMATION_MATRIX thisKBandStaticBiasMat;
static TRANSFORMATION_MATRIX thisKBand2StaticBiasMat;
static TRANSFORMATION_MATRIX thisGenericStaticBiasMat;
/*************************************************************************/

/**
 * This function load the Ku-Band, S-Band, and Generic static bias
 * matricies.
 *
 * @return True or False
 */
gboolean LoadStaticBiasMats(void)
{
    /* Load S-Band static bias matrix */
    if (loadBiasMats("S_BAND_STATIC_BIAS_MATIX_ROW1",
                     &thisSBandStaticBiasMat.col_1.x,
                     &thisSBandStaticBiasMat.col_2.x,
                     &thisSBandStaticBiasMat.col_3.x) == False)
    {
        return False;
    }

    if (loadBiasMats("S_BAND_STATIC_BIAS_MATIX_ROW2",
                     &thisSBandStaticBiasMat.col_1.y,
                     &thisSBandStaticBiasMat.col_2.y,
                     &thisSBandStaticBiasMat.col_3.y) == False)
    {
        return False;
    }

    if (loadBiasMats("S_BAND_STATIC_BIAS_MATIX_ROW3",
                     &thisSBandStaticBiasMat.col_1.z,
                     &thisSBandStaticBiasMat.col_2.z,
                     &thisSBandStaticBiasMat.col_3.z) == False)
    {
        return False;
    }

    /* Load Ku-Band static bias matrix */
    if (loadBiasMats("K_BAND_STATIC_BIAS_MATIX_ROW1",
                     &thisKBandStaticBiasMat.col_1.x,
                     &thisKBandStaticBiasMat.col_2.x,
                     &thisKBandStaticBiasMat.col_3.x) == False)
    {
        return False;
    }

    if (loadBiasMats("K_BAND_STATIC_BIAS_MATIX_ROW2",
                     &thisKBandStaticBiasMat.col_1.y,
                     &thisKBandStaticBiasMat.col_2.y,
                     &thisKBandStaticBiasMat.col_3.y) == False)
    {
        return False;
    }

    if (loadBiasMats("K_BAND_STATIC_BIAS_MATIX_ROW3",
                     &thisKBandStaticBiasMat.col_1.z,
                     &thisKBandStaticBiasMat.col_2.z,
                     &thisKBandStaticBiasMat.col_3.z) == False)
    {
        return False;
    }

    /* Load Ku-Band 2 static bias matrix */
    if (loadBiasMats("K_BAND_2_STATIC_BIAS_MATIX_ROW1",
                     &thisKBand2StaticBiasMat.col_1.x,
                     &thisKBand2StaticBiasMat.col_2.x,
                     &thisKBand2StaticBiasMat.col_3.x) == False)
    {
        return False;
    }

    if (loadBiasMats("K_BAND_2_STATIC_BIAS_MATIX_ROW2",
                     &thisKBand2StaticBiasMat.col_1.y,
                     &thisKBand2StaticBiasMat.col_2.y,
                     &thisKBand2StaticBiasMat.col_3.y) == False)
    {
        return False;
    }

    if (loadBiasMats("K_BAND_2_STATIC_BIAS_MATIX_ROW3",
                     &thisKBand2StaticBiasMat.col_1.z,
                     &thisKBand2StaticBiasMat.col_2.z,
                     &thisKBand2StaticBiasMat.col_3.z) == False)
    {
        return False;
    }

    /* Load Generic static bias matrix */
    if (loadBiasMats("GENERIC_STATIC_BIAS_MATIX_ROW1",
                     &thisGenericStaticBiasMat.col_1.x,
                     &thisGenericStaticBiasMat.col_2.x,
                     &thisGenericStaticBiasMat.col_3.x) == False)
    {
        return False;
    }

    if (loadBiasMats("GENERIC_STATIC_BIAS_MATIX_ROW2",
                     &thisGenericStaticBiasMat.col_1.y,
                     &thisGenericStaticBiasMat.col_2.y,
                     &thisGenericStaticBiasMat.col_3.y) == False)
    {
        return False;
    }

    if (loadBiasMats("GENERIC_STATIC_BIAS_MATIX_ROW3",
                     &thisGenericStaticBiasMat.col_1.z,
                     &thisGenericStaticBiasMat.col_2.z,
                     &thisGenericStaticBiasMat.col_3.z) == False)
    {
        return False;
    }

    return True;
}

/**
 * Does the actual load of the matrix transformations
 *
 * @param keyword string value used to search iam.xml file.
 * @param col1 x output data
 * @param col2 y output data
 * @param col3 z output data
 *
 * @return True or False
 */
static gboolean loadBiasMats(gchar *keyword, gdouble *col1, gdouble *col2, gdouble *col3)
{
    gchar data[256];
    gdouble r1, r2, r3;

    if (CP_GetConfigData(keyword, data) == False)
    {
        gchar message[128];
        g_sprintf(message, "%s configuration variable error!\n"
                "Dynamic Structures not generated.", keyword);
        ErrorMessage(AM_GetActiveDisplay(), "File Error", message);
        return False;
    }
    else
    {
        if (sscanf( data, "%lf %lf %lf", &r1, &r2, &r3 ) != 3)
        {
            gchar message[128];
            g_sprintf(message, "Configuration parameter %s is not formatted correctly!", keyword);
            ErrorMessage(AM_GetActiveDisplay(), "Data Error", message);
            return False;
        }
        else
        {
            *col1 = r1;
            *col2 = r2;
            *col3 = r3;
        }
    }

    return True;
}

/**
 * This function converts a set of station azimuth / elevation coordinates
 * to the equivalent set of S-Band azimuth / elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param az azimuth value
 * @param el elevation value
 * @param saz output azimuth
 * @param sel output elevation
 *
 * @return sband ae
 */
gint StationAEtoSBandAE(gint az, gint el, gint *saz, gint *sel)
{
    gdouble x, y, z;

    StationAEtoXYZ( az, el, &x, &y, &z );
    return XYZtoSBandAE( x, y, z, saz, sel );
}


/**
 * This function converts a set of station azimuth / elevation coordinates
 * to the equivalent set of Ku-Band elevation / cross-elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param saz azimuth
 * @param sel elevation
 * @param kxel output kuband x elevation
 * @param kel output kuband elevation
 *
 * @return kuband kel
 */
gint StationAEtoKuBand(gint saz, gint sel, gint *kxel, gint *kel, gint coord)
{
    gdouble x, y, z;

    StationAEtoXYZ( saz, sel, &x, &y, &z );
    return XYZtoKuBand( x, y, z, kxel, kel, coord );
}

/**
 * This function converts a set of station azimuth / elevation coordinates
 * to the equivalent set of generic antenna azimuth/elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param saz azimuth
 * @param sel elevation
 * @param kxel output kuband x elevation
 * @param kel output kuband elevation
 *
 * @return kuband kel
 */
gint StationAEtoGeneric(gint az, gint el, gint *saz, gint *sel)
{
    gdouble x, y, z;

    StationAEtoXYZ( az, el, &x, &y, &z );
    return XYZtoGenericAE( x, y, z, saz, sel );
}

/**
 * This function converts an XYZ unit vector to the equivalent set of
 * S-Band azimuth / elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *        Input x, y, z are in station coordinate frame
 *
 * @param x x input
 * @param y y input
 * @param z z input
 * @param saz output azimuth
 * @param sel output elevation
 *
 * @return sband ae
 */
gint XYZtoSBandAE(gdouble x, gdouble y, gdouble z, gint *saz, gint *sel)
{
    VECTOR pointingVec;
    VECTOR sBandPointingVec;
    gdouble raz, rel;

    pointingVec.x = x;
    pointingVec.y = y;
    pointingVec.z = z;

    /* convert pointing vector in station coordinate frame to
       S-Band coordinate frame */
    TransformVector(&thisSBandStaticBiasMat, &pointingVec, &sBandPointingVec);

    /* Normalize and clean up vector so that rounding errors do
       not cause problems later with the trig functions
    */
    VUNIT(&sBandPointingVec, &sBandPointingVec);

    if (fabs(sBandPointingVec.x) < EPSILON)
    {
        sBandPointingVec.x = 0;
    }

    if (fabs(sBandPointingVec.y) < EPSILON)
    {
        sBandPointingVec.y = 0;
    }

    if (fabs(sBandPointingVec.z) < EPSILON)
    {
        sBandPointingVec.z = 0;
    }

    rel = acos( -1.0 * sBandPointingVec.z );

    if (sBandPointingVec.x == 0.0 && sBandPointingVec.y == 0.0)
    {
        raz = 0.0;
    }
    else
    {
        raz = atan2( sBandPointingVec.y, sBandPointingVec.x );
    }

    *saz = (gint) ROUND(RADIANS_TO_DEGREES(raz) * 10.0);
    *sel = (gint) ROUND(RADIANS_TO_DEGREES(rel) * 10.0);

    return(*sel * .1 <= MAX_POLAR_RADIUS);
}


/**
 * This function converts an XYZ unit vector to the equivalent set of
 * Ku-Band elevation / cross-elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param x x input
 * @param y y input
 * @param z z input
 * @param kxel kuband x elevation
 * @param kel kuband elevation
 *
 * @return kuband kel
 */
gint XYZtoKuBand(gdouble x, gdouble y, gdouble z, gint *kxel, gint *kel, gint coord)
{
    VECTOR pointingVec, kBandPointingVec;
    gdouble rel, rxel, xzs, sqrt_xzs;

    pointingVec.x = x;
    pointingVec.y = y;
    pointingVec.z = z;

    /* convert pointing vector in station coordinate frame to
       Ku-Band coordinate frame */
    if (coord == KUBAND)
        TransformVector(&thisKBandStaticBiasMat, &pointingVec, &kBandPointingVec);
    else
        TransformVector(&thisKBand2StaticBiasMat, &pointingVec, &kBandPointingVec);

    xzs = kBandPointingVec.x * kBandPointingVec.x +
          kBandPointingVec.z * kBandPointingVec.z;
    sqrt_xzs = sqrt(xzs);
    if (xzs < EPSILON)
    {
        xzs = EPSILON;
    }

    if (sqrt_xzs > 0.0)
    {
        if (kBandPointingVec.x >= 0)
        {
            rel = acos(-1.0 * kBandPointingVec.z / sqrt_xzs);
        }
        else
        {
            rel = -1.0 * acos(-1.0 * kBandPointingVec.z / sqrt_xzs);
        }

        rxel = atan(kBandPointingVec.y / sqrt_xzs);

        *kxel = (gint) ROUND(RADIANS_TO_DEGREES(rxel) * 10.0);
        *kel  = (gint) ROUND(RADIANS_TO_DEGREES(rel) * 10.0);

        return
            (abs(*kxel) <= MAX_KU_CROSS_ELEVATION * 10) &&
            (abs(*kel) <= MAX_KU_ELEVATION * 10);
    }

    return 0;
}
/**
 * This function converts an XYZ unit vector to the equivalent set of
 * Station azimuth / elevation coordinates.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param x x input
 * @param y y input
 * @param z z input
 * @param saz output azimuth
 * @param sel output elevation
 *
 * @return station az/el
 */
void XYZtoStationAE(gdouble x, gdouble y, gdouble z, gint *saz, gint *sel)
{
    gdouble raz, rel;

    raz = (y == 0.0) && (z == 0.0) ? 0.0 : atan2( 1 * y,  -1 * z );
    rel = asin( -1 * x );

    *saz = (gint) ROUND(RADIANS_TO_DEGREES(raz) * -10.0);
    *sel = (gint) ROUND(RADIANS_TO_DEGREES(rel) * -10.0);
}


/**
 * This function converts an XYZ unit vector to the equivalent set of
 * Station azimuth / elevation coordinates in the generic coordinate.
 * frame.
 * NOTE:  all values are in tenths of degrees.
 *
 * @param x x input
 * @param y y input
 * @param z z input
 * @param saz output azimuth
 * @param sel output elevation
 *
 * @return station az/el
 */
gint XYZtoGenericAE(gdouble x, gdouble y, gdouble z, gint *saz, gint *sel)
{
    VECTOR pointingVec, genericPointingVec;
    gdouble raz, rel;

    /* convert pointing vector in station coordinate frame to
       Station AZ/EL coordinate frame */

    pointingVec.x = x;
    pointingVec.y = y;
    pointingVec.z = z;

    TransformVector(&thisGenericStaticBiasMat, &pointingVec, &genericPointingVec);

    raz = (genericPointingVec.y == 0.0) && (genericPointingVec.z == 0.0) ? 0.0 : atan2( 1 * genericPointingVec.y,  -1 * genericPointingVec.z );
    rel = asin( -1 * genericPointingVec.x );

    *saz = (gint) ROUND(RADIANS_TO_DEGREES(raz) * -10.0);
    *sel = (gint) ROUND(RADIANS_TO_DEGREES(rel) * -10.0);

    return 1;
}

/**
 * This function converts a set of station azimuth / elevation coordinates
 * to the equivalent [ x y z ] body vector
 * NOTE:  all values are in tenths of degrees.
 *
 * @param az input azimuth
 * @param el input elevation
 * @param x output x
 * @param y output y
 * @param z output z
 */
static void StationAEtoXYZ(gint az, gint el, gdouble *x, gdouble *y, gdouble *z)
{
    gdouble raz, rel;

    rel  = -1 * DEGREES_TO_RADIANS((gdouble) el / 10.0);
    raz  = -1 * DEGREES_TO_RADIANS((gdouble) az / 10.0);

    *x = (el%1800 == 0)  ? 0.0 : -1 * sin(rel);
    *y = (abs((gint)el) == 900) || (az%1800 == 0)   ? 0.0 : cos(rel) * sin(raz);
    *z = (abs((gint)az) == 900) || (abs((gint)el) == 900) ? 0.0 : -1 * cos(rel) * cos(raz);
}

/**
 * Converts S-Band Az/El to a station body vector
 *
 * @param x x input sband azimuth in tenths of degrees
 * @param y y input sband elevation in tenths of degrees
 * @param sx output x
 * @param sy output y
 * @param sz output z
 */
void SBandAEtoStationXYZ(gint x, gint y, gdouble *sx, gdouble *sy, gdouble *sz)
{
    TRANSFORMATION_MATRIX tSBandtoStation;
    VECTOR pointingVec;
    VECTOR sBandPointingVec;
    gdouble az, el;

    /* convert tenths of degrees to radians */
    az = DEGREES_TO_RADIANS((gdouble) x / 10.0);
    el = DEGREES_TO_RADIANS((gdouble) y / 10.0);

    /* compute S-Band coordinate pointing vector */
    sBandPointingVec.x = (abs((gint)x) == 900) || (y%1800 == 0)   ? 0.0 : sin(el) * cos(az);
    sBandPointingVec.y = (x%1800 == 0) || (y%1800 == 0)  ? 0.0 : sin(el) * sin(az);
    sBandPointingVec.z = (abs((gint)y) == 900) ? 0.0 : -1.0 * cos(el);

    /* compute transpose of thisSBandStaticBiasMat */
    tSBandtoStation.col_1.x = thisSBandStaticBiasMat.col_1.x;
    tSBandtoStation.col_2.x = thisSBandStaticBiasMat.col_1.y;
    tSBandtoStation.col_3.x = thisSBandStaticBiasMat.col_1.z;

    tSBandtoStation.col_1.y = thisSBandStaticBiasMat.col_2.x;
    tSBandtoStation.col_2.y = thisSBandStaticBiasMat.col_2.y;
    tSBandtoStation.col_3.y = thisSBandStaticBiasMat.col_2.z;

    tSBandtoStation.col_1.z = thisSBandStaticBiasMat.col_3.x;
    tSBandtoStation.col_2.z = thisSBandStaticBiasMat.col_3.y;
    tSBandtoStation.col_3.z = thisSBandStaticBiasMat.col_3.z;

    /* convert pointing vector in S-Band coordinate frame to
      station coordinate frame */
    TransformVector(&tSBandtoStation, &sBandPointingVec, &pointingVec);

    /* Normalize and clean up vector so that rounding errors do
       not cause problems later with the trig functions
    */
    VUNIT(&pointingVec, &pointingVec);

    if (fabs(pointingVec.x) < EPSILON)
    {
        pointingVec.x = 0;
    }
    if (fabs(pointingVec.y) < EPSILON)
    {
        pointingVec.y = 0;
    }
    if (fabs(pointingVec.z) < EPSILON)
    {
        pointingVec.z = 0;
    }


    *sx = pointingVec.x;
    *sy = pointingVec.y;
    *sz = pointingVec.z;
}
