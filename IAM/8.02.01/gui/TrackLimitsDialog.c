/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "TrackLimitsDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "RealtimeDataHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/TrackLimitsDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget        *thisDialog             = NULL;
static GtkWidget        *thisTrackLimitEntry    = NULL;
static gint              thisTrackLimit          = LD_DEFAULT_LIMIT;
/*************************************************************************/

/**
 * Creates the track limit dialog. This dialog is used to
 * set a time boundary for the tdrs tracks.
 *
 * @param parent the window that owns this dialog
 */
void TrackLimit_Create(GtkWindow *parent)
{
    GtkWidget *hbox;
    GtkWidget *label;

    if (thisDialog == NULL)
    {
        thisDialog = gtk_dialog_new_with_buttons("Set Track Limits",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(responseCB), NULL);

        /* create box for time label and entries */
        hbox = gtk_hbox_new(False, 0);

        label = gtk_label_new("Maximum Duration: ");

        gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 1);

        /* create time entry field and add to the dialog */
        thisTrackLimitEntry = gtk_entry_new();
        gtk_entry_set_max_length(GTK_ENTRY(thisTrackLimitEntry), 3);
        gtk_entry_set_width_chars(GTK_ENTRY(thisTrackLimitEntry), 4);
        gtk_box_pack_start(GTK_BOX(hbox), thisTrackLimitEntry, False, False, 1);

        label = gtk_label_new("min");
        gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 1);

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), hbox);

    }
}

/**
 * This function pops up the track limit dialog
 */
void TrackLimit_Open(void)
{
    gchar output[4];

    if ((thisDialog != NULL) && (thisTrackLimitEntry != NULL))
    {
        g_sprintf(output, "%d", thisTrackLimit);
        gtk_entry_set_text(GTK_ENTRY(thisTrackLimitEntry), output);
        gtk_widget_show_all(thisDialog);
    }
}

/**
 * This method handles the button events
 *
 * @param widget calling widget
 * @param response_id action that got us here
 * @param data user data
 */
static void responseCB(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;
    gchar *trackLimitPtr;
    gchar output[4];
    gint trackLimit = 0;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            {
                /*-----------------------------*/
                /* OK pushed;  check new value */
                /*-----------------------------*/
                trackLimitPtr = (gchar*)gtk_entry_get_text(GTK_ENTRY(thisTrackLimitEntry));
                sscanf(trackLimitPtr, "%d", &trackLimit);

                if ((trackLimit >= 5) && (trackLimit <= 120))
                {
                    /*----------------------------------------------*/
                    /* New value is valid;  set in real-time object */
                    /*----------------------------------------------*/
                    thisTrackLimit = trackLimit;
                    RTDH_UpdateLimits( thisTrackLimit );
                }
                else
                {
                    /*-------------------------------------------------------------*/
                    /* New value is not valid;  reset old value, leaving dialog up */
                    /*-------------------------------------------------------------*/
                    gdk_beep();
                    g_sprintf(output, "%d", thisTrackLimit);
                    gtk_entry_set_text(GTK_ENTRY(thisTrackLimitEntry), output);
                    doUnmanage = FALSE;
                }
            }
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}
