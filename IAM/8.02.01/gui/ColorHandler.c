/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <unistd.h>
#endif

#define PRIVATE
#include "ColorHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "MemoryHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/ColorHandler.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");



/**************************************************************************
* Private Data Definitions
**************************************************************************/
/**
 * The following list of default colors must match the enumeration
 * defined in the header file.
 * modeled after the isp stcolors.c
 */
static gchar *defaultColors[GC_NUMCOLORS] =
{
    "Black",
    "White",
    "Red",
    "Maroon",
    "Pink",
    "OrangeRed",
    "Orange",
    "DarkOrange",
    "Yellow",
    "Gold",
    "YellowGreen",
    "Green",
    "PaleGreen",
    "SpringGreen",
    "Turquoise",
    "Chartreuse",
    "SeaGreen",
    "ForestGreen",
    "Blue",
    "Cyan",
    "LightBlue",
    "SkyBlue",
    "MidnightBlue",
    "NavyBlue",
    "CornflowerBlue",
    "SteelBlue",
    "SlateBlue",
    "CadetBlue",
    "Violet",
    "Purple",
    "Magenta",
    "Lavender",
    "VioletRed",
    "Brown",
    "RosyBrown",
    "IndianRed",
    "Sienna",
    "Beige",
    "Wheat",
    "DimGray",
    "LightGray",
    "Gray",
    "DarkSlateGray",
    "Gray50"
};

static GdkColormap      *thisColormap           = NULL;
static GHashTable       *thisColorTable         = NULL;
static GPtrArray        *thisColorArray         = NULL;
/*************************************************************************/

/**
 * Load the color table
 */
void CH_Constructor(void)
{
    if (thisColorTable == NULL)
    {
        /* need hash for lookup via string name */
        thisColorTable = g_hash_table_new_full(g_str_hash, g_str_equal,
                                               deleteHashTableKey,
                                               deleteHashTableValue);
        /* need array for lookup via index */
        thisColorArray = g_ptr_array_new();

        /* get system colormap */
        thisColormap = gdk_colormap_get_system ();
    }
}

/**
 * release allocated memory
 */
void CH_Destructor(void)
{
    if (thisColorTable != NULL)
    {
        g_message("Removing thisColorTable hash");
        g_hash_table_foreach_remove(thisColorTable, hashTableForeach, NULL);
    }

    if (thisColorArray != NULL)
    {
        g_ptr_array_free(thisColorArray, False);
    }
}

/**
 * Interface that allows the callee to add a new color
 * to the color table specified by the color_name provided
 *
 * @param color_name use this name for this color
 * @param gcolor color data
 *
 * @return True or False
 */
gboolean CH_AddColor(gchar *color_name, GdkColor *gcolor)
{
    /* color name must be valid, colormap must be allocated */
    if (color_name == NULL || thisColormap == NULL)
    {
        return False;
    }

    /* does the color name already exist? */
    if (isColor(color_name) == False)
    {
        /* allocate the color */
        if (gdk_colormap_alloc_color(thisColormap, gcolor, False, True) == True)
        {
            /* add color to our table */
            if (addColor(color_name, gcolor) != NULL)
            {
                return True;
            }
        }
    }

    return False;
}

/**
 * Gets a color based on the string name. If its a new color, the color is
 * loaded into the color table at the color index
 *
 * @param color_name the name for this color
 * @param gdk_color if null, then allocate color based on name only
 *
 * @return colortable entry or NULL
 */
static ColorEntry *addColor(gchar *color_name, GdkColor *gdk_color)
{
    gint rc;
    gchar *key;
    GdkColor gcolor;
    ColorEntry  *color_entry;

    /* add color to our table */
    color_entry = (ColorEntry *)MH_Calloc(sizeof(ColorEntry), __FILE__, __LINE__);
    g_stpcpy(color_entry->name, color_name);

    if (gdk_color == NULL)
    {
        rc = gdk_color_parse(color_name, &gcolor);
        if (rc == 0)
        {
            /* not found */
            MH_Free(color_entry);
            return NULL;
        }

        if (thisColormap == NULL)
        {
            /* color map allocation error */
            MH_Free(color_entry);
            return NULL;
        }

        rc = gdk_colormap_alloc_color(thisColormap, &gcolor, False, True);
        if (rc == 0)
        {
            /* color allocation error */
            MH_Free(color_entry);
            return NULL;
        }

        gdk_colormap_query_color(thisColormap, gcolor.pixel, &gcolor);

        color_entry->color = gdk_color_copy(&gcolor);
    }
    else
    {
        color_entry->color = gdk_color_copy(gdk_color);
    }

    key = g_ascii_strdown(color_name, -1);
    g_hash_table_insert(thisColorTable, key, color_entry);
    g_ptr_array_add(thisColorArray, color_entry);

    return color_entry;
}

/**
 * Adds a new gdk color instance to the color table
 *
 * @param gcolor add color
 *
 * @return NULL or ColorTable instance
 */
static ColorEntry *addGdkColor(GdkColor *gcolor)
{
    static gchar *user_def_color_name = "USER_DEF_COLOR_";
    static gint  user_def_color_index = 0;
    gchar color_name[32];
    ColorEntry *color_entry;

    g_sprintf(color_name, "%s%d", user_def_color_name, user_def_color_index++);
    color_entry = addColor(color_name, gcolor);

    return color_entry;
}

/**
 * returns the color index in our local color table
 *
 * @param color_name look for this color
 *
 * @return color index or invalid index value
 */
gint CH_GetColor(gchar *color_name)
{
    gint colorNumber = GC_INVALIDCOLOR;

    if (color_name == NULL)
    {
        return GC_INVALIDCOLOR;
    }

    /**
     * Load default colors into the color table if not loaded yet
     */
    if (thisColorTable == NULL)
    {
        CH_Constructor();
    }

    /**
     * Is color is table
     */
    if (isColor(color_name) == False)
    {
        if (addColor(color_name, NULL) != NULL)
        {
            colorNumber = getColorIndex(color_name);
        }
    }
    else
    {
        colorNumber = getColorIndex(color_name);
    }

    return colorNumber;
}

/**
 * returns the color index in our local color table
 *
 * @param colorIndex look for this color
 *
 * @return color name of color
 */
gchar *CH_GetColorName(gint colorIndex)
{
    gchar *key;
    ColorEntry *color_entry = NULL;

    /**
     * Load default colors into the color table if not loaded yet
     */
    if (thisColorTable == NULL)
    {
        CH_Constructor();
    }

    if (colorIndex >= GC_FIRSTCOLOR && colorIndex < GC_LASTCOLOR)
    {
        key = g_ascii_strdown(defaultColors[colorIndex], -1);
        color_entry = (ColorEntry *)g_hash_table_lookup(thisColorTable, key);
        g_free(key);
    }

    if (color_entry == NULL)
    {
        if (colorIndex >= GC_FIRSTCOLOR && colorIndex < GC_LASTCOLOR)
        {
            color_entry = addColor(defaultColors[colorIndex], NULL);
        }
    }

    if (color_entry != NULL)
    {
        return color_entry->name;
    }

    /* not found, return "white" for no good reason */
    return "white";
}

/**
 * returns a new gc set with the color specified
 *
 * @param widget get graphics context from this widget
 * @param color_name look for this color
 *
 * @return dst_gc with foreground color set
 */
GdkGC *CH_SetColorGC(GtkWidget *widget, gchar *color_name)
{
    gchar *key;
    GdkGC *dst_gc = NULL;
    GdkColor *color = NULL;
    GdkColor gcolor;
    ColorEntry *color_entry;

    /**
     * Load default colors into the color table if not loaded yet
     */
    if (thisColorTable == NULL)
    {
        CH_Constructor();
    }

    /**
     * Find color in the color table
     */
    key = g_ascii_strdown(color_name, -1);
    color_entry = (ColorEntry *)g_hash_table_lookup(thisColorTable, key);
    if (color_entry == NULL)
    {
        color_entry = addColor(color_name, NULL);
        if (color_entry != NULL)
        {
            color = color_entry->color;
        }
    }
    else
    {
        color = color_entry->color;
    }
    g_free(key);

    if (color != NULL)
    {
        GdkGCValues _values;
        gcolor.red = color->red;
        gcolor.green = color->green;
        gcolor.blue = color->blue;
        gcolor.pixel = color->pixel;
        _values.foreground = gcolor;

        dst_gc = gdk_gc_new_with_values(widget->window, &_values, GDK_GC_FOREGROUND);
    }

    return dst_gc;
}

/**
 * Returns the gdk color for the given index
 *
 * @param colorIndex index into color table
 *
 * @return GdkColor or null
 */
GdkColor *CH_GetGdkColorByIndex(gint colorIndex)
{
    gchar *key;
    ColorEntry *color_entry = NULL;

    if (colorIndex >= GC_FIRSTCOLOR && colorIndex < GC_LASTCOLOR)
    {
        key = g_ascii_strdown(defaultColors[colorIndex], -1);
        color_entry = (ColorEntry *)g_hash_table_lookup(thisColorTable, key);
        g_free(key);
    }

    if (color_entry == NULL)
    {
        if (colorIndex >= GC_FIRSTCOLOR && colorIndex < GC_LASTCOLOR)
        {
            color_entry = addColor(defaultColors[colorIndex], NULL);
        }
    }

    if (color_entry != NULL)
    {
        return color_entry->color;
    }

    return NULL;
}

/**
 * Returns the gdk color entry by name
 *
 * @param color_name color name to lookup into color table
 *
 * @return color or NULL if not found
 */
GdkColor *CH_GetGdkColorByName(gchar *color_name)
{
    gchar *key;
    ColorEntry *color_entry;

    if (color_name == NULL)
    {
        return NULL;
    }

    /**
     * Load default colors into the color table if not loaded yet
     */
    if (thisColorTable == NULL)
    {
        CH_Constructor();
    }

    key = g_ascii_strdown(color_name, -1);
    color_entry = (ColorEntry *)g_hash_table_lookup(thisColorTable, key);
    g_free(key);

    if (color_entry == NULL)
    {
        color_entry = addColor(color_name, NULL);
    }

    if (color_entry != NULL)
    {
        return color_entry->color;
    }

    return NULL;
}

/**
 * Tries to find the color by the rgb value
 *
 * @param gcolor get color by color specs
 *
 * @return name or NULL
 */
gchar *CH_GetColorNameByGdkColor(GdkColor *gcolor)
{
    guint i;
    ColorEntry *color_entry;

    for (i=0; i<thisColorArray->len; i++)
    {
        color_entry = (ColorEntry *)g_ptr_array_index(thisColorArray, i);
        if (color_entry != NULL)
        {
            if (gcolor->red   == color_entry->color->red &&
                gcolor->green == color_entry->color->green &&
                gcolor->blue  == color_entry->color->blue)
            {
                return color_entry->name;
            }
        }
    }

    color_entry = addGdkColor(gcolor);
    if (color_entry != NULL)
    {
        return color_entry->name;
    }

    return NULL;
}

/**
 * Tries to find the color specified in the color table
 *
 * @param color_name check this color
 *
 * @return True or False
 */
static gboolean isColor(gchar *color_name)
{
    gchar *key = g_ascii_strdown(color_name, -1);
    ColorEntry *color_entry = (ColorEntry *)g_hash_table_lookup(thisColorTable, key);
    g_free(key);

    if (color_entry != NULL)
    {
        return True;
    }

    return False;
}

/**
 * Based on the name, find the corresponding color index (GC_ value).
 *
 * @param color_name color to search for
 *
 * @return color index or GC_INVALIDCOLOR
 */
static gint getColorIndex(gchar *color_name)
{
    guint i;

    for (i=GC_FIRSTCOLOR; i<GC_LASTCOLOR; i++)
    {
        if (g_strcasecmp(defaultColors[i], color_name) == 0)
        {
            return i;
        }
    }

    return GC_INVALIDCOLOR;
}

/**
 * Does nothing; used only to invoke the destroy methods for the
 * key and data
 *
 * @param key not used
 * @param value not used
 * @param user_data not used
 *
 * @return True - invokes the destry methods
 */
static gboolean hashTableForeach(void * key, void * value, void * user_data)
{
    return True;
}

/**
 * Releases the memory associated with the hash table key
 *
 * @param data free this data
 */
static void deleteHashTableKey(void * data)
{
    g_free(data);
}

/**
 * Release the memory assoicated with the hash table data
 *
 * @param data color entry data
 */
static void deleteHashTableValue(void * data)
{
    ColorEntry *color_entry = (ColorEntry *)data;
    if (color_entry->color)
    {
        gdk_color_free(color_entry->color);
        MH_Free(color_entry);
        color_entry = NULL;
    }
}
