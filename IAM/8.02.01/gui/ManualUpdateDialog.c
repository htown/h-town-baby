/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "ManualUpdateDialog.h"
#undef PRIVATE

#include "ConfigParser.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "TimeStrings.h"
#include "XML_Parse.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/ManualUpdateDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget    *thisDialog     = NULL;
static GtkWidget    *thisStartEntry = NULL;
static GtkWidget    *thisStopEntry  = NULL;

static gchar         thisStartTime[TIME_STR_LEN] = {UNSET_TIME};
static gchar         thisStopTime [TIME_STR_LEN] = {UNSET_TIME};
/*************************************************************************/

/**
 * This method creates the update dialog
 *
 * @param parent attach dialog to this window
 */
void MUD_Create(GtkWindow *parent)
{
    if (thisDialog == NULL)
    {
        GtkWidget *vbox;
        GtkWidget *hbox;
        GtkWidget *startLabel;
        GtkWidget *stopLabel;

        thisDialog = gtk_dialog_new_with_buttons("Start Manual Update",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                 GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK(gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(responseCb), NULL);

        /* create box for the horizontal boxes */
        vbox = gtk_vbox_new(True, 5);

        /* create box for time label and entries */
        hbox = gtk_hbox_new(False, 5);

        /* add the horizontal box to the vertical box */
        gtk_box_pack_start(GTK_BOX(vbox), hbox, True, True, 5);

        startLabel = gtk_label_new("Start Time");
        gtk_box_pack_start(GTK_BOX(hbox), startLabel, True, True, 5);

        thisStartEntry = gtk_entry_new();
        g_signal_connect(thisStartEntry, "key-press-event",
                         G_CALLBACK(keyPressCb),
                         NULL);
        gtk_entry_set_max_length(GTK_ENTRY(thisStartEntry), TIME_STR_LEN);
        gtk_box_pack_start(GTK_BOX(hbox), thisStartEntry, True, True, 5);

        hbox = gtk_hbox_new(False, 5);

        /* add the horizontal box to the vertical box */
        gtk_box_pack_start(GTK_BOX(vbox), hbox, True, True, 5);

        stopLabel = gtk_label_new("Stop Time");
        gtk_box_pack_start(GTK_BOX(hbox), stopLabel, True, True, 5);

        thisStopEntry = gtk_entry_new();
        g_signal_connect(thisStopEntry, "key-press-event",
                         G_CALLBACK(keyPressCb),
                         NULL);
        gtk_entry_set_max_length(GTK_ENTRY(thisStopEntry), TIME_STR_LEN);
        gtk_box_pack_start(GTK_BOX(hbox), thisStopEntry, True, True, 5);

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), vbox);
    }
}

/**
 * This method opens the dialog for viewing
 */
void MUD_Open(void)
{
    if (thisDialog != NULL)
    {
        /* initialize the times */
        g_stpcpy(thisStartTime, UNSET_TIME);
        g_stpcpy(thisStopTime, UNSET_TIME);

        if (MUD_GenerateTime(thisStartTime, thisStopTime) == True)
        {
            gtk_entry_set_text(GTK_ENTRY(thisStartEntry), thisStartTime);
            gtk_entry_set_text(GTK_ENTRY(thisStopEntry), thisStopTime);

            gtk_widget_show_all(thisDialog);
        }
        else
        {
            ErrorMessage(thisDialog, "Generate Time Error",
                         "Error occurred while trying to generating "
                         "the start and stop times!");
        }
    }
}

/**
 * Creates the time strings.
 *
 * @param start_time the start time
 * @param stop_time the stop time
 */
gboolean MUD_GenerateTime(gchar *start_time, gchar *stop_time)
{
    gboolean generate_time_status = False;
    glong sec1 = 0;

    if (ValidCurrentUtc())
    {
        sec1 = CurrentUtcSecs();
    }
    else
    {
        GList *currentPt = PDH_GetCurrentPt();
        if (currentPt != NULL)
        {
            sec1 = ((PredictPt *)currentPt->data)->timeSecs;
        }
    }

    /* load the start and stop times */
    if ((generate_time_status = loadAutoConfigData(start_time, stop_time)) == True)
    {
        gint time_type = AM_GetTimeType();
        SecsToStr(time_type, OffsetToSecs(start_time)+sec1, start_time);
        SecsToStr(time_type, OffsetToSecs(stop_time) +sec1, stop_time);
    }

    return generate_time_status;
}

/**
 * This function returns the start or stop time to be used.
 *
 * @param time_type which time UD_START or UD_STOP
 *
 * @return gchar*
 */
gchar *MUD_GetTime(glong time_type)
{
    if ((g_ascii_strcasecmp(thisStartTime, UNSET_TIME) == 0) ||
        (g_ascii_strcasecmp(thisStopTime,  UNSET_TIME) == 0))
    {
        gchar autoStart[TIME_STR_LEN];
        gchar autoStop [TIME_STR_LEN];
        glong sec1 = 0;

        if (loadAutoConfigData(autoStart, autoStop) == True)
        {
            if (ValidCurrentUtc())
            {
                sec1 = CurrentUtcSecs();
            }

            SecsToStr((gint)TS_UTC, OffsetToSecs(autoStart)+sec1, thisStartTime);
            SecsToStr((gint)TS_UTC, OffsetToSecs(autoStop)+sec1, thisStopTime);
        }
    }

    return(time_type == UD_START ? g_strdup(thisStartTime) : g_strdup(thisStopTime));
}

/**
 * Loads the auto config data. Sets the start and stop time if
 * the times are unset.
 *
 * @param start_time the start time
 * @param stop_time the stop time
 */
static gboolean loadAutoConfigData(gchar *start_time, gchar *stop_time)
{
    gchar time_str[TIME_STR_LEN];

    if (g_ascii_strcasecmp(start_time, UNSET_TIME) == 0)
    {
        if (CP_GetConfigData(IAM_AUTO_UPDATE_START, time_str) == False)
        {
            g_warning("IAM_AUTO_UPDATE_START not defined in the config file!");
            ErrorMessage(NULL, "Config Error", "IAM_AUTO_UPDATE_START not defined in the config file!");
            return False;
        }
        else
        {
            g_stpcpy(start_time, time_str);
        }
    }

    if (g_ascii_strcasecmp(stop_time, UNSET_TIME) == 0)
    {
        if (CP_GetConfigData(IAM_AUTO_UPDATE_STOP, time_str) == False)
        {
            g_warning("IAM_AUTO_UPDATE_STOP not defined in the config file!");
            ErrorMessage(NULL, "Config Error", "IAM_AUTO_UPDATE_STOP not defined in the config file!");
            return False;
        }
        else
        {
            g_stpcpy(stop_time, time_str);
        }
    }

    return True;
}

/**
 * This function validates that the start and stop time fields are correctly
 * formatted.
 *
 * @return True or False
 */
static gboolean validInputData(void)
{
    const gchar *start;
    const gchar *stop;

    gint valid = True;

    /* get the start and stop times */
    start = gtk_entry_get_text(GTK_ENTRY(thisStartEntry));
    stop = gtk_entry_get_text(GTK_ENTRY(thisStopEntry));

    /* if not valid, reload the original start time */
    if (! ValidTime(AM_GetTimeType(), (gchar*)start))
    {
        gtk_entry_set_text(GTK_ENTRY(thisStartEntry), thisStartTime);
        valid = False;
    }

    /* if not valid, reload the original stop time */
    if (! ValidTime(AM_GetTimeType(), (gchar*)stop))
    {
        gtk_entry_set_text(GTK_ENTRY(thisStopEntry), thisStopTime);
        valid = False;
    }

    /* make sure the stop time is after the start time */
    if (valid && (StrToSecs(AM_GetTimeType(), (gchar*)stop) <= StrToSecs(AM_GetTimeType(), (gchar*)start)))
        valid = False;

    if (! valid)
        gdk_beep();

    return valid;
}

/**
 * This method processes the button responses
 *
 * @param widget widget making call
 * @param response_id event that got us here
 * @param data user data
 */
static void responseCb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            if (validInputData() == True)
            {
                const gchar *pstr;

                pstr = gtk_entry_get_text(GTK_ENTRY(thisStartEntry));
                g_stpcpy(thisStartTime, pstr);
                pstr = gtk_entry_get_text(GTK_ENTRY(thisStopEntry));
                g_stpcpy(thisStopTime, pstr);

                /* use these times to parse the tdrs predict file */
                XML_SetStartTime(thisStartTime);
                XML_SetStopTime(thisStopTime);
                XML_ProcessData(TDRS_PREDICT);
            }
            else
                doUnmanage = False;
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean keyPressCb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return False;
    }

    gdk_beep();
    return True;
}
