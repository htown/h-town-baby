/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "RealtimeRFDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "Dialog.h"
#include "FontHandler.h"
#include "IspHandler.h"
#include "MemoryHandler.h"
#include "PredictDataHandler.h"
#include "RealtimeDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/RealtimeRFDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* optimum size based on 1280 X 1024 */
#define WINDOW_WIDTH    550
#define WINDOW_HEIGHT   780

static GtkWidget    *thisWindow     = NULL;
static GtkWidget    *thisNotebook   = NULL;

/**
 * coordinate type can only be SBAND or KUBAND
 */
static gint         thisCoordinateType = SBAND1;

/**
 * flag indicating that this dialog is iconified
 */
static gboolean     thisIsIconified = False;

/**
 * Resize font resource
 */
static ResizeFont   thisResizeFont;

/**
 * Define the realtime data and status fields
 */
static GtkWidget    *thisValue[IS_MaxNumberOfSymbols];
static GtkWidget    *thisStatus[IS_MaxNumberOfSymbols];

static gchar        thisStatusIndicator[IS_MaxNumberOfSymbols];

static GtkWidget    *thisSUN_Value  [MAX_TDRS_SLOT];
static GtkWidget    *thisSUN_Status [MAX_TDRS_SLOT];

static GtkWidget    *thisTDRE_Value [MAX_TDRS_SLOT];
static GtkWidget    *thisTDRE_Status[MAX_TDRS_SLOT];

static GtkWidget    *thisTDRW_Value [MAX_TDRS_SLOT];
static GtkWidget    *thisTDRW_Status[MAX_TDRS_SLOT];

#define SUN_SBAND_LABEL             "AZ Position:"
#define SUN_KUBAND_LABEL            "XEL Position:"
#define TDRS_SBAND_LABEL            "AZ:"
#define TDRS_KUBAND_LABEL           "XEL:"

static GtkWidget    *thisSunLabel   = NULL;
static GtkWidget    *thisSlot1Label = NULL;
static GtkWidget    *thisSlot2Label = NULL;
/*************************************************************************/

/**
 * This method will create the dialog for the realtime
 * data. If the dialog was previously created then
 * the creation is by passed.
 *
 * @return True if dialog created, otherwise False
 */
gboolean RTRFD_Create(void)
{
    gint i;

    if (thisWindow == NULL)
    {
        create_dialog();
        D_SetIcon(thisWindow);

        /* initialize the status indicator to something not used */
        for (i=0; i<IS_MaxNumberOfSymbols; i++)
        {
            thisStatusIndicator[i] = 'x';
        }
    }

    return(thisWindow ? True : False);
}

/**
 * Does the actual creation of the realtime data dialog
 */
static void create_dialog(void)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *notebook;
    GtkWidget *hbbox;
    GtkWidget *btn;

    GtkWidget *vpaned;
    GtkWidget *vpaned2;
    GtkWidget *vpaned3;

    Dimension *dim;

    thisWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(thisWindow), "IAM Realtime Data");
    gtk_widget_set_name(thisWindow, "Realtime-Data");

    /* create container for all the components */
    vbox = gtk_vbox_new(False, 0);
    gtk_container_add(GTK_CONTAINER(thisWindow), vbox);

    D_SetIcon(thisWindow);

    /* compute optimum window size */
    dim = AM_GetWindowSize(thisWindow, WINDOW_WIDTH, WINDOW_HEIGHT);
    gtk_window_set_default_size(GTK_WINDOW(thisWindow), dim->width, dim->height);
    gtk_widget_set_size_request(GTK_WIDGET(thisWindow), (gint)(dim->width*WINDOW_MIN_PERCENT), (gint)(dim->height*WINDOW_MIN_PERCENT));

    /* reset the resize font defaults */
    thisResizeFont.default_width = dim->width;
    thisResizeFont.default_height = dim->height;

    /* done with it */
    MH_Free(dim);

    gtk_container_set_border_width(GTK_CONTAINER(thisWindow), 0);

    /* handle window close event */
    g_signal_connect(thisWindow, "destroy", G_CALLBACK(gtk_widget_destroyed), &thisWindow);
    g_signal_connect(G_OBJECT(thisWindow), "delete-event", G_CALLBACK(delete_event_cb), NULL);

    /* handle iconification event */
    g_signal_connect(G_OBJECT(thisWindow), "window-state-event", G_CALLBACK(visibility_cb), NULL);

    /* handle resize event */
    g_signal_connect(G_OBJECT(thisWindow), "configure-event", G_CALLBACK(resize_cb), NULL);

    vpaned = gtk_vpaned_new();
    {
        /* top most components */
        hbox = create_top_data_form();
        gtk_paned_pack1(GTK_PANED(vpaned), hbox, False, True);

        /* tdrs slot #1 and #2 components */
        hbox = create_slot_form();
        gtk_paned_pack2(GTK_PANED(vpaned), hbox, False, True);
    }

    vpaned2 = gtk_vpaned_new();
    {
        /* previous pane */
        gtk_paned_pack1(GTK_PANED(vpaned2), vpaned, False, True);

        /* joint angle components */
        hbox = create_joint_angle_form();
        gtk_paned_pack2(GTK_PANED(vpaned2), hbox, False, True);
    }

    vpaned3 = gtk_vpaned_new();
    {
        /* previous pane */
        gtk_paned_pack1(GTK_PANED(vpaned3), vpaned2, False, True);

        /* notebook that has the sband and kuband components */
        notebook = create_data_temp_form();
        gtk_paned_pack2(GTK_PANED(vpaned3), notebook, False, True);
    }

    /* The dialog window is created with a vbox packed into it. */
    gtk_box_pack_start(GTK_BOX(vbox), vpaned3, True, True, 0);

    hbbox = gtk_hbutton_box_new();
    {
        GtkAccelGroup *accel_group;

        gtk_button_box_set_layout(GTK_BUTTON_BOX(hbbox), GTK_BUTTONBOX_END);
        gtk_box_set_spacing(GTK_BOX(hbbox), 5);

        btn = gtk_button_new_with_label("Close");
        gtk_box_pack_start(GTK_BOX(hbbox), btn, False, False, 2);

        /* add accelerator to button, so user can do <ctrl>c */
        /* to close the window */
        accel_group = gtk_accel_group_new();
        gtk_widget_add_accelerator(btn, "clicked", accel_group, GDK_c,
                                   GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

        g_signal_connect(btn, "clicked", G_CALLBACK(close_cb), NULL);

        gtk_window_add_accel_group(GTK_WINDOW(thisWindow), accel_group);
    }
    gtk_box_pack_start(GTK_BOX(vbox), hbbox, False, False, 5);

    /* initialize font resize instance */
    g_stpcpy(thisResizeFont.rc_string, IAM_GTKRC_REALTIME_DATA_FONT_SIZE);
    thisResizeFont.default_font_size = M_FONT_SIZE;
    thisResizeFont.previous_font_size = M_FONT_SIZE;
    thisResizeFont.default_width = WINDOW_WIDTH;
    thisResizeFont.previous_width = WINDOW_WIDTH;
    thisResizeFont.new_width = -1;
    thisResizeFont.default_height = WINDOW_HEIGHT;
    thisResizeFont.previous_height = WINDOW_HEIGHT;
    thisResizeFont.new_height = -1;
}

/**
 * Opens the window
 */
void RTRFD_Open(void)
{
    if (thisWindow != NULL)
    {
        if (thisIsIconified == True)
        {
            gtk_window_deiconify(GTK_WINDOW(thisWindow));
        }

        gtk_window_set_position(GTK_WINDOW(thisWindow), GTK_WIN_POS_CENTER);
        gtk_widget_show_all(thisWindow);
        gdk_window_raise(thisWindow->window);
    }
}

/**
 * Creates the top level gui components
 *
 * @return container that has top level components
 */
static GtkWidget *create_top_data_form(void)
{
    GtkWidget *frame;
    GtkWidget *hbox;

    hbox = gtk_hbox_new(True, 1);

    /* do TDRS Selected */
    frame = create_tdrs_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 1);

    /* do xel sun tracking */
    frame = create_sun_tracking_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 1);

    return hbox;
}

/**
 * This method builds the tdrs, rise/set components
 *
 * @return container that has tdrs components
 */
static GtkWidget *create_tdrs_form(void)
{
    GtkWidget *label;
    GtkWidget *vbox;
    GtkWidget *table;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(2, 4, False);
        { /* row 1 */
            label = D_CreateLabel("TDRS Selected:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_TdrsSelectFlag] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_TdrsSelectFlag], 1, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_TdrsSelectFlag] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_TdrsSelectFlag], 1, 4, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("Rise/Set Quality:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_RiseSetQuality] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_RiseSetQuality], 2, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_RiseSetQuality] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_RiseSetQuality], 2, 4, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Tdrs", vbox);
}

/**
 * This method builds the sun tracking components
 *
 * @return container that has sun components
 */
static GtkWidget *create_sun_tracking_form(void)
{
    GtkWidget *label;
    GtkWidget *vbox;
    GtkWidget *table;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(2, 5, False);
        { /* row 1 */
            thisSunLabel = D_CreateLabel(SUN_SBAND_LABEL, LEFT_ALIGN, D_FgColor());
            insert_table_item(table, thisSunLabel, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisSUN_Value[TDRS_CMD_AZ] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisSUN_Value[TDRS_CMD_AZ], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_FILL, GAO_NONE);

            thisSUN_Status[TDRS_CMD_AZ] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisSUN_Status[TDRS_CMD_AZ], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("EL Position:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisSUN_Value[TDRS_CMD_EL] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisSUN_Value[TDRS_CMD_EL], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_FILL, GAO_NONE);

            thisSUN_Status[TDRS_CMD_EL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisSUN_Status[TDRS_CMD_EL], 2, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, True, True, 0);
    }

    return D_CreateDataFrameWithLabel("Sun Tracking (deg)", vbox);
}

/**
 * This method creates the tdrs slot components. It calls slot1 and slot2.
 *
 * @return container that has tdrs components
 */
static GtkWidget *create_slot_form(void)
{
    GtkWidget *hbox;
    GtkWidget *frame;

    hbox = gtk_hbox_new(True, 1);

    frame = create_slot1();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = create_slot2();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    return hbox;
}

/**
 * This method builds the slot1 tdrs components
 *
 * @return container has first tdrs components
 */
static GtkWidget *create_slot1(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(1, 2, False);
        { /* row 1 */
            label = D_CreateLabel("TDRS ID:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_ID] = D_CreateLabel("0", LEFT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_ID], 1, 2, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(5, 6, False);
        { /* row 2 */
            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_EXPAND, GAO_NONE);

            label = D_CreateLabel("Pred'd", CENTER_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel("Cmd'd", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_BgColor());
            insert_table_item(table, label, 2, 5, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_BgColor());
            insert_table_item(table, label, 2, 6, GAO_FILL, GAO_NONE);
        }

        { /* row 3 */
            thisSlot1Label = D_CreateLabel(TDRS_SBAND_LABEL, LEFT_ALIGN, D_FgColor());
            insert_table_item(table, thisSlot1Label, 3, 1, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_PRED_AZ] = D_CreateLabel("99", CENTER_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_PRED_AZ], 3, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 3, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_CMD_AZ] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_CMD_AZ], 3, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 5, GAO_FILL, GAO_NONE);

            thisTDRE_Status[TDRS_CMD_AZ] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Status[TDRS_CMD_AZ], 3, 6, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("EL:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_PRED_EL] = D_CreateLabel("99", CENTER_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_PRED_EL], 4, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 3, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_CMD_EL] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_CMD_EL], 4, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 5, GAO_FILL, GAO_NONE);

            thisTDRE_Status[TDRS_CMD_EL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Status[TDRS_CMD_EL], 4, 6, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("AOS:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_AOS] = D_CreateLabel("*", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_AOS], 5, 2, GAO_FILL, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("LOS:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisTDRE_Value[TDRS_LOS] = D_CreateLabel("*", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRE_Value[TDRS_LOS], 6, 2, GAO_FILL, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Slot #1", vbox);
}

/**
 * This method builds the slot2 components
 *
 * @return container has second tdrs components
 */
static GtkWidget *create_slot2(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(1, 2, False);
        { /* row 1 */
            label = D_CreateLabel("TDRS ID:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_ID] = D_CreateLabel("0", LEFT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_ID], 1, 2, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(5, 6, False);
        { /* row 2 */
            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_EXPAND, GAO_NONE);

            label = D_CreateLabel("Pred'd", CENTER_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_BgColor());
            insert_table_item(table, label, 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel("Cmd'd", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_BgColor());
            insert_table_item(table, label, 2, 5, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 6, GAO_FILL, GAO_NONE);
        }

        { /* row 3 */
            thisSlot2Label = D_CreateLabel(TDRS_SBAND_LABEL, LEFT_ALIGN, D_FgColor());
            insert_table_item(table, thisSlot2Label, 3, 1, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_PRED_AZ] = D_CreateLabel("99", CENTER_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_PRED_AZ], 3, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 3, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_CMD_AZ] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_CMD_AZ], 3, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 5, GAO_FILL, GAO_NONE);

            thisTDRW_Status[TDRS_CMD_AZ] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Status[TDRS_CMD_AZ], 3, 6, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("EL:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_PRED_EL] = D_CreateLabel("99", CENTER_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_PRED_EL], 4, 2, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 3, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_CMD_EL] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_CMD_EL], 4, 4, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 5, GAO_FILL, GAO_NONE);

            thisTDRW_Status[TDRS_CMD_EL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Status[TDRS_CMD_EL], 4, 6, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("AOS:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_AOS] = D_CreateLabel("*", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_AOS], 5, 2, GAO_FILL, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("LOS:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisTDRW_Value[TDRS_LOS] = D_CreateLabel("*", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisTDRW_Value[TDRS_LOS], 6, 2, GAO_FILL, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Slot #2", vbox);
}

/**
 * This method builds the joint angle components. It calls the port and
 * starboard component builder.
 *
 * @return container has joint angle components
 */
static GtkWidget *create_joint_angle_form(void)
{
    GtkWidget *frame;
    GtkWidget *hbox;

    hbox = gtk_hbox_new(True, 1);

    frame = create_port_angles();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = create_stbd_angles();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = gtk_frame_new("Joint Angles (deg)");
    gtk_frame_set_label_align(GTK_FRAME(frame), CENTER_ALIGN, CENTER_ALIGN);
    gtk_container_set_border_width(GTK_CONTAINER(frame), 1);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);

    gtk_container_add(GTK_CONTAINER(frame), hbox);

    return frame;
}

/**
 * This method builds the port angle components
 *
 * @return container port joint angle components
 */
static GtkWidget *create_port_angles(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(6, 5, False);
        { /* row 1 */
            label = D_CreateLabel("2B", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_2BJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_2BJointAngle], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_2BJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_2BJointAngle], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("4B", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_4BJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_4BJointAngle], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_4BJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_4BJointAngle], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("2A", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_2AJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_2AJointAngle], 3, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_2AJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_2AJointAngle], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("4A", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_4AJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_4AJointAngle], 4, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_4AJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_4AJointAngle], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("SARJ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_PAJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_PAJointAngle], 5, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 5, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_PAJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_PAJointAngle], 5, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("RAD", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_PTCSJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_PTCSJointAngle], 6, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 6, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_PTCSJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_PTCSJointAngle], 6, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Port", vbox);
}

/**
 * This method builds the starboard angle components
 *
 * @return container starboard joint angle components
 */
static GtkWidget *create_stbd_angles(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(6, 5, False);
        { /* row 1 */
            label = D_CreateLabel("1B", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_1BJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_1BJointAngle], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_1BJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_1BJointAngle], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("3B", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_3BJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_3BJointAngle], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_3BJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_3BJointAngle], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("1A", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_1AJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_1AJointAngle], 3, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_1AJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_1AJointAngle], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("3A", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_3AJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_3AJointAngle], 4, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_3AJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_3AJointAngle], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("SARJ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_SAJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_SAJointAngle], 5, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 5, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_SAJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_SAJointAngle], 5, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("RAD", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_STCSJointAngle] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_STCSJointAngle], 6, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 6, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_STCSJointAngle] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_STCSJointAngle], 6, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }


    return D_CreateDataFrameWithLabel("Starboard", vbox);
}

/**
 * This methods creates the notebook container for the sband and
 * kuband data and temperature components
 *
 * @return container notebook component for temperature data
 */
static GtkWidget *create_data_temp_form(void)
{
    GtkWidget *label;
    GtkWidget *hbox;
    thisNotebook = gtk_notebook_new();

    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(thisNotebook), GTK_POS_TOP);
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(thisNotebook), True);
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(thisNotebook), True);

    /* create the sband1 data and temperature form */
    hbox = create_sband1_form();
    label = gtk_label_new(SBAND1_TITLE);
    gtk_notebook_append_page(GTK_NOTEBOOK(thisNotebook), hbox, label);

    /* create the sband2 data and temperature form */
    hbox = create_sband2_form();
    label = gtk_label_new(SBAND2_TITLE);
    gtk_notebook_append_page(GTK_NOTEBOOK(thisNotebook), hbox, label);

    /* create the kuband data and temperature form */
    hbox = create_kuband_form();
    label = gtk_label_new(KUBAND_TITLE);
    gtk_notebook_append_page(GTK_NOTEBOOK(thisNotebook), hbox, label);

    /* do this last so we avoid recursive calls in the callback */
    g_signal_connect(GTK_NOTEBOOK(thisNotebook), "switch_page", G_CALLBACK(page_switch_cb), NULL);

    return thisNotebook;
}

/**
 * This method builds the data and temperature components for the sband
 *
 * @return container has sband 1 temperature components
 */
static GtkWidget *create_sband1_form(void)
{
    GtkWidget *hbox;
    GtkWidget *frame;

    hbox = gtk_hbox_new(True, 1);

    frame = create_sband1_data_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = create_sband1_temp_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    return hbox;
}

/**
 * This method builds the data components for the sband
 *
 * @return container sband 1 data components
 */
static GtkWidget *create_sband1_data_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(12, 5, False);
        { /* row 1 */
            label = D_CreateLabel("Digital AGC", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 3, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1AGC] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1AGC], 1, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1AGC] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1AGC], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("Frame Lock", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1FrameLk] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1FrameLk], 2, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1FrameLk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1FrameLk], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("Carrier Lock", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1CarrLk] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1CarrLk], 3, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1CarrLk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1CarrLk], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("Bit Detection", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1BitDet] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1BitDet], 4, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1BitDet] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1BitDet], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("AZ Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1CmdAz] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1CmdAz], 5, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1CmdAz] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1CmdAz], 5, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("EL Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1CmdEl] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1CmdEl], 6, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1CmdEl] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1CmdEl], 6, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 7 */
            label = D_CreateLabel("Handover", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 7, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1Handover] = D_CreateLabel("999999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1Handover], 7, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1Handover] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1Handover], 7, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 8 */
            label = D_CreateLabel("Tracking", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 8, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1Tracking] = D_CreateLabel("99999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1Tracking], 8, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1Tracking] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1Tracking], 8, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 9 */
            label = D_CreateLabel("Select Mode", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 9, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1SatSel] = D_CreateLabel("99999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1SatSel], 9, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1SatSel] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1SatSel], 9, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 10 */
            label = D_CreateLabel("Command Link", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 10, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1CmdLnk] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1CmdLnk], 10, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1CmdLnk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1CmdLnk], 10, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 11 */
            label = D_CreateLabel("RELP Sync", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 11, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1RELPCh1] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1RELPCh1], 11, 2, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1RELPCh1] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1RELPCh1], 11, 3, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1RELPCh2] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1RELPCh2], 11, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1RELPCh2] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1RELPCh2], 11, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 12 */
            label = D_CreateLabel("MUX Activity", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 12, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1MUXCh1] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1MUXCh1], 12, 2, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1MUXCh1] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1MUXCh1], 12, 3, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1MUXCh2] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1MUXCh2], 12, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb1MUXCh2] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1MUXCh2], 12, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Systems Data", vbox);
}

/**
 * This methods builds the sband temperature components
 *
 * @return container has sband 1 temperature components
 */
static GtkWidget *create_sband1_temp_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(5, 5, False);
        { /* row 1 */
            label = D_CreateLabel("AZ Gimbal", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_Sb1AZTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1AZTemp], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb1AZTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1AZTemp], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("EL Gimbal", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1ELTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1ELTemp], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb1ELTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1ELTemp], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("RFG Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1RFGTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1RFGTemp], 3, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb1RFGTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1RFGTemp], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("Transponder Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1TransTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1TransTemp], 4, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb1TransTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1TransTemp], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("BSP Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb1BSPTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb1BSPTemp], 5, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label,  5, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb1BSPTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb1BSPTemp], 5, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Temps ("DEGREE_SYMBOL"C)", vbox);
}

/**
 * This method builds the data and temperature components for the sband
 *
 * @return container has sband 2 components
 */
static GtkWidget *create_sband2_form(void)
{
    GtkWidget *hbox;
    GtkWidget *frame;

    hbox = gtk_hbox_new(True, 1);

    frame = create_sband2_data_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = create_sband2_temp_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    return hbox;
}

/**
 * This method builds the data components for the sband
 *
 * @return sband data container
 */
static GtkWidget *create_sband2_data_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(12, 5, False);
        { /* row 1 */
            label = D_CreateLabel("Digital AGC", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 3, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2AGC] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2AGC], 1, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2AGC] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2AGC], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("Frame Lock", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2FrameLk] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2FrameLk], 2, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2FrameLk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2FrameLk], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("Carrier Lock", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2CarrLk] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2CarrLk], 3, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2CarrLk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2CarrLk], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("Bit Detection", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2BitDet] = D_CreateLabel("No-Lock", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2BitDet], 4, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2BitDet] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2BitDet], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("AZ Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2CmdAz] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2CmdAz], 5, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2CmdAz] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2CmdAz], 5, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 6 */
            label = D_CreateLabel("EL Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2CmdEl] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2CmdEl], 6, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2CmdEl] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2CmdEl], 6, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 7 */
            label = D_CreateLabel("Handover", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 7, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2Handover] = D_CreateLabel("999999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2Handover], 7, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2Handover] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2Handover], 7, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 8 */
            label = D_CreateLabel("Tracking", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 8, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2Tracking] = D_CreateLabel("99999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2Tracking], 8, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2Tracking] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2Tracking], 8, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 9 */
            label = D_CreateLabel("Select Mode", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 9, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2SatSel] = D_CreateLabel("99999", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2SatSel], 9, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2SatSel] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2SatSel], 9, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 10 */
            label = D_CreateLabel("Command Link", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 10, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2CmdLnk] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2CmdLnk], 10, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2CmdLnk] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2CmdLnk], 10, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 11 */
            label = D_CreateLabel("RELP Sync", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 11, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2RELPCh1] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2RELPCh1], 11, 2, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2RELPCh1] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2RELPCh1], 11, 3, GAO_NONE, GAO_NONE);

            thisValue[IS_Sb2RELPCh2] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2RELPCh2], 11, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2RELPCh2] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2RELPCh2], 11, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 12 */
            label = D_CreateLabel("MUX Activity", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 12, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2MUXCh1] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2MUXCh1], 12, 2, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2MUXCh1] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2MUXCh1], 12, 3, GAO_NONE, GAO_NONE);

            thisValue[IS_Sb2MUXCh2] = D_CreateLabel("Bad", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2MUXCh2], 12, 4, GAO_FILL, GAO_NONE);

            thisStatus[IS_Sb2MUXCh2] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2MUXCh2], 12, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Systems Data", vbox);
}

/**
 * This methods builds the sband temperature components
 *
 * @return sband temperature container
 */
static GtkWidget *create_sband2_temp_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(5, 5, False);
        { /* row 1 */
            label = D_CreateLabel("AZ Gimbal", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_Sb2AZTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2AZTemp], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb2AZTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2AZTemp], 1, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 2 */
            label = D_CreateLabel("EL Gimbal", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2ELTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2ELTemp], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb2ELTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2ELTemp], 2, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 3 */
            label = D_CreateLabel("RFG Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2RFGTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2RFGTemp], 3, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb2RFGTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2RFGTemp], 3, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 4 */
            label = D_CreateLabel("Transponder Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2TransTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2TransTemp], 4, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb2TransTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2TransTemp], 4, 5, GAO_NONE, GAO_NONE);
        }

        { /* row 5 */
            label = D_CreateLabel("BSP Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_Sb2BSPTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_Sb2BSPTemp], 5, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 5, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_Sb2BSPTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_Sb2BSPTemp], 5, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Temps ("DEGREE_SYMBOL"C)", vbox);
}

/**
 * This method builds the kuband data and temperature components
 *
 * @return kuband container
 */
static GtkWidget *create_kuband_form(void)
{
    GtkWidget *hbox;
    GtkWidget *frame;

    hbox = gtk_hbox_new(True, 1);

    frame = create_kuband_data_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    frame = create_kuband_temp_form();
    gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 0);

    return hbox;
}

/**
 * This method builds the kuband data components
 *
 * @return kuband data container
 */
static GtkWidget *create_kuband_data_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(1, 6, False);
        {   /* row 1 */
            label = D_CreateLabel("PWRL", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            /* this label forces the value and status to be right justified */
            /* only a single label is needed6for this column entry within */
            /* this table defined */
            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_KuPWRL] = D_CreateLabel("-180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPWRL], 1, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPWRL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPWRL], 1, 4, GAO_NONE, GAO_NONE);

            label = D_CreateLabel("dbm", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 5, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 6, GAO_FILL, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(4, 8, False);
        {   /* row 2 */
            label = D_CreateLabel("TRC:", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            label = D_CreateLabel("Pending", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel("    Mis", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 5, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" Actual", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 7, GAO_FILL, GAO_NONE);
        }

        {   /* row 3 */
            label = D_CreateLabel("Pointing Mode", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPtgPend] = D_CreateLabel("A-Trk", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPtgPend], 2, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPtgPend] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPtgPend], 2, 4, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPtgMisCmpr] = D_CreateLabel("A-Trk", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPtgMisCmpr], 2, 5, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPtgMisCmpr] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPtgMisCmpr], 2, 6, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPtgAct] = D_CreateLabel("A-Trk", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPtgAct], 2, 7, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPtgAct] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPtgAct], 2, 8, GAO_FILL, GAO_NONE);
        }

        {   /* row 4 */
            label = D_CreateLabel("PLC", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPLCPend] = D_CreateLabel("Normal", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPLCPend], 3, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPLCPend] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPLCPend], 3, 4, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPLCMisCmpr] = D_CreateLabel("Normal", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPLCMisCmpr], 3, 5, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPLCMisCmpr] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPLCMisCmpr], 3, 6, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPLCAct] = D_CreateLabel("Normal", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPLCAct], 3, 7, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPLCAct] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPLCAct], 3, 8, GAO_FILL, GAO_NONE);
        }

        {   /* row 5 */
            label = D_CreateLabel("Power Amp", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPwrAmpPend] = D_CreateLabel("On", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPwrAmpPend], 4, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPwrAmpPend] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPwrAmpPend], 4, 4, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPwrAmpMisCmpr] = D_CreateLabel("On", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPwrAmpMisCmpr], 4, 5, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPwrAmpMisCmpr] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPwrAmpMisCmpr], 4, 6, GAO_FILL, GAO_NONE);

            thisValue[IS_KuPwrAmpAct] = D_CreateLabel("On", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuPwrAmpAct], 4, 7, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuPwrAmpAct] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuPwrAmpAct], 4, 8, GAO_FILL, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(6, 4, False);
        {   /* row 6 */
            label = D_CreateLabel("XEL Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_KuActualXEL] = D_CreateLabel("-180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuActualXEL], 1, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuActualXEL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuActualXEL], 1, 4, GAO_NONE, GAO_NONE);
        }

        {   /* row 7 */
            label = D_CreateLabel("EL Position", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuActualEL] = D_CreateLabel("180.0", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuActualEL], 2, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuActualEL] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuActualEL], 2, 4, GAO_NONE, GAO_NONE);
        }

        {   /* row 8 */
            label = D_CreateLabel("TDRS ID", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuTDRSSelect] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTDRSSelect], 3, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuTDRSSelect] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTDRSSelect], 3, 4, GAO_NONE, GAO_NONE);
        }

        {   /* row 9 */
            label = D_CreateLabel("Auto Track Status", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuAutotrack] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuAutotrack], 4, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuAutotrack] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuAutotrack], 4, 4, GAO_NONE, GAO_NONE);
        }

        {   /* row 10 */
            label = D_CreateLabel("Handover", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuHandover] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuHandover], 5, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuHandover] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuHandover], 5, 4, GAO_NONE, GAO_NONE);
        }

        {   /* row 11 */
            label = D_CreateLabel("Continuous Retry", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuRetry] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuRetry], 6, 3, GAO_FILL, GAO_NONE);

            thisStatus[IS_KuRetry] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuRetry], 6, 4, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(1, 1, False);
        {   /* row 12 */
            label = D_CreateLabel("Enabled Masks", CENTER_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_EXPAND, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(1, IS_KU_PTG_MASK_MAX_SYMBOLS, False);
        {   /* row 13 */
            guint i;
            for (i=IS_KU_PTG_MASK_FIRST_SYMBOL; i<IS_KU_PTG_MASK_LAST_SYMBOL; i++)
            {
                thisValue[i] = D_CreateLabel("1", CENTER_ALIGN, NULL);
                insert_table_item(table, thisValue[i], 1, i+1, GAO_EXPAND, GAO_NONE);
            }
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);

        table = gtk_table_new(5, 5, False);
        {   /* row 14 */
            label = D_CreateLabel("Gimbal Angle Offset (GAO)", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND, GAO_NONE);

            thisValue[IS_KuGAO] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuGAO], 1, 3, GAO_NONE, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuGAO] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuGAO], 1, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 15 */
            label = D_CreateLabel("Forward Power", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuFwdPwrVolts] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuFwdPwrVolts], 2, 3, GAO_NONE, GAO_NONE);

            label = D_CreateLabel("V", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuFwdPwrVolts] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuFwdPwrVolts], 2, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 16 */
            label = D_CreateLabel("Reflected Power", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuReflPwrVolts] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuReflPwrVolts], 3, 3, GAO_NONE, GAO_NONE);

            label = D_CreateLabel("V", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuReflPwrVolts] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuReflPwrVolts], 3, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 17 */
            label = D_CreateLabel("IF Output", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuIFOutVolts] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuIFOutVolts], 4, 3, GAO_NONE, GAO_NONE);

            label = D_CreateLabel("V", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuIFOutVolts] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuIFOutVolts], 4, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 18 */
            label = D_CreateLabel("IF Input", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuIFInVolts] = D_CreateLabel("99", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuIFInVolts], 5, 3, GAO_NONE, GAO_NONE);

            label = D_CreateLabel("V", RIGHT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuIFInVolts] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuIFInVolts], 5, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Systems Data", vbox);
}

/**
 * This method builds the kuband temperature components
 *
 * @return kuband temperature container
 */
static GtkWidget *create_kuband_temp_form(void)
{
    GtkWidget *vbox;
    GtkWidget *table;
    GtkWidget *label;

    vbox = gtk_vbox_new(False, 0);
    {
        table = gtk_table_new(16, 5, False);
        {   /* row 1 */
            label = D_CreateLabel("SG1 XEL Encoder", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 1, 2, GAO_EXPAND_FILL, GAO_NONE);

            thisValue[IS_KuSG1Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG1Temp], 1, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 1, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG1Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG1Temp], 1, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 2 */
            label = D_CreateLabel("SG2 EL Encoder", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 2, 2, GAO_EXPAND_FILL, GAO_NONE);

            thisValue[IS_KuSG2Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG2Temp], 2, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 2, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG2Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG2Temp], 2, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 3 */
            label = D_CreateLabel("SG3 Tracking Mode Driver", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 3, 2, GAO_EXPAND_FILL, GAO_NONE);

            thisValue[IS_KuSG3Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG3Temp], 3, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 3, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG3Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG3Temp], 3, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 4 */
            label = D_CreateLabel("SG4 Motor Drive Amp", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 4, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuSG4Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG4Temp], 4, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 4, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG4Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG4Temp], 4, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 5 */
            label = D_CreateLabel("SG5 XEL Motor", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 5, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuSG5Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG5Temp], 5, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 5, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG5Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG5Temp], 5, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 6 */
            label = D_CreateLabel("SG6 EL Motor", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 6, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuSG6Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG6Temp], 6, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 6, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG6Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG6Temp], 6, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 7 */
            label = D_CreateLabel("SG7 Tracking Mode Driver", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 7, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuSG7Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG7Temp], 7, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 7, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG7Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG7Temp], 7, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 8 */
            label = D_CreateLabel("SG8 Motor Drive Amp", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 8, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuSG8Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuSG8Temp], 8, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 8, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuSG8Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuSG8Temp], 8, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 9 */
            label = D_CreateLabel("TRC S-1", 0.0, D_FgColor());
            insert_table_item(table, label, 9, 1, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(" ", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 9, 2, GAO_EXPAND_FILL, GAO_NONE);

            thisValue[IS_KuTRCS1Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTRCS1Temp], 9, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 9, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuTRCS1Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTRCS1Temp], 9, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 10 */
            label = D_CreateLabel("TRC S-2", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 10, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuTRCS2Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTRCS2Temp], 10, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 10, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuTRCS2Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTRCS2Temp], 10, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 11 */
            label = D_CreateLabel("TRC S-3", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 11, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuTRCS3Temp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTRCS3Temp], 11, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 11, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuTRCS3Temp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTRCS3Temp], 11, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 12 */
            label = D_CreateLabel("Power Amp", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 12, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuTRCPwrAmpTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTRCPwrAmpTemp], 12, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 12, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuTRCPwrAmpTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTRCPwrAmpTemp], 12, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 13 */
            label = D_CreateLabel("Power Supply", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 13, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuTRCPwrSupTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuTRCPwrSupTemp], 13, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 13, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuTRCPwrSupTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuTRCPwrSupTemp], 13, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 14 */
            label = D_CreateLabel("HRM Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 14, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuHRMTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuHRMTemp], 14, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 14, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuHRMTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuHRMTemp], 14, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 15 */
            label = D_CreateLabel("HRFM Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 15, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuHRFMTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuHRFMTemp], 15, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 15, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuHRFMTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuHRFMTemp], 15, 5, GAO_NONE, GAO_NONE);
        }

        {   /* row 16 */
            label = D_CreateLabel("VBSP Baseplate", LEFT_ALIGN, D_FgColor());
            insert_table_item(table, label, 16, 1, GAO_FILL, GAO_NONE);

            thisValue[IS_KuVBSPTemp] = D_CreateLabel("-999.9", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisValue[IS_KuVBSPTemp], 16, 3, GAO_FILL, GAO_NONE);

            label = D_CreateLabel(DEGREE_SYMBOL, RIGHT_ALIGN, D_DegreeColor());
            insert_table_item(table, label, 16, 4, GAO_NONE, GAO_NONE);

            thisStatus[IS_KuVBSPTemp] = D_CreateLabel("M", RIGHT_ALIGN, NULL);
            insert_table_item(table, thisStatus[IS_KuVBSPTemp], 16, 5, GAO_NONE, GAO_NONE);
        }
        gtk_box_pack_start(GTK_BOX(vbox), table, False, False, 0);
    }

    return D_CreateDataFrameWithLabel("Temps ("DEGREE_SYMBOL"C)", vbox);
}

/**
 * Rows and columns are number starting with 1.
 * @param table add to this table
 * @param item the item to add
 * @param row the row
 * @param col the column
 * @param xoptions fill options
 * @param yoptions fill options
 */
static void insert_table_item(GtkWidget *table, GtkWidget *item, gint row, gint col,
                              GtkAttachOptions xoptions, GtkAttachOptions yoptions)
{
    gtk_table_attach(GTK_TABLE(table), item, col-1, col, row-1, row, xoptions, yoptions, 1, 0);
}

/**
 * Gets the data label
 *
 * @param symbol_id symbol index
 *
 * @return label or NULL
 */
static GtkWidget *get_value_label(gint symbol_id)
{
    return thisValue[symbol_id];
}

/**
 * Gets the status label
 *
 * @param symbol_id symbol index
 *
 * @return label or NULL
 */
static GtkWidget *get_status_label(gint symbol_id)
{
    return thisStatus[symbol_id];
}

/**
 * This method handles the window close event. Instead of destroying the
 * window, just hide the dialog.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True
 */
static gboolean delete_event_cb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    close_window();
    return True;
}

/**
 * Handles the close button event
 *
 * @param button calling widget
 * @param user_data user data
 */
static void close_cb(GtkButton *button, void * user_data)
{
    close_window();
}

/**
 * Close the window and unrealize the dialog.
 * Unrealizing the dialog seems to reduce the cpu load???
 */
static void close_window(void)
{
    gtk_widget_hide_all(thisWindow);

    /* unrealize the dialog window, this will cascade down */
    /* to all child widgets, making them unrealized. this */
    /* action seems to reduce the cpu load */
    gtk_widget_unrealize(thisWindow);
}

/**
 * This method handles the iconification of the app
 * calling widget @param widget
 *
 * @param event event data
 * @param user_data user data
 *
 * @return True
 */
static gboolean visibility_cb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    GdkEventWindowState *windowStateEvent = (GdkEventWindowState *)event;

    if (windowStateEvent->new_window_state&GDK_WINDOW_STATE_ICONIFIED)
    {
        thisIsIconified = True;
    }
    else
    {
        thisIsIconified = False;
    }

    return False;
}

/**
 * Handles the resize event
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False
 */
static gint resize_cb(GtkWidget *widget, GdkEventConfigure *event, void * user_data)
{
    if (thisIsIconified == False)
    {
        /* resize the font ? */
        thisResizeFont.new_width = event->width;
        thisResizeFont.new_height = event->height;
        thisResizeFont.previous_font_size = D_ResizeFont(gtk_widget_get_name(thisWindow),
                                                         &thisResizeFont);
        thisResizeFont.previous_width = event->width;
        thisResizeFont.previous_height = event->height;
    }

    /* let the system event handler execute */
    return False;
}

/**
 * Handles the notebook page switch
 *
 * @param widget the notebook widget
 * @param page current page
 * @param page_num the page number we're switchint to
 * @param user_data user data
 */
static void page_switch_cb(GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data)
{
    GtkNotebook *notebook = GTK_NOTEBOOK(widget);
    gint old_page_num = gtk_notebook_get_current_page(notebook);

    if ((gint)page_num != old_page_num)
    {
        thisCoordinateType = page_num;

        /* change sun tracking label */
        switch (thisCoordinateType)
        {
            case SBAND1:
            case SBAND2:
                D_SetLabelText(thisSunLabel,   SUN_SBAND_LABEL, NULL);
                D_SetLabelText(thisSlot1Label, TDRS_SBAND_LABEL, NULL);
                D_SetLabelText(thisSlot2Label, TDRS_SBAND_LABEL, NULL);
                break;

            case KUBAND:
                D_SetLabelText(thisSunLabel,   SUN_KUBAND_LABEL, NULL);
                D_SetLabelText(thisSlot1Label, TDRS_KUBAND_LABEL, NULL);
                D_SetLabelText(thisSlot2Label, TDRS_KUBAND_LABEL, NULL);
                break;

            default:
                break;
        }

        /* update the tdrs slot data and sun tracking */
        IH_UpdateTDRSValues(page_num, False, True);
    }
}

/**
 * Returns the current coordinate that is selected.
 *
 * @return current coordinate SBAND1, SBAND2, or KUBAND
 */
gint RTRFD_GetCoordinateType(void)
{
    return thisCoordinateType;
}

/**
 * Sets the tdrs id for tdre and tdrw
 *
 * @param tdrs tdrs type
 * @param symbol symbol record
 */
void RTRFD_SetTdrsId(gint tdrs, SymbolRec *symbol)
{
    switch (tdrs)
    {
        case TD_TDRE:
            set_value(thisTDRE_Value[TDRS_ID], symbol);
            break;

        case TD_TDRW:
            set_value(thisTDRW_Value[TDRS_ID], symbol);
            break;

        default:
            break;
    }
}

/**
 * Sets the tdrs az/el xel values for sun, tdrw, tdre
 *
 * @param tdrs tdrs type
 * @param azel_type azel type
 * @param symbol symbol record
 */
void RTRFD_SetTdrsValue(gint tdrs, gint azel_type, SymbolRec *symbol)
{
    switch (tdrs)
    {
        case TD_SUN:
            {
                static gdouble az_value = -1.0;
                static gdouble el_value = -1.0;
                static gchar az_indicator = 'x';
                static gchar el_indicator = 'x';
                if (azel_type == AZ_TYPE)
                {
                    if (symbol->value != az_value ||
                        symbol->status.indicator != az_indicator)
                    {
                        az_value = symbol->value;
                        az_indicator = symbol->status.indicator;
                        set_status(thisSUN_Status[TDRS_CMD_AZ], symbol);
                        set_value(thisSUN_Value[TDRS_CMD_AZ], symbol);
                    }
                }
                else
                {
                    if (symbol->value != el_value ||
                        symbol->value != el_indicator)
                    {
                        el_value = symbol->value;
                        el_indicator = symbol->status.indicator;
                        set_status(thisSUN_Status[TDRS_CMD_EL], symbol);
                        set_value(thisSUN_Value[TDRS_CMD_EL], symbol);
                    }
                }
            }
            break;

        case TD_TDRE:
            {
                static gdouble az_value = -1.0;
                static gdouble el_value = -1.0;
                static gchar az_indicator = 'x';
                static gchar el_indicator = 'x';
                if (azel_type == AZ_TYPE)
                {
                    if (symbol->value != az_value ||
                        symbol->status.indicator != az_indicator)
                    {
                        az_value = symbol->value;
                        az_indicator = symbol->status.indicator;
                        set_status(thisTDRE_Status[TDRS_CMD_AZ], symbol);
                        set_value(thisTDRE_Value[TDRS_CMD_AZ], symbol);
                    }
                }
                else
                {
                    if (symbol->value != el_value ||
                        symbol->value != el_indicator)
                    {
                        el_value = symbol->value;
                        el_indicator = symbol->status.indicator;
                        set_status(thisTDRE_Status[TDRS_CMD_EL], symbol);
                        set_value(thisTDRE_Value[TDRS_CMD_EL], symbol);
                    }
                }
            }
            break;

        case TD_TDRW:
            {
                static gdouble az_value = -1.0;
                static gdouble el_value = -1.0;
                static gchar az_indicator = 'x';
                static gchar el_indicator = 'x';
                if (azel_type == AZ_TYPE)
                {
                    if (symbol->value != az_value ||
                        symbol->status.indicator != az_indicator)
                    {
                        az_value = symbol->value;
                        az_indicator = symbol->status.indicator;
                        set_status(thisTDRW_Status[TDRS_CMD_AZ], symbol);
                        set_value(thisTDRW_Value[TDRS_CMD_AZ], symbol);
                    }
                }
                else
                {
                    if (symbol->value != el_value ||
                        symbol->value != el_indicator)
                    {
                        el_value = symbol->value;
                        el_indicator = symbol->status.indicator;
                        set_status(thisTDRW_Status[TDRS_CMD_EL], symbol);
                        set_value(thisTDRW_Value[TDRS_CMD_EL], symbol);
                    }
                }
            }
            break;

        default:
            break;
    }
}

/**
 * This function sets the prediction data fields for a TDRS data block.
 *
 * @param tdrs tdrs type
 * @param color tdrs color
 */
void RTRFD_SetTdrsPredicts(gint tdrs, guchar color)
{
    gchar st1[100];
    gchar st2[100];
    gchar st3[TIME_STR_LEN];
    gchar st4[TIME_STR_LEN];
    PredictSet *ps;
    glong sec1;
    GdkColor *gcolor = CH_GetGdkColorByIndex(color);
    Point *point;

    /* load the coordinate type */
    RTDH_LoadRtPredicts(thisCoordinateType, tdrs, False);
    ps = RTDH_GetPredicts(tdrs);

    g_stpcpy(st1, " ");
    g_stpcpy(st2, " ");
    g_stpcpy(st3, " ");
    g_stpcpy(st4, " ");

    sec1 = CurrentUtcSecs();
    if ((ps != NULL) && (sec1 >= ps->aosSecs))
    {
        GList *curPredPt = RTDH_GetCurPredictPt(tdrs);
        if (curPredPt != NULL)
        {
            PredictPt *pt;
            static gint st1_value = -1;
            static gint st2_value = -1;

            for (; curPredPt != NULL; curPredPt = g_list_next(curPredPt))
            {
                pt = (PredictPt *)curPredPt->data;
                //GIGO - put guard here 
                //if (pt == NULL || pt->timeSecs >= sec1)
                if (pt->timeSecs >= sec1)
                {
                    break;
                }
            }

            point = PDH_GetCoordinatePoint(thisCoordinateType, pt);
            if (point->x != st1_value)
            {
                st1_value = point->x;
                g_sprintf(st1, "%6.1f", (gdouble)point->x / 10.0);
                if (tdrs == TD_TDRE)
                {
                    D_SetLabelText(thisTDRE_Value[TDRS_PRED_AZ], st1, gcolor);
                }
                else
                {
                    D_SetLabelText(thisTDRW_Value[TDRS_PRED_AZ], st1, gcolor);
                }
            }
            if (point->y != st2_value)
            {
                st2_value = point->y;
                g_sprintf(st2, "%6.1f", (gdouble)point->y / 10.0);
                if (tdrs == TD_TDRE)
                {
                    D_SetLabelText(thisTDRE_Value[TDRS_PRED_EL], st2, gcolor);
                }
                else
                {
                    D_SetLabelText(thisTDRW_Value[TDRS_PRED_EL], st2, gcolor);
                }
            }
        }
    }

    if (ps != NULL)
    {
        if (sec1 <= ps->aosSecs)
        {
            SecsToStr(TS_ELAPSED, ps->aosSecs - sec1, st3);
            StripTimeBlanks(st3);
            if (tdrs == TD_TDRE)
            {
                D_SetLabelText(thisTDRE_Value[TDRS_AOS], st3, gcolor);
            }
            else
            {
                D_SetLabelText(thisTDRW_Value[TDRS_AOS], st3, gcolor);
            }
        }

        if (sec1 <= ps->losSecs)
        {
            SecsToStr(TS_ELAPSED, ps->losSecs - sec1, st4);
            StripTimeBlanks(st4);
            if (tdrs == TD_TDRE)
            {
                D_SetLabelText(thisTDRE_Value[TDRS_LOS], st4, gcolor);
            }
            else
            {
                D_SetLabelText(thisTDRW_Value[TDRS_LOS], st4, gcolor);
            }
        }
    }
}

/**
 * Sets the value and status for the given symbol
 *
 * @param symbol symbol record
 */
void RTRFD_SetValue(SymbolRec *symbol)
{
    GtkWidget *label;

    /* change status indicator on change only */
    if (thisStatusIndicator[symbol->symbol_id] != symbol->status.indicator)
    {
        thisStatusIndicator[symbol->symbol_id] = symbol->status.indicator;
        label = get_status_label(symbol->symbol_id);
        set_status(label, symbol);
    }

    label = get_value_label(symbol->symbol_id);
    set_value(label, symbol);
}

/**
 * Sets the status indicator
 *
 * @param label label widget
 * @param symbol symbol data
 */
static void set_status(GtkWidget *label, SymbolRec *symbol)
{
    gchar indicator[2];

    /* make status indicator a string */
    indicator[0] = symbol->status.indicator;
    indicator[1] = 0;

    D_SetLabelText(label, indicator,
                   CH_GetGdkColorByName(CH_GetColorName(symbol->status.color)));
}

/**
 * Sets the data value
 *
 * @param label label widget
 * @param symbol symbol data
 */
static void set_value(GtkWidget *label, SymbolRec *symbol)
{
    D_SetLabelText(label, symbol->value_str,
                   CH_GetGdkColorByName(CH_GetColorName(symbol->value_color)));
}
