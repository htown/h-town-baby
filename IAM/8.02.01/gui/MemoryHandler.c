/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <gtk/gtk.h>

#define PRIVATE
#include "MemoryHandler.h"
#undef PRIVATE

#ifdef G_OS_WIN32
#include <crtdbg.h>
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/MemoryHandler.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");

#endif

/**************************************************************************
* Private Data Definitions
**************************************************************************/

/*************************************************************************/

/**
 * This function is simply a malloc followed by a test for NULL to assure
 * that the malloc succeeded.
 *
 * @param size size for memory allocation
 * @param filename debug info; callee should use __FILE__
 * @param linenumber debug info; callee should use __LINE__
 *
 * @return allocated memory or exits
 */
void * MH_Calloc(guint size, const gchar *filename, gint linenumber)
{
    /* pointer to allocated memory that has been zero'd */
    void *ptr = NULL;

    /* make sure size is valid */
    if (size > 0)
    {
#if defined(G_OS_WIN32) && defined(_DEBUG)
        ptr = _calloc_dbg(1, size, _NORMAL_BLOCK, filename, linenumber);
#else
        ptr = g_malloc0(size);
#endif
    }

    /* if ptr never got allocated, bail */
    if (ptr == NULL)
    {
        /* force a core */
        g_critical("MH_Calloc: malloc failed for size = %d: %s", size, strerror(errno));
        signal(SIGABRT, SIG_DFL);
        abort();
    }

    return ptr;
}

/**
 * This function is simply a realloc followed by a test for NULL to assure
 * that the realloc succeeded.
 *
 * @param ptr new pointer
 * @param size new size of memory chunk needed
 * @param filename debug info; callee should use __FILE__
 * @param linenumber debug info; callee should use __LINE__
 */
void MH_Realloc(void * *ptr, guint size, const gchar *filename, gint linenumber)
{
    void *new_ptr = NULL;

#if defined(G_OS_WIN32) && defined(_DEBUG)
    new_ptr = _realloc_dbg(*ptr, size, _NORMAL_BLOCK, filename, linenumber);
#else
    new_ptr = g_realloc(*ptr, size); /* pointer to re-allocated memory */
#endif

    /* should have a valid pointer */
    if(new_ptr == NULL)
    {
        /* force a core */
        g_critical("MH_Calloc: realloc failed for size = %d: %s", size, strerror(errno));
        signal(SIGABRT, SIG_DFL);
        abort();
    }

    *ptr = new_ptr;
}

/**
 * Frees the pointer allocated by MH_Calloc or MH_Realloc; sets the
 * pointer to null
 *
 * @param ptr free this memory
 */
void MH_Free(void * ptr)
{
    if (ptr != NULL)
    {
#if defined(G_OS_WIN32) && defined(_DEBUG)
        _free_dbg(ptr, _NORMAL_BLOCK);
#else
        g_free(ptr);
#endif
        ptr = NULL;
    }
}
