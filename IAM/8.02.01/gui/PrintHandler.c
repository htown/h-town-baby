/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #ifndef __USE_BSD
       #define __USE_BSD
    #endif
    #include <unistd.h>
    #undef __USE_BSD
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif
    #include <winsock2.h> /* for gethostname */
    #include <ws2tcpip.h>
    #include <process.h>  /* for _getpid */
#endif

#define PRIVATE
#include "PrintHandler.h"
#undef PRIVATE

#include "ConfigParser.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PrintHandler.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/*************************************************************************/

/**
 * This function prints the prediction information.
 *
 * @param drawable has the graphics
 * @param width width of window
 * @param height height of window
 */
void PRP_PrintPredict(GdkDrawable *drawable, gint width, gint height)
{
    GtkWidget *dialog = PD_GetDialog();

    /* raise the window */
    gdk_window_show(dialog->window);

    /* now get the image */
    if (drawable != NULL)
    {
        GdkPixbuf *pixbuf;

        GdkCursor *watch_cursor = gdk_cursor_new(GDK_WATCH);
        gdk_window_set_cursor(drawable, watch_cursor);

        pixbuf = gdk_pixbuf_get_from_drawable(NULL,
                                              drawable,
                                              gdk_colormap_get_system(),
                                              0, 0, 0, 0,
                                              width, height);
        if (pixbuf != NULL)
        {
            gchar *report_name = build_report_name("predict", "jpg");
            if (save_report_image(pixbuf, report_name) == True)
            {
                PRP_Print(IMAGE_FILE_TYPE, report_name, NULL);
                InfoMessage(dialog, "Print", "Print Job Completed" );
            }
            g_free(report_name);
        }

        gdk_cursor_unref(watch_cursor);
        gdk_window_set_cursor(drawable, NULL);
    }
}

/**
 * This method prints the blockages for the predicts currently being viewed
 * in the predict display.
 *
 * @param sets the list of predicts to print
 */
void PRP_PrintPredictBlockage(GList *sets)
{
    FILE *fp;
    GList *glist;
    PredictSet *ps;

    /* now do the text report */
    gchar *report_name = build_report_name("predict", "txt");
    if ((fp = fopen(report_name, "w")) == NULL)
    {
        gchar message[512];
        g_sprintf(message,
                  "Unable to open blockage data output file.\n"
                  "Error: %s\n"
                  "Prediction blockage data not printed.",
                  strerror(errno));
        ErrorMessage(PD_GetDialog(), "File Error", message);
        return;
    }

    /* build predict report */
    for (glist = sets; glist != NULL; glist = g_list_next(glist))
    {
        ps = (PredictSet *)glist->data;
        if (ps)
        {
            /* is the print flag set ? */
            if (ps->displayed & DISPLAY_PRT)
            {
                print_inview_table(fp, ps);
                print_mask_blockage(fp, ps);
                OM_PrintBlockage(fp, ps);
                print_dynamic_blockage(fp, ps);
            }
        }
    }
    fflush(fp);
    fclose(fp);

    /* print the event blockage report */
    PRP_Print(TEXT_FILE_TYPE, report_name, "IAM Prediction Display Report");

    /* done with resources */
    g_free(report_name);
}

/**
 * This function outputs the data in the drawable for realtime
 *
 * @param drawable has the graphics image
 * @param width width of window
 * @param height height of window
 */
void PRP_PrintRealtime(GdkDrawable *drawable, gint width, gint height)
{
    GtkWidget *dialog = RTD_GetDialog();

    /* raise the window */
    gdk_window_show(dialog->window);

    if (drawable != NULL)
    {
        GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable(NULL,
                                                         drawable,
                                                         gdk_colormap_get_system(),
                                                         0, 0, 0, 0,
                                                         width, height);
        if (pixbuf != NULL)
        {
            gchar *report_name = build_report_name("realtime", "jpg");
            if (save_report_image(pixbuf, report_name) == True)
            {
                PRP_Print(IMAGE_FILE_TYPE, report_name, NULL);
                InfoMessage(dialog, "Print", "Print Job Completed" );
            }
            g_free(report_name);
        }
    }
}

/**
 * Builds a unique name based on the basename.
 *
 * @param basename something like "predict" or "realtime"
 * @param extension primary filename extension
 *
 * @return fullpath to report filename
 */
static gchar *build_report_name(gchar *basename, gchar *extension)
{
    static gint n = 0;
    gchar host[32] = { 0};
    gchar report_dir[MAXPATHLEN] = { 0};
    gchar report_name[MAXPATHLEN] = { 0};

    /* get process id */
#ifdef G_OS_WIN32
    gint pid = _getpid();
#else
    pid_t pid = getpid();
#endif

    /* get host */
    gethostname(host, 31);

    /* build unique name */
    g_sprintf(report_name, "%s-%s-%d.%03d.%s", basename,host,pid,++n,extension);

    /* get report directory, if not found, use tmp */
    if (CP_GetConfigData(IAM_REPORT_DIR, report_dir) == False)
    {
        g_sprintf(report_dir, "%s", g_get_tmp_dir());
    }

    /* build report name with path */
    return g_build_filename(report_dir,  report_name,  NULL);
}

/**
 * Creates a postscript filename from the given file.
 *
 * @param file the base file
 *
 * @return newly allocated filename
 */
static gchar *make_ps_filename(gchar *file)
{
    /* make postscript output file from given filename */
    gchar *dirname = g_path_get_dirname(file);
    gchar *head = CP_GetRootName(file);

    if (head == NULL)
    {
        head = g_path_get_basename(file);
    }

    gchar *psfile =  g_strconcat(dirname, G_DIR_SEPARATOR_S, head, ".ps", NULL);
    g_free(dirname);
    g_free(head);

    return psfile;
}

/**
 * Saves the pixbuf image as a jpeg image into the given report name.
 *
 * @param pixbuf save this image
 * @param report_names save image to the base_filename
 *
 * @return True if successful otherwise False
 */
static gboolean save_report_image(GdkPixbuf *pixbuf, gchar *report_name)
{
    GError *error = NULL;

    if (gdk_pixbuf_save (pixbuf, report_name, "jpeg",
                         &error, "quality", "100", NULL)
        == 0)
    {
        gchar message[MAXPATHLEN];
        gchar *dirname = g_path_get_dirname(report_name);

        if (error != NULL)
        {
            g_warning(error->message);
            g_sprintf(message, "Unable to save image!\n"
                      "Verify '%s' directory exists and is writable.\n"
                      "Additional error information: %s",
                      dirname, error->message);
            g_error_free(error);
        }
        else
        {
            g_sprintf(message, "Unable to save image!\n"
                      "Verify '%s' directory exists and is writable.",
                      dirname);
        }
        g_free(dirname);

        ErrorMessage(PD_GetDialog(), "Print Data", message);

        return False;
    }
    else
    {
        g_warning("save_report_image: gdk_pixbuf_save failed");
        if (error != NULL)
        {
            g_warning("save_report_image: error=%s",error->message);
            g_error_free(error);
        }
    }

    return True;
}

/**
 * This function adds the a set of blockage information to the report file.
 *
 * @param fp file pointer
 * @param ps predict set
 * @param masks bit mask
 * @param title title to use
 */
static void print_blockage_table(FILE *fp, PredictSet *ps, guint64 masks, gchar *title)
{
    GList *ptList;
    gboolean inBlockage = False;
    gboolean blockageOccurred = False;
    gint coord = PD_GetCoordinateType();
    gint timeType = AM_GetTimeType();
    gchar timeStr[TIME_STR_LEN];
    gchar timeTypeStr[4];

    if ((ps != NULL) && (ps->ptList != NULL))
    {
        if (timeType == TS_GMT)
        {
            g_stpcpy(timeTypeStr, "GMT");
        }
        else
        {
            g_stpcpy(timeTypeStr, "UTC");
        }

        fprintf(fp, "-----------------------------------------------------\n");

        switch (coord)
        {
            case SBAND1:
                fprintf(fp, "S-Band 1\n");
                break;

            case SBAND2:
                fprintf(fp, "S-Band 2\n");
                break;

            case KUBAND:
                fprintf(fp, "Ku-Band \n");
                break;

            case STATION:
                fprintf(fp, "Station \n");
                break;

            default:
                break;
        }

        fprintf(fp, "%-25.25s ----%s-----\n", title, timeTypeStr);

        for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
        {
            PredictPt *pt = (PredictPt *)ptList->data;
            PredictPt *nxtPt;

            g_stpcpy(timeStr, pt->timeStr);
            if (timeType == TS_GMT)
            {
                UtcToGmt(timeStr);
            }

            /* get next point in list */
            nxtPt = NULL;
            if (ptList->next != NULL)
            {
                nxtPt = (PredictPt *)ptList->next->data;
            }

            /* see where we're at */
            if ((pt->masks & masks) && (!inBlockage) &&
                ((nxtPt != NULL) && (!(nxtPt->masks & masks))))
            {
                fprintf( fp, "          Crossed:       %s\n", timeStr );
                blockageOccurred = True;
            }
            else if ((pt->masks & masks) && (!inBlockage))
            {
                inBlockage = True;
                fprintf( fp, "             From:       %s\n", timeStr );
                blockageOccurred = True;
            }

            if (((nxtPt != NULL) && (!(nxtPt->masks & masks))) && (inBlockage))
            {
                inBlockage = False;
                fprintf( fp, "               To:       %s\n",  timeStr );
                blockageOccurred = True;
            }
        }

        if (blockageOccurred == False)
        {
            fprintf(fp, "No blockage occurs for this mask.\n");
        }

        fprintf(fp, "\n");
    }
}

/**
 * This function adds the basic track information to the report file.
 *
 * @param fp file pointer
 * @param ps predict set
 */
static void print_inview_table(FILE *fp, PredictSet *ps)
{
    gchar time_str[TIME_STR_LEN];
    gchar aos[TIME_STR_LEN];
    gchar los[TIME_STR_LEN];
    gchar timeTypeStr[4];

    gint timeType = AM_GetTimeType();

    g_stpcpy(aos, ps->aosStr);
    g_stpcpy(los, ps->losStr);
    if (timeType == TS_GMT)
    {
        UtcToGmt(aos);
        UtcToGmt(los);
        g_stpcpy(timeTypeStr, "GMT");
    }
    else
    {
        g_stpcpy(timeTypeStr, "UTC");
    }

    fprintf( fp, "SATELLITE %s, ORBIT %d\n", ps->eventName, ps->orbit );
    fprintf( fp, "=====================================================\n" );
    fprintf( fp, "IN VIEW                  ----%s-----\n", timeTypeStr );

    fprintf( fp, "    AOS                  %20s\n", aos );
    fprintf( fp, "    LOS                  %20s\n", los );

    SecsToStr( TS_ELAPSED, ps->losSecs - ps->aosSecs, time_str);
    StripTimeBlanks( time_str );

    g_stpcpy(aos, ps->shoStart);
    g_stpcpy(los, ps->shoEnd);
    if (timeType == TS_GMT)
    {
        UtcToGmt(aos);
        UtcToGmt(los);

        if (aos[0] == '1' && aos[1] == '9' && aos[2] == '9' && aos[3] == '2')
        {
            aos[0] = '0';
            aos[1] = '0';
            aos[2] = '0';
            aos[3] = '0';

            los[0] = '0';
            los[1] = '0';
            los[2] = '0';
            los[3] = '0';
        }
    }

    fprintf( fp, "    DELTA                %20s\n", time_str );
    fprintf( fp, "    SHO START            %20s\n", aos );
    fprintf( fp, "    SHO  STOP            %20s\n", los );
}

/**
 * This function adds the Mask Blockage information to the report file.
 *
 * @param fp file pointer
 * @param ps predict set
 */
static void print_mask_blockage(FILE *fp, PredictSet *ps)
{
    guint i;
    gint coord = PD_GetCoordinateType();

    for (i=0; i<MF_GetMaskFileCount(PREDICT, coord); i++)
    {
        guint64 flag = MF_GetBlockageMask(PREDICT, coord, i);
        guint64 mask = MF_GetBitMask(PREDICT, coord) & flag;

        print_blockage_table(fp, ps, mask, MF_GetMaskName(PREDICT, coord, i));
    }
}


/**
 * This function prints the blockage for the dynamic parts.
 *
 * @param fp file pointer
 * @param ps predict set
 */
static void print_dynamic_blockage(FILE *fp, PredictSet *ps)
{
    GList *ptList;
    gint flag;
    gint coord = PD_GetCoordinateType();
    gint timeType = AM_GetTimeType();
    gint currentBlocked, nextBlocked;
    gboolean inBlockage       = False;
    gboolean blockageOccurred = False;
    gchar timeStr[TIME_STR_LEN];
    gchar timeTypeStr[4];

    if ((ps != NULL) && (ps->ptList != NULL))
    {
        if (timeType == TS_GMT)
        {
            g_stpcpy(timeTypeStr, "GMT");
        }
        else
        {
            g_stpcpy(timeTypeStr, "UTC");
        }

        fprintf(fp, "-----------------------------------------------------\n");
        fprintf(fp, "%-25.25s ----%s-----\n", "Dynamic Blockage", timeTypeStr);

        for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
        {
            PredictPt *pt = (PredictPt *)ptList->data;

            nextBlocked = False;
            if (ptList->next != NULL)   /* check if next point is blocked */
            {
                /* get next point */
                PredictPt *nxtPt = (PredictPt *)ptList->next->data;

                if (nxtPt->dynamicBlock == PT_BlockageUnknown)
                {
                    Point *point = PDH_GetCoordinatePoint(coord, nxtPt);
                    if (point != NULL)
                    {
                        Dimension *d = PD_GetDrawingAreaSize();
                        if (d != NULL)
                        {
                            flag = DD_PointBlocked(PREDICT, coord, d->width, d->height,
                                                   point, nxtPt->timeStr );
                            nxtPt->dynamicBlock = (flag) ? PT_IsBlocked : PT_IsNotBlocked;
                            MH_Free(d);
                        }
                    }
                }
                nextBlocked = (nxtPt->dynamicBlock == PT_IsBlocked);
            }

            g_stpcpy(timeStr, pt->timeStr);
            if (timeType == TS_GMT)
            {
                UtcToGmt(timeStr);
            }

            if (!inBlockage)  /* if not in blockage area */
            {
                if (pt->dynamicBlock == PT_BlockageUnknown)
                {
                    Point *point = PDH_GetCoordinatePoint(coord, pt);
                    if (point != NULL)
                    {
                        Dimension *d = PD_GetDrawingAreaSize();
                        if (d != NULL)
                        {
                            flag = DD_PointBlocked(PREDICT, coord, d->width, d->height,
                                                   point, pt->timeStr );
                            pt->dynamicBlock = (flag) ? PT_IsBlocked : PT_IsNotBlocked;
                            MH_Free(d);
                        }
                    }
                }
                currentBlocked = (pt->dynamicBlock == PT_IsBlocked);

                if (currentBlocked && !nextBlocked)  /* if single point blocked */
                {
                    fprintf( fp, "          Crossed:       %s\n", timeStr );
                    blockageOccurred = True;
                }
                else if (currentBlocked)  /* if going into blockage */
                {
                    inBlockage = True;
                    fprintf( fp, "             From:       %s\n", timeStr );
                    blockageOccurred = True;
                }
            }
            else  /* currently in blockage area */
            {
                if (!nextBlocked)  /* if coming out of blockage */
                {
                    inBlockage = False;
                    fprintf( fp, "               To:       %s\n", timeStr ); blockageOccurred = True;
                }
            }
        }

        if (blockageOccurred == False)
        {
            fprintf(fp, "No Dynamic blockage occurs for this event.\n");
        }

        fprintf(fp, "\n");
    }
}

/**
 * General interface to the print engine for win32 and unix.
 * For win32, the prfile32.exe is used, it can't print images though.
 * For unix, we use lpr for printing.
 *
 * @param file_type either TEXT_FILE_TYPE or IMAGE_FILE_TYPE
 * @param file print this file
 * @param header optional header for text print.
 */
gboolean PRP_Print(gint file_type, gchar *file, gchar *header)
{
    gboolean rc = False;

    /* for image type, get the jpeg2ps converter */
    if (file_type == IMAGE_FILE_TYPE)
    {
        rc = convert_jpeg2ps(file);
    }
    else
    {
        rc = convert_text2ps(file, header);
    }

    return rc;
}

/**
 * Converts the given jpeg file to postscript data file.
 *
 * @param jpeg_file the jpeg file to convert
 *
 * @return True if conversion was successful, otherwise False
 */
static gboolean convert_jpeg2ps(gchar *jpeg_file)
{
    gboolean rc = False;

    gchar converter[MAXPATHLEN] = {0};
    if (CP_GetConfigData(IAM_JPEG_2_PS_CONVERTER, converter) == True)
    {
        gint i;
        gchar *argv[10];

        /* make postscript output file from jpeg base filename */
        gchar *psfile =  make_ps_filename(jpeg_file);

        i = 0;
        argv[i++] = g_strdup(converter);
        argv[i++] = "-q";
        argv[i++] = "-i";
        argv[i++] = jpeg_file;
        argv[i++] = "-o";
        argv[i++] = psfile;
        argv[i++] = NULL;
        if ((rc = spawn_command(argv)) == True)
        {
            rc = print_psfile(psfile);
        }
        else
        {
            WarningMessage(NULL, "Conversion failed",
                           "Unable to convert screen to JPEG for printing!");
            g_message("convert_jpeg2ps: conversion failed");
        }
        g_free(argv[0]);
        g_free(psfile);
    }

    return rc;
}

/**
 * Converts the given text file to postscript data file.
 *
 * @param text_file the text file to convert
 * @param header optional header that will be display center top
 *               of page.
 * @return True if conversion was successful, otherwise False
 */
static gboolean convert_text2ps(gchar *text_file, gchar *header)
{
    gboolean rc = False;

    gchar converter[MAXPATHLEN] = {0};
    if (CP_GetConfigData(IAM_TEXT_2_PS_CONVERTER, converter) == True)
    {
        gint i;
        gchar *argv[10];

        /* make postscript output file from jpeg base filename */
        gchar *psfile =  make_ps_filename(text_file);

        i = 0;
        argv[i++] = g_strdup(converter);
        argv[i++] = "-i";
        argv[i++] = text_file;
        argv[i++] = "-o";
        argv[i++] = psfile;
        if (header != NULL)
        {
            argv[i++] = "-t";
            argv[i++] = header;
        }
        argv[i++] = NULL;
        if ((rc = spawn_command(argv)) == True)
        {
            rc = print_psfile(psfile);
        }
        g_free(argv[0]);
        g_free(psfile);
    }

    return rc;
}

/**
 * Front-end to the print engine. Expects a postscript file as
 * input.
 *
 * @param psfile the postscript file to print
 *
 * @return True for success otherwise False
 */
static gboolean print_psfile(gchar *psfile)
{
    gboolean rc = False;
    gint i;
    gchar *argv[10];

    gchar print_engine[MAXPATHLEN] = {0};
    if (CP_GetConfigData(IAM_PRINT_ENGINE, print_engine) == True)
    {
        g_message("print_psfile: engine=%s",print_engine);

        i = 0;
        argv[i++] = g_strdup(print_engine);
#ifdef G_OS_WIN32
        argv[i++] = "-query";
        argv[i++] = "-color";
#endif
        argv[i++] = psfile;
        argv[i++] = NULL;
        rc = spawn_command(argv);
        g_free(argv[0]);
    }

    return rc;
}

/**
 * Executes the print command. The argv should contain
 * in the first argument the command to execute. If the
 * standard_out and standard_err are not NULL, then the
 * spawn function will contain any output that the command
 * would normally output to the stdout and stderr.
 *
 * NOTE: GIGO
 * Now for the downside. The g_spawn_sync on WIN32 with the stdout
 * enabled will not work if the buffer exceeds 4016 bytes. This
 * will cause the app to hang. This problem was present in the
 * gtk 2.4.10 version. A bug report was written against this version.
 *
 * @param argv the argument list including the command in the
 * first argument slot.
 *
 * @return True if successful on the start, otherwise False
 */
static gboolean spawn_command(gchar **argv)
{
    GError *error = NULL;
    gchar *standard_output = NULL;
    gchar *standard_error = NULL;
    gint exit_status = 0;

    g_message("spawn_command: in");
    if (g_spawn_sync(NULL, argv, NULL,
                     (GSpawnFlags)(G_SPAWN_SEARCH_PATH),
                     NULL, NULL, &standard_output, &standard_error,
                     &exit_status, &error)
        == False)
    {
        gchar message[256];

        if (error != NULL)
        {
            g_warning(error->message);
            g_sprintf(message,
                      "Unable to start %s executable!\n"
                      "Additional error information: %s",
                      argv[0], error->message);
            g_error_free (error);
        }
        else
        {
            g_sprintf(message,
                      "Unable to start %s executable!\n",
                      argv[0]);
        }

        ErrorMessage(NULL, "Execute Error", message);

        return False;
    }

    return True;
}

/**
 * Sleep command for windows given in seconds.
 *
 * @param wait how glong to wait
 */
#ifdef G_OS_WIN32
static void sleep(gint wait_secs)
{
    gint goal;

    /* convert wait to milliseconds */
    goal = (wait_secs * 1000) + clock();
    while (goal > clock())
        ;
}
#endif
