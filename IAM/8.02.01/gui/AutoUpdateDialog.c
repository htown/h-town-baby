/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #ifndef __USE_BSD
       #define __USE_BSD
    #endif 
    #include <unistd.h> /* for gethostname/getpid */
    #undef __USE_BSD
#endif

#ifdef G_OS_WIN32
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN
    #endif
    #include <winsock2.h> /* for gethostname */
    #include <ws2tcpip.h>
    #include <process.h>  /* for _getpid */
#endif

#define PRIVATE
#include "AutoUpdateDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "IspClient.h"
#include "IspSymbols.h"
#include "MessageHandler.h"
#include "PredictDataHandler.h"
#include "TimeStrings.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/AutoUpdateDialog.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget    *thisDialog         = NULL;
static GtkWidget    *thisStartEntry     = NULL;
static GtkWidget    *thisStopEntry      = NULL;
static GtkWidget    *thisFreqEntry      = NULL;

static gchar        thisStartTime[TIME_STR_LEN] = {UNSET_TIME};
static gchar        thisStopTime [TIME_STR_LEN] = {UNSET_TIME};
static gchar        thisFrequency[TIME_STR_LEN] = {UNSET_TIME};
static gint         thisFreqOffsetSecs  = 0;
/*************************************************************************/

/**
 * This method creates the auto update dialog. This dialog should only
 * be accessible when the user is the MANAGER.
 *
 * @param parent attach dialog to this window
 */
void AUD_Create(GtkWindow *parent)
{
    if (thisDialog == NULL)
    {
        GtkWidget *vbox;
        GtkWidget *hbox;
        GtkWidget *label;

        thisDialog = gtk_dialog_new_with_buttons("Start Auto-Update",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                 GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK(gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(responseCb), NULL);

        /* create box for the horizontal boxes */
        vbox = gtk_vbox_new(True, 5);

        /* create box for time label and entries */
        hbox = gtk_hbox_new(False, 5);

        /* add the horizontal box to the vertical box */
        gtk_box_pack_start(GTK_BOX(vbox), hbox, True, True, 5);

        label = gtk_label_new("Start Time");
        gtk_box_pack_start(GTK_BOX(hbox), label, True, True, 5);

        thisStartEntry = gtk_entry_new();
        g_signal_connect(thisStartEntry, "key-press-event",
                         G_CALLBACK(keyPressTimeCb),
                         NULL);
        gtk_entry_set_max_length(GTK_ENTRY(thisStartEntry), 8);
        gtk_box_pack_start(GTK_BOX(hbox), thisStartEntry, True, True, 5);

        hbox = gtk_hbox_new(False, 5);

        /* add the horizontal box to the vertical box */
        gtk_box_pack_start(GTK_BOX(vbox), hbox, True, True, 5);

        label = gtk_label_new("Frequency");
        gtk_box_pack_start(GTK_BOX(hbox), label, True, True, 5);

        thisFreqEntry = gtk_entry_new();
        g_signal_connect(thisFreqEntry, "key-press-event",
                         G_CALLBACK(keyPressFreqCb),
                         NULL);
        gtk_entry_set_max_length(GTK_ENTRY(thisFreqEntry), 8);
        gtk_box_pack_start(GTK_BOX(hbox), thisFreqEntry, True, True, 5);

        hbox = gtk_hbox_new(False, 5);

        /* add the horizontal box to the vertical box */
        gtk_box_pack_start(GTK_BOX(vbox), hbox, True, True, 5);

        label = gtk_label_new("Stop Time");
        gtk_box_pack_start(GTK_BOX(hbox), label, True, True, 5);

        thisStopEntry = gtk_entry_new();
        g_signal_connect(thisStopEntry, "key-press-event",
                         G_CALLBACK(keyPressTimeCb),
                         NULL);
        gtk_entry_set_max_length(GTK_ENTRY(thisStopEntry), 8);
        gtk_box_pack_start(GTK_BOX(hbox), thisStopEntry, True, True, 5);

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), vbox);
    }
}

/**
 * This method opens the dialog for viewing
 */
void AUD_Open(void)
{
    if (thisDialog != NULL)
    {
        /* initialize the times */
        g_stpcpy(thisStartTime, UNSET_TIME);
        g_stpcpy(thisStopTime, UNSET_TIME);
        g_stpcpy(thisFrequency, UNSET_TIME);

        if (loadAutoConfigData(thisStartTime, thisStopTime, thisFrequency) == True)
        {
            gtk_entry_set_text(GTK_ENTRY(thisStartEntry), thisStartTime);
            gtk_entry_set_text(GTK_ENTRY(thisStopEntry), thisStopTime);
            gtk_entry_set_text(GTK_ENTRY(thisFreqEntry), thisFrequency);

            gtk_widget_show_all(thisDialog);
        }
        else
        {
            ErrorMessage(thisDialog, "Generate Time Error",
                         "Error occurred while trying to generating "
                         "the start and stop times!");
        }
    }
}

/**
 * This function returns the start or stop time to be used by
 * computing the times based upon the offsets stored.
 *
 * @param time_type which time START or STOP
 *
 * @return gchar*
 */
gchar *AUD_GetTime(glong time_type)
{
    gint oSecs;
    glong secs;
    gchar time_str[TIME_STR_LEN];

    if ((g_ascii_strcasecmp(thisStartTime, UNSET_TIME) == 0) ||
        (g_ascii_strcasecmp(thisStopTime,  UNSET_TIME) == 0) ||
        (g_ascii_strcasecmp(thisFrequency, UNSET_TIME) == 0))
    {
        gchar autoStart[TIME_STR_LEN];
        gchar autoStop[TIME_STR_LEN];
        gchar frequency[TIME_STR_LEN];

        if (loadAutoConfigData(autoStart, autoStop, frequency) == True)
        {
            g_stpcpy(thisStartTime, autoStart);
            g_stpcpy(thisStopTime, autoStop);
            g_stpcpy(thisFrequency, frequency);
        }
    }

    oSecs = OffsetToSecs( time_type == UD_START ? thisStartTime : thisStopTime );
    secs  = CurrentUtcSecs() + oSecs;
    SecsToStr( (gint)TS_UTC, secs, time_str );

    return g_strdup(time_str);
}
/**
 * This method retrieves auto update configuration data from the iam config file
 *
 * @param start_time the start time
 * @param stop_time the stop time
 * @param frequency the interval
 *
 * @return True or False
 */
static gboolean loadAutoConfigData(gchar *start_time, gchar *stop_time, gchar *frequency)
{
    gchar time_str[TIME_STR_LEN];

    if (g_ascii_strcasecmp(start_time, UNSET_TIME) == 0)
    {
        if (CP_GetConfigData(IAM_AUTO_UPDATE_START, time_str) == False)
        {
            g_error("IAM_AUTO_UPDATE_START not defined in the config file!");
            return False;
        }
        else
        {
            g_stpcpy(start_time, time_str);
        }
    }

    if (g_ascii_strcasecmp(stop_time, UNSET_TIME) == 0)
    {
        if (CP_GetConfigData(IAM_AUTO_UPDATE_STOP, time_str) == False)
        {
            g_error("IAM_AUTO_UPDATE_STOP not defined in the config file!");
            return False;
        }
        else
        {
            g_stpcpy(stop_time, time_str);
        }
    }

    if (g_ascii_strcasecmp(frequency, UNSET_TIME) == 0)
    {
        if (CP_GetConfigData(IAM_AUTO_UPDATE_FREQUENCY, time_str) == False)
        {
            g_error("IAM_AUTO_UPDATE_FREQUENCY not defined in the config file!");
            return False;
        }
        else
        {
            g_stpcpy(frequency, time_str);
        }
    }

    return True;
}

/**
 * This function validates that the start and stop time fields are correctly
 * formatted.
 *
 * @return True or False
 */
static gboolean validInputData(void)
{
    const gchar *start;
    const gchar *stop;
    const gchar *freq;

    gchar maxHours[32] = {0};
    gint maxStopOffset = 0;

    gint valid = True;

    if (CP_GetConfigData(IAM_MAX_AUTO_UPDATE_HOURS, maxHours) == False)
    {
        ErrorMessage(thisDialog, "File Error",
                     "IAM_MAX_AUTO_UPDATE_HOURS not defined in the config file!\n");
        return False;
    }
    sscanf(maxHours, "%d", &maxStopOffset);

    /* get the start and stop times */
    start = gtk_entry_get_text(GTK_ENTRY(thisStartEntry));
    stop = gtk_entry_get_text(GTK_ENTRY(thisStopEntry));
    freq = gtk_entry_get_text(GTK_ENTRY(thisFreqEntry));

    /* if not valid, reload the original start time */
    if (validOffset((gchar*)start, maxStopOffset) == False)
    {
        gtk_entry_set_text(GTK_ENTRY(thisStartEntry), thisStartTime);
        valid = False;
    }

    /* if not valid, reload the original stop time */
    if (validOffset((gchar*)stop, maxStopOffset) == False)
    {
        gtk_entry_set_text(GTK_ENTRY(thisStopEntry), thisStopTime);
        valid = False;
    }

    /* make sure the stop time is after the start time */
    if (valid && (OffsetToSecs((gchar*)stop) <= OffsetToSecs((gchar*)start)))
    {
        valid = False;
    }

    /* make sure the frequency is also within range */
    if (OffsetToSecs((gchar*)stop) < OffsetToSecs((gchar*)freq) ||
        OffsetToSecs((gchar*)stop) > OffsetToSecs((gchar*)maxHours))
    {
        valid = False;
    }
    else
    {
        thisFreqOffsetSecs = OffsetToSecs((gchar*)freq);
        if (thisFreqOffsetSecs == 0)
        {
            valid = False;
        }
    }

    return valid;
}

/**
 * This function checks to see if the string represents a valid offset between
 * -1:00 and MaxStopOffset, and cleans up the formatting if needed.
 *
 * @return True if valid offset; FALSE otherwise
 */
static gboolean validOffset(gchar *offset, gint maxStopOffset)
{
    gint cv;
    gint h = 0;
    gint m = 0;

    cv = sscanf( offset, "%d:%d", &h, &m );

    if ((cv == 2) || (cv == 1))
    {
        if ((m >= 0) && (m < 60))
        {
            if (((h >= 0) && (h < maxStopOffset))
                || ((h == -1) && (m == 0))
                || ((h == maxStopOffset) && (m == 0)))
            {
                SecsToOffset( 3600 * h + 60 * m, offset );
                return True ;
            }
        }
    }
    return False ;
}

/**
 * This method processes the button responses
 *
 * @param widget the component that got us here
 * @param response_id the action that occurred
 * @param data user data
 */
static void responseCb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            {
                if (validInputData() == True)
                {
                    const gchar *pstr;

                    pstr = gtk_entry_get_text(GTK_ENTRY(thisStartEntry));
                    g_stpcpy(thisStartTime, pstr);
                    pstr = gtk_entry_get_text(GTK_ENTRY(thisStopEntry));
                    g_stpcpy(thisStopTime, pstr);
                    pstr = gtk_entry_get_text(GTK_ENTRY(thisFreqEntry));
                    g_stpcpy(thisFrequency, pstr);

                    PDH_SetAutoTimes(thisStartTime, thisStopTime);
                    PDH_SetFreqOffsetSecs(OffsetToSecs(thisFrequency));

                    /* publish the fact that we're doing auto update */
                    if (AM_GetAutoUpdateMode() == False)
                    {
                        gchar host[256];
                        gchar message[256];
                        gethostname(host, 255);
#ifdef G_OS_WIN32
                        g_sprintf(message, "%s,%d", host, (gint)_getpid());
#else
                        g_sprintf(message, "%s,%d", host, (gint)getpid());
#endif
                        g_message("PUBLISH IS_AntmanAuto message... %s",message);
                        SymbolRec *symbol = IS_GetSymbol(IS_AntmanAuto);
                        IC_PublishIspMsg(message, symbol->symbol_name);
                    }
                }
                else
                {
                    gdk_beep();
                    doUnmanage = False;
                }
            }
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget */
        gtk_widget_destroy(thisDialog);
        thisDialog = NULL;
    }
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean keyPressTimeCb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);
    gint cursor_position = gtk_editable_get_position(GTK_EDITABLE(widget));

    if (g_ascii_isdigit(key) == True ||
        (key == NEGATIVE_SIGN && cursor_position == 0) ||
        (key == POSITIVE_SIGN && cursor_position == 0) ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return False;
    }

    gdk_beep();
    return True;
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean keyPressFreqCb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return False;
    }

    gdk_beep();
    return True;
}
