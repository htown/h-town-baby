/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <math.h>

#include <gtk/gtk.h>

#define PRIVATE
#include "Vector.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/Vector.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");

#undef PRIVATE

/**************************************************************************
* Private Data Definitions
**************************************************************************/

/*************************************************************************/

/**
 * This function sets the x, y, and z fields of a vector.
 *
 * @param vector_A vector of x, y, z values to set
 * @param x x value
 * @param y y value
 * @param z z value
 */
void Vset(VECTOR *vector_A, gdouble x, gdouble y, gdouble z)
{
   vector_A->x = x;
   vector_A->y = y;
   vector_A->z = z;
}


/**
 * This function multiplies a vector by a constant.
 *
 * @param k value used to multiply vector values
 * @param vector_A vector of values
 *
 * @return  pointer to new vector
 */
VECTOR *Vconst(gdouble k, VECTOR *vector_A)
{
   static VECTOR vector_C;

   vector_C.x = vector_A->x*k;
   vector_C.y = vector_A->y*k;
   vector_C.z = vector_A->z*k;

   return(&vector_C);
}


/**
 * This function add two vector.
 *
 * @param vector_A first input vector
 * @param vector_B second input vector
 *
 * @return pointer to new vector
 */
VECTOR *Vadd( VECTOR *vector_A, VECTOR *vector_B )
{
   static VECTOR vector_C;

   vector_C.x = vector_A->x + vector_B->x;
   vector_C.y = vector_A->y + vector_B->y;
   vector_C.z = vector_A->z + vector_B->z;

   return(&vector_C);
}


/**
 * This function subtracts two vectors.
 *
 * @param vector_A first input vector
 * @param vector_B second input vector
 *
 * @return pointer to new vector
 */
VECTOR *Vsub( VECTOR *vector_A, VECTOR *vector_B )
{
   static VECTOR vector_C;

   vector_C.x = vector_A->x - vector_B->x;
   vector_C.y = vector_A->y - vector_B->y;
   vector_C.z = vector_A->z - vector_B->z;

   return(&vector_C);
}

/**
 * This function computes the magnitude of a vector.
 *
 * @param vector input vector
 *
 * @return gdouble magnitute
 */
gdouble Vmag( VECTOR *vector )
{
   gdouble magnitude;

   magnitude = sqrt( SQR(vector->x) + SQR(vector->y) + SQR(vector->z) );
   return(magnitude);
}


/**
 * This function computes the unit vector for a specified vector.
 *
 * @param vector_A input vector
 *
 * @return pointer to new vector
 *  */
VECTOR *Vunit( VECTOR *vector_A )
{
   static VECTOR vector_C;
   gdouble magnitude_A = 0.0;

   magnitude_A = Vmag(vector_A);

   if(magnitude_A == 0.0)
   {
      vector_C.x = vector_C.y = vector_C.z =0.0;
   }
   else
   {
      vector_C.x = vector_A->x / magnitude_A;
      vector_C.y = vector_A->y / magnitude_A;
      vector_C.z = vector_A->z / magnitude_A;
   }
   return(&vector_C);
}


/**
 * This function computes the dot product of two vectors.
 *
 * @param vector_A first input vector
 * @param vector_B second input vector
 *
 * @return gdouble dot product
 */
gdouble Vdot( VECTOR *vector_A, VECTOR *vector_B )
{
   gdouble dot_product;

   dot_product = vector_A->x*vector_B->x +
                 vector_A->y*vector_B->y +
                 vector_A->z*vector_B->z;

   return(dot_product);
}


/**
 * This function computes the cross product of two vectors.
 *
 * @param vector_A first input vector
 * @param vector_B second input vector
 *
 * @return pointer to new vector
 */
VECTOR *Vcross( VECTOR *vector_A, VECTOR *vector_B )
{
    static VECTOR vector_C;

    vector_C.x = vector_A->y*vector_B->z - vector_A->z*vector_B->y;
    vector_C.y = vector_A->z*vector_B->x - vector_A->x*vector_B->z;
    vector_C.z = vector_A->x*vector_B->y - vector_A->y*vector_B->x;

    return(&vector_C);
}


/**
 * This function computes the vector derived from multiplying a vector with
 * a 3x3 matrix.
 *
 * @param ta transformation matrix vector
 * @param vb first input vector
 * @param vc output to this vector
 *
 * @return pointer to new vector
 */
VECTOR *TransformVector( TRANSFORMATION_MATRIX *ta, VECTOR *vb, VECTOR *vc )
{
   vc->x = ta->col_1.x * vb->x  +  ta->col_2.x * vb->y  +  ta->col_3.x * vb->z;

   vc->y = ta->col_1.y * vb->x  +  ta->col_2.y * vb->y  +  ta->col_3.y * vb->z;

   vc->z = ta->col_1.z * vb->x  +  ta->col_2.z * vb->y  +  ta->col_3.z * vb->z;

  return(vc);
}

/**
 * Prints the matrix values.
 *
 * @param matrixName title for output
 * @param m transformation matrix to print
 */
void PrintMatrix(gchar *matrixName, TRANSFORMATION_MATRIX *m)
{
    g_message("Matrix: %s", matrixName);
    g_message(" %f  %f  %f", m->col_1.x, m->col_2.x, m->col_3.x);
    g_message(" %f  %f  %f", m->col_1.y, m->col_2.y, m->col_3.y);
    g_message(" %f  %f  %f\n", m->col_1.z, m->col_2.z, m->col_3.z);
}

/**
 * Prints the vector
 *
 * @param vectorName title for output
 * @param v vector to print
 */
void PrintVector(gchar *vectorName, VECTOR *v)
{
    g_message("Vector: %s", vectorName);
    g_message(" X: %f\n Y: %f\n Z:%f\n", v->x, v->y, v->z);
}
