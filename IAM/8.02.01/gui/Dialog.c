/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
#endif

#define PRIVATE
#include "Dialog.h"
#undef PRIVATE

#include "ColorHandler.h"
#include "ConfigParser.h"
#include "FontHandler.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "WindowGrid.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/Dialog.c 256 2017-01-26 22:05:07Z dpham1@ndc.nasa.gov $");



/**************************************************************************
* Private Data Definitions
**************************************************************************/
static PangoFontDescription *thisLargeBoldFont      = NULL;
static PangoFontDescription *thisLargePlainFont     = NULL;
static PangoFontDescription *thisDefaultBoldFont    = NULL;
static PangoFontDescription *thisDefaultPlainFont   = NULL;
static PangoFontDescription *thisSmallBoldFont      = NULL;
static PangoFontDescription *thisSmallPlainFont     = NULL;

static gchar thisBgColor    [64] = {0};
static gchar thisFgColor    [64] = {0};
static gchar thisDegreeColor[64] = {0};

/* define the index into the mask data that will be changed */
#define MASK_INDEX 2

/* this is just some mask made up */
static const gchar *xpm_mask_data16[] = {
/* width height num_colors chars_per_pixel */
    "16 16 2 1",
/* colors */
    ". c #5A5A5A",
    "# c #FFFFFF",
/* pixels */
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#.",
    ".#.#.#.#.#.#.#.#",
    "#.#.#.#.#.#.#.#."
};

/**
 * Coordinate tracking area
 */
static Area         thisTrackingArea;

/**
 * SBand tracking limit value
 */
static gint          thisSBandTrackingLimit  = 150;

/*************************************************************************/

/**
 * Dialog constructor used to initialize some class variables
 */
void D_DialogConstructor(void)
{
    gchar fg_color[64];
    gchar bg_color[64];
    gchar degree_color[64];
    gchar temp[32];

    thisLargeBoldFont = pango_font_description_from_string(FH_GetLargeBoldFont());
    thisLargePlainFont = pango_font_description_from_string(FH_GetLargePlainFont());

    thisDefaultBoldFont = pango_font_description_from_string(FH_GetDefaultBoldFont());
    thisDefaultPlainFont = pango_font_description_from_string(FH_GetLargePlainFont());

    thisSmallBoldFont = pango_font_description_from_string(FH_GetSmallBoldFont());
    thisSmallPlainFont = pango_font_description_from_string(FH_GetSmallPlainFont());

    if (CP_GetConfigData(IAM_FG_COLOR, fg_color) == True)
    {
        g_stpcpy(thisFgColor, fg_color);
    }
    else
    {
        g_stpcpy(thisFgColor, FG_COLOR);
    }

    if (CP_GetConfigData(IAM_BG_COLOR, bg_color) == True)
    {
        g_stpcpy(thisBgColor, bg_color);
    }
    else
    {
        g_stpcpy(thisBgColor, BG_COLOR);
    }

    if (CP_GetConfigData(IAM_DEGREE_COLOR, degree_color) == True)
    {
        g_stpcpy(thisDegreeColor, degree_color);
    }
    else
    {
        g_stpcpy(thisDegreeColor, DEGREE_COLOR);
    }

    thisTrackingArea.count = 0;

    /* get sband tracking limit value */
    if (CP_GetConfigData(SBAND_COORD_TRACKING_LIMIT, temp) == False)
    {
        thisSBandTrackingLimit = 150;
    }
    else
    {
        sscanf(temp, "%d", &thisSBandTrackingLimit);
    }
}

/**
 * Sets an icon for the application
 *
 * @param window add icon to this widget
 */
void D_SetIcon(GtkWidget *window)
{
    GdkPixbuf *icon;
    GError *error = NULL;
    gchar image_icon[MAXPATHLEN];

    if (window != NULL)
    {
        if (CP_GetConfigData(IAM_IMAGE_ICON, image_icon) == True)
        {
            if ((icon = gdk_pixbuf_new_from_file(image_icon, &error)) != NULL)
            {
                gtk_window_set_icon(GTK_WINDOW(window), icon);
            }
            else
            {
                if (error != NULL)
                {
                    g_warning(error->message);
                    g_error_free(error);
                }
            }
        }
    }
}

/**
 * Deallocates old pixmap, then allocates a new one. This action causes
 * a redraw to occur
 *
 * @param windata the window data
 */
void D_CreatePixmap(WindowData *windata)
{
    /**
     * verify the data structure, drawing area, drawing area's window are valid
     */
    if (windata == NULL ||
        windata->drawing_area == NULL ||
        windata->drawing_area->window == NULL)
    {
        return;
    }

    /* if size changed, allocate a new one, otherwise, just clear it */
    if (windata->width  != windata->drawing_area->allocation.width ||
        windata->height != windata->drawing_area->allocation.height)
    {
        /**
         * deallocate old pixmaps
         */
        if (windata->src_pixmap != NULL)
        {
            g_object_unref(windata->src_pixmap);
            windata->src_pixmap = NULL;
        }

        if (windata->bg_pixmap != NULL)
        {
            g_object_unref(windata->bg_pixmap);
            windata->bg_pixmap = NULL;
        }

        /**
         * save size of drawing area, if the user selects a different notebook
         * tab, then resizes the window, the previous tabs will not know the
         * window was resized. the resize change will have to be checked in
         * the expose event
         */
        windata->width = windata->drawing_area->allocation.width;
        windata->height = windata->drawing_area->allocation.height;

        /* allocate src pixmap */
        windata->src_pixmap = gdk_pixmap_new(windata->drawing_area->window,
                                             windata->drawing_area->allocation.width,
                                             windata->drawing_area->allocation.height,
                                             -1);
        /* clear pixmap */
        AM_ClearDrawable(GDK_DRAWABLE(windata->src_pixmap),
                         windata->drawing_area->style->bg_gc[GTK_STATE_NORMAL],
                         0, 0,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height);

        /* allocate bg pixmap */
        windata->bg_pixmap = gdk_pixmap_new(windata->drawing_area->window,
                                            windata->drawing_area->allocation.width,
                                            windata->drawing_area->allocation.height,
                                            -1);
        /* clear pixmap */
        AM_ClearDrawable(GDK_DRAWABLE(windata->bg_pixmap),
                         windata->drawing_area->style->bg_gc[GTK_STATE_NORMAL],
                         0, 0,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height);

        /* pixmap changed, now we need to redraw the components at least once */
        windata->redraw_pixmap = True;
    }
}

/**
 * Copies the src drawable to the dst drawable
 *
 * @param windata the window data
 */
void D_DrawDrawable(WindowData *windata)
{
    /* copy the main pixmap to the drawing area's drawable */
    copy_drawable(windata,
                  windata->drawing_area->style->fg_gc[GTK_STATE_NORMAL],
                  AM_Drawable(windata, DST_DRAWABLE),
                  AM_Drawable(windata, SRC_DRAWABLE));

    /* redraw no longer needed, just render the pixmap */
    windata->redraw_pixmap = False;
}

/**
 * This method copies the source drawable to the destination drawable using the
 * supplied gc. The destination drawable can be either a window (drawable) or another
 * pixmap.
 *
 * @param windata the window data
 * @param src_gc graphics context used for copy operation
 * @param dst destination drawable, must be window or pixmap
 * @param src source drawable, must be window or pixmap
 */
static void copy_drawable(WindowData *windata, GdkGC *src_gc, GdkDrawable *dst, GdkDrawable *src)
{
    if (dst != src)
    {
        GdkGC *dst_gc;
        GdkGCValues _values;

        _values.function = GDK_COPY;
        dst_gc = gdk_gc_new_with_values(dst, &_values, GDK_GC_FUNCTION);

        gdk_draw_drawable(GDK_DRAWABLE(dst), dst_gc, GDK_DRAWABLE(src),
                          0, 0,  /* dst x, y */
                          0, 0,  /* src x, y */
                          -1, -1 /* src width, height, use ALL */ );

        g_object_unref(dst_gc);
    }
}

/**
 * This method saves the location parameters. These values are later
 * used to clear the drawable of any previously written data.
 *
 * @param windata the window data
 * @param area holds screen data
 * @param x x location
 * @param y y location
 * @param width width
 * @param height height
 */
void D_AddArea(WindowData *windata, Area *area, gint x, gint y, gint width, gint height)
{
    if (GTK_WIDGET_REALIZED(windata->drawing_area) == True)
    {
        if (width > 0 && height > 0)
        {
            gboolean found = False;

            if (area->count < MAX_AREAS)
            {
                gint i;
                for (i=0; i<area->count; i++)
                {
                    if (area->rect[i][AREA_X] == x     && area->rect[i][AREA_Y] == y &&
                        area->rect[i][AREA_W] == width && area->rect[i][AREA_H] == height)
                    {
                        found = True;
                    }
                }

                if (found == False)
                {
                    area->rect[area->count][AREA_X] = x;
                    area->rect[area->count][AREA_Y] = y;
                    area->rect[area->count][AREA_W] = width;
                    area->rect[area->count][AREA_H] = height;

                    area->count++;
                }
            }
        }
    }
}

/**
 * Refreshes the areas recently added, then releases the pixmap buffer and resets
 * the count to zero
 *
 * @param windata the window data
 * @param dst_drawable restore this drawable
 * @param src_drawable use this drawable for restoring the destination
 * @param area has the size constraints for the source drawable
 * @param isResizing flag indicating that the window is in resize mode
 */
void D_RefreshAreas(WindowData *windata, GdkDrawable *dst_drawable, GdkDrawable *src_drawable, Area *area, gboolean isResizing)
{
    gint i;

    if (isResizing == False)
    {
        if (area->count > 0 && dst_drawable != NULL && src_drawable != NULL)
        {
            GdkGC *gc;
            GdkGCValues _values;

            /* get gc for copy */
            _values.function = GDK_COPY;
            gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

            /* clear the drawing area with source pixmap */
            for (i=0; i<area->count; i++)
            {
                gdk_draw_drawable(dst_drawable, gc, src_drawable,
                                  area->rect[i][AREA_X], area->rect[i][AREA_Y],
                                  area->rect[i][AREA_X], area->rect[i][AREA_Y],
                                  area->rect[i][AREA_W], area->rect[i][AREA_H]);
            }

            /* done with gc */
            g_object_unref(gc);

            /* clear the area count */
            area->count = 0;
        }
    }
}

/**
 * Returns the foreground color to use
 * This value can be changed by defining in iam.xml the value IAM_FG_COLOR
 *
 * @return foreground color
 */
gchar *D_FgColor(void)
{
    return(thisFgColor);
}

/**
 * Returns the background color to use
 * This value can be changed by defining in iam.xml the value IAM_BG_COLOR
 *
 * @return background color
 */
gchar *D_BgColor(void)
{
    return(thisBgColor);
}

/**
 * Returns the color to use for the degree symbol
 * This value can be changed by defining in iam.xml the value IAM_DEGREE_COLOR
 *
 * @return degree color
 */
gchar *D_DegreeColor(void)
{
    return(thisDegreeColor);
}

/**
 * Creates a frame with a background color and adds the widget to the frame
 *
 * @param widget place frame around this widget
 *
 * @return frame the frame that has the widget
 */
GtkWidget *D_CreateDataFrame(GtkWidget *widget)
{
    GtkWidget *event_box;
    GdkColor *gcolor;

    GtkWidget *data_frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(data_frame), GTK_SHADOW_IN);
    gtk_container_set_border_width(GTK_CONTAINER(data_frame), 0);

    event_box = gtk_event_box_new();
    gcolor = CH_GetGdkColorByName(thisBgColor);
    if (gcolor != NULL)
    {
        gtk_widget_modify_bg(GTK_WIDGET(event_box), GTK_STATE_NORMAL, gcolor);
    }
    gtk_container_add(GTK_CONTAINER(event_box), widget);
    gtk_container_add(GTK_CONTAINER(data_frame), event_box);

    return(data_frame);
}

/**
 * Creates a frame with a label, adds the widget to the frame. Uses the
 * default background (defined in iam.xml) as the color.
 *
 * @param frame_title the title found on the frame edge
 * @param widget place frame around this widget
 *
 * @return frame the frame that has the widget
 */
GtkWidget *D_CreateDataFrameWithLabel(gchar *frame_title, GtkWidget *widget)
{
    GtkWidget *event_box;
    GdkColor *gcolor;

    GtkWidget *data_frame = gtk_frame_new(frame_title);
    gtk_frame_set_shadow_type(GTK_FRAME(data_frame), GTK_SHADOW_ETCHED_IN);
    gtk_container_set_border_width(GTK_CONTAINER(data_frame), 1);

    event_box = gtk_event_box_new();
    gcolor = CH_GetGdkColorByName(thisBgColor);
    if (gcolor != NULL)
    {
        gtk_widget_modify_bg(GTK_WIDGET(event_box), GTK_STATE_NORMAL, gcolor);
    }
    gtk_container_add(GTK_CONTAINER(event_box), widget);
    gtk_container_add(GTK_CONTAINER(data_frame), event_box);

    return(data_frame);
}

/**
 * Creates a label with the label_name, using the horizontal alignment
 * provided and the foreground text color.
 * Vertical alignment is centered.
 *
 * @param label_name the text for the label
 * @param horizontal_alignment placement of text 0.0 = LEFT, 0.5 = CENTER, 1.0 = RIGHT
 * @param text_color color for label
 *
 * @return label widget
 */
GtkWidget *D_CreateLabel(gchar *label_name, gdouble horizontal_alignment, gchar *text_color)
{
    GdkColor *gcolor;
    GtkWidget *label = gtk_label_new(label_name);
    gtk_label_set_line_wrap(GTK_LABEL(label), False);
    gtk_label_set_use_markup(GTK_LABEL(label), False);

    gtk_misc_set_alignment(GTK_MISC(label), (float)horizontal_alignment, CENTER_ALIGN);

    if (text_color != NULL)
    {
        gcolor = CH_GetGdkColorByName(text_color);
        if (gcolor != NULL)
        {
            gtk_widget_modify_fg(label, GTK_STATE_NORMAL, gcolor);
        }
    }

    return(label);
}

/**
 * Sets the label text. If the foreground color is provided,
 *
 * @param label the label widget
 * @param text new text to use
 * @param fg_color optional foreground color for the text
 */
void D_SetLabelText(GtkWidget *label, gchar *text, GdkColor *fg_color)
{
    if (label != NULL)
    {
        gtk_label_set_text(GTK_LABEL(label), text);
        if (fg_color != NULL)
        {
            gtk_widget_modify_fg(label, GTK_STATE_NORMAL, fg_color);
        }
    }
}

/**
 * Sets the label color. The text color is used to set the label's text.
 *
 * @param label the label widget
 * @param text_color optional foreground text color
 */
void D_SetLabelColor(GtkWidget *label, gchar *text_color)
{
    GdkColor *gcolor = NULL;
    if (label != NULL)
    {
        if (text_color != NULL)
        {
            gcolor = CH_GetGdkColorByName(text_color);
            if (gcolor == NULL)
            {
                g_warning("D_SetLabelColor: text color name not in table\n");
            }
            else
            {
                gtk_widget_modify_fg(GTK_WIDGET(label), GTK_STATE_NORMAL, gcolor);
            }
        }
    }
}

/**
 * Sets the text color and the background color
 *
 * @param widget set this widgets text and background color
 * @param text_color text color
 * @param bg_color background color
 */
void D_SetTextColor(GtkWidget *widget, gchar *text_color, gchar *bg_color)
{
    GdkColor *gdk_text_color;
    GdkColor *gdk_bg_color;

    /* set the background color for the widget? */
    if (bg_color != NULL)
    {
        gdk_bg_color = CH_GetGdkColorByName(bg_color);
        if (gdk_bg_color == NULL)
        {
            g_warning("D_SetTextColor: bg color name not in table\n");
        }
        else
        {
            gtk_widget_modify_base(widget, GTK_STATE_NORMAL, gdk_bg_color);

            /* set the text color? */
            if (text_color != NULL)
            {
                gdk_text_color = CH_GetGdkColorByName(text_color);
                if (gdk_text_color == NULL)
                {
                    g_warning("D_SetTextColor: text color name not in table\n");
                }
                else
                {
                    set_cursor_color(widget, gdk_text_color);

                    gtk_widget_modify_fg(widget, GTK_STATE_NORMAL, gdk_text_color);
                    gtk_widget_modify_text(widget, GTK_STATE_NORMAL, gdk_text_color);
                }
            }
        }
    }
    else
    {
        /* only set the text color ? */
        if (text_color != NULL)
        {
            gdk_text_color = CH_GetGdkColorByName(text_color);
            if (gdk_text_color == NULL)
            {
                g_warning("D_SetTextColor: text color name not in table\n");
            }
            else
            {
                gtk_widget_modify_text(widget,GTK_STATE_NORMAL, gdk_text_color);
            }
        }
    }
}

/**
 * Sets the cursor color to the specified color.
 *
 * @param widget the widget that has the cursor
 * @param cursor_color the new cursor color
 */
static void set_cursor_color(GtkWidget *widget, GdkColor *cursor_color)
{
    gchar style_cursor_color[256];
    const gchar *widget_name = gtk_widget_get_name(widget);
    gchar *resource_type = "widget";

    /* if the widget returns the class name, then it */
    /* was not set, so change the resource type to */
    /* to class. the side-effect though is all entry */
    /* widgets throughout the app will use this color */
    if (g_strcasecmp(widget_name, "GtkEntry") == 0)
    {
        resource_type = "class";
    }

    g_sprintf(style_cursor_color,
              "style \"cursor_color_style\" {\n"
              "    GtkEntry::cursor_color = \"#%02x%02x%02x\"\n"
              "}\n"
              "%s \"%s\" style \"cursor_color_style\"\n",
              (cursor_color->red>>8),
              (cursor_color->green>>8),
              (cursor_color->blue>>8),
              resource_type,
              widget_name);

    gtk_rc_parse_string(style_cursor_color);
}

/**
 * Concatenates the menus together.
 *
 * @param menu1 first menu
 * @param menu2 second menu
 * @param menu3 third menu
 * @param menu4 fourth menu
 * @param menu5 fifth menu
 *
 * @return xml user interface that describes the menubar
 */
gchar *D_CreateMenu(const gchar *menu1, const gchar *menu2, const gchar *menu3, const gchar *menu4, const gchar *menu5)
{
    return(g_strconcat("<ui>\n",
                       "   <menubar name='", MENU_BAR, "'>\n",
                       menu1, menu2, menu3, menu4, menu5,
                       "   </menubar>\n",
                       "</ui>\n",
                       NULL));
}

/**
 * This method adds the blockage items to the pull-right on the blockage drop-down for
 * the sband, kuband and station
 *
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param ui user interface menu
 * @param menu_name blockage menu to add
 * @param callback a callback for the new blockage
 *
 * @return the action group or NULL
 */
GtkActionGroup *D_AddBlockageItemsToMenubar(gint display, gint coord, GtkUIManager *ui, gchar *menu_name, GCallback callback)
{
    guint i;
    guint merge_id = -1;
    GtkActionGroup *action_group = NULL;

    GtkWidget *menu = gtk_ui_manager_get_widget(ui, menu_name);
    if (menu != NULL)
    {
        action_group = gtk_action_group_new (menu_name);
        merge_id = gtk_ui_manager_new_merge_id(ui);
        gtk_ui_manager_insert_action_group (ui, action_group, 0);

        for (i=0; i<MF_GetMaskFileCount(display, coord); i++)
        {
            if (MF_MaskFileExists(display, coord, i) == True &&
                MF_IsLimitMask(display, coord, i) == False)
            {
                GtkToggleActionEntry toggle_entry;
                gchar action_name[255];
                gchar *mask_name = MF_GetMaskName(display, coord, i);

                g_sprintf(action_name, "%s/%s",menu_name, mask_name);

                toggle_entry.name = action_name;
                toggle_entry.stock_id = NULL;

                toggle_entry.label = mask_name;
                toggle_entry.accelerator = NULL;

                toggle_entry.tooltip = NULL;

                toggle_entry.callback = callback;
                toggle_entry.is_active = False;

                gtk_action_group_add_toggle_actions (action_group, &toggle_entry, 1, NULL);

                gtk_ui_manager_add_ui(ui, merge_id,
                                      menu_name, mask_name, action_name,
                                      GTK_UI_MANAGER_MENUITEM, False);
            }
        }
    }

    return action_group;
}

/**
 * This method adds the structure items to the pull-right on the settings drop-down.
 *
 * @param display current display (REALTIME or PREDICT)
 * @param ui user interface menu
 * @param menu_name strcuture menu to add to
 * @param callback a callback for the new structure
 *
 * @return merge id for this add
 */
GtkActionGroup *D_AddStructureItemsToMenuBar(gint display, GtkUIManager *ui, gchar *menu_name, GCallback callback)
{
    guint i;
    guint merge_id = -1;
    GtkActionGroup *action_group = NULL;

    GtkWidget *menu = gtk_ui_manager_get_widget(ui, menu_name);
    if (menu != NULL)
    {
        GPtrArray *ptr_array;
        action_group = gtk_action_group_new (menu_name);
        merge_id = gtk_ui_manager_new_merge_id(ui);

        ptr_array = DD_GetStructureNames();
        if (ptr_array)
        {
            /* define enough entries for future */
            GtkRadioActionEntry radio_entry[32];

            /* get current structure */
            /* we want to make this one active */
            gchar *structure = DD_GetActiveStructure();
            gint structure_val = -1;

            /* get all structures defined */
            for (i=0; i<ptr_array->len; i++)
            {
                gchar *structure_name = (gchar *)g_ptr_array_index(ptr_array, i);
                gchar *action_name = g_strconcat(menu_name, "/",
                                                 structure_name, NULL);

                radio_entry[i].name = action_name;
                radio_entry[i].stock_id = NULL;

                radio_entry[i].label = structure_name;
                radio_entry[i].accelerator = NULL;

                radio_entry[i].tooltip = NULL;
                radio_entry[i].value = i;

                gtk_ui_manager_add_ui(ui, merge_id,
                                      menu_name, structure_name, action_name,
                                      GTK_UI_MANAGER_MENUITEM, False);
            }

            /* find the structure that is currently active */
            /* and make it the selected one */
            if (structure)
            {
                for (i=0; i<ptr_array->len; i++)
                {
                    if (g_ascii_strcasecmp(structure, radio_entry[i].label) == 0)
                    {
                        structure_val = i;
                        break;
                    }
                }
            }

            gtk_action_group_add_radio_actions (action_group, radio_entry, ptr_array->len,
                                                structure_val, G_CALLBACK(callback), NULL);
            gtk_ui_manager_insert_action_group (ui, action_group, 0);

            /* release memory */
            g_ptr_array_free(ptr_array, True);

        }
    }

    return action_group;
}

/**
 * This method adds the blockage items to the pull-right on the blockage drop-down for
 * the sband, kuband and station
 *
 * @param display which dialog is making the call
 * @param coord which coordinate, used for getting the proper mask
 * @param toolbar toolbar for the mask buttons
 * @param tool_items list for tool item instances and handler ids
 * @param blockage_name a name uniquely identifying the blockage
 * @param callback callback for the mask button
 */
void D_AddBlockageItemsToToolbar(gint display, gint coord, GtkWidget *toolbar, GList **tool_items, gchar *blockage_name, GCallback callback)
{
    guint i;

    if (toolbar != NULL)
    {
        GtkWidget *window = (display == REALTIME ? RTD_GetDialog() : PD_GetDialog());

        gchar *mask_data[G_N_ELEMENTS(xpm_mask_data16)];
        for (i=0; i<G_N_ELEMENTS(xpm_mask_data16); i++)
        {
            mask_data[i] = g_strdup(xpm_mask_data16[i]);
        }

        for (i=0; i<MF_GetMaskFileCount(display, coord); i++)
        {
            if (MF_MaskFileExists(display, coord, i) == True &&
                MF_IsLimitMask(display, coord, i) == False)
            {
                GdkColor *gcolor;
                WidgetHandlerData *tool_data;
                GtkTooltips *tool_tips = gtk_tooltips_new();
                gchar *mask_name = MF_GetMaskName(display, coord, i);
                gint mask_color = MF_GetMaskColor(display, coord, i);
                gchar fg_mask_color[32];

                tool_data = (WidgetHandlerData *)MH_Calloc( sizeof(WidgetHandlerData ), __FILE__, __LINE__);
                tool_data->instance = (void *)gtk_toggle_tool_button_new();

                gcolor = CH_GetGdkColorByIndex(mask_color);
                if (gcolor != NULL)
                {
                    GdkPixmap *pixmap;
                    GtkWidget *image;

                    /* make color based on the mask color */
                    /* the gdkcolor values range from 0-65535 */
                    /* we need to get the values from 0-255 */
                    g_free(mask_data[MASK_INDEX]);
                    g_sprintf(fg_mask_color, "# c #%02x%02x%02x",
                              (gcolor->red>>8),
                              (gcolor->green>>8),
                              (gcolor->blue>>8));
                    mask_data[MASK_INDEX] = g_strdup(fg_mask_color);

                    pixmap = gdk_pixmap_create_from_xpm_d(window->window, NULL, NULL, mask_data);
                    image = gtk_image_new_from_pixmap (pixmap, NULL);

                    gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(tool_data->instance), image);
                }

                /* set the label, this info will be used later to get the mask */
                gtk_tool_button_set_label(GTK_TOOL_BUTTON(tool_data->instance), mask_name);
                gtk_tool_item_set_tooltip(GTK_TOOL_ITEM(tool_data->instance), tool_tips,
                                          mask_name, mask_name);

                /* need the handler id for this instance */
                tool_data->handler_id = g_signal_connect(tool_data->instance, "clicked", G_CALLBACK(callback), NULL);

                gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(tool_data->instance), -1);

                /* add tool instance data to list */
                (*tool_items) = g_list_append((*tool_items), tool_data);
            }
        }

        for (i=0; i<G_N_ELEMENTS(xpm_mask_data16); i++)
        {
            g_free(mask_data[i]);
        }
    }
}

/**
 * Updates the menu items by setting the sensitivity of the pull-rights. This is
 * based on the current coordinate type selected.
 *
 * @param ui user interface menu
 * @param display current display (REALTIME or PREDICT)
 * @param coord current coordinate type
 */
void D_UpdateBlockageItems(GtkUIManager *ui, gint display, gint coord, gint kuband_displayed)
{
    switch (coord)
    {
        case SBAND1:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  True);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  False);
            set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
            set_menu_items(ui, display, KUBAND2, "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU, False);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);
            break;

        case SBAND2:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  False);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  True);
            set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
            set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  False);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);
            break;

        case KUBAND:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  False);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  False);
            set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  True);
            set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  False);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);
            break;

        case KUBAND2:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  False);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  False);
            set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
            set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  True);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);
            break;

        case STATION:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  False);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  False);
            set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
            set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  False);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, True);
            break;

        case SBAND1_KUBAND:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  True);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  False);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);

            if (kuband_displayed == 1)
            {
                set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  True);
                set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  False);
            }
            else
            {
                set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
                set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  True);
            }
            break;

        case SBAND2_KUBAND:
            set_menu_items(ui, display, SBAND1,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,  False);
            set_menu_items(ui, display, SBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,  True);
            set_menu_items(ui, display, STATION, "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU, False);

            if (kuband_displayed == 1)
            {
                set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  True);
                set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  False);
            }
            else
            {
                set_menu_items(ui, display, KUBAND,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,  False);
                set_menu_items(ui, display, KUBAND2,  "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,  True);
            }
            break;

        default:
            break;
    }
}

/**
 * Sets the sensitivity of the menu items in the ui manager
 *
 * @param ui user interface menu
 * @param display current display type (REALTIME or PREDICT)
 * @param coord current coordinate type
 * @param blockage_name blockage name to set
 * @param visible flag indicating sensitivity
 */
static void set_menu_items(GtkUIManager *ui, gint display, gint coord, gchar *blockage_name, gboolean visible)
{
    guint i;
    GtkWidget *widget = gtk_ui_manager_get_widget(ui, blockage_name);

    if (widget != NULL)
    {
        if (coord == KUBAND || coord == KUBAND2)
        {
            if (visible)
                gtk_widget_show(widget);
            else
                gtk_widget_hide(widget);
        }

        gtk_widget_set_sensitive(widget, visible);

        for (i=0; i<MF_GetMaskFileCount(display, coord); i++)
        {
            if (MF_MaskFileExists(display, coord, i))
            {
                gchar menu_name[255];
                gchar *mask_name = MF_GetMaskName(display, coord, i);
                g_sprintf(menu_name, "%s/%s",blockage_name, mask_name);
                widget = gtk_ui_manager_get_widget(ui, menu_name);
                if (widget != NULL)
                {
                    gtk_widget_set_sensitive(widget, visible);
                }
            }
        }
    }
}

/**
 * Sets the sensitivity of the menu_item.
 *
 * @param ui the user interface menu
 * @param menu_item the menu to enable/disable
 * @param enable the flag, either True or False
 */
void D_EnableMenuItem(GtkUIManager *ui, gchar *menu_item, gboolean enable)
{
    GtkWidget *menu = gtk_ui_manager_get_widget(ui, menu_item);
    if (menu != NULL)
    {
        gtk_widget_set_sensitive(menu, enable);
    }
}

/**
 * This method selects menu item check items, this will cause the callback to be called
 *
 * @param ui user interface menu
 * @param menu_item menu item to select
 */
void D_PreselectMenuItem(GtkUIManager *ui, gchar *menu_item)
{
    /*
    ** select the item
    ** if a callback has been registered with this item, this will invoke the callback
    */
    GtkWidget *widget = gtk_ui_manager_get_widget(ui, menu_item);
    if (widget != NULL)
    {
        gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (widget), True);
    }
}

/**
 * Adds the left, center and bottom into a single container.
 *
 * @param name name for container
 * @param container the container that has left, bottom, and center components
 * @param left the left component
 * @param bottom the bottom component
 * @param center the center component
 */
void D_ContainerAdd(const gchar *name, GtkWidget **container, GtkWidget **left, GtkWidget **bottom, GtkWidget **center)
{
    GtkWidget *hbox = gtk_hbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (hbox), *left, False, False, 0);
    gtk_box_pack_start (GTK_BOX (hbox), *center, True, True, 0);

    (*container) = gtk_vbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (*container), hbox, True, True, 0);
    gtk_box_pack_start (GTK_BOX (*container), *bottom, False, False, 0);

    gtk_widget_set_name(*container,  name);
}

/**
 * Create a notebook for the three coordinate types (sband, kuband, and station)
 *
 * @param box container for notebook widget
 *
 * @return notebook widget
 */
GtkWidget *D_CreateNotebook(GtkWidget *box)
{
    GtkWidget *notebook = gtk_notebook_new();

    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
    gtk_notebook_set_show_tabs(GTK_NOTEBOOK(notebook), True);
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(notebook), True);
    gtk_box_pack_start(GTK_BOX(box), notebook, True, True, 0);
    gtk_widget_realize(notebook);

    return(notebook);
}

/**
 * Create the components that contains the ku-band background data
 *
 * @param notebook notebook widget
 * @param tab_name name for notebook tab
 * @param widget widget to add to notebook
 */
void D_NotebookAdd(GtkWidget *notebook, gchar *tab_name, GtkWidget **widget)
{
    GtkWidget *tab_label;

    tab_label = gtk_label_new(tab_name);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), *widget, tab_label);
}

/**
 * Adds two components, usally sband and kuband to a notebook. Based on
 * the orientation specified, the two components are layed out either
 * left and right or top and bottom.
 *
 * @param notebook add to this notebook
 * @param tab_name name for the tab
 * @param view_orientation either horizontal or vertical
 * @param lt_widget the left-top widget
 * @param rb_widget right-bottom widget
 *
 * @return container that has both widget components
 */
GtkWidget *D_NotebookAdd2(GtkWidget *notebook, gchar *tab_name, gint view_orientation, GtkWidget **lt_widget, GtkWidget **rb_widget)
{
    GtkWidget *box;
    GtkWidget *tab_label;

    if (view_orientation == View_Orientation_Vertical)
    {
        box = gtk_vbox_new(True, 1);
    }
    else
    {
        box = gtk_hbox_new(True, 1);
    }

    gtk_box_pack_start(GTK_BOX(box), *lt_widget, True, True, 0);
    gtk_box_pack_start(GTK_BOX(box), *rb_widget, True, True, 0);

    tab_label = gtk_label_new(tab_name);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, tab_label);

    return(box);
}

/**
 * Create the components that contains the ku-band background data
 *
 * @param notebook add to this notebook
 * @param tab_name name for the tab
 * @param left left widget to add
 * @param bottom bottom widget to add
 * @param center center widget
 */
void D_NotebookAdd3(GtkWidget *notebook, gchar *tab_name, GtkWidget **left, GtkWidget **bottom, GtkWidget **center)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *tab_label;

    hbox = gtk_hbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (hbox), *left, False, False, 0);
    gtk_box_pack_start (GTK_BOX (hbox), *center, True, True, 0);

    vbox = gtk_vbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, True, True, 0);
    gtk_box_pack_start (GTK_BOX (vbox), *bottom, False, False, 0);

    tab_label = gtk_label_new(tab_name);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), vbox, tab_label);
}

/**
 * Create the components that contains the ku-band background data
 *
 * @param notebook where to put the components
 * @param tab_name a name for the notebook tab
 * @param view_orientation vertical or horizontal
 * @param right usally this is sband
 * @param left the kuband left edge
 * @param bottom the kuband bottom edge
 * @param center the kuband view itself
 *
 * @return the container
 */
GtkWidget *D_NotebookAdd4(GtkWidget *notebook, gchar *tab_name, gint view_orientation, GtkWidget **right, GtkWidget **left, GtkWidget **bottom, GtkWidget **center)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *box;
    GtkWidget *tab_label;

    hbox = gtk_hbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (hbox), *left, False, False, 0);
    gtk_box_pack_start (GTK_BOX (hbox), *center, True, True, 0);

    vbox = gtk_vbox_new(False, 1);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, True, True, 0);
    gtk_box_pack_start (GTK_BOX (vbox), *bottom, False, False, 0);

    if (view_orientation == View_Orientation_Vertical)
    {
        box = gtk_vbox_new(True, 1);
    }
    else
    {
        box = gtk_hbox_new(True, 1);
    }

    gtk_box_pack_start(GTK_BOX(box), *right, True, True, 0);
    gtk_box_pack_start(GTK_BOX(box), vbox, True, True, 0);

    tab_label = gtk_label_new(tab_name);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, tab_label);

    return(box);
}

/**
 * Creates a drawing area widget within a frame.
 *
 * @param container place drawing area into this
 * @param name name for container widget
 * @param toolbar toolbar for this view
 * @param width width of frame
 * @param height height of frame
 *
 * @return drawing area widget
 */
GtkWidget *D_CreateDrawingArea(GtkWidget **container, const gchar *name, GtkWidget *handle_box, GtkWidget *handle_box2, GtkWidget *toolbar, GtkWidget *toolbar2, gint width, gint height)
{
    GtkWidget *drawing_area;

    (*container) = gtk_vbox_new(False, 0);
    gtk_widget_set_size_request(GTK_WIDGET(*container), width, height);
    gtk_widget_set_name(*container,  name);

    if (toolbar != NULL)
    {
        gtk_toolbar_set_tooltips(GTK_TOOLBAR(toolbar), True);
        gtk_toolbar_set_show_arrow(GTK_TOOLBAR(toolbar), True);
        gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
        gtk_container_add(GTK_CONTAINER(handle_box), toolbar);
        gtk_box_pack_start(GTK_BOX(*container), handle_box, False, False, 5);

        if (toolbar2 != NULL)
        {
            gtk_toolbar_set_tooltips(GTK_TOOLBAR(toolbar2), True);
            gtk_toolbar_set_show_arrow(GTK_TOOLBAR(toolbar2), True);
            gtk_toolbar_set_style(GTK_TOOLBAR(toolbar2), GTK_TOOLBAR_ICONS);
            gtk_container_add(GTK_CONTAINER(handle_box2), toolbar2);
            gtk_box_pack_start(GTK_BOX(*container), handle_box2, False, False, 5);
        }
    }

    drawing_area = gtk_drawing_area_new();
    gtk_box_pack_start(GTK_BOX(*container), drawing_area, True, True, 0);

    gtk_widget_show_all(*container);

    return(drawing_area);
}

/**
 * Creates a drawing area widget within a frame. When the direction parameter
 * is HORIZONTAL, a drawing area is created in a horizontal box using the
 * height for both width and height. When the direction is VERTICAL, a
 * drawing area is created in a vertical box using the width for both the
 * width and height.
 *
 * @param frame the frame container that has all the components
 * @param direction either HORIZONTAL or VERTICAL.
 * @param width width of drawing area
 * @param height height of drawing area
 *
 * @return drawing area widget
 */
GtkWidget *D_CreateDrawingAreaWithExtra(GtkWidget **frame, gint direction, gint width, gint height)
{
    GtkWidget *drawing_area;
    GtkWidget *box;
    GtkWidget *da_extra;

    if (direction == HORIZONTAL_DIRECTION)
    {
        box = gtk_hbox_new(False, 0);
    }
    else
    {
        box = gtk_vbox_new(False,  0);
    }

    /* create extra area, typicalling used as a corner */
    da_extra = gtk_drawing_area_new();
    gtk_widget_set_size_request(GTK_WIDGET(da_extra), width, height);
    gtk_box_pack_start (GTK_BOX (box), da_extra, False, False, 0);

    (*frame) = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(*frame), GTK_SHADOW_NONE);

    drawing_area = gtk_drawing_area_new();
    gtk_widget_set_size_request(GTK_WIDGET(drawing_area), width, height);
    gtk_box_pack_start (GTK_BOX (box), drawing_area, True, True, 0);

    gtk_container_add(GTK_CONTAINER(*frame), box);

    gtk_widget_show_all(*frame);

    return(drawing_area);
}

/**
 * Sets the title for the window
 *
 * @param window set title for this dialog window
 * @param which_dialog additional text identifing the dialog
 * @param coord indicates which coordinate is active
 */
void D_SetDialogTitle(GtkWidget *window, gchar *which_dialog, gint coord)
{
    if (window != NULL)
    {
        gchar *title = AM_GetTitle(which_dialog, coord);
        gtk_window_set_title(GTK_WINDOW(window), title);
        MH_Free(title);
    }
}

/**
 * This method retrieves the default view for the specified display
 * from the config file. Based on the value given the appropriate
 * tabbed view is enabled.
 *
 * @param which_dialog REALTIME or PREDICT
 */
void D_SetDefaultView(gint which_dialog, GtkWidget *notebook)
{
    gchar *config_val;
    gchar default_view[64];

    /* only do this once */
    config_val = (which_dialog == REALTIME ? IAM_REALTIME_VIEW_STARTUP : IAM_PREDICT_VIEW_STARTUP);

    /* get the value from the config file */
    if (CP_GetConfigData(config_val, default_view) == False)
    {
        /* couldn't find. */
        /* ignore and just let the default behavior happen */
        g_message("%s not defined in the config file!", config_val);
    }

    /* determine which view */
    /* set the notebook page */
    if (g_ascii_strcasecmp(default_view, SBAND1_VIEW_STR) == 0)
    {
        /* ignore, this is the first tab, just let it happen */
    }
    else if (g_ascii_strcasecmp(default_view, SBAND2_VIEW_STR) == 0)
    {
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), SBAND2);
    }
    else if (g_ascii_strcasecmp(default_view, KUBAND_VIEW_STR) == 0)
    {
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), KUBAND);
    }
    else if (g_ascii_strcasecmp(default_view, STATION_VIEW_STR) == 0)
    {
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), STATION);
    }
    else if (g_ascii_strcasecmp(default_view, SBAND1_KUBAND_VIEW_STR) == 0)
    {
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), SBAND1_KUBAND);
    }
    else if (g_ascii_strcasecmp(default_view, SBAND2_KUBAND_VIEW_STR) == 0)
    {
        gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), SBAND2_KUBAND);
    }
    else
    {
        /* couldn't find valid value, ignore */
        g_message("Invalid %s data value ... %s",config_val, default_view);
    }
}

/**
 * Retrieves the toolbutton data and tries to find the cooresponding
 * mask name. Once found, the button handlers are blocked, then
 * set (either activate or not), then the handlers are unblocked.
 *
 * @param windata the window data
 * @param enable flag indicating whether the mask is enabled or not
 * @param mask_name the name of the mask to enable
 */
void D_SetButtonMaskVisual(WindowData *windata, gboolean enable, const gchar *mask_name, gint coord)
{
    GList *glist = windata->tool_items;

    if (coord == KUBAND2)
        glist = windata->tool_items2;
 
    for (; glist; glist = glist->next)
    {
        WidgetHandlerData *data = (WidgetHandlerData *)glist->data;
        GtkToolItem *tool_button = (GtkToolItem *)data->instance;
        if (GTK_TOOL_ITEM(tool_button))
        {
            const gchar *label = gtk_tool_button_get_label(GTK_TOOL_BUTTON(tool_button));
            if (g_ascii_strcasecmp(mask_name, label) == 0)
            {
                g_signal_handler_block(data->instance, data->handler_id);
                gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(tool_button), enable);
                g_signal_handler_unblock(data->instance,  data->handler_id);
                break;
            }
        }
    }
}

/**
 * Initiates the action for the mask.
 *
 * @param ui the ui manager
 * @param menu_bar the menu bar string
 * @param blockage_menu the blockage menu string
 * @param mask_menu the specific mask menu
 * @param mask_name the mask we're looking for
 */
void D_SetBlockageMaskByAction(GtkUIManager *ui, gchar *menu_bar, gchar *blockage_menu, gchar *mask_menu, const gchar *mask_name)
{
    /* get the action for the menu item */
    /* when activating the action, this will cause the radio button to change */
    GtkAction *action;
    gchar menu[255];
    g_sprintf(menu, "/%s/%s/%s/%s", menu_bar, blockage_menu, mask_menu, mask_name);

    action = gtk_ui_manager_get_action(ui, menu);
    if (action)
    {
        /* activate the action */
        gtk_action_activate(action);
    }
}

/**
 * Create an Arrow widget with the specified parameters
 * and pack it into a button.
 *
 * @param arrow_type type of arrow to create
 * @param shadow_type shadow type of arrow button
 *
 * @return the button with the arrow
 */
GtkWidget *D_CreateArrowButton(GtkArrowType arrow_type, GtkShadowType shadow_type)
{
    GtkWidget *button;
    GtkWidget *arrow;

    button = gtk_button_new ();
    arrow = gtk_arrow_new (arrow_type, shadow_type);

    gtk_container_add (GTK_CONTAINER (button), arrow);

    gtk_widget_show (button);
    gtk_widget_show (arrow);

    return(button);
}

/**
 * Resizes the widget heirarchy when the width or height changes. If the font
 * size changed, then the resource file is reparsed; this causes the
 * widget component heirarchy to change.
 *
 * @param widget_name name of the widget to resize
 * @param resize_font has font information
 *
 * @return new_font_size or DEFAULT_FONT_SIZE
 */
gint D_ResizeFont(const gchar *widget_name, ResizeFont *resize_font)
{
    gint new_font_size;
    float percent   = -1.0;
    float w_percent = 0.0;
    float h_percent = 0.0;

    /* compute if width or height is changing */
    if (resize_font->new_height != resize_font->previous_height ||
        resize_font->new_width  != resize_font->previous_width)
    {
        /* compute if valid */
        if (resize_font->default_width != -1)
        {
            /* compute new width percent */
            w_percent = (float)((float)resize_font->new_width /
                                (float)resize_font->default_width);
        }

        /* compute if valid */
        if (resize_font->default_height != -1)
        {
            /* compute new height percent */
            h_percent = (float)((float)resize_font->new_height /
                                (float)resize_font->default_height);
        }

        /* average the width and height percent */
        if (w_percent > 0.0 && h_percent > 0.0)
        {
            percent = (w_percent + h_percent) / (float)2.0;
        }

        /* apply if valid */
        if (percent > 0.0)
        {
            /* keep font reasonable */
            new_font_size = (gint)(resize_font->default_font_size * percent);

            /* don't allow a font larger than 12 */
            if (new_font_size > 12)
            {
                new_font_size = 12;
            }

            /* don't allow a font smaller than 6 */
            if (new_font_size < 6)
            {
                new_font_size = 6;
            }

            /* apply change if font size changed */
            if (new_font_size != resize_font->previous_font_size)
            {
                FH_AddRCFontStyle(resize_font->rc_string, new_font_size, widget_name);
                return(new_font_size);
            }
        }
    }

    return(resize_font->previous_font_size);
}

/**
 * Determines if the caller should just hide or exit the whole app. If the caller
 * requesting a close is the only dialog up and running, then the whole app is
 * exited. Otherwise, the caller is hidden.
 *
 * @param which_dialog either REALTIME or PREDICT
 */
void D_CloseDialog(gint which_dialog)
{
    if (which_dialog == REALTIME)
    {
        /* just exit, bringing down the house */
        D_ExitIam();
    }
    else
    {
        if (RTD_IsActive() == True)
        {
            gtk_widget_hide_all(PD_GetDialog());
            AM_ClearActiveDisplay();
 #ifndef _EXT_PARTNERS
           /* 
             * If the Predict dialog was opened first, it was set with 
             * User Experience and is not closing the Realtime dialog. 
             * The Realtime dialog remains open. Set it with User Experience.
             */
            UEsetRealtimeDialog();
#endif
        }
        else
        {
            D_ExitIam();
        }
    }
}

/**
 * Destroys widget instances and terminates the main loop
 */
void D_ExitIam(void)
{
    GtkWidget *widget;

    /* make sure the dialog widget's are valid */
    if ((widget = RTD_GetDialog()) != NULL)
    {
        gtk_widget_destroy(widget);
    }

    if ((widget = PD_GetDialog()) != NULL)
    {
        gtk_widget_destroy(widget);
    }

    /* stop main loop */
    gtk_main_quit();

    /* not coming back from this */
    AM_Exit(0);
}

/**
 * This method processes the mouse motion callback. While the mouse is moving,
 * the az/el values are calulated and displayed.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
gboolean D_DrawCoordinateTracking_cb(GtkWidget *widget, GdkEventMotion *event, void * data)
{
    gint x, y;
    gchar message[64];
    gboolean show_track = True;

    PangoFontDescription *pango_font_desc;
    PangoLayout *layout;

    WindowData *windata = get_window_data_by_widget(widget);
    if (windata != NULL)
    {
        /* save mouse coordinates */
        gint x_loc = (gint)event->x;
        gint y_loc = (gint)event->y;

        gint width, height;
        gint coord = windata->coordinate_type;

        /* convert point to az/el or xel/el */
        WG_WindowToPoint(coord,
                         windata->drawing_area->allocation.width, windata->drawing_area->allocation.height,
                         x_loc, y_loc, &x, &y);

        /* for sband, check elevation, if it exceeds a limit, don't show */
        if (coord == SBAND1 || coord == SBAND2 || coord == STATION)
        {
            if (y/10 > thisSBandTrackingLimit)
            {
                show_track = False;
            }
            else
            {
                g_sprintf(message, AZ_LABEL": %5.1f "EL_LABEL": %5.1f", x/10.0, y/10.0);
            }
        }
        else
        {
            g_sprintf(message, XEL_LABEL": %5.1f "EL_LABEL": %5.1f", x/10.0, y/10.0);
        }

        if (show_track == True)
        {
            gint display = AM_GetActiveDisplayID();

            /* need to compute the height so we can offset the string from the cursor */
            pango_font_desc = pango_font_description_copy(
                                                         (display == DISPLAY_PD ? PD_GetPangoFontDescription() : RTD_GetPangoFontDescription()));
            layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
            pango_layout_set_text(layout, message, -1);
            pango_layout_set_font_description(layout, pango_font_desc);
            pango_font_description_free(pango_font_desc);

            width = FH_GetFontWidth(layout);
            height = FH_GetFontHeight(layout);
            g_object_unref(layout);

            if ((y_loc - height) < height)
            {
                y_loc = y_loc+height;
            }
            else
            {
                y_loc = y_loc-height;
            }

            if ((x_loc + width) > windata->drawing_area->allocation.width)
            {
                x_loc = x_loc-width;
            }

            /* clear previous string */
            D_RefreshAreas(windata, windata->drawing_area->window, windata->src_pixmap, &thisTrackingArea, False);

            /* draw the string */
            if (AM_GetActiveDisplayID() == DISPLAY_PD)
            {
                PD_DrawCoordPoints(windata, message, x_loc, y_loc);
            }
            else
            {
                RTD_DrawCoordPoints(windata, message, x_loc, y_loc);
            }

            /* save this strings location */
            D_AddArea(windata, &thisTrackingArea, x_loc, y_loc, width, height);
        }
    }

    /* let the system event handler execute */
    return(False);
}

/**
 * Turns off the motion handlers
 *
 * @param whd widget handler data
 */
void D_BlockMotionHandlers(WidgetHandlerData *whd)
{
    guint i;
    for (i=0; i<NUM_VIEWS; i++)
    {
        if (whd[i].instance != NULL)
        {
            g_signal_handler_block(whd[i].instance,
                                   whd[i].handler_id);
        }
    }
}

/**
 * Turns on the motion handlers
 *
 * @param whd widget handler data
 */
void D_UnblockMotionHandlers(WidgetHandlerData *whd)
{
    guint i;
    for (i=0; i<NUM_VIEWS; i++)
    {
        if (whd[i].instance != NULL)
        {
            g_signal_handler_unblock(whd[i].instance,
                                     whd[i].handler_id);
        }
    }
}

/**
 * Determines which display is active, then calls the
 * appropriate function to get the window data based on the
 * widget.
 *
 * @param widget find window data for this widget
 *
 * @return WindowData* of active display or NULL
 */
static WindowData *get_window_data_by_widget(GtkWidget *widget)
{
    if (AM_GetActiveDisplayID() == DISPLAY_PD)
    {
        return PD_GetWindowDataByWidget(widget);
    }
    else
    {
        return RTD_GetWindowDataByWidget(widget);
    }

    return NULL;
}
