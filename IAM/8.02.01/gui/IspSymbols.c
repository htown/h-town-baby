/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "IspSymbols.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/IspSymbols.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/

/* ISP Symbol Table */
static GPtrArray    *thisSymbolList = NULL;
/*************************************************************************/

/**
 * This routine initializes the data array.  It gets the list of
 * ISP Symbols of interest from the specified file.
 */
void IS_Constructor(void)
{
    FILE *fp;
    gint i;
    gchar line[512];
    gchar symbol_tag[100];
    gchar symbol_name[100];
    gchar symbol_file[MAXPATHLEN];
    SymbolRec *symbol;
    guint symbol_count = 0;

    /*---------------------------*/
    /* Open the file for reading */
    /*---------------------------*/
    if ((CP_GetConfigData(IAM_ISP_SYMBOL_FILE, symbol_file)) == False)
    {
        g_critical("IAM_ISP_SYMBOL_FILE not defined in the config file!");

        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "IAM_ISP_SYMBOL_FILE not defined in the config file!");
        AM_Exit(1);
    }

    if ((fp = fopen(symbol_file, "r")) == NULL)
    {
        gchar *strerr_msg = strerror(errno);
        gchar message[256];

        g_critical("Failed to open ISP Symbols file.\n");
        g_critical("\tfile name  = %s\n", symbol_file);
        g_critical("\terror code = %s\n", strerr_msg);

        g_sprintf(message,
                "Failed to open ISP Symbols file.\n"
                "\tfilename = %s\n"
                "\terror code = %s\n",
                symbol_file,
                strerr_msg);

        ErrorMessage(AM_GetActiveDisplay(), "Open Error", message);

        AM_Exit(1);
    }

    /* allocate ptr array for symbols */
    thisSymbolList = g_ptr_array_new();

    /* Read each line of the file */
    while (fgets(line, 511, fp) != NULL)
    {
        /* Check if current line is a comment */
        line[strlen(line)-1] = '\0';  /* clear out line feed character */
        for (i=0; line[i]==' '; i++);
        if ((line[i] == '#') || (line[i] == '\0'))
        {
            continue;
        }

        /* Parse line for symbol tag and name */
        symbol_tag[0] = '\0';
        symbol_name[0] = '\0';
        sscanf(line, "%99s %99s", symbol_tag, symbol_name);
        CP_StringTrim(symbol_tag);
        CP_StringTrim(symbol_name);

        /* allocate symbol and save */
        symbol = (SymbolRec *)MH_Calloc(sizeof(SymbolRec), __FILE__, __LINE__);
        symbol->symbol_id           = symbol_count;
        g_stpcpy(symbol->symbol_tag, symbol_tag);
        g_stpcpy(symbol->symbol_name, symbol_name);
        symbol->value               = INITIAL_VALUE;
        symbol->value_str[0]        = 0;
        symbol->event_time          = 0;
        symbol->event_time_update   = False;
        symbol->status_update       = False;

        /* add symbol to array list */
        g_ptr_array_add(thisSymbolList, symbol);

        /* keep track of the total symbols */
        symbol_count++;
    }

    /* validate the symbol count with our enum list */
    if (symbol_count != IS_MaxNumberOfSymbols)
    {
        g_critical("Symbol count inconsistency!");
        g_critical("Read %d symbols, but %d symbols enumerated!",
                   symbol_count, IS_MaxNumberOfSymbols);

        ErrorMessage(AM_GetActiveDisplay(), "Symbol Count Error",
                     "Inconsistent symbol count!\n"
                     "Number of symbols defined in iam.symbols\n"
                     "does not equal internal count!\n\n"
                     "Please inform software support.");
        AM_Exit(1);
    }
    fclose(fp);
}

/**
 *  This function frees all allocated messages.
 */
void IS_Destructor(void)
{
    guint i;
    SymbolRec *symbol;

    if (thisSymbolList != NULL)
    {
        for (i=0; i<IS_MaxNumberOfSymbols; i++)
        {
            symbol = (SymbolRec *)g_ptr_array_index(thisSymbolList, i);
            MH_Free(symbol);
        }

        g_ptr_array_free(thisSymbolList, False);
    }
}

/**
 * This functions return the number of ISP Symbols used by ISS AntMan.
 *
 * @return max number of symbols
 */
gint IS_GetSymbolCount(void)
{
    return IS_MaxNumberOfSymbols;
}

/**
 * This function returns the i-th ISP Symbol in the array
 *
 * @param symbol_id
 *
 * @return symbol found at symbol_id or NULL
 */
SymbolRec *IS_GetSymbol(guint symbol_id)
{
    if (thisSymbolList != NULL)
    {
        if ((symbol_id >= 0) && (symbol_id < IS_MaxNumberOfSymbols))
        {
            return(SymbolRec *)g_ptr_array_index(thisSymbolList, symbol_id);
        }
    }

    return NULL;
}

/**
 * Sets the status update flag for all symbols to the state given
 *
 * @param status_update flag indicating the state of the symbols
 */
void IS_SetSymbolStatus(gboolean status_update)
{
    guint i;
    SymbolRec *symbol;

    for (i=0; i<IS_MaxNumberOfSymbols; i++)
    {
        symbol = (SymbolRec *)g_ptr_array_index(thisSymbolList, i);
        if (symbol != NULL)
        {
            symbol->status_update = status_update;
        }
    }
}
