/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#define PRIVATE
#include "ObsMask.h"
#undef PRIVATE

#include "AntMan.h"
#include "ColorHandler.h"
#include "ConfigParser.h"
#include "Conversions.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "WindowGrid.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/ObsMask.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define OBS_COLOR           "obs_color"
#define OBS_COLOR_RED       17990
#define OBS_COLOR_GREEN     33410
#define OBS_COLOR_BLUE      61680
#define SHUTTLE_COLOR       "wheat"

static AntennaInfo sBand1_cone =
{
    {"sBand1.cone-obs",      0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"sBand1.cone-shuttle",  0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo sBand1_los  =
{
    {"sBand1.los-obs",       0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"sBand1.los-shuttle",   0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo sBand2_cone =
{
    {"sBand2.cone-obs",      0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"sBand2.cone-shuttle",  0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo sBand2_los  =
{
    {"sBand2.los-obs",       0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"sBand2.los-shuttle",   0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo kuBand_cone =
{
    {"kuBand.cone-obs",     0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"kuBand.cone-shuttle", 0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo kuBand_los  =
{
    {"kuBand.los-obs",      0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"kuBand.los-shuttle",  0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo kuBand2_cone =
{
    {"kuBand2.cone-obs",     0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"kuBand2.cone-shuttle", 0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo kuBand2_los  =
{
    {"kuBand2.los-obs",      0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"kuBand2.los-shuttle",  0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static AntennaInfo Generic     =
{
    {"station.generic-obs",         0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}},
    {"station.generic-shuttle",     0, 0, NULL, { 0, 0, NULL}, { 0, 0, NULL}}
};

static gint          showCone   [NUM_DISPLAYS] = {False, False};
static gint          showShuttle[NUM_DISPLAYS] = {False, False};

/* holds the redraw state for each view */
static gboolean     thisRedraw[NUM_DISPLAYS][NUM_VIEWS] =
{
    { False, False, False, False, False, False, False, False},
    { False, False, False, False, False, False, False, False}
};
/*************************************************************************/

/**
 * Constructor for the ObsMask file
 *
 * @return True or False
 */
gboolean OM_Constructor(void)
{
    GdkColor gcolor;
    guint i, j;

    gcolor.red   = OBS_COLOR_RED;
    gcolor.green = OBS_COLOR_GREEN;
    gcolor.blue  = OBS_COLOR_BLUE;
    CH_AddColor(OBS_COLOR, &gcolor);

    for (i=0; i<NUM_DISPLAYS; i++)
    {
        for (j=0; j<NUM_VIEWS; j++)
        {
            thisRedraw[i][j] = True;
        }
    }

    return load_all_masks();
}

/**
 * This function disposes of the memory allocated to all masks.
 */
void OM_Destructor(void)
{
    /*------------------------------------*/
    /* Remove s-band 1 cone antenna buffers */
    /*------------------------------------*/
    delete_mask(&sBand1_cone.obscuration);
    delete_mask(&sBand1_cone.shuttle);

    /*-----------------------------------*/
    /* Remove s-band 1 los antenna buffers */
    /*-----------------------------------*/
    delete_mask(&sBand1_los.obscuration);
    delete_mask(&sBand1_los.shuttle);

    /*------------------------------------*/
    /* Remove s-band 2 cone antenna buffers */
    /*------------------------------------*/
    delete_mask(&sBand2_cone.obscuration);
    delete_mask(&sBand2_cone.shuttle);

    /*-----------------------------------*/
    /* Remove s-band 2 los antenna buffers */
    /*-----------------------------------*/
    delete_mask(&sBand2_los.obscuration);
    delete_mask(&sBand2_los.shuttle);

    /*-------------------------------------*/
    /* Remove ku-band cone antenna buffers */
    /*-------------------------------------*/
    delete_mask(&kuBand_cone.obscuration);
    delete_mask(&kuBand_cone.obscuration);
    delete_mask(&kuBand2_cone.shuttle);
    delete_mask(&kuBand2_cone.shuttle);

    /*------------------------------------*/
    /* Remove ku-band los antenna buffers */
    /*------------------------------------*/
    delete_mask(&kuBand_los.obscuration);
    delete_mask(&kuBand_los.shuttle);
    delete_mask(&kuBand2_los.obscuration);
    delete_mask(&kuBand2_los.shuttle);

    /*---------------------------------*/
    /* Remove low-gain antenna buffers */
    /*---------------------------------*/
    delete_mask(&Generic.obscuration);
    delete_mask(&Generic.shuttle);
}

/**
 * This function draws the station obscuration mask for the specified antenna
 *
 * @param windata window data
 * @param display current display
 * @param coord_view current coordinate view
 */
void OM_DrawMasks(WindowData *windata, gint display, gint coord_view)
{
    AntennaInfo *antenna = NULL;
    gchar title_head[15];
    gboolean pixmap_alloc = False;
    gint coord = windata->coordinate_type;

    /*------------------*/
    /* Get antenna type */
    /*------------------*/
    antenna = select_antenna(coord, showCone[display], title_head);

    /*--------------------------------------------*/
    /* Draw the blockage for the selected antenna */
    /*--------------------------------------------*/
    if (antenna != NULL)
    {
        if ((pixmap_alloc = AM_AllocatePixmapData(windata, windata->obs_data, False)) == True ||
            thisRedraw[display][coord_view] == True)
        {
            thisRedraw[display][coord_view] = False;

            /* how did we get here */
            /* if its a redraw request and not a realloc, */
            /* we need to clear the pixmap */
            if (pixmap_alloc == False)
            {
                AM_ClearDrawable(windata->obs_data->pixmap,
                                 windata->drawing_area->style->black_gc,
                                 0, 0,
                                 windata->obs_data->width,
                                 windata->obs_data->height);
            }

            /* draw obscuration mask */
            draw_mask(windata, display, &antenna->obscuration, OBS_COLOR, coord, windata->obs_data->pixmap);

            /* is shuttle needed ? */
            if (showShuttle[display])
            {
                draw_mask(windata, display, &antenna->shuttle, SHUTTLE_COLOR, coord, windata->obs_data->pixmap);
            }
        }

        /* render pixmap to drawable */
        if (windata->mask_data->pixmap != NULL)
        {
            AM_CopyPixmap(windata, windata->obs_data->pixmap);
        }
    }
}

/**
 * This function sets/clears the flag that signifies whether cone or los
 * blockage is to be displayed
 *
 * @param display current display
 * @param shown flag indicating if mask is shown
 */
void OM_ShowCone(gint display, gboolean shown)
{
    guint i;

    /* has the selection changed ? */
    if (showCone[display] != shown)
    {
        showCone[display] = shown;

        /* change occured, set redraw for all views */
        for (i=0; i<NUM_VIEWS; i++)
        {
            thisRedraw[display][i] = True;
        }
    }
}


/**
 * This function sets/clears the flag that signifies whether the docked
 * Shuttle blockage is to be displayed
 *
 * @param display current display
 * @param shown flag indicating if mask is shown
 */
void OM_ShowShuttle(gint display, gboolean shown)
{
    guint i;

    /* has the selection changed ? */
    if (showShuttle[display] != shown)
    {
        showShuttle[display] = shown;

        /* change occured, set redraw for all views */
        for (i=0; i<NUM_VIEWS; i++)
        {
            thisRedraw[display][i] = True;
        }
    }
}

/**
 * This function forces a redraw
 *
 * @param display current display
 * @param coord_view inicates the coord view to redraw
 */
void OM_ForceRedraw (gint display, gint coord_view)
{
    thisRedraw[display][coord_view] = True;
}

/**
 * This function returns flag if the specified point is blocked by the current
 * structure.
 *
 * @param pt check this point for blockage
 * @param coord current coordinate type
 *
 * @return 1 if point is blocked by a structure; 0 if not
 */
gint OM_PointBlocked(Point *pt, gint coord)
{
    AntennaInfo *antenna = NULL;
    gchar title_head[15];

    /*------------------*/
    /* Get antenna type */
    /*------------------*/
    antenna = select_antenna(coord, showCone[PREDICT], title_head);

    /*---------------------------------------------------*/
    /* Determine if point is blocked by selected antenna */
    /*---------------------------------------------------*/
    if (antenna != NULL)
    {
        if (find_point(&antenna->obscuration, pt))
            return True;

        if (showShuttle[PREDICT] && find_point(&antenna->shuttle, pt))
            return True;
    }

    return False;  /* point not found in any of the structures */
}


/**
 * This function prints to the file all blockage time caused by the current
 * structure.
 *
 * @param fp file pointer
 * @param ps predict set
 */
void OM_PrintBlockage(FILE *fp, PredictSet *ps)
{
    AntennaInfo *antenna = NULL;
    gchar title[30];
    gchar title_head[15];

    /*------------------*/
    /* Get antenna type */
    /*------------------*/
    antenna = select_antenna(PD_GetCoordinateType(), showCone[PREDICT], title_head);

    /*---------------------------------------------------*/
    /* Print the selected antenna's blockage to the file */
    /*---------------------------------------------------*/
    if (antenna != NULL)
    {
        g_sprintf( title, "%s Obscuration", title_head );
        print_blockage(&antenna->obscuration, fp, ps, title);

        if (showShuttle[PREDICT])
        {
            g_sprintf(title, "%s Shuttle", title_head);
            print_blockage(&antenna->shuttle, fp, ps, title);
        }
    }
}

/**
 * Loads masks found in the given mask directory.
 */
void OM_ChangeMasks(void)
{
    /* release old mask data */
    OM_Destructor();

    /* load new masks */
    OM_Constructor();
}


/**
 * This function is used by qsort() and bsearch() to compare two points
 *
 * @param a first obspoint
 * @param b second obspoint
 *
 * @return 0 if a == b;  -1 if a < b;  1 if a > b
 */
static gint mask_entry_compare(const void *a, const void *b)
{
    ObsPoint *pa = (ObsPoint *)a;
    ObsPoint *pb = (ObsPoint *)b;

    if (pa->pt.x < pb->pt.x)
    {
        return -1;
    }

    else if (pa->pt.x > pb->pt.x)
    {
        return 1;
    }

    else /* pa->pt.x == pb->pt.x */
    {
        if (pa->pt.y < pb->pt.y)
        {
            return -1;
        }

        else if (pa->pt.y > pb->pt.y)
        {
            return 1;
        }

        else /* pa->pt.y == pb->pt.y */
        {
            return 0;
        }
    }
}

/**
 * This function returns the pointer to the selected antenna, based on
 * coordinate and if cone is displayed
 *
 * @param coord current coordinate type
 * @param isCone flag indicating if cone or los is used
 * @param title_head title name
 *
 * @return AntennaInfo * or NULL
 */
static AntennaInfo *select_antenna(gint coord, gint isCone, gchar *title_head)
{
    AntennaInfo *antenna = NULL;

    /*-------------------------------------------------------*/
    /* Determine which mask to use based on the antenna type */
    /*-------------------------------------------------------*/
    switch (coord)
    {
        case SBAND1:
            if (isCone)
            {
                g_stpcpy(title_head, "S-Band 1 Cone");
                antenna = &sBand1_cone;
            }
            else
            {
                g_stpcpy(title_head, "S-Band 1 Los");
                antenna = &sBand1_los;
            }
            break;

        case SBAND2:
            if (isCone)
            {
                g_stpcpy(title_head, "S-Band 2 Cone");
                antenna = &sBand2_cone;
            }
            else
            {
                g_stpcpy(title_head, "S-Band 2 Los");
                antenna = &sBand2_los;
            }
            break;

        case KUBAND:
            if (isCone)
            {
                g_stpcpy(title_head, "Ku-Band Cone");
                antenna = &kuBand_cone;
            }
            else
            {
                g_stpcpy(title_head, "Ku-Band Los");
                antenna = &kuBand_los;
            }
            break;

        case KUBAND2:
            if (isCone)
            {
                g_stpcpy(title_head, "Ku-Band 2 Cone");
                antenna = &kuBand2_cone;
            }
            else
            {
                g_stpcpy(title_head, "Ku-Band 2 Los");
                antenna = &kuBand2_los;
            }
            break;

        case STATION:
            g_stpcpy(title_head, "Generic");
            antenna = &Generic;
            break;

        default:
            break;
    }

    return antenna;
}

/**
 * This function loads the obscuration masks from the files
 *
 * @return TRUE or FALSE
 */
static gboolean load_all_masks(void)
{
    gint i, j;
    AntennaInfo *antenna = NULL;
    gchar title_head[15];
    gchar title[30];

    for (i=0; i<NUM_COORDS; i++)   /* for each coordinate type */
    {
        for (j=0; j<2; j++)         /* los and cone */
        {
            /*-----------------------------------*/
            /* Low-gain antenna has no cone mode */
            /*-----------------------------------*/
            if (i==STATION && j==1)
            {
                continue;
            }

            /*------------------*/
            /* Get antenna type */
            /*------------------*/
            antenna = select_antenna(i, j, title_head);

            g_sprintf(title, "%s Obscuration", title_head );
            if (load_mask(&antenna->obscuration, title, i) == False)
            {
                return False;
            }

            g_sprintf(title, "%s Shuttle", title_head);
            if (load_mask(&antenna->shuttle, title, i) == False)
            {
                return False;
            }
        }
    }

    return True;
}


/**
 * This function loads a obscuration mask from a specified file
 *
 * @param obs obs info
 * @param title title
 * @param coord current coordindate type
 *
 * @return TRUE or FALSE
 */
static gboolean load_mask(ObsInfo *obs, gchar *title, gint coord)
{
    FILE *fp;
    gchar fname[256];
    gchar line[256];
    gint rc = 0;
    gint az, el;
    gint count;
    gint i;
    ObsPoint *mask;
    gchar *maskfile;
    gchar *coord_s = (coord == SBAND1 ? "sband1" :
                      coord == SBAND2 ? "sband2" :
                      coord == KUBAND ? "kuband" :
                      coord == KUBAND2 ? "kuband2" : "station");


    /*-----------------------------------------------*/
    /* Remove previous obscuration info if it exists */
    /*-----------------------------------------------*/
    if (obs->mask != NULL)
    {
        delete_mask(obs);
    }

    /*--------------------*/
    /* Open the mask file */
    /*--------------------*/
    maskfile = CP_GetMaskFile(coord_s, obs->file_name);
    g_stpcpy(fname, maskfile);
    g_free(maskfile);
    if ((fp = fopen(fname, "r")) == NULL)
    {
        gchar message[256];
        g_sprintf(message, "%s: unable to open blockage file!\n"
                  "Blockage data file not generated.",
                  fname);
        ErrorMessage(AM_GetActiveDisplay(), "Open Error", message);
        return False;
    }

    /*---------------------------------*/
    /* Read the first line of the file */
    /*---------------------------------*/
    if (fgets(line, 256, fp))
    {
        /*--------------------------------------*/
        /* Get number of points from input file */
        /*--------------------------------------*/
        if (sscanf(line, "%d", &obs->count) == 1)
        {
            /*-------------------------------------------------*/
            /* Read all azimuth/elevation points from the file */
            /*-------------------------------------------------*/
            if (obs->count > 0)
            {
                obs->mask = (ObsPoint *) MH_Calloc(sizeof(ObsPoint) * obs->count, __FILE__, __LINE__);
                mask = obs->mask;
                count = 0;
                for (i=0; i<obs->count; i++)
                {
                    /*---------------------------*/
                    /* EOF early, no more points */
                    /*---------------------------*/
                    if (! fgets(line, 256, fp))
                    {
                        break;
                    }

                    /*-----------------------------*/
                    /* Get point from current line */
                    /*-----------------------------*/
                    if (sscanf(line, "%d %d", &az, &el) == 2)
                    {
                        /*---------------------------------------------------*/
                        /* Convert points to coord type and check if in view */
                        /*---------------------------------------------------*/
                        az *= 10;  el *= 10;
                        switch (coord)
                        {
                            case SBAND1:
                            case SBAND2:
                                mask->in_range = StationAEtoSBandAE(az, el, &az, &el);
                                break;

                            case KUBAND:
                            case KUBAND2:
                                mask->in_range = StationAEtoKuBand(az, el, &az, &el, coord);
                                break;

                            case STATION:
                                mask->in_range = StationAEtoGeneric(az, el, &az, &el);
                                break;

                            default:
                                break;
                        }

                        if (mask->in_range)
                        {
                            obs->in_range_count++;
                        }

                        /*--------------------------*/
                        /* Put point in point array */
                        /*--------------------------*/
                        mask->pt.x = (gint)ROUND((gdouble)az/10.0);
                        mask->pt.y = (gint)ROUND((gdouble)el/10.0);
                        mask++;
                        count++;
                    }
                }

                /*---------------------------------------*/
                /* If all point expected not read in,    */
                /* readjust the size of the points array */
                /*---------------------------------------*/
                if (count != obs->count)
                {
                    MH_Realloc((void **)&obs->mask, sizeof(ObsPoint) * obs->count, __FILE__, __LINE__);
                    obs->count = count;
                }

                /*-------------------------------------------------*/
                /* sort the points of the mask for future searches */
                /*-------------------------------------------------*/
                qsort(obs->mask, obs->count, sizeof(ObsPoint), mask_entry_compare );

                rc = 1;
            }
        }
    }

    fclose(fp);

    return True;
}


/**
 * This function loads a station obscuration mask from a specified file
 *
 * @param windata window data
 * @param display current display
 * @param obs obs info
 * @param color_name color to use for mask
 * @param coord current coordinate type
 * @param drawable draw into this drawable
 */
static void draw_mask(WindowData *windata, gint display, ObsInfo *obs, gchar *color_name, gint coord, GdkDrawable *drawable)
{
    GdkGC *gc = NULL;
    gint w, h;
    gint i;
    gint x = 0;
    gint y = 0;
    gint count;
    WindowInfo *window;

    /*------------------------------------*/
    /* If no mask, return without drawing */
    /*------------------------------------*/
    if (obs->mask == NULL)
    {
        return;
    }

    /* get the GC from the GC structure and set the colors */
    gc = CH_SetColorGC(windata->drawing_area, color_name);

    /*-----------------------------------------------------*/
    /* Determine which set of window points are being used */
    /*-----------------------------------------------------*/
    if (display == REALTIME)
    {
        window = &obs->rt_window;
    }
    else  /* display == PREDICT */
    {
        window = &obs->pd_window;
    }

    /*--------------------------------*/
    /* Get dimensions of drawing area */
    /*--------------------------------*/
    w = windata->drawing_area->allocation.width;
    h = windata->drawing_area->allocation.height;

    /*----------------------------------------------*/
    /* Check if window points need to be calculated */
    /*----------------------------------------------*/
    if ((window->winpts == NULL) ||
        (window->width != w) || (window->height != h))
    {
        /* make sure we free previously allocated memory */
        if (window->winpts != NULL)
        {
            MH_Free(window->winpts);
            window->winpts = NULL;
        }

        /*--------------------------------------------------------*/
        /* For each blockage point, calculate its window position */
        /*--------------------------------------------------------*/
        count = 0;
        window->width = w;
        window->height = h;
        window->winpts = (GdkRectangle *) MH_Calloc(sizeof(GdkRectangle) * obs->in_range_count, __FILE__, __LINE__);
        for (i = 0; i < obs->count; i++)
        {
            if (obs->mask[i].in_range)
            {
                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 obs->mask[i].pt.x*10,
                                 obs->mask[i].pt.y*10,
                                 &x, &y);
                window->winpts[count].x      = (gint)x-1;
                window->winpts[count].y      = (gint)y-1;
                window->winpts[count].width  = 3;
                window->winpts[count].height = 3;
                count++;
            }
        }
    }

    /*---------------------------------------------------------------------*/
    /* Draw the blockage points.  Instead of points, filled rectangles are */
    /* drawn to eliminate as much empty space as possible between points.  */
    /*---------------------------------------------------------------------*/
    for (i=0; i<obs->in_range_count; i++)
    {
        gdk_draw_rectangle(drawable, gc, True,
                           window->winpts[i].x, window->winpts[i].y,
                           window->winpts[i].width, window->winpts[i].height);
    }

    g_object_unref(gc);
}


/**
 * This function finds a specified point in an obscuration mask
 *
 * @param obs obs info
 * @param pt point to search for
 *
 * @return 1 if point is found;  0 if not
 */
static gint find_point(ObsInfo *obs, Point *pt)
{
    ObsPoint *found;
    ObsPoint pt1;

    /*-----------------------------------*/
    /* If no mask, return no point found */
    /*-----------------------------------*/
    if (obs->mask == NULL)
    {
        return False;
    }

    if (pt == NULL)
    {
        return False;
    }

    /*-------------------------------*/
    /* Round point to nearest degree */
    /*-------------------------------*/
    pt1.pt.x = (gint)ROUND((gdouble)pt->x / 10.0);
    pt1.pt.y = (gint)ROUND((gdouble)pt->y / 10.0);

    /*-----------------------------------*/
    /* Find the point in the sorted mask */
    /*-----------------------------------*/
    found = (ObsPoint*)bsearch(&pt1, obs->mask, obs->count, sizeof(ObsPoint),
                               mask_entry_compare);

    return(found != NULL);
}

/**
 * This function prints the blockage for a specified obscuration
 *
 * @param obs obs info
 * @param fp file pointer
 * @param ps predict set
 * @param title title for print
 */
static void print_blockage(ObsInfo *obs, FILE *fp, PredictSet *ps, gchar *title)
{
    GList *ptList;
    gint inBlockage = False;
    gint blockageOccurred = False;
    gint currentBlocked, nextBlocked;
    gchar timeStr[TIME_STR_LEN];
    gchar timeTypeStr[4];

    gint timeType = AM_GetTimeType();
    if ((ps != NULL) && (ps->ptList != NULL))
    {
        if (obs->mask != NULL)
        {
            if (timeType == TS_GMT)
            {
                g_stpcpy(timeTypeStr, "GMT");
            }
            else
            {
                g_stpcpy(timeTypeStr, "UTC");
            }

            fprintf(fp, "-----------------------------------------------------\n");
            fprintf(fp, "%-25.25s ----%s-----\n", title, timeTypeStr);

            /* traverse the list of predict points */
            for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
            {
                /* get this point data */
                PredictPt *pt = (PredictPt *)ptList->data;

                g_stpcpy(timeStr, pt->timeStr);
                if (timeType == TS_GMT)
                {
                    UtcToGmt(timeStr);
                }

                /* get next point */
                nextBlocked = (ptList->next != NULL) && find_point(obs, &((PredictPt *)ptList->next->data)->rawPt);

                /* if not in blockage area */
                if (!inBlockage)
                {
                    currentBlocked = find_point( obs, &pt->rawPt );

                    /* if single point blocked */
                    if (currentBlocked && !nextBlocked)
                    {
                        fprintf( fp, "          Crossed:       %s\n", timeStr );
                        blockageOccurred = True;
                    }
                    else if (currentBlocked)  /* if going into blockage */
                    {
                        inBlockage = True;
                        fprintf( fp, "             From:       %s\n", timeStr );
                        blockageOccurred = True;
                    }
                }
                /* currently in blockage area */
                else
                {
                    /* if coming out of blockage */
                    if (!nextBlocked)
                    {
                        inBlockage = False;
                        fprintf( fp, "               To:       %s\n", timeStr );
                        blockageOccurred = True;
                    }
                }
            }

            if (! blockageOccurred)
                fprintf(fp, "No blockage occurs for this mask.\n");

            fprintf(fp, "\n");
        }
    }
}


/**
 * This function deletes a station obscuration mask from memory
 *
 * @param obs obs info
 */
static void delete_mask(ObsInfo *obs)
{
    if (obs->mask != NULL)
    {
        MH_Free(obs->mask);
    }

    if (obs->rt_window.winpts != NULL)
    {
        MH_Free(obs->rt_window.winpts);
    }

    if (obs->pd_window.winpts != NULL)
    {
        MH_Free(obs->pd_window.winpts);
    }

    obs->count             = 0;
    obs->in_range_count    = 0;
    obs->mask              = NULL;
    obs->rt_window.winpts  = NULL;
    obs->rt_window.width   = 0;
    obs->rt_window.height  = 0;
    obs->pd_window.winpts  = NULL;
    obs->pd_window.width   = 0;
    obs->pd_window.height  = 0;
}
