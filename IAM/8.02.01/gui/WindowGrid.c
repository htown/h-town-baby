/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PRIVATE
#include "WindowGrid.h"
#undef PRIVATE

#include "Conversions.h"
#include "PredictDialog.h"
#include "RealtimeDialog.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/WindowGrid.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define  POLAR_BORDER       50
#define  NUM_GRID_SEGMENTS  5
#define  MAJOR_COLOR        GC_GRAY
#define  MINOR_COLOR        GC_GRAY50
/*************************************************************************/

/**
 * This function converts a point from azimuth/elevation (alpha/beta) to
 * window coordinates, in either rectangular or polar form.
 *
 * @param coord coordinate type
 * @param width width of window
 * @param height height of window
 * @param coord_x current x position in window
 * @param coord_y current y position in window
 * @param window_x new x position
 * @param window_y new y position
 */
void WG_PointToWindow(gint coord, gint width, gint height, gint coord_x, gint coord_y, gint *window_x, gint *window_y)
{
    gdouble wd, hd;
    gdouble radius;          /* radius of the polar area */
    gdouble unit;            /* window length of a polar unit */
    gdouble elevation;
    gdouble radians;         /* radians to be converted from degrees */
    gdouble x, y;            /* x and y in polar units */
    gdouble x_orig, y_orig;  /* x & y in window coordinates at polar origin */

    wd = (gdouble)width;
    hd = (gdouble)height;

    x = (gdouble) coord_x / 10.0;
    y = (gdouble) coord_y / 10.0;
    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            radians   = DEGREES_TO_RADIANS(x);
            elevation = y;
            x         = elevation * sin(radians);
            y         = elevation * cos(radians);

            radius    = MIN(wd, hd) / 2.0 - POLAR_BORDER;
            x_orig    = wd/2.0;
            y_orig    = hd/2.0;
            unit      = radius / MAX_POLAR_RADIUS;

            *window_x    = (gint) ROUND(x_orig + x * unit);
            if (*window_x < 0)
            {
                *window_x = 0;
            }
            *window_y    = (gint) ROUND(y_orig - y * unit);
            if (*window_y < 0)
            {
                *window_y = 0;
            }
            break;

        case KUBAND:
        case KUBAND2:
            *window_x = (gint) ((MAX_KU_CROSS_ELEVATION - x) * wd
                                / (MAX_KU_CROSS_ELEVATION * 2.0) );
            *window_y = (gint) ((MAX_KU_ELEVATION + y) * hd
                                / (MAX_KU_ELEVATION * 2.0));
            break;

        case STATION:
            *window_x = (gint) ((MAX_STATION_AZIMUTH - x) * wd
                                / (MAX_STATION_AZIMUTH * 2.0) );
            *window_y = (gint) ((MAX_STATION_ELEVATION - y) * hd
                                / (MAX_STATION_ELEVATION * 2.0));
            break;
    }
}

/**
 * This function converts from window coordinates to azimuth/elevation (alpha/beta)
 * in either rectangular or polar form. (algorithm provided by Dave Goeken).
 *
 * @param coord coordinate type
 * @param width width of window
 * @param height height of window
 * @param window_x current x position in window
 * @param window_y current y position in window
 * @param coord_x new x position
 * @param coord_y new y position
 */
void WG_WindowToPoint(gint coord, gint width, gint height, gint window_x, gint window_y, gint *coord_x, gint *coord_y)
{
    gdouble wd, hd;
    gdouble radius;          /* radius of the polar area */
    gdouble unit;            /* window length of a polar unit */
    gdouble elevation;

    wd = (gdouble)width;
    hd = (gdouble)height;

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            radius    = MIN(wd, hd) / 2.0 - POLAR_BORDER;
            unit      = radius / MAX_POLAR_RADIUS;
            elevation = sqrt( (window_x - wd/2.0)*(window_x - wd/2.0) +
                              (window_y - hd/2.0)*(window_y - hd/2.0) ) / unit;
            if ( 0.0 == elevation )
            {
                *coord_x = 0;
            }
            else
            {
                gdouble deg;
                if ( fabs( (window_x - wd/2.0) ) > fabs( (window_y - hd/2.0) ) )
                {
                    deg = asin( ( window_x - wd/2.0 ) / (unit * elevation ) );
                }
                else
                {
                    deg = acos( ( hd/2.0 - window_y ) / (unit * elevation ) );
                }

                deg = RADIANS_TO_DEGREES( deg );

                if ( ( 2*window_x > wd ) && ( 2*window_y > hd ) )
                {
                    if ( deg <= 90.0 ) deg = 180.0 - deg;
                }
                else if ( ( 2*window_x < wd ) && ( 2*window_y > hd ) )
                {
                    if ( deg <= 0.0 )
                        deg = -(180.0 + deg);
                    else if ( deg >= 135.0 )
                        deg = -deg;
                }
                else if ( ( 2*window_x < wd ) && ( 2*window_y < hd ) )
                {
                    if ( deg > 0.0 ) deg = -deg;
                }
                *coord_x = (gint) (10.0 * deg );
            }
            *coord_y = (gint) ( 10.0 * elevation );

            break;

        case KUBAND:
        case KUBAND2:
            *coord_x = (gint) (10.0 * MAX_KU_CROSS_ELEVATION * (1.0 - 2.0 * window_x / wd));
            *coord_y = (gint) (10.0 * MAX_KU_ELEVATION * (-1.0 + 2.0 * window_y / hd));
            break;

        case STATION:
            *coord_x = (gint) (10.0 * MAX_STATION_AZIMUTH * (1.0 - 2.0 * window_x / wd));
            *coord_y = (gint) (10.0 * MAX_STATION_ELEVATION * (1.0 - 2.0 * window_y / hd));
            break;
    }
}

/**
 * This function converts a point from azimuth/elevation (alpha/beta) to
 * flat 2-D coordinates, in either rectangular or polar form.
 *
 * @param coord coordinate type
 * @param width width of window
 * @param height height of window
 * @param coord_x current x position in window
 * @param coord_y current y position in window
 * @param window_x new x position
 * @param window_y new y position
 * @param fwdConversion conversion factor
 */
void WG_PointToFlatCoord(gint coord, gint width, gint height, gint coord_x, gint coord_y, gint *window_x, gint *window_y, gint fwdConversion)
{
    gdouble wd, hd;
    gdouble radius;           /* radius of the polar area                    */
    gdouble unit;             /* window length of a polar unit               */
    gdouble elevation;
    gdouble radians;          /* radians to be converted from degrees        */
    gdouble x, y, xmagnitude; /* x and y in polar units                      */
    gdouble x_orig, y_orig;   /* x & y in window coordinates at polar origin */

    wd = (gdouble)width;
    hd = (gdouble)height;

    if (fwdConversion)
    {
        x = (gdouble) coord_x / 10.0;
        y = (gdouble) coord_y / 10.0;
    }
    else
    {
        x = (gdouble) coord_x;
        y = (gdouble) coord_y;
    }

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            radius    = MIN(wd, hd) / 2.0 - POLAR_BORDER;
            x_orig    = wd/2.0;
            y_orig    = hd/2.0;
            unit      = radius / MAX_POLAR_RADIUS;

            if (fwdConversion)
            {
                radians   = DEGREES_TO_RADIANS(x);
                elevation = y;
                x         = elevation * sin(radians);
                y         = elevation * cos(radians);


                *window_x    = (gint) ROUND(x_orig + x * unit);
                *window_y    = (gint) ROUND(y_orig - y * unit);
            }
            else
            {
                xmagnitude = (x - x_orig) / unit;
                x = (x - x_orig) / unit;
                y = (y_orig - y) / unit;
                x = (x == 0.0) && (y == 0.0) ? 0.0 : atan2(x, y);
                y = ((gint)(fabs(RADIANS_TO_DEGREES(x))) == 90) ? ROUND(fabs(xmagnitude)) : ROUND(y / cos(x));
                *window_x = (gint)(ROUND(RADIANS_TO_DEGREES(x))) * 10;
                *window_y = (gint)(y) * 10;
            }
            break;

        case KUBAND:
        case KUBAND2:
            if (fwdConversion)
            {
                *window_x = (gint) ((MAX_KU_CROSS_ELEVATION + x) * wd
                                    / (MAX_KU_CROSS_ELEVATION * 2.0) );
                *window_y = (gint) ((MAX_KU_ELEVATION + y) * hd
                                    / (MAX_KU_ELEVATION * 2.0));
            }
            break;

        case STATION:
            if (fwdConversion)
            {
                *window_x = (gint) ((MAX_STATION_AZIMUTH - x) * wd
                                    / (MAX_STATION_AZIMUTH * 2.0) );
                *window_y = (gint) ((MAX_STATION_ELEVATION - y) * hd
                                    / (MAX_STATION_ELEVATION * 2.0));
            }
            break;
    }
}
