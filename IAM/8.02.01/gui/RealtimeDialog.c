/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
    #include <process.h>
#endif

#define PRIVATE
#include "RealtimeDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "AntennaDialog.h"
#include "ColorHandler.h"
#include "ConfigParser.h"
#include "EventDialog.h"
#include "FontHandler.h"
#include "HelpHandler.h"
#include "IspClient.h"
#include "IspHandler.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PixmapHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "PrintHandler.h"
#include "RealtimeDataHandler.h"
#include "RealtimeRFDialog.h"
#include "SolarDraw.h"
#include "TrackLimitsDialog.h"
#include "WindowGrid.h"

#ifdef G_OS_WIN32
/* so the windows it.lib will link */
extern "C"
{
#include "status.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/RealtimeDialog.c 256 2017-01-26 22:05:07Z dpham1@ndc.nasa.gov $");

}
#else
    #include "status.h"
#endif

#ifdef DEBUG
static void print_ptList(gint coord, GList *sets);
#endif

/*
 * set up for User Experience
 */
extern void GTKsetTopLevelWindow(GtkWidget *);
extern void GTKsetWindowIconified(GtkWidget *, gboolean);
extern void GTKsetAppConfigFunc(void *(*f)(int *));
extern void GTKsetAppRestoreFunc(void (*f)(void *, int));

#ifndef _EXT_PARTNERS
/*
 * functions used for User Experience
 */
static void *UEconfigFunc(int *);
static void UErestoreFunc(void *, int);
#endif

/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* optimum size based on 1280 X 1024 */
#define WINDOW_WIDTH    850
#define WINDOW_HEIGHT   650

/* symbols used for drawing area */
#define PAREN_BORESIGHT "(   )"
#define PLUS_BORESIGHT  " + "
static gchar         *thisBoreSight          = PAREN_BORESIGHT;
static const gchar   *thisSBandCrossHair     = "+";
static const gchar   *thisKuBandCrossHair    = "X";
static gchar         thisBoreSightColor[64]  = {0};

/**
 * Define private class variables
 */
static GtkWidget    *thisWindow             = NULL;
static GtkWidget    *thisWindowContainer    = NULL;
static GtkWidget    *thisNotebook           = NULL;

static GtkWidget    *thisTimeButton         = NULL;
static GtkWidget    *thisTimeLabel          = NULL;
static GtkWidget    *thisTimeStatus         = NULL;

static GtkWidget    *thisXmitValue          = NULL;
static GtkWidget    *thisXmitStatus         = NULL;
static GtkWidget    *thisXmitEventBox       = NULL;
static GdkColor     *thisXmitBgColor        = NULL;

static PangoFontDescription *thisPangoFontDescription = NULL;

/* window data */
static WindowData   thisWindowData[MAX_DRAWING_AREAS];

/**
 * defines the menu items
 */
static GtkUIManager *thisUImanager = NULL;

/**
 * One of SBAND1, SBAND2, KUBAND, STATION, or SBAND1_KUBAND, SBAND2_KUBAND
 */
static gint          thisCoordinateType = SBAND1;

/**
 * One of SBAND1_VIEW, SBAND2_VIEW, KUBAND_VIEW, STATION_VIEW,
 * S1KU_SBAND_VIEW, S1KU_KUBAND_VIEW, S2KU_SBAND_VIEW, S2KU_KUBAND_VIEW
 */
static gint          thisCoordinateView = SBAND1_VIEW;

/**
 * Flag indicating app is ready to draw.
 */
static gboolean     thisReady = False;

/**
 * The view orientation; horizontal or vertical
 */
static gint          thisViewOrientation = View_Orientation_Horizontal;

/**
 * This is initialized with set_kuband_view_to_start.
 */
static gint          thisKubandDisplayed;
static gboolean      thisKubandDisplayedIsActive;

static GtkWidget    *thisSBand1KuBandContainer = NULL;
static GtkWidget    *thisSBand2KuBandContainer = NULL;

/**
 * Flag indicating that coordinate tracking is enabled
 */
static gboolean     thisCoordinateTracking = False;

/**
 * Coordinate tracking color
 */
static gchar        thisTrackingColor[64] = {0};

/**
 * SBand tracking limit value
 */
static gint         thisSBandTrackingLimit = 150;

/**
 * Coordinate tracking area
 */
static Area         thisTrackingArea;

/**
 * Resize font resource
 */
static ResizeFont   thisResizeFont;

/**
 * Handler ids for the drawing area motion notify events
 */
static WidgetHandlerData  thisMotionHandlerData[NUM_VIEWS] =
{
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0}
};

/**
 * Stores the cached data for drawing the coordinate points
 * This is used to draw when the main draw routine is invoked.
 */
typedef struct TrackingData
{
    WindowData *windata;
    gchar message[64];
    gint x_loc;
    gint y_loc;
} TrackingData;

static TrackingData        thisTrackingData =
{
    NULL, {0}, 0, 0
};

/**
 * When SBAND1_KUBAND -> SBAND1 or KUBAND
 * When SBAND2_KUBAND -> SBAND2 or KUBAND
 */
static gint          thisSKUCoordinateType   = UNKNOWN_COORD;

static gboolean     thisShowSolarTrace      = False;
static gboolean     thisShowPredicts        = False;
static gboolean     thisShowConeLos         = False;
static gboolean     thisShowLimit           = False;
static gboolean     thisShowShuttle         = False;

static gint         thisDataSBand1          = SBand1DrawingArea;
static gint         thisDataSBand2          = SBand2DrawingArea;

static gint         thisDataKuBand          = KuBandDrawingArea;
static gint         thisDataKuBandLeft      = KuBandLeftDrawingArea;
static gint         thisDataKuBandBottom    = KuBandBottomDrawingArea;

static gint         thisDataStation         = StationDrawingArea;
static gint         thisDataStationLeft     = StationLeftDrawingArea;
static gint         thisDataStationBottom   = StationBottomDrawingArea;

static gint         thisDataS1KU_SBand       = S1KU_SBandDrawingArea;
static gint         thisDataS1KU_KuBand      = S1KU_KuBandDrawingArea;
static gint         thisDataS1KU_KuBandLeft  = S1KU_KuBandLeftDrawingArea;
static gint         thisDataS1KU_KuBandBottom= S1KU_KuBandBottomDrawingArea;

static gint         thisDataS2KU_SBand       = S2KU_SBandDrawingArea;
static gint         thisDataS2KU_KuBand      = S2KU_KuBandDrawingArea;
static gint         thisDataS2KU_KuBandLeft  = S2KU_KuBandLeftDrawingArea;
static gint         thisDataS2KU_KuBandBottom= S2KU_KuBandBottomDrawingArea;

static Area         thisRtArea[NUM_VIEWS];

static gboolean     thisIsResizing  = False;
static gboolean     thisIsIconified = False;

static GtkWidget    *thisViewOrientationButton = NULL;

static GtkWidget    *thisKubandToggleButton = NULL;
static GdkColor     yellow = {0, 0xffff, 0xffff, 0x0000};
static GdkColor     lightyellow = {0, 0xffff, 0xffff, 0x6666};
static GdkColor     gray = {0, 0xdddd, 0xdddd, 0xdddd};
static GdkColor     lightgray = {0, 0xeeee, 0xeeee, 0xeeee};
 

static gboolean     thisIsManualStructureChange = False;
static GtkActionGroup *thisStructureActionGroup = NULL;

/**
 * Define the basic menu action entries.
 */
static GtkActionEntry file_entries[] =
{
    { FILE_MENU, NULL, "_File"},
    { PREDICT_DISPLAY_MENU, NULL,               /* name, stock id */
            "Predict _Display", "<control>d",   /* label, accelerator */
            "Launch the predict display",       /* tooltip */
            G_CALLBACK (file_menu_action_cb)    /* action callback */
    },
    { REALTIME_DATA_DISPLAY_MENU, NULL,
            "_Realtime Data Display","<control>r",
            "Launch the realtime data display",
            G_CALLBACK (file_menu_action_cb)
    },
    { CHECK_ISP_STATUS_MENU, NULL,
            "Check _ISP Status","<control>s",
            "Check ISP connectivity",
            G_CALLBACK (file_menu_action_cb)
    },
    { PRINT_REALTIME_MENU, GTK_STOCK_PRINT,
            "_Print Realtime Screen", "<control>p",
            "Print the realtime display",
            G_CALLBACK (file_menu_action_cb)
    },
    { EXIT_MENU, GTK_STOCK_QUIT,
            "E_xit", "<control>x",
            "Quit",
            G_CALLBACK (file_menu_action_cb)
    }
};
static guint n_file_entries = G_N_ELEMENTS (file_entries);

static GtkActionEntry settings_entries[] =
{
    { SETTINGS_MENU, NULL, "_Settings"},
    { TRACK_LIMITS_MENU, NULL,
            "Track _Limits", "<shift><control>l",
            "Enable limit tracking",
            G_CALLBACK (settings_menu_action_cb)
    },
    { CHANGE_ANTENNA_LOCATION_MENU, NULL,
            "Change _Antenna Location", "<shift><control>a",
            "Reorient the antenna",
            G_CALLBACK (settings_menu_action_cb)
    },
    { CHANGE_COVERAGE_DIRECTORY_MENU, NULL,
            "Change _Coverage Directory", "<shift><control>c",
            "Change location of pointing coverage files",
            G_CALLBACK (settings_menu_action_cb)
    },
    { CHANGE_DYNAMIC_STRUCTURE_MENU, NULL,
            "Change Dynamic Structure"
    },
    { CHANGE_BORESIGHT, NULL,
            "Change Boresight", NULL,
            "Change the boresight symbol",
            G_CALLBACK (settings_menu_action_cb)
    },
    { CHANGE_VIEW_ORIENTATION, NULL,
            "Change View Orientation", NULL,
            "Change the orientation of the application",
            G_CALLBACK (settings_menu_action_cb)
    },
    { CHANGE_KUBAND_VIEW, NULL,
    	    "Change Ku-band View", NULL,
	    "Change which Ku-band is displayed",
	    G_CALLBACK (settings_menu_action_cb)
    },
    { RELOAD_CONFIG_FILE_MENU, NULL,
            "_Reload Config File", "<shift><control>r",
            "Reread the iam.xml file",
            G_CALLBACK (settings_menu_action_cb)
    },
};
static guint n_settings_entries = G_N_ELEMENTS (settings_entries);

static GtkActionEntry blockage_entries[] =
{
    { BLOCKAGE_MENU, NULL, "_Blockage"},
    { DISPLAY_CONE_MENU, NULL,
            "Display Cone", NULL,
            "Toggle view between LOS and Cone",
            G_CALLBACK (blockage_menu_action_cb)
    },

    { SBAND1_MENU, NULL, "SBand 1"},
    { SBAND2_MENU, NULL, "SBand 2"},
    { KUBAND_MENU, NULL, "Ku-band"},
    { KUBAND2_MENU, NULL, "Ku-band"},
    { STATION_MENU, NULL, "Station Az/El"},
};
static guint n_blockage_entries = G_N_ELEMENTS (blockage_entries);

static GtkActionEntry help_entries[] =
{
    { HELP_MENU,     NULL, "_Help"},
    { HELP_HELP_MENU, GTK_STOCK_HELP,
            "_Help", "F1",
            "View user's guide",
            G_CALLBACK (help_menu_action_cb)
    },
    { HELP_GTK_MENU, NULL,
            "About _GTK", "<shift>F1",
            "Information about GTK+",
            G_CALLBACK (help_menu_action_cb)
    },
    { HELP_ABOUT_MENU, NULL,
            "About _IAM", "<control>F1",
            "Information about IAM",
            G_CALLBACK (help_menu_action_cb)
    },
};
static guint n_help_entries = G_N_ELEMENTS (help_entries);

/**
 * Define the check button (toggle) action entries
 */
static GtkToggleActionEntry toggle_entries[] =
{
    /* settings menu toggles */
    { SHOW_PREDICTS_MENU, NULL,                 /* name, stock id */
        "Show _Predicts", "<shift><control>p",  /* label, accelerator */
        "Show predict TDRS tracks",             /* tooltip */
        G_CALLBACK (settings_menu_action_cb),   /* action handler */
        False                                   /* is active */
    },
    { SHOW_SOLAR_TRACE_MENU, NULL,
            "Show _Solar Trace", "<shift><control>s",
            "Show solar track",
            G_CALLBACK (settings_menu_action_cb),
            False
    },
    { COORDINATE_TRACKING_MENU, NULL,
            "Coordinate _Tracking", "<shift><control>t",
            "Enable coordinate tracking",
            G_CALLBACK (settings_menu_action_cb),
            False
    },

    /* blockage menu toggles */
    { SHOW_LIMITS_MENU, NULL,
            "Show Limits", NULL,
            "Toggle limit blockage",
            G_CALLBACK (blockage_menu_action_cb),
            False
    },
    { SHOW_SHUTTLE_DOCKED_MENU, NULL,
            "Show Shuttle Docked", NULL,
            "Toggle shuttle docked",
            G_CALLBACK (blockage_menu_action_cb),
            False
    }
};
static guint n_toggle_entries = G_N_ELEMENTS (toggle_entries);

/**
 * Define the boresight radio entries
 */
static GtkRadioActionEntry boresight_radio_entries[] =
{
    { CHANGE_BORESIGHT_TO_PLUS_MENU, NULL,      /* name, stock id */
        "+", NULL,                              /* label, accelerator */
        "Use cross-hair", Boresight_Plus        /* tooltip, value */
    },
    { CHANGE_BORESIGHT_TO_PARENS_MENU, NULL,
            "( )", NULL,
            "Use parens", Boresight_Parens
    }
};
static guint n_boresight_radio_entries = G_N_ELEMENTS (boresight_radio_entries);

/**
 * Define the view orientation radio entries
 */
static GtkRadioActionEntry view_orientation_radio_entries[] =
{
    { CHANGE_VIEW_ORIENTATION_TO_HORIZONTAL_MENU, NULL,
        "Horizontal", "<control>H",
        NULL, View_Orientation_Horizontal
    },
    { CHANGE_VIEW_ORIENTATION_TO_VERTICAL_MENU, NULL,
            "Vertical", "<control>V",
            NULL, View_Orientation_Vertical
    }
};
static guint n_view_orientation_radio_entries = G_N_ELEMENTS (view_orientation_radio_entries);

static GtkRadioActionEntry kuband_view_radio_entries[] =
{
    { CHANGE_KUBAND_VIEW_TO_ONE_MENU, NULL,
      "1", NULL, NULL, 1
    },
    { CHANGE_KUBAND_VIEW_TO_TWO_MENU, NULL,
      "2", NULL, NULL, 2
    }
};
static guint n_kuband_view_radio_entries = G_N_ELEMENTS (kuband_view_radio_entries);

/**
 * Define the menu layout. Each MENU definition must coorespond
 * to an action entry. The blockage menu items will be added
 * at run-time.
 */
static const gchar *thisFileMenu =
    "       <menu action='"FILE_MENU"'>\n"
    "           <menuitem action='"PREDICT_DISPLAY_MENU"'/>\n"
    "           <menuitem action='"REALTIME_DATA_DISPLAY_MENU"'/>\n"
    "           <menuitem action='"PRINT_REALTIME_MENU"'/>\n"
    "           <separator/>\n"
    "           <menuitem action='"CHECK_ISP_STATUS_MENU"'/>\n"
    "           <separator/>\n"
    "           <menuitem action='"EXIT_MENU"'/>\n"
    "       </menu>\n";

static const gchar *thisSettingsMenu =
    "       <menu action='"SETTINGS_MENU"'>\n"
    "           <menuitem action='"TRACK_LIMITS_MENU"'/>\n"
    "           <menuitem action='"SHOW_PREDICTS_MENU"'/>\n"
    "           <menuitem action='"SHOW_SOLAR_TRACE_MENU"'/>\n"
    "           <separator/>\n"
    "           <menuitem action='"COORDINATE_TRACKING_MENU"'/>\n"
    "           <menuitem action='"CHANGE_ANTENNA_LOCATION_MENU"'/>\n"
    "           <menuitem action='"CHANGE_COVERAGE_DIRECTORY_MENU"'/>\n"
    "           <menu action='"CHANGE_DYNAMIC_STRUCTURE_MENU"'>\n"
    "           </menu>\n";

static const gchar *thisSettingsMenu2 =
    "           <menu action='"CHANGE_BORESIGHT"'>\n"
    "               <menuitem action='"CHANGE_BORESIGHT_TO_PLUS_MENU"'/>\n"
    "               <menuitem action='"CHANGE_BORESIGHT_TO_PARENS_MENU"'/>\n"
    "           </menu>\n"
    "           <menu action='"CHANGE_VIEW_ORIENTATION"'>\n"
    "               <menuitem action='"CHANGE_VIEW_ORIENTATION_TO_HORIZONTAL_MENU"'/>\n"
    "               <menuitem action='"CHANGE_VIEW_ORIENTATION_TO_VERTICAL_MENU"'/>\n"
    "           </menu>\n"
    "           <menu action='"CHANGE_KUBAND_VIEW"'>\n"
    "               <menuitem action='"CHANGE_KUBAND_VIEW_TO_ONE_MENU"'/>\n"
    "               <menuitem action='"CHANGE_KUBAND_VIEW_TO_TWO_MENU"'/>\n"
    "           </menu>\n"
    "           <menuitem action='"RELOAD_CONFIG_FILE_MENU"'/>\n"
    "      </menu>\n";

static const gchar *thisBlockageMenu =
    "      <menu action='"BLOCKAGE_MENU"'>\n"
    "           <menuitem action='"DISPLAY_CONE_MENU"'/>\n"
    "           <menuitem action='"SHOW_LIMITS_MENU"'/>\n"
    "           <menuitem action='"SHOW_SHUTTLE_DOCKED_MENU"'/>\n"
    "           <separator/>\n"
    "           <menu action='"SBAND1_MENU"'>\n"
    "           </menu>\n"
    "           <menu action='"SBAND2_MENU"'>\n"
    "           </menu>\n"
    "           <menu action='"KUBAND_MENU"'>\n"
    "           </menu>\n"
    "           <menu action='"KUBAND2_MENU"'>\n"
    "           </menu>\n"
    "           <menu action='"STATION_MENU"'>\n"
    "           </menu>\n"
    "      </menu>\n";

static const gchar *thisHelpMenu =
    "      <menu action='"HELP_MENU"'>\n"
    "           <menuitem action='"HELP_HELP_MENU"'/>\n"
    "           <menuitem action='"HELP_GTK_MENU"'/>\n"
    "           <menuitem action='"HELP_ABOUT_MENU"'/>\n"
    "      </menu>\n";
/*************************************************************************/

/**
 * Create the main window instance
 *
 * @return True if window was created ok, otherwise False
 */
gboolean RTD_Create(void)
{
    guint i,j;
    gchar temp[32];

    if (thisWindow == NULL)
    {
        for (i=0; i<NUM_VIEWS; i++)
        {
            thisRtArea[i].count = 0;
            for (j=0; j<MAX_AREAS; j++)
            {
                thisRtArea[i].rect[j][AREA_X] = 0;
                thisRtArea[i].rect[j][AREA_Y] = 0;
                thisRtArea[i].rect[j][AREA_W] = 0;
                thisRtArea[i].rect[j][AREA_H] = 0;
            }
        }

        for (i=0; i<MAX_DRAWING_AREAS; i++)
        {
            switch (i)
            {
                /* these are handled differently - pixmaps not needed */
                /* they're data is drawn directly into the drawable */
                case KuBandLeftDrawingArea:
                case KuBandBottomDrawingArea:
                case S1KU_KuBandLeftDrawingArea:
                case S1KU_KuBandBottomDrawingArea:
                case S2KU_KuBandLeftDrawingArea:
                case S2KU_KuBandBottomDrawingArea:
                case StationLeftDrawingArea:
                case StationBottomDrawingArea:
                    /* do nothing */
                    break;

                default:
                    thisWindowData[i].toolbar = NULL;
                    thisWindowData[i].toolbar2 = NULL;
                    thisWindowData[i].tool_items = NULL;
                    thisWindowData[i].tool_items2 = NULL;
                    thisWindowData[i].drawing_area = NULL;
                    thisWindowData[i].src_pixmap = NULL;
                    thisWindowData[i].redraw_pixmap = True;
                    thisWindowData[i].is_resized = False;

                    /* where the structure is drawn */
                    thisWindowData[i].dynamic_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].dynamic_data->pixmap = NULL;
                    thisWindowData[i].dynamic_data->redraw = True;
                    thisWindowData[i].dynamic_data->width = 0;
                    thisWindowData[i].dynamic_data->height = 0;

                    /* where static masks are drawn (except the limit mask */
                    thisWindowData[i].mask_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].mask_data->pixmap = NULL;
                    thisWindowData[i].mask_data->redraw = True;
                    thisWindowData[i].mask_data->width = 0;
                    thisWindowData[i].mask_data->height = 0;

                    /* where the obscuration masks are draw */
                    thisWindowData[i].obs_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].obs_data->pixmap = NULL;
                    thisWindowData[i].obs_data->redraw = True;
                    thisWindowData[i].obs_data->width = 0;
                    thisWindowData[i].obs_data->height = 0;

                    /* the tdrs track go here */
                    thisWindowData[i].track_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].track_data->pixmap = NULL;
                    thisWindowData[i].track_data->redraw = True;
                    thisWindowData[i].track_data->width = 0;
                    thisWindowData[i].track_data->height = 0;

                    /* static background */
                    thisWindowData[i].bgpixmap_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].bgpixmap_data->pixmap = NULL;
                    thisWindowData[i].bgpixmap_data->redraw = True;
                    thisWindowData[i].bgpixmap_data->width = 0;
                    thisWindowData[i].bgpixmap_data->height = 0;
                    break;
            }
        }

        /* get the boresight color to use, if not found use white */
        if (CP_GetConfigData(IAM_BORESIGHT_COLOR, thisBoreSightColor) == False)
        {
            g_stpcpy(thisBoreSightColor, "White");
        }

        /* get coordinate tracking color from config file */
        if (CP_GetConfigData(IAM_TRACKING_COLOR, thisTrackingColor) == False)
        {
            /* couldn't find, default to white */
            g_stpcpy(thisTrackingColor, "White");
        }
        thisTrackingArea.count = 0;

        /* get sband tracking limit value */
        if (CP_GetConfigData(SBAND_COORD_TRACKING_LIMIT, temp) == False)
        {
            thisSBandTrackingLimit = 150;
        }
        else
        {
            sscanf(temp, "%d", &thisSBandTrackingLimit);
        }

        /* initialize font resize instance */
        g_stpcpy(thisResizeFont.rc_string, IAM_GTKRC_REALTIME_FONT_SIZE);
        thisResizeFont.default_font_size = DEFAULT_FONT_SIZE;
        thisResizeFont.previous_font_size = DEFAULT_FONT_SIZE;
        thisResizeFont.default_width = WINDOW_WIDTH;
        thisResizeFont.previous_width = WINDOW_WIDTH;
        thisResizeFont.new_width = -1;
        thisResizeFont.default_height = WINDOW_HEIGHT;
        thisResizeFont.previous_height = WINDOW_HEIGHT;
        thisResizeFont.new_height = -1;

        /* initialize the realtime data handler */
        RTDH_Constructor();

        /* initialize the dialog class */
        D_DialogConstructor();

        /* initialize the pixmap handler */
        PH_Constructor();

        /* create the realtime dialog window */
        create_window(&thisResizeFont);

        /* set icon and title */
        D_SetIcon(thisWindow);
        RTD_SetDialogTitle();

        /* create the pango font description */
        set_pango_font_desc(&thisResizeFont, thisWindow->style->font_desc);

        /* set the coverage menu item to False if 'Mission' activity */
        if (AM_GetActivityType() == ACTIVITY_TYPE_MIS)
        {
            D_EnableMenuItem(thisUImanager,
                             "/"MENU_BAR"/"FILE_MENU"/"CHANGE_COVERAGE_DIRECTORY_MENU,
                             False);
        }

        /* create the real-time data dialog */
        /* we need to instantiate this component so isp */
        /* can send data to populate the components */
        RTRFD_Create();
    }

    return(thisWindow ? True : False);
}

/**
 * This method opens the dialog
 */
void RTD_Open(void)
{
    static gboolean default_view = False;

    if (thisWindow != NULL)
    {
        if (thisIsIconified == True)
        {
            gtk_window_deiconify(GTK_WINDOW(thisWindow));
        }

        gtk_window_set_position(GTK_WINDOW(thisWindow), GTK_WIN_POS_CENTER);
        gtk_widget_show_all(thisWindow);
        gdk_window_raise(thisWindow->window);

        /* set the default view */
        /* this will only set the view the first time */
        /* subsequent calls are ignored */
        if (default_view == False)
        {
            default_view = True;

            D_SetDefaultView(REALTIME, thisNotebook);

            /* set the sband and station masks */
            set_masks_enabled(SBAND1, IAM_REALTIME_SBAND1_ENABLED_MASKS);
            set_masks_enabled(SBAND2, IAM_REALTIME_SBAND2_ENABLED_MASKS);
	    set_masks_enabled(KUBAND, IAM_REALTIME_KUBAND_ENABLED_MASKS);
	    set_masks_enabled(KUBAND2, IAM_REALTIME_KUBAND2_ENABLED_MASKS);
            set_masks_enabled(STATION, IAM_REALTIME_STATION_ENABLED_MASKS);
            AM_SetActiveDisplay(DISPLAY_RD, True);
            set_kuband_view_to_start();
        }
        else
        {
            set_kuband_displayed(thisKubandDisplayed);
        }

    }
}

/**
 * Returns the window instance
 *
 * @return this window
 */
GtkWidget *RTD_GetDialog(void)
{
    return(thisWindow);
}

/**
 * Releases all memory
 */
void RTD_Destructor(void)
{
    guint i;
    GList *glist;

    for (i=0; i<MAX_DRAWING_AREAS; i++)
    {
        for (glist = thisWindowData[i].tool_items; glist; glist = glist->next)
        {
            MH_Free(glist->data);
        }

        if (thisWindowData[i].src_pixmap != NULL)
        {
            g_object_unref(thisWindowData[i].src_pixmap);
        }
        if (thisWindowData[i].dynamic_data != NULL)
        {
            if (thisWindowData[i].dynamic_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].dynamic_data->pixmap);
            }
            MH_Free(thisWindowData[i].dynamic_data);
        }
        if (thisWindowData[i].obs_data != NULL)
        {
            if (thisWindowData[i].obs_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].obs_data->pixmap);
            }
            MH_Free(thisWindowData[i].obs_data);
        }
        if (thisWindowData[i].mask_data != NULL)
        {
            if (thisWindowData[i].mask_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].mask_data->pixmap);
            }
            MH_Free(thisWindowData[i].mask_data);
        }
        if (thisWindowData[i].track_data != NULL)
        {
            if (thisWindowData[i].track_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].track_data->pixmap);
            }
            MH_Free(thisWindowData[i].track_data);
        }
        if (thisWindowData[i].bgpixmap_data != NULL)
        {
            if (thisWindowData[i].bgpixmap_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].bgpixmap_data->pixmap);
            }
            MH_Free(thisWindowData[i].bgpixmap_data);
        }
    }
}

/**
 * Returns the primary drawing area window for the currently selected
 * coordinate type; SBAND1, SBAND2, KUBAND, STATION
 *
 * @param coord current coordinate type
 *
 * @return windowdata struct of primary drawing area or NULL
 */
WindowData *RTD_GetWindowData(gint coord)
{
    WindowData *windata = NULL;
    gint index = get_index_by_coord(coord);

    if (index != -1)
    {
        windata = &thisWindowData[index];
    }
    else
    {
        windata = get_window_data_by_coord(thisCoordinateType, coord);
    }

    return(windata);
}

/**
 * Returns a pixmap save area for the given coordinate.
 *
 * @param coord current coordinate type
 *
 * @return Area* or NULL
 */
Area *RTD_GetArea(gint coord)
{
    Area *area = NULL;

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
        case KUBAND:
            area = &thisRtArea[coord];
            break;

        case KUBAND2:
        case STATION:
            area = &thisRtArea[coord-1];
            break;

        case SBAND1_KUBAND:
            {
                if (thisSKUCoordinateType == SBAND1)
                {
                    area = &thisRtArea[SBAND1_VIEW];
                }
                else
                {
                    area = &thisRtArea[KUBAND_VIEW];
                }
            }
            break;

        case SBAND2_KUBAND:
            {
                if (thisSKUCoordinateType == SBAND2)
                {
                    area = &thisRtArea[SBAND2_VIEW];
                }
                else
                {
                    area = &thisRtArea[KUBAND_VIEW];
                }
            }
            break;

        default:
            break;
    }

    return(area);
}

/**
 * Does actual creation of the window component
 *
 * @param resize_font has font resize information
 *
 * @return window instance
 */
static void create_window(ResizeFont *resize_font)
{
    GtkWidget *vbox;
    GtkWidget *vbox2;
    GtkWidget *hbox;
    GtkWidget *separator;
    gint view_orientation_val = View_Orientation_Horizontal;

    /* create window */
    thisWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_name(thisWindow, "Realtime");

#ifndef _EXT_PARTNERS
    /* set top level window for User Experience */
    GTKsetTopLevelWindow(thisWindow);
    GTKsetWindowIconified(thisWindow, thisIsIconified);
    GTKsetAppConfigFunc(&UEconfigFunc);
    GTKsetAppRestoreFunc(&UErestoreFunc);
#endif

    /*
    * windows/dialogs can only have one container!
    * the first vbox will contain the menubar and hbox. this allows the menubar
    * to attach to the left/right edges without any margin offsets
    */
    vbox = gtk_vbox_new(False, 0);

    /* set the orientation and size of window */
    view_orientation_val = create_orientation_view(resize_font);

    /* create the menu bar */
    create_menu_bar(view_orientation_val);

    /* add dynamic items to the menu bar */
    D_AddBlockageItemsToMenubar(REALTIME, SBAND1,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,
                                (GCallback)sband1_blockage_action_cb);
    D_AddBlockageItemsToMenubar(REALTIME, SBAND2,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,
                                (GCallback)sband2_blockage_action_cb);
    D_AddBlockageItemsToMenubar(REALTIME, KUBAND,  thisUImanager,           
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,
                                (GCallback)kuband_blockage_action_cb);
    D_AddBlockageItemsToMenubar(REALTIME, KUBAND2,  thisUImanager,           
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,
                                (GCallback)kuband2_blockage_action_cb);
    D_AddBlockageItemsToMenubar(REALTIME, STATION, thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU,
                                (GCallback)station_blockage_action_cb);
    thisStructureActionGroup = D_AddStructureItemsToMenuBar(REALTIME, thisUImanager,
                                                            "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_DYNAMIC_STRUCTURE_MENU,
                                                            (GCallback)structure_change_action_cb);

    /* add menubar to main view */
    gtk_box_pack_start (GTK_BOX (vbox),
                        gtk_ui_manager_get_widget (thisUImanager, "/"MENU_BAR),
                        False, False, 0);

    /* this will select the menu item and make the call to the callback */
    /* preselect solar trace and limits */
    D_PreselectMenuItem(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"SHOW_LIMITS_MENU);
    D_PreselectMenuItem(thisUImanager, "/"MENU_BAR"/"SETTINGS_MENU"/"SHOW_PREDICTS_MENU);

    /* setup callbacks */
    g_signal_connect(G_OBJECT(thisWindow), "delete-event", G_CALLBACK(close_cb), NULL);
    g_signal_connect(G_OBJECT(thisWindow), "window-state-event", G_CALLBACK(visibility_cb), NULL);
    g_signal_connect(G_OBJECT(thisWindow), "configure-event", G_CALLBACK(resize_window_cb), resize_font);
    g_signal_connect(G_OBJECT(thisWindow), "focus-in-event", G_CALLBACK(focus_in_event_cb), NULL);
    g_signal_connect(G_OBJECT(thisWindow), "focus-out-event", G_CALLBACK(focus_out_event_cb), NULL);


    /* this is the hbox that will house the vbox which has the components */
    hbox = gtk_hbox_new(True, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, True, True, 5);

    /* the vbox is the main container that has all the components */
    thisWindowContainer = gtk_hbox_new(True, 0);
    gtk_box_pack_start (GTK_BOX (hbox), thisWindowContainer, True, True, 5);

    vbox2 = gtk_vbox_new(False, 0);
    gtk_box_pack_start (GTK_BOX (thisWindowContainer), vbox2, True, True, 5);

    /* now put the primary top-level container into the window */
    gtk_container_add(GTK_CONTAINER(thisWindow), vbox);

    /* create row of buttons */
    create_button_form(vbox2);

    separator = gtk_hseparator_new ();
    gtk_box_pack_start (GTK_BOX (vbox2), separator, False, False, 3);

    thisNotebook = D_CreateNotebook(vbox2);
    g_signal_connect(GTK_NOTEBOOK(thisNotebook), "switch_page", G_CALLBACK(page_switch_cb), NULL);

    /* SBAND 1 */
    create_drawing_area(SBAND1, SBAND1_VIEW,
                        SBand1DrawingArea,
                        SBAND1_TITLE, (GCallback)sband1_blockage_clicked_cb, &thisDataSBand1);

    /* SBAND 2 */
    create_drawing_area(SBAND2, SBAND2_VIEW,
                        SBand2DrawingArea,
                        SBAND2_TITLE, (GCallback)sband2_blockage_clicked_cb, &thisDataSBand2);

    /* KuBAND */
    create_drawing_area3(KUBAND, KUBAND_VIEW,
                         KuBandDrawingArea, KuBandLeftDrawingArea, KuBandBottomDrawingArea,
			 KUBAND_TITLE, (GCallback)kuband_blockage_clicked_cb, &thisDataKuBand,
                         &thisDataKuBand, &thisDataKuBandLeft, &thisDataKuBandBottom);

    /* Station */
    create_drawing_area3(STATION, STATION_VIEW,
                         StationDrawingArea, StationLeftDrawingArea, StationBottomDrawingArea,
                         STATION_TITLE, (GCallback)station_blockage_clicked_cb, &thisDataStation,
                         &thisDataStation, &thisDataStationLeft, &thisDataStationBottom);

    /* SBAND 1 and KuBAND */
    thisSBand1KuBandContainer = create_drawing_area2(SBAND1, S1KU_SBAND_VIEW,
                                                     S1KU_SBandDrawingArea,
                                                     SBAND1_TITLE,
                                                     &thisDataS1KU_SBand,
                                                     &thisDataS1KU_SBand,
                                                     SBAND1_KUBAND, S1KU_KUBAND_VIEW,
                                                     S1KU_KuBandDrawingArea,
                                                     S1KU_KuBandLeftDrawingArea,
                                                     S1KU_KuBandBottomDrawingArea,
                                                     SBAND1_KUBAND_TITLE,
                                                     (GCallback)sband1_blockage_clicked_cb,
                                                     &thisDataS1KU_KuBand,
                                                     &thisDataS1KU_KuBand,
                                                     &thisDataS1KU_KuBandLeft,
                                                     &thisDataS1KU_KuBandBottom,
                                                     view_orientation_val);

    /* SBAND 2 and KuBAND */
    thisSBand2KuBandContainer = create_drawing_area2(SBAND2, S2KU_SBAND_VIEW,
                                                     S2KU_SBandDrawingArea,
                                                     SBAND2_TITLE,
                                                     &thisDataS2KU_SBand,
                                                     &thisDataS2KU_SBand,
                                                     SBAND2_KUBAND, S2KU_KUBAND_VIEW,
                                                     S2KU_KuBandDrawingArea,
                                                     S2KU_KuBandLeftDrawingArea,
                                                     S2KU_KuBandBottomDrawingArea,
                                                     SBAND2_KUBAND_TITLE,
                                                     (GCallback)sband2_blockage_clicked_cb,
                                                     &thisDataS2KU_KuBand,
                                                     &thisDataS2KU_KuBand,
                                                     &thisDataS2KU_KuBandLeft,
                                                     &thisDataS2KU_KuBandBottom,
                                                     view_orientation_val);

    /* stop motion handlers */
    /* these handlers are only needed when the track */
    /* coordinates is enabled */
    D_BlockMotionHandlers(thisMotionHandlerData);
}

/**
 * Creates the menu bar from the ui
 *
 * @param view_orientation_val the default view orientation to use
 */
static void create_menu_bar(gint view_orientation_val)
{
    GtkActionGroup *actions;
    GError *error = NULL;
    gchar *ui_info;

    thisUImanager = gtk_ui_manager_new ();
    gtk_ui_manager_set_add_tearoffs(thisUImanager, True);

    actions = gtk_action_group_new (FILE_MENU);
    gtk_action_group_add_actions (actions, file_entries, n_file_entries, NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(SETTINGS_MENU);
    gtk_action_group_add_actions (actions, settings_entries, n_settings_entries, NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(BLOCKAGE_MENU);
    gtk_action_group_add_actions (actions, blockage_entries, n_blockage_entries, NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(HELP_MENU);
    gtk_action_group_add_actions (actions, help_entries, n_help_entries, NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new("Toggle Actions");
    gtk_action_group_add_toggle_actions (actions,
                                         toggle_entries, n_toggle_entries,
                                         NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(CHANGE_BORESIGHT);
    gtk_action_group_add_radio_actions (actions,
                                        boresight_radio_entries, n_boresight_radio_entries,
                                        Boresight_Parens,
                                        G_CALLBACK (settings_menu_boresight_cb),
                                        NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(CHANGE_VIEW_ORIENTATION);
    gtk_action_group_add_radio_actions (actions,
                                        view_orientation_radio_entries, n_view_orientation_radio_entries,
                                        view_orientation_val,
                                        G_CALLBACK (settings_menu_view_orientation_cb),
                                        NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);
    
    actions = gtk_action_group_new(CHANGE_KUBAND_VIEW);
    gtk_action_group_add_radio_actions (actions,
    					kuband_view_radio_entries, n_kuband_view_radio_entries,
					1, // This needs to be a gint that defaults to the active kuband
					G_CALLBACK (settings_menu_kuband_view_cb),
					NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    gtk_window_add_accel_group (GTK_WINDOW (thisWindow),
                                gtk_ui_manager_get_accel_group (thisUImanager));

    ui_info = D_CreateMenu(thisFileMenu, thisSettingsMenu, thisSettingsMenu2, thisBlockageMenu, thisHelpMenu);
    if (ui_info != NULL)
    {
        g_message("create_window: realtime, ui_info=%s",ui_info);
        if (!gtk_ui_manager_add_ui_from_string (thisUImanager, ui_info, -1, &error))
        {
            g_warning("RealtimeDialog: building menus failed: %s", error->message);
            g_error_free (error);
        }
    }
    else
    {
        g_warning("RealtimeDialog: building menus failed");
    }
    g_free(ui_info);
}

/**
 * Determines the orientation from the iam.xml file. Defaults to
 * horizontal if not found. Then the size of the window is set
 * based on the optimum size for the orientation.
 *
 * @param resize_font font resize information
 *
 * @return View_Orientation_Horizontal or View_Orientation_Vertical
 */
static gint create_orientation_view(ResizeFont *resize_font)
{
    gint view_orientation_val = View_Orientation_Horizontal;
    gchar view_orientation_str[128];
    Dimension *dim;

    /* get window orientation */
    /* now determine which orientation to initially use */
    if (CP_GetConfigData(IAM_VIEW_ORIENTATION, view_orientation_str) == True)
    {
        if (g_ascii_strcasecmp(view_orientation_str, VERTICAL_STR) == 0)
        {
            view_orientation_val = View_Orientation_Vertical;
        }
        else
        {
            view_orientation_val = View_Orientation_Horizontal;
        }
    }

    /* compute optimum window size */
    /* size the window based on view orientation */
    dim = AM_GetWindowSize(thisWindow, WINDOW_WIDTH, WINDOW_HEIGHT);
    {
        if (view_orientation_val == View_Orientation_Vertical)
        {
            thisViewOrientation = View_Orientation_Vertical;
            gtk_window_set_default_size(GTK_WINDOW(thisWindow), dim->height, dim->width);
            gtk_widget_set_size_request(GTK_WIDGET(thisWindow),
                                        (gint)(dim->height*WINDOW_MIN_PERCENT),
                                        (gint)(dim->width*WINDOW_MIN_PERCENT));

            /* reset the resize font defaults */
            resize_font->default_width = dim->height;
            resize_font->default_height = dim->width;
        }
        else
        {
            thisViewOrientation = View_Orientation_Horizontal;
            gtk_window_set_default_size(GTK_WINDOW(thisWindow), dim->width, dim->height);
            gtk_widget_set_size_request(GTK_WIDGET(thisWindow),
                                        (gint)(dim->width*WINDOW_MIN_PERCENT),
                                        (gint)(dim->height*WINDOW_MIN_PERCENT));

            /* reset the resize font defaults */
            resize_font->default_width = dim->width;
            resize_font->default_height = dim->height;
        }
    }
    MH_Free(dim);

    return view_orientation_val;
}

/**
 * Creates a basic sband drawing area.
 *
 * @param coord either SBAND1 or SBAND2
 * @param view either SBAND1_VIEW or SBAND2_VIEW
 * @param drawing_area the sband 1 or 2 drawing area index
 * @param title sband title to use in the notebook tab
 * @param toolbar_callback callback for the toolbar
 * @param notify_data data for the leave-notify-callback
 */
static void create_drawing_area(gint coord, gint view, gint drawing_area, gchar *title, GCallback toolbar_callback, gint *notify_data)
{
    GtkWidget *frame;

    thisWindowData[drawing_area].coordinate_type = coord;
    thisWindowData[drawing_area].toolbar = gtk_toolbar_new();
    thisWindowData[drawing_area].handle_box = gtk_handle_box_new();
    thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&frame,
                                                                    title,
                                                                    thisWindowData[drawing_area].handle_box,
                                                                    NULL,
                                                                    thisWindowData[drawing_area].toolbar,
                                                                    NULL,
                                                                    -1,
                                                                    -1);
    D_AddBlockageItemsToToolbar(REALTIME, coord,
                                thisWindowData[drawing_area].toolbar,
                                &thisWindowData[drawing_area].tool_items,
                                title,  toolbar_callback);

    gtk_widget_set_name(thisWindowData[drawing_area].drawing_area, title);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), notify_data);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), notify_data);

    gtk_widget_add_events(thisWindowData[drawing_area].drawing_area, (GDK_ENTER_NOTIFY_MASK |
                                                                      GDK_LEAVE_NOTIFY_MASK |
                                                                      GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[view].instance = thisWindowData[drawing_area].drawing_area;

    thisMotionHandlerData[view].handler_id = g_signal_connect(thisWindowData[drawing_area].drawing_area,
                                                              "motion-notify-event",
                                                              G_CALLBACK(D_DrawCoordinateTracking_cb),
                                                              NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "enter-notify-event",
                     G_CALLBACK(enter_notify_cb), GINT_TO_POINTER(view));

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "leave-notify-event",
                     G_CALLBACK(leave_notify_cb), notify_data);

    D_NotebookAdd(thisNotebook, title, &frame);
}

/**
 * Creates a multiple view that includes an sband view with kuband.
 *
 * @param sband_coord either SBAND1 or SBAND2
 * @param sband_view either SBAND1_VIEW or SBAND2_VIEW
 * @param sband_drawing_area main drawing area index
 * @param sband_title title used internally by the widget
 * @param sband_notify_data data for the leave-notify-event handler
 * @param sband_expose_data data for the expose event handler
 * @param coord usally KUBAND
 * @param view usally KUBAND_VIEW
 * @param drawing_area main kuband drawing area index
 * @param left_drawing_area left kuband drawing area index
 * @param bottom_drawing_area bottom kuband drawing area index
 * @param sku_title a title for the notebook tab
 * @param toolbar_callback callback handler for toolbar (sband only)
 * @param notify_data data for the leave-notify-event
 * @param expose_data data for the main drawing area expose event
 * @param left_expose_data left drawing area expose event data
 * @param bottom_expose_data bottom drawing area expose event data
 * @param view_orientation_val orients the view either vertically or
 * horizontally; controlled by iam.xml parameter.
 *
 * @return container for the whole view
 */
static GtkWidget *create_drawing_area2(gint sband_coord, gint sband_view,
                                       gint sband_drawing_area,
                                       gchar *sband_title,
                                       gint *sband_notify_data,
                                       gint *sband_expose_data,
                                       gint coord, gint view,
                                       gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                                       gchar *sku_title, GCallback toolbar_callback, gint *notify_data,
                                       gint *expose_data, gint *left_expose_data, gint *bottom_expose_data,
                                       gint view_orientation_val)
{
    GtkWidget *container;
    GtkWidget *sband_frame;
    GtkWidget *left_frame;
    GtkWidget *bottom_frame;
    GtkWidget *center_frame;

    /* SBAND */
    thisWindowData[sband_drawing_area].coordinate_type = sband_coord;

    thisWindowData[sband_drawing_area].toolbar = gtk_toolbar_new();
    thisWindowData[drawing_area].handle_box = gtk_handle_box_new();

    thisWindowData[sband_drawing_area].drawing_area = D_CreateDrawingArea(&sband_frame,
                                                                          sband_title,
                                                                          thisWindowData[drawing_area].handle_box,
                                                                          NULL,
                                                                          thisWindowData[sband_drawing_area].toolbar,
                                                                          NULL,
                                                                          -1,
                                                                          -1);

    D_AddBlockageItemsToToolbar(REALTIME, sband_coord,
                                thisWindowData[sband_drawing_area].toolbar,
                                &thisWindowData[sband_drawing_area].tool_items,
                                sband_title,  (GCallback)toolbar_callback);

    gtk_widget_set_name(thisWindowData[sband_drawing_area].drawing_area, "SKU_SBand");

    g_signal_connect(thisWindowData[sband_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), sband_expose_data);

    g_signal_connect(thisWindowData[sband_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), sband_expose_data);

    gtk_widget_add_events(thisWindowData[sband_drawing_area].drawing_area, (GDK_ENTER_NOTIFY_MASK |
                                                                            GDK_LEAVE_NOTIFY_MASK |
                                                                            GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[sband_view].instance = thisWindowData[sband_drawing_area].drawing_area;

    thisMotionHandlerData[sband_view].handler_id = g_signal_connect(thisWindowData[sband_drawing_area].drawing_area,
                                                                    "motion-notify-event",
                                                                    G_CALLBACK(D_DrawCoordinateTracking_cb), NULL);

    g_signal_connect(thisWindowData[sband_drawing_area].drawing_area, "enter-notify-event",
                     G_CALLBACK(enter_notify_cb), GINT_TO_POINTER(sband_view));

    g_signal_connect(thisWindowData[sband_drawing_area].drawing_area, "leave-notify-event",
                     G_CALLBACK(leave_notify_cb), sband_notify_data);

    /* KUBAND */

    /* Left drawing area */
    thisWindowData[left_drawing_area].coordinate_type = coord;

    thisWindowData[left_drawing_area].drawing_area = D_CreateDrawingArea(&left_frame,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL,
                                                                         NULL,
                                                                         LABEL_AREA_WIDTH,
                                                                         -1);

    gtk_widget_set_name(thisWindowData[left_drawing_area].drawing_area, "Left");

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), left_expose_data);

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), left_expose_data);

    /* Bottom drawing area */
    thisWindowData[bottom_drawing_area].coordinate_type = coord;

    thisWindowData[bottom_drawing_area].drawing_area = D_CreateDrawingAreaWithExtra(&bottom_frame,
                                                                                    HORIZONTAL_DIRECTION,
                                                                                    LABEL_AREA_WIDTH,
                                                                                    LABEL_AREA_HEIGHT);

    gtk_widget_set_name(thisWindowData[bottom_drawing_area].drawing_area, "Bottom");

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), bottom_expose_data);

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), bottom_expose_data);

    /* Main drawing area */
    thisWindowData[drawing_area].coordinate_type = KUBAND;

    thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&center_frame,
                                                                    KUBAND_TITLE,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL,
                                                                    NULL,
                                                                    -1,
                                                                    -1);

    gtk_widget_set_name(thisWindowData[drawing_area].drawing_area, "SKU_KuBand");

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), expose_data);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), expose_data);

    gtk_widget_add_events(thisWindowData[drawing_area].drawing_area, (GDK_ENTER_NOTIFY_MASK |
                                                                      GDK_LEAVE_NOTIFY_MASK |
                                                                      GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[view].instance = thisWindowData[drawing_area].drawing_area;

    thisMotionHandlerData[view].handler_id = g_signal_connect(thisWindowData[drawing_area].drawing_area,
                                                              "motion-notify-event",
                                                              G_CALLBACK(D_DrawCoordinateTracking_cb), NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "enter-notify-event",
                     G_CALLBACK(enter_notify_cb), GINT_TO_POINTER(view));

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "leave-notify-event",
                     G_CALLBACK(leave_notify_cb), notify_data);

    D_ContainerAdd(KUBAND_TITLE, &container, &left_frame, &bottom_frame, &center_frame);

    return D_NotebookAdd2(thisNotebook, sku_title,
                          view_orientation_val,
                          &sband_frame, &container);
}

/**
 * Creates a view that has the left and bottom coordinate measurement included.
 * This is used by the kuband and station views.
 *
 * @param coord either KUBAND or STATION
 * @param view either KUBAND_VIEW or STATION_VIEW
 * @param drawing_area main drawing area index
 * @param left_drawing_area the left drawing area index
 * @param bottom_drawing_area the bottom drawing area index
 * @param title a title for the notebook tab
 * @param toolbar_callback callback handler for toolbar, if NULL, it will
 * not be instantiated
 * @param notify_data data for the leave-notify-event
 * @param expose_data data for the main drawing area expose event
 * @param left_expose_data left drawing area expose event data
 * @param bottom_expose_data bottom drawing area expose event data
 */
static void create_drawing_area3(gint coord, gint view,
                                 gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                                 gchar *title, GCallback toolbar_callback, gint *notify_data,
                                 gint *expose_data, gint *left_expose_data, gint *bottom_expose_data)
{
    GtkWidget *left_frame;
    GtkWidget *bottom_frame;
    GtkWidget *center_frame;

    /* Left drawing area */
    thisWindowData[left_drawing_area].coordinate_type = coord;

    /* if toolbar is required, add extra space at the top of */
    /* the left frame. */
    if (toolbar_callback)
    {
        thisWindowData[left_drawing_area].drawing_area = D_CreateDrawingAreaWithExtra(&left_frame,
                                                                                      VERTICAL_DIRECTION,
                                                                                      LABEL_AREA_WIDTH,
                                                                                      SMALL_ICON_SIZE*2+5);
    }
    else
    {
        thisWindowData[left_drawing_area].drawing_area = D_CreateDrawingArea(&left_frame,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             LABEL_AREA_WIDTH,
                                                                             -1);
    }

    gtk_widget_set_name(thisWindowData[left_drawing_area].drawing_area, "Left");

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), left_expose_data);

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), left_expose_data);

    /* Bottom drawing area */
    thisWindowData[bottom_drawing_area].coordinate_type = coord;

    thisWindowData[bottom_drawing_area].drawing_area = D_CreateDrawingAreaWithExtra(&bottom_frame,
                                                                                    HORIZONTAL_DIRECTION,
                                                                                    LABEL_AREA_WIDTH,
                                                                                    LABEL_AREA_HEIGHT);

    gtk_widget_set_name(thisWindowData[bottom_drawing_area].drawing_area, "Bottom");

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), bottom_expose_data);

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), bottom_expose_data);

    /* main drawing area */
    thisWindowData[drawing_area].coordinate_type = coord;

    /* add toolbar if we have a callback */
    if (toolbar_callback)
    {
        thisWindowData[drawing_area].toolbar = gtk_toolbar_new();
        thisWindowData[drawing_area].handle_box = gtk_handle_box_new();

        if (coord == KUBAND) 
        {
            thisWindowData[drawing_area].toolbar2 = gtk_toolbar_new();
            thisWindowData[drawing_area].handle_box2 = gtk_handle_box_new();
        }

    }

    if (coord == KUBAND)
        thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&center_frame,
                                                                        title,
                                                                        thisWindowData[drawing_area].handle_box,
                                                                        thisWindowData[drawing_area].handle_box2,
                                                                        thisWindowData[drawing_area].toolbar,
                                                                        thisWindowData[drawing_area].toolbar2,
                                                                        -1,
                                                                        -1);
    else 
        thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&center_frame,
                                                                        title,
                                                                        thisWindowData[drawing_area].handle_box,
                                                                        NULL,
                                                                        thisWindowData[drawing_area].toolbar,
                                                                        NULL,
                                                                        -1,
                                                                        -1);

    D_AddBlockageItemsToToolbar(REALTIME, coord,
                                thisWindowData[drawing_area].toolbar,
                                &thisWindowData[drawing_area].tool_items,
                                title,  (GCallback)toolbar_callback);

    if (coord == KUBAND)
    {
        D_AddBlockageItemsToToolbar(REALTIME, KUBAND2,
                                    thisWindowData[drawing_area].toolbar2,
                                    &thisWindowData[drawing_area].tool_items2,
                                    KUBAND2_TITLE, (GCallback)kuband2_blockage_clicked_cb);
    }

    gtk_widget_set_name(thisWindowData[drawing_area].drawing_area, title);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(expose_cb), expose_data);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resize_drawing_area_cb), expose_data);

    gtk_widget_add_events(thisWindowData[drawing_area].drawing_area, (GDK_ENTER_NOTIFY_MASK |
                                                                      GDK_LEAVE_NOTIFY_MASK |
                                                                      GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[view].instance = thisWindowData[drawing_area].drawing_area;

    thisMotionHandlerData[view].handler_id = g_signal_connect(thisWindowData[drawing_area].drawing_area,
                                                              "motion-notify-event",
                                                              G_CALLBACK(D_DrawCoordinateTracking_cb),
                                                              NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "enter-notify-event",
                     G_CALLBACK(enter_notify_cb), GINT_TO_POINTER(view));

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "leave-notify-event",
                     G_CALLBACK(leave_notify_cb), notify_data);

    /* put them all together */
    D_NotebookAdd3(thisNotebook, title, &left_frame, &bottom_frame, &center_frame);
}

/**
 * Create the gui components that reside on top of the drawing areas
 *
 * @param vbox container for buttons
 */
static void create_button_form(GtkWidget *vbox)
{
    GtkWidget *data_frame;
    GtkWidget *hbox;
    GtkWidget *hbox2;
    GtkWidget *hbox3;
    GtkWidget *button;

    hbox2 = gtk_hbox_new(False, 0);
    {
        thisTimeButton = gtk_button_new_with_label(GetTimeName(AM_GetTimeType()));
        gtk_box_pack_start(GTK_BOX(hbox2), thisTimeButton, False, False, 3);
        g_signal_connect(thisTimeButton, "pressed", G_CALLBACK(time_cb), NULL);

        thisTimeLabel = D_CreateLabel("", LEFT_ALIGN, NULL);
        D_SetLabelColor(thisTimeLabel, GOOD_VALUE);

        thisTimeStatus = D_CreateLabel("M", RIGHT_ALIGN, NULL);

        hbox = gtk_hbox_new(False, 2);
        {
            gtk_box_pack_start(GTK_BOX(hbox), thisTimeLabel, False, False, 0);
            gtk_box_pack_start(GTK_BOX(hbox), thisTimeStatus, False, False, 0);

            data_frame = D_CreateDataFrame(hbox);
            gtk_box_pack_start(GTK_BOX(hbox2), data_frame, False, False, 0);
        }
    }

    hbox3 = gtk_hbox_new(False, 0);
    {
    	thisKubandToggleButton = gtk_button_new_with_label(KU_TOGGLE_LABEL_2);
	gtk_box_pack_start(GTK_BOX(hbox3), thisKubandToggleButton, False, False, 2);
	g_signal_connect(thisKubandToggleButton, "pressed", G_CALLBACK(ku_toggle_pressed_cb), NULL);
	
        thisViewOrientationButton = gtk_button_new_with_label(VIEW_H_to_V);
        gtk_box_pack_start(GTK_BOX(hbox3), thisViewOrientationButton, False, False, 2);
        g_signal_connect(thisViewOrientationButton, "pressed", G_CALLBACK(view_orientation_pressed_cb), NULL);

        button = gtk_button_new_with_label(REFRESH_LABEL);
        gtk_box_pack_start(GTK_BOX(hbox3), button, False, False, 2);
        g_signal_connect(button, "pressed", G_CALLBACK(refresh_cb), NULL);

        button = gtk_button_new_with_label(CLEAR_LABEL);
        gtk_box_pack_start(GTK_BOX(hbox3), button, False, False, 2);
        g_signal_connect(button, "pressed", G_CALLBACK(clear_cb), NULL);

        hbox = gtk_hbox_new(False, 0);
        {
            gchar font_desc[256];
            PangoFontDescription *pango_font_desc;

            thisXmitValue = D_CreateLabel(KU_XMIT_STR, CENTER_ALIGN, NULL);
            gtk_box_pack_start(GTK_BOX(hbox), thisXmitValue, True, True, 0);

            g_sprintf(font_desc, "%s Bold %d",
                      pango_font_description_get_family(thisXmitValue->style->font_desc), M_FONT_SIZE);
            pango_font_desc = pango_font_description_from_string(font_desc);
            gtk_widget_modify_font(thisXmitValue, pango_font_desc);
            pango_font_description_free(pango_font_desc);

            thisXmitStatus = D_CreateLabel("M", CENTER_ALIGN, NULL);
            gtk_box_pack_start(GTK_BOX(hbox), thisXmitStatus, True, True, 0);

            data_frame = D_CreateDataFrame(hbox);
            gtk_widget_set_size_request(data_frame, 55, 0);

            gtk_box_pack_start(GTK_BOX(hbox3), data_frame, False, False, 2);

            thisXmitEventBox = gtk_bin_get_child(GTK_BIN(data_frame));
            thisXmitBgColor = CH_GetGdkColorByName(D_BgColor());
        }
    }

    hbox = gtk_hbox_new(False, 0);
    gtk_box_pack_start(GTK_BOX(hbox), hbox2, False, True, 0);
    gtk_box_pack_end(GTK_BOX(hbox), hbox3, False, True, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, False, False, 0);
}


/**
 * Sets the pango font description to use. Usally set during the
 * resize operation.
 *
 * @param resize_font font resize information
 * @param font_desc the font description that has font family.
 */
static void set_pango_font_desc(ResizeFont *resize_font, PangoFontDescription *font_desc)
{
    gchar font_name[256];

    /* create font description for drawing methods */
    g_sprintf(font_name, "%s Bold %d",
              pango_font_description_get_family(font_desc),
              resize_font->previous_font_size);

    if (thisPangoFontDescription != NULL)
    {
        pango_font_description_free(thisPangoFontDescription);
    }
    thisPangoFontDescription = pango_font_description_from_string(font_name);
}

/**
 * Returns the current pango font description set
 *
 * @return PangoFontDescription*
 */
PangoFontDescription *RTD_GetPangoFontDescription(void)
{
    return thisPangoFontDescription;
}

/**
 * Sets the realtime dialog title
 */
void RTD_SetDialogTitle(void)
{
    D_SetDialogTitle(thisWindow, REALTIME_TITLE, thisCoordinateType);
}

/**
 * This method determines if the window is up and active
 *
 * @return True or False
 */
gboolean RTD_IsActive(void)
{
    if (thisWindow != NULL && GTK_WIDGET_VISIBLE(thisWindow))
    {
        return(True);
    }

    return(False);
}

/**
 * Returns the resizing flag. This flag is set the configure_cb and cleared
 * in the expose_cb
 *
 * @return resizing state
 */
gboolean RTD_IsResizing(void)
{
    return(thisIsResizing);
}

/**
 * Returns a flag indicating if the dialog is iconified or not
 *
 * @return True or False
 */
gboolean RTD_IsIconified(void)
{
    return(thisIsIconified);
}

/**
 * Returns the current coordinate type selected.
 *
 * @return current coordinate type
 * SBAND1, SBAND2, KUBAND, or STATION
 */
gint RTD_GetCoordinateType(void)
{
    if (thisCoordinateType == SBAND1_KUBAND ||
        thisCoordinateType == SBAND2_KUBAND)
    {
        if (thisCoordinateType == SBAND1_KUBAND &&
            thisSKUCoordinateType == UNKNOWN_COORD)
        {
            thisSKUCoordinateType = SBAND1;
        }
        else if (thisCoordinateType == SBAND2_KUBAND &&
                 thisSKUCoordinateType == UNKNOWN_COORD)
        {
            thisSKUCoordinateType = SBAND2;
        }

        return(thisSKUCoordinateType);
    }

    return(thisCoordinateType);
}

/**
 * Returns the current view
 *
 * @return current coordinate view
 * SBAND1_VIEW, SBAND2_VIEW, KUBAND_VIEW, STATION_VIEW,
 * S1KU_SBAND_VIEW, S1KU_KUBAND_VIEW, S2KU_SBAND_VIEW, or S2KU_KUBAND_VIEW
 */
gint RTD_GetCoordinateView(void)
{
    return(thisCoordinateView);
}

/**
 * Returns the active state of the currently displayed ku-band
 */
gboolean RTD_GetKubandIsActive(void)
{
    return(thisKubandDisplayedIsActive);
}

/**
 * Sets the value for the given index
 *
 * @param symbol load data for this symbol
 */
void RTD_SetValue(SymbolRec *symbol)
{
    GtkWidget *label = get_status_label(symbol->symbol_id);
    set_status(label, symbol);

    label = get_value_label(symbol->symbol_id);
    if (symbol->symbol_id == IS_Gmt)
    {
        set_time_value(label, symbol, False);
    }
    else
    {
        set_value(label, symbol);
    }
}

/**
 * This method sets the background color of the xmittr. When the color is GC_BG, the
 * original background color is used
 *
 * @param color color for ku xmit
 */
void RTD_SetKuXmitColor(gint color)
{
    GdkColor *gcolor;

    if (color == GC_BG)
    {
        gcolor = thisXmitBgColor;
    }
    else
    {
        gcolor = CH_GetGdkColorByIndex(color);
    }

    if (gcolor != NULL)
    {
        gtk_widget_modify_bg(GTK_WIDGET(thisXmitEventBox), GTK_STATE_NORMAL, gcolor);
    }
}

/**
 * Sets the ku xmit status
 *
 * @param symbol load data for this symbol
 */
void RTD_SetKuXmitStatus(SymbolRec *symbol)
{
    gchar indicator[2];
    indicator[0] = symbol->status.indicator;
    indicator[1] = 0;

    D_SetLabelText(thisXmitStatus, indicator,
                   CH_GetGdkColorByName(CH_GetColorName(symbol->status.color)));
}

/**
 * Sets the ku xmit value
 *
 * @param symbol load data for this symbol
 */
void RTD_SetKuXmitValue(SymbolRec *symbol)
{
    D_SetLabelText(thisXmitValue, symbol->value_str,
                   CH_GetGdkColorByName(CH_GetColorName(symbol->value_color)));
}

/**
 * Sets the status of the xmit/time field
 *
 * @param label status widget
 * @param symbol use status color
 */
static void set_status(GtkWidget *label, SymbolRec *symbol)
{
    if (label != NULL && symbol != NULL)
    {
        gchar indicator[2];
        indicator[0] = symbol->status.indicator;
        indicator[1] = 0;
        D_SetLabelText(label, indicator,
                       CH_GetGdkColorByName(CH_GetColorName(symbol->status.color)));
    }
}

/**
 * Sets the value of the xmit field
 *
 * @param label value widget
 * @param symbol use value color
 */
static void set_value(GtkWidget *label, SymbolRec *symbol)
{
    if (label != NULL && symbol != NULL)
    {
        D_SetLabelText(label, symbol->value_str,
                       CH_GetGdkColorByName(CH_GetColorName(symbol->value_color)));
    }
}

/**
 * Sets the time value field
 *
 * @param label time data
 * @param symbol use value color
 * @param force force load flag
 */
static void set_time_value(GtkWidget *label, SymbolRec *symbol, gboolean force)
{
    WindowData *windata;
    static gchar oldTime[TIME_STR_LEN] = UNSET_TIME;
    gchar time_str[TIME_STR_LEN] = "";

    if (label == NULL || symbol == NULL)
    {
        return;
    }

    D_SetLabelColor(label, CH_GetColorName(symbol->value_color));

    /*-----------------------------*/
    /* Set the time on the display */
    /*-----------------------------*/
    if (ValidCurrentUtc())
    {
        gint time_type;

        g_stpcpy(time_str, CurrentUtc());

        time_type = AM_GetTimeType();
        if (time_type == TS_GMT)
        {
            UtcToGmt(time_str);
        }

        else if (time_type == TS_PET)
        {
            UtcToPet(time_str);
        }

        D_SetLabelText(label, time_str, NULL);
    }
    else
    {
        D_SetLabelText(label, "", NULL);
    }

    /*-----------------------------------*/
    /* Check if current time has changed */
    /*-----------------------------------*/
    if (force || (g_ascii_strcasecmp(CurrentUtc(), oldTime) != 0))
    {
        /*-------------------------------------------------------------*/
        /* Check for special case where time goes backwards.  This can */
        /* occur when a SIM is restarted/rewinded.  If a time warp     */
        /* occurs, force a reload of the predict data.                 */
        /*-------------------------------------------------------------*/
        if (g_ascii_strcasecmp(CurrentUtc(), oldTime) < 0)
        {
            switch (thisCoordinateType)
            {
                case SBAND1_KUBAND:
                    if (thisKubandDisplayed == 1)
                    {
                        RTDH_LoadRtPredicts(SBAND1, TD_TDRW, True);
                        RTDH_LoadRtPredicts(SBAND1, TD_TDRE, True);
                        RTDH_LoadRtPredicts(KUBAND, TD_TDRW, True);
                        RTDH_LoadRtPredicts(KUBAND, TD_TDRE, True);
                    }
                    else
                    {
                        RTDH_LoadRtPredicts(SBAND1, TD_TDRW, True);
                        RTDH_LoadRtPredicts(SBAND1, TD_TDRE, True);
                        RTDH_LoadRtPredicts(KUBAND2, TD_TDRW, True);
                        RTDH_LoadRtPredicts(KUBAND2, TD_TDRE, True);
                    }
                    break;

                case SBAND2_KUBAND:
                    if (thisKubandDisplayed == 1)
                    {
                        RTDH_LoadRtPredicts(SBAND2, TD_TDRW, True);
                        RTDH_LoadRtPredicts(SBAND2, TD_TDRE, True);
                        RTDH_LoadRtPredicts(KUBAND, TD_TDRW, True);
                        RTDH_LoadRtPredicts(KUBAND, TD_TDRE, True);
                    }
                    else
                    {
                        RTDH_LoadRtPredicts(SBAND2, TD_TDRW, True);
                        RTDH_LoadRtPredicts(SBAND2, TD_TDRE, True);
                        RTDH_LoadRtPredicts(KUBAND2, TD_TDRW, True);
                        RTDH_LoadRtPredicts(KUBAND2, TD_TDRE, True);
                    }
                    break;

                default:
                    RTDH_LoadRtPredicts(thisCoordinateType, TD_TDRW, True);
                    RTDH_LoadRtPredicts(thisCoordinateType, TD_TDRE, True);
                    break;
            }
            RTDH_ClearAll();
        }

        /*------------------------------*/
        /* Check tracks for time limits */
        /*------------------------------*/
        RTDH_CheckRtLimits();

        /*---------------------------------------------*/
        /* Set the current predict data on the display */
        /*---------------------------------------------*/
        RTRFD_SetTdrsPredicts(True, TD_TDRW);
        RTRFD_SetTdrsPredicts(True, TD_TDRE);

        /*--------------------------*/
        /* Refresh the drawing area */
        /*--------------------------*/
        if (thisCoordinateType == SBAND1_KUBAND)
        {
            windata = get_window_data_by_coord(SBAND1_KUBAND, SBAND1);
            if (windata != NULL)
                D_RefreshAreas(windata,
                               windata->src_pixmap, windata->track_data->pixmap,
                               &thisRtArea[S1KU_SBAND_VIEW], thisIsResizing);

            windata = get_window_data_by_coord(SBAND1_KUBAND, KUBAND);
            if (windata != NULL)
                D_RefreshAreas(windata,
                               windata->src_pixmap, windata->track_data->pixmap,
                               &thisRtArea[S1KU_KUBAND_VIEW], thisIsResizing);
        }
        else if (thisCoordinateType == SBAND2_KUBAND)
        {
            windata = get_window_data_by_coord(SBAND2_KUBAND, SBAND2);
            if (windata != NULL)
                D_RefreshAreas(windata,
                               windata->src_pixmap, windata->track_data->pixmap,
                               &thisRtArea[S2KU_SBAND_VIEW], thisIsResizing);

            windata = get_window_data_by_coord(SBAND2_KUBAND, KUBAND);
            if (windata != NULL)
                D_RefreshAreas(windata,
                               windata->src_pixmap, windata->track_data->pixmap,
                               &thisRtArea[S2KU_KUBAND_VIEW], thisIsResizing);
        }
        else
        {
            windata = get_window_data_by_coord(thisCoordinateType, thisCoordinateType);
            if (windata != NULL)
                D_RefreshAreas(windata,
                               windata->src_pixmap, windata->track_data->pixmap,
                               &thisRtArea[thisCoordinateView], thisIsResizing);
        }
        RTD_DrawData();

        g_stpcpy(oldTime, CurrentUtc());
    }
}

/**
 * Handles the callback elabelvents for the file menu items
 *
 * @param action what got us here
 */
static void file_menu_action_cb(GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, PREDICT_DISPLAY_MENU) == 0)
        {
            if (PD_Create() == True)
            {
                PD_Open();
            }
        }

        else if (g_ascii_strcasecmp(action_name, REALTIME_DATA_DISPLAY_MENU) == 0)
        {
            if (RTRFD_Create() == True)
            {
                RTRFD_Open();
            }
        }

        else if (g_ascii_strcasecmp(action_name, CHECK_ISP_STATUS_MENU) == 0)
        {
            IC_DisplayIspStatus(thisWindow);
        }

        else if (g_ascii_strcasecmp(action_name, PRINT_REALTIME_MENU) == 0)
        {
            PRP_PrintRealtime(thisWindow->window,
                              thisWindow->allocation.width,
                              thisWindow->allocation.height);
        }

        else if (g_ascii_strcasecmp(action_name, EXIT_MENU) == 0)
        {
            D_ExitIam();
        }

        else
        {
            ;
        }
    }
}

/**
 * Processes the 'settings' menu items.
 *
 * @param action the menu item action that got us here
 */
static void settings_menu_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, TRACK_LIMITS_MENU) == 0)
        {
            TrackLimit_Create(GTK_WINDOW(thisWindow));
            TrackLimit_Open();
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_PREDICTS_MENU) == 0)
        {
            thisShowPredicts = ! thisShowPredicts;
            RTD_SetRedraw(True);
            RTD_DrawData();
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_SOLAR_TRACE_MENU) == 0)
        {
            thisShowSolarTrace = ! thisShowSolarTrace;
            RTD_SetRedraw(True);
            RTD_DrawData();
        }

        else if (g_ascii_strcasecmp(action_name, COORDINATE_TRACKING_MENU) == 0)
        {
            thisCoordinateTracking = ! thisCoordinateTracking;
            if (thisCoordinateTracking == True)
            {
                D_UnblockMotionHandlers(thisMotionHandlerData);
            }
            else
            {
                thisTrackingData.windata = NULL;
                D_BlockMotionHandlers(thisMotionHandlerData);
            }
        }

        else if (g_ascii_strcasecmp(action_name, CHANGE_ANTENNA_LOCATION_MENU) == 0)
        {
            AD_Create(GTK_WINDOW(thisWindow));
            AD_Open();
        }

        else if (g_ascii_strcasecmp(action_name, CHANGE_COVERAGE_DIRECTORY_MENU) == 0)
        {
            PDH_GetCoverage(GTK_WINDOW(thisWindow));
        }

        else if (g_ascii_strcasecmp(action_name, RELOAD_CONFIG_FILE_MENU) == 0)
        {
            /* this will reload the config data */
            CP_Destructor();
            CP_Constructor();

            /* this will reload limits */
            IH_Destructor();
            IH_Constructor();
        }

        else
        {
        }
    }
}

/**
 * Processes the view orientation action emiited by the radio buttons.
 *
 * @param action what got us here
 * @param current the current radio button selection
 */
static void settings_menu_view_orientation_cb(GtkAction *action, GtkRadioAction *current)
{
    set_view_orientation(gtk_radio_action_get_current_value(current));
}

/**
 * Processes the ku-band view toggle action from the menu.
 *
 * @param action that brought us here
 * @param the current radio button selection
 */
static void settings_menu_kuband_view_cb(GtkAction *action, GtkRadioAction *current)
{
    set_kuband_displayed(gtk_radio_action_get_current_value(current));
}

/**
 * Handles the button callback
 *
 * @param widget the button
 * @param data user data
 */
static void view_orientation_pressed_cb(GtkWidget *widget, void * data)
{
    set_view_orientation((thisViewOrientation == View_Orientation_Horizontal ?
                          View_Orientation_Vertical : View_Orientation_Horizontal));
}

/**
 * Processes the ku-band view toggle action from the button.
 *
 * @param widget = the toggle button
 * @param data = user data
 */
static void ku_toggle_pressed_cb(GtkWidget *widget, void * data)
{
	// Toggle which Kuband is displayed
	set_kuband_displayed(thisKubandDisplayed == 1 ? 2 : 1);
}

/**
 * Handles the settings action callback
 *
 * @param action what got us here
 * @param current radio button action
 */
static void settings_menu_boresight_cb(GtkAction *action, GtkRadioAction *current)
{
    gint boresight = gtk_radio_action_get_current_value(current);
    if (boresight == Boresight_Parens)
    {
        thisBoreSight = PAREN_BORESIGHT;
        RTD_SetRedraw(True);
        RTD_DrawData();
    }

    else if (boresight == Boresight_Plus)
    {
        thisBoreSight = PLUS_BORESIGHT;
        RTD_SetRedraw(True);
        RTD_DrawData();
    }
}

/**
 * Handles the dynamic structure change action callback
 *
 * @param action what got us here
 */
static void structure_change_action_cb(GtkAction *action, GtkRadioAction *current)
{
    if (thisIsManualStructureChange == False)
    {
        const gchar *action_name = gtk_action_get_name(GTK_ACTION(current));
        if (action_name != NULL)
        {
            gchar *structure = g_path_get_basename(action_name);
            DD_ChangeStructure(structure);
            PD_ChangeStructure(action_name);
            g_free(structure);
        }
    }
}

/**
 * Changes the structure selection in the menu pulldown. Its assumed
 * that the DD_ChangeStructure has already been called, so we want
 * to disable the structure_change callback.
 *
 * @param action_name the structure to change to
 */
void RTD_ChangeStructure(const gchar *action_name)
{
    if (thisStructureActionGroup != NULL)
    {
        GtkAction *action = gtk_action_group_get_action(thisStructureActionGroup, action_name);
        if (action)
        {
            thisIsManualStructureChange = True;
            gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(action), True);
            thisIsManualStructureChange = False;
        }
    }
}

/**
 * Handles the blockage action callback
 *
 * @param action what got us here
 */
static void blockage_menu_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, DISPLAY_CONE_MENU) == 0)
        {
            /* change the text in the menu drop down */
            GtkWidget *menu;
            GtkWidget *child;
            if (thisShowConeLos == eSHOW_CONE)
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"DISPLAY_CONE_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), SHOW_LOS);
                }
            }
            else
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"DISPLAY_CONE_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), SHOW_CONE);
                }
            }
            thisShowConeLos = ! thisShowConeLos;

            OM_ShowCone(REALTIME, thisShowConeLos);
            RTD_SetRedraw(True);
            RTD_DrawData();
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_LIMITS_MENU) == 0)
        {
            thisShowLimit = ! thisShowLimit;

            /* for the limits mask, the coordinate type is not important */
            /* except, we don't want to pass in one of the dual coordinate */
            /* types, that would effectively do nothing. */
            set_blockage_mask(SBAND1, MF_GetLimitMaskNum(REALTIME, SBAND1));
            RTD_SetRedraw(True);
            RTD_DrawData();
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_SHUTTLE_DOCKED_MENU) == 0)
        {
            thisShowShuttle = ! thisShowShuttle;

            OM_ShowShuttle(REALTIME, thisShowShuttle);
            RTD_SetRedraw(True);
            RTD_DrawData();
        }

        else
        {
        }
    }
}

/**
 * Handles the help action callback
 *
 * @param action what got us here
 */
static void help_menu_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, HELP_HELP_MENU) == 0)
        {
            HH_HelpIam();
        }

        else if (g_ascii_strcasecmp(action_name, HELP_ABOUT_MENU) == 0)
        {
            HH_AboutIam(thisWindow);
        }

        else if (g_ascii_strcasecmp(action_name, HELP_GTK_MENU) == 0)
        {
            HH_AboutGTK(thisWindow);
        }

        else
        {
        }
    }
}

static void set_kuband_view_to_start()
{
        SymbolRec *KuTRC1 = IS_GetSymbol(IS_KuTRC1);
        SymbolRec *KuTRC2 = IS_GetSymbol(IS_KuTRC2);
        gint kuband_view_to_start = 0;
        gchar kuband_start_value[32] = {0};
        gchar error_message[100];

        if (KuTRC1 != NULL && KuTRC2 != NULL)  // NULL if ISP option is off
        {
            if (KuTRC1->value == 1 && KuTRC2->value == 0)
                kuband_view_to_start = 1;
            else if (KuTRC1->value == 0 && KuTRC2->value == 1) 
                kuband_view_to_start = 2;
        }

        if (kuband_view_to_start == 0)
        {
            if (CP_GetConfigData(IAM_KUBAND_VIEW_STARTUP, kuband_start_value) == False)
            {
                /* couldn't find. */
                /* ignore and just let the default behavior happen */
                WarningMessage(AM_GetActiveDisplay(), "Unable to determine active or default ku-band", 
                               "Unable to determine active ku-band from ISP and unable to locate IAM_KUBAND_VIEW_STARTUP in the iam.xml file: The application is defaulting to ku-band 1.");		
                g_message("%s not defined in the iam.xml file!", IAM_KUBAND_VIEW_STARTUP);
                kuband_view_to_start = 1;  // We have no idea what to use, so just use 1.
            }
            else
            {
                // If we've gotten here, then use the default
                sprintf(error_message, "The default ku-band specified in the xml file will be used: ku-band %s", kuband_start_value);
                WarningMessage(AM_GetActiveDisplay(), "Unable to determine active ku-band.", error_message);
                g_message("Unable to determine active ku-band.  Using the default %s.",kuband_start_value);
                if (strcmp(kuband_start_value,"2") == 0)  
                    kuband_view_to_start = 2;
                else
                    kuband_view_to_start = 1;
            }
        }
        set_kuband_displayed(kuband_view_to_start);
}

static void set_kuband_displayed(gint kuband_to_display)
{
        GtkAction *action;
        SymbolRec *KuTRC1 = IS_GetSymbol(IS_KuTRC1);
        SymbolRec *KuTRC2 = IS_GetSymbol(IS_KuTRC2);
        static gboolean in_kuband_displayed = True;
	
        if (in_kuband_displayed == True)
        {
	    // Set the displayed Kuband
	    thisKubandDisplayed = kuband_to_display;
	
	    // Set the button label
	    GtkWidget *mywidget = gtk_bin_get_child(GTK_BIN(thisKubandToggleButton));
	    gtk_label_set_text(GTK_LABEL(mywidget), (thisKubandDisplayed == 1 ? 
	    		       KU_TOGGLE_LABEL_1 : KU_TOGGLE_LABEL_2));
	
	    // Modify the background color if required & ISP available.  Yellow = non-active Kuband
            if (KuTRC1 != NULL && KuTRC2 != NULL)
            {
	        if ((kuband_to_display == 1 && KuTRC1->value == 0)  ||
                    (kuband_to_display == 2 && KuTRC2->value == 0)  ||
		    (KuTRC1->value == 1 && KuTRC2->value == 1))
	        {
	    	        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &yellow);
		        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightyellow);
		        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &yellow);
                        thisKubandDisplayedIsActive = False;
	        }
	        else
	        {
		        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &gray);
		        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightgray);
		        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &gray);	
                        thisKubandDisplayedIsActive = True;
	        }
            }
            else
            {
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &yellow);
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightyellow);
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &yellow);
                thisKubandDisplayedIsActive = False;
            }

            // Set coordinate type for drawing area
            if (kuband_to_display == 1)
            {
                if (thisCoordinateType == KUBAND2) thisCoordinateType = KUBAND;
                thisWindowData[KuBandDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S1KU_KuBandDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S2KU_KuBandDrawingArea].coordinate_type = KUBAND;
                gtk_widget_show(thisWindowData[KuBandDrawingArea].handle_box);
                gtk_widget_hide(thisWindowData[KuBandDrawingArea].handle_box2);
                thisWindowData[KuBandLeftDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S1KU_KuBandLeftDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S2KU_KuBandLeftDrawingArea].coordinate_type = KUBAND;
                thisWindowData[KuBandBottomDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S1KU_KuBandBottomDrawingArea].coordinate_type = KUBAND;
                thisWindowData[S2KU_KuBandBottomDrawingArea].coordinate_type = KUBAND;

                MF_ForceRedraw (REALTIME, KUBAND_VIEW);
                MF_ForceRedraw (REALTIME, S1KU_KUBAND_VIEW);
                MF_ForceRedraw (REALTIME, S2KU_KUBAND_VIEW);
                RTD_SetRedraw(True);
                RTD_DrawData();
            }
            else
            {
                if (thisCoordinateType == KUBAND) thisCoordinateType = KUBAND2;
                thisWindowData[KuBandDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S1KU_KuBandDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S2KU_KuBandDrawingArea].coordinate_type = KUBAND2;
                gtk_widget_hide(thisWindowData[KuBandDrawingArea].handle_box);
                gtk_widget_show(thisWindowData[KuBandDrawingArea].handle_box2);
                thisWindowData[KuBandLeftDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S1KU_KuBandLeftDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S2KU_KuBandLeftDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[KuBandBottomDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S1KU_KuBandBottomDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[S2KU_KuBandBottomDrawingArea].coordinate_type = KUBAND2;

                // thisWindowData[KuBandDrawingArea].mask_data->redraw = True;
                MF_ForceRedraw (REALTIME, KUBAND_VIEW);
                MF_ForceRedraw (REALTIME, S1KU_KUBAND_VIEW);
                MF_ForceRedraw (REALTIME, S2KU_KUBAND_VIEW);
                RTD_SetRedraw(True);
                RTD_DrawData();
            }

            // Update Blockage menus
            D_UpdateBlockageItems(thisUImanager, REALTIME, thisCoordinateType, thisKubandDisplayed);
	
	    // Set the menu item 
            action = gtk_ui_manager_get_action(thisUImanager, kuband_to_display == 1 ?
                                 "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_KUBAND_VIEW"/"CHANGE_KUBAND_VIEW_TO_ONE_MENU :
                                 "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_KUBAND_VIEW"/"CHANGE_KUBAND_VIEW_TO_TWO_MENU);

            if (action)
            {
                in_kuband_displayed = False;
                /* activate the action */
                gtk_action_activate(action);

                in_kuband_displayed = True;
            }
        }
}

/**
 * Determines if the view orientation needs to be changed.
 *
 * @param view_orientation either horizontal or vertical
 */
static void set_view_orientation(gint view_orientation)
{
    gint width, height;
    GtkAction *action;
    static gboolean in_view_orientation = True;

    if (in_view_orientation == True)
    {
        /* set the button label for the orientation */
        GtkWidget *widget = gtk_bin_get_child(GTK_BIN(thisViewOrientationButton));
        gtk_label_set_text(GTK_LABEL(widget), (view_orientation == View_Orientation_Horizontal ?
                                               VIEW_H_to_V : VIEW_V_to_H));

        /* get the action for the menu item */
        /* when activating the action, this will cause the radio button to change */
        action = gtk_ui_manager_get_action(thisUImanager,
                                           view_orientation == View_Orientation_Horizontal ?
                                           "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_VIEW_ORIENTATION"/"CHANGE_VIEW_ORIENTATION_TO_HORIZONTAL_MENU :
                                           "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_VIEW_ORIENTATION"/"CHANGE_VIEW_ORIENTATION_TO_VERTICAL_MENU);
        if (action)
        {
            in_view_orientation = False;

            /* activate the action */
            gtk_action_activate(action);

            in_view_orientation = True;
        }

        /* now do the orientation */
        if (view_orientation == View_Orientation_Horizontal)
        {
            if (thisViewOrientation != View_Orientation_Horizontal)
            {
                thisViewOrientation = View_Orientation_Horizontal;
                if (thisWindow->window != NULL)
                {
                    gdk_window_get_size(thisWindow->window, &width, &height);
                    if (height > width)
                    {
                        gtk_window_resize(GTK_WINDOW(thisWindow), height, width);
                    }

                    change_orientation(&thisSBand1KuBandContainer, View_Orientation_Horizontal);
                    change_orientation(&thisSBand2KuBandContainer, View_Orientation_Horizontal);
                    RTD_SetRedraw(True);
                    RTD_DrawData();
                }
            }
        }

        else if (view_orientation == View_Orientation_Vertical)
        {
            if (thisViewOrientation != View_Orientation_Vertical)
            {
                thisViewOrientation = View_Orientation_Vertical;
                if (thisWindow->window != NULL)
                {
                    gdk_window_get_size(thisWindow->window, &width, &height);
                    if (width > height)
                    {
                        gtk_window_resize(GTK_WINDOW(thisWindow), height, width);
                    }

                    change_orientation(&thisSBand1KuBandContainer, View_Orientation_Vertical);
                    change_orientation(&thisSBand2KuBandContainer, View_Orientation_Vertical);
                    RTD_SetRedraw(True);
                    RTD_DrawData();
                }
            }
        }

        else
        {
            /* do nothing */
        }
    }
}

/**
 * Changes the orientation of the multi-view. When the orientation is
 * horizontal, the sband and kuband are layed out side-by-side. When the
 * orientation is vertical, the sband and kuband are layed out on top
 * of each other.
 *
 * @param container the container that has the sband and kuband
 * components
 * @param orientation either View_Orientation_Horizontal or View_Orientation_Vertical
 */
static void change_orientation(GtkWidget **container, gint orientation)
{
    GList *tmp;
    GList *list = NULL;
    GtkWidget *sband = NULL;
    GtkWidget *kuband = NULL;
    GtkWidget *box = NULL;
    GtkWidget *tab_label;
    gint page_num;
    gint current_page_num;

    /* make sure the container is valid */
    if (*container != NULL)
    {
        /* find the sband and kuband components */
        /* sband is a frame */
        /* kuband is a box */
        list = gtk_container_children(GTK_CONTAINER(*container));
        if (list != NULL)
        {
            for (tmp = list; tmp != NULL; tmp = tmp->next)
            {
                GtkWidget *w = (GtkWidget *)tmp->data;
                const gchar *w_name = gtk_widget_get_name(w);

                if (g_ascii_strncasecmp(SBAND_TITLE,  w_name,  (gsize)strlen(SBAND_TITLE)) == 0)
                {
                    sband = (GtkWidget *)tmp->data;
                }
                else if (g_ascii_strcasecmp(KUBAND_TITLE,  w_name) == 0)
                {
                    kuband = (GtkWidget *)tmp->data;
                }

                if (sband != NULL && kuband != NULL)
                {
                    break;
                }
            }
            g_list_free(list);
        }

        /* create container for requested orientation */
        switch (orientation)
        {
            case View_Orientation_Horizontal:
                box = gtk_hbox_new(True, 1);
                break;

            case View_Orientation_Vertical:
                box = gtk_vbox_new(True, 1);
                break;

            default:

                break;
        }

        /* if we have everything, lets reparent the */
        /* sband and kuband components to the new box */
        if (box != NULL && sband != NULL && kuband != NULL)
        {
            /* need to replace container in notebook ? */
            page_num = gtk_notebook_page_num(GTK_NOTEBOOK(thisNotebook), *container);
            if (page_num != -1)
            {
                /* when the sband and kuband is removed from the */
                /* previous container, those objects will be destroyed */
                /* so we increase the ref_count to keep them around */
                g_object_ref(G_OBJECT(sband));
                g_object_ref(G_OBJECT(kuband));

                /* remove sband and kuband from previous container */
                gtk_container_remove(GTK_CONTAINER(*container), sband);
                gtk_container_remove(GTK_CONTAINER(*container), kuband);

                /* add sband and kuband to new container */
                gtk_box_pack_start (GTK_BOX (box), sband, True, True, 0);
                gtk_box_pack_start (GTK_BOX (box), kuband, True, True, 0);
                gtk_widget_show_all(box);

                /* get the current page number being viewed */
                current_page_num = gtk_notebook_get_current_page(GTK_NOTEBOOK(thisNotebook));

                /* get label */
                tab_label = gtk_notebook_get_tab_label(GTK_NOTEBOOK(thisNotebook), *container);
                if (G_OBJECT(tab_label)->ref_count == 1)
                {
                    g_object_ref(G_OBJECT(tab_label));
                }

                /* remove old page, add new page, select the page */
                gtk_notebook_remove_page(GTK_NOTEBOOK(thisNotebook), page_num);
                gtk_notebook_insert_page(GTK_NOTEBOOK(thisNotebook), box, tab_label, page_num);
                gtk_notebook_set_current_page(GTK_NOTEBOOK(thisNotebook), current_page_num);

                /* save instance pointer */
                *container = box;
            }
        }
    }
}

/**
 * Processes the sband1 callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void sband1_blockage_clicked_cb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, SBAND1_MENU, name);
}

/**
 * Processes the sband2 callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void sband2_blockage_clicked_cb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, SBAND2_MENU, name);
}

/**
 * Processes the kuband callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void kuband_blockage_clicked_cb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, KUBAND_MENU, name);
}

/**
 * Processes the kuband callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void kuband2_blockage_clicked_cb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, KUBAND2_MENU, name);
}

/**
 * Processes the station callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void station_blockage_clicked_cb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, STATION_MENU, name);
}

/**
 * Processes the sband1 blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void sband1_blockage_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    set_blockage_mask_by_name(SBAND1, g_path_get_basename(action_name));
}

/**
 * Processes the sband2 blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void sband2_blockage_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    set_blockage_mask_by_name(SBAND2, g_path_get_basename(action_name));
}

/**
 * Processes the kuband blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void kuband_blockage_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    set_blockage_mask_by_name(KUBAND, g_path_get_basename(action_name));
}

/**
 * Processes the kuband blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void kuband2_blockage_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    set_blockage_mask_by_name(KUBAND2, g_path_get_basename(action_name));
}

/**
 * Processes the station blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void station_blockage_action_cb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    set_blockage_mask_by_name(STATION, g_path_get_basename(action_name));
}

/**
 * Gets the blockage mask for the specified coordinate and mask name
 *
 * @param coord the coordinate
 * @param mask_name the name of the mask
 */
static void set_blockage_mask_by_name(gint coord, const gchar *mask_name)
{
    guint i;
    g_message("SET BLOCKAGE MASK: %s",mask_name);
    for (i=0; i<MF_GetMaskFileCount(REALTIME, coord); i++)
    {
        gchar *blockage_mask = MF_GetMaskName(REALTIME, coord, i);
        if (g_ascii_strcasecmp(mask_name, blockage_mask) == 0)
        {
            /* set the blockage mask itself */
            set_blockage_mask(coord, (gint)i);

            /* set the toolbar buttons visual */
            set_button_mask_visual(coord, i, mask_name);
            break;
        }
    }

    /* now force a redraw */
    RTD_SetRedraw(True);
    RTD_DrawData();
}

/**
 * Sets the blockage mask for the given mask number.
 *
 * @param coord current coordinate type
 * @param mask_num mask number
 */
static void set_blockage_mask(gint coord, gint mask_num)
{
    gboolean visible = MF_GetVisibility(REALTIME, coord, mask_num);

    if (MF_IsLimitMask(REALTIME, coord, mask_num) == True)
    {
        g_message("SETTING LIMIT MASK");
        MF_SetVisibility(REALTIME, SBAND1, MF_GetLimitMaskNum(REALTIME, SBAND1),   (visible ? False : True));
        MF_SetVisibility(REALTIME, SBAND2, MF_GetLimitMaskNum(REALTIME, SBAND2),   (visible ? False : True));
        MF_SetVisibility(REALTIME, KUBAND, MF_GetLimitMaskNum(REALTIME, KUBAND),   (visible ? False : True));
        MF_SetVisibility(REALTIME, KUBAND2, MF_GetLimitMaskNum(REALTIME, KUBAND2),   (visible ? False : True));
        MF_SetVisibility(REALTIME, STATION, MF_GetLimitMaskNum(REALTIME, STATION), (visible ? False : True));
    }
    else
    {
        MF_SetVisibility(REALTIME, coord, mask_num, (visible ? False : True));
    }
}

/**
 * Sets the masks for the given coordinate. The config_keyword defines
 * the list of masks to show or not to show. The stored string represents
 * a bitwise value where '0' indicates no show while a '1' indicates
 * show the mask.
 *
 * @param coord which coordinate
 * @param config_keyword lookup this value, the expected value is
 * a bitwise representation of the masks to enable
 */
static void set_masks_enabled(gint coord, gchar *config_keyword)
{
    guint i;
    gchar mask_value[32] = {0};

    if (CP_GetConfigData(config_keyword, mask_value) == False)
    {
        /* couldn't find. */
        /* ignore and just let the default behavior happen */
        g_message("%s not defined in the config file!", config_keyword);
    }
    else
    {
        /* search thru the masks */
        for (i=0; i<MF_GetMaskFileCount(REALTIME, coord); i++)
        {
            /* should the mask be enabled */
            if (mask_value[i] == '1')
            {
                /* ignore the limit mask */
                if (MF_IsLimitMask(REALTIME, coord, i) == False)
                {
                    gchar *mask_name = MF_GetMaskName(REALTIME, coord, i);
                    gchar *menu = (coord == SBAND1 ? SBAND1_MENU :
		    		   coord == KUBAND ? KUBAND_MENU :
		    		   coord == KUBAND2 ? KUBAND2_MENU :
                                   coord == SBAND2 ? SBAND2_MENU : STATION_MENU);

                    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR,
                                              BLOCKAGE_MENU, menu,
                                              mask_name);
                }
            }
        }
    }
}

/**
 * Sets the toolbar button mask visual.
 *
 * @param coord the coordinate type
 * @param mask_num the mask number
 * @param mask_name the mask name
 */
static void set_button_mask_visual(gint coord, guint mask_num, const gchar *mask_name)
{
    gboolean enable = MF_GetVisibility(REALTIME, coord, (gint)mask_num);
    switch (coord)
    {
        case SBAND1:
        case SBAND1_KUBAND:
            D_SetButtonMaskVisual(&thisWindowData[SBand1DrawingArea], enable, mask_name, coord);
            D_SetButtonMaskVisual(&thisWindowData[S1KU_SBandDrawingArea], enable, mask_name, coord);
            break;

        case SBAND2:
        case SBAND2_KUBAND:
            D_SetButtonMaskVisual(&thisWindowData[SBand2DrawingArea], enable, mask_name, coord);
            D_SetButtonMaskVisual(&thisWindowData[S2KU_SBandDrawingArea], enable, mask_name, coord);
            break;

        case KUBAND:
        case KUBAND2:
            D_SetButtonMaskVisual(&thisWindowData[KuBandDrawingArea], enable, mask_name, coord);
            break;

        case STATION:
            D_SetButtonMaskVisual(&thisWindowData[StationDrawingArea], enable, mask_name, coord);
            break;

        default:
            break;
    }
}

/**
 * Resize callback for the whole window. This callback is used to determine if the
 * font needs to change to accommodate the new window size.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False
 */
static gint resize_window_cb(GtkWidget *widget, GdkEventConfigure *event, void *user_data)
{
    static gint font_size = -1;
    guint i;
    ResizeFont *resize_font = (ResizeFont *)user_data;

    if (thisIsIconified == False)
    {
        /* we're resizing the window */
        thisIsResizing = True;

        /* resize the font ? */
        resize_font->new_width = event->width;
        resize_font->new_height = event->height;
        resize_font->previous_font_size = D_ResizeFont(gtk_widget_get_name(thisWindow),
                                                       resize_font);
        resize_font->previous_width = event->width;
        resize_font->previous_height = event->height;

        /* create a pango font description for this new font size */
        if (resize_font->previous_font_size > 0 &&
            (resize_font->previous_font_size != font_size))
        {
            font_size = resize_font->previous_font_size;
            set_pango_font_desc(resize_font, widget->style->font_desc);
        }

        /* tell all windata pixmaps that the window has been resized */
        /* this will force a complete redraw in the expose event */
        for (i=0; i<MAX_DRAWING_AREAS; i++)
        {
            thisWindowData[i].is_resized = True;
        }
    }

    /* let the system event handler execute */
    return False;
}

/**
 * Resize callback for each drawing area defined. This callback is used to determine
 * if the offscreen pixmaps need to be re-allocated for the size change.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
static gboolean resize_drawing_area_cb(GtkWidget *widget, GdkEventConfigure *event, void * user_data)
{
    /* window is resizing */
    WindowData *windata = RTD_GetWindowDataByWidget(widget);
    if (windata != NULL)
    {
        /* create the pixmap data view for this drawing area */
        EventSizeData event_data = { event->x, event->y, event->width, event->height};
        make_pixmap_data(windata, &event_data, *(gint *)user_data);
    }

    /* let the system event handler execute */
    return False;
}

/**
 * Expose event for the drawing areas.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event.
 * False to propagate the event further.
 */
static gboolean expose_cb(GtkWidget *widget, GdkEventExpose *event, void * data)
{
    WindowData *windata;
    thisReady = True;

    windata = RTD_GetWindowDataByWidget(widget);
    if (windata != NULL)
    {
        gint i, n;

        /* do we need to redraw the whole pixmap? */
        /* with the notebook, after the window has been render, resize events */
        /* are no longer sent, except to the currently visible page */
        if (windata->is_resized == True)
        {
            /* reset the width, height, this will force a reallocation */
            /* of the drawing area */
            windata->width = 0;
            windata->height = 0;

            /* save the event state information */
            EventSizeData event_data = { event->area.x, event->area.y, event->area.width, event->area.height};

            /* go make the pixmap data */
            make_pixmap_data(windata, &event_data, *(gint *)data);

            /* no longer resizing */
            windata->is_resized = False;
        }
        else
        {
            GdkGC *gc;
            GdkRectangle *rect;
            GdkGCValues _values;

            _values.function = GDK_COPY;
            gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

            /* just redraw the exposed region, this is more efficient than */
            /* redrawing the whole pixmap everytime */
            gdk_region_get_rectangles(event->region, &rect, &n);
            for (i=0; i<n; i++)
            {
                /* copy the bg pixmap to the tracks pixmap */
                gdk_draw_drawable(AM_Drawable(windata, DST_DRAWABLE),
                                  gc,
                                  AM_Drawable(windata, SRC_DRAWABLE),
                                  rect[i].x, rect[i].y,  /* dst x, y */
                                  rect[i].x, rect[i].y,  /* src x, y */
                                  rect[i].width, rect[i].height /* src width, height, use ALL */ );
            }
            g_free(rect);
            g_object_unref(gc);
        }
    }

    /* no longer resizing */
    thisIsResizing = False;

    /* continue the event handler */
    return False;
}


/**
 * Determines which coordinate view is currently being shown; calls the
 * appropriate draw method to get the main pixmap drawn. This pixmap will
 * later be used for redrawing in the expose event.
 *
 * @param windata the window data that has the drawing area
 * @param event_data the event data that has sizing information
 * @param which_drawing_area the drawing area to draw to
 */
static void make_pixmap_data(WindowData *windata, EventSizeData *event_data, gint which_drawing_area)
{
    /* allocate new pixmap, resize occurred, after this, the expose event will be called */
    /* need to reallocate pixmaps for all of the different views */
    D_CreatePixmap(windata);

    // ready to draw
    RTD_SetRedraw(True);

    windata->x = event_data->x;
    windata->y = event_data->y;
    windata->width = event_data->width;
    windata->height = event_data->height;

    switch (thisCoordinateType)
    {
        case SBAND1:
        case SBAND2:
            RTD_DrawData();
            break;

        case KUBAND:
        case KUBAND2:
            {
                switch (which_drawing_area)
                {
                    default:
                    case KuBandDrawingArea:
                        RTD_DrawData();
                        break;

                    case KuBandLeftDrawingArea:
                    case KuBandBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(windata, REALTIME, which_drawing_area);
                        break;
                }
            }
            break;

        case STATION:
            {
                switch (which_drawing_area)
                {
                    default:
                    case StationDrawingArea:
                        RTD_DrawData();
                        break;

                    case StationLeftDrawingArea:
                    case StationBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(windata, REALTIME, which_drawing_area);
                        break;
                }
            }
            break;

        case SBAND1_KUBAND:
            {
                set_sku_coordinate_type(which_drawing_area);
                switch (which_drawing_area)
                {
                    case S1KU_SBandDrawingArea:
                        draw_data_for_view(S1KU_SBAND_VIEW);
                        break;

                    case S1KU_KuBandDrawingArea:
                        draw_data_for_view(S1KU_KUBAND_VIEW);
                        break;

                    case S1KU_KuBandLeftDrawingArea:
                    case S1KU_KuBandBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(windata, REALTIME, which_drawing_area);
                        break;

                    default:
                        break;
                }
            }
            break;

        case SBAND2_KUBAND:
            {
                set_sku_coordinate_type(which_drawing_area);
                switch (which_drawing_area)
                {
                    case S2KU_SBandDrawingArea:
                        draw_data_for_view(S2KU_SBAND_VIEW);
                        break;

                    case S2KU_KuBandDrawingArea:
                        draw_data_for_view(S2KU_KUBAND_VIEW);
                        break;

                    case S2KU_KuBandLeftDrawingArea:
                    case S2KU_KuBandBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(windata, REALTIME, which_drawing_area);
                        break;

                    default:
                        break;
                }
            }
            break;

        default:
            break;
    }
}

/**
 * Draw the coordinate points. If the window data structure is null, then
 * used the cached values; this call is probably coming from the main
 * draw routine - this will restore the text on the screen. The main
 * draw routine will clear the main drawing area before drawing again.
 *
 * @param wdata window data
 * @param str the message string to draw
 * @param x x location to start draw
 * @param y y location to start draw
 */
void RTD_DrawCoordPoints(WindowData *wdata, gchar *str, gint x, gint y)
{
    GdkGC *gc;
    PangoFontDescription *pango_font_desc;
    PangoLayout *layout;

    /* save values for redraw */
    if (wdata != NULL)
    {
        thisTrackingData.windata = wdata;
        g_stpcpy(thisTrackingData.message, str);

        thisTrackingData.x_loc = x;
        thisTrackingData.y_loc = y;
    }

    /* only draw if window data is valid */
    if (thisTrackingData.windata != NULL)
    {
        pango_font_desc = pango_font_description_copy(thisPangoFontDescription);
        layout = gtk_widget_create_pango_layout(thisTrackingData.windata->drawing_area, "");
        pango_layout_set_text(layout, thisTrackingData.message, -1);
        pango_layout_set_font_description(layout, pango_font_desc);
        pango_font_description_free(pango_font_desc);

        gc = CH_SetColorGC(thisTrackingData.windata->drawing_area, CH_GetColorName(GC_WHITE));

        gdk_draw_layout(AM_Drawable(thisTrackingData.windata, DST_DRAWABLE), gc,
                        thisTrackingData.x_loc, thisTrackingData.y_loc, layout);

        g_object_unref(gc);
        g_object_unref(layout);
    }
}

/**
 * Handles the active display. When coming here this dialog
 * is considered active.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False continues processing
 */
static gboolean focus_in_event_cb(GtkWidget *widget, GdkEventFocus *event, void * user_data)
{
    AM_SetActiveDisplay(DISPLAY_RD, True);
    return(False);
}

/**
 * Handles the active display. When coming here this dialog
 * is considered no longer active.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False continues processing
 */
static gboolean focus_out_event_cb(GtkWidget *widget, GdkEventFocus *event, void * user_data)
{
    //AM_SetActiveDisplay(DISPLAY_RD, False);
    return(False);
}

/**
 * When the user enters the drawing area, the coordinate type is
 * set based on the value passed into the user data arg.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
static gboolean enter_notify_cb(GtkWidget *widget, GdkEventCrossing *event, void * data)
{
    thisCoordinateView = GPOINTER_TO_INT(data);
    return(True);
}

/**
 * When the user leaves the drawing area, the drawing area needs to be redraw
 * to clear the old points data
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
static gboolean leave_notify_cb(GtkWidget *widget, GdkEventCrossing *event, void * data)
{
    if (thisCoordinateTracking == True)
    {
        /* leaving the window, clear the tracking data, then draw */
        /* this should clear the window we're leaving */
        thisTrackingData.windata = NULL;

        RTD_DrawData();
    }
    return(True);
}

/**
 * Handles the notebook page switch
 *
 * @param widget calling widget
 * @param page current page
 * @param page_num the page number we're switchint to
 * @param user_data user data
 */
static void page_switch_cb(GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data)
{
        /**
         * Set the coordinate type and sku coordinate type
         * The expose event will determine which sku type
         */

        switch (page_num)
        {
            case 0:
                thisCoordinateType = SBAND1;
                break;

            case 1:
                thisCoordinateType = SBAND2;
                break;

            case 2:
                if (thisKubandDisplayed == 1)
                    thisCoordinateType = KUBAND;
                else
                    thisCoordinateType = KUBAND2;
                break;

            case 3:
                thisCoordinateType = STATION;
                break;

            case 4:
                thisCoordinateType = SBAND1_KUBAND;
                break;

            case 5:
                thisCoordinateType = SBAND2_KUBAND;
                break;

            default:
                thisCoordinateType = UNKNOWN_COORD;
                break;
        }

        thisSKUCoordinateType = UNKNOWN_COORD;
        thisCoordinateView    = UNKNOWN_VIEW;

        /* Set the view we're on */
        set_coordinate_view();

        /* update the menu items */
        D_UpdateBlockageItems(thisUImanager, REALTIME, thisCoordinateType, thisKubandDisplayed);

        /* change title */
        RTD_SetDialogTitle();

        /* now draw the view */
        RTD_DrawData();
}

/**
 * This method processes the time button selection
 *
 * @param widget calling widget
 * @param data user data
 */
static void time_cb(GtkWidget *widget, void * data)
{
    RTD_UpdateTimeType(True);
    PD_UpdateTimeType(False);
    ED_UpdateTimeType(False);
}

/**
 * This method performs the refresh action
 *
 * @param widget calling widget
 * @param data user data
 */
static void refresh_cb(GtkWidget *widget, void * data)
{
    MF_LoadMaskData();
    IH_LoadTempLimits();
    RTD_SetRedraw(True);
    RTD_DrawData();
}

/**
 * This method performs the clear action
 *
 * @param widget calling widget
 * @param data user data
 */
static void clear_cb(GtkWidget *widget, void * data)
{
    RTDH_ClearAll();
    RTD_SetRedraw(True);
    RTD_DrawData();
}

/**
 * This method handles the iconification of the app
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
static gboolean visibility_cb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    GdkEventWindowState *windowStateEvent = (GdkEventWindowState *)event;

    if (windowStateEvent->new_window_state&GDK_WINDOW_STATE_ICONIFIED)
    {
        thisIsIconified = True;
    }
    else
    {
        thisIsIconified = False;
    }
#ifndef _EXT_PARTNERS
    GTKsetWindowIconified(thisWindow, thisIsIconified);
#endif

    return(False);
}

/**
 * Handles window close event
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False to propagate the event further.
 */
static gboolean close_cb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    D_CloseDialog(REALTIME);
    return(True);
}

/**
 * Returns the window information, drawing area and pixmap, based on the current
 * coordinate type. If the drawing area is null, then the primary window is returned.
 *
 * @param widget compare with this widget
 *
 * @return SBAND1, SBAND2, KUBAND, or STATION window data
 */
WindowData *RTD_GetWindowDataByWidget(GtkWidget *widget)
{
    WindowData *windata = NULL;
    switch (thisCoordinateType)
    {
        case SBAND1:
            windata = &thisWindowData[SBand1DrawingArea];
            break;

        case SBAND2:
            windata = &thisWindowData[SBand2DrawingArea];
            break;

        case KUBAND:
        case KUBAND2:
            {
                if (widget == NULL)
                {
                    windata = &thisWindowData[KuBandDrawingArea];
                }
                else if (widget == thisWindowData[KuBandLeftDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[KuBandLeftDrawingArea];
                }
                else if (widget == thisWindowData[KuBandBottomDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[KuBandBottomDrawingArea];
                }
                else
                {
                    windata = &thisWindowData[KuBandDrawingArea];
                }
            }
            break;

        case STATION:
            {
                if (widget == NULL)
                {
                    windata = &thisWindowData[StationDrawingArea];
                }
                else if (widget == thisWindowData[StationLeftDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[StationLeftDrawingArea];
                }
                else if (widget == thisWindowData[StationBottomDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[StationBottomDrawingArea];
                }
                else
                {
                    windata = &thisWindowData[StationDrawingArea];
                }
            }
            break;

        case SBAND1_KUBAND:
            {
                if (widget != NULL)
                {
                    if (widget == thisWindowData[S1KU_SBandDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S1KU_SBandDrawingArea];
                    }
                    else if (widget == thisWindowData[S1KU_KuBandDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S1KU_KuBandDrawingArea];
                    }
                    else if (widget == thisWindowData[S1KU_KuBandLeftDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S1KU_KuBandLeftDrawingArea];
                    }
                    else if (widget == thisWindowData[S1KU_KuBandBottomDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S1KU_KuBandBottomDrawingArea];
                    }
                    else
                    {
                        windata = NULL;
                    }
                }
                else
                {
                    if (thisSKUCoordinateType == SBAND1)
                    {
                        windata = &thisWindowData[S1KU_SBandDrawingArea];
                    }
                    else if (thisSKUCoordinateType == KUBAND)
                    {
                        windata = &thisWindowData[S1KU_KuBandDrawingArea];
                    }
                    else
                    {
                        windata = NULL;
                    }
                }
            }
            break;

        case SBAND2_KUBAND:
            {
                if (widget != NULL)
                {
                    if (widget == thisWindowData[S2KU_SBandDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S2KU_SBandDrawingArea];
                    }
                    else if (widget == thisWindowData[S2KU_KuBandDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S2KU_KuBandDrawingArea];
                    }
                    else if (widget == thisWindowData[S2KU_KuBandLeftDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S2KU_KuBandLeftDrawingArea];
                    }
                    else if (widget == thisWindowData[S2KU_KuBandBottomDrawingArea].drawing_area)
                    {
                        windata = &thisWindowData[S2KU_KuBandBottomDrawingArea];
                    }
                    else
                    {
                        windata = NULL;
                    }
                }
                else
                {
                    if (thisSKUCoordinateType == SBAND2)
                    {
                        windata = &thisWindowData[S2KU_SBandDrawingArea];
                    }
                    else if (thisSKUCoordinateType == KUBAND)
                    {
                        windata = &thisWindowData[S2KU_KuBandDrawingArea];
                    }
                    else
                    {
                        windata = NULL;
                    }
                }
            }
            break;

        default:
            break;
    }

    return(windata);
}

/**
 * Returns the drawing area index associated with the coordinate.
 *
 * @param coord current coordinate type
 *
 * @return drawing area index or -1
 */
static gint get_index_by_coord(gint coord)
{
    gint index = -1;

    switch (coord)
    {
        case SBAND1:
            index = SBand1DrawingArea;
            break;

        case SBAND2:
            index = SBand2DrawingArea;
            break;

        case KUBAND:
            index = KuBandDrawingArea;
            break;

        case KUBAND2:
            index = KuBandDrawingArea;
            break;

        case STATION:
            index = StationDrawingArea;
            break;

        default:
            index = -1;
            break;
    }

    return(index);
}

/**
 * Returns the window information, drawing area and pixmap, based on the
 * coordinate type provided.
 *
 * @param coord coordinate type
 * @param sku_coord - used only when coord = SBAND1_KUBAND or SBAND2_KUBAND
 *
 * @return SBAND1, SBAND2, KUBAND, or STATION window data
 */
static WindowData *get_window_data_by_coord(gint coord, gint sku_coord)
{
    WindowData *windata = NULL;
    gint index = get_index_by_coord(coord);

    if (index != -1)
    {
        windata = &thisWindowData[index];
    }
    else
    {
        if (coord == SBAND1_KUBAND)
        {
            switch (sku_coord)
            {
                default:
                case SBAND1:
                    windata = &thisWindowData[S1KU_SBandDrawingArea];
                    break;

                case KUBAND:
                case KUBAND2:
                    windata = &thisWindowData[S1KU_KuBandDrawingArea];
                    break;
            }
        }
        else if (coord == SBAND2_KUBAND)
        {
            switch (sku_coord)
            {
                default:
                case SBAND2:
                    windata = &thisWindowData[S2KU_SBandDrawingArea];
                    break;

                case KUBAND:
                case KUBAND2:
                    windata = &thisWindowData[S2KU_KuBandDrawingArea];
                    break;
            }
        }
    }

    return(windata);
}

/**
 * Returns the coordinate type based on the coordinate view.
 *
 * @param coord_view coordinate view
 *
 * @return coordinate type or -1
 */
gint RTD_GetCoordTypeFromView(gint coord_view)
{
    gint coord = -1;

    switch (coord_view)
    {
        case SBAND1_VIEW:
        case S1KU_SBAND_VIEW:
            coord = SBAND1;
            break;

        case SBAND2_VIEW:
        case S2KU_SBAND_VIEW:
            coord = SBAND2;
            break;

        case KUBAND_VIEW:
        case S1KU_KUBAND_VIEW:
        case S2KU_KUBAND_VIEW:
            if (thisKubandDisplayed == 1)
                coord = KUBAND;
            else coord = KUBAND2;
            break;

        case STATION_VIEW:
            coord = STATION;
            break;

        default:
            break;
    }

    return(coord);
}

/**
 * Returns the window data based on the coordinate view we're on
 *
 * @param coord_view coordinate view
 *
 * @return windowdata or NULL
 */
static WindowData *get_window_data_by_view(gint coord_view)
{
    WindowData *windata = NULL;

    switch (coord_view)
    {
        case SBAND1_VIEW:
            windata = &thisWindowData[SBand1DrawingArea];
            break;

        case SBAND2_VIEW:
            windata = &thisWindowData[SBand2DrawingArea];
            break;

        case KUBAND_VIEW:
            windata = &thisWindowData[KuBandDrawingArea];
            break;

        case STATION_VIEW:
            windata = &thisWindowData[StationDrawingArea];
            break;

        case S1KU_SBAND_VIEW:
            windata = &thisWindowData[S1KU_SBandDrawingArea];
            break;

        case S1KU_KUBAND_VIEW:
            windata = &thisWindowData[S1KU_KuBandDrawingArea];
            break;

        case S2KU_SBAND_VIEW:
            windata = &thisWindowData[S2KU_SBandDrawingArea];
            break;

        case S2KU_KUBAND_VIEW:
            windata = &thisWindowData[S2KU_KuBandDrawingArea];
            break;

        default:
            break;
    }

    return(windata);
}

/**
 * This function updates the reference time type displayed between UTC and PET.
 *
 * @param update_time_type indicates whether or not to update the time type flag
 * or just use the current value.
 */
void RTD_UpdateTimeType(gboolean update_time_type)
{
    /* get current time type in effect */
    gint old_time_type = AM_GetTimeType();
    gint new_time_type = TS_UNKNOWN;

    /* update the time type ? */
    if (update_time_type == True)
    {
        new_time_type = AM_UpdateTimeType();
    }
    else
    {
        new_time_type = old_time_type;
    }

    /* make sure the button is valid */
    if (thisTimeButton != NULL)
    {
        gtk_label_set_text(GTK_LABEL(GTK_BIN(thisTimeButton)->child), GetTimeName(new_time_type));
        set_time_value(get_value_label(IS_Gmt), IS_GetSymbol(IS_Gmt), False);
    }
}

/**
 * Sets all the pixmap redraw flags. The assumption is if
 * a redraw is required for one view, then all views require
 * a redraw.
 *
 * @param flag redraw flag, either True or False
 */
void RTD_SetRedraw(gboolean flag)
{
    guint i;
    for (i=0; i<MAX_DRAWING_AREAS; i++)
    {
        thisWindowData[i].redraw_pixmap = True;
    }
}

/**
 * Returns the value (data) label associated with which and index
 *
 * @param symbol_id symbol index
 *
 * @return label or NULL
 */
static GtkWidget *get_value_label(gint symbol_id)
{
    GtkWidget *label = NULL;

    if (symbol_id == IS_Gmt)
    {
        label = thisTimeLabel;
    }
    else
    {
        label = thisXmitValue;
    }

    return(label);
}

/**
 * Returns the status label associated with which and index
 *
 * @param symbol_id symbol index
 *
 * @return label or NULL
 */
static GtkWidget *get_status_label(gint symbol_id)
{
    GtkWidget *label = NULL;

    if (symbol_id == IS_Gmt)
    {
        label = thisTimeStatus;
    }
    else
    {
        label = thisXmitStatus;
    }

    return(label);
}

/**
 * Determines which coordinate type to use when in multi view and sets it.
 *
 * @param which_drawing_area which drawing are to use
 */
static void set_sku_coordinate_type(gint which_drawing_area)
{
    thisSKUCoordinateType = UNKNOWN_COORD;

    switch (which_drawing_area)
    {
        case SBand1DrawingArea:
        case S1KU_SBandDrawingArea:
            thisSKUCoordinateType = SBAND1;
            break;

        case SBand2DrawingArea:
        case S2KU_SBandDrawingArea:
            thisSKUCoordinateType = SBAND2;
            break;

        case KuBandDrawingArea:
        case KuBandLeftDrawingArea:
        case KuBandBottomDrawingArea:
        case S1KU_KuBandDrawingArea:
        case S1KU_KuBandLeftDrawingArea:
        case S1KU_KuBandBottomDrawingArea:
        case S2KU_KuBandDrawingArea:
        case S2KU_KuBandLeftDrawingArea:
        case S2KU_KuBandBottomDrawingArea:
            if (thisKubandDisplayed == 1)
                thisSKUCoordinateType = KUBAND;
            else thisSKUCoordinateType = KUBAND2;
            break;

        case StationDrawingArea:
        case StationLeftDrawingArea:
        case StationBottomDrawingArea:
            thisSKUCoordinateType = STATION;
            break;

        default:
            break;
    }
}

/**
 * Sets the coordinate view, this is based on the current coordinate type
 */
static void set_coordinate_view(void)
{
    switch (thisCoordinateType)
    {
        case SBAND1:
            thisCoordinateView = SBAND1_VIEW;
            break;

        case SBAND2:
            thisCoordinateView = SBAND2_VIEW;
            break;

        case KUBAND:
            thisCoordinateView = KUBAND_VIEW;
            break;

        case KUBAND2:
            thisCoordinateView = KUBAND_VIEW;
            break;

        case STATION:
            thisCoordinateView = STATION_VIEW;
            break;

        case SBAND1_KUBAND:
            if (thisSKUCoordinateType == SBAND1)
            {
                thisCoordinateView = S1KU_SBAND_VIEW;
            }
            else
            {
                thisCoordinateView = S1KU_KUBAND_VIEW;
            }
            break;

        case SBAND2_KUBAND:
            if (thisSKUCoordinateType == SBAND2)
            {
                thisCoordinateView = S2KU_SBAND_VIEW;
            }
            else
            {
                thisCoordinateView = S2KU_KUBAND_VIEW;
            }
            break;

        default:
            break;
    }
}

/**
 * This function draws the tracks for the realtime display screen.
 *
 * @param windata window data
 * @param coord_view coordinate view
 *
 * @return True if the tracks are done updating, otherwise False
 */
static void draw_rt_tracks(WindowData *windata, gint coord_view)
{
    /* get the background pixmap drawable */
    GdkDrawable *drawable = AM_Drawable(windata, BG_DRAWABLE);

    gint coord = windata->coordinate_type;
    if (coord != -1)
    {
        GdkGC *gc;
        GdkGCValues _values;

        /* see if new pixmap needs to be allocated, size change? */
        /* we're going to use this to store the image before drawing */
        AM_AllocatePixmapData(windata, windata->track_data, False);

        /* get a gc for copy */
        _values.function = GDK_COPY;
        gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

        /* copy the bg pixmap to the tracks pixmap */
        gdk_draw_drawable(windata->track_data->pixmap,
                          gc,
                          drawable,
                          0, 0,  /* dst x, y */
                          0, 0,  /* src x, y */
                          -1, -1 /* src width, height, use ALL */ );

        /* now draw into the tracks pixmap */
        drawable = GDK_DRAWABLE(windata->track_data->pixmap);

        /* draw TDRS tracks */
        draw_rt_track(windata, coord, TD_TDRW, drawable);
        draw_rt_track(windata, coord, TD_TDRE, drawable);

        /* draw Sun track */
        if (thisShowSolarTrace)
        {
            draw_rt_track(windata, coord, TD_SUN, drawable);
        }

        /* draw predicted tracks */
        if (thisShowPredicts)
        {
            draw_rt_predicts(windata, coord, drawable);
        }

        /* draw the cross-hair/boresight */
        draw_ant_point(windata, coord, drawable);

        /* copy the bg and tracks to the src pixmap */
        gdk_draw_drawable(AM_Drawable(windata, SRC_DRAWABLE),
                          gc,
                          windata->track_data->pixmap,
                          0, 0,  /* dst x, y */
                          0, 0,  /* src x, y */
                          -1, -1 /* src width, height, use ALL */ );

        /* all done with the gc */
        g_object_unref(gc);
    }
}

/**
 * This function draws the tracks for the realtime display screen.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param tdrs tdrs value
 * @param drawable draw into this drawable
 */
static void draw_rt_track(WindowData *windata, gint coord, gint tdrs, GdkDrawable *drawable)
{
    RtTrack *track = RTDH_GetTrack(tdrs);
    if (track != NULL &&
        (track->pts != NULL) &&
        ((track->head != track->tail) ||
         (track->pts[track->head].timeStr[0] != '\0')))
    {
        draw_rt_track_line(windata, coord, tdrs, track, drawable);
        draw_rt_track_text(windata, coord, tdrs, track, drawable);
    }
}

/**
 * Draws the tracking line for the given tdrs
 *
 * @param windata window data
 * @param coord coordinate type
 * @param tdrs tdrs type
 * @param track realtime track data
 * @param drawable draw into this drawable
 */
static void draw_rt_track_line(WindowData *windata, gint coord, gint tdrs, RtTrack *track, GdkDrawable *drawable)
{
    gint i;
    GdkGC *gc;
    PTimePoint pt0 = NULL;
    PTimePoint pt1 = NULL;
    PTimePoint pt2 = NULL;
    PTimePoint pt3 = NULL;
    Point p1;
    Point p2;
    Point *p0 = NULL;
    Point *p3 = NULL;
    gint x1;
    gint y1;
    gint x2;
    gint y2;
    gint dx;
    gint dy;
    gdouble y;
    gint wrap;
    gboolean draw_the_line;

    if (track->tail != track->head)
    {
        for (i = track->tail; i != track->head; i = (i + 1) % track->size)
        {
            if (pt0 == NULL)
            {
                p0 = NULL;
            }
            else
            {
                p0 = &(pt0->pt[coord]);
            }

            pt1 = &(track->pts[i]);
            p1  = pt1->pt[coord];
            pt2 = &(track->pts[(i + 1) % track->size]);
            p2  = pt2->pt[coord];

            if (((i + 1) % track->size) == track->head)
            {
                pt3 = NULL;
                p3  = NULL;
            }
            else
            {
                pt3 = &(track->pts[(i + 2) % track->size]);
                p3  = &(pt3->pt[coord]);
            }

            wrap = PDH_CheckWrap(p0, &p1, &p2, p3, coord);
            pt0 = pt1;

            WG_PointToWindow(coord,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height,
                             p1.x, p1.y, &x1, &y1);
            WG_PointToWindow(coord,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height,
                             p2.x, p2.y, &x2, &y2);

            if (tdrs == TD_SUN)
            {
                gc = CH_SetColorGC(windata->drawing_area,
                                   CH_GetColorName(GC_YELLOW));
            }
            else
            {
                gc = CH_SetColorGC(windata->drawing_area,
                                   CH_GetColorName(RTDH_GetTrackColor(pt1->inView)));
            }

            if (wrap != 0)
            {
                if (y1 == y2)
                {
                    y = y1;
                }
                else
                {
                    if (wrap > 0)
                    {
                        y = (gdouble) p1.y + (p2.y - p1.y) *
                            (MAX_STATION_AZIMUTH*10 - p1.x) /
                            (MAX_STATION_AZIMUTH*20 + p2.x - p1.x);
                    }
                    else
                    {
                        y = (gdouble) p1.y + (p2.y - p1.y) *
                            (MAX_STATION_AZIMUTH*10 + p1.x) /
                            (MAX_STATION_AZIMUTH*20 - (p2.x + p1.x));
                    }
                }

                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 MIN_STATION_AZIMUTH*10, (gint)y, &dx, &dy);

                gdk_draw_line(drawable, gc, x1, y1, (wrap > 0) ? 0 : dx, dy);
                gdk_draw_line(drawable, gc, (wrap > 0) ? dx : 0, dy, x2, y2);
            }
            else
            {
                draw_the_line = True;
                switch (coord)
                {
                    case SBAND1:
                    case SBAND2:
                        if (p2.y > MAX_POLAR_RADIUS*10)
                        {
                            draw_the_line = False;
                        }
                        break;

                    case KUBAND:
                    case KUBAND2:
                        if ((p1.y > MAX_KU_ELEVATION*10) &&
                            ((p2.y > MAX_KU_ELEVATION*10) || (p2.y < MIN_KU_ELEVATION*10)))
                        {
                            draw_the_line = False;
                        }

                        else if ((p1.y < MIN_KU_ELEVATION*10) &&
                                 ((p2.y < MIN_KU_ELEVATION*10) || (p2.y > MAX_KU_ELEVATION*10)))
                        {
                            draw_the_line = False;
                        }
                        break;

                    default:
                        break;
                }

                if (draw_the_line)
                {
                    gdk_draw_line(drawable, gc, x1, y1, x2, y2);
                }
            }
            gdk_gc_unref(gc);
        }
    }
}

/**
 * Draws the tdrs track number; prior to drawing the number, the area is saved off
 * into a pixmap so it can be cleared before the next number is drawn.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param tdrs tdrs type
 * @param track realtime track data
 * @param drawable draw into this drawable
 */
static void draw_rt_track_text(WindowData *windata, gint coord, gint tdrs, RtTrack *track, GdkDrawable *drawable)
{
    GdkGC *gc;

    PTimePoint pt = NULL;
    Point p;
    gint x;
    gint y;
    gint width;
    gint height;
    gint font_size;
    PangoFontDescription *pango_font_desc;
    PangoLayout *layout;

    /*----------------------------------*/
    /* draw the track label at the head */
    /*----------------------------------*/
    pt = &(track->pts[track->head]);
    p = pt->pt[coord];

    if (tdrs == TD_SUN)
    {
        SD_DrawSymbolAtPoint(windata, coord, p.x, p.y, drawable);
    }
    else
    {
        /* increase the font size by 2 - want it a little bit bigger */
        font_size = (pango_font_description_get_size(thisPangoFontDescription)/PANGO_SCALE) + 2;
        pango_font_desc = pango_font_description_copy(thisPangoFontDescription);
        pango_font_description_set_size(pango_font_desc, font_size*PANGO_SCALE);

        layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
        pango_layout_set_text(layout, track->label, -1);
        pango_layout_set_font_description(layout, pango_font_desc);
        pango_font_description_free(pango_font_desc);

        gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(RTDH_GetTrackColor(pt->inView)));

        width = FH_GetFontWidth(layout);
        height = FH_GetFontHeight(layout);

        WG_PointToWindow(coord,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height,
                         p.x, p.y, &x, &y);

        x = x - (width / 2) -2;
        y = y - (height / 2);

        gdk_draw_layout(drawable, gc, x, y, layout);

        g_object_unref(gc);
        g_object_unref(layout);
    }
}

/**
 * This function draws the predict tracks for the realtime display screen.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param drawable draw into this drawable
 */
static void draw_rt_predicts(WindowData *windata, gint coord, GdkDrawable *drawable)
{
    draw_rt_predict(windata, coord, TD_TDRW, drawable);
    draw_rt_predict(windata, coord, TD_TDRE, drawable);
}

/**
 * This function draws the predict tracks for the realtime display screen.
 *
 * @param windata window data
 * @param coord coordinate type
 * @param tdrs tdrs type
 * @param drawable draw into this drawable
 */
static void draw_rt_predict(WindowData *windata, gint coord, gint tdrs, GdkDrawable *drawable)
{
    /* validate the predict set */
    PredictSet *ps = validPredicts(coord, tdrs);
    if (ps != NULL)
    {
        PredictPt *pt1, *pt2;
        gint x1, y1, x2, y2;
        gint width, height;
        glong secs = CurrentUtcSecs();
        RtTrack *track;
        PangoLayout *layout;

        /* make sure its not null */
        if (ps->ptList)
        {
            /* get the list of predict points */
            GList *ptList = ps->ptList;

            /* gets the first and second points */
            pt1 = (PredictPt *)ptList->data;
            pt2 = (PredictPt *)ptList->next->data;

            /*----------------------------------------*/
            /* Skip predict points up to current time */
            /*----------------------------------------*/
            while (pt2 != NULL && ptList != NULL)
            {
                if ((pt1->timeSecs < secs) && (secs <= pt2->timeSecs))
                {
                    break;
                }

                pt1 = pt2;

                /* move to next predict */
                ptList = g_list_next(ptList);
                if (ptList != NULL)
                {
                    pt2 = (PredictPt *)ptList->data;
                }
            }

            /*-------------------------------------------------------------*/
            /* Draw event label for predict if at least 5 minutes has past */
            /* since receiving a real-time point for the TDRS              */
            /*-------------------------------------------------------------*/
            if (pt2 != NULL)
            {
                track = RTDH_GetTrack(tdrs);
                if (track != NULL)
                {
                    if (secs - track->pts[track->head].timeSecs > 300)
                    {
                        Point *point;
                        layout = gtk_widget_create_pango_layout(windata->drawing_area,"");
                        pango_layout_set_text(layout, track->label, -1);
                        pango_layout_set_font_description(layout, thisPangoFontDescription);

                        width = FH_GetFontWidth(layout);
                        height = FH_GetFontHeight(layout);

                        point = PDH_GetCoordinatePoint(coord, pt1);
                        WG_PointToWindow(coord,
                                         windata->drawing_area->allocation.width,
                                         windata->drawing_area->allocation.height,
                                         point->x, point->y, &x1, &y1);

                        x1 = x1 - (width  / 2);
                        y1 = y1 + (height / 2);

                        /* now draw label to drawable */
                        gdk_draw_layout(drawable,
                                        windata->drawing_area->style->white_gc,
                                        x1, y1, layout);

                        g_object_unref(layout);
                    }
                }
            }

            #ifdef DEBUG
            print_ptList(coord, ptList);
            #endif

            /*--------------------------------------*/
            /* Draw predict track from current time */
            /*--------------------------------------*/
            while (pt2 != NULL && ptList != NULL)
            {
                if (! PDH_IsWrapped(coord, ptList))
                {
                    Point *point = PDH_GetCoordinatePoint(coord, pt1);
                    WG_PointToWindow(coord,
                                     windata->drawing_area->allocation.width,
                                     windata->drawing_area->allocation.height,
                                     point->x, point->y, &x1, &y1);

                    point = PDH_GetCoordinatePoint(coord, pt2);
                    WG_PointToWindow(coord,
                                     windata->drawing_area->allocation.width,
                                     windata->drawing_area->allocation.height,
                                     point->x, point->y, &x2, &y2);

                    gdk_draw_line(drawable,
                                  windata->drawing_area->style->white_gc,
                                  x1, y1, x2, y2);
                }
                pt1 = pt2;

                /* move to next predict */
                ptList = g_list_next(ptList);
                if (ptList != NULL)
                {
                    pt2 = (PredictPt *)ptList->data;
                }
            }
        }
    }
}

#ifdef DEBUG
/**
 * Used for debugging the predict data set.
 *
 * @param sets
 */
static void print_ptList(gint coord,  GList *ptList)
{
    GList *_ptList = ptList;
    g_message("PRINT PREDICT POINT LIST:: ptList=%x",(gint)ptList);
    for (; _ptList != NULL; _ptList = g_list_next(_ptList))
    {
        PredictPt *pt = (PredictPt *)_ptList->data;
        Point *raw = &pt->rawPt;
        Point *point = PDH_GetCoordinatePoint(coord, pt);

        g_message("COORD PT=%6d %6d  RAW PT=%6d %6d",point->x, point->y, raw->x, raw->y);
    }
    g_message("LEAVING PRINT PREDICT POINT LIST");
}
#endif


/**
 * Determines if the realtime predicts are valid for processing.
 *
 * @param tdrs
 *
 * @return PredictSet*  or NULL
 */
static PredictSet *validPredicts(gint coord, gint tdrs)
{
    PredictSet *ps = NULL;

    if (ValidCurrentUtc() && IS_GetSymbol(IS_Gmt))
    {
        /*---------------------------------*/
        /* Load predict track if necessary */
        /*---------------------------------*/
        RTDH_LoadRtPredicts(coord, tdrs, False);
        ps = RTDH_GetPredicts(tdrs);
        if ((ps != NULL) && ps->ptList != NULL)
        {
            /*----------------------------------------------------------*/
            /* Return immediately if not yet reach AOS of predict track */
            /*----------------------------------------------------------*/
            glong secs = CurrentUtcSecs();
            if (secs < ps->aosSecs)
            {
                ps = NULL;
            }
        }
    }

    return ps;
}
/**
 * Draw the boresight where S-Band and Ku-Band antennas are currently pointing
 *
 * @param windata window data
 * @param coord coordinate type
 * @param drawable draw into this drawable
 */
static void draw_ant_point(WindowData *windata, gint coord, GdkDrawable *drawable)
{
    GdkGC *gc      = NULL;
    PangoLayout *layout  = NULL;
    SymbolRec *symbol  = NULL;

    gint width = 0;
    gint height = 0;
    gint az = 0;
    gint el = 0;
    gint x, y, xc, yc;
    gint font_size;
    PangoFontDescription *pango_font_desc;

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            {
                /* get the actual s-band xel, el angles in degrees. */
                /* Multiply by 10 to put in tenths of degrees. */

                if (coord == SBAND1)
                {
                    symbol = IS_GetSymbol(IS_Sb1CmdAz);
                    if (symbol)
                    {
                        az = (gint) (symbol->value * 10.0);
                    }
                    symbol = IS_GetSymbol(IS_Sb1CmdEl);
                    if (symbol)
                    {
                        el = (gint) (symbol->value * 10.0);
                    }
                }
                else
                {
                    symbol = IS_GetSymbol(IS_Sb2CmdAz);
                    if (symbol)
                    {
                        az = (gint) (symbol->value * 10.0);
                    }
                    symbol = IS_GetSymbol(IS_Sb2CmdEl);
                    if (symbol)
                    {
                        el = (gint) (symbol->value * 10.0);
                    }
                }

                /* draw the cross-hair for sband */
                draw_sband_lga_cross_hair(windata, coord, drawable);

                /* increase the font size by 2 - want it a little bit bigger */
                font_size = (pango_font_description_get_size(thisPangoFontDescription)/PANGO_SCALE) + 4;
                pango_font_desc = pango_font_description_copy(thisPangoFontDescription);
                pango_font_description_set_size(pango_font_desc, font_size*PANGO_SCALE);

                layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
                pango_layout_set_text(layout, thisBoreSight, -1);
                pango_layout_set_font_description(layout, pango_font_desc);
                pango_font_description_free(pango_font_desc);

                /* get label width and height */
                width = FH_GetFontWidth(layout);
                height = FH_GetFontHeight(layout);
                gc = CH_SetColorGC(windata->drawing_area, thisBoreSightColor);
            }
            break;

        case KUBAND:
        case KUBAND2:
            {
                /* get the actual ku-band xel, el angles in degrees. */
                /* Multiply by 10 to put in tenths of degrees. */
                /* NOTE: az = XEL and el = EL for Ku-Band */

                symbol = IS_GetSymbol(IS_KuActualXEL);
                if (symbol)
                {
                    az = (gint) (symbol->value * 10.0);
                }
                symbol = IS_GetSymbol(IS_KuActualEL);
                if (symbol)
                {
                    el = (gint) (symbol->value * 10.0);
                }

                layout = get_kuband_lga_cross_hair(windata, coord, az, el);
                if (layout != NULL)
                {
                    /* get label width and height */
                    width = FH_GetFontWidth(layout);
                    height = FH_GetFontHeight(layout);
                    gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(GC_RED));
                }
                else
                {
                    /* increase the font size by 2 - want it a little bit bigger */
                    font_size = (pango_font_description_get_size(thisPangoFontDescription)/PANGO_SCALE) + 4;
                    pango_font_desc = pango_font_description_copy(thisPangoFontDescription);
                    pango_font_description_set_size(pango_font_desc, font_size*PANGO_SCALE);

                    layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
                    pango_layout_set_text(layout, thisBoreSight, -1);
                    pango_layout_set_font_description(layout, pango_font_desc);
                    pango_font_description_free(pango_font_desc);

                    /* get label width and height */
                    width = FH_GetFontWidth(layout);
                    height = FH_GetFontHeight(layout);
                    gc = CH_SetColorGC(windata->drawing_area, thisBoreSightColor);
                }
            }
            break;

        default:
            break;
    }

    if (gc != NULL)
    {
        WG_PointToWindow(coord,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height,
                         az, el, &xc, &yc);
        switch (coord)
        {
            case KUBAND:
            case KUBAND2:
                x = xc - (width / 2)  -2;
                y = yc - (height / 2);
                break;

            default:
                x = xc - (width / 2);
                y = yc - (height / 2);
                break;
        }

        /* is the boresight in view? */
        if (x > 0 && y > 0)
        {
            gdk_draw_layout(drawable, gc, x, y, layout);
        }

        gdk_gc_unref(gc);
        g_object_unref(layout);
    }
}

/**
 * Draws the cross-hair symbol for the sband display
 *
 * @param windata window data
 * @param coord coordinate type
 * @param drawable draw into this drawable
 */
static void draw_sband_lga_cross_hair(WindowData *windata, gint coord, GdkDrawable *drawable)
{
    PangoLayout *layout;

    gint width, height;
    gint az, el, x, y, xc, yc;

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            {
                layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
                pango_layout_set_text(layout, thisSBandCrossHair, -1);
                pango_layout_set_font_description(layout, thisPangoFontDescription);

                /* get current commanded s-band az, el angles in degrees */
                /* Multiply by 10 to put in tenths of degrees */
                az = (gint) (RTDH_GetLGABoresightAz() * 10.0);
                el = (gint) (RTDH_GetLGABoresightEl() * 10.0);

                /* get label width and height */
                width = FH_GetFontWidth(layout);
                height = FH_GetFontHeight(layout);

                /* compute window coordinate and color for current pointing angle */
                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 az, el, &xc, &yc);
                x = xc - (width / 2);
                y = yc - (height / 2);

                gdk_draw_layout(drawable,
                                windata->drawing_area->style->white_gc,
                                x, y, layout);

                g_object_unref(layout);
            }
            break;

        default:
            break;
    }
}

/**
 * Draws the cross-hair symbol for the ku-band display
 *
 * @param windata window data
 * @param coord coordinate type
 * @param az azimuth
 * @param el elevation
 */
static PangoLayout *get_kuband_lga_cross_hair(WindowData *windata, gint coord, gint az, gint el)
{
    PangoLayout *layout = NULL;

    if (MF_IsPointInMask(coord,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height,
                         az, el))
    {
        layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
        pango_layout_set_text(layout, thisKuBandCrossHair, -1);
        pango_layout_set_font_description(layout, thisPangoFontDescription);
        return(layout);
    }

    return(NULL);
}

/**
 * This function is the front-end to the draw function.
 *
 * @see draw_data_for_view().
 */
void RTD_DrawData(void)
{
    gint c;
    gint num_coords = 0;

    /* get the coordinate type */
    /* for multi-views set the coordinate to the view */
    /* then we'll loop through for two views */
    /* single views map to the coordinate type */
    gint coord = thisCoordinateType;

    switch (coord)
    {
      case SBAND1:
         coord = SBAND1_VIEW;
         num_coords = coord + 1;
         break;

      case SBAND2:
         coord = SBAND2_VIEW;
         num_coords = coord + 1;
         break;

      case KUBAND:
      case KUBAND2:
         coord = KUBAND_VIEW;
         num_coords = coord + 1;
         break;

      case STATION:
         coord = STATION_VIEW;
         num_coords = coord + 1;
         break;

      case SBAND1_KUBAND:
         coord = S1KU_SBAND_VIEW;
         num_coords = coord + 2;
         break;

      case SBAND2_KUBAND:
         coord = S2KU_SBAND_VIEW;
         num_coords = coord + 2;
         break;

      default:
         break;
    }

    /* go thru each coordinate view */
    for (c=coord; c<num_coords; c++)
    {
        /* draw for this view */
        draw_data_for_view(c);
    }
}

/**
 * This function displays all data in the drawing area.
 */
static void draw_data_for_view(gint coord_view)
{
    /* draw if not iconified and were ready */
    if (thisIsIconified == False && thisReady == True)
    {
//        static GStaticMutex mutex = G_STATIC_MUTEX_INIT;

        /* this prevents other threads from entering here and */
        /* mucking up something. we want the draw routine to */
        /* finish uninterrupted. */
//        g_static_mutex_lock(&mutex);
        {
            WindowData *windata;

            /* verify the window is valid */
            windata = get_window_data_by_view(coord_view);
            if (windata != NULL &&
                windata->drawing_area != NULL &&
                windata->drawing_area->window != NULL &&
                windata->src_pixmap != NULL)
            {
                JointAngle *ja = RTDH_GetJointAngle();

                /* draw into the offscreen pixmap */
                windata->which_drawable = BG_DRAWABLE;

                /* do we need to redraw something? */
                if (windata->redraw_pixmap == True)
                {
                    AM_ClearDrawable(AM_Drawable(windata, windata->which_drawable),
                                     windata->drawing_area->style->black_gc,
                                     0, 0,
                                     windata->drawing_area->allocation.width,
                                     windata->drawing_area->allocation.height);

                    /* draw dynamics */
                    DD_DrawDataForAngle(windata, REALTIME, coord_view, ja);

                    /* draw masks */
                    OM_ForceRedraw(REALTIME, coord_view);
                    OM_DrawMasks(windata, REALTIME, coord_view);

                    /* draw the static masks */
                    if (coord_view == KUBAND_VIEW ||
                        coord_view == S1KU_KUBAND_VIEW ||
                        coord_view == S2KU_KUBAND_VIEW)
                    {
                        MF_DrawKuBandMasks(windata, REALTIME, coord_view);
                    }
                    else
                    {
                        MF_DrawMasks(windata, REALTIME, coord_view);
                    }

                    /* draw the limit mask */
                    MF_DrawLimitMask(windata, REALTIME, coord_view);

                    /* draw background on top of everything */
                    PH_DrawBackground(windata, REALTIME, coord_view);
                }

                /* ***** NOTE: *************************************/
                /* separate loops are used to make multiple images */
                /* appear rendered simultaneously                  */
                /* *************************************************/
                /* process the tracking data */
                /* and render the image to the drawing area */

                /* now do all real-time drawing directly into the drawing area widget */
                /* draw tdrs tracks */
                draw_rt_tracks(windata, coord_view);

                /* render src pixmap into the dst drawable */
                D_DrawDrawable(windata);

                /* clear the pixmap redraw flag */
                windata->redraw_pixmap = False;

                /* if coordinate tracking is on, do it */
                if (thisCoordinateTracking == True)
                {
                    RTD_DrawCoordPoints(NULL, NULL, -1, -1);
                }
            }
        }
        /* release the lock */
//        g_static_mutex_unlock(&mutex);
    }
}

#ifndef _EXT_PARTNERS
/**
 * This function is set with User Experience and called for
 * "Save Session".
 */
void *UEconfigFunc(int *size)
{
   *size = (int)sizeof(thisIsIconified);
   return (void *)&thisIsIconified;
}
#endif

#ifndef _EXT_PARTNERS
/**
 * This function is set with User Experience and called for
 * "Restore Session".
 */
void UErestoreFunc(void *data, int size)
{
   gboolean *d = (gboolean *)data;

   if (size) {
      thisIsIconified = *d;
   }
}
#endif

#ifndef _EXT_PARTNERS
/* 
 * If the Predict dialog is opened first and set with User Experience,
 * it does not close the Realtime dialog when it closes.
 */
void UEsetRealtimeDialog (void)
{
    /* clear then set */
    GTKsetTopLevelWindow(NULL);
    GTKsetAppConfigFunc(NULL);
    GTKsetAppRestoreFunc(NULL);

    /* set top level window for User Experience */
    GTKsetTopLevelWindow(thisWindow);
    GTKsetWindowIconified(thisWindow, thisIsIconified);
    GTKsetAppConfigFunc(&UEconfigFunc);
    GTKsetAppRestoreFunc(&UErestoreFunc);
}

#endif
