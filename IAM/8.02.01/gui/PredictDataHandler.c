/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <expat.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
    #include <unistd.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
    #include <process.h>
    #include <io.h>

    #define F_OK 0 /* test for existence. */
    #define X_OK 1 /* test for execute permission */
    #define W_OK 2 /* test for write permission */
    #define R_OK 4 /* test for read permission */
    #define  access   _access /* map to POSIX complaint MS version */
#endif

#define PRIVATE
#include "PredictDataHandler.h"
#undef PRIVATE

#include "AntMan.h"
#include "AutoUpdateDialog.h"
#include "ColorHandler.h"
#include "ConfigParser.h"
#include "DynamicDraw.h"
#include "EventDialog.h"
#include "FTPDialog.h"
#include "gtkjfilechooser.h"
#include "ManualUpdateDialog.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PredictDialog.h"
#include "PrintHandler.h"
#include "RealtimeDialog.h"
#include "RealtimeDataHandler.h"
#include "SolarDraw.h"
#include "WindowGrid.h"
#include "XML_Parse.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PredictDataHandler.c 237 2014-12-30 17:39:36Z llopez1@ndc.nasa.gov $");


#ifdef DEBUG
static void print_data(GList *sets);
#endif

/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GList        *thisPredictSetList     = NULL;
static GList        *thisCurrentPtList      = NULL;

static gint         thisInShoColorId        = GC_GREEN;
static gint         thisOverlapColorId      = GC_CYAN;
static gint         thisOutOfShoColorId     = GC_GRAY;

static gchar        thisPtgCoverageDir  [MAXPATHLEN]    = {0};

static gchar        thisAutoStartTime   [TIME_STR_LEN]  = {UNSET_TIME};
static gchar        thisAutoStopTime    [TIME_STR_LEN]  = {UNSET_TIME};
static gint         thisFreqOffsetSecs      = -1;
static gboolean     thisSendToDropBox       = False;
static gint         thisManager             = PUBLIC;

/* holds the tdrs info */
static GPtrArray    *thisTdrsInfoArray      = NULL;

/*************************************************************************/

/**
 * Initializes the predict data handler
 */
void PDH_Constructor(void)
{
    /* get the manager mode */
    thisManager = AM_GetAppMode();

    /* set the colors of the event types */
    setColorIds();

    /* set the ptg coverage directory location */
    setCoverageDir();

    /* set the frequency offset time */
    setFreqOffset();

    /* setup the FTP access */
    setFtpAccess();

    /* load up the tdrs predict data */
    checkXMLFileStatus(NULL);

    /* start the predict file timer to start checking for new data */
    XML_StartTimer(TDRS_PREDICT, checkXMLFileStatus);
}

/**
 * This function frees all data associated with the prediction display.  This
 * function is called by the destruction callback for the drawing area.
 */
void PDH_Destructor(void)
{
    /* release all predict data */
    freePredictSetList(thisPredictSetList);

    /* release the tdrs info data */
    freeTdrsInfoArray(thisTdrsInfoArray);
}

/**
 * Releases the predict set pointers and the associated predict
 * points.
 *
 * @param psList doubly-link list that has predict sets
 */
static void freePredictSetList(GList *psList)
{
    GList *_psList;
    for (_psList = psList; _psList != NULL; _psList = g_list_next(_psList))
    {
        PredictSet *ps = (PredictSet *)_psList->data;

        /* delete the points list */
        if (ps != NULL)
        {
            freePredictPtList(ps->ptList);

            MH_Free(ps);
            ps = NULL;
        }
    }
    g_list_free(psList);
    psList = NULL;
}

/**
 * This function frees the memory allocated to a linked list of predicted
 * points.
 *
 * @param pts deletes all information pointed to by line
 */
static void freePredictPtList(GList *ptList)
{
    GList *_ptList;
    for (_ptList = ptList; _ptList != NULL; _ptList = g_list_next(_ptList))
    {
        if (_ptList->data)
        {
            MH_Free(_ptList->data);
            _ptList->data = NULL;
        }
    }
    g_list_free(ptList);
    ptList = NULL;
}

/**
 * Releases the TdrsInfo data.
 *
 * @param tdrs
 */
static void freeTdrsInfoArray(GPtrArray *tdrs)
{
    if (tdrs != NULL)
    {
        guint i;
        for (i=0; i<tdrs->len; i++)
        {
            /* get tdrs info*/
            TdrsInfo *tdrsInfo = (TdrsInfo *)g_ptr_array_index(tdrs, i);

            /* now delete the tdrs info */
            MH_Free(tdrsInfo);
        }
        g_ptr_array_free(tdrs, False);
    }
}

/**
 * Loads the color ids for the various event types.
 */
static void setColorIds(void)
{
    gchar data[256];
    gint colorId;

    /* In SHO event type */
    if (CP_GetConfigData(PREDICT_TDRS_INSHO_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisInShoColorId = colorId;
    }

    /* Not in SHO, but TDRS overlap event type */
    if (CP_GetConfigData(PREDICT_TDRS_OVERLAP_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisOverlapColorId = colorId;
    }

    /* Not in SHO, no TDRS overlap event type */
    if (CP_GetConfigData(PREDICT_TDRS_OUTSHO_COLOR, data))
    {
        CP_StringTrim(data);
        colorId = CH_GetColor(data);
        if (colorId != GC_INVALIDCOLOR)
            thisOutOfShoColorId = colorId;
    }
}

/**
 * Parses for the coverage directory where the predict files are
 * located.
 */
static void setCoverageDir(void)
{
    if (CP_GetConfigData(IAM_PREDICT_DIR, thisPtgCoverageDir) == False)
    {
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "IAM_PREDICT_DIR not defined in the config file!"
                     "Unable to process Pointing Coverage files.");
        thisPtgCoverageDir[0] = '\0';
    }
}

/**
 * Parses for the frequency offset.
 */
static void setFreqOffset(void)
{
    gchar data[256];

    if (CP_GetConfigData(IAM_AUTO_UPDATE_FREQUENCY, data) == False)
    {
        g_error("IAM_AUTO_UPDATE_FREQUENCY not defined in the config file!");
        ErrorMessage(AM_GetActiveDisplay(), "Config Error",
                     "IAM_AUTO_UPDATE_FREQUENCY not defined in the config file!\n"
                     "Using '02:00' as frequency offset value.");

        /* force the offset to 2 minutes */
        g_stpcpy(data, "02:00");
    }

    /* convert offset to seconds */
    thisFreqOffsetSecs = OffsetToSecs(data);
}

/**
 * Parses for the FTP access for this user. If this is a mission
 * activity and certified app and finally this is the manager,
 * then allow this user to ftp.
 */
static void setFtpAccess(void)
{
    gchar data[256];

    /* is the override in effect ? */
    if (CP_GetConfigData(IAM_FORCE_FTP, data) == True)
    {
        thisSendToDropBox = True;
    }
    else
    {
        /* the criteria for creating the ftp item to the menu */
        /* $ACTIVITY_TYPE = MIS */
        /* $sw_lvl = cert_apps */
        /* iam is running in manager mode */
        if (AM_GetActivityType() == ACTIVITY_TYPE_MIS)
        {
            if (AM_GetSwLvl() == SW_LVL_CERT_APPS)
            {
                if (thisManager == MANAGER)
                {
                    /* now see if the operation is set for auto or manual */
                    if (CP_GetConfigData(IAM_SEND_PREDICT_DATA, data) == False)
                    {
                        g_warning("IAM_SEND_PREDICT_DATA not defined in the config file!");
                    }
                    else
                    {
                        if (g_ascii_strcasecmp(data, "YES") == 0)
                        {
                            thisSendToDropBox = True;
                        }
                    }
                }
            }
        }
    }
}

/**
 * Timer process that checks to see if new predicts are
 * available for loading.
 *
 * @param data
 *
 * @return gboolean
 */
static gboolean checkXMLFileStatus(void *data)
{
    g_message("enter checkXMLFileStatus");

    static time_t tdrsTime = 0;
    static time_t sunabgTime = 0;
    static time_t sunposTime = 0;
    static time_t shoTime = 0;

    gchar charStartTime[TIME_STR_LEN] = {UNSET_TIME};
    gchar charStopTime[TIME_STR_LEN] = {UNSET_TIME};
    gchar timeStr[TIME_STR_LEN] = {UNSET_TIME};

    glong thisCurrentUTCSecs = 0;
    glong sec1 = 0;
    glong sec2 = 0;

    gint newPredict = 0;
    gint newSunAbg = 0;
    gint newSunPos = 0;
    gint newSho = 0;

    thisCurrentUTCSecs = CurrentUtcSecs();
    if (getAutoUpdateValue(IAM_AUTO_UPDATE_START, timeStr) == True)
    {
         sec1 = thisCurrentUTCSecs + OffsetToSecs(timeStr);
         SecsToStr(TS_UTC, sec1, charStartTime);
         XML_SetStartTime(charStartTime);
    }

    if (getAutoUpdateValue(IAM_AUTO_UPDATE_STOP, timeStr) == True)
    {
         sec2 = sec1 + OffsetToSecs(timeStr);
         SecsToStr(TS_UTC, sec2, charStopTime);
         XML_SetStopTime(charStopTime);
    }

    /* process tdrs data */
    if ((newPredict = XML_HaveNewPredict(TDRS_PREDICT_XML, &tdrsTime)))
    {
        g_message("checkXMLFileStatus: load TDRS data");

        /* free previously allocated data */
        //freePredictSetList(thisPredictSetList);
        //freeTdrsInfoArray(thisTdrsInfoArray);

        /* thread a new tdrs predicts parser */
        XML_ProcessData(TDRS_PREDICT);
    }

    /* process joint angle data */
    if ((newSunAbg = XML_HaveNewPredict(SUNABG_PREDICT_XML, &sunabgTime)))
    {
        g_message("checkXMLFileStatus: load SUNABG data");

        /* release previous memory before loading */
        DD_FreeJointAngleData(DD_GetData());

        /* load new sunabg predicts */
        /* when the thread is done, it will set the pointer array */
        XML_ProcessData(SUNABG_PREDICT);
    }

    /* process sun position data */
    if ((newSunPos = XML_HaveNewPredict(SUNPOS_PREDICT_XML, &sunposTime)))
    {
        g_message("checkXMLFileStatus: load SUNPOS data");

        // free previous memory
        SD_FreeSolarPointList(SD_GetData());

        /* load new sunpos predicts */
        /* when the thread is done, it will set the link-list pointer */
        XML_ProcessData(SUNPOS_PREDICT);
    }

    /* process sho data */
    if ((newSho = XML_HaveNewPredict(IAM_SHO_FILE, &shoTime)))
    {
        g_message("checkXMLFileStatus: load SHO data");
        XML_ProcessData(SHO_PREDICT);
    }

    /* only for MANAGER :: check to see which (if any) predicts have changed */
    /* if any predicts have changed and this is not the first time, post a */
    /* message informing the CATO manager about the new files */
    if (thisManager == MANAGER)
    {
        infoMessage0(newPredict, newSunPos, newSunAbg, newSho);

        /* need to verify that the tdrs predict data has data. */
        /* if the tdrs is outside the current time frame, then */
        /* no data will loaded for the points; we need to tell */
        /* the manager that the tdrs predict is old */
        infoMessage1();
    }

    g_message("leave checkXMLFileStatus");

    /* keep timer going */
    return True;
}

/**
 * This method outputs a single message informing the user (Manager) that
 * predict data has changed. A composite message is built indicating which
 * predict data files have been loaded.
 *
 * @param tdrsChanged
 * @param sunPosChanged
 * @param sunAbgChanged
 * @param shoChanged
 */
static void infoMessage0(gint tdrsChanged, gint sunPosChanged,
                         gint sunAbgChanged, gint shoChanged)
{
    g_message("info_message: determining if message needed");
    /* any predicts updated? if the value is positive, then a change occurred */
    if (tdrsChanged > 0 || sunPosChanged > 0 || sunAbgChanged > 0 || shoChanged > 0)
    {
        GString *changed = g_string_new("Loading new Prediction data:\n");

        if (tdrsChanged)
        {
            changed = g_string_append(changed, "\nTDRS");
        }

        if (shoChanged)
        {
            changed = g_string_append(changed, "\nSHO Schedule");
        }

        if (sunPosChanged)
        {
            changed = g_string_append(changed, "\nSolar");
        }

        if (sunAbgChanged)
        {
            changed = g_string_append(changed, "\nJoint Angle");
        }

        /* post message */
        g_message("info_message: alert user");
        InfoMessage(AM_GetActiveDisplay(), "Predict Data Alert", changed->str);
    }
    else
    {
        g_message("info_message: no message required");
    }
}

/**
 * This function determines if the TDRS predict data has any
 * points generated. If none are found a message is posted.
 * Typically if no points are found it's because the data file
 * is too old and needs to be updated. This message is usally
 * reserved for the MANAGER.
 *
 */
static void infoMessage1(void)
{
    static gboolean messagePosted = False;

    /* if the message has already been posted, then ignore */
    if (messagePosted == False)
    {
        if (XML_HasTDRSPredictPoints() == False)
        {
            messagePosted = True;
            InfoMessage(AM_GetActiveDisplay(), "Predict Data Empty",
                        "The TDRS Predict Data file has no data!\n"
                        "This usally means the data file is old and needs to be updated!");
        }
    }
}

/**
 * Posts a file selection dialog for the user to change the pointing coverage
 * files used by gen_predict. Once the choice is made, the predict directory
 * for the coverage files is changed only for this session.
 *
 * @param parent_window parent window to map the coverage dialog to
 */
void PDH_GetCoverage(GtkWindow *parent_window)
{
    GtkWidget *file_chooser;
    gchar coverage_dir[MAXPATHLEN];
    gboolean run_dialog = True;

    /* launch directory browser */
    file_chooser = gtk_jfile_chooser_new_with_all(parent_window, "Choose Coverage Directory", GTK_DIALOG_MODAL);
    gtk_jfile_chooser_set_hiding_enabled(GTK_JFILE_CHOOSER(file_chooser), True);
    gtk_jfile_chooser_set_file_selection_mode(GTK_JFILE_CHOOSER(file_chooser), GTK_DIRECTORIES_ONLY);
    gtk_jfile_chooser_set_multi_selection(GTK_JFILE_CHOOSER(file_chooser), False);

    /* get starting directory for file dialog */
    if (CP_GetConfigData(IAM_PREDICT_DIR, coverage_dir) == True)
    {
        gchar *dir_part = NULL;

        /* make sure this is a directory */
        if (g_file_test(coverage_dir,  G_FILE_TEST_IS_DIR) == False)
        {
            /* get the directory part */
            dir_part = g_path_get_dirname(coverage_dir);
        }
        else
        {
            dir_part = g_strdup(coverage_dir);
        }

        gtk_jfile_chooser_set_current_directory(GTK_JFILE_CHOOSER(file_chooser), dir_part);
        g_free(dir_part);

        /* show the dialog until user either selects valid directory or 'cancels' */
        do
        {
            /* show the dialog */
            if (gtk_dialog_run(GTK_DIALOG(file_chooser)) == GTK_RESPONSE_OK)
            {
                gchar *dirname = gtk_jfile_chooser_get_file(GTK_JFILE_CHOOSER(file_chooser));
                if (dirname != NULL)
                {
                    /* make sure this is a directory */
                    if (g_file_test(dirname,  G_FILE_TEST_IS_DIR) == False)
                    {
                        dir_part = g_path_get_dirname(dirname);
                    }
                    else
                    {
                        dir_part = g_strdup(dirname);
                    }

                    /* verify that the location contains coverage files */
                    if (hasXmlCoverageFiles(dir_part) == False)
                    {
                        gchar data[32];
                        gchar message[256];

                        run_dialog = True;

                        if (CP_GetConfigData(IAM_PREDICT_SOURCE, data) == False)
                        {
                            g_sprintf(message,
                                      "The selected directory does not contain valid data files!\n"
                                      "Verify the location is valid and try again.");
                        }
                        else
                        {
                            g_sprintf(message,
                                      "The selected directory does not contain valid %s files!\n"
                                      "Verify the location is valid and try again.", data);
                        }
                        WarningMessage(AM_GetActiveDisplay(),
                                       "Coverage Directory Not Valid",
                                       message);
                    }
                    else
                    {
                        run_dialog = False;

                        /* point to the new directory selected */
                        resetCoverage(dir_part);

                        /* update the iam.xml with the directory */
                        /* first push the new data value to the config table */
                        /* then write the config file back out to the system */
                        if (CP_UpdateConfigValue(IAM_PREDICT_DIR, dir_part) == False)
                        {
                            g_warning("Update of config variable IAM_PREDICT_DIR failed!");
                        }
                        else
                        {
                            CP_WriteConfigFile();
                        }
                    }
                    g_free(dir_part);
                }
            }
            else
            {
                run_dialog = False;
            }
        } while (run_dialog == True);
    }
    gtk_widget_destroy(file_chooser);
}

/**
 * Determines if coverage files exist in the specific folder.
 *
 * @param coverageDir check this directory for coverage files
 *
 * @return true or false
 */
static gboolean hasXmlCoverageFiles(gchar *coverageDir)
{
    gboolean hasXml = False;
    gchar data[MAXPATHLEN];

    /* get the name of the los tdrs predict data file */
    if (CP_GetConfigData(TDRS_PREDICT_XML, data) == False)
    {
        g_warning("TDRS_PREDICT_XML not defined in the config file!");
    }
    else
    {
        GError *error = NULL;

        /* get just the filename part of the tdrs predict file */
        gchar *tdrsPredict = g_path_get_basename(data);

        /* open coverage directory */
        GDir *dir = g_dir_open(coverageDir, 0, &error);
        if (dir != NULL)
        {
            const gchar *fname;

            /* get files in coverage directory */
            while ((fname = g_dir_read_name(dir)) != NULL)
            {
                /* find tdrs.predict file */
                if (g_ascii_strcasecmp(tdrsPredict, fname) == 0)
                {
                    hasXml = True;
                    break;
                }
            }
        }
        else
        {
            if (error != NULL)
            {
                g_warning(error->message);
                g_error_free(error);
            }
        }

        g_dir_close(dir);
        g_free(tdrsPredict);
    }

    return hasXml;
}

/**
 * Sets flag that forces gen predict to run. Then calls the
 * timer function.
 *
 * @param coverage_dir directory where ptg coverage files are located
 */
static void resetCoverage(gchar *coverage_dir)
{
    gchar start_time[TIME_STR_LEN] = {UNSET_TIME};
    gchar stop_time [TIME_STR_LEN] = {UNSET_TIME};

    /* stop file checking */
    XML_StopTimer(TDRS_PREDICT);

    /* get the coverage directory */
    /* if the entry has a file in it, then */
    /* strip off and use the directory part */
    if (coverage_dir != NULL)
    {
        if (g_file_test(coverage_dir, G_FILE_TEST_IS_REGULAR) == True)
        {
            gchar *dirpart = g_path_get_dirname(coverage_dir);
            g_stpcpy(thisPtgCoverageDir, dirpart);
            g_free(dirpart);
        }
        else
        {
            g_stpcpy(thisPtgCoverageDir, coverage_dir);
        }

        /* change coverage identifier for dialog title */
        g_message("resetCoverage: new coverage directory=%s",thisPtgCoverageDir);
        gchar *coverageIdentifier = getCoverageIdentifier();
        if (coverageIdentifier != NULL)
        {
            AM_SetCoverageIdentifier(coverageIdentifier);
            g_free(coverageIdentifier);

            /* set the titles */
            PD_SetDialogTitle();
            RTD_SetDialogTitle();
        }

        /* load the start and stop times for manual update */
        if (generateTime(start_time , stop_time) == True)
        {
            struct stat stat_struct;

            /* now load the times into the auto update field */
            PDH_SetAutoTimes(start_time, stop_time);

            /* get the modification time of the ptg dir */
            if (stat(thisPtgCoverageDir, &stat_struct) == 0)
            {
                g_message("resetCoverage: forcing update");
                /* load the new xml file data */
                //gigo loadTdrsDataXML();

                /* restart the timer - check for changes in the */
                /* new coverage directory */
                checkXMLFileStatus(NULL);
            }
        }
    }
}

/**
 * This method opens a MAPS COV* file in the coverage directory. The
 * coverage file is read, the start and stop and year is retrieved.
 *
 * @param start_time buffer for start time
 * @param stop_time buffer for stop time
 *
 * @return True if time generated ok, otherwise False
 */
static gboolean generateTime(gchar *start_time, gchar *stop_time)
{
    gboolean rc = False;
    GDir *dir;
    GError *error = NULL;

    gint start_year = 0;
    gint stop_year = 0;
    gchar tmp_start_time[TIME_STR_LEN] = {0};
    gchar tmp_stop_time[TIME_STR_LEN] = {0};

    /* open coverage directory */
    dir = g_dir_open(thisPtgCoverageDir, 0, &error);
    if (dir != NULL)
    {
        // read xml file
        rc = generateTimeFromXml(dir, &start_year, tmp_start_time,
                                 &stop_year, tmp_stop_time);
    }
    else
    {
        if (error != NULL)
        {
            g_warning(error->message);
            g_error_free(error);
        }
    }
    g_dir_close(dir);

    /* did we get time ? */
    if (rc == True)
    {
        g_message("start=%d %s",start_year, tmp_start_time);
        g_message("stop=%d %s",stop_year, tmp_stop_time);
        /* format and put into output buffers */
        g_sprintf(start_time, "%d %s",start_year,tmp_start_time);
        GmtToUtc(start_time);
        g_sprintf(stop_time, "%d %s",stop_year,tmp_stop_time);
        GmtToUtc(stop_time);
    }

    return rc;
}

/**
 * Generates times from an LOS data file.
 *
 * @param dir
 * @param start_year
 * @param start_time
 * @param stop_year
 * @param stop_time
 *
 * @return gboolean
 */
static gboolean generateTimeFromXml(GDir *dir, gint *startYear, gchar *startTime, gint *stopYear, gchar *stopTime)
{
    gboolean rc = False;
    gchar data[MAXPATHLEN];

    g_message("generateTimeFromXml");
    /* get the name of the los tdrs predict data file */
    if (CP_GetConfigData(TDRS_PREDICT_XML, data) == True)
    {
        const gchar *fname;

        /* get just the filename part of the tdrs predict file */
        gchar *tdrs_predict = g_path_get_basename(data);

        /* get files in coverage directory */
        while ((fname = g_dir_read_name(dir)) != NULL)
        {
            /* find tdrs.predict file */
            if (g_ascii_strcasecmp(fname, tdrs_predict) == 0)
            {
                gchar *tmpname = g_build_filename(thisPtgCoverageDir, fname, NULL);
                FILE *fp = fopen(tmpname, "r");
                if (fp != NULL)
                {
                    /* GIGO - put xml parser here */
                    fclose(fp);
                }
                g_free(tmpname);
                break;
            }
        }
        g_free(tdrs_predict);
    }
    else
    {
        g_warning("TDRS_PREDICT_XML not defined in the config file!");
        rc = False;
    }

    return rc;
}
/**
 * Finds the coverage directory name. If the basename starts with
 * GMT, then the directory above is used, otherwise the
 * current directory is used.
 *
 * @return coverage filename
 */
static gchar *getCoverageIdentifier(void)
{
    /* get the basename */
    gchar *basename = g_path_get_basename(thisPtgCoverageDir);
    gchar **token = NULL;
    gchar *coverage_identifier = NULL;

    /* does this name start with GMT */
    if (g_str_has_prefix(basename, "GMT") == True)
    {
        gchar *dirpart = g_path_get_dirname(thisPtgCoverageDir);
        gchar *tmp = g_path_get_basename(dirpart);

        /* replace underscore with blanks */
        token = g_strsplit(tmp, "_", 0);
        coverage_identifier = g_strjoinv(" ", token);

        g_free(tmp);
        g_free(dirpart);
    }
    else
    {
        gchar coverage_dir[MAXPATHLEN];

        /* no GMT prefix, is this the original dir? */
        if (CP_GetConfigData(IAM_PREDICT_DIR, coverage_dir) == True)
        {
            if (g_ascii_strncasecmp(coverage_dir,
                                    thisPtgCoverageDir,
                                    (gsize)strlen(coverage_dir))
                == 0)
            {
                coverage_identifier = g_strdup(PTG_COVERAGE_STR);
            }
        }

        /* if not set, then use basename as identifier */
        if (coverage_identifier == NULL)
        {
            token = g_strsplit(basename, "_", 0);
            coverage_identifier = g_strjoinv(" ", token);
        }


    }
    g_strfreev(token);
    g_free(basename);

    return coverage_identifier;
}

/**
 * This function checks for the special condition of end point(s) on the
 * prediction track that wrap from one end to the other, i.e. the first/last
 * n points of the track all have coordinates of (+-180, y) for any y.  If this
 * is the case, then the sign of the x-coordinate is set to the same sign as
 * the next point.  This plugs a hole in the wrap-check algorithm.  For example,
 * if the first three points  of the track were (180,74), (180,78), (-175, 81),
 * then they would be  changed to (-180,74), (-180,78), (-175,81).
 *
 * @param pt the points
 */
static void checkEndWraps(GList *ptList)
{
    GList *p1List, *p2List, *p3List;
    PredictPt *pt1, *pt2, *pt3;
    gint x;
    gint min_az = MIN_STATION_AZIMUTH*10;
    gint max_az = MAX_STATION_AZIMUTH*10;

    p1List = ptList;

    if (ptList != NULL)
    {
        pt1 = (PredictPt *)p1List->data;

        if (pt1->rawPt.x == max_az || pt1->rawPt.x == min_az)
        {
            p2List = p1List->next;

            while (p2List != NULL)
            {
                pt2 = (PredictPt *)p2List->data;
                if (pt2->rawPt.x == max_az || pt2->rawPt.x == min_az)
                {
                    p3List = p2List->next;
                    pt3 = (PredictPt *)p3List->data;

                    if ((p3List == NULL) ||
                        ((pt3->rawPt.x != max_az) && (pt3->rawPt.x != min_az)))
                    {
                        x = (p3List == NULL) ?
                            pt2->rawPt.x : ((pt3->rawPt.x < 0) ? min_az : max_az);

                        for (p3List = p1List; p3List != p2List; p3List = g_list_next(p3List))
                        {
                            pt3 = (PredictPt *)p3List->data;
                            pt3->rawPt.x = x;
                        }
                        p2List = NULL;
                    }
                    else
                    {
                        p2List = g_list_next(p2List);
                    }
                }
            }
        }
    }

    /* move to the last entry */
    p1List = g_list_last(p1List);

    /* get the last point */
    pt1 = (PredictPt *)p1List->data;
    if (pt1->rawPt.x == min_az || pt1->rawPt.x == max_az)
    {
        p2List = p1List->prev;
        while (p2List != NULL)
        {
            pt2 = (PredictPt *)p2List->data;
            if (pt2->rawPt.x == max_az || pt2->rawPt.x == min_az)
            {
                p3List = p2List->prev;
                pt3 = (p3List != NULL ? (PredictPt *)p3List->data : NULL);

                if ((p3List == NULL) ||
                    ((pt3->rawPt.x != max_az) && (pt3->rawPt.x != min_az)))
                {
                    x = (p3List == NULL) ?
                        pt2->rawPt.x : ((pt3->rawPt.x < 0) ? min_az : max_az);

                    for (p3List = p1List; p3List != p2List; p3List = g_list_previous(p3List))
                    {
                        pt3 = (PredictPt *)p3List->data;
                        pt3->rawPt.x = x;
                    }
                    p2List = NULL;
                }
                else
                {
                    p2List = g_list_previous(p2List);
                }
            }
        }
    }
}

/**
 * This function checks for points that should be interpreted as wrapping from
 * one side of the display to the other.
 *
 * @param sets predict set
 * @param line_type either DISPLAY_RD or DISPLAY_PD
 */
static void checkWraps(GList *sets, gint line_type)
{
    GList *psList = NULL;
    GList *ptList = NULL;

    for (psList = sets; psList != NULL; psList = g_list_next(psList))
    {
        PredictSet *ps = (PredictSet *)psList->data;
        if (ps != NULL)
        {
            Point *p0 = NULL;
            if (line_type&DISPLAY_RD ||
                line_type&DISPLAY_PD)
            {
                if (ps->ptList != NULL)
                {
                    checkEndWraps(ps->ptList);
                }
                ptList = ps->ptList;
            }

            for (;(ptList != NULL) && (ptList->next != NULL); ptList = g_list_next(ptList))
            {
                PredictPt *pt = (PredictPt *)ptList->data;
                PredictPt *nxtPt = (PredictPt *)ptList->next->data;

                Point *p1 = &(pt->rawPt);
                Point *p2 = &(nxtPt->rawPt);
                Point *p3 = (ptList->next->next != NULL) ? &(((PredictPt *)ptList->next->next->data)->rawPt) : NULL;
                gint wrap = PDH_CheckWrap(p0, p1, p2, p3, PD_GetCoordinateType());

                if (wrap > 0)
                {
                    pt = doWrap(ptList, 1);
                }
                else if (wrap < 0)
                {
                    pt = doWrap(ptList, -1);
                }

                p0 = &(pt->rawPt);
            }
        }
    }
}


/**
 * This function adds zero to two points between pt and pt->next to represent
 * a line wrap from side to side of the display.  A point is added if pt does
 * not fall exactly on +180 or -180, and if pt->next does not fall exactly on
 * +180 or -180.  After wraps are added, any two successive points with azimuth
 * +180 and -180 (in either order) are interpreted as a wrap point.  This means
 * simply that the line segment between the two points is never drawn.  Also,
 * the time of the two points should be the same.
 *
 * @param ptList predict point list
 * @param dir direction
 *
 * @return predict point or NULL
 */
static PredictPt *doWrap(GList *ptList, gint dir)
{
    PredictPt *retPt;
    PredictPt *pt1 = NULL;
    PredictPt *pt2 = NULL;
    PredictPt *pt = (PredictPt *)ptList->data;
    PredictPt *nxtPt = (PredictPt *)ptList->next->data;
    GList *not_used;

    gdouble r;
    gint min_az = MIN_STATION_AZIMUTH*10;
    gint max_az = MAX_STATION_AZIMUTH*10;
    glong sec1;

    if ((dir > 0) && (max_az*2 + nxtPt->rawPt.x - pt->rawPt.x != 0))
    {
        r = (gdouble) (max_az - pt->rawPt.x) /
            (max_az*2 + nxtPt->rawPt.x - pt->rawPt.x);
    }
    else if ((dir <= 0) && (max_az*2 - nxtPt->rawPt.x + pt->rawPt.x != 0))
    {
        r = (gdouble) (max_az + pt->rawPt.x) /
            (max_az*2 - nxtPt->rawPt.x + pt->rawPt.x);
    }
    else
    {
        r = 0;
    }

    sec1 = (glong)(pt->timeSecs + ((gint64)r) * (nxtPt->timeSecs - pt->timeSecs));

    if ((pt->rawPt.x < max_az) && (pt->rawPt.x > min_az))
    {
        pt1 = (PredictPt *) MH_Calloc(sizeof(PredictPt), __FILE__, __LINE__);

        SecsToStr( TS_UTC, sec1, pt1->timeStr );
        pt1->timeSecs     = sec1;
        pt1->rawPt.x = (dir > 0) ? max_az : min_az;
        pt1->rawPt.y = (gint)ROUND(pt->rawPt.y + (nxtPt->rawPt.y - pt->rawPt.y) * r);
        pt1->interpolated = False;
        pt1->shoPt        = False;
        pt1->set          = pt->set;
        pt1->dynamicBlock = PT_BlockageUnknown;
    }

    if ((nxtPt->rawPt.x < max_az) && (nxtPt->rawPt.x > min_az))
    {
        pt2 = (PredictPt *) MH_Calloc(sizeof(PredictPt), __FILE__, __LINE__);
        SecsToStr( TS_UTC, sec1, pt2->timeStr );
        pt2->timeSecs     = sec1;
        pt2->rawPt.x = (dir < 0) ? max_az : min_az;
        pt2->rawPt.y = (gint)ROUND(pt->rawPt.y + (nxtPt->rawPt.y - pt->rawPt.y) * r);
        pt2->interpolated = False;
        pt2->shoPt        = False;
        pt2->set          = pt->set;
        pt2->dynamicBlock = PT_BlockageUnknown;
    }

    if (pt1 != NULL)
    {
        /* insert after */
        not_used=g_list_insert_before(ptList, ptList->next, pt1);
        if (pt2 != NULL)
        {
            /* insert after */
            not_used=g_list_insert_before(ptList, ptList->next, pt2);
        }
    }
    else if (pt2 != NULL)
    {
        /* insert after */
        not_used=g_list_insert_before(ptList, ptList->next, pt2);
    }

    /* determine which point to return */
    if (pt2 != NULL)
    {
        retPt = pt2;
    }
    else if (pt1 != NULL)
    {
        retPt = pt1;
    }
    else
    {
        retPt = pt;
    }

    return retPt;
}

#ifdef DEBUG
/**
 * Used for debugging the predict data set.
 *
 * @param sets
 */
static void print_data(GList *sets)
{
    GList *psList;
    g_message("PRINT PREDICT SET LIST:: sets=%x",(gint)sets);
    for (psList = sets; psList != NULL; psList = g_list_next(psList))
    {
        gchar elapsed[TIME_STR_LEN];
        PredictSet *ps = (PredictSet *)psList->data;
        SecsToStr(TS_ELAPSED, (ps->losSecs - ps->aosSecs),  elapsed);
        StripTimeBlanks(elapsed);
        g_message("%-6s %02d %22s %22s %8s",ps->eventName,ps->orbit,ps->aosStr,ps->losStr,elapsed);

        if (ps->ptList != NULL)
        {
            GList *ptList;
            for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
            {
                PredictPt *pt = (PredictPt *)ptList->data;
                Point rawPt = pt->rawPt;
                g_message("%-6s %22s %d %d",ps->eventName,pt->timeStr,rawPt.x,rawPt.y);
            }
        }
    }
    g_message("LEAVING PRINT PREDICT SET");
}
#endif

/**
 * Sets the tdrsinfo array with the given data array.
 *
 * @param tdrsInfoArray
 */
void PDH_SetTdrsInfo(GPtrArray *tdrsInfoArray)
{
    thisTdrsInfoArray = tdrsInfoArray;
}

/**
 * Returns the TdrsInfo that matches the id
 *
 * @param id
 *
 * @return TdrsInfo*
 */
TdrsInfo *PDH_GetTdrsInfoById(gint id)
{
    TdrsInfo *ret = NULL;
    guint i;

    if (thisTdrsInfoArray != NULL)
    {
        for (i=0; i<thisTdrsInfoArray->len; i++)
        {
            TdrsInfo *tdrsInfo = (TdrsInfo *)g_ptr_array_index(thisTdrsInfoArray, i);
            if (tdrsInfo->id == id)
            {
                ret = tdrsInfo;
                break;
            }
        }
    }

    return ret;
}

/**
 * Returns the TdrsInfo that matches the sho name
 *
 * @param shoName key lookup value for tdrs info
 *
 * @return TdrsInfo*
 */
TdrsInfo *PDH_GetTdrsInfoByShoName(gchar *shoName)
{
    TdrsInfo *ret = NULL;
    guint i;

    if (thisTdrsInfoArray != NULL)
    {
        for (i=0; i<thisTdrsInfoArray->len; i++)
        {
            TdrsInfo *tdrsInfo = (TdrsInfo *)g_ptr_array_index(thisTdrsInfoArray, i);
            if (g_ascii_strcasecmp(tdrsInfo->shoName, shoName) == 0)
            {
                ret = tdrsInfo;
                break;
            }
        }
    }

    return ret;
}

/**
 * This function determines the next point on the current TDRS event that is
 * either an original data point, or an interpolated data point that lies
 * within a mask described by mask.  If there is no data point (i.e., it is
 * the last point in the event), then NULL is returned.
 * IMPORTANT:  although this function returns a pointer to a PredictPt, the
 * calling code DOES NOT have control of the returned structure.  It may look
 * at it for data, but it should NEVER delete it or otherwise modify it.
 *
 * @param currentPt predict point
 * @param stepType by minute or by point
 *
 * @return point if next point is found;  0 if NOT found
 */
static GList *nextPoint(GList *currentPt, gint stepType)
{
    GList *pt2;
    gchar time[TIME_STR_LEN];
    glong sec;

    if (currentPt != NULL)
    {
        gint timeType = AM_GetTimeType();

        pt2 = NULL;
        if (stepType == StepByMinute)
        {
            PredictPt *pt = (PredictPt *)currentPt->data;
            if (pt != NULL)
            {

                /* convert time to total seconds */
                if (timeType == TS_PET)
                {
                    g_stpcpy(time, pt->timeStr);
                    UtcToPet(time);
                    sec = StrToSecs( TS_PET, time );
                }
                else
                {
                    sec = StrToSecs( TS_UTC, pt->timeStr );
                }

                /* move time to the next minute */
                sec = (sec - (sec % 60)) + 60;

                /* convert total seconds to UTC */
                if (timeType == TS_PET)
                {
                    SecsToStr( TS_PET, sec, time );
                    PetToUtc(time);
                }
                else
                {
                    SecsToStr( TS_UTC, sec, time );
                }

                /* interpolate a new point */
                pt2 = PDH_InterpolatePoint(pt->set, time);
            }
        }

        if (pt2 != NULL)
        {
            currentPt = pt2;
        }
        else
        {
            PredictPt *pt = (PredictPt *)currentPt->data;
            if (pt != NULL)
            {
                for (currentPt = g_list_next(currentPt);
                    currentPt != NULL && pt->interpolated;
                    currentPt = g_list_next(currentPt))
                {
                    pt = (PredictPt *)currentPt->data;
                    if (pt == NULL)
                    {
                        break;
                    }
                }
            }
        }
    }

    return currentPt;
}


/**
 * This function determines the last point on the current TDRS event that is
 * either an original data point, or an interpolated data point that lies
 * within a mask described by mask.  If there is no data point (i.e., it is
 * the first point in the event), then NULL is returned.
 * IMPORTANT:  although this function returns a pointer to a PredictPt, the
 * calling code DOES NOT have control of the returned structure.  It may look
 * at it for data, but it should NEVER delete it or otherwise modify it.
 *
 * @param pt predict porint
 * @param stepType by minute or by point
 *
 * @return point if previous point is found;  0 if NOT found
 */
static GList *previousPoint(GList *currentPtList, gint stepType)
{
    gchar time[TIME_STR_LEN];
    GList *ptList2 = NULL;

    glong sec;
    gint timeType = AM_GetTimeType();

    if (currentPtList != NULL)
    {
        PredictPt *cpt = (PredictPt *)currentPtList->data;

        if (stepType == StepByMinute)
        {
            /* convert time to total seconds */
            if (timeType == TS_PET)
            {
                g_stpcpy(time, cpt->timeStr);
                UtcToPet(time);
                sec = StrToSecs( TS_PET, time );
            }
            else
            {
                sec = StrToSecs( TS_UTC, cpt->timeStr );
            }

            /* move time to the previous minute */
            sec = sec - ( ((sec % 60) == 0) ? 60 : (sec % 60) );

            /* convert total seconds to UTC */
            if (timeType == TS_PET)
            {
                SecsToStr( TS_PET, sec, time );
                PetToUtc(time);
            }
            else
            {
                SecsToStr( TS_UTC, sec, time );
            }

            /* interpolate a new point */
            ptList2 = PDH_InterpolatePoint(cpt->set, time);
        }

        if (ptList2 != NULL)
        {
            currentPtList = ptList2;
        }
        else
        {
            for (currentPtList = g_list_previous(currentPtList);
                currentPtList != NULL;
                currentPtList = g_list_previous(currentPtList))
            {
                PredictPt *pt = (PredictPt *)currentPtList->data;
                if (! pt->interpolated)
                {
                    break;
                }
            }
        }
    }

    return currentPtList;
}

/**
 * Determines if the currently selected item in the predict
 * list has something showing in the event list. The direction
 * value indicates whether to check previous or next
 *
 * @param direction either StepNext or StepPrev
 *
 * @return True if current item has something either
 * previous or next on the list showing, otherwise False.
 */
gboolean PDH_HasEvent(gint direction)
{
    gboolean listed = False;

    GList *psList;
    PredictSet *ps = NULL;
    GList *track = NULL;

    /* traverse the list, find the first item being displayed */
    for (psList = PDH_GetData(); psList != NULL; psList = psList->next)
    {
        ps = (PredictSet *)psList->data;
        if (ps->displayed & DISPLAY_PD)
        {
            break;
        }
    }

    if (psList)
    {
        if (direction == StepPrev)
        {
            /* make sure that previous has something valid */
            for (track = psList->prev; track != NULL; track = g_list_previous(track))
            {
                ps = (PredictSet *)track->data;
                if (ps != NULL)
                {
                    if (ps->listed == True)
                    {
                        listed = True;
                        break;
                    }
                }
            }
        }
        else
        {
            /* make sure that next has something valid */
            for (track = psList->next; track != NULL; track = g_list_next(track))
            {
                ps = (PredictSet *)track->data;
                if (ps != NULL)
                {
                    if (ps->listed == True)
                    {
                        listed = True;
                        break;
                    }
                }
            }
        }
    }

    return listed;
}

/**
 * Determines if the current point has a valid point.
 * The direction indicates which direction to check the
 * link list of points. If the current point must be
 * valid.
 *
 * @param direction StepNext or StepPrev
 *
 * @return True if point exists, otherwise False
 */
gboolean PDH_HasPoint(gint direction)
{
    gboolean enabled = False;

    if (thisCurrentPtList != NULL)
    {
        if (direction == StepNext)
        {
            /* does next point exist */
            enabled = (g_list_next(thisCurrentPtList) != NULL);

        }
        else
        {
            /* does previous point exist */
            enabled = (g_list_previous(thisCurrentPtList) != NULL);
        }
    }

    return enabled;
}

/**
 * This function returns a pointer to the currently selected prediction event
 * point.
 *
 * @return current predict point list
 */
GList *PDH_GetCurrentPt(void)
{
    return thisCurrentPtList;
}

/**
 * This method saves the auto start and stop times for generating predict data
 *
 * @param startTime start time
 * @param stopTime stop time
 */
void PDH_SetAutoTimes(gchar *startTime, gchar *stopTime)
{
    g_stpcpy(thisAutoStartTime, startTime);
    g_stpcpy(thisAutoStopTime, stopTime);
}

/**
 * This method saves the frequency offset used for generating auto predict data
 *
 * @param freqOffsetSecs frequency offset in seconds
 */
void PDH_SetFreqOffsetSecs(gint freqOffsetSecs)
{
    thisFreqOffsetSecs = freqOffsetSecs;
}

/**
 * This function checks to see if two points should be represented as wrapping
 * from one side of the display to the other.  There is no perfect
 * wrap-detection algorithm, but the following seems to work fairly well.
 *
 * @param p0 first point
 * @param p1 second point
 * @param p2 third point
 * @param p3 fourth point
 * @param coord coordinate type
 *
 * @return 0 no wrap, 1 wrap, -1 data invalid
 */
gint PDH_CheckWrap(Point *p0, Point *p1, Point *p2, Point *p3, gint coord)
{
    gint rc = 0;
    gint d1, d2, d3;

    if ((p1 != NULL) && (p2 != NULL))
    {
        if (coord == STATION)
        {
            d2 = p2->x - p1->x;
            d1 = (p0 == NULL) ? 0 - d2 : p1->x - p0->x;
            d3 = (p3 == NULL) ? 0 - d2 : p3->x - p2->x;

            rc = (d2 <= -2700 ? 1 : d2 >= 2700 ? -1 :
                  (d1 >= 0 && d2 <= -1350 && d3 >= 0) ? 1 :
                  (d1 <= 0 && d2 >= 1350 && d3 <= 0) ? -1 : 0);
        }
    }

    return rc;
}

/**
 * This function returns the 1 if the point is wrapping or 0 otherwise.
 *
 * @param coord coordinate type
 * @param ptList predict point
 *
 * @return True or False
 */
gboolean PDH_IsWrapped(gint coord, GList *ptList)
{
    gboolean isWrap = False;

    if (ptList != NULL && ptList->next != NULL)
    {
        PredictPt *pt = (PredictPt *)ptList->data;
        PredictPt *nxtPt = (PredictPt *)ptList->next->data;

        switch (coord)
        {
            case STATION:
                if ((pt->genericPt.x == MAX_STATION_AZIMUTH*10) &&
                    (nxtPt->genericPt.x == MIN_STATION_AZIMUTH*10))
                {
                    isWrap = True;
                }

                if ((pt->genericPt.x == MIN_STATION_AZIMUTH*10) &&
                    (nxtPt->genericPt.x == MAX_STATION_AZIMUTH*10))
                {
                    isWrap = True;
                }
                break;

            case KUBAND:
                if ((pt->kubandPt.y > MAX_KU_ELEVATION*10) &&
                    ((nxtPt->kubandPt.y > MAX_KU_ELEVATION*10) ||
                     (nxtPt->kubandPt.y < MIN_KU_ELEVATION*10) ))
                {
                    isWrap = True;
                }

                if ((pt->kubandPt.y < MIN_KU_ELEVATION*10) &&
                    ((nxtPt->kubandPt.y < MIN_KU_ELEVATION*10) ||
                     (nxtPt->kubandPt.y > MAX_KU_ELEVATION*10) ))
                {
                    isWrap = True;
                }
                break;

            case KUBAND2:
                if ((pt->kubandPt2.y > MAX_KU_ELEVATION*10) &&
                    ((nxtPt->kubandPt2.y > MAX_KU_ELEVATION*10) ||
                     (nxtPt->kubandPt2.y < MIN_KU_ELEVATION*10) ))
                {
                    isWrap = True;
                }

                if ((pt->kubandPt2.y < MIN_KU_ELEVATION*10) &&
                    ((nxtPt->kubandPt2.y < MIN_KU_ELEVATION*10) ||
                     (nxtPt->kubandPt2.y > MAX_KU_ELEVATION*10) ))
                {
                    isWrap = True;
                }
                break;

            case SBAND1:
            case SBAND2:
                /* do nothing */
                break;
        }
    }

    return isWrap;
}

/**
 * Returns the color associated with the predict point.
 *
 * @param ptList predict point
 *
 * @return color of predict
 */
gint PDH_GetPointColor(GList *ptList)
{
    gint predictColor = thisOutOfShoColorId;

    /*------------------------------*/
    /* determine which color to use */
    /*------------------------------*/
    if (ptList != NULL)
    {
        PredictPt *pt = (PredictPt *)ptList->data;
        if (pt->set != NULL)
        {
            PredictSet *ps = (PredictSet *)pt->set->data;
            if ((g_ascii_strcasecmp(pt->timeStr, ps->shoStart) >= 0) &&
                (g_ascii_strcasecmp(pt->timeStr, ps->shoEnd) <= 0))
            {
                predictColor = thisInShoColorId;
            }
            else
            {
                if (pt->set->prev != NULL)
                {
                    ps = (PredictSet *)pt->set->prev->data;
                    if (pt->timeSecs <= ps->losSecs)
                    {
                        predictColor = thisOverlapColorId;
                    }
                }

                if (pt->set->next != NULL)
                {
                    ps = (PredictSet *)pt->set->next->data;
                    if (pt->timeSecs >= ps->aosSecs)
                    {
                        predictColor = thisOverlapColorId;
                    }
                }
            }
        }
    }

    return predictColor;
}

/**
 * This function determines the position on the current TDRS event that matches
 * the given time, and returns a pointer to a new prediction point for the given
 * position.  If there is no data point (i.e., the time is outside of the range
 * for the event), then NULL is returned.
 * IMPORTANT:  although this function returns a pointer to a PredictPt, the
 * calling code DOES NOT have control of the returned structure.  It may look at
 * it for data, but it should NEVER delete it or otherwise modify it.
 *
 * @param sets predict set
 * @param time_str when predict occurs
 *
 * @return   GList * - interpolated point list
 */
GList *PDH_InterpolatePoint(GList *sets, gchar *time_str )
{
    GList *retList = NULL;

    if (sets != NULL)
    {
        Point point;
        PredictSet *ps = (PredictSet *)sets->data;
        if (ps != NULL)
        {
            GList *ptList = interpolateOnEvent(ps->ptList, time_str, &point);
            if (ptList != NULL)
            {
                glong secs;
                PredictPt *pt = (PredictPt *)ptList->data;

                if (ps->interpolatedPt != NULL)
                {
                    /* free the link list */
                    freePredictPtList(ps->interpolatedPt);
                }

                secs = StrToSecs(TS_UTC, time_str);
                if (pt->timeSecs == secs)
                {
                    retList = ptList;
                }
                else
                {
                    /* allocate and load */
                    PredictPt *interpolatedPt = (PredictPt *) MH_Calloc(sizeof(PredictPt), __FILE__, __LINE__);

                    g_stpcpy( interpolatedPt->timeStr, time_str );
                    interpolatedPt->timeSecs      = secs;
                    setCoordinatePoint(PD_GetCoordinateType(), interpolatedPt, &point);
                    interpolatedPt->set           = sets;
                    interpolatedPt->interpolated  = True;
                    interpolatedPt->masks         = 0;
                    interpolatedPt->shoPt         = False;
                    interpolatedPt->dynamicBlock  = PT_BlockageUnknown;

                    MF_SetInterpolatedPointMask(interpolatedPt, 0);

                    /* now add to the list */
                    ps->interpolatedPt = g_list_append(ps->interpolatedPt, interpolatedPt);

                    /* return it */
                    retList = ps->interpolatedPt;
                }
            }
        }
    }

    return retList;
}

/**
 * Based on the coordinate type, this method sets the predict
 * point (x,y) fields with the point values given.
 *
 * @param coord the coordinate type
 * @param ppt the predict point set
 * @param point the point values
 */
static void setCoordinatePoint(gint coord, PredictPt *ppt, Point *point)
{
    switch (PD_GetCoordinateType())
    {
        case SBAND1:
        case SBAND2:
            ppt->sbandPt.x = point->x;
            ppt->sbandPt.y = point->y;
            break;

        case KUBAND:
            ppt->kubandPt.x = point->x;
            ppt->kubandPt.y = point->y;
            break;

        case KUBAND2:
            ppt->kubandPt2.x = point->x;
            ppt->kubandPt2.y = point->y;
            break;

        case STATION:
            ppt->genericPt.x = point->x;
            ppt->genericPt.y = point->y;
            break;

        default:
            ppt->rawPt.x = point->x;
            ppt->rawPt.y = point->y;
            break;
    }
}

/**
 * This function determines the position on the current TDRS event that matches
 * the given time, and returns a pointer to a new prediction point for the given
 * position.  If no point is found for the event, a NULL pointer is returned.
 *
 * @param pts the predict point
 * @param time when predict occurs
 * @param point the x,y location of predict
 *
 * @return PredictPt * - interpolated point
 */
static GList *interpolateOnEvent(GList *pts, gchar *time, Point *point)
{
    GList *retList = NULL;

    if (pts != NULL)
    {
        gboolean done = False;
        gint coord = PD_GetCoordinateType();
        glong secs = StrToSecs( TS_UTC, time );
        GList *ptList = pts;

        while (retList == NULL && ptList != NULL && !done)
        {
            glong s1, s2;
            PredictPt *pt = (PredictPt *)ptList->data;

            s1 = pt->timeSecs;

            /* if point matches specified time */
            if (secs == s1)
            {
                /* return this point */
                point = PDH_GetCoordinatePoint(coord, pt);
                retList = ptList;
                done = True;
            }
            /* if point goes beyond specified time */
            else if (secs > s1)
            {
                /* return no point found */
                retList = NULL;
                done = True;
            }
            else
            {
                /* if this is the last point */
                if (g_list_next(ptList) == NULL)
                {
                    /* return no point found */
                    retList = NULL;
                    done = True;
                }
                else
                {
                    ptList = g_list_next(ptList);
                    s2 = ((PredictPt *)ptList->data)->timeSecs;

                    /* if point is prior to specified time */
                    if (secs < s2)
                    {
                        /* skip to next point */
                        ptList = g_list_next(ptList);
                    }
                    /* if next point matches specified time */
                    else if (secs == s2)
                    {
                        /* return next point */
                        ptList = g_list_next(ptList);
                        point = PDH_GetCoordinatePoint(coord, (PredictPt *)ptList->data);
                        retList = ptList;
                        done = True;
                    }
                    else
                    {
                        /* otherwise, compute point at specified time */
                        Point *cur_pt = PDH_GetCoordinatePoint(coord, (PredictPt *)ptList->data);
                        Point *nxt_pt = PDH_GetCoordinatePoint(coord, (PredictPt *)ptList->next->data);

                        gdouble time_ratio = (gdouble)((gint64)(secs-s1)) / (gdouble)((gint64)(s2-s1));
                        point->x = cur_pt->x + (nxt_pt->x - cur_pt->x) * (gint)time_ratio;
                        point->y = cur_pt->y + (nxt_pt->y - cur_pt->y) * (gint)time_ratio;
                        retList = ptList;
                        done = True;
                    }
                }
            }
        }
    }

    return retList;
}

/**
 * This function figures the closest position on any current TDRS event that is
 * either an original data point, or an interpolated data point that lies
 * within  a mask described by mask and that is nearest to the position (x,y).
 *
 * @param windata the window data
 * @param sets predict set
 * @param click x,y coordinate of mouse click
 *
 * @return predict point list or NULL
 */
GList *PDH_NearestPoint(WindowData *windata, GList *sets, Point click )
{
    GList *psList = sets;
    GList *ret = NULL;
    gint coord = PD_GetCoordinateType();
    gdouble d = -1;

    for (; psList != NULL; psList = g_list_next(psList))
    {
        PredictSet *ps = (PredictSet *)psList->data;
        if (ps->displayed & DISPLAY_PD)
        {
            GList *ptList;
            for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
            {
                gdouble d2;
                gint x, y;

                PredictPt *pt = (PredictPt *)ptList->data;
                Point *point = PDH_GetCoordinatePoint(coord, pt);

                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 point->x, point->y, &x, &y);
                d2 = (gdouble) ((x - click.x) * (x - click.x)) +
                     (gdouble) ((y - click.y) * (y - click.y));
                if ((ret == NULL) || (d2 < d))
                {
                    d = d2;
                    ret = ptList;
                }
            }
        }
    }

    return ret;
}

/**
 * This function returns the set of all predict event tracks.
 *
 * @return doubly-link list of predict event data
 */
GList *PDH_GetData(void)
{
    return thisPredictSetList;
}

/**
 * Sets the predict data buffer with the given data.
 *
 * @param predict_set
 */
void PDH_SetData(GList *psList)
{
    /* reset the realtime-data, prevents accessing invalid data */
    RTDH_ResetPredicts();

    /* now release all the data */
    /* any access to this data will generate a bad error - core */
    freePredictSetList(thisPredictSetList);

    /* now point to the new list */
    thisPredictSetList = psList;
    if (thisPredictSetList != NULL)
    {
        /* load the sho data */
        XML_ProcessData(SHO_PREDICT);

        if (PD_GetCoordinateType() == STATION)
        {
            checkWraps(thisPredictSetList, DISPLAY_PD);
        }

        //GIGO - not seeing any!!! removeRedundantPoints(thisPredictSetList);
        //GIGO - not sure if needed!!! checkShoBoundaries(thisPredictSetList);

        /* process the predicts, find events to show */
        if (setEventsToShow() == False)
        {
            /* no events found, check LOS */
            setEventToShowByLos();
        }
    }

    /* now load the data, if the predict set list is empty, this
       will clear the predict window and reset everything */
    PDH_SelectPoint();
    PD_UpdateEventArrows();
    PD_DrawData(True);

    /* update the event list view if currently showing */
    ED_UpdateList();
}

/**
 * Traverses the predict set and looks for a predict that is
 * close to the LOS time.
 */
static void setEventToShowByLos(void )
{
    GList *psList;
    PredictSet *ps = NULL;

    for (psList = thisPredictSetList; psList != NULL; psList = g_list_next(psList))
    {
        ps = (PredictSet *)psList->data;

        if ((ps->losSecs >= CurrentUtcSecs()) &&
            (g_ascii_strcasecmp(ps->shoStart,UNSET_TIME) != 0))
        {
            break;
        }

        if (psList->next == NULL)
        {
            ps = (PredictSet *)thisPredictSetList->data;
            break;
        }
    }

    if (ps != NULL)
    {
        ps->locksel = True;
        ps->displayed |= DISPLAY_PD;
    }
}

/**
 * Determines if this predict is valid or not.
 *
 * @param ps
 * @param ps2
 *
 * @return gboolean
 */
static gboolean showEvent(PredictSet *ps, PredictSet *ps2)
{
    return
    ((g_ascii_strcasecmp(ps2->eventName, ps->eventName) == 0) &&
     (ps2->displayed & ((DISPLAY_PD) + (DISPLAY_ED))) &&
     (ps2->orbit == ps->orbit) &&
     (g_ascii_strcasecmp(ps2->aosStr, ps->aosStr) == 0));
}

/**
 * Determines if the SHO is valid or not.
 *
 * @param ps
 * @param ps2
 *
 * @return gboolean
 */
static gboolean setSHO(PredictSet *ps, PredictSet *ps2)
{
    gboolean sho = False;

    if ((g_ascii_strcasecmp(ps2->eventName, ps->eventName) == 0) &&
        (ps2->orbit == ps->orbit) &&
        (ps2->trackNumber == ps->trackNumber) &&
        (ps2->ShoEventNumber == ps->ShoEventNumber))
    {
        if ((g_ascii_strcasecmp(ps2->shoStart,ps->shoStart) != 0) ||
            (g_ascii_strcasecmp(ps2->shoEnd,ps->shoEnd) != 0))
        {
            sho = True;
        }
    }

    return sho;
}

/**
 * Finds an event to show that matches the current time. If the
 * predict data is old, we'll not find the data.
 *
 *
 * @return gboolean True if event was found
 */
static gboolean setEventsToShow(void)
{
    gboolean eventFound = False;
    GList *oldData = thisPredictSetList;
    GList *psList;
    GList *psList2;

    /* loop thru the list */
    for (psList = thisPredictSetList; psList != NULL; psList = g_list_next(psList))
    {
        /* get the event data */
        PredictSet *ps = (PredictSet *)psList->data;

        /* initially not displayed */
        ps->displayed = 0;

        /* process on second list */
        for (psList2 = oldData; psList2 != NULL; psList2 = g_list_next(psList2))
        {
            PredictSet *ps2 = (PredictSet *)psList2->data;

            /* show this event? */
            if (showEvent(ps, ps2) == True)
            {
                eventFound = True;

                ps->locksel = True;
                ps->displayed = ((DISPLAY_PD) + (DISPLAY_ED));
            }

            /* sho valid? */
            ps->ShoChanged = setSHO(ps, ps2);
        }
    }

    return eventFound;
}

/**
 * Sets the location of the point on the current line.
 * Currently only Home and End is supported. When GDK_Home
 * is specified, the current point is moved to the head. When
 * GDK_End is specified, the current point is moved to the
 * last point.
 *
 * @param location GDK_Home or GDK_End
 */
void PDH_SetPoint(gint location)
{
    GList *new_pt = thisCurrentPtList;
    switch (location)
    {
        case GDK_Home:

            /* find the head */
            for (; new_pt != NULL; new_pt = g_list_previous(new_pt))
            {
                /* we want to break before getting to the head */
                if (g_list_previous(new_pt) == NULL)
                {
                    break;
                }
            }
            break;

        case GDK_End:

            /* find the tail */
            for (; new_pt != NULL; new_pt = g_list_next(new_pt))
            {
                /* we want to break before getting to the end */
                if (g_list_next(new_pt) == NULL)
                {
                    break;
                }
            }
            break;

        default:
            break;
    }

    /* Now set the new point */
    if (new_pt != NULL)
    {
        PDH_MarkPosition(new_pt);
    }
}

/**
 * This function steps the current point forward or backward as determined by
 * the parameter dir.
 *
 * @param direction next or previous
 * @param stepType by minute or by point
 *
 * @return new current point if one is found
 */
GList *PDH_StepPoint(gint direction, gint stepType)
{
    GList *newPt = NULL;

    if (direction == StepPrev)
    {
        newPt = previousPoint(thisCurrentPtList, stepType);
    }

    else
    {
        newPt = nextPoint(thisCurrentPtList, stepType);
    }

    return newPt;
}

/**
 * This function is called when the next/previous event arrow button is pressed.
 * It shifts the display to the next or previous event track, and updates all
 * displays accordingly.
 *
 * @param direction next or previous
 */
void PDH_StepEvent(gint direction)
{
    /* verify the current point is valid */
    if (thisCurrentPtList != NULL)
    {
        /* need to verify if the event list needs to be
           traversed in a linear fashion. if the locksel
           flag is set, then only those events that are
           marked as 'displayed' are used */
        if (ED_GetLockSel() == True)
        {
            /* do the locksel behaviour */
            selectEventLocksel(direction);
        }
        else
        {
            /* do the default behaviour */
            selectEventDefault(direction);
        }
    }
}

/**
 * Traverses the list and finds only those events being
 * displayed in the events dialog. Based on the direction
 * indicates which event is traversed next. For multiple events
 * we want to round-robin on these events, so if we're on the
 * last event, need to back to the beginning.
 *
 * @param direction
 */
static void selectEventLocksel(gint direction)
{
    GList *psList;

    /* get the current points data (PredictPt) */
    PredictPt *currentPt = (PredictPt *)thisCurrentPtList->data;

    /* get the current points, PredictSet */
    PredictSet *ps = (PredictSet *)((GList *)currentPt->set)->data;
    ps->locksel = False;

    /* which direction we going? */
    if (direction == StepNext)
    {
        /* find the next predict */
        for (psList = currentPt->set; psList != NULL; psList = g_list_next(psList))
        {
            if (psList->next != NULL)
            {
                ps = (PredictSet *)psList->next->data;
                if (ps->listed && ps->displayed & DISPLAY_ED)
                {
                    ps->locksel = True;
                    ps->displayed |= DISPLAY_PD;
                    break;
                }
            }
        }

        /* did we run off the end of the list? */
        if (psList == NULL)
        {
            /* start from the beginning */
            /* find the first predict that meets the criteria */
            for (psList = thisPredictSetList; psList != currentPt->set; psList = g_list_next(psList))
            {
                if (psList->next != NULL)
                {
                    ps = (PredictSet *)psList->next->data;
                    if (ps->listed && ps->displayed & DISPLAY_ED)
                    {
                        ps->locksel = True;
                        ps->displayed |= DISPLAY_PD;
                        break;
                    }
                }
            }
        }
    }
    else
    {
        /* find the previous predict */
        for (psList = currentPt->set; psList != NULL; psList = g_list_previous(psList))
        {
            if (psList->prev != NULL)
            {
                ps = (PredictSet *)psList->prev->data;
                if (ps->listed && ps->displayed & DISPLAY_ED)
                {
                    ps->locksel = True;
                    ps->displayed |= DISPLAY_PD;
                    break;
                }
            }
        }

        /* did we run off the end of the list? */
        if (psList == NULL)
        {
            /* start from the end */
            /* find the first predict that meets the criteria */
            for (psList = g_list_last(thisPredictSetList); psList != currentPt->set; psList = g_list_previous(psList))
            {
                if (psList->prev != NULL)
                {
                    ps = (PredictSet *)psList->prev->data;
                    if (ps->listed && ps->displayed & DISPLAY_ED)
                    {
                        ps->locksel = True;
                        ps->displayed |= DISPLAY_PD;
                        break;
                    }
                }
            }
        }
    }
}

/**
 * Traverses the list from the current point, either
 * selects the next or previous event based on the
 * direction selected.
 *
 * @param direction
 */
static void selectEventDefault(gint direction)
{
    GList *psList;
    PredictSet *ps;

    /* get the PredictPt data from the current point */
    PredictPt *currentPt = (PredictPt *)thisCurrentPtList->data;

    /* which direction we going? */
    if (direction == StepNext)
    {
        for (psList = currentPt->set; psList != NULL; psList = g_list_next(psList))
        {
            if (psList->next != NULL)
            {
                ps = (PredictSet *)psList->next->data;
                if (ps != NULL)
                {
                    if (ps->listed)
                    {
                        ps->locksel = True;
                        ps->displayed |= DISPLAY_PD;
                        break;
                    }
                }
            }
        }
    }
    else
    {
        for (psList = currentPt->set; psList != NULL; psList = g_list_previous(psList))
        {
            if (psList->prev != NULL)
            {
                ps = (PredictSet *)psList->prev->data;
                if (ps != NULL)
                {
                    if (ps->listed)
                    {
                        ps->locksel = True;
                        ps->displayed |= DISPLAY_PD;
                        break;
                    }
                }
            }
        }
    }

    /* no longer show the current event, we're changing */
    ps = (PredictSet *)((GList *)currentPt->set)->data;
    if (ps != NULL)
    {
        if (ps->displayed & DISPLAY_PD)
        {
            /* let next point be the current point */
            ps->locksel = False;
            ps->displayed -= DISPLAY_PD;
        }
    }
}

/**
 * This function updates which tracks in the list are marked as selected.
 * It is called when the "next track" or "previous track" button in the
 * prediction display is used to alter which tracks are displayed.
 *
 * @return True is predicts were loaded, otherwise False
 */
gboolean PDH_UpdateEvents(void)
{
    PredictSet *ps;
    GList *cp = NULL;
    gboolean predictsLoaded = False;
    gchar cpEvent[16] = {0};
    gchar cpTime[TIME_STR_LEN] = {0};
    gint cpOrbit = 0;

    /* get the current point info */
    if (thisCurrentPtList != NULL)
    {
        PredictPt *currentPt = (PredictPt *)thisCurrentPtList->data;
        ps = (PredictSet *)currentPt->set->data;
        if (ps != NULL)
        {
            cpOrbit = ps->orbit;
            g_stpcpy(cpEvent, ps->eventName);
            g_stpcpy(cpTime, currentPt->timeStr);
        }
    }

    /* predicts should all be loaded by now */
    if (cpEvent[0] != '\0')
    {
        GList *psList;
        for (psList = thisPredictSetList; (psList != NULL) && (cp == NULL); psList = g_list_next(psList))
        {
            ps = (PredictSet *)psList->data;
            if (ps != NULL)
            {
                if ((g_ascii_strcasecmp(cpEvent, ps->eventName) == False) &&
                    (cpOrbit == ps->orbit) &&
                    (ps->displayed & DISPLAY_PD) &&
                    (ps->locksel == True))
                {
                    cp = PDH_InterpolatePoint(psList, cpTime);
                    if (cp != NULL)
                    {
                        thisCurrentPtList = cp;
                        break;
                    }
                }
            }
        }
    }

    if (cp == NULL)
    {
        PDH_SelectPoint();
    }

    return predictsLoaded;
}

/**
 * This function updates the current point in the prediction display.  It is
 * called when there is no defined current point.  It sets the current point to
 * be the first unblocked point on the earliest possible displayed track.
 * If there are no unblocked points on any track, it sets the current point to
 * be the first point on the first displayed track.  If there are no displayed
 * tracks, it leaves the current point undefined.
 */
void PDH_SelectPoint(void)
{
    GList *psList;
    GList *cpt = NULL;

    for (psList = thisPredictSetList; (psList != NULL) && (cpt == NULL); psList = g_list_next(psList))
    {
        PredictSet *ps = (PredictSet *)psList->data;
        if (ps != NULL)
        {
            if ((ps->displayed & DISPLAY_PD) &&
                (ps->ptList != NULL) &&
                (ps->locksel == True))
            {
                GList *ptList = (GList *)ps->ptList;
                PredictPt *pt = (PredictPt *)ptList->data;
                cpt = ( ! pt->shoPt ) ? ps->ptList : g_list_next(ptList);
                break;
            }
        }
    }

    thisCurrentPtList = cpt;
}

/**
 * This method updates the current point to newPt and performs all necessary
 * ancillary updates
 *
 * @param newPt new point to mark
 */
void PDH_MarkPosition(GList *newPt)
{
    PredictSet *ps;

    if (newPt != NULL)
    {
        GList *psList;
        thisCurrentPtList = newPt;
        for (psList = thisPredictSetList; psList != NULL; psList = g_list_next(psList))
        {
            PredictPt *currentPt = (PredictPt *)thisCurrentPtList->data;

            ps = (PredictSet *)psList->data;
            if (ps != NULL)
            {
                ps->equivalentPt = NULL;
                if ((psList != currentPt->set) && (ps->displayed & DISPLAY_PD))
                {
                    gchar time_str[TIME_STR_LEN];

                    /* make time str copy, the PDH_InterpolatePoint method */
                    /* will release the pointer to interpolatePt, which has */
                    /* the pointer to timeStr */
                    g_stpcpy(time_str, currentPt->timeStr);
                    ps->equivalentPt = PDH_InterpolatePoint(psList, time_str);
                }
            }
        }
    }
}

/**
 * Takes the coordinate type and returns the coordinate point
 * field from the predict point structure.
 *
 * @param coord the coordinate type
 * @param ppt the predict point set
 *
 * @return coordinate point or NULL
 */
Point *PDH_GetCoordinatePoint(gint coord, PredictPt *ppt)
{
    Point *point = NULL;

    switch (coord)
    {
        case SBAND1:
        case SBAND2:
            point = &ppt->sbandPt;
            break;

        case KUBAND:
            point = &ppt->kubandPt;
            break;

        case KUBAND2:
            point = &ppt->kubandPt2;
            break;

        case STATION:
            point = &ppt->genericPt;
            break;

        default:
            point = &ppt->rawPt;
            break;
    }

    return point;
}
/**
 * Retrieves the auto update value from the config value.
 *
 * @param autoUpdateValue the value to retrieve
 * @param timeStr loads the value into this buffer
 *
 * @return gboolean True or False
 */
static gboolean getAutoUpdateValue(gchar *autoUpdateValue, gchar *timeStr)
{
    gboolean rc = True;

    if (CP_GetConfigData(autoUpdateValue, timeStr) == False)
    {
        rc = False;
        g_error("%s not defined in the config file!", autoUpdateValue);
    }

    return rc;
}
