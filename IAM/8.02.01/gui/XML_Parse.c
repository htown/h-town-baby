/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/*******************************************************************************
 **                                                                          **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS                **
 **                                                                          **
 ** This computer program is furnished on the condition that it be used only **
 ** in connection with the specified cooperative project, grant or contract  **
 ** under which it is provided and that no further use or dissemination      **
 ** shall be made without prior written permission of the NASA forwarding    **
 ** office. NMI 2210.2.B (12/13/90)                                          **
 **                                                                          **
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#include <expat.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
    #include <unistd.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
    #include <process.h>
    #include <io.h>
#endif

#define PRIVATE
#include "XML_Parse.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "DynamicDraw.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDataHandler.h"
#include "SolarDraw.h"
#include "TimeStrings.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/XML_Parse.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
#define BUFFER_SIZE 8192

/* various timers for the predict files */
static guint thisXmlTimerId[MAX_PREDICT_TYPES] = {0, 0, 0};

/* holds the TDRS info array data */
static GPtrArray *thisPrivateTdrsInfoArray = NULL;

/* holds the start and stop times for parsing the tdrs.predict */
static gchar thisStartTime[TIME_STR_LEN] = {UNSET_TIME};
static gchar thisStopTime[TIME_STR_LEN] = {UNSET_TIME};

/* flag indicating that the TDRS Predict data file is valid */
static gboolean thisHasPredictPoints = False;

/* holds the PredictSet list data */
static GList *thisPrivatePredictSetList = NULL;
typedef struct TDRSParseInfo
{
    glong utcStartTime;
    glong utcStopTime;

    gint trackNumber;

    gboolean startOfEvent;
    gboolean endOfEvent;
    gchar stopTime[TIME_STR_LEN];

    PredictSet *currentPS;

    gboolean eventIndexesFound;
    guint eventNameIndex;
    guint eventValueIndex;

    gboolean viewIndexesFound;
    guint viewNameIndex;
    guint viewValueIndex;
    guint viewShoIndex;

    gboolean predictIndexesFound;
    guint predictTimeIndex;
    guint predictAzIndex;
    guint predictElIndex;
} TDRSParseInfo;

/* holds the solar array list data */
static GList *thisPrivateSolarList = NULL;
typedef struct SUNPOSParseInfo
{
    gboolean sunIndexesFound;
    guint timeIndex;
    guint sunXindex;
    guint sunYindex;
    guint sunZindex;
} SUNPOSParseInfo;

/* holds the joint angle array data */
static GPtrArray *thisPrivateJointAngleArray = NULL;
typedef struct SUNABGParseInfo
{
    gboolean sunabgIndexesFound;
    guint timeIndex;
    guint a_1Aindex;
    guint a_1Bindex;
    guint a_2Aindex;
    guint a_2Bindex;
    guint a_3Aindex;
    guint a_3Bindex;
    guint a_4Aindex;
    guint a_4Bindex;
    guint PAindex;
    guint SAindex;
    guint PTCSindex;
    guint STCSindex;
} SUNABGParseInfo;

/**
 * Macro definition for leap year In the Gregorian calendar, which is the
 * calendar used by most modern countries, the following rules decides which
 * years are leap years:
 *
 * 1. Every year divisible by 4 is a leap year. 2. But every year divisible by
 * 100 is NOT a leap year 3. Unless the year is also divisible by 400, then it
 * is still a leap year.
 *
 * This means that year 1800, 1900, 2100, 2200, 2300 and 2500 are NOT leap
 * years, while year 2000 and 2400 are leap years.
 *
 * This actually means year 2000 is kind of special, as it is the first time the
 * third rule is used in many parts of the world.
 */
#define LeapYear(y)  ((((y)%400)==0) || ((((y)%100)!=0) && (((y)%4)==0)))

/**
 * Array indicating the default number of days in each month
 */
static int  dayInMth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

/* used for debugging */
#ifdef DEBUG
static void print_data(GList *sets);
#endif
/*************************************************************************/

/**
 * Determines if a new predict file has been dropped in the
 * predict input data folder.
 *
 * @param xmlConfigLookupKey
 * @param predictFileTime
 *
 * @return gint -1 = initial load, 0 = no change, >0 = new file
 */
gint XML_HaveNewPredict(gchar *xmlConfigLookupKey, time_t *predictFileTime)
{
    /* initially get the current file status and save in a static variable, this
     * will be used for subsequent checks to see if the file has changed. this
     * replaces the need for a file to check for status. so the first time in the
     * result will be True, until a new file is placed in the input folder, no
     * processing will occur.
    */
    gchar dataFile[MAXPATHLEN];
    struct stat statStruct;
    gint newFile = 0;

    g_message("enter XML_HaveNewPredict");

    /* get the predict filename from the config file */
    if (CP_GetConfigData(xmlConfigLookupKey, dataFile) == True)
    {
        g_message("XML_HaveNewPredict: %s",dataFile);

        /* stat the predict file */
        if (stat(dataFile, &statStruct) == 0)
        {
            /* is the previous time different ? */
            g_message("XML_HaveNewPredict: compare times:: %ld", *predictFileTime);
            g_message("                                    %ld", statStruct.st_mtime);
            if (*predictFileTime != statStruct.st_mtime)
            {
                g_message("XML_HaveNewPredict: not equal - new file");
                if (*predictFileTime == 0)
                {
                    newFile = -1;
                }
                else
                {
                    newFile = (gint)statStruct.st_mtime;
                }
                *predictFileTime = statStruct.st_mtime;

            }
            else
            {
                g_message("XML_HaveNewPredict: equal - no change");
            }
        }
    }
    else
    {
        g_warning("XML_HaveNewPredict: %s not found", xmlConfigLookupKey);
    }

    return newFile;
}

/**
 * Returns the state of the TDRS predict data file. If no points
 * are found, then the data file is out of date.
 *
 *
 * @return gboolean
 */
gboolean XML_HasTDRSPredictPoints(void)
{
    return thisHasPredictPoints;
}

/**
 * Opens a predict file for reading.
 *
 * @return FILE pointer or null
 */
static FILE *openPredictFile(gchar *whichPredictType)
{
    FILE *fp = NULL;
    struct stat stat_struct;
    gchar message[MAXPATHLEN];

    g_message("enter openPredictFile");

    gchar *predictFile = getPredictFile(whichPredictType);
    if (predictFile != NULL)
    {
        /* open the file */
        if ((fp = fopen(predictFile, "r")) != NULL)
        {
            /* stat the file */
            if (stat(predictFile, &stat_struct) == -1)
            {
                g_sprintf(message, "%s: unable to status Predict data file!", predictFile);
                ErrorMessage(AM_GetActiveDisplay(), "Stat Error", message);
                fclose(fp);
                fp = NULL;
            }

            /* non empty file */
            if (stat_struct.st_size == 0)
            {
                g_sprintf(message, "%s: Predict file is empty!", predictFile);
                ErrorMessage(AM_GetActiveDisplay(), "Stat Error", message);
                fclose(fp);
                fp = NULL;
            }
        }
        else
        {
            g_sprintf(message, "%s: unable to open Predict data file!", predictFile);
            ErrorMessage(AM_GetActiveDisplay(), "Open Error", message);
        }

        /* release memory */
        g_free(predictFile);
    }
    return fp;
}

/**
 * Gets the full path to the predict data file.
 *
 * @param whichPredictType
 *
 * @return gchar* path to predict file
 */
static gchar *getPredictFile(gchar *whichPredictType)
{
    gchar predictFile[MAXPATHLEN] = { 0};

    /* get the predict data file */
    if (CP_GetConfigData(whichPredictType, predictFile) == False)
    {
        gchar message[MAXPATHLEN];
        g_sprintf(message, "%s is not defined in the config file!", whichPredictType);
        ErrorMessage(AM_GetActiveDisplay(), "Config Error", message);
    }

    /* return allocated string */
    return g_strdup(predictFile);
}

/**
 * Starts a timer that calls the function provided.
 *
 * @param whichTimer
 * @param timerFunc
 */
void XML_StartTimer(gint whichTimer, GSourceFunc timerFunc)
{
    g_message("enter XML_StartTimer");

    if (thisXmlTimerId[whichTimer] == 0)
    {
        thisXmlTimerId[whichTimer] = g_timeout_add(getWaitTime(), timerFunc, NULL);
    }
}

/**
 * Parses for the wait time interval used for checking for new predict files.
 *
 * @return guint
 */
static guint getWaitTime(void)
{
    gchar data[256];
    guint waitTimeSecs = 300;

    /* if not found, use default of 300 */
    if (CP_GetConfigData(IAM_WAIT_PERIOD, data) == True)
    {
        /* convert time to a number */
        sscanf(data,"%u",&waitTimeSecs);
    }

    /* now bias from seconds to milliseconds for timer */
    waitTimeSecs *= 1000;

    return waitTimeSecs;
}

/**
 * Stops the timer defined by the timer type.
 *
 * @param whichTimer
 */
void XML_StopTimer(gint whichTimer)
{
    if (VALID_TIMER(whichTimer))
    {
        if (thisXmlTimerId[whichTimer] > 0)
        {
            g_source_remove(thisXmlTimerId[whichTimer]);
            thisXmlTimerId[whichTimer] = -1;
        }
    }
}

/**
 * Function that calls the appropriate function to parse the predict
 * data.
 *
 * @param data holds the predict type to parse
 *
 * @return void*
 */
void   XML_ProcessData(gint whichPredict)
{

    g_message("enter XML_ProcessData: whichPredict=%d\n", whichPredict);
    switch (whichPredict)
    {
        case TDRS_PREDICT:
            loadTdrsDataXML();
            break;

        case SUNPOS_PREDICT:
            loadSunPosDataXML();
            break;

        case SUNABG_PREDICT:
            loadSunAbgDataXML();
            break;

        case SHO_PREDICT:
            loadShoData();
            break;

        default:
            break;
    }
    g_message("leave XML_ProcessData");

    return ;
}

/**
 * Looks up the index for the attribute key and returns the location in the
 * attribute buffer where the value is located.
 *
 * @param attrKey
 * @param attr
 *
 * @return gint
 */
static gint getAttrIndex(gchar *attrKey, const gchar **attr)
{
    guint i;
    gint attrIndex = -1;
    for (i=0; attr[i]; i += 2)
    {
        if (g_strcasecmp(attr[i], attrKey) == 0)
        {
            attrIndex = i+1;
            break;
        }
    }
    return attrIndex;
}

void XML_SetStartTime(gchar *startTime)
{
    g_stpcpy(thisStartTime, startTime);
}

void XML_SetStopTime(gchar *stopTime)
{
    g_stpcpy(thisStopTime, stopTime);
}

/**
 * This value is used to compute the starting time from the
 * current time. The tdrs processed file will only be loaded
 * with the data that falls within the start and stop times.
 *
 * @return glong
 */
static glong getStartTime(void)
{

    /* the current time from ISP will be used */
    glong utcSecs = getTime(thisStartTime);
    g_message (" start time is %s \n",thisStartTime );

    return utcSecs;
}

/**
 * This value is used to compute a time interval
 * that will process only a portion of the tdrs predict file instead all of it.
 *
 *
 * @return glong
 */
static glong getStopTime(void)
{

    /* the current time from ISP will be used */
    glong utcSecs = getTime(thisStopTime);
    g_message (" stop time is %s \n",thisStopTime );

    return utcSecs;
}

/**
 * If the provided time string is not valid, then the ISP
 * provided current time will be used. Sometimes when initially
 * starting, ISP may not be connected, so we wait until it is.
 *
 * @param timeStr
 *
 * @return glong utcSecs
 */
static glong getTime(gchar *timeStr)
{
    glong utcSecs = 0;

    g_message("enter getTime: timeStr=%s", timeStr);

    /* if the time is not set, we need to build our own */
    if (g_ascii_strcasecmp(timeStr, UNSET_TIME) == 0)
    {
        gchar nowstr[TIME_STR_LEN];
        const struct tm *nowstruct;
        time_t nowbin;

        /* get time now */
        if (time(&nowbin) == (time_t) -1)
        {
            g_message("Count not get time of day from time()");
        }

        nowstruct = localtime(&nowbin);

        /* convert time to a format that can be converted to utc */
        if (strftime(nowstr, TIME_STR_LEN, "%Y:%m:%d:%H:%M:%S", nowstruct) == (size_t) 0)
        {
            g_message("Could not get string from strftime()");
        }
        g_message("strftime=%s", nowstr);
        utcSecs = utcStrToSecs(timeToUtcStr(nowstr));
    }
    else
    {
        g_message("getTime: convert time string to utc seconds");
        utcSecs = utcStrToSecs(timeToUtcStr(timeStr));
    }

    g_message("leave getTime: utcSecs=%ld", utcSecs);
    return utcSecs;
}

/**
 * Takes the incoming time string and nomalizes to a UTC
 * formatted string.
 *
 * @param timeStr
 *
 * @return gchar* UTC formatted string
 */
static gchar *timeToUtcStr(gchar *timeStr)
{
    if (IsGMT(timeStr))
    {
        GmtToUtc(timeStr);
    }
    else if (IsPET(timeStr))
    {
        PetToUtc(timeStr);
    }
    else
    {
        /* assumption is UTC format already */
    }

    return timeStr;
}

/**
 * This function converts a time string in UTC format (yyyy:mm:dd:hh:mm:ss -
 * year:month:dayofmonth:hour:minute:seconds) to a duration in seconds since the
 * start of the year.
 *
 * @param time_str
 *
 * @return long seconds since the beginning of the year
 */
static glong utcStrToSecs(gchar *timeStr)
{
    gint y, mm, d, h, m, s;
    gint i;
    glong days;
    glong secs;

    y = mm = d = h = m = s = 0;

    sscanf(timeStr, "%d:%d:%d:%d:%d:%d", &y, &mm, &d, &h, &m, &s );

    days = d;

    for (i=1; i<mm; i++)
    {
        days += dayInMth[i-1];
        if ((i == 2) && LeapYear(y))  days++;
    }

    secs = ((days * 24 + h) * 60 + m) * 60 + s;
    return secs;
}

/**
 * Sets up and manages the processing of the tdrs predict data file. Releases
 * any memory allocated, then reads the predict data file.
 *
 */
static void loadTdrsDataXML(void)
{
    {
        FILE *fp;
        g_message("enter loadTdrsDataXML");

        if ((fp = openPredictFile(TDRS_PREDICT_XML)) != NULL)
        {
            /* clear the internal data stores */
            thisPrivatePredictSetList = NULL;
            thisPrivateTdrsInfoArray = g_ptr_array_new();

            /* not predict points */
            thisHasPredictPoints = False;

            /* allocate our parser data definitions */
            TDRSParseInfo *parseInfo = (TDRSParseInfo *)MH_Calloc(sizeof (TDRSParseInfo), __FILE__, __LINE__);

            /* get start and stop times for parsing data */
            parseInfo->utcStartTime = getStartTime();
            parseInfo->utcStopTime = getStopTime();

            g_message("loadTdrsDataXML: startTime=%ld stopTime=%ld", parseInfo->utcStartTime, parseInfo->utcStopTime);

            /* get parser */
            XML_Parser parser = XML_ParserCreate(NULL);

            /* pass the parser info to event handlers */
            XML_SetUserData(parser, parseInfo);
            XML_UseParserAsHandlerArg(parser);

            /* need reader for data */
            XML_SetElementHandler(parser, startEvent, endEvent);

            /* do until done */
            gint done;
            do
            {
                gchar *buff = (gchar *)XML_GetBuffer(parser, BUFFER_SIZE);
                if (buff == NULL)
                {
                    g_message("COULD NOT GET PARSER BUFFER");
                    break;
                }

                /* read a line */
                gint len = (int)fread(buff, 1, BUFFER_SIZE, fp);
                done = len < sizeof(buff);

                /* parse this line */
                if (XML_ParseBuffer(parser, len, len == 0) == XML_STATUS_ERROR)
                {
                    g_message("PARSE ERROR");
                    break;
                }
            } while (!done);

            /* release resources */
            XML_ParserFree(parser);
            MH_Free(parseInfo);
            fclose( fp );
        }

        /* remove any events that don't have points */
        thisPrivatePredictSetList = removeEventsFromList(thisPrivatePredictSetList);

        /* set the predicts and the tdrs info */
        PDH_SetData(thisPrivatePredictSetList);
        PDH_SetTdrsInfo(thisPrivateTdrsInfoArray);
        g_message("leave loadTdrsDataXML");

    }
}

/**
 * Handles the key and value fields in the configuration file. These items are
 * added to the comment field before inserting into the hash and pointer array.
 *
 * @param data
 * @param el
 * @param attr
 */
static void startEvent(void *data, const gchar *el, const gchar **attr)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);

    /* is it time to save off the data */
    /* this flag is managed by the 'endEvent' function */
    if (parseInfo->endOfEvent == True)
    {
        /* save the data */
        parseInfo->endOfEvent   = False;
        if (parseInfo->currentPS != NULL)
        {
            /* load the stop time values */
            g_stpcpy(parseInfo->currentPS->losStr, parseInfo->stopTime);
            parseInfo->currentPS->losSecs = StrToSecs(TS_UTC, parseInfo->stopTime);
        }
    }

    /* find the beginning of an event */
    /* each match begins the start of an event */
    if (g_strcasecmp(el, "event") == 0)
    {
        /* start of event */
        parseInfo->startOfEvent = True;

        /* bump the track number */
        parseInfo->trackNumber++;

        /* go process this event data */
        processEvent(data, attr);

    }
    else if (g_strcasecmp(el, "predict") == 0)
    {
        /* go process this predict data */
        processPredict(data, attr);
    }
    else if (g_strcasecmp(el, "view") == 0)
    {
        /* go process the tdrs data */
        processView(data, attr);
    }
}

/**
 * Does the end processing of xml data. Currently checks to see if the 'event'
 * tag is present. This will signify when the event element has ended and its
 * time to add the event data.
 *
 * @param data
 * @param el
 */
static void endEvent(void *data, const gchar *el)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);
    if (g_strcasecmp(el, "event") == 0)
    {
        parseInfo->endOfEvent = True;
    }
}

/**
 * Gets the attribute data and loads the initial event information into a link
 * list.
 *
 * @param psList
 * @param attr
 *
 * @return PredictSet*
 */
static void processEvent(void *data, const gchar **attr)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);

    /* get the indexes for lookup of values */
    if (parseInfo->eventIndexesFound == False)
    {
        parseInfo->eventIndexesFound = True;
        parseInfo->eventNameIndex = getAttrIndex("name", attr);
        parseInfo->eventValueIndex = getAttrIndex("value", attr);
    }

    /* add this event to the link list and return it, should have values */
    addEvent(data,
             (gchar *)attr[parseInfo->eventNameIndex],
             (gchar *)attr[parseInfo->eventValueIndex]);
}

/**
 * Creates a new PredictSet instance and adds it to the link list of predicts.
 * The newly allocated predict is returned.
 *
 * @param psList
 * @param tdrs
 * @param tdrsId
 *
 */
static void addEvent(void *data, gchar *tdrs, gchar *tdrsId)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);

    PredictSet *ps = (PredictSet *) MH_Calloc( sizeof(PredictSet), __FILE__, __LINE__ );

    g_stpcpy(ps->eventName, tdrs);
    g_stpcpy(ps->trackLabel, tdrsId);

    g_stpcpy(ps->shoStart, UNSET_TIME);
    g_stpcpy(ps->shoEnd, UNSET_TIME);
    ps->ShoChanged = 0;
    ps->ShoEventNumber = 1;
    g_stpcpy(ps->shoStatus, "");

    ps->orbit = 0;
    ps->displayed = 0;

    /* by default all records are listed */
    ps->listed = True;

    /* we build the point list during the 'predict' parsing */
    ps->ptList = NULL;
    ps->interpolatedPt = NULL;
    ps->equivalentPt = NULL;
    ps->trackNumber = parseInfo->trackNumber;

    /* add the PredictSet to the internal list we maintain */
    /* once the list is built, we'll point it to the public list */
    thisPrivatePredictSetList = g_list_append(thisPrivatePredictSetList, ps);

    /* save the predict set */
    parseInfo->currentPS = ps;
}

/**
 * Processes the tdrs and sho cross-reference table. The sho
 * file references the tdrs as E-TDE, but iam wants to show the
 * tdrs numeric id instead.
 *
 * @param data
 * @param attr
 */
static void processView(void *data, const gchar **attr)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);

    /* get the indexes for lookup of values */
    if (parseInfo->viewIndexesFound == False)
    {
        parseInfo->viewIndexesFound = True;
        parseInfo->viewNameIndex = getAttrIndex("name", attr);
        parseInfo->viewValueIndex = getAttrIndex("value", attr);
        parseInfo->viewShoIndex = getAttrIndex("sho", attr);
    }

    /* load the TDRS info*/
    addTdrsInfo((gchar *)attr[parseInfo->viewNameIndex],
                (gchar *)attr[parseInfo->viewValueIndex],
                (gchar *)attr[parseInfo->viewShoIndex]);
}

/**
 * Adds the TDRS infomation found in the predict xml file.
 *
 * @param name
 * @param id
 * @param shoName
 */
static void addTdrsInfo(gchar *name, gchar *id, gchar *shoName)
{
    /* allocate a tdrs entry */
    TdrsInfo *tdrsInfo = (TdrsInfo *)MH_Calloc(sizeof(TdrsInfo),  __FILE__,  __LINE__);

    /* populate it with the given data */
    g_stpcpy(tdrsInfo->wlon, name);
    sscanf(id, "%d", &tdrsInfo->id);
    g_stpcpy(tdrsInfo->shoName, shoName);
    g_sprintf(tdrsInfo->eventName, "%s   %s", id, tdrsInfo->wlon);

    /* insert this tdrs data */
    g_ptr_array_add(thisPrivateTdrsInfoArray, tdrsInfo);
}

/**
 * Processes the predict data attribute for an event. The time field is returned
 * which is later used as the losStr time.
 *
 * @param psList
 * @param attr
 *
 */
static void processPredict(void *data, const gchar **attr)
{
    XML_Parser parser = (XML_Parser) data;
    TDRSParseInfo *parseInfo = (TDRSParseInfo *)XML_GetUserData(parser);

    /* get the indexes for lookup of values */
    if (parseInfo->predictIndexesFound == False)
    {
        parseInfo->predictIndexesFound = True;
        parseInfo->predictTimeIndex = getAttrIndex("time", attr);
        parseInfo->predictAzIndex = getAttrIndex("az", attr);
        parseInfo->predictElIndex = getAttrIndex("el", attr);
    }

    /* we should now have the attributes we want... */

    /* get time */
    g_stpcpy(parseInfo->stopTime, (gchar *)attr[parseInfo->predictTimeIndex]);

    /* need initial start time */
    if (parseInfo->startOfEvent == True)
    {
        // get the initial time once
        parseInfo->startOfEvent = False;
        if (parseInfo->currentPS != NULL)
        {
            g_stpcpy(parseInfo->currentPS->aosStr,
                     (gchar *)attr[parseInfo->predictTimeIndex]);
            parseInfo->currentPS->aosSecs = StrToSecs(TS_UTC, (gchar *)attr[parseInfo->predictTimeIndex]);
        }
    }

    /* make sure we have a valid predict set */
    if (parseInfo->currentPS != NULL)
    {
        /* only add points that are within the current time frame */
        glong aosUTC = utcStrToSecs((gchar *)attr[parseInfo->predictTimeIndex]);
        if (aosUTC >= parseInfo->utcStartTime &&
            aosUTC <= parseInfo->utcStopTime)
        {
            /* this flag indicates that this XML TDRS predict file is valid */
            thisHasPredictPoints = True;

            /* now add this predict to the set */
            addPredict(parseInfo->currentPS,
                       (gchar *)attr[parseInfo->predictTimeIndex],
                       (gchar *)attr[parseInfo->predictAzIndex],
                       (gchar *)attr[parseInfo->predictElIndex]);
        }
    }
}

/**
 * Adds the time, azimuth, elevation to the predict point list.
 *
 * @param ps
 * @param timeStr
 * @param az
 * @param el
 */
static void addPredict(PredictSet *ps, gchar *timeStr, gchar *az, gchar *el)
{
    gdouble azimuth, elevation;

    /* get the predict set for this point we're about to add */
    GList *psList = g_list_find(thisPrivatePredictSetList, ps);
    if (psList != NULL)
    {
        sscanf(az, "%le", &azimuth);
        sscanf(el, "%le", &elevation);

        /* get memory for point and add to list */
        PredictPt *pt = (PredictPt *) MH_Calloc(sizeof(PredictPt), __FILE__, __LINE__);

        /* next will be pointing to pt */
        pt->set = psList;
        g_stpcpy(pt->timeStr, timeStr);
        pt->timeSecs= StrToSecs(TS_UTC, timeStr);
        pt->interpolated = False;
        pt->shoPt = False;
        pt->dynamicBlock = PT_BlockageUnknown;
        pt->masks = 0;

        pt->rawPt.x = (gint)ROUND(10.0 * azimuth);
        pt->rawPt.y = (gint)ROUND(10.0 * elevation);

        /* convert for sband and kuband and store */
        StationAEtoSBandAE(pt->rawPt.x, pt->rawPt.y,
                           &(pt->sbandPt.x), &(pt->sbandPt.y));
        StationAEtoKuBand(pt->rawPt.x, pt->rawPt.y,
                          &(pt->kubandPt.x), &(pt->kubandPt.y), KUBAND);
        StationAEtoKuBand(pt->rawPt.x, pt->rawPt.y,
                          &(pt->kubandPt2.x), &(pt->kubandPt2.y), KUBAND2);
        StationAEtoGeneric(pt->rawPt.x, pt->rawPt.y,
                           &(pt->genericPt.x), &(pt->genericPt.y));

        /* add to the link list */
        ps->ptList = g_list_append(ps->ptList, pt);
    }
}

static GList *removeEventsFromList(GList *sets)
{
    guint i=0;
    GList *psList = g_list_first(sets);
    g_message("BEFORE REMOVING LENGTH=%d",g_list_length(sets));
    while (psList)
    {
        PredictSet *ps = (PredictSet *)psList->data;
        if (ps && (ps->ptList == NULL))
        {
            i++;
          // GIGO  printf("REMOVING %d:: %X... %-6s %02d %22s %22s",i,ps,ps->eventName,ps->orbit,ps->aosStr,ps->losStr);

            sets = g_list_remove(sets, ps);
            MH_Free(ps);
            ps = NULL;

            psList = g_list_first(sets);
            continue;
        }

        if (psList->next == NULL)
        {
            break;
        }
        psList = g_list_next(psList);
    }
    g_message("AFTER REMOVING LENGTH=%d",g_list_length(sets));
    g_message("REMOVED A TOTAL OF %d ELEMENTS",i);

    return sets;
}

#ifdef DEBUG
/**
 * Used for debugging the predict data set.
 *
 * @param sets
 */
static void print_data(GList *sets)
{
    GList *psList;
    g_message("PRINT PREDICT SET LIST:: List Count=%d",g_list_length(sets));
    for (psList = sets; psList != NULL; psList = g_list_next(psList))
    {
        gchar elapsed[TIME_STR_LEN];
        PredictSet *ps = (PredictSet *)psList->data;
        if (ps != NULL)
        {
            SecsToStr(TS_ELAPSED, (ps->losSecs - ps->aosSecs),  elapsed);
            StripTimeBlanks(elapsed);
            g_message("%x:: %-6s %02d %22s %22s %8s",(gint)ps,ps->eventName,ps->orbit,ps->aosStr,ps->losStr,elapsed);

            if (ps->ptList != NULL)
            {
                GList *ptList;
                for (ptList = ps->ptList; ptList != NULL; ptList = g_list_next(ptList))
                {
                    PredictPt *pt = (PredictPt *)ptList->data;
                    Point rawPt = pt->rawPt;
                    g_message("\t%-6s %22s %d %d",ps->eventName,pt->timeStr,rawPt.x,rawPt.y);
                }
            }
            else
            {
                g_message("\tNO POINTS");
            }
        }
        else
        {
            g_message("PREDICT SET IS NULL");
        }
    }
    g_message("LEAVING PRINT PREDICT SET");
}
#endif

/**
 * Loads the solar points from the sun_pos.predict.xml file. This function will
 * be called when IAM initially starts up and whenever a new predict file is
 * detected.
 *
 */
static void loadSunPosDataXML(void)
{
    {
        FILE *fp = NULL;
        g_message("enter loadSunPosDataXML");

        /* open the sunabg file */
        if ((fp = openPredictFile(SUNPOS_PREDICT_XML)) != NULL)
        {
            /* clear our internal data store */
            thisPrivateSolarList = NULL;

            /* allocate parser definitions */
            SUNPOSParseInfo *parseInfo = (SUNPOSParseInfo *)MH_Calloc(sizeof (SUNPOSParseInfo), __FILE__, __LINE__);

            /* get parser */
            XML_Parser parser = XML_ParserCreate(NULL);

            /* pass the parser info to event handlers */
            XML_SetUserData(parser, parseInfo);
            XML_UseParserAsHandlerArg(parser);

            /* need reader for data */
            XML_SetElementHandler(parser, startSunParser, endSunParser);

            /* do until done */
            gint done;
            do
            {
                gchar *buff = (gchar *)XML_GetBuffer(parser, BUFFER_SIZE);
                if (buff == NULL)
                {
                    g_message("COULD NOT GET PARSER BUFFER");
                    break;
                }

                /* read a line */
                gint len = (int)fread(buff, 1, BUFFER_SIZE, fp);
                done = len < sizeof(buff);

                /* parse this line */
                if (XML_ParseBuffer(parser, len, len == 0) == XML_STATUS_ERROR)
                {
                    g_message("PARSE ERROR");
                    break;
                }
            } while (!done);

            /* release resources */
            XML_ParserFree(parser);
            MH_Free(parseInfo);
            fclose( fp );
        }

        /* now load the solar point list */
        SD_SetData(thisPrivateSolarList);
        g_message("leave loadSunPosDataXML");

    }
}

/**
 * Processes the sun position file called by the parser. When the position
 * element is found, find the attributes we want to use; currently the time, x,
 * y, and z. The others may be used later.
 *
 * @param data
 * @param el
 * @param attr
 */
static void startSunParser(void *data, const gchar *el, const gchar **attr)
{
    /* is this the position element ? */
    if (g_strcasecmp(el, "position") == 0)
    {
        XML_Parser parser = (XML_Parser) data;
        SUNPOSParseInfo *parseInfo = (SUNPOSParseInfo *)XML_GetUserData(parser);

        gchar timeStr[TIME_STR_LEN] = {0};
        gdouble sun_x, sun_y, sun_z;

        /* get the indexes for lookup of values */
        if (parseInfo->sunIndexesFound == False)
        {
            parseInfo->sunIndexesFound = True;
            parseInfo->timeIndex = getAttrIndex("time", attr);
            parseInfo->sunXindex = getAttrIndex("sun_x", attr);
            parseInfo->sunYindex = getAttrIndex("sun_y", attr);
            parseInfo->sunZindex = getAttrIndex("sun_z", attr);
        }

        g_stpcpy(timeStr, attr[parseInfo->timeIndex]);
        sscanf(attr[parseInfo->sunXindex], "%le", &sun_x);
        sscanf(attr[parseInfo->sunYindex], "%le", &sun_y);
        sscanf(attr[parseInfo->sunZindex], "%le", &sun_z);

        /* process sun position data, should be set by now */
        addSunPos(timeStr, sun_x, sun_y, sun_z);
    }
}

/**
 * Called when a terminating element keyword is found. Currently not used.
 *
 * @param data
 * @param el
 */
static void endSunParser(void *data, const gchar *el)
{
}

/**
 * Allocates a SolarPoint instance and adds it to the doubly-link list.
 *
 * @param timeStr
 * @param x
 * @param y
 * @param z
 */
static void addSunPos(gchar *timeStr, gdouble x, gdouble y, gdouble z)
{
    SolarPoint *sp = (SolarPoint *) MH_Calloc(sizeof(SolarPoint), __FILE__, __LINE__);

    /* save the time */
    g_stpcpy (sp->timeStr, timeStr);

    /* convert for sband */
    XYZtoSBandAE   (x, y, z, &(sp->pt[SBAND1].x),  &(sp->pt[SBAND1].y));
    /* copy the sband1 to sband2, they're the same */
    sp->pt[SBAND2].x = sp->pt[SBAND1].x;
    sp->pt[SBAND2].y = sp->pt[SBAND1].y;

    /* convert for kuband */
    XYZtoKuBand    (x, y, z, &(sp->pt[KUBAND].x),  &(sp->pt[KUBAND].y), KUBAND);
    XYZtoKuBand    (x, y, z, &(sp->pt[KUBAND2].x),  &(sp->pt[KUBAND2].y), KUBAND2);
    /* convert for generic */
    XYZtoGenericAE (x, y, z, &(sp->pt[STATION].x), &(sp->pt[STATION].y));

    /* add to the link list */
    thisPrivateSolarList = g_list_append(thisPrivateSolarList, sp);
}

/**
 * Loads the joint angle data defined in the XML file.
 *
 */
static void loadSunAbgDataXML(void)
{
    {
        FILE *fp = NULL;
        g_message("enter loadSunAbgDataXML");

        /* open the sunabg file */
        if ((fp = openPredictFile(SUNABG_PREDICT_XML)) != NULL)
        {
            /* clear our private data store */
            thisPrivateJointAngleArray = NULL;

            /* allocate parser data definitions */
            SUNABGParseInfo *parseInfo = (SUNABGParseInfo *)MH_Calloc(sizeof (SUNABGParseInfo), __FILE__, __LINE__);

            /* get parser */
            XML_Parser parser = XML_ParserCreate(NULL);

            /* pass the parser info to event handlers */
            XML_SetUserData(parser, parseInfo);
            XML_UseParserAsHandlerArg(parser);

            /* need reader for data */
            XML_SetElementHandler(parser, startAngleParser, endAngleParser);

            /* allocate the pointer array for the joint angles */
            thisPrivateJointAngleArray = g_ptr_array_new();

            /* do until done */
            gint done;
            do
            {
                gchar *buff = (gchar *)XML_GetBuffer(parser, BUFFER_SIZE);
                if (buff == NULL)
                {
                    g_message("COULD NOT GET PARSER BUFFER");
                    break;
                }

                /* read a line */
                gint len = (int)fread(buff, 1, BUFFER_SIZE, fp);
                done = len < sizeof(buff);

                /* parse this line */
                if (XML_ParseBuffer(parser, len, len == 0) == XML_STATUS_ERROR)
                {
                    g_message("PARSE ERROR");
                    break;
                }
            } while (!done);

            /* release resources */
            XML_ParserFree(parser);
            MH_Free(parseInfo);
            fclose( fp );
        }

        /* now make data available */
        DD_SetData(thisPrivateJointAngleArray);
        g_message("leave loadSunAbgDataXML");

    }
}

/**
 * Parses the element and attributes for the joint angle data.
 *
 * @param data
 * @param el
 * @param attr
 */
static void startAngleParser(void *data, const gchar *el, const gchar **attr)
{
    if (g_strcasecmp(el, "angle") == 0)
    {
        XML_Parser parser = (XML_Parser) data;
        SUNABGParseInfo *parseInfo = (SUNABGParseInfo *)XML_GetUserData(parser);

        gchar timeStr[TIME_STR_LEN] = {0};
        gdouble angle2B, angle4B, angle2A, angle4A, anglePA, angleSA, angle1A;
        gdouble angle3A, angle1B, angle3B, anglePTCS, angleSTCS;

        if (parseInfo->sunabgIndexesFound == False)
        {
            parseInfo->sunabgIndexesFound = True;
            parseInfo->timeIndex = getAttrIndex("time", attr);
            parseInfo->a_2Bindex = getAttrIndex("a_2B", attr);
            parseInfo->a_4Bindex = getAttrIndex("a_4B", attr);
            parseInfo->a_2Aindex = getAttrIndex("a_2A", attr);
            parseInfo->a_4Aindex = getAttrIndex("a_4A", attr);
            parseInfo->PAindex = getAttrIndex("PA", attr);
            parseInfo->SAindex = getAttrIndex("SA", attr);
            parseInfo->a_1Aindex = getAttrIndex("a_1A", attr);
            parseInfo->a_3Aindex = getAttrIndex("a_3A", attr);
            parseInfo->a_1Bindex = getAttrIndex("a_1B", attr);
            parseInfo->a_3Bindex = getAttrIndex("a_3B", attr);
            parseInfo->PTCSindex = getAttrIndex("PTCS", attr);
            parseInfo->STCSindex = getAttrIndex("STCS", attr);
        }

        g_stpcpy(timeStr, attr[parseInfo->timeIndex]);
        sscanf(attr[parseInfo->a_2Bindex], "%le", &angle2B);
        sscanf(attr[parseInfo->a_4Bindex], "%le", &angle4B);
        sscanf(attr[parseInfo->a_2Aindex], "%le", &angle2A);
        sscanf(attr[parseInfo->a_4Aindex], "%le", &angle4A);
        sscanf(attr[parseInfo->PAindex], "%le", &anglePA);
        sscanf(attr[parseInfo->SAindex], "%le", &angleSA);
        sscanf(attr[parseInfo->a_1Aindex], "%le", &angle1A);
        sscanf(attr[parseInfo->a_3Aindex], "%le", &angle3A);
        sscanf(attr[parseInfo->a_1Bindex], "%le", &angle1B);
        sscanf(attr[parseInfo->a_3Bindex], "%le", &angle3B);
        sscanf(attr[parseInfo->PTCSindex], "%le", &anglePTCS);
        sscanf(attr[parseInfo->STCSindex], "%le", &angleSTCS);

        /* now load the angle into the link list, these values */
        /* should be loaded by now */
        addAngle(timeStr, angle2B, angle4B, angle2A,
                 angle4A, anglePA, angleSA, angle1A,
                 angle3A, angle1B, angle3B, anglePTCS,
                 angleSTCS);
    }
}

/**
 * Called when the terminating element is detected ... not used!
 *
 * @param data
 * @param el
 */
static void endAngleParser(void *data, const gchar *el)
{
}

/**
 * Adds angle data to a pointer array.
 *
 * @param timeStr
 * @param angle2B
 * @param angle4B
 * @param angle2A
 * @param angle4A
 * @param anglePA
 * @param angleSA
 * @param angle1A
 * @param angle3A
 * @param angle1B
 * @param angle3B
 * @param anglePTCS
 * @param angleSTCS
 */
static void addAngle(gchar *timeStr, gdouble angle2B, gdouble angle4B, gdouble angle2A,
                     gdouble angle4A, gdouble anglePA, gdouble angleSA, gdouble angle1A,
                     gdouble angle3A, gdouble angle1B, gdouble angle3B, gdouble anglePTCS,
                     gdouble angleSTCS)
{
    JointAngle *jointAngle = (JointAngle *)MH_Calloc(sizeof(JointAngle),  __FILE__,  __LINE__);

    g_stpcpy(jointAngle->timeStr, timeStr);
    jointAngle->angleValue[PORT_ANGLE_2B] = angle2B;
    jointAngle->angleValue[PORT_ANGLE_4B] = angle4B;
    jointAngle->angleValue[PORT_ANGLE_2A] = angle2A;
    jointAngle->angleValue[PORT_ANGLE_4A] = angle4A;
    jointAngle->angleValue[PORT_ANGLE_PA] = anglePA;
    jointAngle->angleValue[PORT_ANGLE_PTCS] = anglePTCS;

    jointAngle->angleValue[STBD_ANGLE_1A] = angle1A;
    jointAngle->angleValue[STBD_ANGLE_3A] = angle3A;
    jointAngle->angleValue[STBD_ANGLE_1B] = angle1B;
    jointAngle->angleValue[STBD_ANGLE_3B] = angle3B;
    jointAngle->angleValue[STBD_ANGLE_SA] = angleSA;
    jointAngle->angleValue[STBD_ANGLE_STCS] = angleSTCS;

    /* add to the pointer array */
    g_ptr_array_add(thisPrivateJointAngleArray,  jointAngle);
}


/**
 * This function scans the sho data file and loads the basic information for
 * each event track.  The information for each track is stored in a set.
 *
 * NOTE:  SHO file is not being updated for ISS, so it has times in the old
 *        shuttle GMT (no years).  So in order to match SHO times with predict
 *        events, the year of the first predict event is assumed to be the year
 *        of the SHO file.  This is a VERY BAD assumption, but it is all that we
 *        have until the SHO file is formatted for ISS.
 *
 * NOTE: this is NOT XML format, but instead what I call
 *       CLASSIC NASA FORMAT - basically a flat file with patterns - ugh!
 *
 * @param sets predict set
 */
static void loadShoData(void)
{
  
    /* get the current predict set list */
    GList *psList = PDH_GetData();
    if (psList != NULL)
    {
        FILE *fp;

        /*---------------*/
        /* Open SHO file */
        /*---------------*/
        if ((fp = openPredictFile(IAM_SHO_FILE)) != NULL)
        {
            gint year;

            /*----------------------------------------*/
            /* Get the year for the first predict set */
            /*----------------------------------------*/
            PredictSet *ps = (PredictSet *)psList->data;
            sscanf(ps->aosStr, "%d", &year);

            /*----------------------------*/
            /* Read each line in SHO file */
            /*----------------------------*/
            while (!feof(fp))
            {
                gchar line[256];

                gint orbit;

                gchar sat[24];
                gchar prevStartRaw[TIME_STR_LEN] = UNSET_TIME;
                gchar startRaw[TIME_STR_LEN], endRaw[TIME_STR_LEN];
                gchar startSho[TIME_STR_LEN], endSho[TIME_STR_LEN];
                gchar status[16];

                fgets(line, 256, fp);
                if (sscanf(line,"%d %23s %20s %*s %20s %15s",
                           &orbit, sat, startRaw, endRaw, status) == 5)
                {
                    GList *bestSet = NULL;
                    TdrsInfo *tdrsInfo;

                    /*--------------------*/
                    /* Validate TDRS name and cross reference to an ID */
                    /*--------------------*/
                    tdrsInfo = PDH_GetTdrsInfoByShoName(sat);
                    if (tdrsInfo)
                    {
                        g_sprintf(sat, "%d", tdrsInfo->id);
                    }
                    else
                    {
                        continue;
                    }

                    /*--------------------------------------------------*/
                    /* Validate start and stop times and convert to UTC */
                    /*--------------------------------------------------*/
                    if (g_ascii_strcasecmp(prevStartRaw, UNSET_TIME) == 0)
                    {
                        g_stpcpy(prevStartRaw, startRaw);
                    }

                    if (g_ascii_strcasecmp(prevStartRaw, startRaw) > 0)
                    {
                        year++;
                    }
                    g_sprintf(startSho, "%d %s", year, startRaw);

                    if (g_ascii_strcasecmp(startRaw, endRaw) > 0)
                    {
                        g_sprintf( endSho, "%d %s", year+1, endRaw);
                    }
                    else
                    {
                        g_sprintf(endSho, "%d %s", year, endRaw);
                    }

                    /* skip if the times are not valid */
                    if ((! ValidTime(TS_GMT, startSho)) || (! ValidTime(TS_GMT, endSho)))
                    {
                        continue;
                    }

                    g_stpcpy( prevStartRaw, startRaw );
                    GmtToUtc(startSho);
                    GmtToUtc(endSho);

                    /* Find predict set that best matches current SHO times */
                    bestSet = getBestMatch(psList, sat, startSho, endSho);
                    if (bestSet != NULL && bestSet->next != NULL)
                    {
                        /* get the current predict set */
                        PredictSet *bestPs = (PredictSet *)bestSet->data;

                        /* if the sho is not set, initialize it */
                        if (isShoUnset(bestPs, startSho, endSho, status, orbit) == False)
                        {
                            /* allocate a new predict */
                            PredictSet *psNew = (PredictSet *) MH_Calloc( sizeof(PredictSet), __FILE__, __LINE__ );

                            /* load up the new predict, memcpy will copy the eventName and trackLabel */
                            memcpy(psNew, bestPs, sizeof(PredictSet));
                            g_stpcpy(psNew->shoStart, startSho);
                            g_stpcpy(psNew->shoEnd, endSho);
                            g_stpcpy(psNew->shoStatus, status);
                            psNew->ShoChanged = 0;
                            psNew->orbit = orbit;
                            psNew->displayed = 0;
                            psNew->listed = True;
                            psNew->ptList = NULL;
                            psNew->interpolatedPt = NULL;
                            psNew->equivalentPt = NULL;

                            if (isShoProcessed(psList, bestSet, psNew) == False)
                            {
                                MH_Free(psNew);
                            }
                        }
                    }
                }
            }

            /* done with the file */
            fclose( fp );

            /* check the orbit numbers */
            checkOrbitNumbers(psList);
        }
    }
}

/**
 * Finds the predict with the closest time to the start and end of the SHO
 * entry.
 *
 * @param endSho
 * @param startSho
 * @param sat
 * @param sets
 *
 * @return
 */
static GList *getBestMatch(GList *sets, gchar *sat, gchar *startSho, gchar *endSho)
{
    GList *bestSet = NULL;

    glong bestTime = 0;

    glong startSecs = StrToSecs(TS_UTC, startSho);
    glong endSecs   = StrToSecs(TS_UTC, endSho);
    glong minTime   = (endSecs - startSecs) / 4;

    GList *psList;
    for (psList = sets; psList != NULL; psList = g_list_next(psList))
    {
        PredictSet *ps = (PredictSet *)psList->data;

        if (ps->trackLabel[0] == sat[0])
        {
            if ((ps->aosSecs <= endSecs) && (ps->losSecs >= startSecs))
            {
                glong time = MIN(ps->losSecs, endSecs) - MAX(ps->aosSecs, startSecs);
                if ((time >= minTime) && (time > bestTime) && (ps->ShoEventNumber == 1))
                {
                    bestTime = time;
                    bestSet  = psList;
                }
            }
        }
    }

    return bestSet;
}

/**
 * Determines if the SHO start time is unset. If it is, then the predict set is
 * initialized with the given argument values.
 *
 * @param orbit
 * @param status
 * @param endSho
 * @param startSho
 * @param bestPs
 *
 * @return
 */
static gboolean isShoUnset(PredictSet *bestPs, gchar *startSho, gchar *endSho, gchar *status, gint orbit)
{
    gboolean isUnset = False;

    if (g_ascii_strcasecmp(bestPs->shoStart, UNSET_TIME) == 0)
    {
        isUnset = True;
        g_stpcpy(bestPs->shoStart, startSho);
        g_stpcpy(bestPs->shoEnd, endSho);
        g_stpcpy(bestPs->shoStatus, status);
        bestPs->orbit = orbit;
    }

    return isUnset;
}

/**
 * Determines if the SHO entry has been processed.
 *
 * @param psNew
 * @param bestSet
 * @param sets
 *
 * @return True or False
 */
static gboolean isShoProcessed(GList *sets, GList *bestSet, PredictSet *psNew)
{
    gboolean isProcessed = False;

    /* get the current predict set */
    PredictSet *bestPs = (PredictSet *)bestSet->data;

    /* get the next predict set */
    PredictSet *bestPsNext = (PredictSet *)bestSet->next->data;

    if (g_ascii_strcasecmp(bestPsNext->eventName, bestPs->eventName) != 0)
    {
        isProcessed = True;

        /* update the sho event number */
        psNew->ShoEventNumber = bestPs->ShoEventNumber + 1;

        /* get the next set and insert the new data before it */
        bestSet = bestSet->next;
        sets = g_list_insert_before(sets, bestSet, psNew);
    }
    else
    {
        bestSet = bestSet->next;
        if (bestSet->next != NULL)
        {
            bestPsNext = (PredictSet *)bestSet->next->data;
            if (g_ascii_strcasecmp(bestPsNext->eventName, bestPs->eventName) != 0)
            {
                isProcessed = True;

                /* update the sho event number */
                psNew->ShoEventNumber = bestPs->ShoEventNumber + 2;

                /* get the next set and insert the new data before it */
                bestSet = bestSet->next;
                sets = g_list_insert_before(sets, bestSet, psNew);
            }
            else
            {
                /* the event names match so go to the next event
                   which is just another event on this track
                   and chain a data block to it */
                if (bestSet->next != NULL)
                {
                    bestSet = bestSet->next;
                    bestPsNext = (PredictSet *)bestSet->next->data;
                    if (g_ascii_strcasecmp(bestPsNext->eventName,bestPs->eventName) != 0)
                    {
                        isProcessed = True;

                        /* update the sho event number */
                        psNew->ShoEventNumber = bestPs->ShoEventNumber + 3;

                        /* get the next set and insert the new data before it */
                        bestSet = bestSet->next;
                        sets = g_list_insert_before(sets, bestSet, psNew);
                    }
                    else
                    {
                        /* the event names match so go to the next event
                           which is just another event on this track
                           and chain a data block to it */
                        if (bestSet->next != NULL)
                        {
                            bestSet = bestSet->next;
                            bestPsNext = (PredictSet *)bestSet->next->data;

                            if (g_ascii_strcasecmp(bestPsNext->eventName,bestPs->eventName) != 0)
                            {
                                isProcessed = True;

                                /* update the sho event number */
                                psNew->ShoEventNumber = bestPs->ShoEventNumber + 4;

                                /* get the next set and insert the new data before it */
                                bestSet = bestSet->next;
                                sets = g_list_insert_before(sets, bestSet, psNew);
                            }
                            /******************************************************************************
                            * Note, this code accommodates five SHOs during one track.  Any more than     *
                            * that, some events will "drop out of" the IAM Event Listing.  This is not    *
                            * the way I (Jackson) would prefer it, but until we start handling each SHO   *
                            * as a stand alone member of a class of SHO's this is the way it will have to *
                            * be.                                                                         *
                            ******************************************************************************/
                        }
                        else
                        {
                            isProcessed = True;

                            /* update the sho event number */
                            psNew->ShoEventNumber = bestPs->ShoEventNumber + 1;

                            /* the next event does not exist, so all I have to
                               do is to lease a data block and chain it to the
                               end of the list of events */
                            sets = g_list_append(sets, psNew);
                        }
                    }
                }
            }
        }
    }

    return isProcessed;
}

/**
 * This function assigns orbit numbers to the sets, based upon the orbit
 * information from the SHO data file.
 *
 * @param sets predict sets
 */
static void checkOrbitNumbers(GList *sets)
{
    GList *psList;
    PredictSet *ps;
    PredictSet *ps_next;
    gint noneSet = True;
    gint orbit;

    if (sets != NULL)
    {
        for (psList = sets; psList != NULL; psList = g_list_next(psList))
        {
            ps = (PredictSet *)psList->data;
            if (ps)
            {
                noneSet = noneSet & (ps->orbit == 0);
            }
        }

        if (noneSet)
        {
            orbit = 1;
            for (psList = sets; psList != NULL; psList = g_list_next(psList))
            {
                if ((ps = (PredictSet *)psList->data) != NULL)
                {
                    ps->orbit = orbit;
                    if (psList->next != NULL)
                    {
                        ps_next = (PredictSet *)psList->next->data;
                        if (ps_next->trackLabel[0] == 'W')
                        {
                            orbit++;
                        }
                    }
                }
            }
        }
        else
        {
            ps = (PredictSet *)sets->data;
            if (ps != NULL && ps->orbit == 0)
            {
                psList = sets;
                while ((psList != NULL) && (ps->orbit == 0))
                {
                    psList = g_list_next(psList);
                    if (psList != NULL)
                    {
                        ps = (PredictSet *)psList->data;
                    }
                }

                if (ps != NULL)
                {
                    orbit = ps->orbit;
                    for (; psList != NULL; psList = g_list_previous(psList))
                    {
                        /* assign the orbit */
                        ps->orbit = orbit;
                        if (ps != NULL && ps->trackLabel[0] == 'W')
                        {
                            /* different orbit */
                            orbit--;
                        }

                        /* get some data */
                        ps = (PredictSet *)psList->data;
                        if (ps == NULL)
                            break;
                    }
                }
            }

            for (psList = g_list_next(sets); psList != NULL; psList = g_list_next(psList))
            {
                ps = (PredictSet *)psList->data;
                if (ps != NULL)
                {
                    if ((ps->orbit == 0) && (psList->prev != NULL))
                    {
                        PredictSet *psPrev = (PredictSet *)psList->prev->data;
                        if (psPrev != NULL)
                        {
                            ps->orbit = psPrev->orbit;
                            if (ps->trackLabel[0] == 'W')
                            {
                                ps->orbit++;
                            }
                        }
                    }
                }
            }
        }
    }
}
