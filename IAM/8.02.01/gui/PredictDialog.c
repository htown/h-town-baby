/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#ifdef G_OS_WIN32
    #define WIN32_LEAN_AND_MEAN 1
//#include <windows.h>
    #include <process.h>
#endif

#define PRIVATE
#include "PredictDialog.h"
#undef PRIVATE

#include "AntennaDialog.h"
#include "AutoUpdateDialog.h"
#include "ColorHandler.h"
#include "ConfigParser.h"
#include "DynamicDraw.h"
#include "EventDialog.h"
#include "FontHandler.h"
#include "FTPDialog.h"
#include "HelpHandler.h"
#include "IspHandler.h"
#include "JointAngleDialog.h"
#include "ManualUpdateDialog.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "PixmapHandler.h"
#include "PetDialog.h"
#include "PredictDataHandler.h"
#include "PredictKuMaskDialog.h"
#include "PrintHandler.h"
#include "RealtimeDialog.h"
#include "SolarDraw.h"
#include "WindowGrid.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/PredictDialog.c 275 2017-02-22 21:29:05Z dpham1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/* optimum size based on 1280 X 1024 */
#define WINDOW_WIDTH    565
#define WINDOW_HEIGHT   650

#ifndef _EXT_PARTNERS
/*
 * set up for User Experience
 */
extern void GTKsetTopLevelWindow(GtkWidget *);
extern void GTKsetWindowIconified(GtkWidget *, gboolean);
extern void GTKsetAppConfigFunc(void *(*f)(int *));
extern void GTKsetAppRestoreFunc(void (*f)(void *, int));

/*
 * functions used for User Experience
 */
static void *UEconfigFunc(int *);
static void UErestoreFunc(void *, int);
#endif

/*
 * Define private data
 */
static GtkWidget *thisWindow = NULL;
static GtkWidget *thisWindowContainer = NULL;
static GtkWidget *thisTopForm = NULL;
static GtkWidget *thisNotebook = NULL;

/* window data */
static WindowData thisWindowData[MAX_DRAWING_AREAS];

static GtkUIManager *thisUImanager = NULL;

static GtkWidget *thisTrackValue = NULL;

static GtkWidget *thisAZXEL_label = NULL;

static GtkWidget *thisCurrentPoint[MAX_POINTS] = { NULL, NULL};
static GtkWidget *thisSolarPoint  [MAX_POINTS] = { NULL, NULL};

static PangoFontDescription *thisPangoFontDescription = NULL;

static GtkWidget *thisTrackButton = NULL;

/**
 * Flag indicating that coordinate tracking is enabled
 */
static gboolean thisCoordinateTracking = False;

/**
 * Handler ids for the drawing area motion notify events
 */
static WidgetHandlerData thisMotionHandlerData[NUM_VIEWS] =
{
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},
    {NULL, 0},/* notused */
    {NULL, 0},/* notused */
    {NULL, 0},/* notused */
    {NULL, 0} /* notused */
};

/**
 * Stores the cached data for drawing the coordinate points
 * This is used to draw when the main draw routine is invoked.
 */
typedef struct TrackingData
{
    WindowData *windata;
    gchar message[64];
    gint x_loc;
    gint y_loc;
} TrackingData;

static TrackingData thisTrackingData = { NULL, {0}, 0, 0};

/**
 * Coordinate tracking color
 */
static gchar thisTrackingColor[64]   = {0};

/* handler IDs for the pressed and activate callbacks */
static gulong thisLeftPressedHID = 0;
static gulong thisRightPressedHID = 0;
static gulong thisLeftActivateHID = 0;
static gulong thisRightActivateHID = 0;

static GtkWidget *thisTimeButton = NULL;
static GtkWidget *thisTimeEntry = NULL;

static GtkWidget *thisPointsLeftArrow = NULL;
static GtkWidget *thisPointsRightArrow = NULL;

static GtkWidget *thisEventsLeftArrow = NULL;
static GtkWidget *thisEventsRightArrow = NULL;

static TimerInfo thisTimerInfo = {0, 0, 0, NULL};

/**
 * One of SBAND1, SBAND2, KUBAND, STATION
 */
static gint thisCoordinateType = SBAND1;

/**
 * This is initialized with set_kuband_view_to_start.
 */
static gint          thisKubandDisplayed;
static gboolean      thisKubandDisplayedIsActive;

/**
 * One of SBAND1_VIEW, SBAND2_VIEW KUBAND_VIEW, STATION_VIEW, SKU_SBAND_VIEW, SKU_KUBAND_VIEW
 */
static gint thisCoordinateView = SBAND1_VIEW;

/**
 * Resize font resource
 */
static ResizeFont thisResizeFont;

/**
 * When the coordinate type is set to SKU_BAND, then this
 * value is set to one of SBAND or KUBAND
 */
static gint thisSKUCoordinateType = UNKNOWN_COORD;

static gint thisStepBy = StepByMinute;
static gboolean thisShowConeLos = False;
static gboolean thisShowLimit = False;
static gboolean thisShowShuttle = False;
static gboolean thisShowSolarTrace = False;
static gboolean thisKeyPressEntryActive = False;

static gint thisDataSBand1 = SBand1DrawingArea;
static gint thisDataSBand2 = SBand2DrawingArea;

static gint thisDataKuBandLeft = KuBandLeftDrawingArea;
static gint thisDataKuBandBottom = KuBandBottomDrawingArea;
static gint thisDataKuBand = KuBandDrawingArea;

static gint thisDataStationLeft = StationLeftDrawingArea;
static gint thisDataStationBottom = StationBottomDrawingArea;
static gint thisDataStation = StationDrawingArea;

static gboolean thisIsResizing = False;
static gboolean thisIsIconified = False;

static gboolean thisIsManualStructureChange = False;
static GtkActionGroup *thisStructureActionGroup = NULL;

static GtkWidget    *thisKubandToggleButton = NULL;
static GdkColor     yellow = {0, 0xffff, 0xffff, 0x0000};
static GdkColor     lightyellow = {0, 0xffff, 0xffff, 0x6666};
static GdkColor     gray = {0, 0xdddd, 0xdddd, 0xdddd};
static GdkColor     lightgray = {0, 0xeeee, 0xeeee, 0xeeee};

/**
 * Define the basic menu action entries.
 */
static GtkActionEntry entries[] =
{
    { FILE_MENU, NULL, "_File"},
    { REALTIME_DISPLAY_MENU, NULL,              /* name, stock id */
            "_Realtime Display", "<control>r",  /* label, accelerator */
            "Launch the realtime display",      /* tooltip */
            G_CALLBACK (fileMenuActionCb)    /* action callback */
    },
    { PREDICT_EVENTS_MENU, NULL,
            "Predict _Events","<control>e",
            "Launch the predict event display",
            G_CALLBACK (fileMenuActionCb)
    },
    { PRINT_PREDICT_MENU, GTK_STOCK_PRINT,
            "_Print Predict Screen", "<control>p",
            "Print the predict display",
            G_CALLBACK (fileMenuActionCb)
    },
    { PRINT_PREDICT_BLOCKAGE_MENU, GTK_STOCK_PREFERENCES,
            "Print Predict _Blockage", "<control>b",
            "Print the predict blockage",
            G_CALLBACK (fileMenuActionCb)
    },
    { GENERATE_PREDICTION_DATA_MENU, NULL,
            "_Generate Prediction Data", "<control>g",
            "Generate predication data",
            G_CALLBACK (fileMenuActionCb)
    },
    { CHECK_DATA_AUTOMATICALLY_MENU, NULL,
            "Check Data _Automatically", "<control>a",
            "Check for predict data automatically",
            G_CALLBACK (fileMenuActionCb)
    },
    { SEND_PREDICTION_DATA_MENU, NULL,
            "_Send Data to IPs", "<control>s",
            "Send prediction data to International Partners",
            G_CALLBACK (fileMenuActionCb)
    },
    { CLOSE_MENU, GTK_STOCK_CLOSE,
            "_Close", "<control>C",
            "Close Window",
            G_CALLBACK (fileMenuActionCb)
    },

    { SETTINGS_MENU, NULL, "_Settings"},
    { SET_BASE_PET_MENU, NULL,
            "Set Base _PET", "<shift><control>p",
            "Enable limit tracking",
            G_CALLBACK (settingsMenuActionCb)
    },
    { STEP_BY_MENU, NULL,
            "Step _by Minute", "<shift><control>b",
            "Step by time or data",
            G_CALLBACK (settingsMenuActionCb)
    },
    { CHANGE_ANTENNA_LOCATION_MENU, NULL,
            "Change _Antenna Location", "<shift><control>a",
            "Reorient the antenna",
            G_CALLBACK (settingsMenuActionCb)
    },
    { CHANGE_COVERAGE_DIRECTORY_MENU, NULL,
            "Change _Coverage Directory", "<shift><control>c",
            "Change location of pointing coverage files",
            G_CALLBACK (settingsMenuActionCb)
    },
    { CHANGE_DYNAMIC_STRUCTURE_MENU, NULL,
            "Change Dynamic Structure", NULL,
            "Change the dynamic structure currently loaded",
            G_CALLBACK (settingsMenuActionCb)
    },
    { CHANGE_KUBAND_VIEW, NULL,
            "Change Ku-band View", NULL,
            "Change which Ku-band is displayed",
            G_CALLBACK (settingsMenuActionCb)
    },
    { RELOAD_CONFIG_FILE_MENU, NULL,
            "_Reload Config File", "<shift><control>r",
            "Reread the iam.xml file",
            G_CALLBACK (settingsMenuActionCb)
    },

    { BLOCKAGE_MENU, NULL, "_Blockage"},
    { DISPLAY_CONE_MENU, NULL,
            "Display Cone", NULL,
            "Toggle view between LOS and Cone",
            G_CALLBACK (blockageMenuActionCb)
    },
    { SHOW_JOINT_ANGLES_MENU, NULL,
            "Show Joint Angles", "<control>j",
            "Show joint angle data",
            G_CALLBACK (blockageMenuActionCb)
    },
    { SHOW_KUBAND_MENU, NULL,
            "Define KuBand Masks", "<control>k",
            "KuBand mask definitions",
            G_CALLBACK (blockageMenuActionCb)
    },

    { SBAND1_MENU, NULL, "SBand 1"},
    { SBAND2_MENU, NULL, "SBand 2"},
    { KUBAND_MENU, NULL, "Ku-band"},
    { KUBAND2_MENU, NULL, "Ku-band"},
    { STATION_MENU, NULL, "Generic Az/El"},

    { HELP_MENU,     NULL, "_Help"},
    { HELP_HELP_MENU, GTK_STOCK_HELP,
            "_Help", "F1",
            "View user's guide",
            G_CALLBACK (helpMenuActionCb)
    },
    { HELP_GTK_MENU, NULL,
            "About _GTK", "<shift>F1",
            "Information about GTK+",
            G_CALLBACK (helpMenuActionCb)
    },
    { HELP_ABOUT_MENU, NULL,
            "About _IAM", "<control>F1",
            "Information about IAM",
            G_CALLBACK (helpMenuActionCb)
    },
};
static guint n_entries = G_N_ELEMENTS (entries);

/**
 * Define the check button (toggle) action entries
 */
static GtkToggleActionEntry toggle_entries[] =
{
    /* settings menu toggles */
    { SHOW_SOLAR_TRACE_MENU, NULL,                  /* name, stock id */
        "Show _Solar Trace", "<shift><control>s",   /* label, accelerator */
        "Show solar track",                         /* tooltip */
        G_CALLBACK (settingsMenuActionCb),       /* action handler */
        False                                       /* is active */
    },
    { COORDINATE_TRACKING_MENU, NULL,
            "Coordinate _Tracking", "<shift><control>t",
            "Enable coordinate tracking",
            G_CALLBACK (settingsMenuActionCb),
            False
    },
    /* blockage menu toggles */
    { SHOW_LIMITS_MENU, NULL,
            "Show Limits", NULL,
            "Toggle limit blockage",
            G_CALLBACK (blockageMenuActionCb),
            False
    },
    { SHOW_SHUTTLE_DOCKED_MENU, NULL,
            "Show Shuttle Docked", NULL,
            "Toggle shuttle docked",
            G_CALLBACK (blockageMenuActionCb),
            False
    }
};
static guint n_toggle_entries = G_N_ELEMENTS (toggle_entries);

static GtkRadioActionEntry kuband_view_radio_entries[] =
{
    { CHANGE_KUBAND_VIEW_TO_ONE_MENU, NULL,
      "1", NULL, NULL, 1
    },
    { CHANGE_KUBAND_VIEW_TO_TWO_MENU, NULL,
      "2", NULL, NULL, 2
    }
};
static guint n_kuband_view_radio_entries = G_N_ELEMENTS (kuband_view_radio_entries);

static const gchar *thisFileMenu =
    "       <menu action='"FILE_MENU"'>\n"
    "           <menuitem action='"REALTIME_DISPLAY_MENU"'/>\n"
    "           <menuitem action='"PREDICT_EVENTS_MENU"'/>\n"
    "           <menuitem action='"PRINT_PREDICT_MENU"'/>\n"
    "           <menuitem action='"PRINT_PREDICT_BLOCKAGE_MENU"'/>\n"
    "           <separator/>\n"
    "           <menuitem action='"GENERATE_PREDICTION_DATA_MENU"'/>\n"
    "           <menuitem action='"CHECK_DATA_AUTOMATICALLY_MENU"'/>\n"
    "           <menuitem action='"SEND_PREDICTION_DATA_MENU"'/>\n"
    "           <separator/>\n"
    "           <menuitem action='"CLOSE_MENU"'/>\n"
    "       </menu>\n";

static const gchar *thisSettingsMenu =
    "       <menu action='"SETTINGS_MENU"'>\n"
    "           <menuitem action='"SET_BASE_PET_MENU"'/>\n"
    "           <menuitem action='"STEP_BY_MENU"'/>\n"
    "           <menuitem action='"SHOW_SOLAR_TRACE_MENU"'/>\n";

static const gchar *thisSettingsMenu2 =
    "           <separator/>\n"
    "           <menuitem action='"COORDINATE_TRACKING_MENU"'/>\n"
    "           <menuitem action='"CHANGE_ANTENNA_LOCATION_MENU"'/>\n"
    "           <menuitem action='"CHANGE_COVERAGE_DIRECTORY_MENU"'/>\n"
    "           <menu action='"CHANGE_DYNAMIC_STRUCTURE_MENU"'>\n"
    "           </menu>\n"
    "           <menu action='"CHANGE_KUBAND_VIEW"'>\n"
    "               <menuitem action='"CHANGE_KUBAND_VIEW_TO_ONE_MENU"'/>\n"
    "               <menuitem action='"CHANGE_KUBAND_VIEW_TO_TWO_MENU"'/>\n"
    "           </menu>\n"
    "           <menuitem action='"RELOAD_CONFIG_FILE_MENU"'/>\n"
    "      </menu>\n";

static const gchar *thisBlockageMenu =
    "      <menu action='"BLOCKAGE_MENU"'>\n"
    "           <menuitem action='"DISPLAY_CONE_MENU"'/>\n"
    "           <menuitem action='"SHOW_LIMITS_MENU"'/>\n"
    "           <menuitem action='"SHOW_SHUTTLE_DOCKED_MENU"'/>\n"
    "           <menuitem action='"SHOW_JOINT_ANGLES_MENU"'/>\n"
    "           <menuitem action='"SHOW_KUBAND_MENU"'/>\n"
    "           <separator/>\n"
    "           <menu action='"SBAND1_MENU"'/>\n"
    "           <menu action='"SBAND2_MENU"'/>\n"
    "           <menu action='"KUBAND_MENU"'/>\n"
    "           <menu action='"KUBAND2_MENU"'/>\n"
    "           <menu action='"STATION_MENU"'/>\n"
    "      </menu>\n";

static const gchar *thisHelpMenu =
    "      <menu action='"HELP_MENU"'>\n"
    "           <menuitem action='"HELP_HELP_MENU"'/>\n"
    "           <menuitem action='"HELP_GTK_MENU"'/>\n"
    "           <menuitem action='"HELP_ABOUT_MENU"'/>\n"
    "      </menu>\n";
/*************************************************************************/

/**
 * Create the main window instance
 *
 * @return True if window was created ok, otherwise False
 */
gboolean PD_Create(void)
{
    guint i;

    if (thisWindow == NULL)
    {
        for (i=0; i<MAX_DRAWING_AREAS; i++)
        {
            switch (i)
            {
                /* these are handled differently - pixmaps not needed */
                /* they're data is drawn directly into the drawable */
                case KuBandLeftDrawingArea:
                case KuBandBottomDrawingArea:
                case S1KU_KuBandLeftDrawingArea:
                case S1KU_KuBandBottomDrawingArea:
                case S2KU_KuBandLeftDrawingArea:
                case S2KU_KuBandBottomDrawingArea:
                case StationLeftDrawingArea:
                case StationBottomDrawingArea:
                    /* do nothing */
                    break;

                default:
                    thisWindowData[i].toolbar = NULL;
                    thisWindowData[i].toolbar2 = NULL;
                    thisWindowData[i].tool_items = NULL;
                    thisWindowData[i].tool_items2 = NULL;
                    thisWindowData[i].drawing_area = NULL;
                    thisWindowData[i].src_pixmap = NULL;
                    thisWindowData[i].redraw_pixmap = True;
                    thisWindowData[i].is_resized = False;

                    /* where the structure is drawn */
                    thisWindowData[i].dynamic_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].dynamic_data->pixmap = NULL;
                    thisWindowData[i].dynamic_data->redraw = True;
                    thisWindowData[i].dynamic_data->width = 0;
                    thisWindowData[i].dynamic_data->height = 0;

                    /* where static masks are drawn (except the limit mask */
                    thisWindowData[i].mask_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].mask_data->pixmap = NULL;
                    thisWindowData[i].mask_data->redraw = True;
                    thisWindowData[i].mask_data->width = 0;
                    thisWindowData[i].mask_data->height = 0;

                    /* where the obscuration masks are draw */
                    thisWindowData[i].obs_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].obs_data->pixmap = NULL;
                    thisWindowData[i].obs_data->redraw = True;
                    thisWindowData[i].obs_data->width = 0;
                    thisWindowData[i].obs_data->height = 0;

                    /* the tdrs track go here */
                    thisWindowData[i].track_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].track_data->pixmap = NULL;
                    thisWindowData[i].track_data->redraw = True;
                    thisWindowData[i].track_data->width = 0;
                    thisWindowData[i].track_data->height = 0;

                    /* static background */
                    thisWindowData[i].bgpixmap_data = (PixmapData *)MH_Calloc(sizeof (PixmapData), __FILE__, __LINE__);
                    thisWindowData[i].bgpixmap_data->pixmap = NULL;
                    thisWindowData[i].bgpixmap_data->redraw = True;
                    thisWindowData[i].bgpixmap_data->width = 0;
                    thisWindowData[i].bgpixmap_data->height = 0;
                    break;
            }
        }

        /* initialize font resize instance */
        g_stpcpy(thisResizeFont.rc_string, IAM_GTKRC_PREDICT_FONT_SIZE);
        thisResizeFont.default_font_size = DEFAULT_FONT_SIZE;
        thisResizeFont.previous_font_size = DEFAULT_FONT_SIZE;
        thisResizeFont.default_width = WINDOW_WIDTH;
        thisResizeFont.previous_width = WINDOW_WIDTH;
        thisResizeFont.new_width = -1;
        thisResizeFont.default_height = WINDOW_HEIGHT;
        thisResizeFont.previous_height = WINDOW_HEIGHT;
        thisResizeFont.new_height = -1;

        /* get coordinate tracking color from config file */
        if (CP_GetConfigData(IAM_TRACKING_COLOR, thisTrackingColor) == False)
        {
            /* couldn't find, default to white */
            g_stpcpy(thisTrackingColor, "White");
        }

        /* initialize the dialog constructor */
        D_DialogConstructor();

        /* now make the dialog */
        createWindow(&thisResizeFont);

        /* attach icon to the window */
        D_SetIcon(thisWindow);

        /* create the dialog title */
        PD_SetDialogTitle();

        /* set gen_predict menu item for manager mode */
        enableGenPredictMenuItem();

        /* set the coverage menu item to False if 'Mission' activity */
        if (AM_GetActivityType() == ACTIVITY_TYPE_MIS)
        {
            D_EnableMenuItem(thisUImanager,
                             "/"MENU_BAR"/"FILE_MENU"/"CHANGE_COVERAGE_DIRECTORY_MENU,
                             False);
        }

        /* disable the auto update capability if this is NOT the manager */
        if (AM_GetAppMode() != MANAGER)
        {
            D_EnableMenuItem(thisUImanager,
                             "/"MENU_BAR"/"FILE_MENU"/"CHECK_DATA_AUTOMATICALLY_MENU,
                             False);
        }

        /* create the pango font description */
        setPangoFontDesc(&thisResizeFont, thisWindow->style->font_desc);

        /* set ftp menu item for cato/manager cert mode only */
        enableFtpMenuItem();

        /* setup kuband masks for drawing */
        MF_SetKuBandPredictMasks();
    }

    // 8.01
    // Create Event dialog to get initial data. Do not show.
    if (ED_Create() == True) 
      {
      // Points to next event on default list
	if (PDH_HasEvent(StepNext) == True)
	  setEvents(StepNext);
      }
    // End 8.01

    return(thisWindow ? True : False);
}

/**
 * This method opens the dialog
 */
void PD_Open(void)
{
    static gboolean default_view = False;

    if (thisWindow != NULL)
    {
        PD_UpdateEventArrows();
        if (thisIsIconified == True)
        {
            gtk_window_deiconify(GTK_WINDOW(thisWindow));
        }

        gtk_window_set_position(GTK_WINDOW(thisWindow), GTK_WIN_POS_CENTER);
        gtk_widget_show_all(thisWindow);
        gdk_window_raise(thisWindow->window);

        if (default_view == False)
        {
            default_view = True;
            D_SetDefaultView(PREDICT, thisNotebook);

            /* set the sband and station masks */
            setMasksEnabled(SBAND1, IAM_PREDICT_SBAND1_ENABLED_MASKS);
            setMasksEnabled(SBAND2, IAM_PREDICT_SBAND2_ENABLED_MASKS);
            setMasksEnabled(KUBAND, IAM_PREDICT_KUBAND_ENABLED_MASKS);
            setMasksEnabled(KUBAND2, IAM_PREDICT_KUBAND2_ENABLED_MASKS);
            setMasksEnabled(STATION, IAM_PREDICT_STATION_ENABLED_MASKS);
            AM_SetActiveDisplay(DISPLAY_PD, True);
            set_kuband_view_to_start();
        }
        else
        {
            set_kuband_displayed(thisKubandDisplayed);
        }

    }
}

/**
 * Returns the window instance
 *
 * @return this window
 */
GtkWidget *PD_GetDialog(void)
{
    return(thisWindow);
}

/**
 * Releases all memory
 */
void PD_Destructor(void)
{
    guint i;
    GList *glist;

    for (i=0; i<MAX_DRAWING_AREAS; i++)
    {
        for (glist = thisWindowData[i].tool_items; glist; glist = glist->next)
        {
            MH_Free(glist->data);
            glist->data = NULL;
        }

        if (thisWindowData[i].src_pixmap != NULL)
        {
            g_object_unref(thisWindowData[i].src_pixmap);
        }
        if (thisWindowData[i].dynamic_data != NULL)
        {
            if (thisWindowData[i].dynamic_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].dynamic_data->pixmap);
            }
            MH_Free(thisWindowData[i].dynamic_data);
            thisWindowData[i].dynamic_data = NULL;

        }
        if (thisWindowData[i].obs_data != NULL)
        {
            if (thisWindowData[i].obs_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].obs_data->pixmap);
            }
            MH_Free(thisWindowData[i].obs_data);
            thisWindowData[i].obs_data = NULL;
        }
        if (thisWindowData[i].mask_data != NULL)
        {
            if (thisWindowData[i].mask_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].mask_data->pixmap);
            }
            MH_Free(thisWindowData[i].mask_data);
        }
        if (thisWindowData[i].track_data != NULL)
        {
            if (thisWindowData[i].track_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].track_data->pixmap);
            }
            MH_Free(thisWindowData[i].track_data);
            thisWindowData[i].track_data = NULL;
        }
        if (thisWindowData[i].bgpixmap_data != NULL)
        {
            if (thisWindowData[i].bgpixmap_data->pixmap != NULL)
            {
                g_object_unref(thisWindowData[i].bgpixmap_data->pixmap);
            }
            MH_Free(thisWindowData[i].bgpixmap_data);
            thisWindowData[i].bgpixmap_data = NULL;
        }
    }
}

/**
 * Sets the predict dialog title
 */
void PD_SetDialogTitle(void)
{
    D_SetDialogTitle(thisWindow, PREDICT_TITLE, thisCoordinateType);
}

/**
 * This method determines if the dialog is up and active
 *
 * @return True or False
 */
gboolean PD_IsActive(void)
{
    if (thisWindow != NULL && GTK_WIDGET_VISIBLE(thisWindow))
    {
        return(True);
    }

    return(False);
}

/**
 * Returns the resizing flag. This flag is set the resizeDrawingAreaCb and cleared
 * in the exposeCb
 *
 * @return resizing state
 */
gboolean PD_IsResizing(void)
{
    return(thisIsResizing);
}

/**
 * Does actual creation of the window component
 *
 * @param resize_font has font resize information
 *
 * @return window instance
 */
static void createWindow(ResizeFont *resize_font)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *separator;

    GtkActionGroup *actions;
    GError *error = NULL;
    gchar *ui_info;

    Dimension *dim;

    /* create window */
    thisWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_name(thisWindow, "Predict");

#ifndef _EXT_PARTNERS
     /* set top level window for User Experience */
    GTKsetTopLevelWindow(thisWindow);
    GTKsetWindowIconified(thisWindow, thisIsIconified);
    GTKsetAppConfigFunc(&UEconfigFunc);
    GTKsetAppRestoreFunc(&UErestoreFunc);
#endif

    /*
    * windows/dialogs can only have one container!
    * the first vbox will contain the menubar and hbox. this allows the menubar
    * to attach to the left/right edges without and margin offsets
    */
    vbox = gtk_vbox_new(False, 0);

    actions = gtk_action_group_new ("Actions");
    gtk_action_group_add_actions (actions, entries, n_entries, NULL);
    gtk_action_group_add_toggle_actions (actions,
                                         toggle_entries, n_toggle_entries,
                                         NULL);

    thisUImanager = gtk_ui_manager_new ();
    gtk_ui_manager_set_add_tearoffs(thisUImanager, True);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    actions = gtk_action_group_new(CHANGE_KUBAND_VIEW);
    gtk_action_group_add_radio_actions (actions,
                                        kuband_view_radio_entries, n_kuband_view_radio_entries,
                                        1, // This needs to be a gint that defaults to the active kuband
                                        G_CALLBACK (settings_menu_kuband_view_cb),
                                        NULL);
    gtk_ui_manager_insert_action_group (thisUImanager, actions, 0);

    gtk_window_add_accel_group (GTK_WINDOW (thisWindow),
                                gtk_ui_manager_get_accel_group (thisUImanager));

    ui_info = D_CreateMenu(thisFileMenu, thisSettingsMenu, thisSettingsMenu2, thisBlockageMenu, thisHelpMenu);
    if (ui_info != NULL)
    {
        g_message("createWindow: predict, ui_info=%s",ui_info);
        if (!gtk_ui_manager_add_ui_from_string (thisUImanager, ui_info, -1, &error))
        {
            g_warning("PredictDialog: building menus failed: %s", error->message);
            g_error_free (error);
        }
    }
    else
    {
        g_warning("PredictDialog: building menus failed");
    }
    g_free(ui_info);

    D_AddBlockageItemsToMenubar(PREDICT, SBAND1,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND1_MENU,
                                (GCallback)sband1BlockageActionCb);
    D_AddBlockageItemsToMenubar(PREDICT, SBAND2,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"SBAND2_MENU,
                                (GCallback)sband2BlockageActionCb);
    D_AddBlockageItemsToMenubar(PREDICT, KUBAND,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND_MENU,
                                (GCallback)kubandBlockageActionCb);
    D_AddBlockageItemsToMenubar(PREDICT, KUBAND2,  thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"KUBAND2_MENU,
                                (GCallback)kuband2BlockageActionCb);
    D_AddBlockageItemsToMenubar(PREDICT, STATION, thisUImanager,
                                "/"MENU_BAR"/"BLOCKAGE_MENU"/"STATION_MENU,
                                (GCallback)stationBlockageActionCb);
    thisStructureActionGroup = D_AddStructureItemsToMenuBar(PREDICT, thisUImanager,
                                                            "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_DYNAMIC_STRUCTURE_MENU,
                                                            (GCallback)structureChangeActionCb);


    gtk_box_pack_start (GTK_BOX (vbox),
                        gtk_ui_manager_get_widget (thisUImanager, "/"MENU_BAR),
                        False, False, 0);

    /* this will select the menu item and make the call to the callback */
    /* preselect solar trace and limits */
    D_PreselectMenuItem(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"SHOW_LIMITS_MENU);
    D_PreselectMenuItem(thisUImanager, "/"MENU_BAR"/"SETTINGS_MENU"/"SHOW_SOLAR_TRACE_MENU);

    /* setup callbacks */
    g_signal_connect(G_OBJECT(thisWindow), "key-press-event",
                     G_CALLBACK(keyPressEventCb),
                     NULL);
    g_signal_connect(G_OBJECT(thisWindow), "delete-event",
                     G_CALLBACK(closeCb),
                     NULL);
    g_signal_connect(G_OBJECT(thisWindow), "window-state-event",
                     G_CALLBACK(visibilityCb),
                     NULL);
    g_signal_connect(G_OBJECT(thisWindow), "configure-event",
                     G_CALLBACK(resizeWindowCb),
                     resize_font);
    g_signal_connect(G_OBJECT(thisWindow), "focus-in-event",
                     G_CALLBACK(focusInEventCb),
                     NULL);
    g_signal_connect(G_OBJECT(thisWindow), "focus-out-event",
                     G_CALLBACK(focusOutEventCb),
                     NULL);

    /* compute optimum window size */
    dim = AM_GetWindowSize(thisWindow, WINDOW_WIDTH, WINDOW_HEIGHT);
    {
        gtk_window_set_default_size(GTK_WINDOW(thisWindow), dim->width, dim->height);
        gtk_widget_set_size_request(GTK_WIDGET(thisWindow),
                                    (gint)(dim->width*0.65),
                                    (gint)(dim->height*WINDOW_MIN_PERCENT));

        /* reset the resize font defaults */
        thisResizeFont.default_width = dim->width;
        thisResizeFont.default_height = dim->height;
    }
    MH_Free(dim);

    /* this is the hbox that will house the vbox which has the components */
    hbox = gtk_hbox_new(True, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, True, True, 5);

    /* the vbox is the main container that has all the components */
    thisWindowContainer = gtk_vbox_new(False, 0);
    gtk_box_pack_start(GTK_BOX(hbox), thisWindowContainer, True, True, 5);

    /* now put the primary top-level container into the window */
    gtk_container_add(GTK_CONTAINER(thisWindow), vbox);

    /* create the gui components that are above the notebook tabs */
    createTopForm();

    separator = gtk_hseparator_new ();
    gtk_box_pack_start(GTK_BOX(thisWindowContainer), separator, False, False, 3);

    thisNotebook = D_CreateNotebook(thisWindowContainer);
    g_signal_connect(GTK_NOTEBOOK(thisNotebook), "switch-page", G_CALLBACK(pageSwitchCb), NULL);

    /* sband 1 */
    createDrawingArea(SBAND1, SBAND1_VIEW,
                      SBand1DrawingArea,
                      SBAND1_TITLE,
                      (GCallback)sband1BlockageClickedCb,
                      &thisDataSBand1);

    /* sband 2 */
    createDrawingArea(SBAND2, SBAND2_VIEW,
                      SBand2DrawingArea,
                      SBAND2_TITLE,
                      (GCallback)sband2BlockageClickedCb,
                      &thisDataSBand2);

    /* kuband */
    createDrawingArea3(KUBAND, KUBAND_VIEW,
                       KuBandDrawingArea, KuBandLeftDrawingArea, KuBandBottomDrawingArea,
                       KUBAND_TITLE, (GCallback)kubandBlockageClickedCb, &thisDataKuBand,
                       &thisDataKuBand, &thisDataKuBandLeft, &thisDataKuBandBottom);

    /* station */
    createDrawingArea3(STATION, STATION_VIEW,
                       StationDrawingArea, StationLeftDrawingArea, StationBottomDrawingArea,
                       STATION_TITLE, (GCallback)stationBlockageClickedCb, &thisDataStation,
                       &thisDataStation, &thisDataStationLeft, &thisDataStationBottom);

    /* stop motion handlers */
    /* these handlers are only needed when the track */
    /* coordinates is enabled */
    D_BlockMotionHandlers(thisMotionHandlerData);
}

/**
 * Creates drawing for sband views
 *
 * @param coord current coordinate type
 * @param view curernt coordinate view
 * @param drawing_area draw to this
 * @param title tab name
 * @param toolbar_callback callback for this tab
 * @param expose_data data passed to the expose event
 */
static void createDrawingArea(gint coord, gint view, gint drawing_area, gchar *title, GCallback toolbar_callback, gint *expose_data)
{
    GtkWidget *sband_frame;

    thisWindowData[drawing_area].coordinate_type = coord;

    thisWindowData[drawing_area].toolbar = gtk_toolbar_new();
    thisWindowData[drawing_area].handle_box = gtk_handle_box_new();

    thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&sband_frame,
                                                                    title,
                                                                    thisWindowData[drawing_area].handle_box,
                                                                    NULL,
                                                                    thisWindowData[drawing_area].toolbar,
                                                                    NULL,
                                                                    -1,
                                                                    -1);
    D_AddBlockageItemsToToolbar(PREDICT, coord,
                                thisWindowData[drawing_area].toolbar,
                                &thisWindowData[drawing_area].tool_items,
                                title, toolbar_callback);

    gtk_widget_set_name(thisWindowData[drawing_area].drawing_area, title);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(exposeCb), expose_data);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resizeDrawingAreaCb), expose_data);

    gtk_widget_add_events(thisWindowData[drawing_area].drawing_area, (GDK_BUTTON_PRESS_MASK |
                                                                      GDK_BUTTON_RELEASE_MASK |
                                                                      GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[view].instance = thisWindowData[drawing_area].drawing_area;

    thisMotionHandlerData[view].handler_id = g_signal_connect(thisWindowData[drawing_area].drawing_area,
                                                              "motion-notify-event",
                                                              G_CALLBACK(D_DrawCoordinateTracking_cb),
                                                              NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "button-press-event",
                     G_CALLBACK(buttonPressCb), NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "motion-notify-event",
                     G_CALLBACK(motionCb), NULL);

    D_NotebookAdd(thisNotebook, title, &sband_frame);
}

/**
 * Creates drawing for kuband and station views
 *
 * @param coord current coordinate type
 * @param view curernt coordinate view
 * @param drawing_area main drawing area
 * @param left_drawing_area left drawing area
 * @param bottom_drawing_area bottom drawing area
 * @param title tab name
 * @param toolbar_callback callback for this tab
 * @param notify_data data used for the notify callback
 * @param expose_data data main drawing area's expose data used for expose callback
 * @param left_expose_data left drawing area's expose data used expose callback
 * @param bottom_expose_data bottom drawing area's expose data for expose callback
 */
static void createDrawingArea3(gint coord, gint view,
                               gint drawing_area, gint left_drawing_area, gint bottom_drawing_area,
                               gchar *title, GCallback toolbar_callback, gint *notify_data,
                               gint *expose_data, gint *left_expose_data, gint *bottom_expose_data)
{
    GtkWidget *left_frame;
    GtkWidget *bottom_frame;
    GtkWidget *center_frame;

    /* Left drawing area */
    thisWindowData[left_drawing_area].coordinate_type = coord;

    /* if toolbar is required, add extra space at the top of */
    /* the left frame */
    if (toolbar_callback)
    {
        thisWindowData[left_drawing_area].drawing_area = D_CreateDrawingAreaWithExtra(&left_frame,
                                                                                      VERTICAL_DIRECTION,
                                                                                      LABEL_AREA_WIDTH,
                                                                                      SMALL_ICON_SIZE*2+5);
    }
    else
    {
        thisWindowData[left_drawing_area].drawing_area = D_CreateDrawingArea(&left_frame,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             NULL,
                                                                             LABEL_AREA_WIDTH,
                                                                             -1);
    }

    gtk_widget_set_name(thisWindowData[left_drawing_area].drawing_area, "Left");

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(exposeCb), left_expose_data);

    g_signal_connect(thisWindowData[left_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resizeDrawingAreaCb), left_expose_data);

    /* Bottom drawing area */
    thisWindowData[bottom_drawing_area].coordinate_type = coord;

    thisWindowData[bottom_drawing_area].drawing_area = D_CreateDrawingAreaWithExtra(&bottom_frame,
                                                                                    HORIZONTAL_DIRECTION,
                                                                                    LABEL_AREA_WIDTH,
                                                                                    LABEL_AREA_HEIGHT);

    gtk_widget_set_name(thisWindowData[bottom_drawing_area].drawing_area, "Bottom");

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(exposeCb), bottom_expose_data);

    g_signal_connect(thisWindowData[bottom_drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resizeDrawingAreaCb), bottom_expose_data);

    /* main drawing area */
    thisWindowData[drawing_area].coordinate_type = coord;

    /* add toolbar if we have a callback for it */
    if (toolbar_callback)
    {
        thisWindowData[drawing_area].toolbar = gtk_toolbar_new();
        thisWindowData[drawing_area].handle_box = gtk_handle_box_new();

        if (coord == KUBAND)
        {
            thisWindowData[drawing_area].toolbar2 = gtk_toolbar_new();
            thisWindowData[drawing_area].handle_box2 = gtk_handle_box_new();
        }
    }

    if (coord == KUBAND)
        thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&center_frame,
                                                                        title,
                                                                        thisWindowData[drawing_area].handle_box,
                                                                        thisWindowData[drawing_area].handle_box2,
                                                                        thisWindowData[drawing_area].toolbar,
                                                                        thisWindowData[drawing_area].toolbar2,
                                                                        WINDOW_WIDTH-125,
                                                                        -1);
    else
        thisWindowData[drawing_area].drawing_area = D_CreateDrawingArea(&center_frame,
                                                                        title,
                                                                        thisWindowData[drawing_area].handle_box,
                                                                        NULL,
                                                                        thisWindowData[drawing_area].toolbar,
                                                                        NULL,
                                                                        WINDOW_WIDTH-125,
                                                                        -1);

    D_AddBlockageItemsToToolbar(PREDICT, coord,
                                thisWindowData[drawing_area].toolbar,
                                &thisWindowData[drawing_area].tool_items,
                                title,  (GCallback)toolbar_callback);

    if (coord == KUBAND)
    {
        D_AddBlockageItemsToToolbar(PREDICT, KUBAND2,
                                    thisWindowData[drawing_area].toolbar2,
                                    &thisWindowData[drawing_area].tool_items2,
                                    KUBAND2_TITLE, (GCallback)kuband2BlockageClickedCb);
    }

    gtk_widget_set_name(thisWindowData[drawing_area].drawing_area, title);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "expose-event",
                     G_CALLBACK(exposeCb), expose_data);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "configure-event",
                     G_CALLBACK(resizeDrawingAreaCb), expose_data);

    gtk_widget_add_events(thisWindowData[drawing_area].drawing_area, (GDK_BUTTON_PRESS_MASK |
                                                                      GDK_BUTTON_RELEASE_MASK |
                                                                      GDK_POINTER_MOTION_MASK));

    thisMotionHandlerData[view].instance = thisWindowData[drawing_area].drawing_area;

    thisMotionHandlerData[view].handler_id = g_signal_connect(thisWindowData[drawing_area].drawing_area,
                                                              "motion-notify-event",
                                                              G_CALLBACK(D_DrawCoordinateTracking_cb),
                                                              NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "button-press-event",
                     G_CALLBACK(buttonPressCb), NULL);

    g_signal_connect(thisWindowData[drawing_area].drawing_area, "motion-notify-event",
                     G_CALLBACK(motionCb), NULL);

    /* put it all together */
    D_NotebookAdd3(thisNotebook, title, &left_frame, &bottom_frame, &center_frame);

}

/**
 * This method creates the components found above the tab area. This includes
 * the event track data with buttons, current and solar point data with buttons,
 * and the gmt button and gmt data.
 */
static void createTopForm(void)
{
    GtkWidget *table;
    GtkWidget *pt_table;
    GtkWidget *label;
    GtkWidget *frame;
    GtkWidget *data_frame;
    GtkWidget *hbox;
    GtkWidget *hbox2;
    GtkWidget *vbox;

    /* put everything in a table so things are layout nice and neat */
    table = gtk_table_new(2, 4, False);
    {
        frame = gtk_frame_new(NULL);
        { /* row 1 & 2, col 1 */
            gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
            gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);

            /* build items top-down, left-right */
            /* e.g. track and time buttons are placed in vertical box */
            /* then the vertical box in placed in horizontal box */
            hbox = gtk_hbox_new(False, 3);
            {
                /* put track and time buttons in vertical box */
                vbox = gtk_vbox_new(True, 3);
                {
                    thisTrackButton = gtk_button_new_with_label("Track");
                    {
                        thisRightPressedHID = g_signal_connect(thisTrackButton, "pressed",
                                                               G_CALLBACK(pressedEventsCb),
                                                               GINT_TO_POINTER(StepNext));

                        /* this stops the 'repeat' handler from */
                        /* continously running */
                        g_signal_connect(thisTrackButton, "released",
                                         G_CALLBACK(releasedCb),
                                         &thisTimerInfo);

                        thisRightActivateHID = g_signal_connect(thisTrackButton, "activate",
                                                                G_CALLBACK(activateEventsCb),
                                                                GINT_TO_POINTER(StepNext));
                    }
                    gtk_box_pack_start(GTK_BOX(vbox), thisTrackButton, False, False, 0);

                    thisTimeButton = gtk_button_new_with_label(GetTimeName(AM_GetTimeType()));
                    {
                        g_signal_connect(thisTimeButton, "pressed",
                                         G_CALLBACK(timeCb),
                                         NULL);
                    }
                    gtk_box_pack_start(GTK_BOX(vbox), thisTimeButton, False, False, 0);
                }
                /* put vertical box in horizontal box */
                gtk_box_pack_start(GTK_BOX(hbox), vbox, False, False, 0);

                /* put event and time entries in vertical box */
                vbox = gtk_vbox_new(True, 3);
                {
                    thisTrackValue = D_CreateLabel(" ", LEFT_ALIGN, NULL);
                    {
                        /* set the size of the field to prevent expansion/shrinkage */
                        gtk_widget_set_size_request(thisTrackValue, 125, -1);
                        D_SetLabelColor(thisTrackValue, GOOD_VALUE);
                        data_frame = D_CreateDataFrame(thisTrackValue);
                    }
                    gtk_box_pack_start(GTK_BOX(vbox), data_frame, True, True, 0);

                    thisTimeEntry = gtk_entry_new();
                    {
                        /* set the name so the cursor color style can work */
                        gtk_widget_set_name(thisTimeEntry, "Predict.*.time-entry");

                        gtk_editable_set_editable(GTK_EDITABLE(thisTimeEntry), False);
                        g_signal_connect(thisTimeEntry, "activate",
                                         G_CALLBACK(activateTimeCb),
                                         NULL);
                        g_signal_connect(thisTimeEntry, "focus-out-event",
                                         G_CALLBACK(focusOutTimeCb),
                                         NULL);
                        g_signal_connect(thisTimeEntry, "key-press-event",
                                         G_CALLBACK(keyPressCb),
                                         NULL);
                        D_SetTextColor(thisTimeEntry, "Green", D_BgColor());
                    }
                    gtk_box_pack_start(GTK_BOX(vbox), thisTimeEntry, True, True, 0);
                }
                /* put vertical box in horizontal box */
                gtk_box_pack_start(GTK_BOX(hbox), vbox, False, False, 0);

                /* make arrow buttons */
                /* first group the arrows in horizontal box, then */
                /* place this box into a vertical box, which is placed */
                /* into the main horizontal box */
                vbox = gtk_vbox_new(True, 3);
                {
                    hbox2 = gtk_hbox_new(True, 0);
                    {
                        thisEventsLeftArrow = D_CreateArrowButton(GTK_ARROW_LEFT, GTK_SHADOW_OUT);
                        {
                            g_signal_connect(thisEventsLeftArrow, "pressed",
                                             G_CALLBACK(pressedEventsCb),
                                             GINT_TO_POINTER(StepPrev));
                            g_signal_connect(thisEventsLeftArrow, "released",
                                             G_CALLBACK(releasedCb),
                                             &thisTimerInfo);
                            g_signal_connect(thisEventsLeftArrow, "activate",
                                             G_CALLBACK(activateEventsCb),
                                             GINT_TO_POINTER(StepPrev));
                        }
                        gtk_box_pack_start(GTK_BOX(hbox2), thisEventsLeftArrow, False, False, 2);

                        thisEventsRightArrow = D_CreateArrowButton(GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
                        {
                            g_signal_connect(thisEventsRightArrow, "pressed",
                                             G_CALLBACK(pressedEventsCb),
                                             GINT_TO_POINTER(StepNext));
                            g_signal_connect(thisEventsRightArrow, "released",
                                             G_CALLBACK(releasedCb),
                                             &thisTimerInfo);
                            g_signal_connect(thisEventsRightArrow, "activate",
                                             G_CALLBACK(activateEventsCb),
                                             GINT_TO_POINTER(StepNext));
                        }
                        gtk_box_pack_start(GTK_BOX(hbox2), thisEventsRightArrow, False, False, 2);
                    }
                    gtk_box_pack_start(GTK_BOX(vbox), hbox2, False, False, 0);

                    /* more arrow buttons */
                    hbox2 = gtk_hbox_new(True, 0);
                    {
                        thisPointsLeftArrow = D_CreateArrowButton(GTK_ARROW_LEFT, GTK_SHADOW_OUT);
                        {
                            g_signal_connect(thisPointsLeftArrow, "pressed",
                                             G_CALLBACK(pressedPointsCb),
                                             GINT_TO_POINTER(StepPrev));
                            g_signal_connect(thisPointsLeftArrow, "released",
                                             G_CALLBACK(releasedCb),
                                             &thisTimerInfo);
                            g_signal_connect(thisPointsLeftArrow, "activate",
                                             G_CALLBACK(activatePointsCb),
                                             GINT_TO_POINTER(StepPrev));
                        }
                        gtk_box_pack_start(GTK_BOX(hbox2), thisPointsLeftArrow, False, False, 2);

                        thisPointsRightArrow = D_CreateArrowButton(GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
                        {
                            g_signal_connect(thisPointsRightArrow, "pressed",
                                             G_CALLBACK(pressedPointsCb),
                                             GINT_TO_POINTER(StepNext));
                            g_signal_connect(thisPointsRightArrow, "released",
                                             G_CALLBACK(releasedCb),
                                             &thisTimerInfo);
                            g_signal_connect(thisPointsRightArrow, "activate",
                                             G_CALLBACK(activatePointsCb),
                                             GINT_TO_POINTER(StepNext));
                        }
                        gtk_box_pack_start(GTK_BOX(hbox2), thisPointsRightArrow, False, False, 2);

                    }
                    gtk_box_pack_start(GTK_BOX(vbox), hbox2, False, False, 0);
                }
                gtk_box_pack_start(GTK_BOX(hbox), vbox, False, False, 0);
            }
            gtk_container_add(GTK_CONTAINER(frame), hbox);
        }
        gtk_table_attach(GTK_TABLE(table), frame, 0, 1, 0, 2, GAO_NONE, GAO_NONE, 2, 0);

        /* now build the current and solar points components */
        frame = gtk_frame_new(NULL);
        { /* row 1 & 2, col 2 */
            gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
            gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);

            pt_table = gtk_table_new(3, 3, False);
            {
                label = D_CreateLabel("Points:", RIGHT_ALIGN, NULL);
                gtk_table_attach(GTK_TABLE(pt_table), label, 0, 1, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                thisAZXEL_label = D_CreateLabel(AZ_LABEL, RIGHT_ALIGN, NULL);
                gtk_table_attach(GTK_TABLE(pt_table), thisAZXEL_label, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                label = D_CreateLabel(EL_LABEL, RIGHT_ALIGN, NULL);
                gtk_table_attach(GTK_TABLE(pt_table), label, 2, 3, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                label = D_CreateLabel("Current", RIGHT_ALIGN, NULL);
                gtk_table_attach(GTK_TABLE(pt_table), label, 0, 1, 1, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                thisCurrentPoint[AZ_POINT] = D_CreateLabel("-180", RIGHT_ALIGN, NULL);
                D_SetLabelColor(thisCurrentPoint[AZ_POINT], GOOD_VALUE);
                data_frame = D_CreateDataFrame(thisCurrentPoint[AZ_POINT]);
                gtk_table_attach(GTK_TABLE(pt_table), data_frame, 1, 2, 1, 2, GAO_NONE, GAO_NONE, 2, 0);

                thisCurrentPoint[EL_POINT] = D_CreateLabel("-180", RIGHT_ALIGN, NULL);
                D_SetLabelColor(thisCurrentPoint[EL_POINT], GOOD_VALUE);
                data_frame = D_CreateDataFrame(thisCurrentPoint[EL_POINT]);
                gtk_table_attach(GTK_TABLE(pt_table), data_frame, 2, 3, 1, 2, GAO_NONE, GAO_NONE, 2, 0);

                label = D_CreateLabel("Solar", RIGHT_ALIGN, NULL);
                gtk_table_attach(GTK_TABLE(pt_table), label, 0, 1, 2, 3, GAO_NONE, GAO_NONE, 2, 0);

                thisSolarPoint[AZ_POINT] = D_CreateLabel("-180", RIGHT_ALIGN, NULL);
                D_SetLabelColor(thisSolarPoint[AZ_POINT], GOOD_VALUE);
                data_frame = D_CreateDataFrame(thisSolarPoint[AZ_POINT]);
                gtk_table_attach(GTK_TABLE(pt_table), data_frame, 1, 2, 2, 3, GAO_NONE, GAO_NONE, 2, 0);

                thisSolarPoint[EL_POINT] = D_CreateLabel("-180", RIGHT_ALIGN, NULL);
                D_SetLabelColor(thisSolarPoint[EL_POINT], GOOD_VALUE);
                data_frame = D_CreateDataFrame(thisSolarPoint[EL_POINT]);
                gtk_table_attach(GTK_TABLE(pt_table), data_frame, 2, 3, 2, 3, GAO_NONE, GAO_NONE, 2, 0);

                /* set the size of the point labels: note on linux the first time the
                   the data is rendered, the values are a little to the left of the
                   right edge. once the fields are updated, the values justify correctly,
                   don't know why???. if the size request is eliminated, the fields
                   shrink and expand based on the contents of the data */
                gtk_widget_set_size_request(thisCurrentPoint[AZ_POINT], 35, -1);
                gtk_widget_set_size_request(thisCurrentPoint[EL_POINT], 35, -1);
                gtk_widget_set_size_request(thisSolarPoint[AZ_POINT], 35, -1);
                gtk_widget_set_size_request(thisSolarPoint[EL_POINT], 35, -1);
            }
            gtk_container_add(GTK_CONTAINER(frame), pt_table);
        }
        gtk_table_attach(GTK_TABLE(table), frame, 1, 2, 0, 2, GAO_NONE, GAO_NONE, 2, 0);

        /* build the search button */
        frame = gtk_frame_new("Search");
        { /* row 1 & 2, col 3 */
            GtkWidget *button = gtk_button_new_with_label("Track");

            g_signal_connect(button, "clicked",
                             G_CALLBACK(searchClickedCb),
                             NULL);

            gtk_container_set_border_width(GTK_CONTAINER(frame), 3);
            gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);

            hbox2 = gtk_hbox_new(True, 0);
            gtk_box_pack_start(GTK_BOX(hbox2), button, False, False, 3);

            gtk_container_add(GTK_CONTAINER(frame), hbox2);
        }
        gtk_table_attach(GTK_TABLE(table), frame, 2, 3, 0, 1, GAO_NONE, GAO_NONE, 0, 0);

        /* build the ku-band toggle button */
        thisKubandToggleButton = gtk_button_new_with_label(KU_TOGGLE_LABEL_2);
        g_signal_connect(thisKubandToggleButton, "pressed", G_CALLBACK(ku_toggle_pressed_cb), NULL);

        gtk_table_attach(GTK_TABLE(table), thisKubandToggleButton, 2, 3, 1, 2, GAO_NONE, GAO_NONE, 0, 0);
    }

    thisTopForm = gtk_frame_new(NULL);
    gtk_container_set_border_width (GTK_CONTAINER (thisTopForm), 0);
    gtk_frame_set_shadow_type(GTK_FRAME(thisTopForm), GTK_SHADOW_NONE);
    gtk_container_add(GTK_CONTAINER(thisTopForm), table);

    gtk_box_pack_start(GTK_BOX(thisWindowContainer), thisTopForm, False, False, 0);

    return;
}

/**
 * Sets the sensitivity of the generate predict option.
 * Only CATO users can generate predicts.
 */
static void enableGenPredictMenuItem(void)
{
    /* if not the manager, then disable the gen predict option */
    if (AM_GetAppMode() == PUBLIC)
    {
        GtkWidget *menu = gtk_ui_manager_get_widget (thisUImanager, "/"MENU_BAR"/"FILE_MENU"/"GENERATE_PREDICTION_DATA_MENU);
        if (menu != NULL)
        {
            gtk_widget_set_sensitive(menu, False);
        }
    }
}

/**
 * Sets the sensitivity of the ftp option. This option allows the user to send
 * the generated predicts to the external partners via the drop-boxes. The user
 * must be in 'Mission' mode; must be certified mode; and must be the manager.
 * For testing a config value has been setup to override these criteria.
 */
static void enableFtpMenuItem(void)
{
    gchar data[64];

    /* get the menu that holds the ftp item */
    GtkWidget *menu = gtk_ui_manager_get_widget (thisUImanager, "/"MENU_BAR"/"FILE_MENU"/"SEND_PREDICTION_DATA_MENU);

    /* is the override in effect ? */
    if (CP_GetConfigData(IAM_FORCE_FTP, data) == True)
    {
        gtk_widget_set_sensitive(menu, True);
    }
    else
    {
        /* by default set the menu item to False, */
        /* if the following criteria is meet, enable it */
        gtk_widget_set_sensitive(menu, False);

        /* the criteria for creating the ftp item to the menu */
        /* $ACTIVITY_TYPE = MIS */
        /* $sw_lvl = cert_apps */
        /* iam is running in manager mode */
        if (AM_GetActivityType() == ACTIVITY_TYPE_MIS)
        {
            if (AM_GetSwLvl() == SW_LVL_CERT_APPS)
            {
                if (AM_GetAppMode() == MANAGER)
                {
                    gtk_widget_set_sensitive(menu, True);
                }
            }
        }
    }
}

/**
 * Returns the window information, drawing area and pixmap, based on the current
 * coordinate type and the widget address. If the widget arg is null, then the
 * primary window data for the current coordinate is returned.
 *
 * @param widget compare with widget
 *
 * @return SBAND1, SBAND2, KUBAND, or STATION window data
 */
WindowData *PD_GetWindowDataByWidget(GtkWidget *widget)
{
    WindowData *windata = NULL;
    switch (thisCoordinateType)
    {
        case SBAND1:
            windata = &thisWindowData[SBand1DrawingArea];
            break;

        case SBAND2:
            windata = &thisWindowData[SBand2DrawingArea];
            break;

        case KUBAND:
        case KUBAND2:
            {
                if (widget == NULL)
                {
                    windata = &thisWindowData[KuBandDrawingArea];
                }
                else if (widget == thisWindowData[KuBandLeftDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[KuBandLeftDrawingArea];
                }
                else if (widget == thisWindowData[KuBandBottomDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[KuBandBottomDrawingArea];
                }
                else
                {
                    windata = &thisWindowData[KuBandDrawingArea];
                }
            }
            break;

        case STATION:
            {
                if (widget == NULL)
                {
                    windata = &thisWindowData[StationDrawingArea];
                }
                else if (widget == thisWindowData[StationLeftDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[StationLeftDrawingArea];
                }
                else if (widget == thisWindowData[StationBottomDrawingArea].drawing_area)
                {
                    windata = &thisWindowData[StationBottomDrawingArea];
                }
                else
                {
                    windata = &thisWindowData[StationDrawingArea];
                }
            }
            break;

        default:
            break;
    }

    return(windata);
}

/**
 * Returns the window information, drawing area and pixmap, based on the current
 * coordinate type. Only the primary drawing area is returned.
 *
 * @return SBAND1, SBAND2, KUBAND, or STATION window data
 */
static WindowData *getWindowDataByCoordinate(void)
{
    WindowData *windata = NULL;
    switch (thisCoordinateType)
    {
        case SBAND1:
            windata = &thisWindowData[SBand1DrawingArea];
            break;

        case SBAND2:
            windata = &thisWindowData[SBand2DrawingArea];
            break;

        case KUBAND:
        case KUBAND2:
            windata = &thisWindowData[KuBandDrawingArea];
            break;

        case STATION:
            windata = &thisWindowData[StationDrawingArea];
            break;

        default:
            break;
    }

    return(windata);
}

/**
 * Returns the window data based on the coordinate view we're on
 *
 * @param coord_view current coordinate view
 *
 * @return windowdata or NULL
 */
static WindowData *getWindowDataByView(gint coord_view)
{
    WindowData *windata = NULL;

    switch (coord_view)
    {
        case SBAND1_VIEW:
            windata = &thisWindowData[SBand1DrawingArea];
            break;

        case SBAND2_VIEW:
            windata = &thisWindowData[SBand2DrawingArea];
            break;

        case KUBAND_VIEW:
            windata = &thisWindowData[KuBandDrawingArea];
            break;

        case STATION_VIEW:
            windata = &thisWindowData[StationDrawingArea];
            break;

        default:
            break;
    }

    return(windata);
}

/**
 * Returns the active state of the currently displayed ku-band
 */
gboolean PD_GetKubandIsActive(void)
{
    return(thisKubandDisplayedIsActive);
}

/**
 * Sets the coordinate view, this is based on the current coordinate type
 */
static void setCoordinateView(void)
{
    switch (thisCoordinateType)
    {
        case SBAND1:
            thisCoordinateView = SBAND1_VIEW;
            break;

        case SBAND2:
            thisCoordinateView = SBAND2_VIEW;
            break;

        case KUBAND:
        case KUBAND2:
            thisCoordinateView = KUBAND_VIEW;
            break;

        case STATION:
            thisCoordinateView = STATION_VIEW;
            break;

        default:
            break;
    }
}

/**
 * Sets the pango font description to use. Usally set during the
 * resize operation.
 *
 * @param resize_font font resize information
 * @param font_desc the font description that has font family.
 */
static void setPangoFontDesc(ResizeFont *resize_font, PangoFontDescription *font_desc)
{
    gchar font_name[256];

    /* create font description for drawing methods */
    g_sprintf(font_name, "%s Bold %d",
              pango_font_description_get_family(font_desc),
              resize_font->previous_font_size);

    if (thisPangoFontDescription != NULL)
    {
        pango_font_description_free(thisPangoFontDescription);
    }
    thisPangoFontDescription = pango_font_description_from_string(font_name);
}

/**
 * Returns the current pango font description being used.
 *
 * @return PangoFontDescription*
 */
PangoFontDescription *PD_GetPangoFontDescription(void)
{
    return thisPangoFontDescription;
}

/**
 * Handles the active display. When coming here this dialog
 * is considered active.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False continues processing
 */
static gboolean focusInEventCb(GtkWidget *widget, GdkEventFocus *event, void * user_data)
{
    AM_SetActiveDisplay(DISPLAY_PD, True);
    return(False);
}

/**
 * Handles the active display. When coming here this dialog
 * is considered no longer active.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 * @return False continues processing
 */
static gboolean focusOutEventCb(GtkWidget *widget, GdkEventFocus *event, void * user_data)
{
    //AM_SetActiveDisplay(DISPLAY_PD, False);
    return(False);
}

/**
 * Using the resize callback for the window
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean resizeWindowCb(GtkWidget *widget, GdkEventConfigure *event, void * user_data)
{
    static gint font_size = -1;
    guint i;
    ResizeFont *resize_font = (ResizeFont *)user_data;
    if (thisIsIconified == False)
    {
        /* we're resizing the window */
        thisIsResizing = True;

        /* resize the font ? */
        resize_font->new_width = event->width;
        resize_font->new_height = event->height;
        resize_font->previous_font_size = D_ResizeFont(gtk_widget_get_name(thisWindow),
                                                       resize_font);
        resize_font->previous_width = event->width;
        resize_font->previous_height = event->height;

        /* create a pango font description for this new font size */
        if (resize_font->previous_font_size > 0 &&
            (resize_font->previous_font_size != font_size))
        {
            font_size = resize_font->previous_font_size;
            setPangoFontDesc(resize_font, widget->style->font_desc);
        }

        /* tell all windata pixmaps that the window has been resized */
        /* this will force a complete redraw in the expose event */
        for (i=0; i<MAX_DRAWING_AREAS; i++)
        {
            thisWindowData[i].is_resized = True;
        }
    }

    return(False);
}

/**
 * Using the configure callback to allocate/deallocate pixmaps
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean resizeDrawingAreaCb(GtkWidget *widget, GdkEventConfigure *event, void * data)
{
    /* we're resizing the window */
    WindowData *windata = PD_GetWindowDataByWidget(widget);
    if (windata != NULL)
    {
        /* create the pixmap data view for this drawing area */
        EventSizeData event_data = { event->x, event->y, event->width, event->height};
        makePixmapData(windata, &event_data, *(gint *)data);
    }

    /* let the system event handler execute */
    return False;
}

/**
 * expose event for the drawing area
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean exposeCb(GtkWidget *widget, GdkEventExpose *event, void * data)
{
    WindowData *windata = PD_GetWindowDataByWidget(widget);
    if (windata != NULL)
    {
        gint i, n;

        /* do we need to redraw the whole pixmap? */
        /* with the notebook, after the window has been render, resize events */
        /* are no longer sent, except to the currently visible page */
        if (windata->is_resized == True)
        {
            /* save the event state information */
            EventSizeData event_data = { event->area.x, event->area.y, event->area.width, event->area.height};

            /* reset the width, height, this will force a reallocation */
            /* of the drawing area */
            windata->width = 0;
            windata->height = 0;

            /* go make the pixmap data */
            makePixmapData(windata, &event_data, *(gint *)data);

            /* no longer resizing */
            windata->is_resized = False;
        }
        else
        {
            GdkGC *gc;
            GdkRectangle *rect;
            GdkGCValues _values;

            _values.function = GDK_COPY;
            gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

            /* just redraw the exposed region, this is more efficient than */
            /* redrawing the whole pixmap everytime */
            gdk_region_get_rectangles(event->region, &rect, &n);
            for (i=0; i<n; i++)
            {
                /* copy the bg pixmap to the tracks pixmap */
                gdk_draw_drawable(AM_Drawable(windata, DST_DRAWABLE),
                                  gc,
                                  AM_Drawable(windata, SRC_DRAWABLE),
                                  rect[i].x, rect[i].y,  /* dst x, y */
                                  rect[i].x, rect[i].y,  /* src x, y */
                                  rect[i].width, rect[i].height /* src width, height, use ALL */ );
            }
            g_free(rect);
            g_object_unref(gc);
        }
    }

    /* no longer resizing */
    thisIsResizing = False;

    /* continue the event handler */
    return False;
}

/**
 * Determines which coordinate view is currently being shown; calls the
 * appropriate draw method to get the main pixmap drawn. This pixmap will
 * later be used for redrawing in the expose event.
 *
 * @param windata the window data that has the drawing area
 * @param event_data the event data that has sizing information
 * @param which_drawing_area the drawing area to draw to
 */
static void makePixmapData(WindowData *windata, EventSizeData *event_data, gint which_drawing_area)
{
    /* verify the pixmap is still valid for the current window size */
    D_CreatePixmap(windata);

    PD_SetRedraw(True);

    windata->x = event_data->x;
    windata->y = event_data->y;
    windata->width = event_data->width;
    windata->height = event_data->height;

    switch (thisCoordinateType)
    {
        case SBAND1:
        case SBAND2:
            PD_DrawData(True);
            break;

        case KUBAND:
        case KUBAND2:
            {
                switch (which_drawing_area)
                {
                    default:
                    case KuBandDrawingArea:
                        PD_DrawData(True);
                        break;

                    case KuBandLeftDrawingArea:
                    case KuBandBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(&thisWindowData[which_drawing_area],
                                                PREDICT, which_drawing_area);
                        break;
                }
            }
            break;

        case STATION:
            {
                switch (which_drawing_area)
                {
                    default:
                    case StationDrawingArea:
                        PD_DrawData(True);
                        break;

                    case StationLeftDrawingArea:
                    case StationBottomDrawingArea:
                        PH_DrawLeftBottomPixmap(&thisWindowData[which_drawing_area],
                                                PREDICT, which_drawing_area);
                        break;
                }
            }
            break;

        default:
            break;
    }
}

/**
 * Processes the ku-band view toggle action from the menu.
 *
 * @param action that brought us here
 * @param the current radio button selection
 */
static void settings_menu_kuband_view_cb(GtkAction *action, GtkRadioAction *current)
{
    set_kuband_displayed(gtk_radio_action_get_current_value(current));
}

/**
 * Processes the ku-band view toggle action from the button.
 *
 * @param widget = the toggle button
 * @param data = user data
 */
static void ku_toggle_pressed_cb(GtkWidget *widget, void * data)
{
        // Toggle which Kuband is displayed
        set_kuband_displayed(thisKubandDisplayed == 1 ? 2 : 1);
}

/**
 * This method processes the button 1 press callback. When the user selects
 * somewhere on the drawing area display, the point coordinate finds the nearest
 * point, then updates the event data and display. When the button is pressed, a
 * flag is set indicating the button is pressed. The flag is cleared in the
 * button release event.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean buttonPressCb(GtkWidget *widget, GdkEventButton *event, void * user_data)
{
    if (event->button == 1)
    {
        markPosition(event->x, event->y);
    }

    return(False);
}

/**
 * This method processes the mouse motion event. This event is only processed
 * while the button 1 has been pressed and held.
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean motionCb(GtkWidget *widget, GdkEventMotion *event, void * user_data)
{
    gint x, y;
    GdkModifierType state;

    if (event->is_hint)
    {
        gdk_window_get_pointer (event->window, &x, &y, &state);
    }
    else
    {
        x = (gint)event->x;
        y = (gint)event->y;
        state = (GdkModifierType)event->state;
    }

    /* process only when the user has clicked the mouse */
    if (state & GDK_BUTTON1_MASK)
    {
        markPosition(event->x, event->y);
    }

    return(False);
}

/**
 * Marks the position of the point, then calls the draw method.
 *
 * @param x x position to mark
 * @param y y position to mark
 */
static void markPosition(gdouble x, gdouble y)
{
    GList *newPt;

    Point point;
    point.x = (gint)x;
    point.y = (gint)y;

    newPt = PDH_NearestPoint(getWindowDataByCoordinate(), PDH_GetData(), point);
    PDH_MarkPosition(newPt);
    PD_DrawData(True);
}

/**
 * Handles the notebook page switch
 *
 * @param widget the notebook widget
 * @param page curerent page number
 * @param page_num the page number we're switching to
 * @param user_data user data
 */
static void pageSwitchCb(GtkWidget *widget, GtkNotebookPage *page, guint page_num, void * user_data)
{
    GtkNotebook *notebook = GTK_NOTEBOOK(widget);
    gint old_page_num = gtk_notebook_get_current_page(notebook);

    if ((gint)page_num != old_page_num)
    {
        GtkWidget *menu;

        /**
         * Set the coordinate type and sku coordinate type
         * The expose event will determine which sku type
         */

        switch (page_num)
        {
            case 0:
                thisCoordinateType = SBAND1;
                thisSKUCoordinateType = SBAND1;
                break;

            case 1:
                thisCoordinateType = SBAND2;
                thisSKUCoordinateType = SBAND2;
                break;

            case 2:
                if (thisKubandDisplayed == 1)
                {
                    thisCoordinateType = KUBAND;
                    thisSKUCoordinateType = KUBAND;
                }
                else
                {
                    thisCoordinateType = KUBAND2;
                    thisSKUCoordinateType = KUBAND2;
                }
                break;

            case 3:
                thisCoordinateType = STATION;
                thisSKUCoordinateType = STATION;
                break;

            default:
                thisCoordinateType = UNKNOWN_COORD;
                thisSKUCoordinateType = UNKNOWN_COORD;
                break;
        }

        thisCoordinateView    = UNKNOWN_VIEW;

        /* Set the view we're on */
        setCoordinateView();

        /* update the menu items */
        D_UpdateBlockageItems(thisUImanager, PREDICT, thisCoordinateType, thisKubandDisplayed);

        /* do the kuband menu sensitivity */
        menu = gtk_ui_manager_get_widget (thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"SHOW_KUBAND_MENU);
        if (menu != NULL)
        {
            if (thisCoordinateType == KUBAND || thisCoordinateType == KUBAND2)
            {
                gtk_widget_set_sensitive(menu, True);
            }
            else
            {
                gtk_widget_set_sensitive(menu, False);
            }
        }

        /* change the dialog title */
        PD_SetDialogTitle();

        /* now change the AZ/XEL label */
        switch (thisCoordinateType)
        {
            case SBAND1:
            case SBAND2:
            case STATION:
                gtk_label_set_text(GTK_LABEL(thisAZXEL_label), AZ_LABEL);
                gtk_widget_hide(thisKubandToggleButton);
                break;

            case KUBAND:
            case KUBAND2:
                gtk_label_set_text(GTK_LABEL(thisAZXEL_label), XEL_LABEL);
                gtk_widget_show(thisKubandToggleButton);
                break;

            default:
                break;
        }

        /* now draw the view */
        PD_DrawData(True);
    }
}

/**
 * Handles the callback events for the file menu items
 *
 * @param action the action that got us here
 */
static void fileMenuActionCb(GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, REALTIME_DISPLAY_MENU) == 0)
        {
            if (RTD_Create() == True)
            {
                RTD_Open();
            }
        }
        else if (g_ascii_strcasecmp(action_name, PREDICT_EVENTS_MENU) == 0)
        {
            if (ED_Create() == True)
            {
                ED_Open();
            }
        }
        else if (g_ascii_strcasecmp(action_name, PRINT_PREDICT_MENU) == 0)
        {
            PRP_PrintPredict(thisWindow->window,
                             thisWindow->allocation.width,
                             thisWindow->allocation.height);
        }
        else if (g_ascii_strcasecmp(action_name, PRINT_PREDICT_BLOCKAGE_MENU) == 0)
        {
            /* mark the currently selected predicts for print */
            setDisplayPrintFlag(DISPLAY_PRT);

            /* print the blockage report */
            PRP_PrintPredictBlockage(PDH_GetData());

            /* now clear the print flags */
            setDisplayPrintFlag(DISPLAY_PRT);
        }
        else if (g_ascii_strcasecmp(action_name, GENERATE_PREDICTION_DATA_MENU) == 0)
        {
            MUD_Create(GTK_WINDOW(thisWindow));
            MUD_Open();
        }
        else if (g_ascii_strcasecmp(action_name, CHECK_DATA_AUTOMATICALLY_MENU) == 0)
        {
            AUD_Create(GTK_WINDOW(thisWindow));
            AUD_Open();
        }
        else if (g_ascii_strcasecmp(action_name, SEND_PREDICTION_DATA_MENU) == 0)
        {
            FTP_Create(GTK_WINDOW(thisWindow));
            FTP_Open();
        }
        else if (g_ascii_strcasecmp(action_name, CLOSE_MENU) == 0)
        {
            D_CloseDialog(PREDICT);
        }
        else
        {
        }
    }
}

/**
 * Sets or clears the display flag in the predict set.
 *
 * @param display the flag value used for toggling the state
 */
static void setDisplayPrintFlag(gint display)
{
    GList *glist = PDH_GetData();

    for (; glist != NULL; glist = g_list_next(glist))
    {
        PredictSet *ps = (PredictSet *)glist->data;

        if (ps->displayed & DISPLAY_PD)
        {
            /* this will toggle the value */
            ps->displayed ^= display;
        }
    }
}

/**
 * Processes the setting menu items selections
 *
 * @param action the action that got us here
 */
static void settingsMenuActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, SET_BASE_PET_MENU) == 0)
        {
            PET_Create(GTK_WINDOW(thisWindow));
            PET_Open();
        }

        else if (g_ascii_strcasecmp(action_name, COORDINATE_TRACKING_MENU) == 0)
        {
            thisCoordinateTracking = ! thisCoordinateTracking;
            if (thisCoordinateTracking == True)
            {
                D_UnblockMotionHandlers(thisMotionHandlerData);
            }
            else
            {
                thisTrackingData.windata = NULL;
                D_BlockMotionHandlers(thisMotionHandlerData);
            }
        }

        else if (g_ascii_strcasecmp(action_name, STEP_BY_MENU) == 0)
        {
            GtkWidget *menu;
            GtkWidget *child;
            if (thisStepBy == StepByMinute)
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"SETTINGS_MENU"/"STEP_BY_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), STEP_BY_DATA);
                }
            }
            else
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"SETTINGS_MENU"/"STEP_BY_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), STEP_BY_MINUTE);
                }
            }
            thisStepBy ^= StepByMinute;
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_SOLAR_TRACE_MENU) == 0)
        {
            thisShowSolarTrace = ! thisShowSolarTrace;
            PD_SetRedraw(True);
            PD_DrawData(False);
            updateEventName();
        }

        else if (g_ascii_strcasecmp(action_name, CHANGE_ANTENNA_LOCATION_MENU) == 0)
        {
            AD_Create(GTK_WINDOW(thisWindow));
            AD_Open();
        }

        else if (g_ascii_strcasecmp(action_name, CHANGE_COVERAGE_DIRECTORY_MENU) == 0)
        {
            PDH_GetCoverage(GTK_WINDOW(thisWindow));
        }

        else if (g_ascii_strcasecmp(action_name, RELOAD_CONFIG_FILE_MENU) == 0)
        {
            /* this will reload the config data */
            CP_Destructor();
            CP_Constructor();

            /* this will reload the isp limits */
            IH_Destructor();
            IH_Constructor();
        }

        else
        {
        }
    }
}

/**
 * Handles the dynamic structure change action callback
 *
 * @param action what got us here
 */
static void structureChangeActionCb(GtkAction *action, GtkRadioAction *current)
{
    if (thisIsManualStructureChange == False)
    {
        const gchar *action_name = gtk_action_get_name(GTK_ACTION(current));
        if (action_name != NULL)
        {
            gchar *structure = g_path_get_basename(action_name);
            DD_ChangeStructure(structure);
            RTD_ChangeStructure(action_name);
            g_free(structure);
        }
    }
}

/**
 * Changes the structure selection in the menu pulldown. Its assumed
 * that the DD_ChangeStructure has already been called, so we want
 * to disable the structure_change callback.
 *
 * @param action_name the structure to change to
 */
void PD_ChangeStructure(const gchar *action_name)
{
    if (thisStructureActionGroup != NULL)
    {
        GtkAction *action = gtk_action_group_get_action(thisStructureActionGroup, action_name);
        if (action)
        {
            thisIsManualStructureChange = True;
            gtk_toggle_action_set_active(GTK_TOGGLE_ACTION(action), True);
            thisIsManualStructureChange = False;
        }
    }
}

/**
 * Processes the blockage selection menu-item
 *
 * @param action the action that got us here
 */
static void blockageMenuActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, DISPLAY_CONE_MENU) == 0)
        {
            /* change the text in the menu drop down */
            GtkWidget *menu;
            GtkWidget *child;
            if (thisShowConeLos == eSHOW_CONE)
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"DISPLAY_CONE_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), SHOW_LOS);
                }
            }
            else
            {
                menu = gtk_ui_manager_get_widget(thisUImanager, "/"MENU_BAR"/"BLOCKAGE_MENU"/"DISPLAY_CONE_MENU);
                if (menu != NULL)
                {
                    child = GTK_BIN(menu)->child;
                    gtk_label_set_text (GTK_LABEL(child), SHOW_CONE);
                }
            }
            thisShowConeLos = ! thisShowConeLos;

            OM_ShowCone(PREDICT, thisShowConeLos);
            PD_SetRedraw(True);
            PD_DrawData(False);
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_LIMITS_MENU) == 0)
        {
            thisShowLimit = ! thisShowLimit;

            /* for the limits mask, the coordinate type is not important */
            /* except, we don't want to pass in one of the dual coordinate */
            /* types, that would effectively do nothing. */
            setBlockageMask(SBAND1, MF_GetLimitMaskNum(PREDICT, SBAND1));
            PD_SetRedraw(True);
            PD_DrawData(False);
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_SHUTTLE_DOCKED_MENU) == 0)
        {
            thisShowShuttle = ! thisShowShuttle;

            OM_ShowShuttle(PREDICT, thisShowShuttle);
            PD_SetRedraw(True);
            PD_DrawData(False);
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_JOINT_ANGLES_MENU) == 0)
        {
            JAD_Create(GTK_WINDOW(thisWindow));
            JAD_Open();
        }

        else if (g_ascii_strcasecmp(action_name, SHOW_KUBAND_MENU) == 0)
        {
            PKMD_Create(GTK_WINDOW(thisWindow));
            PKMD_Open();
        }

        else
        {
        }
    }
}

/**
 * Processes the help selection menu-item
 *
 * @param action the action that got us here
 */
static void helpMenuActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    if (action_name != NULL)
    {
        if (g_ascii_strcasecmp(action_name, HELP_HELP_MENU) == 0)
        {
            HH_HelpIam();
        }

        else if (g_ascii_strcasecmp(action_name, HELP_ABOUT_MENU) == 0)
        {
            HH_AboutIam(thisWindow);
        }

        else if (g_ascii_strcasecmp(action_name, HELP_GTK_MENU) == 0)
        {
            HH_AboutGTK(thisWindow);
        }

        else
        {
        }
    }
}

static void set_kuband_view_to_start()
{

        SymbolRec *KuTRC1 = IS_GetSymbol(IS_KuTRC1);
        SymbolRec *KuTRC2 = IS_GetSymbol(IS_KuTRC2);
        gint kuband_view_to_start = 0;
        gchar kuband_start_value[32] = {0};
        gchar error_message[100];

        // Set initial state of ku-band toggle button (shown/hidden)
        if (thisCoordinateType == KUBAND || thisCoordinateType == KUBAND2)
                gtk_widget_show(thisKubandToggleButton);
        else
                gtk_widget_hide(thisKubandToggleButton);

        if (KuTRC1 != NULL && KuTRC2 != NULL)  // NULL if ISP option is off
        {
            if (KuTRC1->value == 1 && KuTRC2->value == 0)
                kuband_view_to_start = 1;
            else if (KuTRC1->value == 0 && KuTRC2->value == 1)
                kuband_view_to_start = 2;
        }

        if (kuband_view_to_start == 0)
        {
            if (CP_GetConfigData(IAM_KUBAND_VIEW_STARTUP, kuband_start_value) == False)
            {
                /* couldn't find. */
                /* ignore and just let the default behavior happen */
                WarningMessage(AM_GetActiveDisplay(), "Unable to determine active or default ku-band",
                               "Unable to determine active ku-band from ISP and unable to locate IAM_KUBAND_VIEW_STARTUP in the iam.xml file: The application is defaulting to ku-band 1.");
                g_message("%s not defined in the iam.xml file!", IAM_KUBAND_VIEW_STARTUP);
                kuband_view_to_start = 1;  // We have no idea what to use, so just use 1.
            }
            else
            {
                // If we've gotten here, then use the default
                sprintf(error_message, "The default ku-band specified in the xml file will be used: ku-band %s", kuband_start_value);
                WarningMessage(AM_GetActiveDisplay(), "Unable to determine active ku-band.", error_message);
                g_message("Unable to determine active ku-band.  Using the default %s.",kuband_start_value);
                if (strcmp(kuband_start_value,"2") == 0)
                    kuband_view_to_start = 2;
                else
                    kuband_view_to_start = 1;
            }
        }
        set_kuband_displayed(kuband_view_to_start);
}

static void set_kuband_displayed(gint kuband_to_display)
{
        GtkAction *action;
        SymbolRec *KuTRC1 = IS_GetSymbol(IS_KuTRC1);
        SymbolRec *KuTRC2 = IS_GetSymbol(IS_KuTRC2);
        static gboolean in_kuband_displayed = True;

        if (in_kuband_displayed == True)
        {
            // Set the displayed Kuband
            thisKubandDisplayed = kuband_to_display;

            // Set the button label
            GtkWidget *mywidget = gtk_bin_get_child(GTK_BIN(thisKubandToggleButton));
            gtk_label_set_text(GTK_LABEL(mywidget), (thisKubandDisplayed == 1 ?
                               KU_TOGGLE_LABEL_1 : KU_TOGGLE_LABEL_2));

            // Modify the background color if required & ISP available.  Yellow = non-active Kuband
            if (KuTRC1 != NULL && KuTRC2 != NULL)
            {
                if ((kuband_to_display == 1 && KuTRC1->value == 0)  ||
                    (kuband_to_display == 2 && KuTRC2->value == 0)  ||
                    (KuTRC1->value == 1 && KuTRC2->value == 1))
                {
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &yellow);
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightyellow);
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &yellow);
                        thisKubandDisplayedIsActive = False;
                }
                else
                {
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &gray);
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightgray);
                        gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &gray);
                        thisKubandDisplayedIsActive = True;
                }
            }
            else
            {
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_NORMAL, &yellow);
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_PRELIGHT, &lightyellow);
                gtk_widget_modify_bg (GTK_WIDGET(thisKubandToggleButton), GTK_STATE_ACTIVE, &yellow);
                thisKubandDisplayedIsActive = False;
            }

            // Set coordinate type for drawing area
            if (kuband_to_display == 1)
            {
                if (thisCoordinateType == KUBAND2) thisCoordinateType = KUBAND;
                thisWindowData[KuBandDrawingArea].coordinate_type = KUBAND;
                gtk_widget_show(thisWindowData[KuBandDrawingArea].handle_box);
                gtk_widget_hide(thisWindowData[KuBandDrawingArea].handle_box2);
                thisWindowData[KuBandLeftDrawingArea].coordinate_type = KUBAND;
                thisWindowData[KuBandBottomDrawingArea].coordinate_type = KUBAND;

                MF_ForceRedraw (PREDICT, KUBAND_VIEW);
                PD_SetRedraw(True);
                PD_DrawData(True);
            }
            else
            {
                if (thisCoordinateType == KUBAND) thisCoordinateType = KUBAND2;
                thisWindowData[KuBandDrawingArea].coordinate_type = KUBAND2;
                gtk_widget_hide(thisWindowData[KuBandDrawingArea].handle_box);
                gtk_widget_show(thisWindowData[KuBandDrawingArea].handle_box2);
                thisWindowData[KuBandLeftDrawingArea].coordinate_type = KUBAND2;
                thisWindowData[KuBandBottomDrawingArea].coordinate_type = KUBAND2;

                MF_ForceRedraw (PREDICT, KUBAND_VIEW);
                PD_SetRedraw(True);
                PD_DrawData(True);
            }

            // Gray out the "Use Realtime" button on the Predict Ku Mask Display
            if (PD_GetKubandIsActive())
                PKD_EnableUseRealtime(True);
            else
                PKD_EnableUseRealtime(False);

            // Update Blockage menus
            D_UpdateBlockageItems(thisUImanager, PREDICT, thisCoordinateType, thisKubandDisplayed);
	
            // Set the menu item
            action = gtk_ui_manager_get_action(thisUImanager, kuband_to_display == 1 ?
                                 "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_KUBAND_VIEW"/"CHANGE_KUBAND_VIEW_TO_ONE_MENU :
                                 "/"MENU_BAR"/"SETTINGS_MENU"/"CHANGE_KUBAND_VIEW"/"CHANGE_KUBAND_VIEW_TO_TWO_MENU);

            if (action)
            {
                in_kuband_displayed = False;

                /* activate the action */
                gtk_action_activate(action);

                in_kuband_displayed = True;
            }
        }
}

/**
 * Draw the coordinate points. If the window data structure is null, then used
 * the cached values; this call is probably coming from the main draw routine -
 * this will restore the text on the screen. The main draw routine will clear
 * the main drawing area before drawing again.
 *
 * @param wdata window data
 * @param str the message string to draw
 * @param x x location to start draw
 * @param y y location to start draw
 */
void PD_DrawCoordPoints(WindowData *wdata, gchar *str, gint x, gint y)
{
    GdkGC *gc;
    PangoFontDescription *pango_font_desc;
    PangoLayout *layout;

    /* save values for redraw */
    if (wdata != NULL)
    {
        thisTrackingData.windata = wdata;
        g_stpcpy(thisTrackingData.message, str);

        thisTrackingData.x_loc = x;
        thisTrackingData.y_loc = y;
    }

    /* only draw if window data is valid */
    if (thisTrackingData.windata != NULL)
    {
        pango_font_desc = pango_font_description_copy(thisPangoFontDescription);
        layout = gtk_widget_create_pango_layout(thisTrackingData.windata->drawing_area, "");
        pango_layout_set_text(layout, thisTrackingData.message, -1);
        pango_layout_set_font_description(layout, pango_font_desc);
        pango_font_description_free(pango_font_desc);

        gc = CH_SetColorGC(thisTrackingData.windata->drawing_area, CH_GetColorName(GC_WHITE));

        gdk_draw_layout(AM_Drawable(thisTrackingData.windata, DST_DRAWABLE), gc,
                        thisTrackingData.x_loc, thisTrackingData.y_loc, layout);

        g_object_unref(gc);
        g_object_unref(layout);
    }
}

/**
 * Processes the sband1 blockage mask callback
 *
 * @param tool_button calling button
 * @param user_data user data
 */
static void sband1BlockageClickedCb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, SBAND1_MENU, name);
}

/**
 * Processes the sband2 blockage mask callback
 *
 * @param tool_button calling button
 * @param user_data user data
 */
static void sband2BlockageClickedCb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, SBAND2_MENU, name);
}

/**
 * Processes the kuband callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void kubandBlockageClickedCb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, KUBAND_MENU, name);
}

/**
 * Processes the kuband callback for the blockage mask from the toolbar
 * button. This method calls the action handler that will ultimately
 * set the mask itself. The sequence eliminates an endless loop caused
 * when evoking the handlers.
 *
 * @param tool_button the button that got us here
 * @param user_data not used
 */
static void kuband2BlockageClickedCb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, KUBAND2_MENU, name);
}

/**
 * Processes the station blockage mask callback
 *
 * @param tool_button calling button
 * @param user_data user data
 */
static void stationBlockageClickedCb(GtkToolButton *tool_button, void * user_data)
{
    const gchar *name = gtk_tool_button_get_label(tool_button);
    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR, BLOCKAGE_MENU, STATION_MENU, name);
}

/**
 * Processes the sband1 blockage mask callback
 *
 * @param action the blockage mask
 */
static void sband1BlockageActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    setBlockageMaskByName(SBAND1, g_path_get_basename(action_name));
}

/**
 * Processes the sband2 blockage mask callback
 *
 * @param action the blockage mask
 */
static void sband2BlockageActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    setBlockageMaskByName(SBAND2, g_path_get_basename(action_name));
}

/**
 * Processes the kuband blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void kubandBlockageActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    setBlockageMaskByName(KUBAND, g_path_get_basename(action_name));
}

/**
 * Processes the kuband blockage mask callback from the menubar. This
 * method will ultimately set the visual for the toolbar buttons then
 * set the mask.
 *
 * @param action the blockage mask
 */
static void kuband2BlockageActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    setBlockageMaskByName(KUBAND2, g_path_get_basename(action_name));
}

/**
 * Processes the station blockage mask callback
 *
 * @param action the blockage mask
 */
static void stationBlockageActionCb (GtkAction *action)
{
    const gchar *action_name = gtk_action_get_name(action);
    setBlockageMaskByName(STATION, g_path_get_basename(action_name));
}

/**
 * Gets the blockage mask for the specified coordinate and mask name
 *
 * @param coord the coordinate
 * @param mask_name the name of the mask
 */
static void setBlockageMaskByName(gint coord, const gchar *mask_name)
{
    guint i;
    for (i=0; i<MF_GetMaskFileCount(PREDICT, coord); i++)
    {
        gchar *blockage_mask = MF_GetMaskName(PREDICT, coord, i);
        if (g_ascii_strcasecmp(mask_name, blockage_mask) == 0)
        {
            /* set the blockage mask itself */
            setBlockageMask(coord, (gint)i);

            /* set the toolbar buttons visual */
            setButtonMaskVisual(coord, i, mask_name);
            break;
        }
    }

    /* now draw masks */
    PD_SetRedraw(True);
    PD_DrawData(False);
}

/**
 * Determines what the blockage mask is, then sets it and calls the
 * method to draw it
 *
 * @param coord current coordinate type
 * @param mask_num mask number
 */
static void setBlockageMask(gint coord, gint mask_num)
{

    gboolean visible = MF_GetVisibility(PREDICT, coord, mask_num);

    if (MF_IsLimitMask(PREDICT, coord, mask_num) == True)
    {
        MF_SetVisibility(PREDICT, SBAND1, MF_GetLimitMaskNum(PREDICT, SBAND1), visible ? False : True);
        MF_SetVisibility(PREDICT, SBAND2, MF_GetLimitMaskNum(PREDICT, SBAND2), visible ? False : True);
        MF_SetVisibility(PREDICT, KUBAND, MF_GetLimitMaskNum(PREDICT, KUBAND), visible ? False : True);
        MF_SetVisibility(PREDICT, KUBAND2, MF_GetLimitMaskNum(PREDICT, KUBAND2), visible ? False : True);
        MF_SetVisibility(PREDICT, STATION, MF_GetLimitMaskNum(PREDICT, STATION), visible ? False : True);
    }
    else
    {
        MF_SetVisibility(PREDICT, coord, mask_num, visible ? False : True);
    }
}

/**
 * Sets the masks for the given coordinate. The config_keyword defines the list
 * of masks to show or not to show. The stored string represents a bitwise value
 * where '0' indicates no show while a '1' indicates show the mask.
 *
 * @param coord which coordinate
 * @param config_keyword lookup this value, the expected value is
 * a bitwise representation of the masks to enable
 */
static void setMasksEnabled(gint coord, gchar *config_keyword)
{
    gchar mask_value[32] = {0};
    guint i;

    if (CP_GetConfigData(config_keyword, mask_value) == False)
    {
        /* couldn't find. */
        /* ignore and just let the default behavior happen */
        g_message("%s not defined in the config file!", config_keyword);
    }
    else
    {
        /* search thru the masks */
        for (i=0; i<MF_GetMaskFileCount(PREDICT, coord); i++)
        {
            /* should the mask be enabled */
            if (mask_value[i] == '1')
            {
                /* ignore the limit mask */
                if (MF_IsLimitMask(PREDICT, coord, i) == False)
                {
                    gchar *mask_name = MF_GetMaskName(PREDICT, coord, i);
                    gchar *menu = (coord == SBAND1 ? SBAND1_MENU :
                                   coord == KUBAND ? KUBAND_MENU :
                                   coord == KUBAND2 ? KUBAND2_MENU :
                                   coord == SBAND2 ? SBAND2_MENU : STATION_MENU);

                    D_SetBlockageMaskByAction(thisUImanager, MENU_BAR,
                                              BLOCKAGE_MENU, menu,
                                              mask_name);
                }
            }
        }
    }
}

/**
 * Processes the activate callback for the points arrow button. The user data
 * contains which arrow (direction) was selected. This method then calls the
 * method to set the points in the interface.
 *
 * @param widget the button widget
 * @param user_data has the arrow direction value
 */
static void activatePointsCb(GtkWidget *widget, void * user_data)
{
    setPoints(GPOINTER_TO_INT(user_data));
}

/**
 * This method processes the arrow buttons for the points selection.
 *
 * @param widget calling widget
 * @param user_data user data
 */
static void pressedPointsCb(GtkWidget *widget, void * user_data)
{
    // timer handles the case of holding the button down
    thisTimerInfo.timer_state = REPEAT_STAGE0_DELAY;
    thisTimerInfo.user_data = user_data;
    thisTimerInfo.timer_count = 0;
    thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE0_DELAY, (GSourceFunc)pointsTimerFunc, NULL);
}

/**
 * While the arrow button is held down, the timer will continue processing the
 * 'psuedo' press event by coming here. Once the button becomes insensitive, the
 * timer is removed.
 *
 * @param user_data has the timer info
 *
 * @return False to keep the timer going, False to stop
 */
static gint pointsTimerFunc(void *user_data)
{
    gint retval = False;

    /* process request */
    setPoints(GPOINTER_TO_INT(thisTimerInfo.user_data));

    /* repeat state machine */
    switch (thisTimerInfo.timer_state)
    {
        /* enable slow repeat */
        case REPEAT_STAGE0_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_STAGE1_DELAY;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE1_DELAY, (GSourceFunc)pointsTimerFunc, NULL);
            break;

            /* is it time for a fast repeat ? */
        case REPEAT_STAGE1_DELAY:
            if (thisTimerInfo.timer_count++ > 50)
                thisTimerInfo.timer_state = REPEAT_STAGE2_DELAY;
            retval = True;
            break;

            /* start really fast repeat */
        case REPEAT_STAGE2_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_TIMER_CONTINUE;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE2_DELAY, (GSourceFunc)pointsTimerFunc, NULL);
            break;

            /* place holder */
        case REPEAT_TIMER_CONTINUE:
            retval = True;
            break;

            /* shutdown timer - comes from release callback */
        case REPEAT_TIMER_STOP:
        default:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_id = 0;
            thisTimerInfo.timer_state = REPEAT_TIMER_STOP;
            thisTimerInfo.timer_count = 0;
            thisTimerInfo.user_data = NULL;
            return False;
    }

    /* do we need to release timer resources */
    /* this method checks to see we're out of points on the last event */
    if ((retval = setPointsRepeatTimer()) == False)
    {
        if (thisTimerInfo.timer_id)
            g_source_remove(thisTimerInfo.timer_id);
        thisTimerInfo.timer_id = 0;
        thisTimerInfo.timer_state = REPEAT_TIMER_STOP;
        thisTimerInfo.timer_count = 0;
        thisTimerInfo.user_data = NULL;
    }

    return retval;
}

/**
 * Does the actual setting of the points within an event for the display. This
 * method determines which direction to move the data point on the display. It
 * also automatically moves the event when the end of the points occurs. When
 * moving off the point list, the next or previous event is loaded.
 *
 * @param direction which direction to step the data
 */
static void setPoints(guint direction)
{
    gboolean show;

    /* get next point */
    GList *nxtPt = PDH_StepPoint(direction, thisStepBy);

    /* make this the current point */
    PDH_MarkPosition(nxtPt);

    /* check if next event needs to be loaded */
    /* any more points? */
    show = PDH_HasPoint(direction);

    /* move to next event */
    if (show == False)
    {
        /* event found? */
        if (updateEventTracks(direction) == True)
        {
            /* move to next event */
            setEvents(direction);
        }
    }

    /* draw the data */
    PD_DrawData(True);
}

/**
 * Determines if the repeat timer should continue or not. If the timer is on and
 * the button that represents the direction that is being processed becomes
 * insensitive, then turn off the timer.
 *
 * @return False if timer is stopped, otherwise True
 */
static gboolean setPointsRepeatTimer(void)
{
    gint direction = GPOINTER_TO_INT(thisTimerInfo.user_data);
    if (direction == StepNext)
    {
        return GTK_WIDGET_IS_SENSITIVE(thisPointsRightArrow);
    }

    return GTK_WIDGET_IS_SENSITIVE(thisPointsLeftArrow);
}

/**
 * This method processes the arrow buttons activate callback. This is activated
 * by selecting the carriage return. The user data has the direction which to
 * move the event.
 *
 * @param widget the event button widget
 * @param user_data which direction to move the event.
 */
static void activateEventsCb(GtkWidget *widget, void * user_data)
{
    setEvents(GPOINTER_TO_INT(user_data));
}

/**
 * This method processes the arrow buttons for the events selection.
 *
 * @param widget calling widget
 * @param user_data user data
 */
static void pressedEventsCb(GtkWidget *widget, void * user_data)
{
    // timer handles the case of holding the button down
    thisTimerInfo.timer_state = REPEAT_STAGE0_DELAY;
    thisTimerInfo.timer_count = 0;
    thisTimerInfo.user_data = user_data;
    thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE0_DELAY, (GSourceFunc)eventsTimerFunc, NULL);
}

/**
 * While the arrow button is held down, the timer will continue processing the
 * 'psuedo' press event by coming here. If the button becomes insensitive, then
 * the timer is removed.
 *
 * @param user_data has the timer info
 *
 * @return True to keep the timer going, False to stop the timer
 */
static gint eventsTimerFunc(void *user_data)
{
    gint retval = False;

    /* process request */
    setEvents(GPOINTER_TO_INT(thisTimerInfo.user_data));

    /* repeat state machine */
    switch (thisTimerInfo.timer_state)
    {
        /* enable slow repeat */
        case REPEAT_STAGE0_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_STAGE1_DELAY;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE1_DELAY, (GSourceFunc)eventsTimerFunc, NULL);
            break;

            /* is it time for a fast repeat ? */
        case REPEAT_STAGE1_DELAY:
            if (thisTimerInfo.timer_count++ > 50)
                thisTimerInfo.timer_state = REPEAT_STAGE2_DELAY;
            retval = True;
            break;

            /* start really fast repeat */
        case REPEAT_STAGE2_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_TIMER_CONTINUE;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE2_DELAY, (GSourceFunc)eventsTimerFunc, NULL);
            break;

            /* place holder */
        case REPEAT_TIMER_CONTINUE:
            retval = True;
            break;

            /* shutdown timer - comes from release callback */
        case REPEAT_TIMER_STOP:
        default:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_id = 0;
            thisTimerInfo.timer_state = 0;
            thisTimerInfo.timer_count = 0;
            thisTimerInfo.user_data = NULL;
            return False;
    }

    /* do we need to release timer resources */
    /* this method checks to see we're out of events */
    if ((retval = setEventsRepeatTimer()) == False)
    {
        if (thisTimerInfo.timer_id)
            g_source_remove(thisTimerInfo.timer_id);
        thisTimerInfo.timer_id = 0;
        thisTimerInfo.timer_state = 0;
        thisTimerInfo.timer_count = 0;
        thisTimerInfo.user_data = NULL;
    }

    return retval;
}

/**
 * Determines if the repeat timer should continue or not. If the timer is on and
 * the button that represents the direction that is being processed becomes
 * insensitive, then turn off the timer.
 *
 * @return False if timer should be stopped, otherwise True to continue
 */
static gboolean setEventsRepeatTimer(void)
{
    gint direction = GPOINTER_TO_INT(thisTimerInfo.user_data);
    if (direction == StepNext)
    {
        return GTK_WIDGET_IS_SENSITIVE(thisEventsRightArrow);
    }

    return GTK_WIDGET_IS_SENSITIVE(thisEventsLeftArrow);
}

/**
 * When the button (arrows) are released, turn off the repeat timer. Also if the
 * arrow buttons are no longer accesible, remove the timer.
 *
 * @param widget button widget
 * @param user_data has the timer info
 */
static void releasedCb(GtkWidget *widget, void *user_data)
{
    thisTimerInfo.timer_state = REPEAT_TIMER_STOP;
}

/**
 * Processes the key press event for the arrow buttons. Allows the user to
 * select the right or left arrow keys on the keyboard for processing the event
 * list.
 *
 * @param widget the arrow button widget
 * @param event event data, has the direction
 * @param user_data has the direction
 *
 * @return True, stops the callback from continuing
 */
static gboolean keyPressEventCb(GtkWidget *widget, GdkEventKey *event, void * user_data)
{
    gboolean retval = False;

    /* if control is pressed and left/right, then process points */
    if (event->state & GDK_CONTROL_MASK)
    {
        switch (event->keyval)
        {
            case GDK_Left:
            case GDK_Right:
                {
                    gint direction = (event->keyval==GDK_Left)? StepPrev : StepNext;
                    setPoints(direction);
                    retval = True;
                }
                break;

            default:
                break;
        }
    }
    /* if no modifier and left/right, then process events */
    else if (event->state == 0)
    {
        switch (event->keyval)
        {
            case GDK_Left:
            case GDK_Right:
                {
                    gint direction = (event->keyval==GDK_Left)? StepPrev : StepNext;
                    if (PDH_HasEvent(direction) == True)
                    {
                        setEvents(direction);
                        retval = True;
                    }
                }
                break;

            case GDK_Home:
            case GDK_End:
                PDH_SetPoint(event->keyval);
                PD_DrawData(True);
                break;

            default:
                break;
        }
    }
    else
    {
        /* do nothing */
    }

    return retval;
}

/**
 * This method does the event update on the display. Based on the direction, the
 * event data is moved in that direction.
 *
 * @param direction which direction to move the event
 */
static void setEvents(guint direction)
{
    /* move the event in the direction specified */
    PDH_StepEvent(direction);

    /* update the event dialog if its visible */
    if (ED_GetLockSel() == False)
    {
        ED_Reset();
    }

    /* load the next event, rereads the tdrs.predict file! */
    PDH_UpdateEvents();

    /* update the arrow sensitivity */
    PD_UpdateEventArrows();

    /* draw the data */
    PD_DrawData(True);
}

/**
 * This method handles the iconification of the app
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean visibilityCb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    GdkEventWindowState *windowStateEvent = (GdkEventWindowState *)event;

    if (windowStateEvent->new_window_state&GDK_WINDOW_STATE_ICONIFIED ||
        windowStateEvent->new_window_state&GDK_WINDOW_STATE_WITHDRAWN)
    {
        thisIsIconified = True;
    }
    else
    {
        thisIsIconified = False;
    }
#ifndef _EXT_PARTNERS
    GTKsetWindowIconified(thisWindow, thisIsIconified);
#endif

    return(True);
}

/**
 * Handles window close event
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event. False
 *         to propagate the event further.
 */
static gboolean closeCb(GtkWidget *widget, GdkEvent *event, void * user_data)
{
    D_CloseDialog(PREDICT);
    return(True);
}

/**
 * Sets the toolbar button mask visual.
 *
 * @param coord the coordinate type
 * @param mask_num the mask number
 * @param mask_name the mask name
 */
static void setButtonMaskVisual(gint coord, guint mask_num, const gchar *mask_name)
{
    gboolean enable = MF_GetVisibility(PREDICT, coord, (gint)mask_num);
    switch (coord)
    {
        case SBAND1:
        case SBAND1_KUBAND:
            D_SetButtonMaskVisual(&thisWindowData[SBand1DrawingArea], enable, mask_name, coord);
            D_SetButtonMaskVisual(&thisWindowData[S1KU_SBandDrawingArea], enable, mask_name, coord);
            break;

        case SBAND2:
        case SBAND2_KUBAND:
            D_SetButtonMaskVisual(&thisWindowData[SBand2DrawingArea], enable, mask_name, coord);
            D_SetButtonMaskVisual(&thisWindowData[S2KU_SBandDrawingArea], enable, mask_name, coord);
            break;

        case KUBAND:
        case KUBAND2:
            D_SetButtonMaskVisual(&thisWindowData[KuBandDrawingArea], enable, mask_name, coord);
            break;

        case STATION:
            D_SetButtonMaskVisual(&thisWindowData[StationDrawingArea], enable, mask_name, coord);
            break;

        default:
            break;
    }
}

/**
 * This function updates the arrow buttons and label used to select
 * next/previous prediction tracks.  The label is visible only if exactly one
 * event track is selected and one or both buttons are displayed, and the
 * buttons are visible only if exactly one track is selected and there is a
 * next/previous track available.
 *
 * @param direction which direction the event is moving
 *
 * @return Status of event data. True if more data available in
 *         the direction specified, otherwise False.
 */
void PD_UpdateEventArrows(void)
{
    /* update the next and previous event tracks */
    /* don't care about the return value */
    updateEventTracks(StepNext);
    updateEventTracks(StepPrev);
}

/**
 * This function updates the arrow buttons and label used to select
 * next/previous prediction tracks.  The label is visible only if exactly
 * one event track is selected and one or both buttons are displayed, and
 * the buttons are visible only if exactly one track is selected and there
 * is a next/previous track available.
 *
 * @param direction which direction the event is moving
 *
 * @return Status of event data. True if more data available in
 *         the direction specified, otherwise False.
 */
static gboolean updateEventTracks(gint direction)
{
    gboolean enabled = False;

    if (direction == StepNext)
    {
        if (thisEventsRightArrow != NULL)
        {
            /* does the predicts have anymore events ? */
            enabled = PDH_HasEvent(direction);
            gtk_widget_set_sensitive(thisEventsRightArrow, enabled);

            /* update handlers */
            updateTrackButtonHandlers(direction, enabled);
        }
    }
    else
    {
        if (thisEventsLeftArrow != NULL)
        {
            /* does the predicts have anymore events ? */
            enabled = PDH_HasEvent(direction);
            gtk_widget_set_sensitive(thisEventsLeftArrow, enabled);

            /* update handlers */
            updateTrackButtonHandlers(direction, enabled);
        }
    }

    return enabled;
}

/**
 * Enables/disables the direction for the pressed and activate handlers that are
 * attached to the track button. If the first button is disabled, then make the
 * button move forward. When the last button is disabled, then reverse the
 * direction.
 *
 * @param direction indicates which handlers to enable/disable
 * @param enabled flag indicating if arrow button is enabled or disabled.
 */
static void updateTrackButtonHandlers(gint direction, gboolean enabled)
{
    /* determine if the track button needs to have the */
    /* callback changed. when the events reach the end of */
    /* the list, switch the callback to start selecting previous */
    /* events. when the events are at the beginning, switch the */
    /* callback to select the next event. */
    if (thisTrackButton != NULL)
    {
        if (enabled == False)
        {
            if (direction == StepPrev)
            {
                if (thisLeftPressedHID != 0)
                {
                    g_signal_handler_disconnect(thisTrackButton, thisLeftPressedHID);
                    thisLeftPressedHID = 0;

                    g_signal_handler_disconnect(thisTrackButton, thisLeftActivateHID);
                    thisLeftActivateHID = 0;
                }

                /* switch callback to select previous */
                if (thisRightPressedHID == 0)
                {
                    thisRightPressedHID = g_signal_connect(thisTrackButton, "pressed",
                                                           G_CALLBACK(pressedEventsCb),
                                                           GINT_TO_POINTER(StepNext));

                    thisRightActivateHID = g_signal_connect(thisTrackButton, "activate",
                                                            G_CALLBACK(activateEventsCb),
                                                            GINT_TO_POINTER(StepNext));
                }
            }

            if (direction == StepNext)
            {
                if (thisRightPressedHID != 0)
                {
                    g_signal_handler_disconnect(thisTrackButton, thisRightPressedHID);
                    thisRightPressedHID = 0;

                    g_signal_handler_disconnect(thisTrackButton, thisRightActivateHID);
                    thisRightActivateHID = 0;
                }

                /* switch callback to select next */
                if (thisLeftPressedHID == 0)
                {
                    thisLeftPressedHID = g_signal_connect(thisTrackButton, "pressed",
                                                          G_CALLBACK(pressedEventsCb),
                                                          GINT_TO_POINTER(StepPrev));

                    thisLeftActivateHID = g_signal_connect(thisTrackButton, "activate",
                                                           G_CALLBACK(activateEventsCb),
                                                           GINT_TO_POINTER(StepPrev));
                }
            }
        }
    }
}

/**
 * Sets the sensitivity of the arrow buttons for the point buttons. The current
 * line is checked to see if a points exists before and after. Based on these
 * results the arrow buttons sensitivity is set. Since the points tracking also
 * moves to next/previous event, a check is made to see if a previous event
 * exists.
 */
static void updateEventPoints(void)
{
    /* does the current track have more points */
    gboolean show_prev = PDH_HasPoint(StepPrev);
    gboolean show_next = PDH_HasPoint(StepNext);

    /* no more previous points, does an event exist */
    if (show_prev == False)
    {
        show_prev = PDH_HasEvent(StepPrev);
    }

    gtk_widget_set_sensitive(thisPointsLeftArrow, show_prev);
    gtk_widget_set_sensitive(thisPointsRightArrow, show_next);
}

/**
 * This function updates the text display of the currently selected data point
 * coordinate and matching solar coordinate.
 */
static void updateEventName(void)
{
    gchar str[80];
    Point solar_pt;
    Point *point;
    GList *currentPt = PDH_GetCurrentPt();

    /* update current point and solar point */
    if (currentPt != NULL)
    {
        PredictPt *pt = (PredictPt *)currentPt->data;
        if (pt != NULL)
        {
            PredictSet *ps = (PredictSet *)pt->set->data;

            /* current solar point */
            solar_pt.x = -1;
            solar_pt.y = -1;
            if (thisShowSolarTrace)
            {
                SD_GetPoint(thisCoordinateType, pt->timeStr, &solar_pt.x, &solar_pt.y);
            }

            point = PDH_GetCoordinatePoint(thisCoordinateType, pt);
            setAzElPoints(point, &solar_pt);

            /* Update current track name field */
            g_sprintf(str, "%s %d %s",
                      ps->eventName, ps->orbit,
                      (pt->shoPt) ?
                      (currentPt->prev == NULL ? "(SHO Start)" : "(SHO End)") :
                      (ps->shoStatus[0] == '\0' ? "(no SHO)" : ps->shoStatus));
        }
        else
        {
            g_stpcpy(str, "  ");
        }
    }
    else
    {
        g_stpcpy(str, "  ");
    }

    D_SetLabelText(thisTrackValue, str, NULL);
}

/**
 * Sets the AZ/EL points for the current and solar labels
 *
 * @param current_pt has the current point values
 * @param solar_pt has the solar point values
 */
static void setAzElPoints(Point *current_pt, Point *solar_pt)
{
    gchar    str[80];
    Point   cpt;

    cpt.x = (gint)ROUND((gdouble) current_pt->x / 10.0);
    cpt.y = (gint)ROUND((gdouble) current_pt->y / 10.0);

    g_sprintf(str, "%4d", cpt.x);
    D_SetLabelText(thisCurrentPoint[AZ_POINT], str, NULL);
    g_sprintf(str, "%4d", cpt.y);
    D_SetLabelText(thisCurrentPoint[EL_POINT], str, NULL);

    if (solar_pt->x != -1 && solar_pt->y != -1)
    {
        Point spt;
        spt.x = (gint)ROUND( (gdouble) solar_pt->x / 10.0 );
        spt.y = (gint)ROUND( (gdouble) solar_pt->y / 10.0 );

        g_sprintf(str, "%4d", spt.x);
        D_SetLabelText(thisSolarPoint[AZ_POINT], str, NULL);

        g_sprintf(str, "%4d", spt.y);
        D_SetLabelText(thisSolarPoint[EL_POINT], str, NULL);
    }
    else
    {
        D_SetLabelText(thisSolarPoint[AZ_POINT], "", NULL);
        D_SetLabelText(thisSolarPoint[EL_POINT], "", NULL);
    }
}

/**
 * Callback method that handles the toggling of the gmt/utc time formats
 *
 * @param widget calling widget
 * @param user_data user data
 */
static void timeCb(GtkWidget *widget, void * user_data)
{
    PD_UpdateTimeType(True);
    RTD_UpdateTimeType(False);
    ED_UpdateTimeType(False);
}

/**
 * Callback method that proceses the carriage return while in the entry field.
 * If the user was changing the event time, then the predict time is only
 * changed. If the user is simply selecting the carriage return, then the time
 * is moved to the next timed point.
 *
 * @param widget calling widget
 * @param user_data user data
 */
static void activateTimeCb(GtkWidget *widget, void * user_data)
{
    if (thisKeyPressEntryActive == True)
    {
        PD_UpdateTime(1);
        thisKeyPressEntryActive = False;
    }
    else
    {
        setPoints(StepNext);
    }
}

/**
 * Callback method that validates the time entry
 *
 * @param widget calling widget
 * @param event event data
 * @param user_data user data
 *
 * @return False continues processing
 */
static gboolean focusOutTimeCb(GtkWidget *widget, GdkEventFocus *event, void * user_data)
{
    PD_UpdateTime(1);
    return(False);
}

/**
 * Processes the key press event for the time entry field. If the user selects a
 * key value that is not the carriage return, a flag is set indicating the user
 * is making a change to the time entry field. The activate_cb method will clear
 * the flag.
 *
 * @param widget the time entry widget
 * @param user_data user data not used
 *
 * @return False allows other callbacks to process
 */
static gboolean keyPressCb(GtkWidget *widget, GdkEventKey *event, void * user_data)
{
    if (event->keyval != GDK_Return)
    {
        thisKeyPressEntryActive = True;
    }

    return False;
}

/**
 * Handles the click event for the search button. The action will render a popup
 * dialog that allows the user to enter a time that will ultimately search the
 * tdrs.predict data file and load the time if found, otherwise no action is
 * taken if the time is not found.
 *
 * @param button the button the invoked this action
 * @param user_data user data, not used
 */
static void searchClickedCb(GtkButton *button, void * user_data)
{
    GtkWidget *dialog = createSearchTimeDlg();
    openSearchTimeDlg(dialog);
}

/**
 * Opens the dialog for searching for event tracks by time.
 *
 * @param dialog the search dialog instance
 */
static void openSearchTimeDlg(GtkWidget *dialog)
{
    if (dialog != NULL)
    {
        gtk_widget_show_all(dialog);
    }
}

/**
 * Creates the dialog for searcing for event tracks by time.
 *
 * @return dialog returns the dialog instance
 */
static GtkWidget *createSearchTimeDlg(void)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *frame;
    GtkWidget *label;
    GtkWidget *time_entry;
    GList *glist = PDH_GetData();
    GList *gdata;
    PredictSet *ps;

    GtkWidget *dialog = gtk_dialog_new_with_buttons("Enter time", GTK_WINDOW(thisWindow),
                                                    (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                                     GTK_DIALOG_DESTROY_WITH_PARENT),
                                                    GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                    NULL);

    gtk_container_set_border_width(GTK_CONTAINER(dialog), 8);

    vbox = gtk_vbox_new(False, 3);
    {
        // 8.01.01 
        gchar time_str[TIME_STR_LEN] = ": :";
        gint time_type = AM_GetTimeType();

        hbox = gtk_hbox_new(False, 3);
        {
            label = gtk_label_new("AOS >=");
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);

	    // 8.01.01, make sure string is not null
            /* get first record */
            gdata = g_list_first(glist);
	    if (gdata != NULL) {
	      ps = (PredictSet *)gdata->data;

	      // make sure src string is not null
	      /* make AOS string for first record */
	      if (ps != NULL && ps->aosStr != NULL) {
		g_stpcpy(time_str, ps->aosStr);
		if (time_type == TS_GMT)
		  {
		    UtcToGmt(time_str);
		  }
		else if (time_type == TS_PET)
		  {
		    UtcToPet(time_str);
		  }
	      }
	    }
            label = gtk_label_new(time_str);
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);


        hbox = gtk_hbox_new(False, 3);
        {
            frame = gtk_frame_new("Enter Time");
            gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
            gtk_container_set_border_width(GTK_CONTAINER(frame), 2);

            /* create time entry field and add to the dialog */
            time_entry = gtk_entry_new();
            gtk_container_add(GTK_CONTAINER(frame), time_entry);

            g_signal_connect(time_entry, "activate",
                             G_CALLBACK(searchActivateCb),
                             NULL);
            g_signal_connect(time_entry, "key-press-event",
                             G_CALLBACK(searchKeyPressCb),
                             NULL);

            gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);

        hbox = gtk_hbox_new(False, 3);
        {
            /* get last record */
            gdata = g_list_last(glist);
 	    // 8.01.01, make sure string is not null
	    if (gdata != NULL) {
	      ps = (PredictSet *)gdata->data;
	    
	      // make sure src string is not null
	      /* make LOS string for last record */
	      if (ps != NULL && ps->losStr != NULL) {
		g_stpcpy(time_str, ps->losStr);
		if (time_type == TS_GMT)
		  {
		    UtcToGmt(time_str);
		  }
		else if (time_type == TS_PET)
		  {
		    UtcToPet(time_str);
		  }
	      }
	    }
            label = gtk_label_new(time_str);
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);

            label = gtk_label_new("<= LOS");
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);
    }

    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox);

    /* handle button events */
    g_signal_connect(dialog, "response", G_CALLBACK(searchResponseCb), time_entry);

    return dialog;
}

/**
 * Handles the response callback for the search dialog for event tracks.
 *
 * @param widget the dialog widget
 * @param response_id the button response
 * @param data user data time entry widget
 */
static void searchResponseCb(GtkWidget *widget, gint response_id, void * data)
{
    gboolean doUnmanage = True;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            {
                doUnmanage = processSearchTime((GtkWidget *)data);
            }
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(widget);
        widget = NULL;
    }
}

/**
 * Handles the carriage return in the entry field widget.
 * Processes the time data that the user entered.
 *
 * @param widget the time entry widget
 * @param data not used
 */
static void searchActivateCb(GtkWidget *widget, void * data)
{
    processSearchTime(widget);
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean searchKeyPressCb(GtkWidget *widget, GdkEventKey *event, void * data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return(False);
    }

    gdk_beep();
    return(True);
}

/**
 * Processes the search request. Looks for event tracks that
 * are within the time specified. The time must be between
 * the aos and los times exclusively.
 *
 * @param widget the time entry widget
 *
 * @return True if processing ok, otherwise False.
 */
static gboolean processSearchTime(GtkWidget *widget)
{
    gint time_type = AM_GetTimeType();

    /* validate the input, is it a valid time */
    const gchar *time_str = gtk_entry_get_text(GTK_ENTRY(widget));
    if (ValidTime(time_type, (gchar *)time_str) == True)
    {
        gboolean found = False;

        /* convert the time for comparison */
        glong secs = StrToSecs(time_type, (gchar *)time_str);

        GList *glist = PDH_GetData();
        for (; glist != NULL; glist = g_list_next(glist))
        {
            PredictSet *ps = (PredictSet *)glist->data;
            if (ps->listed)
            {
                /* clear display state for predict view */
                if (ps->displayed & DISPLAY_PD)
                {
                    ps->locksel = False;
                    ps->displayed -= DISPLAY_PD;
                }

                /* does time entry fall within aos and los time? */
                if ((secs >= ps->aosSecs) && (secs <= ps->losSecs))
                {
                    ps->locksel = True;
                    ps->displayed |= DISPLAY_PD;
                    found = True;
                }
            }
        }

        /* did we find an event? */
        if (found == True)
        {
            /* update the event dialog if its visible */
            ED_Reset();

            /* update the rest */
            PDH_UpdateEvents();
            PD_UpdateEventArrows();
            PD_DrawData(True);
        }
    }
    else
    {
        gchar message[512];

        /* not valid, post error dialog */
        g_sprintf(message, "Time entered must be a valid %s format!",
                  (time_type == TS_UTC ? "UTC" :
                   time_type == TS_GMT ? "GMT" :
                   time_type == TS_PET ? "PET" : "TIME"));
        ErrorMessage(widget, "Invalid Time Format", message);

        return False;
    }

    return True;
}

/**
 * Draws the events selected for displaying
 *
 * @param windata window data
 * @param coord_view current coordinate view
 * @param sets the predict data sets
 */
static void drawPredictEvents(WindowData *windata, gint coord_view, GList *sets)
{
    /* get the background pixmap drawable */
    GdkDrawable *drawable = AM_Drawable(windata, BG_DRAWABLE);

    gint coord = windata->coordinate_type;
    if (coord != -1)
    {
        GList *glist;
        GdkGC *gc;
        GdkGCValues _values;
        GList *currentPt = PDH_GetCurrentPt();

        /* see if new pixmap needs to be allocated, size change? */
        /* we're going to use this to store the image before drawing */
        AM_AllocatePixmapData(windata, windata->track_data, False);

        /* get a gc for copy */
        _values.function = GDK_COPY;
        gc = gdk_gc_new_with_values(windata->drawing_area->window, &_values, GDK_GC_FUNCTION);

        /* copy the bg pixmap to the tracks pixmap */
        gdk_draw_drawable(windata->track_data->pixmap,
                          gc,
                          drawable,
                          0, 0,  /* dst x, y */
                          0, 0,  /* src x, y */
                          -1, -1 /* src width, height, use ALL */ );

        /* now draw into the tracks pixmap */
        drawable = GDK_DRAWABLE(windata->track_data->pixmap);

        glist = sets;
        for (; glist != NULL; glist = glist->next)
        {
            PredictSet *ps = (PredictSet *)glist->data;
            if ((ps->displayed&DISPLAY_PD) && (ps->ptList != NULL))
            {
                /* draw the event */
                drawPredictEvent(windata, coord, glist, currentPt, drawable);
            }
        }

        if (thisShowSolarTrace == True)
        {
            SD_DrawData(windata, coord, sets, drawable);
        }

        if (currentPt != NULL)
        {
            if (thisShowSolarTrace == True)
            {
                PredictPt *pt = (PredictPt *)currentPt->data;
                SD_DrawSymbolAtTime(windata, coord, pt->timeStr, drawable);
            }

            drawPredictBox(windata, coord, currentPt, drawable);
        }

        /* copy the bg and tracks to the src pixmap */
        gdk_draw_drawable(AM_Drawable(windata, SRC_DRAWABLE),
                          gc,
                          windata->track_data->pixmap,
                          0, 0,  /* dst x, y */
                          0, 0,  /* src x, y */
                          -1, -1 /* src width, height, use ALL */ );

        /* all done with the gc */
        g_object_unref(gc);
    }
}

/**
 * Draws the a predict event selected for displaying
 *
 * @param windata window data
 * @param coord current coordinate type
 * @param sets doubly-link list of predict sets
 * @param currentPt current point
 * @param drawable draw to this
 */
static void drawPredictEvent(WindowData *windata, gint coord, GList *sets, GList *currentPt, GdkDrawable *drawable)
{
    GdkGC *gc;
    PangoLayout *layout;
    gint color;
    gint x1, y1;
    gint x2, y2;
    gint w, h;

    PredictSet *ps = (PredictSet *)sets->data;
    if (ps != NULL && currentPt != NULL)
    {
        PredictPt *cpt = (PredictPt *)currentPt->data;

        GList *pp1 = ps->ptList;
        GList *pp2 = pp1->next;
        if (pp1 != NULL && pp2 != NULL)
        {
            PredictPt *pp1pt = (PredictPt *)pp1->data;
            PredictPt *pp2pt = (PredictPt *)pp2->data;
            if (pp1pt != NULL && pp2pt != NULL)
            {
                Point *point1 = PDH_GetCoordinatePoint(coord, pp1pt);
                Point *point2 = PDH_GetCoordinatePoint(coord, pp2pt);

                /*--------------------------------------------------------*/
                /* Draw event label and SHO start diamond, if appropriate */
                /*--------------------------------------------------------*/
                if ((cpt != NULL) && (cpt->set != sets))
                {
                    color = PDH_GetPointColor(ps->equivalentPt);
                }
                else
                {
                    color = PDH_GetPointColor(currentPt);
                }

                gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(color));

                if (pp1pt->shoPt)
                {
                    drawDiamond(windata, coord, gc, point1, drawable);
                }

                /*----------------------*/
                /* Draw the event label */
                /*----------------------*/
                layout = gtk_widget_create_pango_layout(windata->drawing_area, "");
                pango_layout_set_text (layout, ps->trackLabel, -1);

                /* determine window point to place the event label */
                WG_PointToWindow(coord,
                                 windata->drawing_area->allocation.width,
                                 windata->drawing_area->allocation.height,
                                 point1->x, point1->y, &x1, &y1);

                x1 = x1 - (FH_GetFontWidth(layout) / 2);
                y1 = y1 + (FH_GetFontHeight(layout) / 2);

                /* adjust point if label to be display out of bounds */
                w = windata->drawing_area->allocation.width;
                h = windata->drawing_area->allocation.height;

                if (x1 > (gint)w)
                {
                    x1 = (gint)w - FH_GetFontWidth(layout);
                }

                if (y1 > (gint)h)
                {
                    y1 = (gint)h;
                }

                if (x1 < 0)
                {
                    x1 = 0;
                }

                if (y1 < FH_GetFontHeight(layout))
                {
                    y1 = FH_GetFontHeight(layout);
                }

                gdk_draw_layout(drawable, gc, x1, y1, layout);

                g_object_unref(gc);
                g_object_unref(layout);

                /*-----------------------*/
                /* Draw the predict line */
                /*-----------------------*/
                while (pp2 != NULL)
                {
                    color = PDH_GetPointColor(pp1);
                    gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(color));

                    if (PDH_IsWrapped(coord, pp1) == False)
                    {
                        gdk_gc_set_line_attributes(gc, 1, GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_ROUND);

                        WG_PointToWindow(coord,
                                         windata->drawing_area->allocation.width,
                                         windata->drawing_area->allocation.height,
                                         point1->x, point1->y, &x1, &y1);
                        WG_PointToWindow(coord,
                                         windata->drawing_area->allocation.width,
                                         windata->drawing_area->allocation.height,
                                         point2->x, point2->y, &x2, &y2);

                        gdk_draw_line(drawable, gc, x1, y1, x2, y2);
                    }

                    pp1 = pp2;
                    pp2 = pp2->next;
                    if (pp2 != NULL)
                    {
                        pp1pt = (PredictPt *)pp1->data;
                        pp2pt = (PredictPt *)pp2->data;

                        point1 = PDH_GetCoordinatePoint(coord, pp1pt);
                        point2 = PDH_GetCoordinatePoint(coord, pp2pt);

                        /*---------------------------------------*/
                        /* Draw SHO stop diamond, if appropriate */
                        /*---------------------------------------*/
                        if (pp1pt->shoPt)
                        {
                            drawDiamond(windata, coord, gc, point1, drawable);
                        }

                        g_object_unref(gc);
                    }
                }
            }
        }
    }
}

/**
 * This function displays the box around the current data point.
 *
 * @param windata window data
 * @param coord current coordinate type
 * @param pt predict point
 * @param drawable draw to this
 */
static void drawPredictBox(WindowData *windata, gint coord, GList *ptList, GdkDrawable *drawable)
{
    gint x, y;
    gint w, h;
    GdkGC *gc;

    if (ptList != NULL)
    {
        PredictPt *pt = (PredictPt *)ptList->data;
        if (pt != NULL)
        {
            Point *point = PDH_GetCoordinatePoint(coord, pt);

            WG_PointToWindow(coord,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height,
                             point->x, point->y, &x, &y);

            /*------------------------------------*/
            /* Do not attempt to draw if point is */
            /* out of bounds of the drawing area  */
            /*------------------------------------*/
            w = windata->drawing_area->allocation.width;
            h = windata->drawing_area->allocation.height;
            if ((x >= 0 && x <= w) &&
                (y >= 0 && y <= h))
            {
                /*---------------------------*/
                /* Draw box around the point */
                /*---------------------------*/
                gc = CH_SetColorGC(windata->drawing_area, CH_GetColorName(PDH_GetPointColor(ptList)));
                gdk_draw_rectangle(drawable,
                                   gc, False,(gint)x-4,(gint)y-4,8,8);
                g_object_unref(gc);
            }
        }
    }
}

/**
 * This function draws a diamond of "radius" 5 at pt.  This is done to mark the
 * end of a line as having a SHO time extending beyond the extent of a predict
 * event (although this function doesn't care about that).
 *
 * @param windata window data
 * @param coord current coordinate type
 * @param gc graphics context
 * @param pt point
 * @param drawable draw to this
 */
static void drawDiamond(WindowData *windata, gint coord, GdkGC *gc, Point *pt, GdkDrawable *drawable)
{
    gint  x, y;
    gint  d = 5;

    if (windata != NULL)
    {
        WG_PointToWindow(coord,
                         windata->drawing_area->allocation.width,
                         windata->drawing_area->allocation.height,
                         pt->x, pt->y, &x, &y);

        gdk_draw_line(drawable, gc, x-d, y,   x,   y-d);
        gdk_draw_line(drawable, gc, x,   y-d, x+d, y);
        gdk_draw_line(drawable, gc, x+d, y,   x,   y+d);
        gdk_draw_line(drawable, gc, x,   y+d, x-d, y);
    }
}

/**
 * Does the toggling of the time button
 *
 * @param update_time_type indicates whether or not to update the time type flag
 * or just use the current value.
 */
void PD_UpdateTimeType(gboolean update_time_type)
{
    /* get current time type in effect */
    gint old_time_type = AM_GetTimeType();
    gint new_time_type = TS_UNKNOWN;

    /* update the time type ? */
    if (update_time_type == True)
    {
        new_time_type = AM_UpdateTimeType();
    }
    else
    {
        new_time_type = old_time_type;
    }

    if (thisTimeButton != NULL)
    {
        gtk_label_set_text(GTK_LABEL(GTK_BIN(thisTimeButton)->child),
                           GetTimeName(new_time_type));
        PD_UpdateTime(0);
    }
}

/**
 * public interface to the time button update
 *
 * @param action update action
 */
void PD_UpdateTime(gint action)
{
    GList *currentPt = PDH_GetCurrentPt();
    if (currentPt != NULL)
    {
        if (action == 1)
        {
            gint time_type = AM_GetTimeType();
            gchar *timePtr = (gchar*)gtk_entry_get_text(GTK_ENTRY(thisTimeEntry));
            if (ValidTime(time_type, timePtr) == True)
            {
                GList *newPt;
                PredictPt *pt = (PredictPt *)currentPt->data;

                if (time_type == TS_GMT)
                {
                    GmtToUtc(timePtr);
                }
                else if (time_type == TS_PET)
                {
                    PetToUtc(timePtr);
                }

                newPt = PDH_InterpolatePoint(pt->set, timePtr);
                if (newPt != NULL)
                {
                    PDH_MarkPosition(newPt);
                    PD_DrawData(True);
                }
                else
                {
                    updateEventTime();
                }
            }
            else
            {
                updateEventTime();
            }
        }
        else
        {
            updateEventTime();
            updateEventName();
            updateEventPoints();
        }
    }
}

/**
 * This function updates the text displaying the time for the currently
 * selected position.
 */
static void updateEventTime(void)
{
    gchar newTime[TIME_STR_LEN] = {0};
    GList *currentPt = PDH_GetCurrentPt();

    if (currentPt == NULL)
    {
        /* disables widget, can't edit */
        gtk_editable_set_editable(GTK_EDITABLE(thisTimeEntry), False);
        gtk_entry_set_text(GTK_ENTRY(thisTimeEntry), newTime);
    }
    else
    {
        gint time_type;
        PredictPt *pt = (PredictPt *)currentPt->data;
        if (pt != NULL)
        {
            g_stpcpy(newTime, pt->timeStr);

            time_type = AM_GetTimeType();
            if (time_type == TS_GMT)
            {
                UtcToGmt(newTime);
            }

            else if (time_type == TS_PET)
            {
                UtcToPet(newTime);
            }

            /* enables widget for editing */
            gtk_editable_set_editable(GTK_EDITABLE(thisTimeEntry), True);
            gtk_entry_set_text(GTK_ENTRY(thisTimeEntry), newTime);
        }
        else
        {
            /* disables widget, can't edit */
            gtk_editable_set_editable(GTK_EDITABLE(thisTimeEntry), False);
            gtk_entry_set_text(GTK_ENTRY(thisTimeEntry), newTime);
        }
    }
}

/**
 * Returns the current type selected
 *
 * @return SBAND1, SBAND2 KUBAND, STATION
 */
gint PD_GetCoordinateType(void)
{
    return(thisCoordinateType);
}

/**
 * Returns the dimension of the current drawing area.
 *
 * @return dimension of current drawing area; memory must
 * be freed when done.
 */
Dimension *PD_GetDrawingAreaSize(void)
{
    Dimension *d = NULL;
    GtkWidget *w = NULL;

    switch (thisCoordinateType)
    {
        case SBAND1:
            w = thisWindowData[SBand1DrawingArea].drawing_area;
            break;

        case SBAND2:
            w = thisWindowData[SBand2DrawingArea].drawing_area;

        case KUBAND:
        case KUBAND2:
            w = thisWindowData[KuBandDrawingArea].drawing_area;
            break;

        case STATION:
            w = thisWindowData[StationDrawingArea].drawing_area;
            break;

        default:
            w = NULL;
            break;
    }

    if (w != NULL)
    {
        d = (Dimension *)MH_Calloc(sizeof(Dimension), __FILE__, __LINE__);
        d->width = w->allocation.width;
        d->height = w->allocation.height;
    }

    return(d);
}

/**
 * This method allows the callee, to set the redraw flag in the window data
 * structure, this forces a redraw of the pixmap.
 *
 * @param flag
 */
void PD_SetRedraw(gboolean flag)
{
    gint i;
    for (i=0; i<MAX_DRAWING_AREAS; i++)
    {
        thisWindowData[i].redraw_pixmap = True;
    }
}

/**
 * This is it, does the actual drawing. Order of the drawing functions is
 * important, otherwise some objects (masks) will be drawn on top of other
 * objects; not the desired effect
 *
 * @param doText flag indicating if events should be drawn
 */
void PD_DrawData(gboolean doText)
{
    /*
    ** NOTE: this draw method is modeled after the
    ** Realtime draw method. Currently, the predict
    ** dialog does not support multi-view because of
    ** the cpu load. Multi-view requires each view to
    ** recompute the predict data tracks which is very
    ** costly.
    ** So the number of coordinate types to process will
    ** always be one, so no need to loop for multi-views
    */
//    static GStaticMutex mutex = G_STATIC_MUTEX_INIT;
//    g_static_mutex_lock(&mutex);
    {
        WindowData *windata;

        /* get the coordinate type */
        gint coord_view = -1;

        switch (thisCoordinateType)
        {
          case SBAND1:
             coord_view = SBAND1_VIEW;
             break;

          case SBAND2:
             coord_view = SBAND2_VIEW;
             break;

          case KUBAND:
          case KUBAND2:
             coord_view = KUBAND_VIEW;
             break;

          case STATION:
             coord_view = STATION_VIEW;
             break;

          case SBAND1_KUBAND:
             coord_view = S1KU_SBAND_VIEW;
             break;

          case SBAND2_KUBAND:
             coord_view = S2KU_SBAND_VIEW;
             break;

          default:
             break;
        }

        /* create the offscreen image */
        /* verify the window is valid */
        windata = getWindowDataByView(coord_view);
        if (windata != NULL &&
            windata->drawing_area != NULL &&
            windata->drawing_area->window != NULL &&
            windata->src_pixmap != NULL)
        {

            /* draw into the pixmap */
            windata->which_drawable = BG_DRAWABLE;

            /* always redraw the pixmaps. when the track is changing
             * we need to update the solar array joint angles.
             */
            AM_ClearDrawable(AM_Drawable(windata, windata->which_drawable),
                             windata->drawing_area->style->black_gc,
                             0, 0,
                             windata->drawing_area->allocation.width,
                             windata->drawing_area->allocation.height);

            /* draw the dynamics */
            DD_DrawDataAtTime(windata, PREDICT, coord_view, PDH_GetCurrentPt());

            /* draw masks */
            OM_ForceRedraw(PREDICT, coord_view);
            OM_DrawMasks(windata, PREDICT, coord_view);

            if (coord_view == KUBAND_VIEW)
            {
                /* are we using realtime ? */
                MF_DrawKuBandMasks(windata, PREDICT, coord_view);
            }
            else
            {
                MF_DrawMasks(windata, PREDICT, coord_view);
            }

            /* draw limit mask */
            MF_DrawLimitMask(windata, PREDICT, coord_view);

            /* draw background on top of everything */
            PH_DrawBackground(windata, PREDICT, coord_view);


            /* process the tracking data, always do this regardles */
            /* of the redraw pixmap state flag */
            /* render the image to the drawing area */
            /* get window data */
            /* draw the predict event data */
            drawPredictEvents(windata, coord_view, PDH_GetData());

            /* render src pixmap into the dst drawable */
            D_DrawDrawable(windata);

            /* update joint angle dialog if its visible */
            JAD_Update();

            /* do we need to update the event visuals ? */
            if (doText)
            {
                updateEventPoints();
                updateEventName();
                updateEventTime();
            }

            windata->redraw_pixmap = False;
        }
    }
    /* release lock */
//    g_static_mutex_unlock(&mutex);
}

#ifndef _EXT_PARTNERS
/**
 * This function is set with User Experience and called for
 * "Save Session". Save the iconified state.
 */
void *UEconfigFunc(int *size)
{
   *size = (int)sizeof(thisIsIconified);
   return (void *)&thisIsIconified;
}
#endif

#ifndef _EXT_PARTNERS
/**
 * This function is set with User Experience and called for
 * "Restore Session". Restore the iconified state.
 */
void UErestoreFunc(void *data, int size)
{
   gboolean *d = (gboolean *)data;

   if (size) {
      thisIsIconified = *d;
   }
}
#endif

