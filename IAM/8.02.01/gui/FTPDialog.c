/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <gtk/gtk.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#include "ftplib.h"

#define PRIVATE
#include "FTPDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "FontHandler.h"
#include "MessageHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/FTPDialog.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget    *thisDialog     = NULL;
static GtkWidget    *thisHostEntry  = NULL;
static GtkWidget    *thisUserEntry  = NULL;
static GtkWidget    *thisPassEntry  = NULL;
static GtkWidget    *thisPoicEntry  = NULL;
static GtkWidget    *thisHsrEntry   = NULL;
static GtkWidget    *thisEsaEntry   = NULL;
static gboolean     thisSendToPoic  = False;
static gboolean     thisSendToEsa   = False;
static gboolean     thisSendToHsr   = False;
static gboolean     thisSendSunAbgFile = False;
static gboolean     thisSendSunPositionFile = False;
static gboolean     thisSendTdrsFile= False;
static gboolean     thisSendShoFile = False;
/*************************************************************************/

/**
 * This method creates the dialog
 *
 * @param parent attach dialog to this window
 */
void FTP_Create(GtkWindow *parent)
{
    GtkWidget *container;

    if (thisDialog == NULL)
    {
        /* make the dialog with ok,cancel,clear buttons; the clear button will
         * use the close response
         */
        thisDialog = gtk_dialog_new_with_buttons("FTP Predict Data to IPs",
                                                 parent,
                                                 (GtkDialogFlags)(GTK_DIALOG_DESTROY_WITH_PARENT),
                                                 "Transmit", GTK_RESPONSE_OK,
                                                 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                 NULL);

        gtk_container_set_border_width(GTK_CONTAINER(thisDialog), 8);

        /* handle window close event */
        g_signal_connect(thisDialog, "destroy", G_CALLBACK (gtk_widget_destroyed), &thisDialog);

        /* handle button events */
        g_signal_connect(thisDialog, "response", G_CALLBACK(response_cb), NULL);

        /* build the gui */
        container = create_interface();
        if (container == NULL)
        {
            gtk_widget_destroy(thisDialog);
            thisDialog = NULL;
        }
        else
        {
            gtk_container_add(GTK_CONTAINER(GTK_DIALOG(thisDialog)->vbox), container);
        }
    }
}

/**
 * Opens the dialog.
 */
void FTP_Open(void)
{
    if (thisDialog != NULL)
    {
        /* show the dialog */
        gtk_widget_show_all(thisDialog);
    }
}

/**
 * Sends the predict data to all partners.
 *
 * @param parent
 */
gboolean FTP_Send(GtkWindow *parent)
{
    gboolean rc = True;

    /* set send flags to True */
    thisSendToPoic = True;
    thisSendToEsa  = True;
    thisSendToHsr  = True;
    thisSendSunAbgFile = True;
    thisSendSunPositionFile = True;
    thisSendTdrsFile = True;
    thisSendShoFile = True;

    /* now send the data */
    if ((rc = ftp_send_to_drop_box()) == True)
    {
        g_message("FTP transmission was successful!");
    }
    else
    {
        g_message("FTP transmission failed!");
    }

    return rc;
}

/**
 * Builds the dialog interface
 *
 * @return interface container or NULL
 */
static GtkWidget *create_interface(void)
{
    GtkWidget *table;
    GtkWidget *widget;

    table = gtk_table_new(10, 2, False);
    {
        {
            gchar host[32];
            gchar user[32];
            gchar pass[32];

            /* get the host to connect to */
            if (CP_GetConfigData(IAM_HOST, host) == False)
            {
                g_warning("IAM_HOST not defined in the config file!");
                ErrorMessage(NULL, "Config Error",
                             "IAM_HOST not defined in the config file!\n"
                             "Unable to create FTP Dialog.");
                return NULL;
            }

            /* define the host location */
            widget = gtk_label_new("Drop box Host:");
            gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), widget, 0, 1, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            thisHostEntry = gtk_entry_new();
            gtk_entry_set_text(GTK_ENTRY(thisHostEntry), host);
            gtk_table_attach(GTK_TABLE(table), thisHostEntry, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            /* get the login id */
            if (CP_GetConfigData(IAM_LOGIN, user) == False)
            {
                g_warning("IAM_LOGIN not defined in the config file");
                ErrorMessage(NULL, "Config Error",
                             "IAM_LOGIN not defined in the config file!"
                             "Unable to create FTP Dialog.");
                return NULL;
            }

            /* define the user id */
            widget = gtk_label_new("Drop box User ID:");
            gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), widget, 0, 1, 1, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            thisUserEntry = gtk_entry_new();
            gtk_entry_set_text(GTK_ENTRY(thisUserEntry),user);
            gtk_table_attach(GTK_TABLE(table), thisUserEntry, 1, 2, 1, 2, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            /* get the password */
            if (CP_GetConfigData(IAM_PWD, pass) == False)
            {
                g_warning("IAM_PWD not defined in the config file");
                ErrorMessage(NULL, "Config Error",
                             "IAM_PWD not defined in the config file!\n"
                             "Unable to create FTP Dialog.");
                return NULL;
            }

            /* define the anonymous password */
            widget = gtk_label_new("Drop box Password:");
            gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), widget, 0, 1, 2, 3, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            thisPassEntry = gtk_entry_new();
            gtk_entry_set_visibility(GTK_ENTRY(thisPassEntry), False);
            gtk_entry_set_text(GTK_ENTRY(thisPassEntry), pass);
            gtk_table_attach(GTK_TABLE(table), thisPassEntry, 1, 2, 2, 3, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
        }

        {
            gchar poic_out[MAXPATHLEN];
            gchar hsr_out[MAXPATHLEN];
            gchar esa_out[MAXPATHLEN];
            GtkWidget *inner_table;
            GtkWidget *check_box;

            /* define POIC */
            inner_table = gtk_table_new(1, 2, False);
            {
                /* get the drop-box locations */
                if (CP_GetConfigData(IAM_POIC_OUT, poic_out) == False)
                {
                    g_warning("IAM_POIC_OUT not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "IAM_POIC_OUT not defined in the config file!"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_poic_out_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                widget = gtk_label_new("POIC Drop Box:");
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 3, 4, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                thisPoicEntry = gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(thisPoicEntry),poic_out);
                gtk_table_attach(GTK_TABLE(table), thisPoicEntry, 1, 2, 3, 4, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
            }

            /* define HSR */
            inner_table = gtk_table_new(1, 2, False);
            {
                /* get the drop-box locations */
                if (CP_GetConfigData(IAM_HSR_OUT, hsr_out) == False)
                {
                    g_warning("IAM_HSR_OUT not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "IAM_HSR_OUT not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_hsr_out_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                widget = gtk_label_new("HSR Drop Box:");
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 4, 5, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                thisHsrEntry = gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(thisHsrEntry),hsr_out);
                gtk_table_attach(GTK_TABLE(table), thisHsrEntry, 1, 2, 4, 5, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
            }

            /* define ESA */
            inner_table = gtk_table_new(1, 2, False);
            {
                /* get the drop-box locations */
                if (CP_GetConfigData(IAM_ESA_OUT, esa_out) == False)
                {
                    g_warning("IAM_ESA_OUT not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "IAM_ESA_OUT not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_esa_out_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                widget = gtk_label_new("ESA Drop Box:");
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 6, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                thisEsaEntry = gtk_entry_new();
                gtk_entry_set_text(GTK_ENTRY(thisEsaEntry),esa_out);
                gtk_table_attach(GTK_TABLE(table), thisEsaEntry, 1, 2, 6, 7, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
            }
        }

        {
            GtkWidget *separator = gtk_hseparator_new();
            gtk_table_attach(GTK_TABLE(table), separator, 0, 2, 7, 8, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
        }

        {
            gchar predict_file[MAXPATHLEN];
            gchar *basename;
            gchar str_time[64];
            struct stat buf;
            struct tm *mod_time;
            GtkWidget *inner_table;
            GtkWidget *check_box;

            /* SUN_ABG */
            inner_table = gtk_table_new(1, 2, False);
            {
                if (CP_GetConfigData(SUNABG_PREDICT_XML, predict_file) == False)
                {
                    g_warning("SUNABG_PREDICT_XML not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "SUNABG_PREDICT_XML not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                if (stat(predict_file, &buf) != 0)
                {
                    g_warning("%s file stat error: %s",predict_file, strerror(errno));
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_sun_abg_predict_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                basename = g_path_get_basename(predict_file);
                widget = gtk_label_new(basename);
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 8, 9, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                if ((mod_time = localtime(&buf.st_mtime)) != NULL)
                {
                    strftime(str_time, 64, "%m/%d/%Y %I:%M:%S %p", mod_time);
                    widget = gtk_label_new(str_time);
                    gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                    gtk_table_attach(GTK_TABLE(table), widget, 1, 2, 8, 9, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
                }
            }

            /* SUN POSITION */
            inner_table = gtk_table_new(1, 2, False);
            {
                if (CP_GetConfigData(SUNPOS_PREDICT_XML, predict_file) == False)
                {
                    g_warning("SUNPOS_PREDICT_XML not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "SUNPOS_PREDICT_XML not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                if (stat(predict_file, &buf) != 0)
                {
                    g_warning("%s file stat error: %s",predict_file, strerror(errno));
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_sun_position_predict_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                basename = g_path_get_basename(predict_file);
                widget = gtk_label_new(basename);
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 9, 10, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                if ((mod_time = localtime(&buf.st_mtime)) != NULL)
                {
                    strftime(str_time, 64, "%m/%d/%Y %I:%M:%S %p", mod_time);
                    widget = gtk_label_new(str_time);
                    gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                    gtk_table_attach(GTK_TABLE(table), widget, 1, 2, 9, 10, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
                }
            }

            /* TDRS PREDICT */
            inner_table = gtk_table_new(1, 2, False);
            {
                if (CP_GetConfigData(TDRS_PREDICT_XML, predict_file) == False)
                {
                    g_warning("TDRS_PREDICT_XML not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "TDRS_PREDICT_XML not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                if (stat(predict_file, &buf) != 0)
                {
                    g_warning("%s file stat error: %s",predict_file, strerror(errno));
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_tdrs_predict_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                basename = g_path_get_basename(predict_file);
                widget = gtk_label_new(basename);
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 10, 11, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                if ((mod_time = localtime(&buf.st_mtime)) != NULL)
                {
                    strftime(str_time, 64, "%m/%d/%Y %I:%M:%S %p", mod_time);
                    widget = gtk_label_new(str_time);
                    gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                    gtk_table_attach(GTK_TABLE(table), widget, 1, 2, 10, 11, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
                }
            }

            /* SHO FILE */
            inner_table = gtk_table_new(1, 2, False);
            {
                if (CP_GetConfigData(IAM_SHO_FILE, predict_file) == False)
                {
                    g_warning("IAM_SHO_FILE not defined in the config file");
                    ErrorMessage(NULL, "Config Error",
                                 "IAM_SHO_FILE not defined in the config file!\n"
                                 "Unable to create FTP Dialog.");
                    return NULL;
                }

                if (stat(predict_file, &buf) != 0)
                {
                    g_warning("%s file stat error: %s",predict_file, strerror(errno));
                    return NULL;
                }

                check_box = gtk_check_button_new();
                g_signal_connect(G_OBJECT(check_box), "toggled", G_CALLBACK(toggle_sho_file_cb), NULL);
                gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_box), True);
                gtk_table_attach(GTK_TABLE(inner_table), check_box, 0, 1, 0, 1, GAO_NONE, GAO_NONE, 2, 0);

                basename = g_path_get_basename(predict_file);
                widget = gtk_label_new(basename);
                gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                gtk_table_attach(GTK_TABLE(inner_table), widget, 1, 2, 0, 1, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                gtk_table_attach(GTK_TABLE(table), inner_table, 0, 1, 11, 12, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

                if ((mod_time = localtime(&buf.st_mtime)) != NULL)
                {
                    strftime(str_time, 64, "%m/%d/%Y %I:%M:%S %p", mod_time);
                    widget = gtk_label_new(str_time);
                    gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
                    gtk_table_attach(GTK_TABLE(table), widget, 1, 2, 11, 12, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
                }
            }

            /* Last Transmission */
            if (CP_GetConfigData(IAM_LAST_DROP_BOX_TRANSMISSION, predict_file) == False)
            {
                g_warning("IAM_LAST_DROP_BOX_TRANSMISSION not defined in the config file");
                ErrorMessage(NULL, "Config Error",
                             "IAM_LAST_DROP_BOX_TRANSMISSION not defined in the config file!\n"
                             "Unable to create FTP Dialog.");
                return NULL;
            }

            widget = gtk_label_new("LAST TRANSMISSION:");
            gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), widget, 0, 1, 12, 13, GAO_EXPAND_FILL, GAO_NONE, 2, 0);

            if (stat(predict_file, &buf) != 0)
            {
                g_warning("%s file stat error: %s",predict_file, strerror(errno));
                widget = gtk_label_new("not defined");
            }
            else
            {
                if ((mod_time = localtime(&buf.st_mtime)) != NULL)
                {
                    strftime(str_time, 64, "%m/%d/%Y %I:%M:%S %p", mod_time);
                    widget = gtk_label_new(str_time);
                }
                else
                {
                    widget = gtk_label_new("not defined");
                }
            }
            gtk_misc_set_alignment(GTK_MISC(widget), LEFT_ALIGN, CENTER_ALIGN);
            gtk_table_attach(GTK_TABLE(table), widget, 1, 2, 12, 13, GAO_EXPAND_FILL, GAO_NONE, 2, 0);
        }
    }

    return table;
}

/**
 * Sends the predict data to the various drop boxes
 *
 * @return True if successful otherwise False
 */
static gboolean ftp_send_to_drop_box(void)
{
    gboolean rc = True;
    netbuf *nControl = NULL;

    gchar *host = get_host();
    gchar *user = get_user();
    gchar *pass = get_pass();
    gchar *poic_out = get_poic_out();
    gchar *hsr_out = get_hsr_out();
    gchar *esa_out = get_esa_out();

    /* make sure all the returned values are valid */
    if (host != NULL && user != NULL && pass != NULL &&
        poic_out != NULL && hsr_out != NULL && esa_out != NULL)
    {
        /* do we have anything to send? */
        if ((thisSendToPoic == True ||
             thisSendToEsa  == True ||
             thisSendToHsr  == True)
            &&
            (thisSendSunAbgFile      == True ||
             thisSendSunPositionFile == True ||
             thisSendTdrsFile        == True ||
             thisSendShoFile         == True))
        {
            /* connect to the host */
            g_message("FTP Connect: host=%s",host);
            if (FtpConnect(host, &nControl))
            {
                /* login */
                g_message("FTP Login: user=%s pass=%s",user,pass);
                if (FtpLogin(user, pass, nControl))
                {
                    gchar *sun_abg_predict = NULL;
                    gchar *sun_position_predict = NULL;
                    gchar *tdrs_predict = NULL;
                    gchar *sho_file = NULL;
                    gchar sho_outfile[MAXPATHLEN] = {0};

                    if (thisSendSunAbgFile == True)
                    {
                        sun_abg_predict = get_sun_abg_predict();
                    }

                    if (thisSendSunPositionFile == True)
                    {
                        sun_position_predict = get_sun_position_predict();
                    }

                    if (thisSendTdrsFile == True)
                    {
                        tdrs_predict = get_tdrs_predict();
                    }

                    if (thisSendShoFile == True)
                    {
    #define SHO_OUTPUT_NAME "current_TDRS_Mission"
                        sho_file = get_sho_file();
                        if (CP_GetConfigData(IAM_SHO_FILE_OUT, sho_outfile) == False)
                        {
                            g_warning("IAM_SHO_FILE_OUT not defined in the config file");
                            g_warning("Using fallback name %s", SHO_OUTPUT_NAME);
                            g_stpcpy(sho_outfile, SHO_OUTPUT_NAME);
                        }
                    }

                    g_message("FTP : sun_abg=%s", (sun_abg_predict != NULL? sun_abg_predict : "NULL"));
                    g_message("FTP : sun_pos=%s", (sun_position_predict) != NULL? sun_position_predict : "NULL");
                    g_message("FTP : tdrs   =%s", (tdrs_predict) != NULL? tdrs_predict : "NULL");
                    g_message("FTP : sho    =%s", (sho_file) != NULL? sho_file : "NULL");

                    /* send data to poic ? */
                    if (thisSendToPoic == True)
                    {
                        g_message("FTP : sending data to POIC");
                        rc = ftp_send(sun_abg_predict, poic_out, NULL, nControl);
                        rc = ftp_send(sun_position_predict, poic_out, NULL, nControl);
                        rc = ftp_send(tdrs_predict, poic_out, NULL, nControl);
                        rc = ftp_send(sho_file, poic_out, sho_outfile, nControl);
                    }

                    /* send data to esa and previous transmission successful ? */
                    if (thisSendToEsa == True && rc == True)
                    {
                        g_message("FTP : sending data to ESA");
                        rc = ftp_send(sun_abg_predict, esa_out, NULL, nControl);
                        rc = ftp_send(sun_position_predict, esa_out, NULL, nControl);
                        rc = ftp_send(tdrs_predict, esa_out, NULL, nControl);
                        rc = ftp_send(sho_file, esa_out, sho_outfile, nControl);
                    }

                    /* send data to hsr and previous transmission successfule ? */
                    if (thisSendToHsr == True && rc == True)
                    {
                        g_message("FTP : sending data to HSR");
                        rc = ftp_send(sun_abg_predict, hsr_out, NULL, nControl);
                        rc = ftp_send(sun_position_predict, hsr_out, NULL, nControl);
                        rc = ftp_send(tdrs_predict, hsr_out, NULL, nControl);
                        rc = ftp_send(sho_file, hsr_out, sho_outfile, nControl);
                    }

                    /* release memory */
                    if (sun_abg_predict != NULL)
                        g_free(sun_abg_predict);

                    if (sun_position_predict != NULL)
                        g_free(sun_position_predict);

                    if (tdrs_predict != NULL)
                        g_free(tdrs_predict);

                    if (sho_file != NULL)
                        g_free(sho_file);

                    set_last_transmission();

                    /* all done */
                    FtpQuit(nControl);
                }
                else
                {
                    g_warning("Unable to login to the drop-box server: %s",host);
                    rc = False;
                }
            }
            else
            {
                g_warning("Unable to connect to the drop-box server: %s",host);
                rc = False;
            }
        }
        else
        {
            g_message("No data was selected for sending");
            rc = False;
        }

        /* release memory */
        g_free(host);
        g_free(user);
        g_free(pass);
        g_free(poic_out);
        g_free(hsr_out);
        g_free(esa_out);
    }
    return rc;
}

/**
 * Does the actual ftp put for the put_file given.  Prior to
 * making this call, the FtpChdir command must be issued,
 * otherwise the location will the user's home directory; not
 * the desired outcome. The output filename will be taken from
 * the put_file or if provided the out_name.
 *
 * @param put_file send this file, if the out_name is not set
 *                 then the basename of this paramenter is used.
 * @param chdir send data here
 * @param out_name alternate name to use
 * @param nControl internal use only
 *
 * @return True if successful otherwise False
 */
static gboolean ftp_send(gchar *put_file, gchar *chdir, gchar *out_name, netbuf *nControl)
{
    gboolean rc = True;

    g_message("FTP Chdir: chdir=%s",chdir);
    if (FtpChdir(chdir, nControl))
    {
        /* process data file? */
        if (put_file != NULL)
        {
            gchar put_name[256];

            if (out_name != NULL)
            {
                g_stpcpy(put_name, out_name);
            }
            else
            {
                gchar *basename = g_path_get_basename(put_file);
                g_stpcpy(put_name, basename);
                g_free(basename);
            }

            /* send data */
            if (FtpPut(put_file, put_name, FTPLIB_ASCII, nControl) == 0)
            {
                g_warning("Failed sending %s file to destination", put_name);
                rc = False;
            }
            else
            {
                g_message("Success sending %s file to destination", put_name);
            }
        }
        else
        {
            g_message("Source data is NULL");
            rc = False;
        }
    }
    else
    {
        g_message("Unable to chdir to %s", chdir);
        rc = False;
    }

    return rc;
}

/**
 * Sets the last transmission time stamp
 */
static void set_last_transmission(void)
{
    gchar last_transmission[MAXPATHLEN];

    if (CP_GetConfigData(IAM_LAST_DROP_BOX_TRANSMISSION, last_transmission) == False)
    {
        g_warning("IAM_LAST_DROP_BOX_TRANSMISSION not defined in the config file");
    }
    else
    {
        FILE *fp = fopen(last_transmission, "w+");
        if (fp != NULL)
        {
            time_t t = time(&t);
            gchar *s = ctime(&t);
            fputs(s, fp);
            fclose(fp);
        }
    }
}

/**
 * Gets the hostname from the entry widget
 *
 * @return data from host entry widget
 */
static gchar *get_host(void)
{
    gchar host[32];

    if (thisHostEntry != NULL)
    {
        g_stpcpy(host, gtk_entry_get_text(GTK_ENTRY(thisHostEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_HOST, host) == False)
        {
            g_warning("IAM_HOST not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(host);
}

/**
 * Gets the user name from the entry widget
 *
 * @return data from user name entry widget
 */
static gchar *get_user(void)
{
    gchar user[32];

    if (thisUserEntry != NULL)
    {
        g_stpcpy(user, gtk_entry_get_text(GTK_ENTRY(thisUserEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_LOGIN, user) == False)
        {
            g_warning("IAM_LOGIN not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(user);
}

/**
 * Gets the password from the entry widget
 *
 * @return data from password entry widget
 */
static gchar *get_pass(void)
{
    gchar pass[32];

    if (thisPassEntry != NULL)
    {
        g_stpcpy(pass, gtk_entry_get_text(GTK_ENTRY(thisPassEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_PWD, pass) == False)
        {
            g_warning("IAM_PWD not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(pass);
}

/**
 * Gets the poic from the entry widget
 *
 * @return data from poic entry widget
 */
static gchar *get_poic_out(void)
{
    gchar poic_out[MAXPATHLEN];

    if (thisPoicEntry != NULL)
    {
        g_stpcpy(poic_out, gtk_entry_get_text(GTK_ENTRY(thisPoicEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_POIC_OUT, poic_out) == False)
        {
            g_warning("IAM_POIC_OUT not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(poic_out);
}

/**
 * Gets the hsr from the entry widget
 *
 * @return data from hsr entry widget
 */
static gchar *get_hsr_out(void)
{
    gchar hsr_out[MAXPATHLEN];

    if (thisHsrEntry != NULL)
    {
        g_stpcpy(hsr_out, gtk_entry_get_text(GTK_ENTRY(thisHsrEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_HSR_OUT, hsr_out) == False)
        {
            g_warning("IAM_HSR_OUT not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(hsr_out);
}

/**
 * Gets the esa from the entry widget
 *
 * @return data from esa entry widget
 */
static gchar *get_esa_out(void)
{
    gchar esa_out[MAXPATHLEN];

    if (thisEsaEntry != NULL)
    {
        g_stpcpy(esa_out, gtk_entry_get_text(GTK_ENTRY(thisEsaEntry)));
    }
    else
    {
        if (CP_GetConfigData(IAM_ESA_OUT, esa_out) == False)
        {
            g_warning("IAM_ESA_OUT not defined in the config file");
            return NULL;
        }
    }

    return g_strdup(esa_out);
}

/**
 * Gets the sun_abg predict file from the iam.xml file
 *
 * @return sun_abg predict file or NULL if not defined
 */
static gchar *get_sun_abg_predict(void)
{
    gchar predict_file[MAXPATHLEN];
    struct stat buf;

    if (CP_GetConfigData(SUNABG_PREDICT_XML, predict_file) == False)
    {
        g_warning("SUNABG_PREDICT_XML not defined in the config file");
        return NULL;
    }

    if (stat(predict_file, &buf) != 0)
    {
        g_warning("%s file stat error: %s",predict_file, strerror(errno));
        return NULL;
    }

    return g_strdup(predict_file);
}

/**
 * Gets the sun_position predict file from the iam.xml file
 *
 * @return sun_position predict file or NULL if not defined
 */
static gchar *get_sun_position_predict()
{
    gchar predict_file[MAXPATHLEN];
    struct stat buf;

    if (CP_GetConfigData(SUNPOS_PREDICT_XML, predict_file) == False)
    {
        g_warning("SUNPOS_PREDICT_XML not defined in the config file");
        return NULL;
    }

    if (stat(predict_file, &buf) != 0)
    {
        g_warning("%s file stat error: %s",predict_file, strerror(errno));
        return NULL;
    }

    return g_strdup(predict_file);
}

/**
 * Gets the tdrs predict file from the iam.xml file
 *
 * @return tdrs predict file or NULL if not defined
 */
static gchar *get_tdrs_predict()
{
    gchar predict_file[MAXPATHLEN];
    struct stat buf;

    if (CP_GetConfigData(TDRS_PREDICT_XML, predict_file) == False)
    {
        g_warning("TDRS_PREDICT_XML not defined in the config file");
        return NULL;
    }

    if (stat(predict_file, &buf) != 0)
    {
        g_warning("%s file stat error: %s",predict_file, strerror(errno));
        return NULL;
    }

    return g_strdup(predict_file);
}

/**
 * Gets the sho file from the iam.xml file
 *
 * @return tdrs predict file or NULL if not defined
 */
static gchar *get_sho_file()
{
    gchar predict_file[MAXPATHLEN];
    struct stat buf;

    if (CP_GetConfigData(IAM_SHO_FILE, predict_file) == False)
    {
        g_warning("IAM_SHO_FILE not defined in the config file");
        return NULL;
    }

    if (stat(predict_file, &buf) != 0)
    {
        g_warning("%s file stat error: %s",predict_file, strerror(errno));
        return NULL;
    }

    return g_strdup(predict_file);
}

/**
 * This method handles the button events
 *
 * @param widget
 * @param response_id
 * @param data
 */
static void response_cb(GtkWidget *widget, gint response_id, void * data)
{
    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            if (ftp_send_to_drop_box() == True)
            {
                InfoMessage(thisDialog, "FTP Ok", "FTP transmission was successful!");
            }
            else
            {
                ErrorMessage(thisDialog, "FTP Failed", "FTP transmission failed!");
            }
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    /* destroy the widget, its so small, we'll just build it each time */
    gtk_widget_destroy(thisDialog);
    thisDialog = NULL;
}

/**
 * Toggle callback for the poic out check box; interrogates the button argument
 * for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_poic_out_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendToPoic = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the esa out check box; interrogates the button argument
 * for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_esa_out_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendToEsa = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the hsr out check box; interrogates the button argument
 * for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_hsr_out_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendToHsr = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the sun_abg data file check box; interrogates the
 * button argument for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_sun_abg_predict_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendSunAbgFile = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the sun_position data file check box; interrogates the
 * button argument for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_sun_position_predict_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendSunPositionFile = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the tdrs data file check box; interrogates the button
 * argument for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_tdrs_predict_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendTdrsFile = gtk_toggle_button_get_active(togglebutton);
}

/**
 * Toggle callback for the sho data file check box; interrogates the button
 * argument for the current state, then sets a local class variable.
 *
 * @param togglebutton the button itself
 * @param user_data not used
 */
static void toggle_sho_file_cb(GtkToggleButton *togglebutton, void * user_data)
{
    thisSendShoFile = gtk_toggle_button_get_active(togglebutton);
}
