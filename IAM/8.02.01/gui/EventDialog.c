/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_UNIX
    #include <sys/param.h>
#endif

#define PRIVATE
#include "EventDialog.h"
#undef PRIVATE

#include "AntMan.h"
#include "ConfigParser.h"
#include "Dialog.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "PredictDataHandler.h"
#include "PredictDialog.h"
#include "PrintHandler.h"
#include "RealtimeDialog.h"
#include "TimeStrings.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/EventDialog.c 271 2017-02-20 20:59:17Z dpham1@ndc.nasa.gov $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
static GtkWidget *thisWindow = NULL;

static GtkWidget *thisAllTreeView = NULL;
static GtkWidget *thisScheduledTreeView = NULL;

static GtkListStore *thisAllListStore = NULL;
static GtkListStore *thisScheduledListStore = NULL;

static gulong thisAllHandlerId = -1;
static gulong thisScheduleHandlerId = -1;

static gboolean  thisLockSelState = False;

static GtkWidget *thisTimeButton = NULL;
static GtkWidget *thisResetButton = NULL;
static GtkWidget *thisUpdateButton = NULL;
static GtkWidget *thisLeftButton = NULL;
static GtkWidget *thisRightButton = NULL;

const static guint thisLeftArrowSelected = 0;
const static guint thisRightArrowSelected = 1;

static gint thisListType = ALL_LIST;
static gboolean thisShosChanged = False;
static gboolean thisIsIconified = True;
static TimerInfo thisTimerInfo = {-1, -1, -1, NULL};

/* used to block/unblock a signal handler */
static WidgetHandlerData thisHandlerData = {NULL, 0};

/*************************************************************************/

/**
 * This method opens up the dialog for viewing
 */
void ED_Open(void)
{
    if (thisWindow != NULL)
    {
        if (thisShosChanged == True)
        {
            gtk_widget_set_sensitive(thisResetButton, False);
            gtk_widget_set_sensitive(thisUpdateButton, True);
        }

        if (thisIsIconified == True)
        {
            gtk_window_deiconify(GTK_WINDOW(thisWindow));
        }

        gtk_window_set_position(GTK_WINDOW(thisWindow), GTK_WIN_POS_CENTER);

        /* block the page switch handler */
        g_signal_handler_block(thisHandlerData.instance, thisHandlerData.handler_id);

        /* render the event dialog */
        gtk_widget_show_all(thisWindow);
        gdk_window_raise(thisWindow->window);

        /* now let the handler execute */
        g_signal_handler_unblock(thisHandlerData.instance, thisHandlerData.handler_id);

        /* set the initial tab */
        setInitialTab(GTK_NOTEBOOK(thisHandlerData.instance));
    }
}

/**
 * Public ginterface that creates the event dialog
 *
 * @return True if dialog was created ok, otherwise False
 */
gboolean ED_Create (void)
{
    if (thisWindow == NULL)
    {
        createDialog();
        D_SetIcon(thisWindow);
    }

    return(thisWindow ? True : False);
}

/**
 * Releases all memory
 */
void ED_Destructor(void)
{
    thisAllTreeView        = NULL;
    thisScheduledTreeView  = NULL;

    thisAllListStore       = NULL;
    thisScheduledListStore = NULL;

    thisTimeButton     = NULL;
    thisResetButton    = NULL;
    thisUpdateButton   = NULL;
    thisLeftButton     = NULL;
    thisRightButton    = NULL;
}

/**
 * Returns the lock selection state. This flag indicates whether or not the
 * user wants to keep the currently selected events pinned and not have the
 * events move through the list. The default behavior is to move from event to
 * the next in the list.
 *
 * @return gboolean
 */
gboolean ED_GetLockSel(void)
{
    return thisLockSelState;
}

/**
 * Build the event dialog with 4 buttons
 *
 * @return GtkWidget pointer to event dialog component
 */
static void createDialog(void)
{
    GtkWidget *notebook = NULL;
    GtkWidget *btn;
    GtkWidget *vbox;
    GtkWidget *hbbox;

    /* create dialog */
    thisWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(thisWindow), "Event Predicts");

    gtk_window_set_modal(GTK_WINDOW(thisWindow), False);

    /* signal for iconification */
    g_signal_connect(G_OBJECT(thisWindow), "window-state-event", G_CALLBACK(visibilityCb), NULL);

    /* handle window close event */
    g_signal_connect(G_OBJECT(thisWindow), "delete-event", G_CALLBACK(deleteEventCb), NULL);

    /* create container for all the components */
    vbox = gtk_vbox_new(False, 0);
    gtk_container_add(GTK_CONTAINER(thisWindow), vbox);

    gtk_container_set_border_width(GTK_CONTAINER (thisWindow), 8);

    /* create notebook for 'all' and 'scheduled' events */
    notebook = D_CreateNotebook(vbox);

    hbbox = gtk_hbutton_box_new();
    {
        GtkAccelGroup *accel_group;

        gtk_button_box_set_layout(GTK_BUTTON_BOX(hbbox), GTK_BUTTONBOX_END);
        gtk_box_set_spacing(GTK_BOX(hbbox), 5);

        /* lock selection checkbox */
        btn = gtk_check_button_new_with_label("LockSel");
        gtk_box_pack_start(GTK_BOX(hbbox), btn, False, False, 2);
        g_signal_connect(btn, "toggled", G_CALLBACK(lockselCb), NULL);

        /* create buttons */
        thisLeftButton = D_CreateArrowButton(GTK_ARROW_UP, GTK_SHADOW_OUT);

        g_signal_connect(thisLeftButton, "pressed",
                         G_CALLBACK(pressedEventCb), GINT_TO_POINTER(thisLeftArrowSelected));
        g_signal_connect(thisLeftButton, "released",
                         G_CALLBACK(releasedCb), NULL);

        g_signal_connect(thisLeftButton, "activate",
                         G_CALLBACK(activateEventCb), GINT_TO_POINTER(thisLeftArrowSelected));
        gtk_box_pack_start(GTK_BOX(hbbox), thisLeftButton, False, False, 2);

        thisRightButton = D_CreateArrowButton(GTK_ARROW_DOWN, GTK_SHADOW_OUT);

        g_signal_connect(thisRightButton, "pressed",
                         G_CALLBACK(pressedEventCb), GINT_TO_POINTER(thisRightArrowSelected));
        g_signal_connect(thisRightButton, "released",
                         G_CALLBACK(releasedCb), NULL);

        g_signal_connect(thisRightButton, "activate",
                         G_CALLBACK(activateEventCb), GINT_TO_POINTER(thisRightArrowSelected));
        gtk_box_pack_start(GTK_BOX(hbbox), thisRightButton, False, False, 2);

        btn = gtk_button_new_from_stock(GTK_STOCK_PRINT);
        gtk_box_pack_start(GTK_BOX(hbbox), btn, False, False, 2);
        g_signal_connect(btn, "clicked", G_CALLBACK(printCb), NULL);

        thisTimeButton = gtk_button_new_with_label(GetTimeName(AM_GetTimeType()));
        gtk_box_pack_start(GTK_BOX(hbbox), thisTimeButton, False, False, 2);
        g_signal_connect(thisTimeButton, "clicked", G_CALLBACK(timeCb), NULL);

        thisResetButton = gtk_button_new_with_label ("Reset");
        gtk_box_pack_start(GTK_BOX(hbbox), thisResetButton, False, False, 2);
        g_signal_connect(thisResetButton, "clicked", G_CALLBACK(resetCb), NULL);

        thisUpdateButton = gtk_button_new_with_label ("Update");
        gtk_box_pack_start(GTK_BOX(hbbox), thisUpdateButton, False, False, 2);
        g_signal_connect(thisUpdateButton, "clicked", G_CALLBACK(updateCb), NULL);

        btn = gtk_button_new_with_label("Close");
        gtk_box_pack_start(GTK_BOX(hbbox), btn, False, False, 2);

        /* add accelerator to button, so user can do <ctrl>c */
        /* to close the window */
        accel_group = gtk_accel_group_new();
        gtk_widget_add_accelerator(btn, "clicked", accel_group, GDK_c,
                                   GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

        g_signal_connect(btn, "clicked", G_CALLBACK(closeCb), NULL);

        gtk_window_add_accel_group(GTK_WINDOW(thisWindow), accel_group);
    }
    gtk_box_pack_start(GTK_BOX(vbox), hbbox, False, False, 5);

    /* create the 'all' and 'schedule' event view */
    createAllListView(notebook);
    createScheduleListView(notebook);
    
    // 8.01
    // PredictDialog calls to create EventDialog early to get the config data
    // setInitialTab reads config file for IAM_EVENT_DIALOG_INITIAL_TAB
    setInitialTab((GtkNotebook *)notebook);
    /* build the list */
    buildEventList(getTreeView(), PDH_GetData());

    /* now hook up the page switch callback */
    /* when the dialog is opened this callback will be called */
    /* then the buildEventList will be inited */
    thisHandlerData.instance = notebook;
    thisHandlerData.handler_id = g_signal_connect(GTK_NOTEBOOK(notebook),
                                                  "switch-page",
                                                  G_CALLBACK(pageSwitchCb),
                                                  NULL);

    /* finish & show */
    gtk_window_set_default_size(GTK_WINDOW(thisWindow), EVENT_DIALOG_WIDTH, EVENT_DIALOG_HEIGHT);
}

/**
 * Create the list model, defines the column attributes
 *
 * @return GtkTreeModel the model used by the list
 */
static GtkListStore *createListStore (void)
{
    /* create list store */
    GtkListStore *list_store = gtk_list_store_new (NUM_EVENT_COLUMNS,
                                                   PANGO_TYPE_FONT_DESCRIPTION,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_POINTER);

    return(list_store);
}

/**
 * Builds the schedule list
 *
 * @param notebook notebook widget
 */
static void createScheduleListView(GtkWidget *notebook)
{
    GtkWidget *scrolled_window;
    GtkTreeModel *sort_model;
    GtkTreeModel *model;
    GtkWidget *tab_label = gtk_label_new("Scheduled");

    thisListType = SCHEDULED_LIST;

    /* add scrolled window to list view */
    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled_window),
                                        GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scrolled_window),
                                   GTK_POLICY_ALWAYS,
                                   GTK_POLICY_AUTOMATIC);


    /* create tree model and view */
    thisScheduledListStore = createListStore();
    model = GTK_TREE_MODEL(thisScheduledListStore);

    /**
     * add a sort model to the list model. this will allow the sort
     * to toggle from ascending, descending then back to the orignal setting.
     * without it, the sort will toggle between ascending and descending,
     * never returning to the original state.
     */
    sort_model = gtk_tree_model_sort_new_with_model (GTK_TREE_MODEL(model));
    thisScheduledTreeView = gtk_tree_view_new_with_model(sort_model);
    gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(thisScheduledTreeView), True);
    g_object_unref(model);
    g_object_unref(sort_model);

    thisScheduleHandlerId = g_signal_connect(G_OBJECT(GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(thisScheduledTreeView)))),
                                             "changed",
                                             G_CALLBACK(selectionChangeCb),
                                             sort_model);
    /* need the motion event */
    gtk_widget_set_events(thisScheduledTreeView, GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK);
    g_signal_connect(G_OBJECT(thisScheduledTreeView), "motion-notify-event",
                     G_CALLBACK(listboxDragCb), NULL);

    /* attach gdouble-click handler */
    g_signal_connect(GTK_TREE_VIEW(thisScheduledTreeView),
                     "row-activated",
                     G_CALLBACK(rowActivatedCb),
                     NULL);

    /* add view to scrolled window */
    gtk_container_add(GTK_CONTAINER (scrolled_window), thisScheduledTreeView);

    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scrolled_window, tab_label);

    /* add columns for the scheduled view */
    addColumns(thisScheduledTreeView);
}

/**
 * Builds the all view list
 *
 * @param notebook notebook widget
 */
static void createAllListView(GtkWidget *notebook)
{
    GtkWidget *scrolled_window;
    GtkTreeModel *sort_model;
    GtkTreeModel *model;
    GtkWidget *tab_label = gtk_label_new("All");

    thisListType = ALL_LIST;

    /* add scrolled window to list view */
    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled_window),
                                        GTK_SHADOW_ETCHED_IN);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scrolled_window),
                                   GTK_POLICY_NEVER,
                                   GTK_POLICY_AUTOMATIC);

    /* create tree model and view */
    thisAllListStore = createListStore();
    model = GTK_TREE_MODEL(thisAllListStore);

    /**
     * add a sort model to the list model. this will allow the sort
     * to toggle from ascending, descending then back to the orignal setting.
     * without it, the sort will toggle between ascending and descending,
     * never returning to the original state.
     */
    sort_model = gtk_tree_model_sort_new_with_model (GTK_TREE_MODEL (model));
    thisAllTreeView = gtk_tree_view_new_with_model (sort_model);
    gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (thisAllTreeView), True);
    g_object_unref(model);
    g_object_unref(sort_model);

    thisAllHandlerId = g_signal_connect(G_OBJECT(GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(thisAllTreeView)))),
                                        "changed",
                                        G_CALLBACK(selectionChangeCb),
                                        sort_model);

    /* need the motion event */
    gtk_widget_set_events(thisAllTreeView, GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK);
    g_signal_connect(G_OBJECT(thisAllTreeView), "motion-notify-event",
                     G_CALLBACK(listboxDragCb), NULL);

    /* attach gdouble-click handler */
    g_signal_connect(GTK_TREE_VIEW(thisAllTreeView),
                     "row-activated",
                     G_CALLBACK(rowActivatedCb),
                     NULL);

    /* add view to scrolled window */
    gtk_container_add(GTK_CONTAINER (scrolled_window), thisAllTreeView);

    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), scrolled_window, tab_label);

    /* add columns for the scheduled view */
    addColumns(thisAllTreeView);
}

/**
 * Adds the column definitions, the first three columns are used to
 * define the background, forground, and font definitions for the row.
 *
 * @param widget defines the view
 */
static void addColumns(GtkWidget *widget)
{
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkTreeView *tree_view = GTK_TREE_VIEW(widget);

    /* column for orbit numbers */
    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes ("Orbit",
                                                       renderer,
                                                       "font_desc", COLUMN_FONT,
                                                       "text", COLUMN_ORBIT,
                                                       NULL);
    gtk_tree_view_append_column (tree_view, column);
    gtk_tree_view_column_set_sort_column_id (column, COLUMN_ORBIT);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_resizable(column, True);
    gtk_tree_view_column_set_reorderable(column, False);

    /* column for tdrs id */
    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes ("TDRS ID",
                                                       renderer,
                                                       "font_desc", COLUMN_FONT,
                                                       "text", COLUMN_TDRS_ID,
                                                       NULL);
    gtk_tree_view_append_column (tree_view, column);
    gtk_tree_view_column_set_sort_column_id (column, COLUMN_TDRS_ID);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_resizable(column, True);
    gtk_tree_view_column_set_reorderable(column, False);

    /* column for AOS */
    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes ("AOS",
                                                       renderer,
                                                       "font_desc", COLUMN_FONT,
                                                       "text", COLUMN_AOS,
                                                       NULL);
    gtk_tree_view_append_column (tree_view, column);
    gtk_tree_view_column_set_sort_column_id (column, COLUMN_AOS);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_resizable(column, True);
    gtk_tree_view_column_set_reorderable(column, False);

    /* column for LOS */
    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes ("LOS",
                                                       renderer,
                                                       "font_desc", COLUMN_FONT,
                                                       "text", COLUMN_LOS,
                                                       NULL);
    gtk_tree_view_append_column (tree_view, column);
    gtk_tree_view_column_set_sort_column_id (column, COLUMN_LOS);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_resizable(column, True);
    gtk_tree_view_column_set_reorderable(column, False);

    /* column for Elapsed time */
    renderer = gtk_cell_renderer_text_new ();
    column = gtk_tree_view_column_new_with_attributes ("Elapsed Time",
                                                       renderer,
                                                       "font_desc", COLUMN_FONT,
                                                       "text", COLUMN_ELAPSED_TIME,
                                                       NULL);
    gtk_tree_view_append_column (tree_view, column);
    gtk_tree_view_column_set_sort_column_id (column, COLUMN_ELAPSED_TIME);
    gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
    gtk_tree_view_column_set_resizable(column, True);
    gtk_tree_view_column_set_reorderable(column, False);
}

/**
 * Reads the iam config file and loads the initial tab that
 * is specified. If the configuration parameter is not found
 * then the default tab (first tab) is selected.
 *
 * @param notebook
 */
static void setInitialTab(GtkNotebook *notebook)
{
    gchar tab[64];
    gint page_count;
    gint i;
    GtkWidget *child;

    /* get the initial tab to select */
    if (CP_GetConfigData(IAM_EVENT_DIALOG_INITIAL_TAB, tab) == False)
    {
        g_message("IAM_EVENT_DIALOG_INITIAL_TAB not defined in the config file!");
        return;
    }

    /* find which tab this selection beglongs to */
    page_count = gtk_notebook_get_n_pages(notebook);
    for (i=0; i<page_count; i++)
    {
        child = gtk_notebook_get_nth_page(notebook, i);
        if (child != NULL)
        {
            const gchar *label = gtk_notebook_get_tab_label_text(notebook, child);
            if (g_ascii_strcasecmp(tab, label) == 0)
            {
                gtk_notebook_set_current_page(notebook, i);
                break;
            }
        }
    }
}

/**
 * Callback that handles the page switching between 'schedule' and 'all' pages
 *
 * @param widget notebook widget
 * @param page current page
 * @param page_num current page number
 * @param user_data user data
 */
static void pageSwitchCb(GtkWidget *widget, GtkNotebookPage *page, guint page_num, gpointer user_data)
{
    GtkNotebook *notebook = GTK_NOTEBOOK(widget);
    guint oldPageNum = gtk_notebook_get_current_page(notebook);

    /* if page has not changed, ignore */
    if (page_num == oldPageNum)
    {
        return;
    }

    /* Toggle between Scheduled and ALL */
    thisListType = page_num;

    /* build the list */
    buildEventList(getTreeView(), PDH_GetData());

    /* we switched views, update predict arrow buttons */
    PD_UpdateEventArrows();
}

/**
 * Handles the print callback. It gets the data directly from the
 * current model being viewed, then traverses the list getting the
 * data. If the current view doesn't have any data, then the print
 * obvousily doesn't happen.
 *
 * @param button the print button
 * @param data user data
 */
static void printCb(GtkButton *button, gpointer data)
{
    GtkTreeIter iter;
    GValue value={0};
    const gchar *orbit = NULL;

    /* the tree view model will be the sort model */
    GtkTreeModel *sort_model = gtk_tree_view_get_model(getTreeView());

    /* does this view have any data, for schedule view it could be empty */
    if (gtk_tree_model_get_iter_first(sort_model, &iter) == True)
    {
        gtk_tree_model_get_value(sort_model, &iter, COLUMN_ORBIT, &value);
        orbit = g_value_get_string(&value);
        if (orbit != NULL)
        {
            GtkWidget *dialog = createTimePrintDlg();
            openTimePrintDlg(dialog);
        }
    }
}

/**
 * Callback method that handles the toggling of the gmt/utc time
 * formats.
 *
 * @param button button widget
 * @param data user data
 */
static void timeCb(GtkButton *button, gpointer data)
{
    /* this call will update the time type for the whole app */
    ED_UpdateTimeType(True);

    /* these calls only convert and update the visual */
    RTD_UpdateTimeType(False);
    PD_UpdateTimeType(False);
}

/**
 * Handles the reset callback. Resets the selections in the list
 * view back to the original settings.
 *
 * @param button the button making the call
 * @param user_data user data
 */
static void resetCb(GtkButton *button, gpointer user_data)
{
    /* reload the original data */
    ED_Reset();

    /* turn button off */
    setEventButtons(False);
}

/**
 * Handles the gdouble-click event for the schedule and all list views.
 * Calls the select event method that processes the current selection.
 *
 * @param tree_view not used
 * @param path not used
 * @param column not used
 * @param data not used
 */
static void rowActivatedCb(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data)
{
    selectEvent();
}

/**
 * Processes the update button event. Calls the select event method
 * that processes the current selection.
 *
 * @param button the event dialog instance
 * @param user_data user data
 */
static void updateCb(GtkButton *button, gpointer user_data)
{
    selectEvent();
}

/**
 * Handles the mouse motion event. When the user clicks the
 * first mouse button and drags the mouse, the events within the
 * mouse selection will be selected.
 *
 * @param listbox
 * @param event
 * @param data
 *
 * @return gboolean TRUE or FALSE
 */
static gboolean listboxDragCb(GtkWidget *listbox, GdkEventMotion *event, gpointer data)
{
    gint x, y;
    GdkModifierType state;
    GtkTreeView *view = GTK_TREE_VIEW(listbox);
    static GtkTreePath *start_path = NULL;
    GtkTreePath *end_path = NULL;

    /* get event data */
    if (event->is_hint)
    {
        gdk_window_get_pointer (event->window, &x, &y, &state);
    }
    else
    {
        x = (gint)event->x;
        y = (gint)event->y;
        state = (GdkModifierType)event->state;
    }

    /* process only when the user has clicked and dragged the mouse */
    if (state & GDK_BUTTON1_MASK)
    {
        GtkTreeSelection *select = NULL;

        /* first time load the start_path */
        /* after that load the end_path */
        if (start_path == NULL)
        {
            if (! gtk_tree_view_get_path_at_pos(view, x, y, &start_path, NULL, NULL, NULL))
            {
                return FALSE;
            }
        }
        else
        {
            if (! gtk_tree_view_get_path_at_pos(view, x, y, &end_path, NULL, NULL, NULL))
            {
                return FALSE;
            }
        }

        /* get the selection from the view */
        select = gtk_tree_view_get_selection(view);
        if (select == NULL)
        {
            start_path = NULL;
            return FALSE;
        }

        /* select when boths paths are set */
        if (start_path != NULL && end_path != NULL)
        {
            gtk_tree_selection_unselect_all(select);
            gtk_tree_selection_select_range(select, start_path, end_path);
        }

        /* done with the end_path */
        gtk_tree_path_free(end_path);
    }
    else
    {
        /* release start_path - no glonger selecting */
        if (start_path != NULL)
        {
            gtk_tree_path_free(start_path);
            start_path = NULL;
        }
    }

    return FALSE;
}

/**
 * callback method that handles the locking (or not) of the
 * selected events while the user is travsering the selections.
 *
 * @param button
 * @param data
 */
static void lockselCb(GtkToggleButton *button, gpointer data)
{
    GList *glist;

    /* get the checkbox state */
    thisLockSelState = gtk_toggle_button_get_active(button);

    // set the locl select flag for each event being displayed
    for (glist = PDH_GetData(); glist != NULL; glist = glist->next)
    {
        PredictSet *ps = (PredictSet *)glist->data;
        if (ps->displayed & DISPLAY_ED)
        {
            ps->locksel = (thisLockSelState == True ? True : False);
        }
        else
        {
            ps->locksel = False;
        }
    }
}

/**
 * Processes the current selection. Finds which predict event is currently
 * marked for viewing in the event dialog, then marks it for displaying in
 * the predict display. Eventually, the predict buttons and the event dialog
 * button visuals are set based on the current state of the data.
 */
static void selectEvent(void)
{
    GList *glist;
    PredictSet *ps;

    /* select events to be displayed or not in the predict dialog */
    for (glist = PDH_GetData(); glist != NULL; glist = glist->next)
    {
        ps = (PredictSet *)glist->data;

        /* if being displayed in the event dialog
           make it viewable in predict dialog */
        if (ps->displayed & DISPLAY_ED)
        {
            ps->locksel = True;
            ps->displayed |= DISPLAY_PD;
        }
        else
        {
            /* if being displayed in the predict dialog, clear it */
            if (ps->displayed & DISPLAY_PD)
            {
                ps->locksel = False;
                ps->displayed -= DISPLAY_PD;
            }
        }
    }

    /* do some updating to make changes happen */
    PDH_UpdateEvents();
    PD_UpdateEventArrows();
    setEventButtons(False);
    PD_DrawData(True);
}

/**
 * Handles the window delete event callback, closes the event window
 *
 * @param widget widget making call
 * @param event event data
 * @param user_data user data
 *
 * @return True
 */
static gboolean deleteEventCb(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    closeWindow();
    return(True);
}

/**
 * Handles the close button event
 *
 * @param button the button making the call
 * @param user_data user data
 */
static void closeCb(GtkButton *button, gpointer user_data)
{
    closeWindow();
}

/**
 * Closes the event dialog
 */
static void closeWindow()
{
    if (thisWindow != NULL)
    {
        /* if the current view is schedule and there is no data */
        /* then reset the 'listed' as if 'all' view was showing */
        if (thisListType == SCHEDULED_LIST)
        {
            GtkTreeIter iter;
            GtkTreeModel *model = gtk_tree_view_get_model(getTreeView());

            /* any rows listed ? */
            if (gtk_tree_model_get_iter_first(model, &iter) == False)
            {
                /* set 'listed' for all records */
                /* the schdeduled list has no data */
                GList *glist = PDH_GetData();
                for (; glist != NULL; glist = g_list_next(glist))
                {
                    PredictSet *ps = (PredictSet *)glist->data;
                    if (ps != NULL)
                    {
                        ps->listed = True;
                    }
                }

                /* update the arrow buttons in the predict dialog */
                PD_UpdateEventArrows();
            }
        }

        /* release resources */
        gtk_widget_destroy(thisWindow);
        thisWindow = NULL;
        ED_Destructor();
    }
}

/**
 * Processes the selection 'change' (single-click) callback for the list. The
 * list is traversed for all selected rows.
 *
 * @param selection tree selection
 * @param data user data
 */
static void selectionChangeCb(GtkTreeSelection *selection, gpointer data)
{
    GList *glist;
    GtkTreeModel *model = GTK_TREE_MODEL(data);

    /* reset the display state for the data points */
    resetDisplayState(PDH_GetData());

    glist = gtk_tree_selection_get_selected_rows(selection, &model);
    while (glist)
    {
        GtkTreeIter iter;
        GtkTreePath *tree_path = (GtkTreePath *)glist->data;
        if (gtk_tree_model_get_iter(model, &iter, tree_path) == True)
        {
            PredictSet *ps;
            gtk_tree_model_get(model, &iter,
                               COLUMN_DATA, &ps,
                               -1);

            if (ps)
            {
                ps->displayed |= DISPLAY_ED;
            }
        }

        glist = g_list_next(glist);
    }

    /* selection occured, make buttons available */
    setEventButtons(True);
}

/**
 * This method processes the arrow buttons activate callback. This is activated
 * by selecting the carriage return. The user data has the direction which to
 * move the event.
 *
 * @param widget the event button widget
 * @param user_data which direction to move the event.
 */
static void activateEventCb(GtkWidget *widget, gpointer user_data)
{
    setEvents(GPOINTER_TO_INT(user_data));
}

/**
 * This method processes the arrow buttons for the events selection.
 *
 * @param widget calling widget
 * @param user_data user data
 */
static void pressedEventCb(GtkWidget *widget, gpointer user_data)
{
    thisTimerInfo.timer_state = REPEAT_STAGE0_DELAY;
    thisTimerInfo.timer_count = 0;
    thisTimerInfo.user_data = user_data;
    thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE0_DELAY, (GSourceFunc)timerFunc, NULL);
}

/**
 * When the button (arrows) are released, turn off the repeat timer.
 * Also if the arrow buttons are no glonger accesible, remove the timer.
 *
 * @param widget button widget
 * @param user_data has the timer info
 */
static void releasedCb(GtkWidget *widget, gpointer user_data)
{
    thisTimerInfo.timer_state = REPEAT_TIMER_STOP;
}

/**
 * While the arrow button is held down, the timer will continue processing
 * the 'psuedo' press event by coming here. If the button is becomes
 * insensitive, then the timer is removed.
 *
 * @param user_data has the timer info
 *
 * @return True to keep the timer going, False to stop
 */
static gint timerFunc(gpointer user_data)
{
    gint retval = False;

    /* process request */
    setEvents(GPOINTER_TO_INT(thisTimerInfo.user_data));

    /* repeat state machine */
    switch (thisTimerInfo.timer_state)
    {
        /* enable slow repeat */
        case REPEAT_STAGE0_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_STAGE1_DELAY;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE1_DELAY, (GSourceFunc)timerFunc, NULL);
            break;

            /* is it time for a fast repeat ? */
        case REPEAT_STAGE1_DELAY:
            if (thisTimerInfo.timer_count++ > 50)
                thisTimerInfo.timer_state = REPEAT_STAGE2_DELAY;
            retval = True;
            break;

            /* start really fast repeat */
        case REPEAT_STAGE2_DELAY:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_state = REPEAT_TIMER_CONTINUE;
            thisTimerInfo.timer_id = g_timeout_add(REPEAT_STAGE2_DELAY, (GSourceFunc)timerFunc, NULL);
            break;

            /* place holder */
        case REPEAT_TIMER_CONTINUE:
            retval = True;
            break;

            /* shutdown timer - comes from release callback */
        case REPEAT_TIMER_STOP:
        default:
            if (thisTimerInfo.timer_id)
                g_source_remove(thisTimerInfo.timer_id);
            thisTimerInfo.timer_id = 0;
            thisTimerInfo.timer_state = 0;
            thisTimerInfo.timer_count = 0;
            thisTimerInfo.user_data = NULL;
            return False;
    }

    /* do we need to release timer resources */
    /* this method checks to see we're out of events */
    if ((retval = setRepeatTimer()) == False)
    {
        if (thisTimerInfo.timer_id)
            g_source_remove(thisTimerInfo.timer_id);
        thisTimerInfo.timer_id = 0;
        thisTimerInfo.timer_state = 0;
        thisTimerInfo.timer_count = 0;
        thisTimerInfo.user_data = NULL;
    }

    return retval;
}

/**
 * Processes the arrow button event. The direction indicates which
 * item is selected in the list.
 *
 * @param which_direction process data either as Next or Prev
 */
static void setEvents(guint which_direction)
{
    /* get the tree view, then the tree selection */
    GtkTreeView *tree_view = getTreeView();
    GtkTreeSelection *selection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(tree_view));

    /* the tree view model will be the sort model */
    GtkTreeModel *sort_model = gtk_tree_view_get_model(tree_view);

    /* from the sort model get the child model */
    GtkTreeModel *child_model = gtk_tree_model_sort_get_model(GTK_TREE_MODEL_SORT(sort_model));

    GList *glist = gtk_tree_selection_get_selected_rows(selection, &child_model);
    if (glist)
    {
        if (g_list_length(glist) == 1)
        {
            GtkTreePath *tree_path;
            GtkTreeIter iter;
            PredictSet *ps;

            resetDisplayState(PDH_GetData());

            tree_path = (GtkTreePath *)glist->data;
            gtk_tree_selection_unselect_path(selection, tree_path);

            if (gtk_tree_model_get_iter(child_model, &iter, tree_path) == True)
            {
                /* get aos and los from the data model */
                gtk_tree_model_get(child_model, &iter,
                                   COLUMN_DATA, &ps,
                                   -1);

                /* indicate that this predict is no longer being displayed */
                if (ps)
                {
                    ps->locksel = False;
                    ps->displayed -= DISPLAY_PD;
                }
            }

            /* select the event in the list */
            if (which_direction == thisRightArrowSelected)
            {
                gtk_tree_path_next(tree_path);
            }
            else
            {
                gtk_tree_path_prev(tree_path);
            }

            gtk_tree_selection_select_path(selection, tree_path);
            if (gtk_tree_model_get_iter(child_model, &iter, tree_path) == True)
            {
                /* get aos and los from the data model */
                gtk_tree_model_get(child_model, &iter,
                                   COLUMN_DATA, &ps,
                                   -1);

                /* indicate that this predict is being displayed */
                if (ps)
                {
                    ps->locksel = True;
                    ps->displayed |= ((DISPLAY_ED) + (DISPLAY_PD));
                }
            }

            /* scroll selection to center of view */
            gtk_tree_view_scroll_to_cell(tree_view, tree_path, NULL, True, 0.5, 0.0);

            PDH_UpdateEvents();
            PD_UpdateEventArrows();
            PD_DrawData(True);
        }
    }

    /* set the buttons off */
    setEventButtons(False);
}

/**
 * This method resets the display variable in the predict data set to indicate
 * that the points are currently being viewed in the predict and/or real-time
 * dialogs.
 *
 * @param sets doubly-link list of predict sets
 */
static void resetDisplayState(GList *sets)
{
    GList *glist;
    PredictSet *ps;

    /* traverse the predict list */
    for (glist = sets; glist != NULL; glist = glist->next)
    {
        /* get the data and reset the display value */
        ps = (PredictSet *)glist->data;
        ps->locksel = False;
        ps->displayed &= ((DISPLAY_PD) + (DISPLAY_RD));
    }
}

/**
 * This method sets the display variable only if the checkFlag indicates the
 * displayed flag is set
 *
 * @param sets doubly-link list of predict sets
 * @param checkFlag check flag
 * @param setFlag set flag
 */
static void set_display_state(GList *sets, gint checkFlag, gint setFlag)
{
    GList *glist;
    PredictSet *ps;

    /* traverse the predict list */
    for (glist = sets; glist != NULL; glist = glist->next)
    {
        /* get the data */
        ps = (PredictSet *)glist->data;

        /* is it checked? */
        if (ps->displayed & checkFlag)
        {
            ps->displayed |= setFlag;
        }
    }
}

/**
 * This method sets the reset/update buttons sensitivity using the flag argument.
 * Also the up/down predict arrows are set baed on availability of data gint the
 * predict list.
 *
 * @param flag sensitivity flag
 */
static void setEventButtons(gboolean flag)
{
    ButtonInfo button_info = {False, False};

    /* the tree view model will be the sort model */
    GtkTreeModel *sort_model = gtk_tree_view_get_model(getTreeView());

    /* see which buttons are available */
    gtk_tree_model_foreach(GTK_TREE_MODEL(sort_model), foreachButtonFunc, &button_info);

    /* set the button */
    gtk_widget_set_sensitive(thisLeftButton, button_info.prev);
    gtk_widget_set_sensitive(thisRightButton, button_info.next);

    gtk_widget_set_sensitive(thisResetButton, flag);
    gtk_widget_set_sensitive(thisUpdateButton, flag);
}

/**
 * Determines if the repeat timer should continue or not. If the timer is on
 * and the button that represents the direction that is being processed becomes
 * insensitive, then turn off the timer.
 *
 * @return True if timer is stopped, otherwise False
 */
static gboolean setRepeatTimer(void)
{
    gint direction = GPOINTER_TO_INT(thisTimerInfo.user_data);
    if (direction == thisRightArrowSelected)
    {
        return GTK_WIDGET_IS_SENSITIVE(thisRightButton);
    }

    return GTK_WIDGET_IS_SENSITIVE(thisLeftButton);
}

/**
 * This method updates the shoChange flag in the predict data set.
 *
 * @param sets doubly-link list of predict sets
 */
static void setShosChanged(GList *sets)
{
    GList *glist;
    PredictSet *ps;

    for (glist = sets; glist != NULL; glist = glist->next)
    {
        ps = (PredictSet *)glist->data;

        if (ps->ShoChanged == True)
        {
            thisShosChanged = True;
            ps->ShoChanged = False;
        }
    }

    if (thisShosChanged == True)
    {
        if (thisWindow != NULL)
        {
            gchar *title = "SHO Updates";
            gtk_window_set_title(GTK_WINDOW(thisWindow), title);
        }
        gtk_widget_set_sensitive(thisResetButton, True);
    }
}

/**
 * This function resets the list in the list view to match the
 * data currently displayed in the prediction display
 */
void ED_Reset(void)
{
    GList *glist;
    PredictSet *ps;

    GtkTreeView *tree_view = NULL;
    GtkTreePath *tree_path = NULL;
    GtkTreeIter iter;

    /* make sure dialog is valid */
    if (thisWindow != NULL)
    {
        /* get the data items */
        glist = PDH_GetData();
        if (glist != NULL)
        {
            gboolean valid;
            gulong handler_id;
            GtkWidget *instance;

            GtkTreeSelection *selection;
            GtkTreeModel *model;

            tree_view = getTreeView();
            instance = (GtkWidget *)GTK_TREE_SELECTION(gtk_tree_view_get_selection(tree_view));

            /* disable selection change callback */
            handler_id = (thisListType == ALL_LIST ?
                          thisAllHandlerId :
                          thisScheduleHandlerId);
            g_signal_handler_block(instance, handler_id);

            /* get selection and model */
            selection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(tree_view));
            model = gtk_tree_view_get_model(tree_view);

            /* deselect all items */
            gtk_tree_selection_unselect_all(selection);

            /* Get the first iter in the list */
            valid = gtk_tree_model_get_iter_first(model, &iter);
            for (; glist != NULL && valid == True; glist = glist->next)
            {
                ps = (PredictSet *)glist->data;

                /* is predict set valid? */
                if (ps != NULL)
                {
                    /* the predict set can have entries not listed */
                    /* position to the first item listed */
                    if (ps->listed == False)
                    {
                        continue;
                    }

                    /* verify that its being displayed and listed */
                    if (ps->displayed & DISPLAY_PD && ps->listed == True)
                    {
                        /* make it displayed in the event dialog */
                        ps->displayed |= DISPLAY_ED;
                        gtk_tree_selection_select_iter(selection, &iter);

                        /* scroll selection to center of view */
                        tree_path = gtk_tree_model_get_path (model, &iter);
                        gtk_tree_view_scroll_to_cell(tree_view, tree_path, NULL, True, 0.5, 0.0);
                    }

                    /* get next row in list */
                    valid = gtk_tree_model_iter_next(model, &iter);
                }
            }

            /* set the buttons off */
            setEventButtons(False);

            /* enable selection change callback */
            g_signal_handler_unblock(instance, handler_id);
        }
    }
}

/**
 * This method updates the list of possible and displayed tracks. It is
 * called if a new data file is received when the select event dialog
 * is opened.
 */
void ED_UpdateList(void)
{
    if (thisWindow != NULL)
    {
        /* build new list */
        buildEventList(getTreeView(), PDH_GetData());
    }
}

/**
 * Changes the time type
 *
 * @param updateTime_type indicates whether or not to update the time type
 * flag or just use the current value.
 */
void ED_UpdateTimeType(gboolean updateTime_type)
{
    /* get current time type in effect */
    gint old_time_type = AM_GetTimeType();
    gint new_time_type = TS_UNKNOWN;

    /* update the time type ? */
    if (updateTime_type == True)
    {
        new_time_type = AM_UpdateTimeType();
    }
    else
    {
        new_time_type = old_time_type;
    }

    /* make sure button is valid */
    if (thisTimeButton != NULL)
    {
        /* change button text */
        gtk_label_set_text(GTK_LABEL(GTK_BIN(thisTimeButton)->child), GetTimeName(new_time_type));

        /* now that the time has been changed, update the list */
        updateTime();
    }
}

/**
 * Build the event list data elements. After the data is loaded, the list
 * elements are selected based on the data points currently being viewed.
 *
 * @param tree_view the view
 */
static void buildEventList(GtkTreeView *tree_view, GList *sets)
{
    GList *glist;
    PredictSet *ps;

    gchar orbit[10];
    gchar tdrs_id[10];
    gchar aos[TIME_STR_LEN];
    gchar los[TIME_STR_LEN];
    gchar elapsed[TIME_STR_LEN];
    gchar *aoslos;

    gint time_type = AM_GetTimeType();

    glong shoStartSecs = 0;
    glong shoStopSecs  = 0;

    GtkTreeSelection *treeSelection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(tree_view));
    gtk_tree_selection_set_mode(treeSelection, GTK_SELECTION_MULTIPLE);

    /* reset the display state variable */
    resetDisplayState(sets);

    /* traverse the data list */
    for (glist = sets; glist != NULL; glist = glist->next)
    {
        ps = (PredictSet *)glist->data;

        g_stpcpy(tdrs_id, ps->eventName);
        tdrs_id[6] = '\0';

        g_sprintf(orbit, "%02d", ps->orbit);

        switch (thisListType)
        {
            case ALL_LIST:
                {
                    g_stpcpy(aos, ps->aosStr);
                    g_stpcpy(los, ps->losStr);

                    if (time_type == TS_GMT)
                    {
                        UtcToGmt(aos);
                        UtcToGmt(los);
                    }
                    else if (time_type == TS_PET)
                    {
                        UtcToPet(aos);
                        UtcToPet(los);
                    }

                    SecsToStr(TS_ELAPSED, (ps->losSecs - ps->aosSecs),  elapsed);
                    StripTimeBlanks(elapsed);
                }
                break;

            case SCHEDULED_LIST:
                {
                    if (g_ascii_strcasecmp(ps->shoStart,UNSET_TIME) != 0)
                    {
                        shoStartSecs = StrToSecs(TS_UTC, ps->shoStart);
                        if (shoStartSecs <= ps->aosSecs)
                        {
                            g_stpcpy(aos, ps->aosStr);
                        }
                        else
                        {
                            g_stpcpy(aos, ps->shoStart);
                        }
                        shoStopSecs = StrToSecs(TS_UTC, ps->shoEnd);

                        if (shoStopSecs >= ps->losSecs)
                        {
                            g_stpcpy(los, ps->losStr);
                        }
                        else
                        {
                            g_stpcpy(los, ps->shoEnd);
                        }
                    }
                    else
                    {
                        g_stpcpy(aos, UNSET_TIME);
                        g_stpcpy(los, UNSET_TIME);
                    }

                    if (time_type == TS_GMT)
                    {
                        UtcToGmt(aos);
                        UtcToGmt(los);
                    }
                    else if (time_type == TS_PET)
                    {
                        UtcToPet(aos);
                        UtcToPet(los);
                    }

                    if ((shoStartSecs >= ps->aosSecs) && (shoStopSecs <= ps->losSecs))
                    {
                        SecsToStr(TS_ELAPSED, (shoStopSecs - shoStartSecs),  elapsed);
                    }

                    if ((shoStartSecs == ps->aosSecs) && (shoStopSecs > ps->losSecs))
                    {
                        SecsToStr(TS_ELAPSED, (ps->losSecs - shoStartSecs),  elapsed);
                        aoslos = g_strconcat(los, "*", NULL);
                        g_stpcpy(los, aoslos);
                        g_free(aoslos);
                    }
                    else if ((shoStartSecs < ps->aosSecs) && (shoStopSecs <= ps->losSecs))
                    {
                        SecsToStr(TS_ELAPSED, (shoStopSecs - ps->aosSecs),  elapsed);
                        aoslos = g_strconcat(aos, "*", NULL);
                        g_stpcpy(aos, aoslos);
                        g_free(aoslos);
                    }

                    else if ((shoStartSecs > ps->aosSecs) && (shoStopSecs > ps->losSecs))
                    {
                        SecsToStr(TS_ELAPSED, (ps->losSecs - shoStartSecs),  elapsed);
                        aoslos = g_strconcat(los, "*", NULL);
                        g_stpcpy(los, aoslos);
                        g_free(aoslos);
                    }
                    else if ((shoStartSecs < ps->aosSecs) && (shoStopSecs > ps->losSecs))
                    {
                        SecsToStr(TS_ELAPSED, (ps->losSecs - ps->aosSecs),  elapsed);
                        aoslos = g_strconcat(aos, "*", NULL);
                        g_stpcpy(aos, aoslos);
                        g_free(aoslos);

                        aoslos = g_strconcat(los, "*", NULL);
                        g_stpcpy(los, aoslos);
                        g_free(aoslos);
                    }

                    StripTimeBlanks( elapsed );

                    if (ps->ShoChanged)
                    {
                        g_stpcpy(orbit, "Chg");
                    }
                }
                break;

            default:
                g_warning("buildEventList: list type undefined");
                break;
        }

        /* save data in text structure */
        g_stpcpy(ps->text.orbit, orbit);
        g_stpcpy(ps->text.tdrs_id, tdrs_id);
        g_stpcpy(ps->text.aos, aos);
        g_stpcpy(ps->text.los, los);
        g_stpcpy(ps->text.elapsed, elapsed);
    }

    /* get the model and detach prior to adding lots of rows */
    /* later we'll reattach */
    {
        GtkTreeModel *sort_model = gtk_tree_view_get_model(tree_view);
        g_object_ref(sort_model);

        /* dettach model */
        gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view), NULL);

        /* load all the data */
        loadEventList(sets);

        /* reattach model */
        gtk_tree_view_set_model(tree_view, sort_model);
        g_object_unref(sort_model);

        gdk_flush();
    }

    /* do selections based on points currently being shown */
    ED_Reset();

    /* set the display state */
    set_display_state(sets, ((DISPLAY_PD)+(DISPLAY_RD)), (DISPLAY_ED));

    /* set the button states off */
    setEventButtons(False);

    /* set the state of the sho change flag */
    setShosChanged(sets);
}

/**
 * Determines if the data should be displayed
 * @param sets PredictSet data pointer
 */
static void loadEventList(GList *sets)
{
    GList *glist;
    PredictSet *ps;
    GtkListStore *list_store;
    GtkTreeIter iter;
    PangoFontDescription *font_desc;

    list_store = (thisListType == ALL_LIST ?
                  thisAllListStore :
                  thisScheduledListStore);

    /* clear the list */
    gtk_list_store_clear(list_store);

    /*
    ** create the font for the list
    */
    font_desc = pango_font_description_new();
    pango_font_description_set_family(font_desc, "Courier");
    pango_font_description_set_style(font_desc, PANGO_STYLE_NORMAL);
    pango_font_description_set_variant(font_desc, PANGO_VARIANT_NORMAL);
    pango_font_description_set_weight(font_desc, PANGO_WEIGHT_BOLD);
    pango_font_description_set_stretch(font_desc, PANGO_STRETCH_NORMAL);
    pango_font_description_set_size(font_desc, 10 * PANGO_SCALE);

    for (glist = sets; glist != NULL; glist = glist->next)
    {
        gchar orbit[10];
        gchar tdrs_id[10];
        gchar aos[TIME_STR_LEN];
        gchar los[TIME_STR_LEN];
        gchar elapsed[TIME_STR_LEN];

        ps = (PredictSet *)glist->data;

        g_stpcpy(orbit, ps->text.orbit);
        g_stpcpy(tdrs_id, ps->text.tdrs_id);
        g_stpcpy(aos, ps->text.aos);
        g_stpcpy(los, ps->text.los);
        g_stpcpy(elapsed, ps->text.elapsed);

        ps->listed = False;
        switch (thisListType)
        {
            case ALL_LIST:{
                    gtk_list_store_append (list_store, &iter);
                    gtk_list_store_set (list_store, &iter,
                                        COLUMN_FONT,         font_desc,
                                        COLUMN_ORBIT,        orbit,
                                        COLUMN_TDRS_ID,      tdrs_id,
                                        COLUMN_AOS,          aos,
                                        COLUMN_LOS,          los,
                                        COLUMN_ELAPSED_TIME, elapsed,
                                        COLUMN_DATA,         ps,
                                        -1);
                    ps->listed = True;
                }
                break;

            case SCHEDULED_LIST:{
                    if (g_ascii_strcasecmp (ps->shoStart, UNSET_TIME) != 0)
                    {
                        gtk_list_store_append (list_store, &iter);
                        gtk_list_store_set (list_store, &iter,
                                            COLUMN_FONT,         font_desc,
                                            COLUMN_ORBIT,        orbit,
                                            COLUMN_TDRS_ID,      tdrs_id,
                                            COLUMN_AOS,          aos,
                                            COLUMN_LOS,          los,
                                            COLUMN_ELAPSED_TIME, elapsed,
                                            COLUMN_DATA,         ps,
                                            -1);
                        ps->listed = True;
                    }
                }
                break;

            default:
                ps->listed = False;
                break;
        }
    }
}

/**
 * This method handles the window state of the app
 *
 * @param widget widget making call
 * @param event event data
 * @param user_data user data
 *
 * @return True to stop other handlers from being invoked for the event.
 * False to propagate the event further.
 */
static gboolean visibilityCb(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    GdkEventWindowState *windowStateEvent = (GdkEventWindowState *)event;

    if (windowStateEvent->new_window_state&GDK_WINDOW_STATE_ICONIFIED ||
        windowStateEvent->new_window_state&GDK_WINDOW_STATE_WITHDRAWN)
    {
        thisIsIconified = True;
    }
    else
    {
        if (thisIsIconified == True)
        {
            thisIsIconified = False;
        }
    }

    return(False);
}

/**
 * Handles the time conversion event. This method will update the aos and los
 * fields in the list. Sets up a foreach traversal method that does the actual
 * work. The time type is passed in through the user data field.
 */
static void updateTime(void)
{
    GtkTreeModel *sort_model = gtk_tree_view_get_model(getTreeView());
    gint time_type = AM_GetTimeType();
    GtkTreeModel *child_model = gtk_tree_model_sort_get_model(GTK_TREE_MODEL_SORT(sort_model));
    gtk_tree_model_foreach(GTK_TREE_MODEL(child_model), foreachChangeTimeFunc, GINT_TO_POINTER(time_type));
}

/**
 * Processes the time conversion for the data model. Based on the current
 * time type in effect, the AOS and LOS fields are converted and stored back
 * ginto the view.
 *
 * @param child_model has the underyling data
 * @param path path to the selection, not used
 * @param child_iter the record iterator, used to get the underyling data in the view
 * @param user_data has the time type value
 *
 * @return False continues the callback
 */
static gboolean foreachChangeTimeFunc(GtkTreeModel *child_model, GtkTreePath *path, GtkTreeIter *child_iter, gpointer user_data)
{
    PredictSet *ps;
    gchar *tdrs;
    gchar *aos_p;
    gchar *los_p;
    gchar aos[TIME_STR_LEN];
    gchar los[TIME_STR_LEN];
    gint time_type = GPOINTER_TO_INT(user_data);

    /* Note: here we use 'iter' and not '&iter', because we did not allocate
     *  the iter on the stack and are already getting the pointer to a tree iter */
    gtk_tree_model_get (child_model, child_iter,
                        COLUMN_DATA, &ps,
                        COLUMN_TDRS_ID, &tdrs,
                        COLUMN_AOS, &aos_p,
                        COLUMN_LOS, &los_p,
                        -1);

    g_stpcpy(aos, ps->aosStr);
    g_stpcpy(los, ps->losStr);

    /* convert the time */
    if (time_type == TS_GMT)
    {
        UtcToGmt(aos);
        UtcToGmt(los);
    }
    else if (time_type == TS_PET)
    {
        UtcToPet(aos);
        UtcToPet(los);
    }

    /* now look at the original string loaded */
    /* we need to see if '*' was part of the format */
    /* if it was then added to the string */
    if (g_strrstr(aos_p, "*") != NULL)
    {
        aos_p = g_strconcat(aos, "*", NULL);
        g_stpcpy(aos, aos_p);
    }

    if (g_strrstr(los_p, "*") != NULL)
    {
        los_p = g_strconcat(los, "*", NULL);
        g_stpcpy(los, los_p);
    }

    /* change the row data */
    gtk_list_store_set(GTK_LIST_STORE(child_model),  child_iter,
                       COLUMN_AOS,  aos,
                       COLUMN_LOS,  los,
                       -1);

    g_free(aos_p);
    g_free(los_p);
    g_free(tdrs);

    return(False);
}

/**
 * Creates a print dialog for the event data. The user can enter
 * data for a particular time range or simply select 'All'.
 *
 * @return dialog widget
 */
static GtkWidget *createTimePrintDlg(void)
{
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *frame;
    GtkWidget *label;
    GtkWidget *time_entry;
    GList *glist = PDH_GetData();
    GList *gdata;
    PredictSet *ps;

    GtkWidget *dialog = gtk_dialog_new_with_buttons("Enter time", GTK_WINDOW(thisWindow),
                                                    (GtkDialogFlags)(GTK_DIALOG_MODAL |
                                                                     GTK_DIALOG_DESTROY_WITH_PARENT),
                                                    GTK_STOCK_OK, GTK_RESPONSE_OK,
                                                    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                    NULL);

    gtk_container_set_border_width(GTK_CONTAINER(dialog), 8);

    vbox = gtk_vbox_new(False, 3);
    {
        gchar time_str[TIME_STR_LEN];
        gint time_type = AM_GetTimeType();

        hbox = gtk_hbox_new(False, 3);
        {
            label = gtk_label_new("AOS >=");
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);

            /* get first record */
            gdata = g_list_first(glist);
            ps = (PredictSet *)gdata->data;

            /* make AOS string for first record */
            g_stpcpy(time_str, ps->aosStr);
            if (time_type == TS_GMT)
            {
                UtcToGmt(time_str);
            }
            else if (time_type == TS_PET)
            {
                UtcToPet(time_str);
            }

            label = gtk_label_new(time_str);
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);


        hbox = gtk_hbox_new(False, 3);
        {
            frame = gtk_frame_new("Enter Time");
            gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_ETCHED_IN);
            gtk_container_set_border_width(GTK_CONTAINER(frame), 2);

            /* create time entry field and add to the dialog */
            time_entry = gtk_entry_new();
            gtk_container_add(GTK_CONTAINER(frame), time_entry);

            g_signal_connect(time_entry, "activate",
                             G_CALLBACK(activateCb),
                             NULL);
            g_signal_connect(time_entry, "key-press-event",
                             G_CALLBACK(keyPressCb),
                             NULL);

            gtk_box_pack_start(GTK_BOX(hbox), frame, True, True, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);

        hbox = gtk_hbox_new(False, 3);
        {
            /* get last record */
            gdata = g_list_last(glist);
            ps = (PredictSet *)gdata->data;

            /* make LOS string for last record */
            g_stpcpy(time_str, ps->losStr);
            if (time_type == TS_GMT)
            {
                UtcToGmt(time_str);
            }
            else if (time_type == TS_PET)
            {
                UtcToPet(time_str);
            }

            label = gtk_label_new(time_str);
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);

            label = gtk_label_new("<= LOS");
            gtk_box_pack_start(GTK_BOX(hbox), label, False, False, 2);
        }
        gtk_box_pack_start(GTK_BOX(vbox), hbox, False, False, 2);
    }

    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox);

    /* handle button events */
    g_signal_connect(dialog, "response", G_CALLBACK(responseCb), time_entry);

    return(dialog);
}

/**
 * Launches the print dialog for printing.
 *
 * @param dialog the print dialog
 */
static void openTimePrintDlg(GtkWidget *dialog)
{
    if (dialog != NULL)
    {
        gtk_widget_show_all(dialog);
    }
}

/**
 * This method handles the button events. If the user selected the 'All'
 * checkbox, then any time entered is ignored. Otherwise, the time field
 * string is retrieved and validated. The time field entered must be in
 * UTC format.
 *
 * @param widget widget making call
 * @param response_id user selection
 * @param data user data has time entry widget
 */
static void responseCb(GtkWidget *widget, gint response_id, gpointer data)
{
    gboolean doUnmanage = True;

    switch (response_id)
    {
        case GTK_RESPONSE_OK:
            doUnmanage = processPrintTime((GtkWidget *)data);
            break;

        default:
        case GTK_RESPONSE_CANCEL:
            break;
    }

    if (doUnmanage == True)
    {
        /* destroy the widget, its so small, we'll just build it each time */
        gtk_widget_destroy(widget);
        widget = NULL;
    }
}

/**
 * Handles the carriage return in the entry field widget.
 * Processes the time data that the user entered.
 *
 * @param widget the entry widget
 * @param data not used
 */
static void activateCb(GtkWidget *widget, gpointer data)
{
    processPrintTime(widget);
}

/**
 * Callback for processing the key press event for the time entry.
 *
 * @param widget calling widget
 * @param event event data
 * @param data user data
 *
 * @return True for error, False is ok.
 */
static gboolean keyPressCb(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
    guint keyval = event->keyval;
    guint key = gdk_unicode_to_keyval(keyval);

    if (g_ascii_isdigit(key) == True ||
        keyval == GDK_colon     ||
        keyval == GDK_space     ||
        keyval == GDK_slash     ||
        keyval == GDK_Home      ||
        keyval == GDK_Left      ||
        keyval == GDK_Right     ||
        keyval == GDK_Prior     ||
        keyval == GDK_Next      ||
        keyval == GDK_End       ||
        keyval == GDK_BackSpace ||
        keyval == GDK_Delete    ||
        keyval == GDK_Tab       ||
        keyval == GDK_Return    ||
        keyval == GDK_Shift_L   ||
        keyval == GDK_Shift_R   ||
        keyval == GDK_VoidSymbol)
    {
        return(False);
    }

    gdk_beep();
    return(True);
}

/**
 * Processes the entry time widget. Retrieves the data and verifies
 * that the data entered conforms to the UTC format. If data is ok
 * the request to print is sent on.
 *
 * @param widget the entry widget
 *
 * @return True data validated ok otherwise False
 */
static gboolean processPrintTime(GtkWidget *widget)
{
    gint time_type = AM_GetTimeType();

    /* validate the input, is it a valid time */
    const gchar *time_str = gtk_entry_get_text(GTK_ENTRY(widget));
    if (ValidTime(time_type, (gchar *)time_str) == True)
    {
        printEvent(PRINT_TIME, (gchar *)time_str);
    }
    else
    {
        gchar message[512];

        /* not valid, post error dialog */
        g_sprintf(message, "Time entered must be a valid %s format!",
                  (time_type == TS_UTC ? "UTC" :
                   time_type == TS_GMT ? "GMT" :
                   time_type == TS_PET ? "PET" : "TIME"));
        ErrorMessage(widget, "Invalid Time Format", message);

        return(False);
    }

    return(True);
}

/**
 * Prints the predict blockage report. This method sets up a foreach method
 * that calls each row against the event list. If the user has selected
 * 'ALL_LIST', then all data will be printed, otherwise only that data
 * that falls within the time range will be printed.
 * NOTE: PRINT_ALL NOT IMPLEMENTED
 *
 * @param which_print PRINT_ALL or PRINT_TIME
 * @param time_str if PRINT_ALL ignored (should be NULL), otherwise
 * for PRINT_TIME should have a valid time in UTC format.
 */
static void printEvent(gint which_print, gchar *time_str)
{
    /* the tree view model will be the sort model */
    GtkTreeModel *sort_model = gtk_tree_view_get_model(getTreeView());

    /* set the DISPLAY_PRT for those records that meet the time criteria */
    PrintData print_data = { AM_GetTimeType(), time_str};
    gtk_tree_model_foreach(GTK_TREE_MODEL(sort_model), foreachPrintFunc, &print_data);

    /* all points are loaded by default, */
    /* so print only those marked for printing */
    PRP_PrintPredictBlockage(PDH_GetData());

    /* clear the display print flag on the records */
    /* this clears the DISPLAY_PRT flag on the original dataset */
    gtk_tree_model_foreach(GTK_TREE_MODEL(sort_model), foreachClearFunc, NULL);
}

/**
 * The method traverses the list getting the data from the model.
 * For 'All' every record is marked as 'Displayed' otherwise only
 * those rows that meet the criteria are marked as 'Displayed'.
 *
 * @param model the current model
 * @param path the current path in the view
 * @param iter the current iterator
 * @param user_data has the file pointer
 *
 * @return True to stop the method, False to continue
 */
static gboolean foreachPrintFunc(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
    PredictSet *ps;
    gchar *aos;
    gchar *los;
    PrintData *print_data = (PrintData *)user_data;

    /* Note: here we use 'iter' and not '&iter', because we did not allocate
     *  the iter on the stack and are already getting the pointer to a tree iter,
     * We need the PredictSet, aos, and los for processing */
    gtk_tree_model_get (model, iter,
                        COLUMN_DATA, &ps,
                        COLUMN_AOS, &aos,
                        COLUMN_LOS, &los,
                        -1);

    /* if null, print all data */
    if (print_data->time_str == NULL)
    {
        ps->displayed |= DISPLAY_PRT;
    }
    else
    {
        /* see if time is within range */
        /* our string is always utc */
        glong time_secs = StrToSecs(print_data->time_type, print_data->time_str);

        /* the current view time type may be different */
        glong aos_secs = StrToSecs(print_data->time_type, aos);
        glong los_secs = StrToSecs(print_data->time_type, los);

        if (time_secs >= aos_secs && time_secs <= los_secs)
        {
            ps->displayed |= DISPLAY_PRT;
        }
    }

    g_free(aos);
    g_free(los);

    return(False); /* do not stop walking the store, call us with next row */
}

/**
 * The method traverses the list getting the data from the model.
 * For 'All' every record is marked as 'Displayed' otherwise only
 * those rows that meet the criteria are marked as 'Displayed'.
 *
 * @param model the current model
 * @param path the current path in the view
 * @param iter the current iterator
 * @param user_data has the file pointer
 *
 * @return True to stop the method, False to continue
 */
static gboolean foreachClearFunc(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
    PredictSet *ps;

    /* Note: here we use 'iter' and not '&iter', because we did not allocate
     *  the iter on the stack and are already getting the pointer to a tree iter */
    gtk_tree_model_get (model, iter,
                        COLUMN_DATA, &ps,
                        -1);

    /* clear the print flag */
    ps->locksel = False;
    ps->displayed &= ((DISPLAY_PD) + (DISPLAY_RD));

    return(False); /* do not stop walking the store, call us with next row */
}
/**
 * Processes the model's list of data. If the current path given is selected,
 * then the next and previous items are check to see if they exist. The user
 * data has button info that represents the boolean setting for previous and
 * next states.
 *
 * @param model the current model
 * @param path the current data path
 * @param iter the current iter
 * @param user_data has ButtonInfo
 *
 * @return True to stop the method, False to continue searching.
 */
static gboolean foreachButtonFunc(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
    GtkTreeSelection *selection;

    ButtonInfo *button_data = (ButtonInfo *)user_data;
    button_data->next = False;
    button_data->prev = False;

    selection = GTK_TREE_SELECTION(gtk_tree_view_get_selection(getTreeView()));

    if (gtk_tree_selection_path_is_selected(selection, path) == True)
    {
        button_data->next = gtk_tree_model_iter_next(model, iter);
        button_data->prev = gtk_tree_path_prev(path);

        /* stop the foreach, we're done searching */
        return(True);
    }

    /* continue the search */
    return(False);
}

/**
 * Returns the current tree view based on the current list type
 *
 * @return ALL or Scheduled tree view
 */
static GtkTreeView *getTreeView(void)
{
    return((thisListType == ALL_LIST) ?
           GTK_TREE_VIEW(thisAllTreeView) :
           GTK_TREE_VIEW(thisScheduledTreeView));
}
