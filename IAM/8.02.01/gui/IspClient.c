/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written permission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gprintf.h>

#ifdef G_OS_WIN32
/* so the windows it.lib will link */
extern "C"
{
    #include "It.h"
    #include "status.h"
    extern gint _ItSetPort(ItContext, gint);
}
#else
    #include "It.h"
    #include "status.h"
    extern gint _ItSetPort(ItContext, gint);
#endif

#ifdef G_OS_WIN32
    #ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
    #endif

    #include <winsock2.h>
    #include <ws2tcpip.h>
#endif

#define PRIVATE
#include "IspClient.h"
#undef PRIVATE

#include "Conversions.h"
#include "IspHandler.h"
#include "IspSymbols.h"
#include "MemoryHandler.h"
#include "MessageHandler.h"
#include "ObsMask.h"
#include "RealtimeDataHandler.h"
#include "RealtimeDialog.h"
#include "SolarDraw.h"
#include "PredictDataHandler.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/IspClient.c 195 2013-08-05 18:29:49Z llopez1@ndc.nasa.gov $");


#define  SUBSCRIBE_MSG "Subscription to the following msid(s) failed:\n"
extern gint gmt_valid;

/**************************************************************************
* Private Data Definitions
**************************************************************************/
static ItContext    thisContext;
static guint thisReconnectTimer = 0;
static guint thisEventSourceID = 0;
static gchar         *thisSubscribeMessage = NULL;

/* need the port on reconnects */
static gint          thisPort = -1;
/*************************************************************************/

/**
 * This function starts the ISP client process and sets up all of
 * the callback functions.
 *
 * @param argc count of arguments
 * @param argv the argument list
 */
void IC_StartIsp(gint argc, gchar **argv)
{
    /* only save the port if the args have the -P argument */
    /* do this before calling ItInitialize, it will altered the argv */
    thisPort = getPort(argc, argv);
    g_message("Port Number=%d",thisPort);

    /* initialize with ISP, argc/argv will be modified when done */
    if (ItInitialize(&thisContext, &argc, argv) == ISPERROR)
    {
        g_critical("ISP failed to initialize!\n");
        ErrorMessage(AM_GetActiveDisplay(), "ISP Init Error",
                     "ISP Failed to initialize!"
                     "Aborting...");
        ItPrintErrorMessage();
        AM_Exit(1);
    }

    ItSetClientName( thisContext, "iam");
    ItSetClientVersion( thisContext, AM_GetVersion() );

    /* read in the dictionary to get the symbol names */
    IS_Constructor();
    IH_Constructor();

    /* set up the callback routines for ISP */
    (void) ItAddCallback(thisContext, ISPSUBSCRIBE,
                         (ItCallbackProc)subscribe_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPACCEPT,
                         (ItCallbackProc)accept_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPVALUE,
                         (ItCallbackProc)value_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPMESSAGE,
                         (ItCallbackProc)message_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPSTATUS,
                         (ItCallbackProc)status_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPCYCLE,
                         (ItCallbackProc)cycle_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPHEARTBEAT,
                         (ItCallbackProc)cycle_cb,
                         (ItCallbackData *)NULL);

    (void) ItAddCallback(thisContext, ISPDISCONNECT,
                         (ItCallbackProc)disconnect_cb,
                         (ItCallbackData *)NULL);

    /* Connect to the server */
    connectISP();
}

/**
 * This function stops the ISP client process and disconnects
 * from the server
 */
void IC_DisconnectIsp(void)
{
    if (ItGetConnectedToServerFlag(thisContext))
    {
        /* disconnect from the server */
        ItDisconnectServer(thisContext);

        /* release resources */
        ItContextFree(&thisContext);
    }
}

/**
 * This function publishes a new value for an ISP message.
 *
 * @param message message to publish
 * @param symbol symbol associated with message
 *
 * @return True or False
 */
gint IC_PublishIspMsg(gchar *message, gchar *symbol)
{
    gint rc = False;

    /* need to request a publish of the symbol */
    if (ItPublishByType(thisContext, symbol, False, ISP_ASCII) == ISPOK)
    {
        /* publish message */
        ItPublishMessage(thisContext, symbol, message, (gint)(strlen(message)+1), 0.0);
        rc = True;
    }

    /* this forces the message_cb to be called for the originating iam client */
    ItRefreshSymbols(thisContext);

    return rc;
}

/**
 * This function displays in a message dialog the current ISP
 * connection status.
 *
 * @param parent parent window for dialog
 */
void IC_DisplayIspStatus(GtkWidget *parent)
{
    gchar message[256];
    gchar *host;
    gint port;
    gchar *source;
    gchar *owner;
    gchar *mode;
    gint connected;

    if (thisContext != NULL)
    {
        host      = ItGetHost(thisContext);
        port      = ItGetPort(thisContext);
        source    = ItGetSource(thisContext);
        owner     = ItGetOwner(thisContext);
        mode      = ItGetMode(thisContext);
        connected = ItGetConnectedToServerFlag(thisContext);

        g_sprintf(message,
                "ISP server %s connected!\n"
                "[H=%s P=%d S=%s O=%s M=%s]",
                (connected ? "is" : "is not"),
                host, port, source, owner, mode);

        free(host);
        free(source);
        free(owner);
        free(mode);
    }

    else
    {
        g_stpcpy(message, "ISP server is NOT connected!");
    }

    InfoMessage(parent, "ISP Server", message);
}

/**
 * Sets the port if its a valid number (>0)
 *
 */
static void setPort(void)
{
    if (thisPort > 0)
    {
        /* for -P we need to reset the PORT */
        g_message("Set port %d", thisPort);
        _ItSetPort(thisContext, thisPort);
    }
}

/**
 * Determines if the ISP PORT option is inuse. First the
 * argument list is checked, if not found there, then
 * the environment is checked.
 *
 * @param argc count of arguments
 * @param argv the arguments
 *
 * @return the port number or -1.
 */
static gint getPort(gint argc, gchar **argv)
{
    gint port = -1;
    gchar *sPort = NULL;
    gint c;

    /* check arg list first */
    for (c=0; c<argc; c++)
    {
        g_message("argv[%d]=%s",c,(argv)[c]);
        if ((argv)[c][0] == '-')
        {
            if ((argv)[c][1] == 'P' &&
                 strlen((argv)[c]) == 2)
            {
                sscanf(argv[c+1], "%d", &port);
                break;
            }
        }
    }

    /* check environment last */
    if ((sPort = (gchar *)g_getenv("ISPPORT")) != NULL)
    {
        sscanf(sPort, "%d", &port);
    }

    return port;
}

/**
 * This routine is meant to be triggered in response to an ISPACCEPT
 * event.  The occurance of an ISPACCEPT event means that a connection
 * has been established with the server.  In response, we subscribe
 * to the entire set of parameters which we (previously) read from
 * the input file.
 *
 * @param context isp context
 * @param packet package data from isp
 * @param data user data
 */
static void accept_cb(ItContext context, ItAcceptPacketStruct *packet, ItCallbackData *data)
{
    gint i;
    SymbolRec *symbol;
    gchar *message = NULL;

    /* make sure the isp server accepts us */
    if (packet->ok == (gchar)ISPTRUE)
    {
        /* Request msids from the ISP server */
        for (i=0; i<IS_GetSymbolCount(); i++)
        {
            symbol = IS_GetSymbol(i);

            /* ignore symbols that are reserved for future use */
            if (g_ascii_strcasecmp(symbol->symbol_name, RESERVED_SYMBOL) == 0)
            {
                continue;
            }

            /* ok, subscribe to this symbol */
            if ((ItSubscribe(thisContext, symbol->symbol_name, (gchar *)(&symbol->symbol_id),
                             (guint)sizeof(gint), (guint)0))
                == ISPERROR)
            {
                if (message == NULL)
                {
                    message = (gchar *)MH_Calloc((gint)(strlen(SUBSCRIBE_MSG)+
                                                strlen(symbol->symbol_name)+1),
                                                __FILE__, __LINE__);
                    g_sprintf(message, "%s%s", SUBSCRIBE_MSG, symbol->symbol_name);
                }
                else
                {
                    MH_Realloc((void **)&message,
                               (gint)(strlen(message)+strlen(symbol->symbol_name)+2),
                               __FILE__, __LINE__);
                    g_sprintf(message, "%s\n%s", message, symbol->symbol_name);
                }
            }
        }

        /* if some symbols not subscribed to display message for user */
        if (message != NULL)
        {
            ErrorMessage(AM_GetActiveDisplay(), "ISP Error", message);
            ItPrintErrorMessage();
            MH_Free(message);
            message = NULL;
        }

        /* Tell the server when we're done requesting */
        ItEnableSymbols(thisContext);
    }
    else
    {
#define ACCEPT_FAILED_MSG "Fatal Error!\n\n"\
                          "ISP Server cannot accept requests from IAM.\n"\
                          "Contact software support.\n\n"\
                          "Exiting..."

        /* some kind of fatal error occurred */
        /* the server can't process requests */
        /* tell user then exit */
        ErrorMessage(AM_GetActiveDisplay(), "ISP Error", ACCEPT_FAILED_MSG);
        AM_Exit(0);
    }
}

/**
 * When a previous subscription request by the client (us) is rejected
 * by the server, the server "bounces" the ISPSUBSCRIBE event back to
 * the client.  This routine is invoked when such an event occurs.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void subscribe_cb(ItContext context, ItSubscribePacketStruct *packet, ItCallbackData *data)
{
    gint symbol_id;
    SymbolRec *symbol = NULL;

    /* extract the symbol array index from the packet  */
    symbol_id = *(gint*)ISPCLIENTSPECBUFFER(packet);
    symbol = IS_GetSymbol(symbol_id);

    if (symbol != NULL)
    {
        /* build subscribe error message for each invalid symbol */
        /* subscribe message will be issued in the cycle callback, just once */
        if (thisSubscribeMessage == NULL)
        {
            thisSubscribeMessage = (gchar *)MH_Calloc((gint)(strlen(SUBSCRIBE_MSG)+
                                                     strlen(symbol->symbol_name)+2),
                                                     __FILE__, __LINE__);
            g_sprintf(thisSubscribeMessage, "%s\t%s", SUBSCRIBE_MSG, symbol->symbol_name);
        }
        else
        {
            MH_Realloc((void **)&thisSubscribeMessage,
                       (gint)(strlen(thisSubscribeMessage)+strlen(symbol->symbol_name)+3),
                       __FILE__, __LINE__);
            g_sprintf(thisSubscribeMessage, "%s\n\t%s", thisSubscribeMessage, symbol->symbol_name);
        }
    }
}

/**
 * This callback routine is invoked when a ISPCYCLE event occurs, indicating
 * that the end of ISP's (once per second) data acquisition cycle has been
 * reached.  Strictly speaking, clients don't need to be terribly concerned
 * with this message.  Nevertheless, it is extremely useful to use this
 * message to "synchronize" the client's output.  Although this "feature"
 * is most important for graphical displays, it's used here also.
 *
 * Although we're not using the information in this particular callback,
 * notice that the packet argument to the routine is a pointer to
 * an integer which is a "count" of all the "change events" which arrived
 * in the last second.  "All change events" means changed values, changed
 * statuses and changed limits for any symbol.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void cycle_cb(ItContext context, ItStatusEventStruct *packet, ItCallbackData *data)
{
    static int first = True;  /*only call contructor in the beginning of cycleCB */
    /* if subscribe errors occurred, then display here */
    if (thisSubscribeMessage != NULL)
    {
        ErrorMessage(AM_GetActiveDisplay(), "ISP Error", thisSubscribeMessage);
        ItPrintErrorMessage();
        MH_Free(thisSubscribeMessage);
        thisSubscribeMessage = NULL;
    }

    /* Each handler will determine if the value/status should be updated */
    IH_UpdateRiseSet();

    IH_UpdateSBand();

    IH_UpdateKuBand();

    IH_UpdateKuMasks();

    IH_UpdateKuMisCmpr();

    IH_UpdatePathGO();

    IH_UpdateTDRSSel();

    IH_UpdateTDRS();

    IH_UpdateAngle();

    IH_AutoUpdate();

    /* update time last, this will always cause a redraw of the dialog */
    IH_UpdateTime();

    /* added constructor for Predicts after we have receieved time from ISP */
    if ((first) && (gmt_valid))
    {

    /* load the solar point data */
       SD_Constructor();

    /* initialize dynamics */
       DD_Constructor();

    /* initialize the predicts */
       PDH_Constructor();

    /* initialize the obs mask files */
//    if (OM_Constructor() == False)
//    {
//        g_critical("Failed to load the obscuration masks");
//        ErrorMessage(NULL, "Error!", "Failed to load the obscuration masks!\nAborting...");
//        AM_Exit(1);
//    }

       first = False;
    }

}

/**
 * This callback routine is invoked when there's a change in the message
 * of one of the MSIDs to which we've subscribed.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void message_cb(ItContext context, ItMessageEventStruct *packet, ItCallbackData *data)
{
    gint symbol_id;
    gint length;
    SymbolRec *symbol = NULL;

    /* extract the array index for this MSID from the packet's */
    /* client data buffer                                      */
    symbol_id = *(gint *)ISPCLIENTBUFFER(packet);
    symbol = IS_GetSymbol(symbol_id);
    if (symbol != NULL)
    {
        /* save data value in data structure so we can process it later */
        length = ISPEVENTMESSAGESIZE(packet);

        if (length > 0)
        {
            if (length >= ISP_MESSAGE_SIZE)
            {
                length = ISP_MESSAGE_SIZE - 1;
            }

            /* get length and message string */
            symbol->value_str_len = length;
            strncpy(symbol->value_str, ISPEVENTMESSAGE(packet), length);
            symbol->value_str[length] = 0;

            g_message("PROCESS MESSAGE: %s",symbol->value_str);

            /* get time */
            symbol->event_time = (gdouble)ISPEVENTTIME(packet);
            if (symbol->event_time != 0)
            {
                symbol->event_time_update = True;
            }

            symbol->status_update = True;
        }
    }
}

/**
 * This callback routine is invoked when there's a change in the status
 * of one of the MSIDs to which we've subscribed.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void status_cb(ItContext context, ItStatusEventStruct *packet, ItCallbackData *data)
{
    gint symbol_id;
    SymbolRec *symbol = NULL;

    /* extract the array index for this MSID from the packet's */
    /* client data buffer                                      */
    symbol_id = *(gint *)ISPCLIENTBUFFER(packet);
    symbol = IS_GetSymbol(symbol_id);
    if (symbol != NULL)
    {
        /* get status */
        memcpy(&symbol->status, ISPEVENTSTATUS(packet), sizeof(StStatusStruct));

        /* get time */
        symbol->event_time = (gdouble)ISPEVENTTIME(packet);
        if (symbol->event_time != 0)
        {
            symbol->event_time_update = True;
        }
        symbol->status_update = True;
    }

    return;
}

/**
 * This callback routine is invoked when there's a change in the value
 * of one of the MSIDs to which we've subscribed. If the change is to
 * one of the TDRE, TDRW, STDN, or BFS coordinate values AND all of
 * the values for that set have been changed since the last cycle
 * event, then they are sent ahead into AntMan without waiting for
 * a cycle event -- basically, when all have been updated, then all
 * are sent at once.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void value_cb(ItContext context, ItValueEventStruct *packet, ItCallbackData *data)
{
    gint symbol_id;
    SymbolRec *symbol = NULL;
    gdouble value;

    /* extract the array index for this MSID from the packet's */
    /* client data buffer                                      */
    symbol_id = *(gint *)ISPCLIENTBUFFER(packet);
    symbol = IS_GetSymbol(symbol_id);
    if (symbol != NULL)
    {
        value = ISPEVENTVALUE(packet);

        /* if value really hasn't changed, return */
        if (fabs(value - symbol->value) != 0)
        {
            /* get time */
            symbol->value = value;
            symbol->event_time = (gdouble)ISPEVENTTIME(packet);
            if (symbol->event_time != 0)
            {
                symbol->event_time_update = True;
            }
            symbol->status_update = True;
        }
    }
}

/**
 * This function attempts to connect to the ISP server.  If it fails,
 * it gives a warning message and then starts the reconnect attempt loop.
 */
static void connectISP(void)
{
  ItStatusEventStruct *packet = NULL;
  ItCallbackData *data = NULL;
    /* Ask the ISP server for a connection */
    if (ItConnectServer(thisContext) == ISPERROR)
    {
        g_message("ISP connect failed");

        ErrorMessage(AM_GetActiveDisplay(), "ISP Error",
                     "Connection to ISP server failed.");
        addTimeout();
    }
    else
    {
        g_message("Successfully connected to ISP server.");
        addIOWatch();
    }
    cycle_cb(thisContext, packet, data);
}

/**
 * This callback is invoked when the client receives an ISPDISCONNECT
 * event from the server.
 *
 * @param context isp context
 * @param packet packet data from isp
 * @param data user data
 */
static void disconnect_cb(ItContext context, ItDisconnectEventStruct *packet, ItCallbackData *data)
{
    g_message("Got disconnect callback");
    removeIOWatch();
    disconnectISP();
}

/**
 * This method does the actual isp disconnect.
 * Only disconnect if the reconnect timer is clear.
 */
static void disconnectISP(void)
{
    /* only process if timer event id is ready */
    g_message("Disconnecting from ISP");
    if (thisReconnectTimer == 0)
    {
        g_message("Disconnect will try to reconnect to ISP");
        ErrorMessage(AM_GetActiveDisplay(), "ISP Error",
                     "ISP server unexpectedly disconnected.");

        ItReinitialize(thisContext);

        /* reset the port */
        setPort();

        /* set all values in the gui to a dead status */
        IH_SetDeadStatus();

        /* try to reconnect */
        addTimeout();
    }
}

/**
 * This function is a timer callback for attempting to reconnect to
 * the ISp server.  If it succeeds, everyone's happy.  If it fails
 * then it sends a timer event to activate itself again in 10 seconds
 * to make another attempt.
 *
 * @param data user data
 *
 * @return False will remove the source, otherwise True will cause the
 * timeout to run indefinitely
 */
static gboolean reconnectISP(void * data)
{
    g_message("Reconnect ISP request");

    /* Ask the ISP server for a connection */
    if (ItConnectServer(thisContext) != ISPERROR)
    {
        g_message("Reconnect success");

        /* remove old timeout before proceeding */
        removeTimeout();

        /* remove old watch and channel */
        removeIOWatch();

        /* add new io watch to process the data */
        addIOWatch();

        InfoMessage(AM_GetActiveDisplay(), "ISP Server",
                    "Successfully re-connected to ISP server." );
    }
    else
    {
        if (thisContext != NULL)
        {
            g_message("Reconnect failed");
            ItReinitialize(thisContext);

            /* reset the port */
            setPort();
#ifdef _DEBUG
            {
                gchar *host      = ItGetHost(thisContext);
                gint port        = ItGetPort(thisContext);
                gchar *source    = ItGetSource(thisContext);
                gchar *owner     = ItGetOwner(thisContext);
                gchar *mode      = ItGetMode(thisContext);

                g_message("Host=%s Port=%d Source=%s Owner=%s Mode=%s",
                          host, port, source, owner, mode);

                free(host);
                free(source);
                free(owner);
                free(mode);
            }
#endif
        }
    }

    /* continue timer until it's explicitly removed */
    return True;
}

/**
 * Adds the watch for the socket id
 */
static void addIOWatch(void)
{
    GIOChannel *gio = g_io_channel_unix_new(ItGetClientSocket(thisContext));

    /* connect an i/o channel for input only */
    thisEventSourceID = g_io_add_watch(gio, (GIOCondition)(G_IO_IN),
                                       (GIOFunc)readSocket, NULL);

    /* don't need gio anymore*/
    g_message("Unreferencing IO Channel");
    g_io_channel_unref(gio);
    gio = NULL;
}

/**
 * This method processes the socket read on the io channel
 *
 * @param source io channel source
 * @param condition condition that got us here (looking for G_IO_IN)
 * @param data user data
 *
 * @return True to keep handler installed
 */
static gboolean readSocket(GIOChannel *source, GIOCondition condition, void * data)
{
    if (condition & G_IO_IN)
    {
        if (ItReadSocket(thisContext, NULL, NULL) == ISPERROR)
        {
            g_message("Read socket error");
        }
    }
    /* shouldn't get these, but... */
    else if ((condition & G_IO_OUT) ||
             (condition & G_IO_HUP) ||
             (condition & G_IO_PRI) ||
             (condition & G_IO_ERR) ||
             (condition & G_IO_NVAL))
    {
        /*g_message("readSocket: bad read condition = %d",(gint)condition);*/
    }
    else
    {
    }

    /* keep alive */
    return True;
}

/**
 * Adds a timeout that will install a callback to reconnect to isp.
 */
static void addTimeout(void)
{
    g_message("Add new reconnect timeout");
    thisReconnectTimer = g_timeout_add(10000, reconnectISP, NULL);
}

/**
 * Removes the timer.
 */
static void removeTimeout(void)
{
    /* clear the timer event id */
    if (thisReconnectTimer != 0)
    {
        g_message("Removing reconnect timer: %d",thisReconnectTimer);
        g_source_remove(thisReconnectTimer);
        thisReconnectTimer = 0;
    }
}

/**
 * Removes the Event Source ID that is returned from the
 * g_io_add_watch_id method. If the source id is not removed,
 * then on unix, multiple disconnect callbacks can occurr. On
 * windows nothing happens.
 */
static void removeIOWatch(void)
{
    /* make sure the source id is valid */
    if (thisEventSourceID != 0)
    {
        g_message("Removing watch id: %d",thisEventSourceID);
        g_source_remove(thisEventSourceID);
        thisEventSourceID = 0;
    }
}
