/********************************************************/
/***  Copyright (C) 2013                              ***/
/***  National Aeronautics and Space Administration.  ***/
/***  All Rights Reserved.                            ***/
/********************************************************/

/**************************************************************************
 **                                                                      **
 **              NOTICE OF COMPUTER PROGRAM USER RESTRICTIONS            **
 **                                                                      **
 ** This computer program is furnished on the condition that it be       **
 ** used only in connection with the specified cooperative project,      **
 ** grant or contract under which it is provided and that no further     **
 ** use or dissemination shall be made without prior written premission  **
 ** of the NASA forwarding office. NMI 2210.2.B (12/13/90)               **
 **                                                                      **
 **************************************************************************/
 
/************** RCS Keywords **********************************************
 *
 *
 * $Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/GetOpt.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $
 *
 *
 * $Revision$
 *
 *
 * $Log$
 *
 *
***************************************************************************/

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>

#include "GetOpt.h"
#include "keywords.h"

RCS("$Header: https://ndjsmsdxcm02.ndc.nasa.gov:9443/svn/cato/iam/trunk/gui/GetOpt.c 176 2013-02-14 00:21:09Z mcolema3@sns.mcps $");


/**************************************************************************
* Private Data Definitions
**************************************************************************/
/*************************************************************************/

/**
 * GetOpt returns the next option letter in argv that matches a letter(s) in
 * optstring. Optstring is a strng of recognized options letters; if a letter
 * is followed by colon, the option is expected to have an argument that may
 * or may not be separated from it by white space; if the optstring includes
 * the brackets '{}' with characters enclosed, the option is interpreted as
 * string rather than a single character. The character '!' will be returned
 * and the external variable 'ext_optexpand' will contain the actual argument
 * entered. The Expanded argument may contain the complete string or any
 * number of characters that belong to the string as long as the characters
 * are the sequence given in optstring. If an argument is given, the variable
 * ext_optarg will contain the argument.
 * @param argc
 * @param **argv
 * @param *opts
 * @return EOF - argument list exhausted
 */
char    *ext_optexpand;
char    *ext_optarg;

int     ext_opterr = 1;
int     ext_optind = 1;

/**
 * Processes argument list
 * @param argc count of arguments
 * @param argv the arguments
 * @param opts string to parse
 * @return ! for string option or the option character
 */
int GetOpt (int argc, char **argv, char *opts)
{
    register int opt;
    register int i;
    register char *option;
    register char *p;
    register char arg;

    static char Expand [512];
    char *fBracket;
    char *bBracket;

    if (ext_optind >= argc || argv[ext_optind][0] != '-' || argv[ext_optind][1] == '\0')
    {
        return EOF;
    }
    else if ((g_ascii_strcasecmp(argv[ext_optind], "--")) == 0)
    {
        return EOF;
    }

    /* get just the option entered, bump past the dash */
    option = (argv[ext_optind])+1;

    opt = '?';
    arg = '?';

    /* Process string args */
    if (strlen (option) > 1)
    {
        fBracket = opts;
        while ((fBracket = strchr (fBracket, '{')) != (char *)NULL)
        {
            /* Confine the search to the string between the brackets */
            bBracket = strchr (fBracket, '}');

            /* Expand has the entire argument */
            Expand [0] = '\0';
            fBracket++;
            strncpy (Expand, fBracket, (strlen(fBracket)+1-strlen(bBracket)));
            Expand[(strlen (fBracket)+1)-(strlen (bBracket)+1)] = '\0';

            /* Limit check to only those args that start with the same char */
            if (Expand[0] == option[0])
            {
                /* Did the user enter something close */
                if ((g_ascii_strncasecmp (Expand, option, strlen (Expand))) == 0)
                {
                    /* Save option and point arg to next char; check later for optional argument. */
                    opt = '!';
                    ext_optexpand = Expand;
                    arg = *(++bBracket);
                    break;
                }
            }
        }
    }

    /* Process single args, do only if string arg not found. */
    if (opt == '?')
    {
        i = 0;
        while (i < (int)strlen (opts))
        {
            /* Skip string options */
            if (opts[i] == '{')
            {
                p = strchr (&opts[i], '}');
                i += (strlen (&opts[i]) - strlen (++p));
            }
            else
            {
                if (option[0] == opts[i])
                {
                    opt = option[0];
                    arg = opts[i+1];
                    break;
                }
                i++;
            }
        }
    }

    /* Do any argument processing */
    if (arg == ':')
    {
        /* Process string args */
        if (opt == '!')
        {
            /* Is the argument with the option */
            if (option[strlen (ext_optexpand)] != '\0')
            {
                ext_optarg = &argv[ext_optind][strlen (ext_optexpand)];
            }
            else
            {
                if (++ext_optind < argc)
                {
                    ext_optarg = argv[ext_optind];

                    if (argv[ext_optind][0] != '-')
                    {
                        ext_optarg = argv[ext_optind];
                    }
                    else
                    {
                        opt = '-';
                    }
                }
                else
                {
                    opt = '-';
                }
            }
        }

        /* Process single char option args */
        else
        {
            /* Is the arg and option together ? */
            if (option[1] != '\0')
            {
                ext_optarg = &option[1];
            }
            else
            {
                if (++ext_optind < argc)
                {
                    if (argv[ext_optind][0] != '-')
                    {
                        ext_optarg = argv[ext_optind];
                    }
                    else
                    {
                        opt = '-';
                    }
                }
                else
                {
                    opt = '-';
                }
            }
        }
    }

    /* was an option found or argument needed ? */
    switch (opt)
    {
        case '?':
            {
                g_warning("%s: illegal option -- %s\n", argv[0], option);
            }
            break;

        case '-':
            {
                --ext_optind;
                g_warning("%s: option requires an argument -- %s\n", argv[0], option);
                opt = '?';
            }
            break;
    }

    ++ext_optind;

    return(opt);
}
