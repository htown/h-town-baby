import numpy as np
import math
import shutil
from numpy import genfromtxt
from sklearn.externals import joblib
import time
import os

def main():

    goodFolder = "sBandGood"
    badFolder = "sBandBad"

    save_to_folder = "binaryFolder"

    startDir = '/home/hal/Documents/H-TownHussies'

    read_from_folder = "binaryFolder/SBandData_raw"

    dir = '{}/{}'.format(startDir, read_from_folder)

    start = time.time()

    my_data = np.zeros((1,22))

    for file in os.listdir(dir):
        if file.endswith(".csv"):
            filename = file
            file = '{}/{}'.format(read_from_folder, file)
            print(file)
            # Create data from csv file. Generates into numpy arrays
            new_data = (genfromtxt(file, delimiter=',', usecols=np.arange(0, 22), skip_header=6))

            # Append data from new file to my_data
            my_data = np.append(my_data, new_data, axis=0)

            shutil.move(dir + '/' + filename, startDir + '/binaryFolder' + '/SBandData_used')

    # Format Flag
    print("Formatting Data...")

    columns_to_delete = [21, 20, 19, 0]
    # Delete first column of each row in dataset (i.e. delete timestamp)
    for column in columns_to_delete:
        my_data = np.delete(my_data, column, 1)

    # Delete the row of zeros at the beginning
    my_data = np.delete(my_data, 0, 0)

    print(" Formatting data time: {} seconds".format((time.time() - start)))

    # Shuffle Data Flag
    print("     Shuffling Data...")
    # Shuffle rows of data
    np.random.shuffle(my_data)

    print("         Shuffling data time: {} seconds".format((time.time() - start)))

    # Create a list of indices for rows that have 'nan' values in them. These will be deleted

    train_bad = np.zeros(shape=(1, 18))
    test_bad = np.zeros(shape=(1, 18))
    valid_bad = np.zeros(shape=(1, 18))

    train_good = np.zeros(shape=(1, 18))
    test_good = np.zeros(shape=(1, 18))
    valid_good = np.zeros(shape=(1, 18))
    i = 0
    g_i = 0
    b_i = 0
    print("Starting checks")
    for row in my_data:
        works = True
        # Ignore row if any values are 'nan'
        for column in row:
            if math.isnan(column):
                works = False
        if works is True:
            row = np.reshape(row, (1,18))
            if row[0][2] < 35:
                row[0][2] = 0
                if (g_i % 20) < 14:
                    train_good = np.append(train_good, row, axis=0)
                    if len(train_good) > 50000:
                        train_good = np.delete(train_good,0,0)
                        np.savetxt(
                            "{}/{}/{}/trainData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                                     time.strftime("%d-%m-%Y %H-%M-%S")),
                            train_good, delimiter=",")
                        train_good = np.zeros(shape=(1,18))
                elif (g_i % 20) < 17:
                    test_good = np.append(test_good, row, axis=0)
                    if len(test_good) > 50000:
                        test_good = np.delete(test_good, 0, 0)
                        np.savetxt(
                            "{}/{}/{}/testData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                                     time.strftime("%d-%m-%Y %H-%M-%S")),
                            test_good, delimiter=",")
                        test_good = np.zeros(shape=(1,18))
                else:
                    valid_good = np.append(valid_good, row, axis=0)
                    if len(valid_good) > 50000:
                        valid_good = np.delete(valid_good, 0, 0)
                        np.savetxt(
                            "{}/{}/{}/validationData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                                     time.strftime("%d-%m-%Y %H-%M-%S")),
                            valid_good, delimiter=",")
                        valid_good = np.zeros(shape=(1,18))
                g_i += 1
            else:
                row[0][2] = 1
                if b_i % 20 < 14:
                    train_bad = np.append(train_bad, row, axis=0)
                    if len(train_bad) > 50000:
                        train_bad = np.delete(train_bad, 0, 0)
                        np.savetxt(
                            "{}/{}/{}/trainData/{}.csv".format(startDir, save_to_folder, badFolder,
                                                               time.strftime("%d-%m-%Y %H-%M-%S")),
                            train_bad, delimiter=",")
                        train_good = np.zeros(shape=(1, 18))

                elif b_i % 20 < 17:
                    test_bad = np.append(test_bad, row, axis=0)
                    if len(test_bad) > 50000:
                        test_bad = np.delete(test_bad, 0, 0)
                        np.savetxt(
                            "{}/{}/{}/testData/{}.csv".format(startDir, save_to_folder, badFolder,
                                                              time.strftime("%d-%m-%Y %H-%M-%S")),
                            test_bad, delimiter=",")
                        test_bad = np.zeros(shape=(1, 18))
                else:
                    valid_bad = np.append(valid_bad, row, axis=0)
                    if len(valid_bad) > 50000:
                        valid_bad = np.delete(valid_bad, 0, 0)
                        np.savetxt(
                            "{}/{}/{}/validationData/{}.csv".format(startDir, save_to_folder, badFolder,
                                                                    time.strftime("%d-%m-%Y %H-%M-%S")),
                            valid_bad, delimiter=",")
                        valid_bad = np.zeros(shape=(1, 18))
                b_i += 1
            i += 1
            if i % 100000 == 0:
                print(i)


    print("saving and deleting")


    train_good = np.delete(train_good, 0, 0)
    if len(train_good) != 0:
        np.savetxt(
            "{}/{}/{}/trainData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                               time.strftime("%d-%m-%Y %H-%M-%S")),
            train_good, delimiter=",")

    test_good = np.delete(test_good, 0, 0)
    if len(test_good) != 0:
        np.savetxt(
            "{}/{}/{}/testData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                              time.strftime("%d-%m-%Y %H-%M-%S")),
            test_good, delimiter=",")

    valid_good = np.delete(valid_good, 0, 0)
    if len(valid_good) != 0:
        np.savetxt(
            "{}/{}/{}/validationData/{}.csv".format(startDir, save_to_folder, goodFolder,
                                                    time.strftime("%d-%m-%Y %H-%M-%S")),
            valid_good, delimiter=",")

    train_bad = np.delete(train_bad, 0, 0)
    if len(train_bad) != 0:
        np.savetxt(
            "{}/{}/{}/trainData/{}.csv".format(startDir, save_to_folder, badFolder,
                                               time.strftime("%d-%m-%Y %H-%M-%S")),
            train_bad, delimiter=",")

    test_bad = np.delete(test_bad, 0, 0)
    if len(test_bad) != 0:
        np.savetxt(
            "{}/{}/{}/testData/{}.csv".format(startDir, save_to_folder, badFolder,
                                              time.strftime("%d-%m-%Y %H-%M-%S")),
            test_bad, delimiter=",")

    valid_bad = np.delete(valid_bad, 0, 0)
    if len(valid_bad) != 0:
        np.savetxt(
            "{}/{}/{}/validationData/{}.csv".format(startDir, save_to_folder, badFolder,
                                                    time.strftime("%d-%m-%Y %H-%M-%S")),
            valid_bad, delimiter=",")


    print(i)

    print(len(my_data))

    print("Done with checks")


    print("Done splitting {}".format(time.time() - start))




if __name__ == '__main__':
    main()
