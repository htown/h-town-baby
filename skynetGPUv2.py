import keras
from keras.models import Sequential, load_model, save_model
from keras.layers import Dense, Input
import numpy as np
from numpy import genfromtxt
import time
import os
import shutil
import h5py
import math


def main():
    cudaCores = 5952

    dir = '/home/hal/Documents/H-TownHussies/binaryFolder/'

    trainDataPoints = loadData(dir, "trainData")
    input_data = trainDataPoints[0]
    output = trainDataPoints[1]


    testDataPoints = loadData(dir, "testData", even=False)
    test_input = testDataPoints[0]
    test_output = testDataPoints[1]


    # Fit the model
    batchSize = len(input_data) // cudaCores
    # batchSize = 32


    percentages = []

    for i in range(1):

        linearModel = Sequential()
        linearModel.add(Dense(17, input_dim=17, activation='sigmoid'))
        linearModel.add(Dense(8, input_dim=17, activation='sigmoid'))
        # linearModel.add(Dense(289, input_dim=289, activation='sigmoid'))
        # linearModel.add(Dense(289, input_dim=289, activation='sigmoid'))
        # linearModel.add(Dense(4, input_dim=8, activation='sigmoid'))
        linearModel.add(Dense(1, input_dim=8, activation='sigmoid'))

        # Compile model
        linearModel.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])


        print(i)

        linearModel.fit(input_data, output, epochs=10, batch_size= batchSize, verbose=2)

        batchSize = len(test_input) // cudaCores
        linearPredictions = linearModel.predict(test_input, batch_size=batchSize, verbose=1)

        rangeError = 0
        nnPosi = 0
        falsePos = 0
        print("Prediction\tActual")


        for i in range(len(linearPredictions)):
            # if linearPredictions[i] > 80:
            #     print("{}\t{}".format(linearPredictions[i], testOutput[i]))
            if test_output[i] == 1:
                rangeError += 1
                if  linearPredictions[i] * 2 >= 1:
                    nnPosi += 1
            if linearPredictions[i] * 2 >= 1 and test_output[i] == 0:
                falsePos += 1
            # if abs(linearPredictions[i] - output[i]) > 10:
            #     print("\nActual Output: {}\n"
            #           "Test Output: {}".format(output[i], linearPredictions[i]))

        linearCorrectness = 100 * (nnPosi / rangeError)

        print("Correctness: {}".format(linearCorrectness))

        print("False Positives: {}".format(falsePos))

        print("Range Error: {}".format(rangeError))
        percentages.append([linearCorrectness, falsePos, rangeError])
    # linearModel.save('my_model.h5')
    for i in percentages:
        print(i)

    linearModel.save("PyQt/output3.h5")

    model = load_model("PyQt/output3.h5")
    # src = '/home/hal/Desktop/H-TownHussies'
    # dst = '/home/hal/Desktop/H-TownHussies/PyQt'
    # shutil.move(src, dst)

    prediction = model.predict(input_data)
    # print(prediction)

def loadData(dir, typeData, even=True):
    # dir = /home/hal/Desktop/H-TownHussies/binaryFolder/
    # typeData = trainData

    # Initialize numpy array. All values will be zero. Will be deleted later.
    good_data = np.zeros(shape=(1, 18))
    bad_data = np.zeros(shape=(1, 18))

    print("Loading Bad Data")

    folder = dir + "sBandBad/{}".format(typeData)

    for file in os.listdir(folder):
        if file.endswith(".csv"):
            file = '{}/{}'.format(folder, file)
            print(file)
            # Create data from csv file. Generates into numpy arrays
            new_data = (genfromtxt(file, delimiter=',', usecols=np.arange(0, 18), skip_header=0))

            # Append data from new file to my_data
            bad_data = np.append(bad_data, new_data, axis=0)

    folder = dir + "sBandGood/{}".format(typeData)

    print("Loading Good Data")

    start = time.time()
    count = 0
    for file in os.listdir(folder):
        if file.endswith(".csv") and count < 10:
            file = '{}/{}'.format(folder, file)
            print(file)
            # Create data from csv file. Generates into numpy arrays
            new_data = (genfromtxt(file, delimiter=',', usecols=np.arange(0, 18), skip_header=0))

            # Append data from new file to my_data
            good_data = np.append(good_data, new_data, axis=0)
            count+=1

    # Create list for output using column 2 and then convert to numpy array. Col 2 is deleted from every row


    print("\tLoading data time:{} \n\tSeperating inputs from outputs".format(time.time() - start))
    good_data = np.delete(good_data, 0, 0)
    np.random.shuffle(good_data)

    print(len(bad_data))
    print(len(good_data))
    if even == True:
        my_data = np.append(bad_data, good_data[:len(bad_data)*2], axis=0)
    else:
        my_data = np.append(bad_data, good_data, axis=0)

    testOutput = []
    for row in my_data:
        testOutput.append(row[2])

    output = []
    for row in my_data:
        output.append(row[2])
    my_data = np.delete(my_data, 2, 1)
    output = np.asarray(output)

    start = time.time()

    print("\t\tSeperation time: {}\n\t\t\tFitting".format(time.time() - start))

    # change 'my_data' into 'input_data' for clarity
    input_data = my_data

    input_data = np.delete(input_data, 0, 0)
    output = np.delete(output, 0, 0)

    return input_data, output




    # print(count)
if __name__ == '__main__':
    main()
